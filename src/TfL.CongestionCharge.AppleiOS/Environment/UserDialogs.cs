using System;
using System.Threading.Tasks;
using CoreGraphics;
using Foundation;
using TelerikUI;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using TfL.CongestionCharge.AppleiOS.UIUtils.Alerts;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Environment
{
    public class UserDialog : IUserDialog
    {
        public async Task ShowError(string message)
        {
            await ShowOkMessageWithLink(message, Strings.ErrorTitle);
        }

        public async Task ShowUnknownError()
        {
            await ShowOkMessageWithLink(Strings.TransactionFailedMessage, Strings.TransactionFailedTitle);
        }

        public async Task ShowWarning(string message)
        {
            await ShowMessage(message, Strings.Warning);
        }

        public async Task ShowMessage(string message, string title)
        {
            await ShowOkMessageWithLink(message, title);
        }

        public async Task<bool> Show2ButtonMessage(string message, string title, string buttonConfirmText, string buttonCancelText)
        {
            var tcs = new TaskCompletionSource<bool>();
            var alertView = AlertHelper.GetFormattedAlert(message, title);
            alertView.AddActionWithTitle(buttonConfirmText, (alert, action) =>
            {
                tcs.SetResult(true);
                alertView.Dismiss(true);
                return true;
            });
            alertView.AddActionWithTitle(buttonCancelText, (alert, action) =>
            {
                tcs.SetResult(false);
                alertView.Dismiss(true);
                return false;
            });
            alertView.Show(true);
            return await tcs.Task;
        }

        public Task ShowOkMessageWithLink(string message, string title)
        {
            var tcs = new TaskCompletionSource<bool?>();
            var alertView = AlertHelper.GetFormattedAlert(message, title);
            alertView.AddActionWithTitle(Strings.Ok, (alert, action) =>
            {
                tcs.SetResult(true);
                alertView.Dismiss(true);
                return true;
            });
            alertView.Show(true);
            return tcs.Task;
        }
    }
}