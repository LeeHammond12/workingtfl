using System.Diagnostics;
using System.Linq;
using Foundation;
using GalaSoft.MvvmLight.Views;
using Tfl.CongestionCharge.Core.Environment;
using UIKit;
using System;

namespace TfL.CongestionCharge.AppleiOS.Environment
{
    public class AppNavigationService : IAppNavigationService
    {
        private readonly NavigationService _navigationService;

        public AppNavigationService(UINavigationController controller)
        {
            _navigationService = new NavigationService();

            var nav = _navigationService;
            nav.Initialize(controller);

            nav.Configure(AppPage.ChooseAVehicle.ToString(), GetWhichVehicleInfo);
            nav.Configure(AppPage.LookupVrm.ToString(), GetLookupVrm);
            nav.Configure(AppPage.ConfirmVrm.ToString(), GetVehicleDetails);
            nav.Configure(AppPage.PcnLookup.ToString(), GetPcnLookup);
            nav.Configure(AppPage.PostCodeChecker.ToString(), GetPostCodeChecker);
            nav.Configure(AppPage.ChooseDates.ToString(), GetDates);
            nav.Configure(AppPage.JourneyDetails.ToString(), GetJourneyDetails);
            nav.Configure(AppPage.AllDone.ToString(), GetAllDone);
            nav.Configure(AppPage.SelectPaymentCard.ToString(), SelectPaymentCard);
            nav.Configure(AppPage.Payment.ToString(), GetPayment);
            nav.Configure(AppPage.ViewReceipt.ToString(), GetReceipts);
            nav.Configure(AppPage.TermsAndConditionsBrowser.ToString(), GetTermsAndConditionsWebView);
            nav.Configure(AppPage.PrivacyPolicyBrowser.ToString(), GetPrivacyPolicyBrowser);
            nav.Configure(AppPage.LoginPageKey.ToString(), GetLogin);
            nav.Configure(AppPage.YourVehiclesAndCards.ToString(), GetManageStoredInformation);
            nav.Configure(AppPage.YourVehiclesAnonymousUser.ToString(), GetAnonymousUserVehicles);
            nav.Configure(AppPage.AnonymousUserYourPaymentMethods.ToString(), GetAnonymousUserYourPaymentMethods);
            nav.Configure(AppPage.YourAccount.ToString(), GetYourAccountViewController);

            _navigationService.Configure(AppPage.YourVehiclesAndServices.ToString(), GetYourVehiclesAndServicesViewController);
            _navigationService.Configure(AppPage.LoggedInUserYourPaymentMethods.ToString(), GetLoggedInUserYourPaymentMethodsViewController);
            nav.Configure(AppPage.TermsAndConditions.ToString(), GetTermAndCondition);
            nav.Configure(AppPage.ManageCcAutoPay.ToString(), GetManageAutoCcPay);
            nav.Configure(AppPage.AutoPayCreditCard.ToString(), GetAutoPayPaymentCard);
            nav.Configure(AppPage.AutoPayVehicles.ToString(), GetAutoPayVehicles);
        }

        public void NavigateTo(AppPage page, object viewModel)
        {
            var controller = GetViewController(page);
            var c = _navigationService.NavigationController.ViewControllers.FirstOrDefault(
                  x => x.IsKindOfClass(controller.Class));

            if (c != null)
            {
                _navigationService.NavigationController.PopToViewController(c, true);
            }
            else
            {
                _navigationService.NavigateTo(page.ToString(), viewModel);
            }
        }

        public void NavigateToAppDownloadPage()
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("https://itunes.apple.com/us/app/tfl-congestion-charge/id1115214764?ls=1&mt=8"));
        }

        public void NavigateToAppStoreReview()
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("https://itunes.apple.com/us/app/tfl-congestion-charge/id1115214764?ls=1&mt=8"));
        }

        public void NavigateHomeAndClearBackStack()
        {
            var navController = UIApplication.SharedApplication.KeyWindow?.RootViewController as UINavigationController;
            var firstVc = navController?.ViewControllers.FirstOrDefault();

            if (firstVc != null)
            {
                navController.PopToViewController(firstVc, false);
            }
        }

        public void NavigateToLoginAndClearBackStack()
        {
            var navController = UIApplication.SharedApplication.KeyWindow?.RootViewController as UINavigationController;
            var firstVc = navController?.ViewControllers.FirstOrDefault();

            if (firstVc == null)
            {
                return;
            }

            var loginViewController = GetLogin(string.Empty);
            navController.PushViewController(loginViewController, true);
            foreach (var navControllerViewController in navController?.ViewControllers)
            {
                if (Equals(navControllerViewController, loginViewController) ||
                    Equals(navControllerViewController, firstVc))
                {
                    return;
                }

                navControllerViewController.RemoveFromParentViewController();
            }
        }

        public void GoBackOnePage()
        {
            var navController = UIApplication.SharedApplication.KeyWindow?.RootViewController as UINavigationController;

            if (navController?.ViewControllers.Count() > 1)
            {
                navController.PopViewController(true);
            }
        }

        private UIViewController GetViewController(AppPage key)
        {
            switch (key)
            {
                case AppPage.ChooseAVehicle:
                    return GetWhichVehicleInfo(AppPage.ChooseAVehicle.ToString());
                case AppPage.LookupVrm:
                    return GetLookupVrm(AppPage.LookupVrm.ToString());
                case AppPage.ConfirmVrm:
                    return GetVehicleDetails(AppPage.ConfirmVrm.ToString());
                case AppPage.PcnLookup:
                    return GetPcnLookup(AppPage.PcnLookup.ToString());
                case AppPage.PostCodeChecker:
                    return GetPostCodeChecker(AppPage.PostCodeChecker.ToString());
                case AppPage.ChooseDates:
                    return GetDates(AppPage.ChooseDates.ToString());
                case AppPage.JourneyDetails:
                    return GetJourneyDetails(AppPage.JourneyDetails.ToString());
                case AppPage.AllDone:
                    return GetAllDone(AppPage.AllDone.ToString());
                case AppPage.SelectPaymentCard:
                    return SelectPaymentCard(AppPage.SelectPaymentCard.ToString());
                case AppPage.Payment:
                    return GetPayment(AppPage.Payment.ToString());
                case AppPage.ViewReceipt:
                    return GetReceipts(AppPage.ViewReceipt.ToString());
                case AppPage.TermsAndConditionsBrowser:
                    return GetTermsAndConditionsWebView(AppPage.TermsAndConditionsBrowser.ToString());
                case AppPage.PrivacyPolicyBrowser:
                    return GetPrivacyPolicyBrowser(AppPage.PrivacyPolicyBrowser.ToString());
                case AppPage.LoginPageKey:
                    return GetLogin(AppPage.LoginPageKey.ToString());
                case AppPage.YourVehiclesAndCards:
                    return GetManageStoredInformation(AppPage.YourVehiclesAndCards.ToString());
                case AppPage.YourVehiclesAnonymousUser:
                    return GetAnonymousUserVehicles(AppPage.YourVehiclesAnonymousUser.ToString());
                case AppPage.AnonymousUserYourPaymentMethods:
                    return GetAnonymousUserYourPaymentMethods(AppPage.AnonymousUserYourPaymentMethods.ToString());
                case AppPage.YourAccount:
                    return GetYourAccountViewController(AppPage.YourAccount.ToString());
                case AppPage.YourVehiclesAndServices:
                    return GetYourVehiclesAndServicesViewController(AppPage.YourVehiclesAndServices.ToString());
                case AppPage.LoggedInUserYourPaymentMethods:
                    return GetLoggedInUserYourPaymentMethodsViewController(AppPage.LoggedInUserYourPaymentMethods.ToString());
                case AppPage.TermsAndConditions:
                    return GetTermAndCondition(AppPage.TermsAndConditions.ToString());
                case AppPage.ManageCcAutoPay:
                    return GetManageAutoCcPay(AppPage.ManageCcAutoPay.ToString());
                case AppPage.AutoPayCreditCard:
                    return GetAutoPayPaymentCard(AppPage.AutoPayCreditCard.ToString());
                case AppPage.AutoPayVehicles:
                    return GetAutoPayVehicles(AppPage.AutoPayVehicles.ToString());
                default:
                    return null;
            }
        }

        private UIViewController GetLookupVrm(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("LookupVrmViewController");
        }

        private UIViewController GetVehicleDetails(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("ConfirmVrmViewController");
        }

        private UIViewController GetWhichVehicleInfo(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("WhichVehicleViewController");
        }

        private UIViewController GetDates(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("ChooseDatesViewController");
        }

        private UIViewController GetJourneyDetails(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("JourneyDetailsViewController");
        }

        private UIViewController SelectPaymentCard(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("SelectPaymentCardViewController");
        }

        private UIViewController GetPayment(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("PaymentViewController");
        }

        private UIViewController GetAllDone(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("AllDoneViewController");
        }

        private UIViewController GetPcnLookup(object arg)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("PcnLookupViewController");
        }

        private UIViewController GetReceipts(object arg)
        {
            var storyBoard = UIStoryboard.FromName("PayCCFlow", null);
            return storyBoard.InstantiateViewController("ViewReceiptViewController");
        }

        private UIViewController GetPostCodeChecker(object arg)
        {
            var storyBoard = UIStoryboard.FromName("Main", null);
            return storyBoard.InstantiateViewController("PostCodeCheckViewController");
        }

        private UIViewController GetLogin(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("Main", null);
            return storyBoard.InstantiateViewController("LoginViewController");
        }

        private UIViewController GetTermAndCondition(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("Main", null);
            return storyBoard.InstantiateViewController("TermAndConditionsViewController");
        }

        private UIViewController GetTermsAndConditionsWebView(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("Main", null);
            return storyBoard.InstantiateViewController("TermsAndConditionsWebViewViewController");
        }

        private UIViewController GetPrivacyPolicyBrowser(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("Main", null);
            return storyBoard.InstantiateViewController("PrivacyPolicyBrowserViewController");
        }

        private UIViewController GetManageStoredInformation(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("AnonymousUser", null);
            return storyBoard.InstantiateViewController("ManageStoredInformationViewController");
        }

        private UIViewController GetAnonymousUserVehicles(object inputString)
        {
            var storyBoard = UIStoryboard.FromName("AnonymousUser", null);
            return storyBoard.InstantiateViewController("AnonymousUserYourVehiclesViewController");
        }

        private UIViewController GetAnonymousUserYourPaymentMethods(object arg)
        {
            var storyBoard = UIStoryboard.FromName("AnonymousUser", null);
            return storyBoard.InstantiateViewController("AnonymousUserYourPaymentMethodsViewController");
        }

        private UIViewController GetYourAccountViewController(object arg)
        {
            var storyBoard = UIStoryboard.FromName("LoggedInUser", null);
            return storyBoard.InstantiateViewController("YourAccountViewController");
        }

        private UIViewController GetLoggedInUserYourPaymentMethodsViewController(object arg)
        {
            var storyBoard = UIStoryboard.FromName("LoggedInUser", null);
            return storyBoard.InstantiateViewController("LoggedInUserYourPaymentMethodsViewController");
        }

        private UIViewController GetYourVehiclesAndServicesViewController(object arg)
        {
            var storyBoard = UIStoryboard.FromName("LoggedInUser", null);
            return storyBoard.InstantiateViewController("YourVehiclesAndServicesViewController");
        }

        private UIViewController GetManageAutoCcPay(object arg)
        {
            var storyBoard = UIStoryboard.FromName("LoggedInUser", null);
            return storyBoard.InstantiateViewController("ManageCcAutoPayViewController");
        }

        private UIViewController GetAutoPayPaymentCard(object arg)
        {
            var storyBoard = UIStoryboard.FromName("LoggedInUser", null);
            return storyBoard.InstantiateViewController("AutoPayPaymentCardViewController");
        }

        private UIViewController GetAutoPayVehicles(object arg)
        {
            var storyBoard = UIStoryboard.FromName("LoggedInUser", null);
            return storyBoard.InstantiateViewController("AutoPayVehiclesViewController");
        }
    }
}