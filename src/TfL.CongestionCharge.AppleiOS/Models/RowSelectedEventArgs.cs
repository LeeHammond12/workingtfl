﻿using System;
using Foundation;

namespace TfL.CongestionCharge.AppleiOS.Models
{
    public class RowSelectedEventArgs : EventArgs
    {
        public RowSelectedEventArgs(NSIndexPath indexPath)
        {
            IndexPath = indexPath;
        }

        public NSIndexPath IndexPath { get; }
    }
}