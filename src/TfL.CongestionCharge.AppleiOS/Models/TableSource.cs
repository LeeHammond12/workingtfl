﻿using System;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Models
{
    public class TableSource : UITableViewSource
    {
        private readonly string[] _tableItems;

        private readonly string _cellIdentifier = "TableCell";

        public TableSource(string[] items)
        {
            _tableItems = items;
        }

        public event EventHandler RowClicked = delegate { };

        public string SelectedRow { get; set; }

        public NSIndexPath NsIndexPathCurrent { get; set; }

        public NSIndexPath NsIndexPathPrevious { get; set; }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _tableItems.Length;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var item = _tableItems[indexPath.Row];
            var cell = new UITableViewCell(UITableViewCellStyle.Default, _cellIdentifier);
            cell.TextLabel.Text = item;
            cell.TextLabel.SetBook(false, UiConstants.SmallFont);
            if (indexPath.Equals(NsIndexPathCurrent))
            {
                cell.Accessory = UITableViewCellAccessory.Checkmark;
            }

            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            NsIndexPathCurrent = indexPath;
            SelectedRow = _tableItems[indexPath.Row];
            RowClicked(this, new RowSelectedEventArgs(indexPath));
            tableView.DeselectRow(indexPath, true);
        }
    }
}