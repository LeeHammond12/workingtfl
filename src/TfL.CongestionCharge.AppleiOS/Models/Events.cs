﻿namespace TfL.CongestionCharge.AppleiOS.Models
{
    public static class Events
    {
        public const string TouchUpInside = "TouchUpInside";
        public const string EditingChanged = "EditingChanged";
        public const string ValueChanged = "ValueChanged";
    }
}