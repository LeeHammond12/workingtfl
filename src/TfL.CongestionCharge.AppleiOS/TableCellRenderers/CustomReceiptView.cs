﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using Tfl.CongestionCharge.Core.ViewModel.Receipts;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.TableCellRenderers
{
    public sealed class CustomReceiptView : UITableViewCell
    {
        private readonly List<Tuple<UILabel, UILabel>> _elements;
        private readonly List<UIView> _seprators;
        private readonly UIView _view;
        private readonly int _numberOfColumn;

        public CustomReceiptView(NSString cellId, int numberOfColumn) : base(UITableViewCellStyle.Default, cellId)
        {
            _numberOfColumn = numberOfColumn;
            SelectionStyle = UITableViewCellSelectionStyle.None;

            ContentView.BackgroundColor = UiConstants.TableViewCellBackground;

            _view = new UIView(new CGRect(0, 6, 280, 44));

            _view.SetCardViewStyle();
            _view.BackgroundColor = UIColor.White;

            _elements = new List<Tuple<UILabel, UILabel>>();
            _seprators = new List<UIView>();
            for (var i = 0; i <= _numberOfColumn; i++)
            {
                var colView0 = GetLabelView(true);
                var colView1 = GetLabelView(false);
                var seprator = GetSepratorView();
                _view.AddSubviews(colView0, colView1);
                if (i != _numberOfColumn)
                {
                    _view.AddSubview(seprator);
                    _seprators.Add(seprator);
                }
                _elements.Add(new Tuple<UILabel, UILabel>(colView0, colView1));
            }

            ContentView.AddSubview(_view);
        }

        public void UpdateCell(ReceiptViewModel vm)
        {
            for (var i = 0; i < vm.Fields.Count; i++)
            {
                _elements[i].Item1.Text = vm.Fields[i].Key;
                _elements[i].Item2.Text = vm.Fields[i].Value;
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            nfloat topMargin = 8;
            nfloat cellHeight = 30;

            for (var i = 0; i < _numberOfColumn; i++)
            {
                var yPos = i == 0 ? topMargin : (i * cellHeight) + topMargin;

                _elements[i].Item1.Frame = new CGRect(8, yPos, 100, cellHeight);
                _elements[i].Item2.Frame = new CGRect(120, yPos, 220, cellHeight);
                _seprators[i].Hidden = i == _numberOfColumn - 1;
                if (i != _numberOfColumn)
                {
                    _seprators[i].Frame = new CGRect(8, yPos + cellHeight, ContentView.Frame.Width - 16, 0.5);
                }
            }

            var frame = _view.Frame;
            frame.Height = ContentView.Frame.Height - 10;
            frame.Width = ContentView.Frame.Width;
            _view.Frame = frame;
        }

        private static UILabel GetLabelView(bool isLeftLabel)
        {
            var label = new UILabel();
            if (isLeftLabel)
            {
                label.SetLightTextStyle();
            }
            else
            {
                label.SetNormalTextStyle();
            }

            return label;
        }

        private static UIView GetSepratorView()
        {
            return new UIView
            {
                BackgroundColor = UiConstants.DividerColor
            };
        }
    }
}