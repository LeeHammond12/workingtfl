﻿using Cirrious.FluentLayouts.Touch;
using Foundation;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.TableCellRenderers
{
    public class CustomCardView : UITableViewCell
    {
        private readonly UILabel _cardNumber;
        private readonly UIImageView _cardImage;

        public CustomCardView(NSString cellId) : base(UITableViewCellStyle.Default, cellId)
        {
            SelectionStyle = UITableViewCellSelectionStyle.Default;
            ContentView.BackgroundColor = UIColor.White;

            _cardImage = new UIImageView();

            _cardNumber = new UILabel
            {
                TextColor = AppDelegate.Current.PrimaryBlue,
                BackgroundColor = UIColor.Clear,
                Font = UIFont.FromName("NJFont-Book", 15)
            };

            ContentView.AddSubviews(_cardImage, _cardNumber);
        }

        public void UpdateCell(string ccNumber, string imageFilename)
        {
            _cardNumber.Text = ccNumber;
            _cardImage.Image = UIImage.FromFile(imageFilename);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            ContentView.RemoveConstraints(ContentView.Constraints);
            ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            ContentView.AddConstraints(
                _cardImage.AtTopOf(ContentView, 8),
                _cardImage.WithSameLeft(ContentView).Plus(8),
                _cardImage.Height().EqualTo(30),
                _cardImage.Width().EqualTo(40),
                _cardNumber.WithSameCenterY(_cardImage),
                _cardNumber.ToRightOf(_cardImage, 10));
        }
    }
}