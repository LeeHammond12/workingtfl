﻿namespace TfL.CongestionCharge.AppleiOS.TableCellRenderers
{
    using UIKit;

    public class ReceiptClass
    {
        protected UITableViewCellStyle cellStyle = UITableViewCellStyle.Default;

        protected UITableViewCellAccessory cellAccessory = UITableViewCellAccessory.None;

        public ReceiptClass()
        {
        }

        public ReceiptClass(string receipt)
        {
            ReceiptId = receipt;
        }

        public string ReceiptId { get; set; }

        public string VRM { get; set; }

        public string Price { get; set; }

        public string Date { get; set; }

        public string ChargeType { get; set; }

        public UITableViewCellStyle CellStyle
        {
            get { return cellStyle; }
            set { cellStyle = value; }
        }

        public UITableViewCellAccessory CellAccessory
        {
            get { return cellAccessory; }
            set { cellAccessory = value; }
        }
    }
}