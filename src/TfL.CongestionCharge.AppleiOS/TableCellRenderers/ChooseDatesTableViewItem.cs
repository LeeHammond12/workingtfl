﻿using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.TableCellRenderers
{
    public class ChooseDatesTableViewItem
    {
        public ChargeViewModelBase Charge { get; set; }
        public SingleDayChargeViewModel SingleDayCharge { get; set; }
        public MultipleDayChargeViewModel MultipleDayChargeViewModel { get; set; }

        public decimal Amount => Charge.Amount;

        public string DateType => Charge.ChargePeriodString;

        public string Date => Charge.DateString;

        public string Message => MultipleDayChargeViewModel?.ConsecutiveDaysString ?? string.Empty;

        public UITableViewCellStyle CellStyle { get; set; } = UITableViewCellStyle.Default;

        public UITableViewCellAccessory CellAccessory { get; set; } = UITableViewCellAccessory.None;

        public ChooseDatesTableViewItem(ChargeViewModelBase charge)
        {
            Charge = charge;
        }
    }
}
