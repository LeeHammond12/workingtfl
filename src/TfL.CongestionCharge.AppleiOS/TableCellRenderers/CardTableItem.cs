﻿using Tfl.CongestionCharge.Core.ViewModel;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.TableCellRenderers
{
    public class CardTableItem
    {
        public UITableViewCellStyle CellStyle => UITableViewCellStyle.Default;

        public UITableViewCellAccessory CellAccessory => UITableViewCellAccessory.None;

        public PaymentCardViewModel PaymentCard { get; }

        public string CardImage => $"{PaymentCard.Issuer.ToLower()}";

        public CardTableItem(PaymentCardViewModel paymentCard)
        {
            PaymentCard = paymentCard;
        }
    }
}