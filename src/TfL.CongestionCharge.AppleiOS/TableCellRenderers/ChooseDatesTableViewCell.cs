﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Resources;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.TableCellRenderers
{
    public class ChooseDatesTableViewCell : UITableViewCell
    {
        private const int NumberMultipleDaysFields = 4;
        private readonly List<UILabel> _elements;
        private bool _isMultipleDays;

        public ChooseDatesTableViewCell(NSString cellId)
            : base(UITableViewCellStyle.Default, cellId)
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;

            ContentView.BackgroundColor = UIColor.White;

            _elements = new List<UILabel>();

            for (var i = 0; i <= NumberMultipleDaysFields; i++)
            {
                var colView0 = GetLabelView(i);

                ContentView.AddSubviews(colView0);

                _elements.Add(colView0);
            }
        }

        public void UpdateCell(ChooseDatesTableViewItem selectDate)
        {
            _isMultipleDays = !(selectDate.DateType.Equals(Strings.DailyCongestionCharge) || selectDate.DateType.Equals(Strings.PreviousChargingDay));

            if (_isMultipleDays)
            {
                _elements[0].Text = selectDate.Amount.ToCurrencyString();
                _elements[1].Text = selectDate.DateType;
                _elements[2].Text = selectDate.Date;
                _elements[3].Text = selectDate.Message;
            }
            else
            {
                _elements[0].Text = selectDate.Amount.ToCurrencyString();
                _elements[1].Text = selectDate.DateType;
                _elements[2].Text = selectDate.Date;
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            nfloat topMargin = 20;
            nfloat leftMargin = 10;

            for (var i = 0; i <= (_isMultipleDays ? 4 : 3); i++)
            {
                var yPos = i == 0 || i == 1 ? 0 : (i - 1) * topMargin;
                var xPos = i == 0 ? leftMargin : leftMargin * 8;

                _elements[i].Frame = new CGRect(xPos, yPos, 200, 30);
            }
        }

        private static UILabel GetLabelView(int column)
        {
            return new UILabel
            {
                TextColor = column % 2 != 0 ? AppDelegate.Current.PrimaryBlue : UIColor.Black,
                BackgroundColor = UIColor.Clear,
                Font = UIFont.FromName("NJFont-Book", column == 0 || column == 1 ? UiConstants.LargeFont : UiConstants.SmallFont),
            };
        }
    }
}