﻿using System;
using System.Collections.Generic;
using Foundation;
using Tfl.CongestionCharge.Core.ViewModel.Payments;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.TableCellRenderers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class CardTableSource : UITableViewSource
    {
        private readonly List<CardTableItem> _tblItems;
        private readonly NSString _cellIdentifier = new NSString("TableCell");
        private readonly PaymentViewModel _paymentViewModel;

        public CardTableSource(List<CardTableItem> items, PaymentViewModel vmModel)
        {
            _tblItems = items;
            _paymentViewModel = vmModel;
        }

        public event EventHandler RowClicked = delegate { };

        public NSIndexPath NsIndexPathCurrent { get; set; }

        public NSIndexPath NsIndexPathPrevious { get; set; }

        public CardTableItem SelectedCard { get; set; }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _tblItems.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            NsIndexPathCurrent = indexPath;
            SelectedCard = _tblItems[indexPath.Row];
            RowClicked(this, new RowSelectedEventArgs(indexPath));
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = new CustomCardView(_cellIdentifier);
            cell.UpdateCell(_tblItems[indexPath.Row].PaymentCard.Last4Digits, _tblItems[indexPath.Row].CardImage);
            if (indexPath.Equals(NsIndexPathCurrent))
            {
                cell.Accessory = UITableViewCellAccessory.Checkmark;
            }

            return cell;
        }
    }
}