﻿using System;
using System.Collections.Generic;
using Tfl.CongestionCharge.Core.Extensions;
using Foundation;
using UIKit;
using TfL.CongestionCharge.AppleiOS.Cells;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class PayCcChooseDatesTableViewSource : UITableViewSource
    {
        private IReadOnlyList<ChargeViewModelBase> TblItems => _viewModel.Charges;

        private readonly ChooseDatesViewModel _viewModel;

        public PayCcChooseDatesTableViewSource(ChooseDatesViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return TblItems.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            _viewModel.SelectCharge(TblItems[indexPath.Row]);
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var charge = TblItems[indexPath.Row];
            var multiple = charge as MultipleDayChargeViewModel;
            var cell =
                (ChooseDateTableViewCell)tableView.DequeueReusableCell(ChooseDateTableViewCell.Key);

            cell.UpdateCell(charge.Amount.ToCurrencyString(), charge.ChargePeriodString, charge.DateString, multiple != null ? multiple.ConsecutiveDaysString : string.Empty, multiple != null);

            cell.SelectionStyle = UITableViewCellSelectionStyle.None;

            return cell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 76;
        }
    }
}