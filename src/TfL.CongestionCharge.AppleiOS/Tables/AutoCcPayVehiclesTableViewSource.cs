﻿using System;
using System.Collections.Generic;
using Foundation;
using ObjCRuntime;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Cells;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class AutoCcPayVehiclesTableViewSource : UITableViewSource
    {
        private readonly IReadOnlyList<VehicleViewModel> _tblItems;
        private readonly Action<VehicleViewModel> _clickAction;
        private string _cellIcon;

        public AutoCcPayVehiclesTableViewSource(IReadOnlyList<VehicleViewModel> items, Action<VehicleViewModel> clickAction, string cellIcon)
        {
            _tblItems = items;
            _clickAction = clickAction;
            _cellIcon = cellIcon;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _tblItems.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var nibs = NSBundle.MainBundle.LoadNib(AutoCcPayVehicleTableViewCell.Key, tableView, null);
            var cell = Runtime.GetNSObject(nibs.ValueAt(0)) as AutoCcPayVehicleTableViewCell;
            var viewModel = _tblItems[indexPath.Row];
            cell.UpdateCell(viewModel.Vrm, viewModel.ColorMakeModel, _cellIcon, indexPath.Row == RowsInSection(tableView, indexPath.Section) - 1);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;
            cell.Button.TouchUpInside += (sender, args) =>
            {
                _clickAction.Invoke(viewModel);
            };
            return cell;
        }
    }
}