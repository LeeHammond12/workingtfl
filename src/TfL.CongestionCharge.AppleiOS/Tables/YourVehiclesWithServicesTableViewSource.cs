﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreGraphics;
using Foundation;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model.AccountInfo;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class YourVehiclesWithServicesTableViewSource : UITableViewSource
    {
        private readonly IReadOnlyList<VehicleViewModel> _vehicles;
        private readonly Func<VehicleViewModel, Task> _deleteAction;

        public YourVehiclesWithServicesTableViewSource(IReadOnlyList<VehicleViewModel> vehicles, Func<VehicleViewModel, Task> deleteAction)
        {
            _vehicles = vehicles;
            _deleteAction = deleteAction;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var service = GetService(indexPath.Section, indexPath.Row);

            var cell =
                tableView.DequeueReusableCell(YourVehicleAndServiceTableViewCell.Key) as
                    YourVehicleAndServiceTableViewCell;
            if (service == null)
            {
                return cell;
            }
            cell?.UpdateCell(service.Name ?? "Unknown", service.EndDate.HasValue ? service.EndDate.Value.ToHumanReadableFormat() : string.Empty, service.EndDate.HasValue, indexPath.Row == RowsInSection(tableView, indexPath.Section) - 1);
            
            return cell;
        }

        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
        {
            if (indexPath.Row != RowsInSection(tableView, indexPath.Section) - 1)
            {
                return;
            }
            cell.MakeRoundedCorner(UIRectCorner.BottomLeft | UIRectCorner.BottomRight);
        }

        public override void WillDisplayHeaderView(UITableView tableView, UIView headerView, nint section)
        {
            headerView.MakeRoundedCorner(UIRectCorner.TopLeft | UIRectCorner.TopRight);
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _vehicles[Convert.ToInt32(section)].Services.Count;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return _vehicles.Count;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            var vehicle = GetVehicle(Convert.ToInt32(section));
            return string.IsNullOrWhiteSpace(vehicle.Vrm) ? 30 : 50;
        }

        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            var cell = tableView.DequeueReusableCell(YourVehicleAndServiceTableViewCellHeader.Key) as YourVehicleAndServiceTableViewCellHeader;
            var vehicle = GetVehicle(Convert.ToInt32(section));
            cell.BackgroundColor = UiConstants.PageHeaderLabelColor;
            cell.UpdateCell(vehicle.Vrm, vehicle.ColorMakeModel);
            cell.DeleteButton.TouchUpInside += (s, e) =>
            {
                _deleteAction.Invoke(vehicle);
                tableView.ReloadData();
            };
            return cell;
        }

        public override UIView GetViewForFooter(UITableView tableView, nint section)
        {
            var view = new UIView(new CGRect(0, 5, tableView.Bounds.Size.Width, 8))
            {
                BackgroundColor = UIColor.White
            };
            return view;
        }

        public override nfloat GetHeightForFooter(UITableView tableView, nint section)
        {
            return 8;
        }

        private VehicleViewModel GetVehicle(int position)
            => _vehicles[position];

        private AccountTflServiceDto GetService(int groupPosition, int childPosition)
            => _vehicles[groupPosition]?.Services[childPosition];
    }
}