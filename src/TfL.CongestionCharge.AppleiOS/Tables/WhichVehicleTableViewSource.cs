﻿using System;
using System.Collections.Generic;
using Foundation;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Cells;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class WhichVehicleTableViewSource : UITableViewSource
    {
        private readonly IReadOnlyList<VehicleViewModel> _tblItems;
        private readonly Action<VehicleViewModel> _clickAction;

        public WhichVehicleTableViewSource(IReadOnlyList<VehicleViewModel> items, Action<VehicleViewModel> clickAction)
        {
            _tblItems = items;
            _clickAction = clickAction;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _tblItems.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            // RowClicked(this, new RowSelectedEventArgs(indexPath));
            _clickAction?.Invoke(_tblItems[indexPath.Row]);
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell =
                (VehicleDetailsTableViewCell)tableView.DequeueReusableCell(VehicleDetailsTableViewCell.Key);
            cell.UpdateCell(_tblItems[indexPath.Row].Vrm, _tblItems[indexPath.Row].ColorMakeModel);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;

            return cell;
        }
    }
}