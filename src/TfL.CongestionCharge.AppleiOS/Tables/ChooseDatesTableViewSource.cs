﻿using System;
using System.Collections.Generic;
using Foundation;
using Tfl.CongestionCharge.Core.Resources;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.TableCellRenderers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    internal class ChooseDatesTableViewSource : UITableViewSource
    {
        private readonly NSString _cellIdentifier = new NSString("TableCell");
        private readonly List<ChooseDatesTableViewItem> _items;

        public ChooseDatesTableViewSource(List<ChooseDatesTableViewItem> items)
        {
            _items = items;
        }

        public event EventHandler RowClicked = delegate { };

        public NSIndexPath NsIndexPathCurrent { get; set; }

        public NSIndexPath NsIndexPathPrevious { get; set; }

        public ChooseDatesTableViewItem SelectedDate { get; set; }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var receiptViewModelItem = _items[indexPath.Row];
            var cell = new ChooseDatesTableViewCell(_cellIdentifier);

            cell.UpdateCell(receiptViewModelItem);

            if (indexPath.Equals(NsIndexPathCurrent))
            {
                cell.Accessory = UITableViewCellAccessory.Checkmark;
            }

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _items.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            NsIndexPathCurrent = indexPath;
            SelectedDate = _items[indexPath.Row];
            RowClicked(this, new RowSelectedEventArgs(indexPath));
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return _items[indexPath.Row].DateType.Equals(Strings.PreviousChargingDay) || _items[indexPath.Row].DateType.Equals(Strings.DailyCongestionCharge) ? 50 : 70;
        }
    }
}