﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.AppleiOS.Cells;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;
using Tfl.CongestionCharge.Core.Extensions;
using ObjCRuntime;
using TfL.CongestionCharge.AppleiOS.Utilities;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class JourneyDetailsTableViewSource : UITableViewSource
    {
        private readonly List<JourneyDetailsViewModel.GroupedJourneyViewModel> _groupedJourneys;
        private readonly JourneyDetailsViewModel _viewModel;

        public JourneyDetailsTableViewSource(List<JourneyDetailsViewModel.GroupedJourneyViewModel> groupedJourneys, JourneyDetailsViewModel viewModel)
        {
            _viewModel = viewModel;
            _groupedJourneys = groupedJourneys;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var nibs = NSBundle.MainBundle.LoadNib(JourneyDetailsTableViewCell.Key, tableView, null);
            var cell = Runtime.GetNSObject(nibs.ValueAt(0)) as JourneyDetailsTableViewCell;
            var journey = GetChildJourney(indexPath.Section, indexPath.Row);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;
            cell.UpdateCell(journey.ChargeStartDate.ToHumanReadableFormat(), journey.Price.ToCurrencyString(), journey.ConsecutiveDaysString, () => DeleteJourney(journey, tableView), indexPath.Row != 0, indexPath.Row == RowsInSection(tableView, indexPath.Section) - 1);
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _groupedJourneys[Convert.ToInt32(section)].Journeys.Count;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return _groupedJourneys.Count;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return 50;
        }

        public override UIView GetViewForFooter(UITableView tableView, nint section)
        {
            return new UIView(new CGRect(0, 0, tableView.Bounds.Size.Width, 8))
            {
                BackgroundColor = UIColor.White
            };
        }

        public override nfloat GetHeightForFooter(UITableView tableView, nint section)
        {
            return 8;
        }

        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            var nibs = NSBundle.MainBundle.LoadNib(JourneyDetailsTableViewHeaderCell.Key, tableView, null);
            var cell = Runtime.GetNSObject(nibs.ValueAt(0)) as JourneyDetailsTableViewHeaderCell;
            var journey = GetJourney(Convert.ToInt32(section));
            var vehicle = journey.Vehicle;
            cell.BackgroundColor = UiConstants.PageHeaderLabelColor;
            cell.UpdateCell(vehicle.Vrm, vehicle.ColorMakeModel, () => _viewModel.AddDate(vehicle, journey.IsRegisteredInUk));
            return cell;
        }

        public override void WillDisplayHeaderView(UITableView tableView, UIView headerView, nint section)
        {
            headerView.MakeRoundedCorner(UIRectCorner.TopLeft | UIRectCorner.TopRight);
        }

        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
        {
            if (indexPath.Row == RowsInSection(tableView, indexPath.Section) - 1)
            {
                cell.MakeRoundedCorner(UIRectCorner.BottomLeft | UIRectCorner.BottomRight);
            }
        }

        private async void DeleteJourney(JourneyDetails journey, UITableView tableView)
        {
            if (!await _viewModel.DeleteJourney(journey))
            {
                return;
            }

            tableView.ReloadData();
        }

        private JourneyDetailsViewModel.GroupedJourneyViewModel GetJourney(int groupPosition)
            => _groupedJourneys[groupPosition];

        private JourneyDetails GetChildJourney(int groupPosition, int childPosition)
            => _groupedJourneys[groupPosition].Journeys[childPosition];
    }
}