﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Cells;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class StoredCardsTableViewSource : UITableViewSource
    {
        private readonly Action<PaymentCardViewModel> _clickAction;
        private readonly Func<IReadOnlyList<PaymentCardViewModel>> _cardsListGetter;

        public StoredCardsTableViewSource(Func<IReadOnlyList<PaymentCardViewModel>> items, Action<PaymentCardViewModel> clickAction)
        {
            _cardsListGetter = items;
            _clickAction = clickAction;
        }

        private IReadOnlyList<PaymentCardViewModel> Cards => _cardsListGetter.Invoke();

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Cards.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            _clickAction?.Invoke(Cards[indexPath.Row]);
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell =
                (StoredCardTableViewCell)tableView.DequeueReusableCell(StoredCardTableViewCell.Key);
            var card = Cards[indexPath.Row];
            cell.UpdateCell(card.IssuerWithCardPostfix, card.CardNumber, card.ExpiryDateWithSeparator, card.Issuer);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;

            return cell;
        }
    }
}