﻿using System;
using System.Collections.Generic;
using Foundation;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Cells;
using UIKit;
using ObjCRuntime;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class YourVehiclesTableViewSource : UITableViewSource
    {
        private readonly Func<IReadOnlyList<VehicleViewModel>> _vehicleListGetter;
        private readonly Action<VehicleViewModel> _clickAction;

        public YourVehiclesTableViewSource(Func<IReadOnlyList<VehicleViewModel>> items, Action<VehicleViewModel> clickAction)
        {
            _vehicleListGetter = items;
            _clickAction = clickAction;
        }

        public event EventHandler RowClicked = delegate { };

        private IReadOnlyList<VehicleViewModel> Vehicles => _vehicleListGetter.Invoke();

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Vehicles.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var nibs = NSBundle.MainBundle.LoadNib(YourVehicleTableViewCell.Key, tableView, null);
            var cell = Runtime.GetNSObject(nibs.ValueAt(0)) as YourVehicleTableViewCell;
            cell.UpdateCell(Vehicles[indexPath.Row].Vrm, Vehicles[indexPath.Row].ColorMakeModel);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;

            cell.DeleteButton.TouchUpInside += (s, e) =>
            {
                _clickAction.Invoke(Vehicles[indexPath.Row]);
            };

            return cell;
        }
    }
}