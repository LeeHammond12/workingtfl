﻿using System;
using System.Collections.Generic;
using Foundation;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Cells;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class YourVehiclesWithoutServicesTableViewSource : UITableViewSource
    {
        private readonly IReadOnlyList<VehicleViewModel> _vehicles;

        public YourVehiclesWithoutServicesTableViewSource(IReadOnlyList<VehicleViewModel> vehicles)
        {
            _vehicles = vehicles;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(VehicleDetailsTableViewCell.Key) as VehicleDetailsTableViewCell;
            var vehicle = GetVehicle(indexPath.Row);
            cell.UpdateCell(vehicle.Vrm, vehicle.ColorMakeModel, true);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _vehicles.Count;
        }

        private VehicleViewModel GetVehicle(int position)
            => _vehicles[position];
    }
}