﻿using System;
using System.Collections.Generic;
using Foundation;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Cells;
using UIKit;
using ObjCRuntime;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class YourCardsTableViewSource : UITableViewSource
    {
        private readonly Func<IReadOnlyList<PaymentCardViewModel>> _vehicleListGetter;
        private readonly Action<PaymentCardViewModel> _clickAction;

        public YourCardsTableViewSource(Func<IReadOnlyList<PaymentCardViewModel>> items, Action<PaymentCardViewModel> clickAction)
        {
            _vehicleListGetter = items;
            _clickAction = clickAction;
        }

        public event EventHandler RowClicked = delegate { };

        private IReadOnlyList<PaymentCardViewModel> Cards => _vehicleListGetter.Invoke();

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Cards.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var nibs = NSBundle.MainBundle.LoadNib(YourCardTableViewCell.Key, tableView, null);
            var cell = Runtime.GetNSObject(nibs.ValueAt(0)) as YourCardTableViewCell;
            var card = Cards[indexPath.Row];
            cell.UpdateCell(card.IssuerWithCardPostfix, card.CardNumber, card.ExpiryDateWithSeparator, card.Issuer);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;

            cell.DeleteButton.TouchUpInside += (s, e) =>
            {
                _clickAction.Invoke(Cards[indexPath.Row]);
            };

            return cell;
        }
    }
}