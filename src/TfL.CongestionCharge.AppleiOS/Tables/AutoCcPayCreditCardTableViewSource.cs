﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Foundation;
using ObjCRuntime;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Helpers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    public class AutoCcPayCreditCardTableViewSource : UITableViewSource
    {
        private readonly IReadOnlyList<PaymentCardViewModel> _cards;
        private readonly List<PaymentCardViewHolder> _viewHolders = new List<PaymentCardViewHolder>();
        private readonly Func<PaymentCardViewModel, Task<bool>> _cardSelectedAction;

        public AutoCcPayCreditCardTableViewSource(IReadOnlyList<PaymentCardViewModel> cards, Func<PaymentCardViewModel, Task<bool>> cardSelectedAction)
        {
            _cards = cards;
            _cardSelectedAction = cardSelectedAction;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _cards.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.DeselectRow(indexPath, true);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var nibs = NSBundle.MainBundle.LoadNib(AutoCcPayCreditCardTableViewCell.Key, tableView, null);
            var cell = Runtime.GetNSObject(nibs.ValueAt(0)) as AutoCcPayCreditCardTableViewCell;

            var card = _cards[indexPath.Row];
            var isCardSelected = card?.IsAutoPayCard == true;
            cell.UpdateCell(card.IssuerWithCardPostfix, card.CardNumber, card.ExpiryDateWithSeparator, card.Issuer, isCardSelected);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;

            var vh = _viewHolders.FirstOrDefault(v => v.ViewModel == card);
            if (vh == null)
            {
                vh = new PaymentCardViewHolder(card);
                _viewHolders.Add(vh);
            }

            vh.View = cell;
            vh.RadioButton = cell.SelectCardSwitch;
            cell.SelectCardSwitch.ValueChanged += (s, e) => { CheckedChange(vh); };
            return (UITableViewCell)vh.View;
        }

        public void DeselectAllExcept(PaymentCardViewModel selected)
        {
            var others = _viewHolders.Where(r => r.ViewModel != selected);
            foreach (var creditCardViewHolder in others)
            {
                creditCardViewHolder.RadioButton.On = false;
                creditCardViewHolder.RadioButton.UserInteractionEnabled = true;
            }
        }

        private async void CheckedChange(PaymentCardViewHolder viewHolder)
        {
            if (!viewHolder.RadioButton.On)
            {
                return;
            }

            var selected = viewHolder;

            if (!await _cardSelectedAction.Invoke(selected.ViewModel))
            {
                selected.RadioButton.On = false;
            }
            else
            {
                DeselectAllExcept(selected.ViewModel);
            }
        }
    }
}