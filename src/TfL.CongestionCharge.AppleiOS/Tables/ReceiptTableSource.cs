﻿using System;
using System.Collections.Generic;
using Foundation;
using Tfl.CongestionCharge.Core.ViewModel.Receipts;
using TfL.CongestionCharge.AppleiOS.TableCellRenderers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Tables
{
    internal class ReceiptTableSource : UITableViewSource
    {
        private readonly IReadOnlyList<ReceiptViewModel> _tblItems;
        private readonly NSString _cellIdentifier = new NSString("TableCell");

        public ReceiptTableSource(IReadOnlyList<ReceiptViewModel> items)
        {
            _tblItems = items;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _tblItems.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var receiptViewModelItem = _tblItems[indexPath.Row];

            var cell = new CustomReceiptView(_cellIdentifier, receiptViewModelItem.Fields.Count);

            cell.UpdateCell(receiptViewModelItem);
            return cell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return _tblItems[indexPath.Row].Fields.Count * 35;
        }
    }
}