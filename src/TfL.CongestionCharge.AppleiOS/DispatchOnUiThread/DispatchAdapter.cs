﻿using System;
using System.Threading.Tasks;
using Foundation;
using Tfl.CongestionCharge.Core.Environment;

namespace TfL.CongestionCharge.AppleiOS.DispatchOnUiThread
{
    public class DispatchAdapter : IDispatchOnUiThread
    {
        public readonly NSObject Owner;

        public DispatchAdapter(NSObject owner)
        {
            Owner = owner;
        }

        public void Invoke(Action action)
        {
            Owner.BeginInvokeOnMainThread(action);
        }

        public void Invoke(Task<Action> action)
        {
            Owner.BeginInvokeOnMainThread(async () => await action);
        }
    }
}