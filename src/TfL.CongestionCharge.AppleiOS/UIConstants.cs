﻿using System;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public static class UiConstants
    {
        public static nfloat ButtonBottomBarHeight = 4;
        public static nfloat HomeScreenButtonHeight = 50;
        public static nfloat DefaultButtonHeight = 40;
        public static nfloat SmallMargin = 8;
        public static nfloat VerticalMargin = 10;
        public static nfloat HeaderHeight = 50;
        public static nfloat TitleMargin = 12;
        public static float SmallFont = 14;
        public static float LargeFont = 18;
        public static nfloat LeftMargin = 10;
        public static nfloat RightMargin = 10;
        public static nfloat SmallVerticalMargin = 4;
        public static nfloat TextFieldHeight = 30;
        public static nfloat VerticalMarginFromNc = 70;

        public static UIColor CardviewLightGreyBackground = ToUiColor("#F5F5F5");
        public static UIColor GrayButtonColor = ToUiColor("#F5F5F5");
        public static UIColor GrayButtonTextColor = ToUiColor("#000000");
        public static UIColor DefaultBlueButtonColor = ToUiColor("#155893");
        public static UIColor DefaultBlueButtonTextColor = ToUiColor("FFFFFF");
        public static UIColor DefaultAppTextColor = ToUiColor("#2D3039");
        public static UIColor BlueTextColor = ToUiColor("#155994");
        public static UIColor SwitchTintColor = ToUiColor("#153d79");
        public static UIColor TextFieldBorderColor = ToUiColor("#BABABA");
        public static UIColor TextFieldTextColor = ToUiColor("#000000");
        public static UIColor TextFieldPlaceHolderColor = ToUiColor("#B9C1C8");
        public static UIColor LightBlueColor = ToUiColor("#3C90DB");
        public static UIColor BlueTextFieldBackGround = ToUiColor("#64A6E1");
        public static UIColor BlueTextFieldBorderColor = ToUiColor("#3D90DB");
        public static UIColor BlueTextFieldTextColor = ToUiColor("#D4E6F6");
        public static UIColor LightTextColor = ToUiColor("#8C8C8C");
        public static UIColor ScreenBackgroundColor = ToUiColor("#F5F5F5");
        public static UIColor StatusBorderColorGreen = ToUiColor("#E3F7DC");
        public static UIColor StatusBackGroundColorGreen = ToUiColor("#F0FFEB");
        public static UIColor StatusBackGroundColorRed = ToUiColor("#F2D7D7");
        public static UIColor StatusBorderColorRed = ToUiColor("#EFBDBD");
        public static UIColor CardInputColor = ToUiColor("#D4E6F6");
        public static UIColor DividerColor = ToUiColor("#D6D6D4");
        public static UIColor CardInputBorderColor = ToUiColor("#3D90DB");

        public static UIColor PageHeaderLabelColor = ToUiColor("#155994");
        public static UIColor PageSubHeaderLabelColor = ToUiColor("#383939");
        public static UIColor LeftLabelColor = ToUiColor("#979797");
        public static UIColor RightLabelColor = ToUiColor("#414243");
        public static UIColor MainScreenOptionsBackgoColor = ToUiColor("#F5F5F5");
        public static UIColor BalanceLabelColor = ToUiColor("#56B144");
        public static UIColor BalanceStatusViewColor = ToUiColor("#F0FFEB");

        public static float ButtonTitleFont = 16;
        public static float LargeTextFontSize = 16;
        public static float DefaultTextFontSize = 14;
        public static float SmallTextFontSize = 12;
        public static float ListItemHeaderLabelFontSize = 14;
        public static float ListItemSubHeaderLabelFontSize = 12;

        public static UIColor BottomButtomBorderColor => UIColor.Black;

        public static UIColor HyperlinkColor => UIColor.Blue;

        public static UIColor NearWhiteBackground = ToUiColor("#FAFAFA");
        public static UIColor TableCellBorderColor = ToUiColor("#A2A2A2");
        public static UIColor TableViewCellBackground = ToUiColor("#F5F5F5");

        public static UIFont GetFontFromSize(float fontSize, bool bold = false)
        {
            return bold ? UIFont.BoldSystemFontOfSize(fontSize) : UIFont.SystemFontOfSize(fontSize);
        }

        public static UIColor ToUiColor(string hexString, int alpha = 255)
        {
            hexString = hexString.Replace("#", string.Empty);

            if (hexString.Length == 3)
                hexString = hexString + hexString;

            if (hexString.Length != 6)
                throw new Exception("Invalid hex string");

            var red = int.Parse(hexString.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
            var green = int.Parse(hexString.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
            var blue = int.Parse(hexString.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);

            return UIColor.FromRGBA(red, green, blue, alpha);
        }
    }
}