﻿using System;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public class Application
    {
        // This is the main entry point of the application.
        public static void Main(string[] args)
        {
            UIApplication.Main(args, null, "AppDelegate");            
        }
    }
}