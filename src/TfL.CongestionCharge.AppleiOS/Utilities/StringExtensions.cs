﻿using System.Text.RegularExpressions;

namespace TfL.CongestionCharge.AppleiOS.Utilities
{
    public static class StringExtensions
    {
        public static bool ContainsOnlyDigit(this string s)
        {
            int _;
            var options = int.TryParse(s, out _);
            return options;
        }

        public static int GetNumericForm(this string s)
        {
            int n;
            var options = int.TryParse(s, out n);
            return options ? n : 0;
        }

        public static string TrimNonDigitCharacters(this string s)
        {
            var rgx = new Regex("[^a-zA-Z0-9 -]");
            s = rgx.Replace(s, string.Empty);
            return s;
        }

        public static string TrimLastCharacter(this string str)
        {
            return string.IsNullOrEmpty(str) ? str : str.TrimEnd(str[str.Length - 1]);
        }
    }
}