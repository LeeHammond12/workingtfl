﻿using System;
using System.Linq;
using CoreAnimation;
using CoreGraphics;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Utilities
{
    public static class TflUiUtils
    {
        public static void MakeRoundedCorner(this UIView view, UIRectCorner corners)
        {
            var bounds = view.Bounds;
            var maskLayer = new CAShapeLayer();
            var maskPath = UIBezierPath.FromRoundedRect(bounds, corners, new CGSize(4, 4));
            maskLayer.Frame = bounds;
            maskLayer.Path = maskPath.CGPath;
            view.Layer.Mask = maskLayer;
        }

        public static void SetSwitchStyle(this UISwitch switchControl)
        {
            switchControl.Transform = CGAffineTransform.MakeScale(0.75f, 0.75f);
            switchControl.TintColor = UiConstants.SwitchTintColor;
        }

        public static void SetLargeBlueTextStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-BookBold", UiConstants.LargeTextFontSize);
            lbLabel.TextColor = UiConstants.BlueTextColor;
        }

        public static void SetLargeWhiteTextStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-Book", UiConstants.LargeTextFontSize);
            lbLabel.TextColor = UIColor.White;
        }

        public static void SetSmallBlueTextStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-Book", UiConstants.SmallTextFontSize);
            lbLabel.TextColor = UiConstants.BlueTextColor;
        }

        public static void SetSmallLightTextStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-Book", UiConstants.SmallTextFontSize);
            lbLabel.TextColor = UiConstants.LightTextColor;
        }

        public static void SetNormalBlueTextStyle(this UILabel lbLabel, bool isBold = false)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName(isBold ? "NJFont-BookBold" : "NJFont-Book", UiConstants.DefaultTextFontSize);
            lbLabel.TextColor = UiConstants.BlueTextColor;
        }

        public static void SetNormalWhiteTextStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-Book", UiConstants.DefaultTextFontSize);
            lbLabel.TextColor = UIColor.White;
        }

        public static void SetBoldWhiteTextStyle(this UILabel lbLabel, float fontSize = 14.0f)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-BookBold", fontSize);
            lbLabel.TextColor = UIColor.White;
        }

        public static void SetNormalTextStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-Book", UiConstants.DefaultTextFontSize);
            lbLabel.TextColor = UiConstants.DefaultAppTextColor;
        }

        public static void SetNormalTextBoldStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-BookBold", UiConstants.DefaultTextFontSize);
            lbLabel.TextColor = UiConstants.DefaultAppTextColor;
        }

        public static void SetNormalLargeTextBoldStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-BookBold", UiConstants.LargeTextFontSize);
            lbLabel.TextColor = UiConstants.DefaultAppTextColor;
        }

        public static void SetSmallTextStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-Book", UiConstants.SmallTextFontSize);
            lbLabel.TextColor = UiConstants.DefaultAppTextColor;
        }

        public static void SetSmallTextBoldStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-BookBold", UiConstants.SmallTextFontSize);
            lbLabel.TextColor = UiConstants.DefaultAppTextColor;
        }

        public static void SetLightTextStyle(this UILabel lbLabel)
        {
            lbLabel.Lines = 0;
            lbLabel.LineBreakMode = UILineBreakMode.WordWrap;
            lbLabel.Font = UIFont.FromName("NJFont-Book", UiConstants.DefaultTextFontSize);
            lbLabel.TextColor = UiConstants.LightTextColor;
        }

        public static void SetDefaultBlueButtonStyle(this UIButton button)
        {
            button.Font = UIFont.FromName("NJFont-Book", UiConstants.ButtonTitleFont);
            button.SetTitleColor(UiConstants.DefaultBlueButtonTextColor, UIControlState.Normal);
            button.BackgroundColor = UiConstants.DefaultBlueButtonColor;
            button.Layer.CornerRadius = 4f;
            button.ClipsToBounds = true;
        }

        public static void SetLeftIconAndTitleInCenterStyle(this UIButton button)
        {
            button.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            button.ImageEdgeInsets = new UIEdgeInsets(8, 8, 8, 8);
            button.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            var availableSpace = new UIEdgeInsets().InsetRect(button.Bounds);

            var availableWidth = availableSpace.Width - (2 * button.ImageEdgeInsets.Left) - (3 * button.ImageEdgeInsets.Right) - (nfloat)button.ImageView?.Frame.Width -
                                 button.TitleLabel?.Frame.Width ?? 0;
            button.TitleEdgeInsets = new UIEdgeInsets(0, availableWidth / 2, 0, 0);
        }

        public static void SetGrayButtonStyle(this UIButton button, UIColor titleColor, bool leftIconAvailable = false)
        {
            button.Font = UIFont.FromName("NJFont-Book", UiConstants.ButtonTitleFont);
            button.SetTitleColor(titleColor, UIControlState.Normal);
            button.BackgroundColor = UiConstants.GrayButtonColor;
            SetCardViewStyle(button);
            if (leftIconAvailable)
            {
                button.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
                button.ImageEdgeInsets = new UIEdgeInsets(8, 8, 8, 8);
                button.TitleEdgeInsets = new UIEdgeInsets(0, 16, 0, 0);
            }
        }

        public static void SetCardViewStyle(this UIView view)
        {
            view.Layer.CornerRadius = 4f;
            view.Layer.ShadowColor = UiConstants.GrayButtonColor.CGColor;
            view.Layer.CornerRadius = 4f;
            view.Layer.BorderColor = UIColor.Gray.CGColor;
            view.Layer.BorderWidth = 0.4f;
            view.Layer.ShadowOpacity = 0.5f;
            view.Layer.ShadowRadius = 5.0f;
            view.Layer.ShadowOffset = new CGSize(0, 3);
        }

        public static void SetBlueBorderViewStyle(this UIView view)
        {
            view.Layer.CornerRadius = 4f;
            view.Layer.ShadowColor = UiConstants.BlueTextColor.CGColor;
            view.Layer.CornerRadius = 4f;
            view.Layer.BorderColor = UiConstants.BlueTextColor.CGColor;
            view.Layer.BorderWidth = 0.4f;
            view.Layer.ShadowOpacity = 0.5f;
            view.Layer.ShadowRadius = 5.0f;
            view.Layer.ShadowOffset = new CGSize(0, 3);
        }

        public static void SetCardBorderStyle(this UIView view)
        {
            view.Layer.CornerRadius = 4f;
            view.Layer.ShadowColor = UiConstants.CardInputBorderColor.CGColor;
            view.Layer.CornerRadius = 4f;
            view.Layer.BorderColor = UiConstants.CardInputBorderColor.CGColor;
            view.Layer.BorderWidth = 0.4f;
            view.Layer.ShadowOpacity = 0.5f;
            view.Layer.ShadowRadius = 5.0f;
            view.Layer.ShadowOffset = new CGSize(0, 3);
        }

        public static void SetStatusCardViewStyleGreen(this UIView view)
        {
            view.BackgroundColor = UiConstants.StatusBackGroundColorGreen;
            view.Layer.CornerRadius = 4f;
            view.Layer.BorderColor = UiConstants.StatusBorderColorGreen.CGColor;
            view.Layer.BorderWidth = 1.0f;
        }

        public static void SetStatusCardViewStyleRed(this UIView view)
        {
            view.BackgroundColor = UiConstants.StatusBackGroundColorRed;
            view.Layer.CornerRadius = 4f;
            view.Layer.BorderColor = UiConstants.StatusBorderColorRed.CGColor;
            view.Layer.BorderWidth = 1.0f;
        }

        public static void SetTextFieldStyle(this UITextField txtField, float fontSize = 14)
        {
            txtField.Font = UIFont.FromName("NJFont-Book", fontSize);
            txtField.TextColor = UiConstants.TextFieldTextColor;
            txtField.SetTextStartLeftPaddign();
            SetCardViewStyle(txtField);
        }

        public static void SetBlueTextFieldStyle(this UITextField txtField, float fontSize = 14)
        {
            txtField.Font = UIFont.FromName("NJFont-Book", fontSize);
            txtField.BackgroundColor = UiConstants.BlueTextFieldBackGround;
            txtField.TextColor = UiConstants.BlueTextFieldTextColor;
            txtField.SetTextStartLeftPaddign();
            SetCardViewStyle(txtField);
        }

        public static void SetWhiteTextFieldStyle(this UITextField txtField, string placeHolderText = null)
        {
            float fontSize = 13;
            txtField.Font = UIFont.FromName("NJFont-Light", fontSize);
            if (!string.IsNullOrEmpty(placeHolderText))
            {
                txtField.AttributedPlaceholder = new Foundation.NSAttributedString(
                placeHolderText,
                font: UIFont.FromName("NJFont-Light", fontSize),
                foregroundColor: UiConstants.CardInputColor);
            }

            txtField.BackgroundColor = UiConstants.BlueTextFieldBackGround;
            txtField.TextColor = UiConstants.CardInputColor;
            txtField.SetTextStartLeftPaddignSmall();
            SetCardBorderStyle(txtField);
        }

        public static void SetBook(this UILabel lbLabel, bool isBlue = false, float fontSize = 16)
        {
            lbLabel.Font = UIFont.FromName("NJFont-Book", fontSize);
            if (isBlue)
            {
                lbLabel.TextColor = AppDelegate.Current.PrimaryBlue;
            }
        }

        public static void SetBook(this UITextField txtField, float fontSize = 16)
        {
            txtField.Font = UIFont.FromName("NJFont-Book", fontSize);
        }

        public static void SetBook(this UITextView txtView, float fontSize = 16)
        {
            txtView.Font = UIFont.FromName("NJFont-Book", fontSize);
        }

        public static void SetTextStartLeftPaddign(this UITextField textField)
        {
            textField.LeftViewMode = UITextFieldViewMode.Always;
            textField.LeftView = new UIView(new CGRect(0, 0, 4, 0));
        }

        public static void SetTextStartLeftPaddignSmall(this UITextField textField)
        {
            textField.LeftViewMode = UITextFieldViewMode.Always;
            textField.LeftView = new UIView(new CGRect(0, 0, 2, 0));
        }

        public static void SetRightIcon(this UITextField textField, string icon, Action clickAction = null)
        {
            textField.RightViewMode = UITextFieldViewMode.Always;
            var btnCvvHint = new UIButton(new CGRect(0, 0, 28, 28));
            btnCvvHint.SetBackgroundImage(UIImage.FromBundle(icon), UIControlState.Normal);
            btnCvvHint.TouchUpInside += (s, e) => clickAction?.Invoke();
            textField.RightView = btnCvvHint;
        }

        public static void SetBook(this UIButton btnButton, bool makeBlue = false, float fontSize = 16)
        {
            btnButton.Font = UIFont.FromName("NJFont-Book", fontSize);
            if (makeBlue)
            {
                btnButton.SetTitleColor(AppDelegate.Current.PrimaryBlue, UIControlState.Normal);
            }
            else
            {
                btnButton.SetTitleColor(UIColor.Black, UIControlState.Normal);
            }

            btnButton.BackgroundColor = AppDelegate.Current.LightGrey;
        }

        public static void SetMedium(this UILabel lbLabel, bool makeBlue = false, float fontSize = 16)
        {
            lbLabel.Font = UIFont.FromName("NJFont-Medium", fontSize);
            if (makeBlue)
            {
                lbLabel.TextColor = AppDelegate.Current.PrimaryBlue;
            }

            lbLabel.AdjustsFontSizeToFitWidth = true;
        }

        public static void SetMediumBold(this UILabel lbLabel, bool makeBlue = false, float fontSize = 16)
        {
            lbLabel.Font = UIFont.FromName("NJFont-BookBold", fontSize);
            if (makeBlue)
            {
                lbLabel.TextColor = AppDelegate.Current.PrimaryBlue;
            }

            lbLabel.AdjustsFontSizeToFitWidth = true;
        }

        public static UIFont GetDefaultFont(float fontSize = 16, bool isBold = false)
        {
            return UIFont.FromName(isBold ? "NJFont-BookBold" : "NJFont-Book", fontSize);
        }

        public static UIView GetBottomBorderView(nfloat width, nfloat height)
        {
            return new UIView(new CGRect(0, height - UiConstants.ButtonBottomBarHeight, width, UiConstants.ButtonBottomBarHeight))
            {
                BackgroundColor = UiConstants.BottomButtomBorderColor
            };
        }

        public static void ShowView(UIView v, float height)
        {
            var c = GetHeightConstraint(v);
            if (c != null)
            {
                c.Constant = height;
                v.LayoutIfNeeded();
            }
        }

        public static void HideView(UIView v)
        {
            var c = GetHeightConstraint(v);
            if (c != null)
            {
                c.Constant = 0;
                v.LayoutIfNeeded();
            }
        }

        private static NSLayoutConstraint GetHeightConstraint(UIView v)
        {
            return v.Constraints.FirstOrDefault(c => c.FirstAttribute == NSLayoutAttribute.Height);
        }
    }
}