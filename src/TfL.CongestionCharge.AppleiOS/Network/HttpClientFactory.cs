using System.Net.Http;
using Tfl.CongestionCharge.Core.Environment;

namespace TfL.CongestionCharge.AppleiOS.Network
{
    public class HttpClientFactory : IHttpClientFactory
    {
        public HttpClient GetClient(IAuthService authService)
        {
            return new HttpClient(new IOsAuthenticatedHttpClientHandler(authService));
        }
    }
}