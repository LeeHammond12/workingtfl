﻿using System;
using CoreGraphics;
using Foundation;
using TelerikUI;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Helpers
{
    public sealed class AlertCustomContentView : UIView
    {
        public TKCalendar CalendarView;
        public DateTime SelectedDate;
        public bool IsDateChanged;

        public AlertCustomContentView(CGRect frame) : base(frame)
        {
            var width = frame.Size.Width;
            var height = frame.Size.Height;

            CalendarView = new TKCalendar(new CGRect(15, 5, width - 30, height - 44))
            {
                BackgroundColor = UIColor.White,
                Delegate = new CalendarDelegate(this)
            };
            AddSubview(CalendarView);

            CalendarView.TintColor = UIColor.White;
            var presenter = (TKCalendarMonthPresenter)CalendarView.Presenter;
            presenter.Style.BackgroundColor = UIColor.White;
            CalendarView.SelectedDate = new NSDate();
        }
    }
}
