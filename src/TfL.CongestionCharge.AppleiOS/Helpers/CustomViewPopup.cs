﻿using CoreGraphics;
using Foundation;
using ObjCRuntime;
using System;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Payments;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class CustomViewPopup : UIView
    {
        private static Action HidePopup;
        private static SelectPaymentCardViewModel ViewModel;
        private static PaymentCardViewModel Card;

        public CustomViewPopup(IntPtr handle) : base(handle)
        {
        }

        public static CustomViewPopup Create(Action hidePopup, PaymentCardViewModel card, SelectPaymentCardViewModel viewModel)
        {
            HidePopup = hidePopup;
            Card = card;
            ViewModel = viewModel;
            var arr = NSBundle.MainBundle.LoadNib("CustomViewPopup", null, null);
            var v = Runtime.GetNSObject<CustomViewPopup>(arr.ValueAt(0));
            v.BackgroundColor = UiConstants.ToUiColor("#000000", 100);
            return v;
        }

        public override void AwakeFromNib()
        {
            new KeyboardHelper(this, true, UIInterfaceOrientation.Portrait);
            ButtonMakePayment.BackgroundColor = UiConstants.PageHeaderLabelColor;
            SwitchSendReceipt.ConfirmSwitch.Transform = CGAffineTransform.MakeScale(0.75f, 0.75f);
            SetStyle();
            BindEvents();
            BindData();
            HideShowCvvHintView(true);
            HideShowMobileNumber(true);
        }

        private void BindEvents()
        {
            ButtonClosePopup.TouchUpInside += (s, e) =>
            {
                HidePopup.Invoke();
            };

            SwitchSendReceipt.LabelConfirmationDescription.Text = Strings.SendMyReceiptViaSms;

            SwitchSendReceipt.ConfirmSwitch.ValueChanged += (sender, args) =>
            {
                ViewModel.IsSmsTextVisible = SwitchSendReceipt.ConfirmSwitch.On;
                HideShowMobileNumber(!ViewModel.IsSmsTextVisible);
            };

            ButtonShowCvvHint.TouchUpInside += (s, e) => HideShowCvvHintView(!ViewContainerCvvHit.Hidden);
            ButtonMakePayment.TouchUpInside += async (s, e) =>
            {
                ViewModel.Cvv2 = TextFieldSecurityCode.Text;
                ViewModel.SmsNumber = TextFieldMobileNumber.Text;
                await ViewModel.PayWithCard(Card);
            };
        }

        private void BindData()
        {
            LabelCardType.Text = Card.IssuerWithCardPostfix;
            LabelCardNumber.Text = Card.CardNumber;
            LabelExpiryDate.Text = Card.ExpiryDateWithSeparator;
            ImageViewCardTypeIcon.ContentMode = UIViewContentMode.ScaleAspectFit;
            ImageViewCardTypeIcon.Image = PaymentCardTypeImageUtils.GetImageFromCardName(Card.Issuer);
            ViewContainerSendReceipt.Hidden = ViewModel.IsPcn;
            ViewContainerSendReceiptHeight.Constant = ViewModel.IsPcn ? 0 : 100;
            TextFieldMobileNumber.Text = ViewModel.SmsNumber;
            ButtonMakePayment.SetTitle(ViewModel.PaymentButtonText, UIControlState.Normal);
        }

        private void HideShowCvvHintView(bool hide)
        {
            ViewContainerCvvHit.Hidden = hide;
            ViewContainerCvvHintHeight.Constant = hide ? 0 : 114;
            ScrollView.ContentSize = new CGSize(ScrollView.Frame.Width, ViewContainer.Frame.Height + (hide ? -114 : 114));
            ButtonShowCvvHint.SetBackgroundImage(UIImage.FromBundle(hide ? "help" : "remove"), UIControlState.Normal);
        }

        private void HideShowMobileNumber(bool hide)
        {
            TextFieldMobileNumber.Hidden = hide;
            TextFieldMobileNumberHeight.Constant = hide ? 0 : 44;
            TextFieldMobileNumberBottom.Constant = hide ? 0 : 8;
            ViewContainerSendReceiptHeight.Constant = hide ? 48 : 100;
            ScrollView.ContentSize = new CGSize(ScrollView.Frame.Width, ViewContainer.Frame.Height + (hide ? -104 : 104));
        }

        private void SetStyle()
        {
            ViewContainerSecurityCode.BackgroundColor = UiConstants.CardviewLightGreyBackground;
            ViewContainerSendReceipt.BackgroundColor = UiConstants.CardviewLightGreyBackground;
            ViewContainerSendReceipt.Layer.BorderWidth = 0.4f;
            ViewContainerSendReceipt.Layer.BorderColor = UiConstants.TextFieldBorderColor.CGColor;
            ViewContainer.BackgroundColor = UiConstants.CardviewLightGreyBackground;
            SwitchSendReceipt.BackgroundColor = UiConstants.CardviewLightGreyBackground;
            ViewContainerCvvHit.BackgroundColor = UIColor.White;
            ViewContainerCvvHit.Layer.BorderWidth = 0.4f;
            ViewContainerCvvHit.Layer.BorderColor = UiConstants.TextFieldBorderColor.CGColor;
            ViewContainerCardDetails.BackgroundColor = UiConstants.LightBlueColor;
            LabelCardType.SetLargeWhiteTextStyle();
            LabelNumber.SetNormalWhiteTextStyle();
            LabelCardNumber.SetNormalWhiteTextStyle();
            LabelExpiry.SetNormalWhiteTextStyle();
            LabelExpiryDate.SetNormalWhiteTextStyle();
            LabelEnterSecurityCode.SetNormalBlueTextStyle();
            LabelCvvHint.SetSmallLightTextStyle();
        }
    }

}