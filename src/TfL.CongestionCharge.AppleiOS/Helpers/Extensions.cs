﻿using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Helpers
{
    public static class Extensions
    {
        public static void HidekeyBoardOnUnFocus(this UITextField control)
        {
            control.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };
        }
    }
}