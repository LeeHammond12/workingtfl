﻿using System;
using UIKit;
using CoreGraphics;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Payments;

namespace TfL.CongestionCharge.AppleiOS.Helpers
{
    public sealed class SavedCardPaymentPopup : UIView
    {
        public SavedCardPaymentPopup(CGRect frame, Action hide, PaymentCardViewModel card, SelectPaymentCardViewModel viewModel) : base(frame)
        {
            BackgroundColor = UiConstants.ToUiColor("#000000", 100);
            var controls = CustomViewPopup.Create(hide, card, viewModel);
            controls.Frame = frame;
            AddSubview(controls);
        }

        public void Hide()
        {
            Animate(
                0.5,
                () => { Alpha = 0; },
                RemoveFromSuperview);
        }
    }
}