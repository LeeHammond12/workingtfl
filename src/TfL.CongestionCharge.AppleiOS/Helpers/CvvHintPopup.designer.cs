// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("CvvHintPopup")]
    partial class CvvHintPopup
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonClose { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelCvvHint { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonClose != null) {
                ButtonClose.Dispose ();
                ButtonClose = null;
            }

            if (LabelCvvHint != null) {
                LabelCvvHint.Dispose ();
                LabelCvvHint = null;
            }
        }
    }
}