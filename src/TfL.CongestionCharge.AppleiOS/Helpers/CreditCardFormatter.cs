﻿using System;
using System.Linq;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Helpers
{
    public class CreditCardFormatter : NSObject
    {
        private static string MLastInput;

        public static void FormatToCreditCardNumber(bool isAmex, UITextField textField, string previousTextContent, UITextRange previousCursorSelection)
        {
            var selectedRangeStart = textField.EndOfDocument;

            if (textField.SelectedTextRange?.Start != null)
            {
                selectedRangeStart = textField.SelectedTextRange?.Start;
            }

            if (!string.IsNullOrEmpty(textField.Text))
            {
                var targetCursorPosition = textField.GetOffsetFromPosition(textField.BeginningOfDocument, selectedRangeStart);
                var cardNumberWithoutSpaces = RemoveNonDigitsFromString(textField.Text, targetCursorPosition);

                var cardNumberWithSpaces = isAmex ?
                    InsertSpacesInAmexFormat(cardNumberWithoutSpaces, targetCursorPosition)
                    : InsertSpacesIntoEvery4DigitsIntoString(cardNumberWithoutSpaces, targetCursorPosition);

                textField.Text = cardNumberWithSpaces;
                var finalCursorPosition = textField.GetPosition(textField.BeginningOfDocument, textField.Text.Length);
                textField.SelectedTextRange = textField.GetTextRange(finalCursorPosition, finalCursorPosition);
            }
        }

        public static string RemoveNonDigitsFromString(string s, nint cursorPosition)
        {
            var digitsOnlyString = string.Empty;
            var chars = s.ToCharArray();
            foreach (var charToAdd in chars)
            {
                if (IsDigit(charToAdd))
                {
                    digitsOnlyString += charToAdd;
                }
            }

            return digitsOnlyString;
        }

        public static string InsertSpacesInAmexFormat(string s, nint cursorPosition)
        {
            var stringWithAddedSpaces = string.Empty;
            for (var i = 0; i < s.Length; i++)
            {
                if (i == 3)
                {
                    stringWithAddedSpaces += " ";
                }

                if (i == 10)
                {
                    stringWithAddedSpaces += " ";
                }

                if (i < 15)
                {
                    var charToAdd = s.ToCharArray()[i];
                    stringWithAddedSpaces += charToAdd;
                }
            }

            return stringWithAddedSpaces;
        }

        public static string InsertSpacesIntoEvery4DigitsIntoString(string s, nint cursorPosition)
        {
            var stringWithAddedSpaces = string.Empty;

            for (var index = 0; index < s.Length; index++)
            {
                if (index != 0 && index % 4 == 0)
                {
                    if (index != 16)
                    {
                        stringWithAddedSpaces += " ";
                        stringWithAddedSpaces += s.ElementAt(index);
                    }
                }
                else if (index < 16)
                {
                    var characterToAdd = s.ToCharArray()[index];
                    stringWithAddedSpaces += characterToAdd;
                }
            }

            return stringWithAddedSpaces;
        }

        public static void FormatCreditCardDate(UITextField textField)
        {
            var str = textField.Text.TrimNonDigitCharacters();
            var month = str.GetNumericForm();

            if (str.Length == 2 && !MLastInput.EndsWith("/"))
            {
                if (month <= 12)
                {
                    textField.Text = textField.Text.Contains("/") ? textField.Text : textField.Text + "/";
                }
                else if (month > 12)
                {
                    textField.Text = textField.Text.TrimLastCharacter();
                }
            }
            else if (str.Length == 2 && MLastInput.EndsWith("/"))
            {
                textField.Text = month <= 12 ? textField.Text.Substring(0, 1) : string.Empty;
            }
            else if (str.Length == 1)
            {
                if (month > 1)
                {
                    textField.Text = "0" + textField.Text + "/";
                }
            }
            else if (str.Length > 4)
            {
                textField.Text = MLastInput;
            }

            var finalCursorPosition =
                textField.GetPosition(textField.BeginningOfDocument, textField.Text.Length);
            textField.SelectedTextRange = textField.GetTextRange(finalCursorPosition, finalCursorPosition);

            MLastInput = textField.Text;
        }

        private static bool IsDigit(char c)
        {
            return c.ToString().ContainsOnlyDigit();
        }
    }
}