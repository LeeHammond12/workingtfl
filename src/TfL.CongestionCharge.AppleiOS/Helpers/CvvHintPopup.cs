﻿using Foundation;
using System;
using ObjCRuntime;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class CvvHintPopup : UIView
    {
        private static Action Hide;

        public CvvHintPopup(IntPtr handle) : base(handle)
        {
        }

        public static CvvHintPopup Create(Action hide)
        {
            Hide = hide;
            var arr = NSBundle.MainBundle.LoadNib("CvvHintPopup", null, null);
            var v = Runtime.GetNSObject<CvvHintPopup>(arr.ValueAt(0));
            v.BackgroundColor = UiConstants.ToUiColor("#000000", 100);
            return v;
        }

        public override void AwakeFromNib()
        {
            ButtonClose.TouchUpInside += (s, e) =>
            {
                Hide.Invoke();
            };
        }
    }
}