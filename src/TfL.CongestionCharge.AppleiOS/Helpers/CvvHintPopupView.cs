﻿using System;
using CoreGraphics;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Helpers
{
    public sealed class CvvHintPopupView : UIView
    {
        public CvvHintPopupView(CGRect frame, Action hide) : base(frame)
        {
            BackgroundColor = UiConstants.ToUiColor("#000000", 100);
            var controls = CvvHintPopup.Create(hide);
            controls.Frame = frame;
            controls.Center = Center;
            AddSubview(controls);
        }

        public void Hide()
        {
            Animate(
                0.5,
                () => { Alpha = 0; },
                RemoveFromSuperview);
        }
    }
}