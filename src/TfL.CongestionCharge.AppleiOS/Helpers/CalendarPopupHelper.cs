﻿using System;
using System.Threading.Tasks;
using CoreGraphics;
using TelerikUI;
using TfL.CongestionCharge.AppleiOS.CustomLabel;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Helpers
{
    public class CalendarPopupHelper
    {
        public AlertCustomContentView CustomCalendar;
        private TKAlert _alert;

        public void InitPopupCalendar(Func<Task<bool>> buttonClick)
        {
            _alert = new TKAlert
            {
                Style =
                {
                    HeaderHeight = 0, BackgroundColor = UIColor.White
                },
                TintColor = UIColor.White,
                CustomFrame = new CGRect(30, 0, UIScreen.MainScreen.Bounds.Width * 3 / 4, (UIScreen.MainScreen.Bounds.Height * 1 / 2) + 54)
            };

            CustomCalendar = new AlertCustomContentView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width * 3 / 4, UIScreen.MainScreen.Bounds.Height * 1 / 2));

            _alert.ContentView.AddSubview(CustomCalendar);

            _alert.AllowParallaxEffect = true;
            _alert.Style.CenterFrame = true;

            _alert.Style.ShowAnimation = TKAlertAnimation.Scale;
            _alert.Style.DismissAnimation = TKAlertAnimation.Scale;

            _alert.Style.BackgroundDimAlpha = 0.5f;
            _alert.Style.BackgroundTintColor = UIColor.LightGray;

            _alert.AnimationDuration = 0.5f;

            var buttonView = new UIView()
            {
                Frame = new CGRect(
                    10,
                    (UIScreen.MainScreen.Bounds.Height * 1 / 2) - 44,
                    (UIScreen.MainScreen.Bounds.Width * 3 / 4) - 20,
                    44),
            };

            var doneButton = Get(5,
                (buttonView.Frame.Width / 2) - 8,
                "Done");

            doneButton.TouchUpInside += async (sender, args) =>
            {
                _alert.Dismiss(true);
                await buttonClick();
            };

            var cancelButton = Get((buttonView.Frame.Width - doneButton.Frame.Width) - 5,
                (buttonView.Frame.Width / 2) - 8,
                "Cancel");
            
            cancelButton.TouchUpInside += (sender, args) =>
            {
                _alert.Dismiss(true);
            };

            buttonView.Add(doneButton);
            buttonView.Add(cancelButton);

            _alert.ContentView.AddSubview(buttonView);
        }

        public void ShowCalendarPopup()
        {
            CustomCalendar.IsDateChanged = false;
            _alert?.Show(true);
        }

        private CustomButton Get(nfloat x, nfloat width, string title)
        {
            var button = new CustomButton
            {
                Frame = new CGRect(x, 10, width, 36),
                BackgroundColor = UiConstants.PageHeaderLabelColor,
            };

            button.SetTitle(title, UIControlState.Normal);
            button.SetTitleColor(UIColor.White, UIControlState.Normal);

            return button;
        }
    }
}
