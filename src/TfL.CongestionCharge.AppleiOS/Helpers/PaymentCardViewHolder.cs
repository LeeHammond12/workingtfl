﻿using Tfl.CongestionCharge.Core.ViewModel;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Helpers
{
    public class PaymentCardViewHolder
    {
        public PaymentCardViewHolder(PaymentCardViewModel vm)
        {
            ViewModel = vm;
        }

        public UIView View { get; set; }

        public UISwitch RadioButton { get; set; }

        public PaymentCardViewModel ViewModel { get; }
    }
}
