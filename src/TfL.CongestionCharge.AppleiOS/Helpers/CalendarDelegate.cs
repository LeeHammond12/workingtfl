﻿using System;
using Foundation;
using TelerikUI;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Helpers
{
    public sealed class CalendarDelegate : TKCalendarDelegate
    {
        private readonly AlertCustomContentView _owner;

        public CalendarDelegate(AlertCustomContentView owner)
        {
            _owner = owner;
        }

        public override void UpdateVisualsForCell(TKCalendar calendar, TKCalendarCell cell)
        {
            cell.Style.BottomBorderWidth = 0;
            cell.Style.TopBorderWidth = 0;
            cell.Style.TextFont = UIFont.FromName("NJFont-Book", UiConstants.DefaultTextFontSize);
            cell.Style.ShapeFill = new TKSolidFill(UiConstants.DefaultBlueButtonColor);
            cell.Style.BackgroundColor = UIColor.Clear;

            if (cell.IsKindOfClass(new ObjCRuntime.Class(typeof(TKCalendarDayCell))))
            {
                var dayCell = (TKCalendarDayCell)cell;
                if ((dayCell.State & TKCalendarDayState.Selected) != 0)
                {
                    dayCell.Style.TextColor = UIColor.White;
                    cell.Style.BackgroundColor = UiConstants.DefaultBlueButtonColor;
                }
            }

            if (cell.IsKindOfClass(new ObjCRuntime.Class(typeof(TKCalendarDayNameCell))))
            {
                var dayNameCell = (TKCalendarDayNameCell)cell;
                dayNameCell.Label.Text = dayNameCell.Label.Text.Substring(0, 1);
            }

            if (cell.IsKindOfClass(new ObjCRuntime.Class(typeof(TKCalendarMonthTitleCell))))
            {
                var titleCell = (TKCalendarMonthTitleCell)cell;
                titleCell.NextMonthButton.TintColor = UiConstants.DefaultBlueButtonColor;
                titleCell.PreviousMonthButton.TintColor = UiConstants.DefaultBlueButtonColor;
                titleCell.LayoutMode = TKCalendarMonthTitleCellLayoutMode.MonthWithButtons;
            }
        }

        public override void DidSelectDate(TKCalendar calendar, NSDate date)
        {
            _owner.SelectedDate = ToDateTime(date);
            _owner.IsDateChanged = true;
        }

        public static DateTime ToDateTime(NSDate date)
        {
            DateTime reference = new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var utcDateTime = reference.AddSeconds(date.SecondsSinceReferenceDate);
            var dateTime = utcDateTime.ToLocalTime();
            return dateTime;
        }
    }
}
