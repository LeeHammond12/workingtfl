// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("CustomViewPopup")]
    partial class CustomViewPopup
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonClosePopup { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonMakePayment { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonShowCvvHint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewCardTypeIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewCvvHintIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCardNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCardType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCvvHint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelEnterSecurityCode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelExpiry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelExpiryDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomSwitchAndLabelControl SwitchSendReceipt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomTextField TextFieldMobileNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TextFieldMobileNumberBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TextFieldMobileNumberHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TextFieldMobileNumberTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomTextField TextFieldSecurityCode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainerCardDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ViewContainerCvvHintHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainerCvvHit { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainerSecurityCode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainerSendReceipt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ViewContainerSendReceiptHeight { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonClosePopup != null) {
                ButtonClosePopup.Dispose ();
                ButtonClosePopup = null;
            }

            if (ButtonMakePayment != null) {
                ButtonMakePayment.Dispose ();
                ButtonMakePayment = null;
            }

            if (ButtonShowCvvHint != null) {
                ButtonShowCvvHint.Dispose ();
                ButtonShowCvvHint = null;
            }

            if (ImageViewCardTypeIcon != null) {
                ImageViewCardTypeIcon.Dispose ();
                ImageViewCardTypeIcon = null;
            }

            if (ImageViewCvvHintIcon != null) {
                ImageViewCvvHintIcon.Dispose ();
                ImageViewCvvHintIcon = null;
            }

            if (LabelCardNumber != null) {
                LabelCardNumber.Dispose ();
                LabelCardNumber = null;
            }

            if (LabelCardType != null) {
                LabelCardType.Dispose ();
                LabelCardType = null;
            }

            if (LabelCvvHint != null) {
                LabelCvvHint.Dispose ();
                LabelCvvHint = null;
            }

            if (LabelEnterSecurityCode != null) {
                LabelEnterSecurityCode.Dispose ();
                LabelEnterSecurityCode = null;
            }

            if (LabelExpiry != null) {
                LabelExpiry.Dispose ();
                LabelExpiry = null;
            }

            if (LabelExpiryDate != null) {
                LabelExpiryDate.Dispose ();
                LabelExpiryDate = null;
            }

            if (LabelNumber != null) {
                LabelNumber.Dispose ();
                LabelNumber = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (SwitchSendReceipt != null) {
                SwitchSendReceipt.Dispose ();
                SwitchSendReceipt = null;
            }

            if (TextFieldMobileNumber != null) {
                TextFieldMobileNumber.Dispose ();
                TextFieldMobileNumber = null;
            }

            if (TextFieldMobileNumberBottom != null) {
                TextFieldMobileNumberBottom.Dispose ();
                TextFieldMobileNumberBottom = null;
            }

            if (TextFieldMobileNumberHeight != null) {
                TextFieldMobileNumberHeight.Dispose ();
                TextFieldMobileNumberHeight = null;
            }

            if (TextFieldMobileNumberTop != null) {
                TextFieldMobileNumberTop.Dispose ();
                TextFieldMobileNumberTop = null;
            }

            if (TextFieldSecurityCode != null) {
                TextFieldSecurityCode.Dispose ();
                TextFieldSecurityCode = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }

            if (ViewContainerCardDetails != null) {
                ViewContainerCardDetails.Dispose ();
                ViewContainerCardDetails = null;
            }

            if (ViewContainerCvvHintHeight != null) {
                ViewContainerCvvHintHeight.Dispose ();
                ViewContainerCvvHintHeight = null;
            }

            if (ViewContainerCvvHit != null) {
                ViewContainerCvvHit.Dispose ();
                ViewContainerCvvHit = null;
            }

            if (ViewContainerSecurityCode != null) {
                ViewContainerSecurityCode.Dispose ();
                ViewContainerSecurityCode = null;
            }

            if (ViewContainerSendReceipt != null) {
                ViewContainerSendReceipt.Dispose ();
                ViewContainerSendReceipt = null;
            }

            if (ViewContainerSendReceiptHeight != null) {
                ViewContainerSendReceiptHeight.Dispose ();
                ViewContainerSendReceiptHeight = null;
            }
        }
    }
}