﻿using System.Drawing;
using CoreGraphics;
using Tfl.CongestionCharge.Core.Resources;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public sealed class LoadingOverlay : UIView
    {
        public LoadingOverlay(CGRect frame) : base(frame)
        {
            BackgroundColor = UiConstants.ToUiColor("#000000", 160);
            AutoresizingMask = UIViewAutoresizing.All;

            var centerX = Frame.Width / 2;
            var centerY = Frame.Height / 2;

            var activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
            activitySpinner.Frame = new RectangleF(
                (float)(centerX - (activitySpinner.Frame.Width / 2)),
                (float)centerY,
                (float)activitySpinner.Frame.Width,
                (float)activitySpinner.Frame.Height);
            activitySpinner.AutoresizingMask = UIViewAutoresizing.All;
            AddSubview(activitySpinner);
            activitySpinner.StartAnimating();
        }

        public void FadeOutAndRemoveFromParentView()
        {
            Animate(
                0.5, // duration
                () => { Alpha = 0; },
                RemoveFromSuperview);
        }
    }
}