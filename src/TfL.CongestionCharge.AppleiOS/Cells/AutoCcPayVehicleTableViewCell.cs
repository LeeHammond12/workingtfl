﻿using System;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class AutoCcPayVehicleTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("AutoCcPayVehicleTableViewCell");
        public static readonly UINib Nib;
        public UIButton Button;

        static AutoCcPayVehicleTableViewCell()
        {
            Nib = UINib.FromName("AutoCcPayVehicleTableViewCell", NSBundle.MainBundle);
        }

        protected AutoCcPayVehicleTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string heading, string subHeading, string iconName, bool hideDivider = false)
        {
            Divider.BackgroundColor = UiConstants.DividerColor;
            Divider.Hidden = hideDivider;
            ContentView.BackgroundColor = UiConstants.TableViewCellBackground;
            LabelVehicleRegMark.SetNormalTextStyle();
            LabelVehicleDetails.SetSmallTextStyle();
            LabelVehicleRegMark.Text = heading;
            LabelVehicleDetails.Lines = 1;
            LabelVehicleDetails.LineBreakMode = UILineBreakMode.TailTruncation;
            LabelVehicleDetails.Text = subHeading;
            ButtonDelete.SetBackgroundImage(UIImage.FromBundle(iconName), UIControlState.Normal);
            Button = ButtonDelete;
        }
    }
}
