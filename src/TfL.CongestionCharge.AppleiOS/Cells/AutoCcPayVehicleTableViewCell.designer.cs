// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("AutoCcPayVehicleTableViewCell")]
    partial class AutoCcPayVehicleTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonDelete { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider Divider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewCarIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelVehicleDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelVehicleRegMark { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonDelete != null) {
                ButtonDelete.Dispose ();
                ButtonDelete = null;
            }

            if (Divider != null) {
                Divider.Dispose ();
                Divider = null;
            }

            if (ImageViewCarIcon != null) {
                ImageViewCarIcon.Dispose ();
                ImageViewCarIcon = null;
            }

            if (LabelVehicleDetails != null) {
                LabelVehicleDetails.Dispose ();
                LabelVehicleDetails = null;
            }

            if (LabelVehicleRegMark != null) {
                LabelVehicleRegMark.Dispose ();
                LabelVehicleRegMark = null;
            }
        }
    }
}