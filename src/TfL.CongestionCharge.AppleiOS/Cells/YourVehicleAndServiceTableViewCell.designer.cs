// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("YourVehicleAndServiceTableViewCell")]
    partial class YourVehicleAndServiceTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider divider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelExpires { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelExpiresWidth { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelExpireyDateWidth { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelExpiryDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelExpiryLeft { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelExpirysLeft { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelService { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (divider != null) {
                divider.Dispose ();
                divider = null;
            }

            if (LabelExpires != null) {
                LabelExpires.Dispose ();
                LabelExpires = null;
            }

            if (LabelExpiresWidth != null) {
                LabelExpiresWidth.Dispose ();
                LabelExpiresWidth = null;
            }

            if (LabelExpireyDateWidth != null) {
                LabelExpireyDateWidth.Dispose ();
                LabelExpireyDateWidth = null;
            }

            if (LabelExpiryDate != null) {
                LabelExpiryDate.Dispose ();
                LabelExpiryDate = null;
            }

            if (LabelExpiryLeft != null) {
                LabelExpiryLeft.Dispose ();
                LabelExpiryLeft = null;
            }

            if (LabelExpirysLeft != null) {
                LabelExpirysLeft.Dispose ();
                LabelExpirysLeft = null;
            }

            if (LabelService != null) {
                LabelService.Dispose ();
                LabelService = null;
            }
        }
    }
}