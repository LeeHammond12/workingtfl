// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("AutoCcPayCreditCardTableViewCell")]
    partial class AutoCcPayCreditCardTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewCardTypeIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelCardNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemHeaderLabel LabelCardType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelExpiry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelExpiryDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch SwitchSelectCard { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ImageViewCardTypeIcon != null) {
                ImageViewCardTypeIcon.Dispose ();
                ImageViewCardTypeIcon = null;
            }

            if (LabelCardNumber != null) {
                LabelCardNumber.Dispose ();
                LabelCardNumber = null;
            }

            if (LabelCardType != null) {
                LabelCardType.Dispose ();
                LabelCardType = null;
            }

            if (LabelExpiry != null) {
                LabelExpiry.Dispose ();
                LabelExpiry = null;
            }

            if (LabelExpiryDate != null) {
                LabelExpiryDate.Dispose ();
                LabelExpiryDate = null;
            }

            if (LabelNumber != null) {
                LabelNumber.Dispose ();
                LabelNumber = null;
            }

            if (SwitchSelectCard != null) {
                SwitchSelectCard.Dispose ();
                SwitchSelectCard = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}