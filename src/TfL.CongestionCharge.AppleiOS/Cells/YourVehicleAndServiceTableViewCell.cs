﻿using System;

using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class YourVehicleAndServiceTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("YourVehicleAndServiceTableViewCell");
        public static readonly UINib Nib;

        static YourVehicleAndServiceTableViewCell()
        {
            Nib = UINib.FromName("YourVehicleAndServiceTableViewCell", NSBundle.MainBundle);
        }

        protected YourVehicleAndServiceTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string serviceName, string expiryDate, bool hasExpiryDate, bool hideDivider = false)
        {
            divider.BackgroundColor = UiConstants.DividerColor;
            divider.Hidden = hideDivider;
            ContentView.BackgroundColor = UiConstants.TableViewCellBackground;
            LabelExpires.SetSmallTextBoldStyle();
            LabelService.Text = serviceName;
            LabelExpiryDate.Text = expiryDate;
            LabelExpiryDate.Hidden = !hasExpiryDate;
            LabelExpires.Hidden = !hasExpiryDate;

            if (hasExpiryDate)
            {
                return;
            }
            LabelExpiresWidth.Constant = 0;
            LabelExpireyDateWidth.Constant = 0;
            LabelExpiryLeft.Constant = 0;
            LabelExpirysLeft.Constant = 0;
        }
    }
}
