// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("YourCardTableViewCell")]
    partial class YourCardTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonDelete { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewCardType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelCardNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCardType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelExpiry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelExpiryDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelNo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonDelete != null) {
                ButtonDelete.Dispose ();
                ButtonDelete = null;
            }

            if (ImageViewCardType != null) {
                ImageViewCardType.Dispose ();
                ImageViewCardType = null;
            }

            if (LabelCardNumber != null) {
                LabelCardNumber.Dispose ();
                LabelCardNumber = null;
            }

            if (LabelCardType != null) {
                LabelCardType.Dispose ();
                LabelCardType = null;
            }

            if (LabelExpiry != null) {
                LabelExpiry.Dispose ();
                LabelExpiry = null;
            }

            if (LabelExpiryDate != null) {
                LabelExpiryDate.Dispose ();
                LabelExpiryDate = null;
            }

            if (LabelNo != null) {
                LabelNo.Dispose ();
                LabelNo = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}