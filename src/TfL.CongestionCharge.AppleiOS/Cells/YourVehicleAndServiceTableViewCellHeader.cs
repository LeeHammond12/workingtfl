﻿using System;

using Foundation;
using Tfl.CongestionCharge.Core.Resources;
using UIKit;
using TfL.CongestionCharge.AppleiOS.Utilities;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class YourVehicleAndServiceTableViewCellHeader : UITableViewCell
    {
        public static readonly NSString Key = new NSString("YourVehicleAndServiceTableViewCellHeader");
        public static readonly UINib Nib;
        public UIButton DeleteButton;

        static YourVehicleAndServiceTableViewCellHeader()
        {
            Nib = UINib.FromName("YourVehicleAndServiceTableViewCellHeader", NSBundle.MainBundle);
        }

        protected YourVehicleAndServiceTableViewCellHeader(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string vehicleRegNumber, string vrmColorMakeModel)
        {
            DeleteButton = ButtonDelete;
            ButtonDelete.Hidden = true;
            ButtonDeleteLeft.Constant = 0;
            ButtonDeleteWidth.Constant = 0;
            LabelVrmMark.SetBoldWhiteTextStyle();
            LabelVehicleDetails.SetNormalWhiteTextStyle();
            LabelVehicleDetails.Lines = 1;
            LabelVehicleDetails.LineBreakMode = UILineBreakMode.TailTruncation;

            if (string.IsNullOrWhiteSpace(vehicleRegNumber))
            {
                LabelVrmMark.Text = Strings.YourServices;
                ImageViewVehicleIconRight.Constant = 4;
                ImageViewVehicleIconWidth.Constant = 0;
                ImageViewVehicleIcon.Hidden = true;
                //ButtonDelete.Hidden = true;
                LabelVehicleDetails.Hidden = true;
            }
            else
            {
                LabelVrmMark.Text = vehicleRegNumber;
                //ButtonDelete.Hidden = true;
                LabelVehicleDetails.Text = vrmColorMakeModel;
            }
        }
    }
}
