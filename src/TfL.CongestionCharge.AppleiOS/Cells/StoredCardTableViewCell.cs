﻿using System;

using Foundation;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class StoredCardTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("StoredCardTableViewCell");
        public static readonly UINib Nib;

        static StoredCardTableViewCell()
        {
            Nib = UINib.FromName("StoredCardTableViewCell", NSBundle.MainBundle);
        }

        protected StoredCardTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string cardType, string cardNumber, string expiry, string cardTypeIconName)
        {
            ViewContainer.BackgroundColor = UiConstants.TableViewCellBackground;
            LabelCardType.Text = cardType;
            LabelCardNumber.Text = cardNumber;
            LabelExpiryDate.Text = expiry;
            ImageViewCardType.ContentMode = UIViewContentMode.ScaleAspectFit;
            ImageViewCardType.Image = PaymentCardTypeImageUtils.GetImageFromCardName(cardTypeIconName);
        }
    }
}
