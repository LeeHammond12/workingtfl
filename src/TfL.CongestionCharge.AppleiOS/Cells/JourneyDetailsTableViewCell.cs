﻿using System;
using Foundation;
using Tfl.CongestionCharge.Core.Resources;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class JourneyDetailsTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("JourneyDetailsTableViewCell");
        public static readonly UINib Nib;

        static JourneyDetailsTableViewCell()
        {
            Nib = UINib.FromName("JourneyDetailsTableViewCell", NSBundle.MainBundle);
        }

        protected JourneyDetailsTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string journeyDate, string amount, string consecutiveDays, Action deleteJourney, bool isHeadingHide, bool hideDivider = false)
        {
            Divider.Hidden = hideDivider;
            Divider.BackgroundColor = UiConstants.DividerColor;
            ContentView.BackgroundColor = UiConstants.TableViewCellBackground;
            ViewContainer.BackgroundColor = UiConstants.TableViewCellBackground;
            LabelJourneyDate.Text = journeyDate;
            LabelPayableAmount.Text = amount;
            LabelConsecutiveDays.Hidden = string.IsNullOrEmpty(consecutiveDays);
            LabelConsecutiveDays.Text = string.IsNullOrEmpty(consecutiveDays) ? string.Empty : consecutiveDays;
            ButtonDelete.TouchUpInside += (s, e) => deleteJourney.Invoke();

            LabelHeadingAmount.SetNormalTextBoldStyle();
            LabelHeadingJourneyDates.SetNormalTextBoldStyle();
            LabelHeadingAmount.Hidden = isHeadingHide;
            LabelHeadingJourneyDates.Hidden = isHeadingHide;
            LabelHeadingJourneyDatesBottom.Constant = isHeadingHide ? 0 : 10;
            LabelHeadingAmount.Text = isHeadingHide ? string.Empty : Strings.Amount;
            LabelHeadingJourneyDates.Text = isHeadingHide ? string.Empty : Strings.JourneyDatesNoColon;
        }
    }
}
