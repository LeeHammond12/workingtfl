﻿using System;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class VehicleDetailsTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("VehicleDetailsTableViewCell");
        public static readonly UINib Nib;

        static VehicleDetailsTableViewCell()
        {
            Nib = UINib.FromName("VehicleDetailsTableViewCell", NSBundle.MainBundle);
        }

        protected VehicleDetailsTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string heading, string subHeading, bool hideRightArrow = false)
        {
            LabelListItemSubHeading.SetSmallTextStyle();
            if (hideRightArrow)
            {
                ImageRightArrow.Hidden = true;
                ImageRightArrowWidth.Constant = 0;
            }

            ViewContainer.BackgroundColor = UiConstants.TableViewCellBackground;
            LabelListItemHeading.Text = heading;
            LabelListItemSubHeading.Lines = 1;
            LabelListItemSubHeading.LineBreakMode = UILineBreakMode.TailTruncation;
            LabelListItemSubHeading.Text = subHeading;
        }
    }
}
