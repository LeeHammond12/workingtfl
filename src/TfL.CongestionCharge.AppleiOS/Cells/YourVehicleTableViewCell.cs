﻿using System;

using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class YourVehicleTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("YourVehicleTableViewCell");
        public static readonly UINib Nib;
        public UIButton DeleteButton;

        static YourVehicleTableViewCell()
        {
            Nib = UINib.FromName("YourVehicleTableViewCell", NSBundle.MainBundle);
        }

        protected YourVehicleTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string heading, string subHeading)
        {
            DeleteButton = ButtonDelete;
            ViewContainer.BackgroundColor = UiConstants.TableViewCellBackground;
            LabelListItemHeading.Text = heading;
            LabelListItemSubHeading.SetSmallTextStyle();
            LabelListItemSubHeading.Lines = 1;
            LabelListItemSubHeading.LineBreakMode = UILineBreakMode.TailTruncation;
            LabelListItemSubHeading.Text = subHeading;
        }
    }
}
