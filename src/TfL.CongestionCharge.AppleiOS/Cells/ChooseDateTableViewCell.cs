﻿using System;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class ChooseDateTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("ChooseDateTableViewCell");
        public static readonly UINib Nib;

        static ChooseDateTableViewCell()
        {
            Nib = UINib.FromName("ChooseDateTableViewCell", NSBundle.MainBundle);
        }

        protected ChooseDateTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string amount, string heading, string subHeading, string payingFor, bool isShow)
        {
            ViewContainer.BackgroundColor = UiConstants.TableViewCellBackground;
            LabelAmount.SetNormalWhiteTextStyle();
            LabelAmount.BackgroundColor = UiConstants.BlueTextColor;
            LabelAmount.Text = amount;
            ListItemLabelPayFor.SetLargeBlueTextStyle();
            ListItemLabelPayFor.Text = heading;
            ListItemLabelPayForDate.Text = subHeading;
            ListItemLabelPayForHowManyDays.Text = payingFor;

            if (string.IsNullOrEmpty(payingFor))
            {
                ListItemLabelPayForTop.Constant = 14;
            }
            else
            {
                ListItemLabelPayForTop.Constant = 6;
            }
        }
    }
}
