﻿using System;

using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class JourneyDetailsTableViewHeaderCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("JourneyDetailsTableViewHeaderCell");
        public static readonly UINib Nib;

        static JourneyDetailsTableViewHeaderCell()
        {
            Nib = UINib.FromName("JourneyDetailsTableViewHeaderCell", NSBundle.MainBundle);
        }

        protected JourneyDetailsTableViewHeaderCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string vrmRegMark, string vehicleDetails, Action addDate)
        {
            ViewContainerAddDate.BackgroundColor = UiConstants.PageHeaderLabelColor;
            LabelVehicleRegMark.SetBoldWhiteTextStyle();
            LabelVehicleDetails.SetNormalWhiteTextStyle();
            LabelVehicleDetails.Lines = 1;
            LabelVehicleDetails.LineBreakMode = UILineBreakMode.TailTruncation;
            LabelVehicleRegMark.Text = vrmRegMark;
            LabelVehicleDetails.Text = vehicleDetails;
            ButtonAddDate.TouchUpInside -= (s, e) => addDate.Invoke();
            ButtonAddDate.TouchUpInside += (s, e) => addDate.Invoke();
        }
    }
}
