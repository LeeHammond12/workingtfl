﻿using System;

using Foundation;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class YourCardTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("YourCardTableViewCell");
        public static readonly UINib Nib;
        public UIButton DeleteButton;

        static YourCardTableViewCell()
        {
            Nib = UINib.FromName("YourCardTableViewCell", NSBundle.MainBundle);
        }

        protected YourCardTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string cardType, string cardNumber, string expiry, string cardTypeIconName)
        {
            LabelNo.SetSmallTextBoldStyle();
            LabelExpiry.SetSmallTextBoldStyle();
            LabelCardType.SetNormalBlueTextStyle(true);
            ViewContainer.BackgroundColor = UiConstants.TableViewCellBackground;
            DeleteButton = ButtonDelete;
            LabelCardType.Text = cardType;
            LabelCardNumber.Text = cardNumber;
            LabelExpiryDate.Text = expiry;
            ImageViewCardType.ContentMode = UIViewContentMode.ScaleAspectFit;
            ImageViewCardType.Image = PaymentCardTypeImageUtils.GetImageFromCardName(cardTypeIconName);
        }
    }
}
