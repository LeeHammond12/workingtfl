// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("JourneyDetailsTableViewCell")]
    partial class JourneyDetailsTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonDelete { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider Divider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemColoredSubHeaderLabel LabelConsecutiveDays { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelHeadingAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelHeadingJourneyDates { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelHeadingJourneyDatesBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelJourneyDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel LabelPayableAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonDelete != null) {
                ButtonDelete.Dispose ();
                ButtonDelete = null;
            }

            if (Divider != null) {
                Divider.Dispose ();
                Divider = null;
            }

            if (LabelConsecutiveDays != null) {
                LabelConsecutiveDays.Dispose ();
                LabelConsecutiveDays = null;
            }

            if (LabelHeadingAmount != null) {
                LabelHeadingAmount.Dispose ();
                LabelHeadingAmount = null;
            }

            if (LabelHeadingJourneyDates != null) {
                LabelHeadingJourneyDates.Dispose ();
                LabelHeadingJourneyDates = null;
            }

            if (LabelHeadingJourneyDatesBottom != null) {
                LabelHeadingJourneyDatesBottom.Dispose ();
                LabelHeadingJourneyDatesBottom = null;
            }

            if (LabelJourneyDate != null) {
                LabelJourneyDate.Dispose ();
                LabelJourneyDate = null;
            }

            if (LabelPayableAmount != null) {
                LabelPayableAmount.Dispose ();
                LabelPayableAmount = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}