// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("YourVehicleAndServiceTableViewCellHeader")]
    partial class YourVehicleAndServiceTableViewCellHeader
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonDelete { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonDeleteLeft { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonDeleteWidth { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewVehicleIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ImageViewVehicleIconRight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ImageViewVehicleIconWidth { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelVehicleDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelVrmMark { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonDelete != null) {
                ButtonDelete.Dispose ();
                ButtonDelete = null;
            }

            if (ButtonDeleteLeft != null) {
                ButtonDeleteLeft.Dispose ();
                ButtonDeleteLeft = null;
            }

            if (ButtonDeleteWidth != null) {
                ButtonDeleteWidth.Dispose ();
                ButtonDeleteWidth = null;
            }

            if (ImageViewVehicleIcon != null) {
                ImageViewVehicleIcon.Dispose ();
                ImageViewVehicleIcon = null;
            }

            if (ImageViewVehicleIconRight != null) {
                ImageViewVehicleIconRight.Dispose ();
                ImageViewVehicleIconRight = null;
            }

            if (ImageViewVehicleIconWidth != null) {
                ImageViewVehicleIconWidth.Dispose ();
                ImageViewVehicleIconWidth = null;
            }

            if (LabelVehicleDetails != null) {
                LabelVehicleDetails.Dispose ();
                LabelVehicleDetails = null;
            }

            if (LabelVrmMark != null) {
                LabelVrmMark.Dispose ();
                LabelVrmMark = null;
            }
        }
    }
}