// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("ChooseDateTableViewCell")]
    partial class ChooseDateTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageRightArrow { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ListItemLabelPayFor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemSubHeaderLabel ListItemLabelPayForDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ListItemLabelPayForDateBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemColoredSubHeaderLabel ListItemLabelPayForHowManyDays { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ListItemLabelPayForTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ImageRightArrow != null) {
                ImageRightArrow.Dispose ();
                ImageRightArrow = null;
            }

            if (LabelAmount != null) {
                LabelAmount.Dispose ();
                LabelAmount = null;
            }

            if (ListItemLabelPayFor != null) {
                ListItemLabelPayFor.Dispose ();
                ListItemLabelPayFor = null;
            }

            if (ListItemLabelPayForDate != null) {
                ListItemLabelPayForDate.Dispose ();
                ListItemLabelPayForDate = null;
            }

            if (ListItemLabelPayForDateBottom != null) {
                ListItemLabelPayForDateBottom.Dispose ();
                ListItemLabelPayForDateBottom = null;
            }

            if (ListItemLabelPayForHowManyDays != null) {
                ListItemLabelPayForHowManyDays.Dispose ();
                ListItemLabelPayForHowManyDays = null;
            }

            if (ListItemLabelPayForTop != null) {
                ListItemLabelPayForTop.Dispose ();
                ListItemLabelPayForTop = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}