// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("JourneyDetailsTableViewHeaderCell")]
    partial class JourneyDetailsTableViewHeaderCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonAddDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageVehicleIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAddDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelVehicleDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelVehicleRegMark { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainerAddDate { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonAddDate != null) {
                ButtonAddDate.Dispose ();
                ButtonAddDate = null;
            }

            if (ImageVehicleIcon != null) {
                ImageVehicleIcon.Dispose ();
                ImageVehicleIcon = null;
            }

            if (LabelAddDate != null) {
                LabelAddDate.Dispose ();
                LabelAddDate = null;
            }

            if (LabelVehicleDetails != null) {
                LabelVehicleDetails.Dispose ();
                LabelVehicleDetails = null;
            }

            if (LabelVehicleRegMark != null) {
                LabelVehicleRegMark.Dispose ();
                LabelVehicleRegMark = null;
            }

            if (ViewContainerAddDate != null) {
                ViewContainerAddDate.Dispose ();
                ViewContainerAddDate = null;
            }
        }
    }
}