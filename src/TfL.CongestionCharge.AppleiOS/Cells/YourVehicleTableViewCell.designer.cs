// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    [Register ("YourVehicleTableViewCell")]
    partial class YourVehicleTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonDelete { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewVehicleIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemHeaderLabel LabelListItemHeading { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelListItemSubHeading { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonDelete != null) {
                ButtonDelete.Dispose ();
                ButtonDelete = null;
            }

            if (ImageViewVehicleIcon != null) {
                ImageViewVehicleIcon.Dispose ();
                ImageViewVehicleIcon = null;
            }

            if (LabelListItemHeading != null) {
                LabelListItemHeading.Dispose ();
                LabelListItemHeading = null;
            }

            if (LabelListItemSubHeading != null) {
                LabelListItemSubHeading.Dispose ();
                LabelListItemSubHeading = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}