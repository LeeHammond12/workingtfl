﻿using System;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.Cells
{
    public partial class AutoCcPayCreditCardTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("AutoCcPayCreditCardTableViewCell");
        public static readonly UINib Nib;
        public UISwitch SelectCardSwitch;

        static AutoCcPayCreditCardTableViewCell()
        {
            Nib = UINib.FromName("AutoCcPayCreditCardTableViewCell", NSBundle.MainBundle);
        }

        protected AutoCcPayCreditCardTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void UpdateCell(string cardType, string cardNumber, string expiry, string cardTypeIconName, bool isCardSelected)
        {
            ViewContainer.BackgroundColor = UiConstants.TableViewCellBackground;
            SwitchSelectCard.Transform = CGAffineTransform.MakeScale(0.75f, 0.75f);
            LabelCardType.Text = cardType;
            LabelCardNumber.Text = cardNumber;
            LabelExpiryDate.Text = expiry;
            ImageViewCardTypeIcon.ContentMode = UIViewContentMode.ScaleAspectFit;
            ImageViewCardTypeIcon.Image = PaymentCardTypeImageUtils.GetImageFromCardName(cardTypeIconName);
            SwitchSelectCard.On = isCardSelected;
            SwitchSelectCard.UserInteractionEnabled = !isCardSelected;
            SelectCardSwitch = SwitchSelectCard;
        }
    }
}
