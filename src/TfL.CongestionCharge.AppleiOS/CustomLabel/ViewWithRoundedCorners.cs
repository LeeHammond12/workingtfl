﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("ViewWithRoundedCorners"), DesignTimeVisible(true)]
    public class ViewWithRoundedCorners : UIView
    {
        private CGRect _rect;

        public ViewWithRoundedCorners(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public ViewWithRoundedCorners()
        {
            Initialize();
        }

        public ViewWithRoundedCorners(CGRect rect)
        {
            _rect = rect;
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(_rect.IsEmpty ? rect : _rect);

            Layer.MasksToBounds = true;
            this.SetCardViewStyle();
        }

        private void Initialize()
        {
            SetNeedsDisplay();
        }
    }
}
