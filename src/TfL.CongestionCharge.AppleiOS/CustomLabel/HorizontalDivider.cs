﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("HorizontalDivider"), DesignTimeVisible(true)]
    public class HorizontalDivider : UIView
    {
        public HorizontalDivider(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public HorizontalDivider()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            BackgroundColor = UiConstants.DividerColor;
        }

        private void Initialize()
        {
            SetNeedsDisplay();
        }
    }
}