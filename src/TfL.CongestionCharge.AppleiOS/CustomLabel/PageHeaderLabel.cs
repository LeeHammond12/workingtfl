﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("PageHeaderLabel"), DesignTimeVisible(true)]
    public class PageHeaderLabel : UILabel
    {
        public PageHeaderLabel(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public PageHeaderLabel()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            SetStyle();
        }

        private void Initialize()
        {
            SetStyle();
            SetNeedsDisplay();
        }

        private void SetStyle()
        {
            this.SetLargeBlueTextStyle();
        }
    }
}