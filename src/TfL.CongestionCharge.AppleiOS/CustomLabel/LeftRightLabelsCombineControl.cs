﻿using System;
using System.ComponentModel;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("LeftRightLabelsCombineControl"), DesignTimeVisible(true)]
    public class LeftRightLabelsCombineControl : UIView
    {
        public UILabel LeftLabel;
        public UILabel RightLabel;
        public UIView ContainerView;

        public LeftRightLabelsCombineControl(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public LeftRightLabelsCombineControl()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            ContainerView.AddSubviews(LeftLabel, RightLabel);
            AddSubview(ContainerView);

            InitUi();
        }

        private void Initialize()
        {
            ContainerView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 50));

            LeftLabel = new UILabel();
            LeftLabel.SetLightTextStyle();

            RightLabel = new UILabel();
            RightLabel.SetNormalTextStyle();

            SetNeedsDisplay();
        }

        private void InitUi()
        {
            ContainerView.RemoveConstraints(ContainerView.Constraints);
            ContainerView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            this.AddConstraints(
                ContainerView.AtLeftOf(this),
                ContainerView.AtRightOf(this),
                ContainerView.AtTopOf(this),
                ContainerView.AtBottomOf(this),
                LeftLabel.AtLeftOf(ContainerView),
                LeftLabel.AtTopOf(ContainerView, 8),
                LeftLabel.Width().EqualTo(100),
                RightLabel.ToRightOf(LeftLabel, 8),
                RightLabel.WithSameTop(LeftLabel),
                RightLabel.AtRightOf(ContainerView, 8),
                RightLabel.AtBottomOf(ContainerView, 8));
        }
    }
}