﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using UIKit;
using Cirrious.FluentLayouts.Touch;
using TfL.CongestionCharge.AppleiOS.Utilities;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("CustomSwitchAndLabelControl"), DesignTimeVisible(true)]
    public class CustomSwitchAndLabelControl : UIView
    {
        public UISwitch ConfirmSwitch;
        public UILabel LabelConfirmationDescription;
        public UIView ContainerView;

        public CustomSwitchAndLabelControl(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public CustomSwitchAndLabelControl()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            ContainerView.AddSubviews(ConfirmSwitch, LabelConfirmationDescription);
            AddSubview(ContainerView);

            InitUi();
        }

        private void Initialize()
        {
            ContainerView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 50));

            ConfirmSwitch = new UISwitch();
            ConfirmSwitch.SetSwitchStyle();

            LabelConfirmationDescription = new UILabel();
            LabelConfirmationDescription.SetSmallBlueTextStyle();

            SetNeedsDisplay();
        }

        private void InitUi()
        {
            ContainerView.RemoveConstraints(ContainerView.Constraints);
            ContainerView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            this.AddConstraints(
                ConfirmSwitch.AtLeftOf(ContainerView),
                ConfirmSwitch.WithSameTop(ContainerView),
                ConfirmSwitch.Height().EqualTo(40),
                ConfirmSwitch.Width().EqualTo(40),
                LabelConfirmationDescription.ToRightOf(ConfirmSwitch, 10),
                LabelConfirmationDescription.WithSameCenterY(ConfirmSwitch).Minus(4),
                LabelConfirmationDescription.AtRightOf(this, 10));
        }
    }
}