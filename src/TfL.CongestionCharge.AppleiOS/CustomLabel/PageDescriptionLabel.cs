﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("PageDescriptionLabel"), DesignTimeVisible(true)]
    public class PageDescriptionLabel : UILabel
    {
        public PageDescriptionLabel(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public PageDescriptionLabel()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            SetStyle();
        }

        private void Initialize()
        {
            SetStyle();
            SetNeedsDisplay();
        }

        private void SetStyle()
        {
            this.SetNormalTextStyle();
        }
    }
}