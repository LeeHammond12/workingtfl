﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("ListItemHeaderLabel"), DesignTimeVisible(true)]
    public class ListItemHeaderLabel : UILabel
    {
        public ListItemHeaderLabel(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public ListItemHeaderLabel()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            SetStyle();
        }

        private void Initialize()
        {
            SetStyle();
            SetNeedsDisplay();
        }

        private void SetStyle()
        {
            this.SetNormalBlueTextStyle();
        }
    }
}