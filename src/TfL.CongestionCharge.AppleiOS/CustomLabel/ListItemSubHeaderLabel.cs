﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("ListItemSubHeaderLabel"), DesignTimeVisible(true)]
    public class ListItemSubHeaderLabel : UILabel
    {
        public ListItemSubHeaderLabel(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public ListItemSubHeaderLabel()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            SetStyle();
        }

        private void Initialize()
        {
            SetStyle();
            SetNeedsDisplay();
        }

        private void SetStyle()
        {
            this.SetSmallTextStyle();
        }
    }
}