﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("ListItemColoredSubHeaderLabel"), DesignTimeVisible(true)]
    public class ListItemColoredSubHeaderLabel : UILabel
    {
        public ListItemColoredSubHeaderLabel(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public ListItemColoredSubHeaderLabel()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            SetStyle();
        }

        private void Initialize()
        {
            SetStyle();
            SetNeedsDisplay();
        }

        private void SetStyle()
        {
            this.SetSmallBlueTextStyle();
        }
    }
}