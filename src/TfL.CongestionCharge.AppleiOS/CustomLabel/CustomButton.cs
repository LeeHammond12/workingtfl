﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("CustomButton"), DesignTimeVisible(true)]
    public class CustomButton : UIButton
    {
        public CustomButton(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public CustomButton()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            SetStyle();
        }

        private void Initialize()
        {
            SetStyle();
            SetNeedsDisplay();
        }

        private void SetStyle()
        {
            this.SetDefaultBlueButtonStyle();
        }
    }
}