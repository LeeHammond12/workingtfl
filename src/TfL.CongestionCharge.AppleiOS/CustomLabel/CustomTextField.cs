﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.CustomLabel
{
    [Register("CustomTextField"), DesignTimeVisible(true)]
    public class CustomTextField : UITextField
    {
        public CustomTextField(IntPtr p)
            : base(p)
        {
            Initialize();
        }

        public CustomTextField()
        {
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            this.SetTextFieldStyle();
        }

        private void Initialize()
        {
            SetNeedsDisplay();
        }
    }
}