﻿using System;
using System.Drawing;
using CoreGraphics;
using Foundation;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.UIUtils
{
    public sealed class KeyboardHelper
    {
        private readonly UIView _view;
        private readonly bool _isViewLoaded;
        private readonly UIInterfaceOrientation _interfaceOrientation;

        /// <summary>
        /// Set this field to any view inside the scroll view to center this view instead of the current responder
        /// </summary>
        private UIView _viewToCenterOnKeyboardShown;

        public KeyboardHelper(UIView view, bool isViewLoaded, UIInterfaceOrientation interfaceOrientation)
        {
            _view = view;
            _isViewLoaded = isViewLoaded;
            _interfaceOrientation = interfaceOrientation;
            RegisterForKeyboardNotifications();
            DismissKeyboardOnBackgroundTap();
        }

        public static UIKeyboardType GetInputType(string type)
        {
            switch (type)
            {
                case "EML":
                    return UIKeyboardType.EmailAddress;
                case "MBL":
                case "TEL":
                    return UIKeyboardType.PhonePad;
            }
            return UIKeyboardType.Default;
        }

        public bool HandlesKeyboardNotifications()
        {
            return true;
        }

        private void RegisterForKeyboardNotifications()
        {
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, OnKeyboardNotification);
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, OnKeyboardNotification);
        }

        /// <summary>
        /// Gets the UIView that represents the "active" user input control (e.g. textfield, or button under a text field)
        /// </summary>
        /// <returns>
        /// A <see cref="UIView"/>
        /// </returns>
        private UIView KeyboardGetActiveView()
        {
            return _view.FindFirstResponder();
        }

        private void OnKeyboardNotification(NSNotification notification)
        {
            if (!_isViewLoaded) return;

            //Check if the keyboard is becoming visible
            var visible = notification.Name == UIKeyboard.WillShowNotification;

            //Start an animation, using values from the keyboard
            UIView.BeginAnimations("AnimateForKeyboard");
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(UIKeyboard.AnimationDurationFromNotification(notification));
            UIView.SetAnimationCurve((UIViewAnimationCurve)UIKeyboard.AnimationCurveFromNotification(notification));

            //Pass the notification, calculating keyboard height, etc.
            bool landscape = _interfaceOrientation == UIInterfaceOrientation.LandscapeLeft || _interfaceOrientation == UIInterfaceOrientation.LandscapeRight;
            var keyboardFrame = visible
                                    ? UIKeyboard.FrameEndFromNotification(notification)
                                    : UIKeyboard.FrameBeginFromNotification(notification);

            OnKeyboardChanged(visible, landscape ? keyboardFrame.Width : keyboardFrame.Height);

            //Commit the animation
            UIView.CommitAnimations();
        }

        /// <summary>
        /// Override this method to apply custom logic when the keyboard is shown/hidden
        /// </summary>
        /// <param name='visible'>
        ///     If the keyboard is visible
        /// </param>
        /// <param name='keyboardHeight'>
        ///     Calculated height of the keyboard (width not generally needed here)
        /// </param>
        private void OnKeyboardChanged(bool visible, nfloat keyboardHeight)
        {
            var activeView = _viewToCenterOnKeyboardShown ?? KeyboardGetActiveView();
            if (activeView == null)
                return;

            var scrollView = activeView.FindSuperviewOfType(_view, typeof(UIScrollView)) as UIScrollView;
            if (scrollView == null)
                return;

            if (!visible)
                RestoreScrollPosition(scrollView);
            else
                CenterViewInScroll(activeView, scrollView, keyboardHeight);
        }

        private void CenterViewInScroll(UIView viewToCenter, UIScrollView scrollView, nfloat keyboardHeight)
        {
            var contentInsets = new UIEdgeInsets(0.0f, 0.0f, keyboardHeight, 0.0f);
            scrollView.ContentInset = contentInsets;
            scrollView.ScrollIndicatorInsets = contentInsets;

            // Position of the active field relative isnside the scroll view
            CGRect relativeFrame = viewToCenter.Superview.ConvertRectToView(viewToCenter.Frame, scrollView);

            bool landscape = _interfaceOrientation == UIInterfaceOrientation.LandscapeLeft || _interfaceOrientation == UIInterfaceOrientation.LandscapeRight;
            var spaceAboveKeyboard = (landscape ? scrollView.Frame.Width : scrollView.Frame.Height) - keyboardHeight;

            // Move the active field to the center of the available space
            var offset = (float)(relativeFrame.Y - ((spaceAboveKeyboard - viewToCenter.Frame.Height) / 2));
            scrollView.ContentOffset = new PointF(0, offset);
        }

        private void RestoreScrollPosition(UIScrollView scrollView)
        {
            scrollView.ContentInset = UIEdgeInsets.Zero;
            scrollView.ScrollIndicatorInsets = UIEdgeInsets.Zero;
        }

        /// <summary>
        /// Call it to force dismiss keyboard when background is tapped
        /// </summary>
        private void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard
            var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
            tap.AddTarget(() => _view.EndEditing(true));
            _view.AddGestureRecognizer(tap);
        }
    }
}
