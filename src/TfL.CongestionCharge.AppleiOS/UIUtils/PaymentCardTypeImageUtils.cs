﻿using System;
using System.Linq;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.UIUtils
{
    public class PaymentCardTypeImageUtils
    {
        public static UIImage GetImageFromCardName(string cardName)
        {
            try
            {
                var name = cardName.Split('.');
                if (name.Length == 0)
                {
                    return null;
                }

                var card = name.FirstOrDefault()?.ToLower();
                if (card == null)
                {
                    return null;
                }

                card = card.Replace("debit", string.Empty);
                return UIImage.FromBundle(card);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}