﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using TelerikUI;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.UIUtils.Alerts
{
    public class AlertHelper
    {
        public static TKAlert GetFormattedAlert(string message, string title)
        {
            var alertView = new TKAlert
            {
                Title = title,
                Style = { BackgroundStyle = TKAlertBackgroundStyle.Dim, BackgroundColor = UiConstants.NearWhiteBackground }
            };

            var messageView = new UIView { BackgroundColor = UIColor.LightGray };

            var messageTextView = new UITextView(UIScreen.MainScreen.Bounds)
            {
                BackgroundColor = UiConstants.NearWhiteBackground,
                TextAlignment = UITextAlignment.Center,
                TextColor = UIColor.DarkGray,
                Editable = false,
                SelectedRange = new NSRange(0, 0),
                Selectable = true,
                DataDetectorTypes = UIDataDetectorType.Link,
                ScrollEnabled = true,
                TextContainerInset = new UIEdgeInsets(5, 5, 5, 5)
            };
            messageTextView.TextContainer.LineBreakMode = UILineBreakMode.WordWrap;

            messageTextView.AttributedText = message.Contains("<b>") ? GetAttributedText(message, true) : GetAttributedText(message, false);

            var contentHeight = GetHeight(message, messageTextView);
            contentHeight = contentHeight < AlertConstants.AlertMinimumHeight ? AlertConstants.AlertMinimumHeight : contentHeight;
            var customFrameHeight = contentHeight + AlertConstants.AlertButtonHeight + AlertConstants.AlertHeaderHeight;

            alertView.CustomFrame = new CGRect(AlertConstants.AlertLeftMargin, (UIScreen.MainScreen.Bounds.Height - customFrameHeight) / 2, UIScreen.MainScreen.Bounds.Width - AlertConstants.AlertTotalHorizontalMargin, customFrameHeight);
            messageView.Frame = new CGRect(0, 0, alertView.CustomFrame.Width, contentHeight + 10);
            messageTextView.Frame = new CGRect(0, 0.5, alertView.CustomFrame.Width, contentHeight - 4);

            messageView.AddSubview(messageTextView);
            alertView.ContentView.AddSubview(messageView);

            alertView.AlertView.BackgroundColor = UIColor.LightGray;
            alertView.ButtonsView.BackgroundColor = UIColor.LightGray;
            return alertView;
        }

        private static nfloat GetHeight(string message, UITextView view)
        {
            var height = new NSString(message).GetBoundingRect(new CGSize(UIScreen.MainScreen.Bounds.Width - AlertConstants.AlertTotalMargin, nfloat.MaxValue),
                    NSStringDrawingOptions.UsesLineFragmentOrigin,
                    new UIStringAttributes { Font = UIFont.SystemFontOfSize(AlertConstants.FontSizeForAlertMessage) },
                    null)
                .Height;

            if (view != null)
            {
                var numberOfOccurrences = view.Text.Split(new[] { "\n\n" }, StringSplitOptions.None).Length - 1;
                var lineBreakHeight = numberOfOccurrences * AlertConstants.SeparatorHeight;
                height = height + lineBreakHeight;

                if (numberOfOccurrences == 0 || numberOfOccurrences == 1)
                {
                    height += AlertConstants.ExtraHeightForNoLineBreakTexts;
                }

                //Removes extra space for HREF tag text
                if (message.Contains("href"))
                {
                    if (numberOfOccurrences > 2)
                    {
                        height -= numberOfOccurrences * AlertConstants.ExtraHeightOfTextsIncludingLinks;
                    }
                    else if (numberOfOccurrences != 2)
                    {
                        height -= AlertConstants.ExtraHeightOfTextsIncludingLinks;
                    }
                }
            }

            if (height + AlertConstants.TotalGapFromBottomAndTop > UIScreen.MainScreen.Bounds.Size.Height)
            {
                height = UIScreen.MainScreen.Bounds.Size.Height - (UIScreen.MainScreen.Bounds.Size.Height / 4);
            }
            return height;
        }

        private static NSMutableAttributedString GetAttributedText(string message, bool isbold)
        {
            var attr = new NSAttributedStringDocumentAttributes();
            var nserror = new NSError();
            attr.DocumentType = NSDocumentType.HTML;
            attr.StringEncoding = NSStringEncoding.UTF8;
            message = message.Replace("\r\n", "<br/>");

            var boldstrings = new List<NSString>();
            if (isbold)
            {
                boldstrings = ExtractFromString(message, "<b>", "</b>");
            }

            var htmlData = NSData.FromString(message);
            var parsedString = new NSAttributedString(htmlData, attr, ref nserror);

            var attributedString = new NSMutableAttributedString(parsedString);
            attributedString.AddAttribute(UIStringAttributeKey.Font, UIFont.SystemFontOfSize(AlertConstants.FontSizeForAlertMessage), new NSRange(0, attributedString.Length));
            attributedString.AddAttribute(UIStringAttributeKey.ForegroundColor, UiConstants.DefaultAppTextColor, new NSRange(0, attributedString.Length));
            attributedString.AddAttribute(UIStringAttributeKey.ParagraphStyle, new NSMutableParagraphStyle { Alignment = UITextAlignment.Center }, new NSRange(0, attributedString.Length));

            var sourcestring = new NSString(attributedString.Value);
            var boldfont = UIFont.BoldSystemFontOfSize(AlertConstants.FontSizeForAlertMessage);

            foreach (var boldstring in boldstrings)
            {
                var range = CalcRangeFor(sourcestring, boldstring);
                attributedString.AddAttribute(UIStringAttributeKey.Font, boldfont, range);
            }

            return attributedString;
        }

        private static NSRange CalcRangeFor(string source, string substring)
        {
            var range = new NSRange
            {
                Location = source.IndexOf(substring),
                Length = substring.Length
            };

            return range;
        }

        private static List<NSString> ExtractFromString(string text, string startString, string endString)
        {
            var matched = new List<NSString>();
            var exit = false;

            while (!exit)
            {
                var indexStart = text.IndexOf(startString);
                var indexEnd = text.IndexOf(endString);
                if (indexStart != -1 && indexEnd != -1)
                {
                    matched.Add(new NSString(text.Substring(indexStart + startString.Length,
                        indexEnd - indexStart - startString.Length)));
                    text = text.Substring(indexEnd + endString.Length);
                }
                else
                {
                    exit = true;
                }
            }

            return matched;
        }
    }
}