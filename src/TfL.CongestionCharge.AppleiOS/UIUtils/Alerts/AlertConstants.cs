﻿namespace TfL.CongestionCharge.AppleiOS.UIUtils.Alerts
{
    internal class AlertConstants
    {
        public const float FontSizeForAlertMessage = 14;
        public const float TotalGapFromBottomAndTop = 100;
        public const float AlertButtonHeight = 40;
        public const float AlertMinimumHeight = 50;
        public const float AlertHeaderHeight = 40;
        public const float AlertLeftMargin = 30;
        public const float AlertTotalHorizontalMargin = 60;
        public const float MessageLeftMargin = 5;
        public const float MessageTotalHorizontalMargin = 10;
        public const float AlertTotalMargin = AlertTotalHorizontalMargin + MessageTotalHorizontalMargin;
        public const float SeparatorHeight = 17;
        public const float ExtraHeightForNoLineBreakTexts = 30;
        public const float ExtraHeightOfTextsIncludingLinks = 20;
    }
}