﻿using System;

namespace TfL.CongestionCharge.AppleiOS.UIUtils
{
    public class PickerChangedEventArgs : EventArgs
    {
        public string SelectedValue { get; set; }

        public int SelectedRow { get; set; }
    }
}