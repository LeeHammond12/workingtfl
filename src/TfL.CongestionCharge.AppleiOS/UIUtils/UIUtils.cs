﻿using System;
using System.Drawing;
using CoreGraphics;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.UIUtils
{
    public static class UiUtils
    {
        /* By default, iOS will not provide a return/done button for certain types of input (such as
        numeric keyboards). The following methods add a toolbar to the top of the keyboard which then will
        dismiss the keyboard when the "Done" button is pressed.

        The methods can either be called as extension methods

        myTextField.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.Default);
        myTextView.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.Default);

        or as standard method calls

        UiUtils.UiSetKeyboardEditorWithCloseButton(myTextField, UIKeyboardType.Default);
        UiUtils.UiSetKeyboardEditorWithCloseButton(myTextView, UIKeyboardType.Default); */

        public static void UiSetKeyboardEditorWithCloseButton(this UITextView txt, UIKeyboardType keyboardType)
        {
            var toolbar = new UIToolbar
            {
                BarStyle = UIBarStyle.Black,
                Translucent = true,
            };
            toolbar.SizeToFit();

            var text = new UITextView(new CGRect(0, 0, 240, 32))
            {
                ContentInset = UIEdgeInsets.Zero,
                KeyboardType = keyboardType,
                Text = txt.Text,
                UserInteractionEnabled = true
            };
            text.Layer.CornerRadius = 4f;
            text.BecomeFirstResponder();

            var doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Done,
                                 (s, e) =>
                                 {
                                     text.ResignFirstResponder();
                                     txt.ResignFirstResponder();
                                 });

            toolbar.SetItems(new UIBarButtonItem[] { doneButton }, true);

            txt.InputAccessoryView = toolbar;
        }

        public static void SetKeyboardEditorWithCloseButton(UITextView txt, UIKeyboardType keyboardType)
        {
            var toolbar = new UIToolbar
            {
                BarStyle = UIBarStyle.Black,
                Translucent = true,
            };
            toolbar.SizeToFit();

            var text = new UITextView(new CGRect(0, 0, 240, 32))
            {
                ContentInset = UIEdgeInsets.Zero,
                KeyboardType = keyboardType,
                Text = txt.Text,
                UserInteractionEnabled = true
            };
            text.Layer.CornerRadius = 4f;
            text.BecomeFirstResponder();

            var doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Done,
                                 (s, e) =>
                                 {
                                     text.ResignFirstResponder();
                                     txt.ResignFirstResponder();
                                 });

            toolbar.SetItems(new UIBarButtonItem[] { doneButton }, true);

            txt.InputAccessoryView = toolbar;
        }

        public static void UiSetKeyboardEditorWithCloseButton(this UITextField txt, UIKeyboardType keyboardType)
        {
            var toolbar = new UIToolbar
            {
                BarStyle = UIBarStyle.Black,
                Translucent = true,
            };
            txt.KeyboardType = keyboardType;
            toolbar.SizeToFit();

            var text = new UITextView(new CGRect(0, 0, 200, 32))
            {
                ContentInset = UIEdgeInsets.Zero,
                KeyboardType = keyboardType,
                Text = txt.Text,
                UserInteractionEnabled = true
            };
            text.Layer.CornerRadius = 4f;
            text.BecomeFirstResponder();

            var doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Done,
                                 (s, e) =>
                                 {
                                     text.ResignFirstResponder();
                                     txt.ResignFirstResponder();
                                 });

            toolbar.UserInteractionEnabled = true;
            toolbar.SetItems(new UIBarButtonItem[] { doneButton }, true);

            txt.InputAccessoryView = toolbar;
        }

        public static void SetKeyboardEditorWithCloseButton(UITextField txt, UIKeyboardType keyboardType)
        {
            var toolbar = new UIToolbar
            {
                BarStyle = UIBarStyle.Black,
                Translucent = true,
            };
            txt.KeyboardType = keyboardType;
            toolbar.SizeToFit();

            var text = new UITextView(new CGRect(0, 0, 200, 32))
            {
                ContentInset = UIEdgeInsets.Zero,
                KeyboardType = keyboardType,
                Text = txt.Text,
                UserInteractionEnabled = true
            };
            text.Layer.CornerRadius = 4f;
            text.BecomeFirstResponder();

            var doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Done,
                                 (s, e) =>
                                 {
                                     text.ResignFirstResponder();
                                     txt.ResignFirstResponder();
                                 });

            toolbar.UserInteractionEnabled = true;
            toolbar.SetItems(new UIBarButtonItem[] { doneButton }, true);

            txt.InputAccessoryView = toolbar;
        }

        // animates scroll up and down when the keyboard shows/vanishes
        public static void AnimateTextField(UIView view, bool direction)
        {
            var moveDist = 100;
            var moveDur = 0.3;
            var move = direction ? -moveDist : 0;
            UIView.BeginAnimations("anim");
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(moveDur);
            view.Frame = new CGRect(view.Frame.X, (float)move, view.Bounds.Width, view.Bounds.Height);
            UIView.CommitAnimations();
        }

        public static float LinesHeight(string text, float width, float defaultControlheight)
        {
            var s = text.StringSize(UIFont.SystemFontOfSize(16), new SizeF(width, 500), UILineBreakMode.WordWrap);

            return (float)s.Height;
        }
        public static UIImage MaxResizeImage(UIImage sourceImage, float maxWidth, float maxHeight)
        {
            var sourceSize = sourceImage.Size;
            var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
            if (maxResizeFactor > 1) return sourceImage;
            var width = (float)(maxResizeFactor * sourceSize.Width);
            var height = (float)(maxResizeFactor * sourceSize.Height);
            UIGraphics.BeginImageContext(new SizeF(width, height));
            sourceImage.Draw(new RectangleF(0, 0, width, height));
            var resultImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return resultImage;
        }
    }
}