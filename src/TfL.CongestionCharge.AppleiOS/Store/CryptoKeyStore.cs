using System;
using Foundation;
using Security;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.PCLCrypto;

namespace TfL.CongestionCharge.AppleiOS.KeyProvider
{
    public class CryptoKeyStore : ICryptoKeyStore
    {
        public void StoreKey()
        {
            var sr = new SecRecord(SecKind.GenericPassword)
            {
                ValueData = NSData.FromString(Convert.ToBase64String(CryptoUtilities.GetAes256KeyMaterial())),
                Generic = NSData.FromString(Sharedkeys.PclCryptoKey)
            };

            var status = SecKeyChain.Add(sr);
            if (status != SecStatusCode.Success && status != SecStatusCode.DuplicateItem)
            {
                throw new Exception($"CryptoKeyStore: Couldnt save key, error: {status}");
            }
        }

        public byte[] GetKey()
        {
            var sr = new SecRecord(SecKind.GenericPassword)
            {
                Generic = NSData.FromString(Sharedkeys.PclCryptoKey)
            };

            SecStatusCode statusCode;
            var match = SecKeyChain.QueryAsRecord(sr, out statusCode);
            if (statusCode == SecStatusCode.Success && match != null)
            {
                var keyValue = match.ValueData.ToString();
                var keybytes = Convert.FromBase64String(keyValue);
                return keybytes;
            }

            throw new Exception("CryptoKeyStore: Couldnt load key, error: {status}");
        }
    }
}