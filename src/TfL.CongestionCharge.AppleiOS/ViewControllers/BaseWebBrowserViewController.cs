﻿using System;
using Foundation;
using Tfl.CongestionCharge.Core.Resources;
using UIKit;
using Tfl.CongestionCharge.Core.ViewModel;

namespace TfL.CongestionCharge.AppleiOS.ViewControllers
{
    public class BaseWebBrowserViewController<TViewModel> : BaseViewController<TViewModel> where TViewModel : TflViewModelBase
    {
        private LoadingOverlay _loadingOverlay;
        private UIWebView _webView;

        protected BaseWebBrowserViewController(IntPtr handle)
            : base(handle)
        {
        }

        protected BaseWebBrowserViewController(string nib, NSBundle bundle)
            : base(nib, bundle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.Privacy, () => ViewModel.GoBackCommand.Execute(null));
        }

        public void ShowLoader(UIView view)
        {
            if (_loadingOverlay != null)
                return;
            _loadingOverlay = new LoadingOverlay(view.Frame);
            View.Add(_loadingOverlay);
        }

        public void HideLoader()
        {
            _loadingOverlay?.FadeOutAndRemoveFromParentView();
        }

        public void ShowPrivacyDetailsInWebView(string url, UIWebView webView)
        {
            _webView = webView;
            _webView.BackgroundColor = UIColor.White;
            var req = new NSMutableUrlRequest(new NSUrl(url));
            _webView.LoadRequest(req);
            _webView.LoadStarted += (sender, args) => ShowLoader(_webView);
            _webView.LoadFinished += (sender, args) => HideLoader();
        }
    }
}