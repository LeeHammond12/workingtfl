﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;
using Tfl.CongestionCharge.Core.ViewModel.Payments;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.TableCellRenderers;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.ViewControllers
{
    /// <summary>
    /// The pcc payment detail view controller.
    /// </summary>
    public partial class PaymentViewController : BaseViewController<PaymentViewModel>
    {
        private readonly nfloat _btnHeight = UiConstants.DefaultButtonHeight;
        private readonly nfloat _verticalMargin = UiConstants.VerticalMargin;
        private readonly nfloat _smallVerticalMargin = UiConstants.SmallVerticalMargin;
        private readonly nfloat _textFieldHeight = UiConstants.TextFieldHeight;
        private readonly float _viewOffset = 32.0f;

        private NSLayoutConstraint _nsCardContainerHeight, _nsLblOrLeftLineHeight, _nsLblOrRightLineHeight, _nsLblOrheight, _nsLblSelectSavedCardHeight, _nsTblSavedCardsHeight,
            _nsCardContainerBelowLineHeight, _nsTextNumberHeight;

        private UIView _activeview;
        private float _scrollAmount;
        private bool _moveViewUp;

        public PaymentViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            scrlView.ContentSize = new CGSize(320, tblStoredCards.ContentSize.Height + 520);
            ChangeCardContainerHeight();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetChargeDetails();
            InitUi();
            AddNsLayoutConstraintsTocardContainer();
            GetcardContainerNsLayoutHeightConstraint();
            PerformDataBind();
            SetKeyboard();
            SetFonts();
            RenderSavedCards();
            txtNumber.Hidden = true;
            BindButton();
            RenderSmsOption();
            ChangeBtnColor(btnConfirmPay, ViewModel.CanProceedWithPayment);

            tblStoredCards.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            LoadingTitle = Strings.PleaseWaitForPaymentMessage;
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, KeyBoardUpNotification);
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidHideNotification, KeyBoardDownNotification);
            NavigationItem.Title = ViewModel.PageTitle;
        }
        private void SetChargeDetails()
        {
            var payment = NavigationParameter as CcState;
            if (payment != null)
            {
                //ViewModel.Payment = payment;

                InvokeOnMainThread(() =>
                {
                    //lblAmountDetail.Text = ViewModel.Payment.TotalPrice.ToCurrencyString();
                });
            }
        }

        private void KeyBoardDownNotification(NSNotification notification)
        {
            var rect = UIKeyboard.BoundsFromNotification(notification);
            if (_moveViewUp)
            {
                CenterViewInScroll(_activeview, scrlView, 0, 150);
                _moveViewUp = false;
            }
        }

        private void RenderSmsOption()
        {
            if (ViewModel.IsPcn)
            {
                lblSendReceiptSMS.Hidden = true;
                swchSendReceipt.Hidden = true;
                lblReceipt.Hidden = true;
            }
        }

        private void SetKeyboard()
        {
            txtCardName.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.Default);
            txtNumber.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.NumberPad);
            editCardNumber.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.NumberPad);
            editCVV.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.NumberPad);
            editExpDate.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.NumberPad);

            txtCardName.AutocorrectionType = UITextAutocorrectionType.No;
            txtNumber.AutocorrectionType = UITextAutocorrectionType.No;
            editCardNumber.AutocorrectionType = UITextAutocorrectionType.No;
            editCVV.AutocorrectionType = UITextAutocorrectionType.No;
            editExpDate.AutocorrectionType = UITextAutocorrectionType.No;

            txtCardName.ReturnKeyType = UIReturnKeyType.Next;

            txtCardName.ShouldReturn += (textField) =>
            {
                editCardNumber.BecomeFirstResponder();
                return true;
            };
        }

        private void SetFonts()
        {
            btnConfirmPay.SetBook();
            editExpDate.SetBook();
            editCVV.SetBook();
            editCardNumber.SetBook();
            txtCardName.SetBook();
            txtNumber.SetBook();
            lblOR.SetBook(false, UiConstants.SmallFont);
            lblPaymentDetails.SetMedium(true);
            lblReviewDetails.SetMedium(true);
            lblSaveDetails.SetMediumBold(true);
            lblSendReceiptSMS.SetBook(true);
            lblOR.SetBook();
            lblSelectStoredCard.SetMedium(true);
            lblReceipt.SetMedium(true);
        }

        private void BindButton()
        {
            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy, BindingMode.OneWay).WhenSourceChanges(() =>
            {
                if (ViewModel.IsBusy)
                {
                    NavigationItem.SetHidesBackButton(true, false);
                }
                else
                {
                    NavigationItem.SetHidesBackButton(false, true);
                }

                txtCardName.ResignFirstResponder();
                txtNumber.ResignFirstResponder();
                editCardNumber.ResignFirstResponder();
                editCVV.ResignFirstResponder();
                editExpDate.ResignFirstResponder();
            }));
            btnConfirmPay.SetCommand(Events.TouchUpInside, ViewModel.MakePaymentCommand);
        }

        private void PerformDataBind()
        {
            Bindings.Add(this.SetBinding(() => ViewModel.CardNumber, () => editCardNumber.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.ExpiryDate, () => editExpDate.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.Cvv2, () => editCVV.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.NameOnCard, () => txtCardName.Text, BindingMode.TwoWay));
            // todo IOS why are these bindings duplicate?
            Bindings.Add(this.SetBinding(() => ViewModel.SmsNumber, () => txtNumber.Text, BindingMode.TwoWay));

            Bindings.Add(this.SetBinding(() => editCardNumber.Text)
                .UpdateSourceTrigger(Events.EditingChanged)
                .WhenSourceChanges(() =>
                {
                    ViewModel.CardNumber = editCardNumber.Text;
                    ChangeBtnColor(btnConfirmPay, ViewModel.CanProceedWithPayment);
                }));

            Bindings.Add(this.SetBinding(() => editCVV.Text)
                .UpdateSourceTrigger(Events.EditingChanged)
                .WhenSourceChanges(() =>
                {
                    ViewModel.Cvv2 = editCVV.Text;
                    ChangeBtnColor(btnConfirmPay, ViewModel.CanProceedWithPayment);
                }));

            Bindings.Add(this.SetBinding(() => editExpDate.Text)
                .UpdateSourceTrigger(Events.EditingChanged)
                .WhenSourceChanges(() =>
                {
                    ViewModel.ExpiryDate = editExpDate.Text;
                    ChangeBtnColor(btnConfirmPay, ViewModel.CanProceedWithPayment);
                }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsSmsTextVisible, BindingMode.TwoWay)
                    .WhenSourceChanges(ChangeEditNumber));

            Bindings.Add(this.SetBinding(() => txtCardName.Text)
                .UpdateSourceTrigger(Events.EditingChanged)
                .WhenSourceChanges(() =>
                {
                    ViewModel.NameOnCard = txtCardName.Text;
                    ChangeBtnColor(btnConfirmPay, ViewModel.CanProceedWithPayment);
                }));

            Bindings.Add(this.SetBinding(() => txtNumber.Text)
                .UpdateSourceTrigger(Events.EditingChanged)
                .WhenSourceChanges(() =>
                {
                    ViewModel.SmsNumber = txtNumber.Text;
                }));

            Bindings.Add(this.SetBinding(() => swchSendReceipt.On)
                .UpdateSourceTrigger(Events.ValueChanged)
                .WhenSourceChanges(() =>
                {
                    ViewModel.IsSmsTextVisible = swchSendReceipt.On;
                    ChangeEditNumber();
                }));

            Bindings.Add(this.SetBinding(() => swtchSaveDetails.On)
                .UpdateSourceTrigger(Events.ValueChanged)
                .WhenSourceChanges(() =>
                {
                    ViewModel.SaveDetails = swtchSaveDetails.On;
                }));
        }

        private void ChangeEditNumber()
        {
            InvokeOnMainThread(() =>
            {
                txtNumber.Hidden = !swchSendReceipt.On;
                _nsTextNumberHeight.Constant = swchSendReceipt.On ? _textFieldHeight : 0;
            });
        }

        private void RenderSavedCards()
        {
            var tableItems = new List<CardTableItem>();

            if (ViewModel.UserPaymentCards != null)
            {
                if (ViewModel.UserPaymentCards.Any())
                {
                    tableItems.AddRange(ViewModel.UserPaymentCards.Select(vm => new CardTableItem(vm)));

                    var tableSource = new CardTableSource(tableItems, ViewModel);

                    tblStoredCards.Source = tableSource;

                    tableSource.RowClicked += (o, i) =>
                    {
                        // todo IOS
                        // there are now 2 viewmodels, one for selecting a card
                        // and one for creating a card, therefore we need 2 view controllers:
                        //  - SelectPaymentCardViewController 
                        //  - PaymentViewController
                        // ViewModel.SelectCard(tableSource.SelectedCard);

                        if (tableSource.NsIndexPathPrevious == null)
                        {
                            tableSource.NsIndexPathPrevious = ((RowSelectedEventArgs)i).IndexPath;
                            tblStoredCards.CellAt(tableSource.NsIndexPathPrevious).Accessory =
                                UITableViewCellAccessory.Checkmark;

                            BeginInvokeOnMainThread(async () =>
                            {
                                await ViewModel.PayWithCard();
                                editCVV.AttributedPlaceholder = new NSAttributedString(Strings.EnterCcv, UIFont.FromName("NJFont-Book", 16), UIColor.Red);
                                editCVV.BecomeFirstResponder();
                                lblSaveDetails.TextColor = AppDelegate.Current.LightGrey;
                                swtchSaveDetails.Enabled = false;
                            });
                        }
                        else if (tableSource.NsIndexPathCurrent.Equals(tableSource.NsIndexPathPrevious))
                        {
                            tblStoredCards.DeselectRow(((RowSelectedEventArgs)i).IndexPath, true);
                            tblStoredCards.CellAt(tableSource.NsIndexPathPrevious).Accessory = UITableViewCellAccessory.None;
                            tableSource.NsIndexPathPrevious = null;
                            editCVV.AttributedPlaceholder = new NSAttributedString(Strings.EnterCcv, null, UIColor.FromRGB(199, 199, 205));
                            lblSaveDetails.TextColor = AppDelegate.Current.PrimaryBlue;
                            swtchSaveDetails.Enabled = true;
                        }
                        else
                        {
                            tblStoredCards.DeselectRow(tableSource.NsIndexPathPrevious, true);

                            var cell = tblStoredCards.CellAt(tableSource.NsIndexPathPrevious);
                            if (cell != null)
                            {
                                cell.Accessory = UITableViewCellAccessory.None;
                            }

                            tableSource.NsIndexPathPrevious = ((RowSelectedEventArgs)i).IndexPath;
                            tblStoredCards.CellAt(tableSource.NsIndexPathPrevious).Accessory =
                                UITableViewCellAccessory.Checkmark;
                            BeginInvokeOnMainThread(async () => await ViewModel.PayWithCard());
                            lblSaveDetails.TextColor = AppDelegate.Current.LightGrey;
                            swtchSaveDetails.Enabled = false;
                            editCVV.BecomeFirstResponder();
                        }

                        ChangeBtnColor(btnConfirmPay, false);
                    };
                }
                else
                {
                    ChangeConstraintsWhenNoCardSaved();
                    lblOR.Hidden = true;
                    tblStoredCards.Hidden = true;
                    lblSelectStoredCard.Hidden = true;
                    lblORLeftLine.Hidden = true;
                    lblORRightLine.Hidden = true;
                    cardContainerBelowLine.Hidden = true;
                }
            }
        }

        private void SetAutoresizingMaskToAllViews()
        {
            scrlView.TranslatesAutoresizingMaskIntoConstraints = false;
            lblPaymentDetails.TranslatesAutoresizingMaskIntoConstraints = false;
            txtCardName.TranslatesAutoresizingMaskIntoConstraints = false;
            editCardNumber.TranslatesAutoresizingMaskIntoConstraints = false;
            editExpDate.TranslatesAutoresizingMaskIntoConstraints = false;
            editCVV.TranslatesAutoresizingMaskIntoConstraints = false;
            lblSaveDetails.TranslatesAutoresizingMaskIntoConstraints = false;
            swtchSaveDetails.TranslatesAutoresizingMaskIntoConstraints = false;
            cardContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            lblOR.TranslatesAutoresizingMaskIntoConstraints = false;
            lblORLeftLine.TranslatesAutoresizingMaskIntoConstraints = false;
            lblORRightLine.TranslatesAutoresizingMaskIntoConstraints = false;
            lblSelectStoredCard.TranslatesAutoresizingMaskIntoConstraints = false;
            tblStoredCards.TranslatesAutoresizingMaskIntoConstraints = false;
            cardContainerBelowLine.TranslatesAutoresizingMaskIntoConstraints = false;
            lblReviewDetails.TranslatesAutoresizingMaskIntoConstraints = false;

            if (ViewModel.IsPcn)
            {
                btnConfirmPay.TranslatesAutoresizingMaskIntoConstraints = false;
            }
            else
            {
                lblReceipt.TranslatesAutoresizingMaskIntoConstraints = false;
                lblSendReceiptSMS.TranslatesAutoresizingMaskIntoConstraints = false;
                swchSendReceipt.TranslatesAutoresizingMaskIntoConstraints = false;
                txtNumber.TranslatesAutoresizingMaskIntoConstraints = false;
                btnConfirmPay.TranslatesAutoresizingMaskIntoConstraints = false;
            }
        }

        private void InitUi()
        {
            SetAutoresizingMaskToAllViews();
            View.RemoveConstraints(View.Constraints);
            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            scrlView.RemoveConstraints(scrlView.Constraints);
            View.AddConstraints(
                scrlView.AtTopOf(View, 6 * _verticalMargin),
                scrlView.WithSameLeft(View),
                scrlView.WithSameRight(View),
                scrlView.WithSameBottom(View),
                lblPaymentDetails.AtTopOf(scrlView, _verticalMargin),
                lblPaymentDetails.WithSameLeft(View).Plus(UiConstants.LeftMargin),
                lblPaymentDetails.WithSameRight(View).Minus(UiConstants.LeftMargin),
                txtCardName.Below(lblPaymentDetails, _verticalMargin),
                txtCardName.WithSameLeft(lblPaymentDetails),
                txtCardName.WithSameRight(lblPaymentDetails),
                editCardNumber.Below(txtCardName, _verticalMargin),
                editCardNumber.WithSameLeft(txtCardName),
                editCardNumber.WithSameRight(txtCardName),
                editExpDate.Below(editCardNumber, _verticalMargin),
                editExpDate.WithSameLeft(editCardNumber),
                editExpDate.ToLeftOf(editCVV, _verticalMargin),
                editCVV.WithSameTop(editExpDate),
                editCVV.WithSameRight(editCardNumber),
                lblSaveDetails.Below(editExpDate, _verticalMargin),
                lblSaveDetails.WithSameLeft(editExpDate),
                lblSaveDetails.WithSameHeight(editCVV),
                swtchSaveDetails.WithSameTop(lblSaveDetails),
                swtchSaveDetails.ToRightOf(lblSaveDetails, _verticalMargin),
                swtchSaveDetails.WithSameRight(editCardNumber),
                cardContainer.Below(swtchSaveDetails, _verticalMargin),
                cardContainer.WithSameLeft(lblSaveDetails),
                cardContainer.WithSameRight(swtchSaveDetails),
                cardContainer.Height().EqualTo(200).SetPriority(500),
                lblOR.AtTopOf(cardContainer),
                lblOR.WithSameCenterX(cardContainer),
                lblORLeftLine.WithSameCenterY(lblOR),
                lblORLeftLine.WithSameLeft(cardContainer),
                lblORLeftLine.ToLeftOf(lblOR, 5),
                lblORLeftLine.Height().EqualTo(1),
                lblORRightLine.WithSameCenterY(lblOR),
                lblORRightLine.WithSameRight(cardContainer),
                lblORRightLine.ToRightOf(lblOR, 5),
                lblORRightLine.Height().EqualTo(1),
                lblSelectStoredCard.Below(lblOR).Plus(6),
                lblSelectStoredCard.WithSameLeft(cardContainer),
                lblSelectStoredCard.WithSameRight(cardContainer),
                tblStoredCards.Below(lblSelectStoredCard),
                tblStoredCards.WithSameLeft(cardContainer),
                tblStoredCards.WithSameRight(cardContainer),
                tblStoredCards.WithSameBottom(cardContainer).Minus(10),
                cardContainerBelowLine.WithSameBottom(cardContainer),
                cardContainerBelowLine.WithSameLeft(cardContainer),
                cardContainerBelowLine.WithSameRight(cardContainer),
                cardContainerBelowLine.Height().EqualTo(1),
                lblReviewDetails.Below(cardContainer, _verticalMargin),
                lblReviewDetails.WithSameLeft(cardContainer),
                lblReviewDetails.WithSameRight(cardContainer));

            if (ViewModel.IsPcn)
            {
                //View.AddConstraints(
                //    btnConfirmPay.Below(lblJourneyDatePeriod, 15),
                //    btnConfirmPay.WithSameLeft(lblPaymentDetails),
                //    btnConfirmPay.WithSameRight(lblPaymentDetails),
                //    btnConfirmPay.Height().EqualTo(_btnHeight));
            }
            else
            {
                //View.AddConstraints(
                //    lblReceipt.Below(lblJourneyDatePeriod, _verticalMargin),
                //    lblReceipt.WithSameLeft(lblSaveDetails),
                //    lblReceipt.WithSameRight(lblSaveDetails),
                //    lblSendReceiptSMS.Below(lblReceipt),
                //    lblSendReceiptSMS.WithSameLeft(lblSaveDetails),
                //    lblSendReceiptSMS.ToLeftOf(swchSendReceipt, _verticalMargin),
                //    lblSendReceiptSMS.WithSameHeight(editCVV),
                //    swchSendReceipt.WithSameTop(lblSendReceiptSMS),
                //    swchSendReceipt.WithSameRight(editCardNumber),
                //    txtNumber.Below(lblSendReceiptSMS, _smallVerticalMargin),
                //    txtNumber.WithSameLeft(lblPaymentDetails),
                //    txtNumber.WithSameRight(lblPaymentDetails),
                //    btnConfirmPay.Below(txtNumber, 15),
                //    btnConfirmPay.WithSameLeft(txtNumber),
                //    btnConfirmPay.WithSameRight(txtNumber),
                //    btnConfirmPay.Height().EqualTo(_btnHeight));
            }

            tblStoredCards.ReloadData();
            tblStoredCards.SectionFooterHeight = 0;
            tblStoredCards.TableFooterView = null;
            tblStoredCards.TableHeaderView = null;
            tblStoredCards.ContentInset = new UIEdgeInsets(0, -8, 0, 0);
            AutomaticallyAdjustsScrollViewInsets = false;
            //lblReviewDetails.Text = ViewModel.ReviewChargesText;

            btnConfirmPay.AddBottomBorder();
        }

        private void ChangeCardContainerHeight()
        {
            _nsCardContainerHeight.Constant = tblStoredCards.ContentSize.Height == 0 ? 0 : tblStoredCards.ContentSize.Height + 60;
            _nsTblSavedCardsHeight.Constant = tblStoredCards.ContentSize.Height;
        }

        private void ChangeConstraintsWhenNoCardSaved()
        {
            _nsCardContainerHeight.Constant = 0;
            _nsLblOrLeftLineHeight.Constant = 0;
            _nsLblOrRightLineHeight.Constant = 0;
            _nsLblOrheight.Constant = 0;
            _nsLblSelectSavedCardHeight.Constant = 0;
            _nsTblSavedCardsHeight.Constant = 0;
            _nsCardContainerBelowLineHeight.Constant = 0;
        }

        private void AddNsLayoutConstraintsTocardContainer()
        {
            var cardContainerHeight = NSLayoutConstraint.Create(cardContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 20);
            var lblOrLeftLine = NSLayoutConstraint.Create(lblORLeftLine, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 1);
            var lblOr = NSLayoutConstraint.Create(lblOR, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 15);
            var lblOrRightLine = NSLayoutConstraint.Create(lblORRightLine, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 1);
            var lblSelectStoredCard = NSLayoutConstraint.Create(this.lblSelectStoredCard, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 15);
            var tblStoredCards = NSLayoutConstraint.Create(this.tblStoredCards, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 20);
            var cardContainerBelowLine = NSLayoutConstraint.Create(this.cardContainerBelowLine, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 1);
            var textNumber = NSLayoutConstraint.Create(txtNumber, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 10);
            scrlView.AddConstraints(new[] { cardContainerHeight, lblOrLeftLine, lblOr, lblOrRightLine, lblSelectStoredCard, tblStoredCards, cardContainerBelowLine, textNumber });
        }

        private void GetcardContainerNsLayoutHeightConstraint()
        {
            foreach (var constraint in scrlView.Constraints)
            {
                if (constraint.FirstItem == lblORLeftLine)
                {
                    _nsLblOrLeftLineHeight = constraint;
                }
                else if (constraint.FirstItem == lblOR)
                {
                    _nsLblOrheight = constraint;
                }
                else if (constraint.FirstItem == lblORRightLine)
                {
                    _nsLblOrRightLineHeight = constraint;
                }
                else if (constraint.FirstItem == lblSelectStoredCard)
                {
                    _nsLblSelectSavedCardHeight = constraint;
                }
                else if (constraint.FirstItem == tblStoredCards)
                {
                    _nsTblSavedCardsHeight = constraint;
                }
                else if (constraint.FirstItem == cardContainerBelowLine)
                {
                    _nsCardContainerBelowLineHeight = constraint;
                }
                else if (constraint.FirstItem == txtNumber)
                {
                    _nsTextNumberHeight = constraint;
                }
                else
                {
                    _nsCardContainerHeight = constraint;
                }
            }
        }

        private void KeyBoardUpNotification(NSNotification notification)
        {
            var rect = UIKeyboard.BoundsFromNotification(notification);

            foreach (var view in scrlView.Subviews)
            {
                if (view.IsFirstResponder)
                {
                    _activeview = view;
                }
            }

            if (_activeview != null)
            {
                var bottom = _activeview.Frame.Y + _activeview.Frame.Height + _viewOffset;
                _scrollAmount = (float)(rect.Height - (scrlView.Frame.Size.Height - bottom));

                if (_scrollAmount > 0 && !_moveViewUp)
                {
                    _moveViewUp = true;
                    CenterViewInScroll(_activeview, scrlView, rect.Height, rect.Height);
                }
            }
        }

    }
}