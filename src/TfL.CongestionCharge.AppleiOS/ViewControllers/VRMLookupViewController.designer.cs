﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.ViewControllers
{
    [Register ("LookupVrmViewController")]
    partial class LookupVrmViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnCheckVrm { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnChooseDates { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField editAccNo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField editVRM { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblCCZone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblConfirmVRM { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSaveDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSaveVehicle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSelectSavedVehicle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblUKReg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblVRMColor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblVRMMake { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblVRMModal { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblWhichVehicle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView savedVehicleContainerView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView scrlView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch swchCCZone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch swchConfirm { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch swchSaveVrm { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch swchUKReg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tblVehicles { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView vehicleDetailsContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnCheckVrm != null) {
                btnCheckVrm.Dispose ();
                btnCheckVrm = null;
            }

            if (btnChooseDates != null) {
                btnChooseDates.Dispose ();
                btnChooseDates = null;
            }

            if (editAccNo != null) {
                editAccNo.Dispose ();
                editAccNo = null;
            }

            if (editVRM != null) {
                editVRM.Dispose ();
                editVRM = null;
            }

            if (lblCCZone != null) {
                lblCCZone.Dispose ();
                lblCCZone = null;
            }

            if (lblConfirmVRM != null) {
                lblConfirmVRM.Dispose ();
                lblConfirmVRM = null;
            }

            if (lblSaveDetails != null) {
                lblSaveDetails.Dispose ();
                lblSaveDetails = null;
            }

            if (lblSaveVehicle != null) {
                lblSaveVehicle.Dispose ();
                lblSaveVehicle = null;
            }

            if (lblSelectSavedVehicle != null) {
                lblSelectSavedVehicle.Dispose ();
                lblSelectSavedVehicle = null;
            }

            if (lblUKReg != null) {
                lblUKReg.Dispose ();
                lblUKReg = null;
            }

            if (lblVRMColor != null) {
                lblVRMColor.Dispose ();
                lblVRMColor = null;
            }

            if (lblVRMMake != null) {
                lblVRMMake.Dispose ();
                lblVRMMake = null;
            }

            if (lblVRMModal != null) {
                lblVRMModal.Dispose ();
                lblVRMModal = null;
            }

            if (lblWhichVehicle != null) {
                lblWhichVehicle.Dispose ();
                lblWhichVehicle = null;
            }

            if (savedVehicleContainerView != null) {
                savedVehicleContainerView.Dispose ();
                savedVehicleContainerView = null;
            }

            if (scrlView != null) {
                scrlView.Dispose ();
                scrlView = null;
            }

            if (swchCCZone != null) {
                swchCCZone.Dispose ();
                swchCCZone = null;
            }

            if (swchConfirm != null) {
                swchConfirm.Dispose ();
                swchConfirm = null;
            }

            if (swchSaveVrm != null) {
                swchSaveVrm.Dispose ();
                swchSaveVrm = null;
            }

            if (swchUKReg != null) {
                swchUKReg.Dispose ();
                swchUKReg = null;
            }

            if (tblVehicles != null) {
                tblVehicles.Dispose ();
                tblVehicles = null;
            }

            if (vehicleDetailsContainer != null) {
                vehicleDetailsContainer.Dispose ();
                vehicleDetailsContainer = null;
            }
        }
    }
}