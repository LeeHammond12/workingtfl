﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using GalaSoft.MvvmLight.Views;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Environment;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.ViewControllers
{
    public class BaseViewController<TViewModel> : ControllerBase where TViewModel : TflViewModelBase
    {
        protected List<Binding> Bindings = new List<Binding>();

        private LoadingOverlay _loadingOverlay;

        public BaseViewController(IntPtr handle)
            : base(handle)
        {
            ViewModel = AppEnvironment.Current.GetViewModel<TViewModel>();
        }

        public BaseViewController(string v, NSBundle p) : base(v, p)
        {
        }

        protected AppNavigationService NavigationService
            => (AppNavigationService)AppEnvironment.Current.NavigationService;

        protected TViewModel ViewModel { get; set; }

        protected string LoadingTitle { get; set; }

        public override void ViewDidLoad()
        {
            NavigationItem.HidesBackButton = true;
            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(HandleLoadingNotifications));
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            base.ViewDidLoad();
        }

        public void ChangeBtnColor(UIButton btn, bool isActive)
        {
            btn.SetTitleColor(UIColor.White, UIControlState.Normal);
            btn.BackgroundColor = isActive ? AppDelegate.Current.PrimaryBlue : AppDelegate.Current.LightBlue;
        }

        public void SetUpNavigationBar(string title = "", Action goBack = null, bool hideBackButton = false)
        {
            var width = View.Frame.Width;

            if (hideBackButton)
            {
                if (ViewModel.IsGoBackToHomeButtonVisible)
                {
                    NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(string.Empty, UIBarButtonItemStyle.Plain, null), true);
                }
            }
            else
            {
                var image = UiUtils.MaxResizeImage(UIImage.FromBundle("back_arrow"), 10, 10);
                var negativeSpace = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace) { Width = -6 };
                var leftButton = new UIBarButtonItem(image, UIBarButtonItemStyle.Plain, (sender, args) => { GoBack(goBack); });
                var items = new[] { negativeSpace, leftButton };
                NavigationItem.SetLeftBarButtonItems(items, true);
                width -= leftButton.Width;
            }

            if (ViewModel.IsGoBackToHomeButtonVisible)
            {
                var rightButton = new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Plain, null);
                // workaround for the linker
                rightButton.Clicked += (_, __) => { };
                rightButton.SetCommand(ViewModel.GoBackToHomeCommand);
                var attributes = new UITextAttributes { Font = UIFont.FromName("NJFont-Book", UiConstants.SmallTextFontSize) };
                rightButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
                width -= rightButton.Width;
                NavigationItem.SetRightBarButtonItem(rightButton, true);
            }
            else if (!hideBackButton)
            {
                NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(string.Empty, UIBarButtonItemStyle.Plain, null), true);
            }

            NavigationItem.TitleView = MultipleLineTitle(title, width);
        }

        protected UIView GetNavigationHeaderView(string title)
        {
            return MultipleLineTitle(title, View.Frame.Width);
        }

        protected virtual void CenterViewInScroll(UIView viewToCenter, UIScrollView scrollView, nfloat keyboardHeight, nfloat keyboardHeightSpace)
        {
            UIView.BeginAnimations(string.Empty, IntPtr.Zero);
            UIView.SetAnimationDuration(0.3);
            var contentInsets = new UIEdgeInsets(0.0f, 0.0f, keyboardHeight, 0.0f);
            scrollView.ContentInset = contentInsets;
            scrollView.ScrollIndicatorInsets = contentInsets;
            var relativeFrame = viewToCenter.Superview.ConvertRectToView(viewToCenter.Frame, scrollView);
            var landscape = InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight;
            var spaceAboveKeyboard = (landscape ? scrollView.Frame.Width : scrollView.Frame.Height) - keyboardHeightSpace;
            var offset = (float)(relativeFrame.Y - ((spaceAboveKeyboard - viewToCenter.Frame.Height) / 2));
            scrollView.ContentOffset = new PointF(0, offset);
            UIView.CommitAnimations();
        }

        private UILabel MultipleLineTitle(string titleText, nfloat width)
        {
            var title = new UILabel(new CGRect(0, 0, width, 44))
            {
                BackgroundColor = UIColor.Clear,
                TextAlignment = UITextAlignment.Center,
                Text = titleText
            };
            title.SetNormalWhiteTextStyle();
            return title;
        }

        private void GoBack(Action goBack)
        {
            if (ViewModel.IsBusy)
            {
                return;
            }

            if (goBack != null)
            {
                goBack.Invoke();
            }
            else
            {
                NavigationController?.PopViewController(true);
            }
        }

        private void HandleLoadingNotifications()
        {
            if (ViewModel.IsBusy)
            {
                // Determine the correct size to start the overlay (depending on device orientation)
                var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
                if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
                {
                    bounds.Size = new CGSize(bounds.Size.Height, bounds.Size.Width);
                }

                // show the loading overlay on the UI thread using the correct orientation sizing
                _loadingOverlay = new LoadingOverlay(bounds);
                View.Add(_loadingOverlay);
            }
            else
            {
                _loadingOverlay?.FadeOutAndRemoveFromParentView();
            }
        }
    }
}