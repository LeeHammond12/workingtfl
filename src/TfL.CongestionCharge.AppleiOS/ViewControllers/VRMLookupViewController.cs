﻿using System;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS.ViewControllers
{
    /// <summary>
    /// The vrm lookup view controller.
    /// </summary>
    public partial class LookupVrmViewController : BaseViewController<LookupVrmViewModel>
    {
        private readonly nfloat _btnHeight = UiConstants.DefaultButtonHeight;
        private readonly nfloat _smallMargin = UiConstants.SmallMargin;
        private readonly nfloat _verticalMargin = UiConstants.VerticalMargin;
        private readonly nfloat _textFieldHeight = UiConstants.TextFieldHeight;
        private readonly nfloat _leftMargin = UiConstants.LeftMargin;
        private readonly float _viewOffset = 32.0f;
        private readonly float _lblDetailHeight = 17;

        private NSLayoutConstraint _nsLayoutHeightConstraint, _nsLblSelectVehicleHeight, _nsTblVehiclesHeight, _nsLblVRMColorHeight, _nsLblVRMModalHeight, _nsLblVRMMakeHeight, _nsVehicleDetailContainerHeight;

        private UIView _activeview;
        private float _scrollAmount;
        private bool _moveViewUp;
        private TableSource _tableSource;

        public LookupVrmViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            scrlView.ContentSize = new CGSize(320, tblVehicles.ContentSize.Height + 460);
            ChangesavedVehicleContainerViewHeight();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            InitUi();
            AddNsLayoutConstraintsToeditAccNo();
            GeteditAccNoNsLayoutHeightConstraint();
            PerformDataBind();
            SetKeyboardDismissal();
            SetFonts();
            BindButtons();
            // todo IOS
            //ChangeBtnColor(btnChooseDates, ViewModel.CanSaveAndProceed);
            ChangeBtnColor(btnCheckVrm, !string.IsNullOrEmpty(ViewModel.EnteredVrm));
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, KeyBoardUpNotification);
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidHideNotification, KeyBoardDownNotification);

            NavigationItem.Title = ViewModel.PageTitle;
        }

        private void KeyBoardUpNotification(NSNotification notification)
        {
            var rect = UIKeyboard.BoundsFromNotification(notification);

            foreach (var view in scrlView.Subviews)
            {
                if (view.IsFirstResponder)
                {
                    _activeview = view;
                }
            }

            if (_activeview != null)
            {
                var bottom = _activeview.Frame.Y + _activeview.Frame.Height + _viewOffset;
                _scrollAmount = (float)(rect.Height - (scrlView.Frame.Size.Height - bottom));

                if (_scrollAmount > 0 && !_moveViewUp)
                {
                    _moveViewUp = true;
                    CenterViewInScroll(_activeview, scrlView, rect.Height, rect.Height);
                }
            }
        }

        private void KeyBoardDownNotification(NSNotification notification)
        {
            if (_moveViewUp)
            {
                CenterViewInScroll(_activeview, scrlView, 0, 0);
                _moveViewUp = false;
            }
        }

        // todo deep avoid weird abbreviations, I got no idea what that means
        // is it GetEditAccountNumbersLayoutHeightConstraint ?
        private void GeteditAccNoNsLayoutHeightConstraint()
        {
            foreach (var constraint in scrlView.Constraints)
            {
                if (constraint.FirstItem == lblSelectSavedVehicle)
                {
                    _nsLblSelectVehicleHeight = constraint;
                }
                else if (constraint.FirstItem == tblVehicles)
                {
                    _nsTblVehiclesHeight = constraint;
                }
                else if (constraint.FirstItem == lblVRMColor)
                {
                    _nsLblVRMColorHeight = constraint;
                }
                else if (constraint.FirstItem == lblVRMModal)
                {
                    _nsLblVRMModalHeight = constraint;
                }
                else if (constraint.FirstItem == lblVRMMake)
                {
                    _nsLblVRMMakeHeight = constraint;
                }
                else if (constraint.FirstItem == vehicleDetailsContainer)
                {
                    _nsVehicleDetailContainerHeight = constraint;
                }
                else
                {
                    _nsLayoutHeightConstraint = constraint;
                }
            }
        }

        private void AddNsLayoutConstraintsToeditAccNo()
        {
            var editAccNoheight = NSLayoutConstraint.Create(editAccNo, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, _textFieldHeight);
            var lblSelectVehicleHeight = NSLayoutConstraint.Create(lblSelectSavedVehicle, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 15);
            var tblSelectedVehicleHeight = NSLayoutConstraint.Create(tblVehicles, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 20);
            var lblVRMColorHeight = NSLayoutConstraint.Create(lblVRMColor, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 15);
            var lblVRMModalHeight = NSLayoutConstraint.Create(lblVRMModal, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 15);
            var lblVRMMakeHeight = NSLayoutConstraint.Create(lblVRMMake, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 15);
            var vehicleDetailContainerHeight = NSLayoutConstraint.Create(vehicleDetailsContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 15);
            scrlView.AddConstraints(new[]
            {
                editAccNoheight, lblSelectVehicleHeight, tblSelectedVehicleHeight, lblVRMColorHeight, lblVRMModalHeight,
                lblVRMMakeHeight, vehicleDetailContainerHeight
            });
        }

        // todo deep casing: ChangeSave
        private void ChangesavedVehicleContainerViewHeight()
        {
            _nsTblVehiclesHeight.Constant = tblVehicles.ContentSize.Height;
        }

        private void BindButtons()
        {
            btnCheckVrm.SetCommand(Events.TouchUpInside, ViewModel.LookupVRMCommand);
            // todo IOS
            //btnChooseDates.SetCommand(Events.TouchUpInside, ViewModel.NextCommand);
        }

        private void SetFonts()
        {
            btnChooseDates.SetBook();
            btnCheckVrm.SetBook();
            lblCCZone.SetBook(true);
            lblVRMColor.SetBook();
            lblVRMModal.SetBook();
            lblVRMMake.SetBook();
            lblSaveVehicle.SetMedium(true);
            lblWhichVehicle.SetMedium(true);
            lblSaveDetails.SetBook(true);
            editAccNo.SetBook();
            editVRM.SetBook();
            lblUKReg.SetBook(true);
            lblConfirmVRM.SetMediumBold(true);
            lblSelectSavedVehicle.SetMedium(true);
        }

        private void SetKeyboardDismissal()
        {
            editVRM.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.Default);
            editAccNo.UiSetKeyboardEditorWithCloseButton(UIKeyboardType.NumberPad);

            editVRM.ReturnKeyType = UIReturnKeyType.Done;
            editVRM.AutocorrectionType = UITextAutocorrectionType.No;
            editAccNo.AutocorrectionType = UITextAutocorrectionType.No;
            editAccNo.ReturnKeyType = UIReturnKeyType.Done;

            editVRM.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };

            editAccNo.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };
        }

        private void SetVehicleDetails()
        {
            if (string.IsNullOrEmpty(ViewModel.Vehicle?.Color))
            {
                lblVRMColor.Text = string.Empty;
                lblVRMColor.Hidden = true;
            }
            else
            {
                lblVRMColor.Hidden = false;
                lblVRMColor.Text = ViewModel.Vehicle?.Color + " ";
            }

            lblVRMModal.Text = ViewModel.Vehicle?.Model;
            lblVRMMake.Text = ViewModel.Vehicle?.Make;
        }

        private void PerformDataBind()
        {
            Bindings.Add(this.SetBinding(() => ViewModel.EnteredVrm, () => editVRM.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.VehicleDetails, BindingMode.TwoWay).WhenSourceChanges(SetVehicleDetails));

            Bindings.Add(this.SetBinding(() => editVRM.Text)
                .UpdateSourceTrigger(Events.EditingChanged)
               .WhenSourceChanges(ResetAllControls));

            Bindings.Add(this.SetBinding(() => ViewModel.ConfirmVrmIsCorrect)
              .WhenSourceChanges(() =>
              {
                  swchConfirm.On = ViewModel.ConfirmVrmIsCorrect;
                  EvaluateButtonColors();
              }));

            swchConfirm.ValueChanged += (sender, args) =>
            {
                ViewModel.ConfirmVrmIsCorrect = swchConfirm.On;
                EvaluateButtonColors();
            };

            Bindings.Add(this.SetBinding(() => ViewModel.VehicleDetails, BindingMode.TwoWay)
               .WhenSourceChanges(
                   () => InvokeOnMainThread(EvaluateButtonColors)));

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
               {
                   editVRM.ResignFirstResponder();
                   editAccNo.ResignFirstResponder();
               }));

            Bindings.Add(this.SetBinding(() => swchSaveVrm.On)
               .UpdateSourceTrigger(Events.TouchUpInside)
               .WhenSourceChanges(() =>
               {
                   ViewModel.SaveVehicle = swchSaveVrm.On;
               }));
        }

        private void ResetAllControls()
        {
            ViewModel.EnteredVrm = editVRM.Text;
            swchConfirm.On = false;
            ViewModel.ConfirmVrmIsCorrect = swchConfirm.On;
            EvaluateButtonColors();
        }

        private void EvaluateButtonColors()
        {
            ChangeBtnColor(btnCheckVrm, !string.IsNullOrEmpty(ViewModel.EnteredVrm));
        }

        private void SetAutoresizingMaskToAllViews()
        {
            scrlView.TranslatesAutoresizingMaskIntoConstraints = false;
            lblWhichVehicle.TranslatesAutoresizingMaskIntoConstraints = false;
            editVRM.TranslatesAutoresizingMaskIntoConstraints = false;
            swchUKReg.TranslatesAutoresizingMaskIntoConstraints = false;
            lblUKReg.TranslatesAutoresizingMaskIntoConstraints = false;
            btnCheckVrm.TranslatesAutoresizingMaskIntoConstraints = false;
            swchConfirm.TranslatesAutoresizingMaskIntoConstraints = false;
            lblConfirmVRM.TranslatesAutoresizingMaskIntoConstraints = false;
            vehicleDetailsContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            lblVRMColor.TranslatesAutoresizingMaskIntoConstraints = false;
            lblVRMModal.TranslatesAutoresizingMaskIntoConstraints = false;
            lblVRMMake.TranslatesAutoresizingMaskIntoConstraints = false;
            savedVehicleContainerView.TranslatesAutoresizingMaskIntoConstraints = false;
            lblSelectSavedVehicle.TranslatesAutoresizingMaskIntoConstraints = false;
            tblVehicles.TranslatesAutoresizingMaskIntoConstraints = false;
            swchCCZone.TranslatesAutoresizingMaskIntoConstraints = false;
            lblCCZone.TranslatesAutoresizingMaskIntoConstraints = false;
            editAccNo.TranslatesAutoresizingMaskIntoConstraints = false;
            swchSaveVrm.TranslatesAutoresizingMaskIntoConstraints = false;
            lblSaveDetails.TranslatesAutoresizingMaskIntoConstraints = false;
            btnChooseDates.TranslatesAutoresizingMaskIntoConstraints = false;
        }

        private void InitUi()
        {
            SetAutoresizingMaskToAllViews();
            View.RemoveConstraints(View.Constraints);
            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            scrlView.RemoveConstraints(scrlView.Constraints);
            vehicleDetailsContainer.RemoveConstraints(vehicleDetailsContainer.Constraints);
            View.AddConstraints(
                scrlView.AtTopOf(View, UiConstants.VerticalMarginFromNc),
                scrlView.WithSameLeft(View),
                scrlView.WithSameRight(View),
                scrlView.WithSameBottom(View),
                lblWhichVehicle.AtTopOf(scrlView, UiConstants.TitleMargin),
                lblWhichVehicle.WithSameLeft(View).Plus(_leftMargin),
                lblWhichVehicle.WithSameRight(View).Minus(_leftMargin),
                editVRM.Below(lblWhichVehicle, _smallMargin),
                editVRM.WithSameLeft(lblWhichVehicle),
                editVRM.WithSameRight(lblWhichVehicle),
                editVRM.Height().EqualTo(_textFieldHeight),
                swchUKReg.Below(editVRM, _verticalMargin),
                swchUKReg.WithSameLeft(editVRM),
                swchUKReg.ToLeftOf(lblUKReg, _verticalMargin),
                swchUKReg.WithSameCenterY(lblUKReg),
                swchUKReg.Height().EqualTo(_textFieldHeight),
                lblUKReg.WithSameTop(swchUKReg),
                lblUKReg.Height().EqualTo(_textFieldHeight),
                btnCheckVrm.Below(lblUKReg, _verticalMargin),
                btnCheckVrm.WithSameLeft(editVRM),
                btnCheckVrm.WithSameRight(editVRM),
                btnCheckVrm.Height().EqualTo(_btnHeight),
                swchConfirm.Below(btnCheckVrm, _verticalMargin),
                swchConfirm.WithSameLeft(editVRM),
                swchConfirm.ToLeftOf(lblConfirmVRM, _verticalMargin),
                swchConfirm.WithSameCenterY(lblConfirmVRM),
                lblConfirmVRM.WithSameTop(swchConfirm),
                lblConfirmVRM.Height().EqualTo(_textFieldHeight),
                vehicleDetailsContainer.Below(swchConfirm, _smallMargin),
                vehicleDetailsContainer.WithSameLeft(editVRM),
                vehicleDetailsContainer.WithSameRight(editVRM),
                vehicleDetailsContainer.Height().EqualTo(60).SetPriority(500),
                lblVRMColor.AtTopOf(vehicleDetailsContainer),
                lblVRMColor.WithSameLeft(vehicleDetailsContainer),
                lblVRMModal.WithSameTop(lblVRMColor),
                lblVRMModal.ToRightOf(lblVRMColor),
                lblVRMMake.Below(lblVRMColor),
                lblVRMMake.WithSameLeft(lblVRMColor),
                lblVRMMake.WithSameRight(vehicleDetailsContainer),
                savedVehicleContainerView.Below(vehicleDetailsContainer, _smallMargin),
                savedVehicleContainerView.WithSameLeft(lblWhichVehicle),
                savedVehicleContainerView.WithSameRight(lblWhichVehicle),
                savedVehicleContainerView.Height().EqualTo(200).SetPriority(500),
                lblSelectSavedVehicle.AtTopOf(savedVehicleContainerView),
                lblSelectSavedVehicle.WithSameLeft(savedVehicleContainerView),
                lblSelectSavedVehicle.WithSameRight(savedVehicleContainerView),
                tblVehicles.Below(lblSelectSavedVehicle),
                tblVehicles.WithSameLeft(savedVehicleContainerView),
                tblVehicles.WithSameRight(savedVehicleContainerView),
                tblVehicles.WithSameBottom(savedVehicleContainerView),
                swchCCZone.Below(savedVehicleContainerView, _verticalMargin),
                swchCCZone.WithSameLeft(editVRM),
                swchCCZone.ToLeftOf(lblCCZone, _verticalMargin),
                swchCCZone.WithSameCenterY(lblCCZone),
                lblCCZone.WithSameTop(swchCCZone),
                lblCCZone.Height().EqualTo(_textFieldHeight),
                editAccNo.Below(lblCCZone, _verticalMargin),
                editAccNo.WithSameRight(editVRM),
                editAccNo.WithSameLeft(editVRM),
                swchSaveVrm.Below(editAccNo, _verticalMargin / 2),
                swchSaveVrm.WithSameLeft(editVRM),
                swchSaveVrm.ToLeftOf(lblSaveDetails, _verticalMargin),
                swchSaveVrm.WithSameCenterY(lblSaveDetails),
                lblSaveDetails.WithSameTop(swchSaveVrm),
                lblSaveDetails.Height().EqualTo(_textFieldHeight),
                btnChooseDates.Below(lblSaveDetails, _verticalMargin),
                btnChooseDates.WithSameLeft(editVRM),
                btnChooseDates.WithSameRight(editVRM),
                btnChooseDates.Height().EqualTo(_btnHeight));

            tblVehicles.ReloadData();
            tblVehicles.SectionFooterHeight = 0;
            tblVehicles.TableFooterView = null;
            tblVehicles.TableHeaderView = null;
            tblVehicles.ContentInset = new UIEdgeInsets(0, -8, 0, 0);
            AutomaticallyAdjustsScrollViewInsets = false;
            btnChooseDates.AddBottomBorder();
            btnCheckVrm.AddBottomBorder();
        }

        private void ChangeConstraintsWhenNoSavedVehicle()
        {
            _nsLblSelectVehicleHeight.Constant = 0;
            _nsTblVehiclesHeight.Constant = 0;
            _nsLayoutHeightConstraint.Constant = 0;
        }
    }
}