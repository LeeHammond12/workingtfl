﻿using CoreGraphics;
using System;
using System.Linq;
using Foundation;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class YourVehiclesAndServicesViewController : BaseViewController<LoggedInAccountViewModel>
    {
        public YourVehiclesAndServicesViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            if (ViewModel.LoggedInUser.VehiclesWithServices?.Any() == true)
            {
                TableViewVehicleWithServicesHeight.Constant = TableViewVehicleWithServices.ContentSize.Height - 56;
            }
            TableViewVehicleWithoutServicesHeight.Constant = TableViewVehicleWithoutServices.ContentSize.Height;
            ViewContainer.LayoutIfNeeded();
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, ViewContainer.Frame.Height);
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.YourVehiclesAndServices);
            LabelPageDescription1.Text = Strings.VehiclesAndServicesDescription1;
            LabelPageDescription2.Text = Strings.VehiclesAndServicesDescription2;
            LabelPageDescription4.SetNormalTextBoldStyle();
            LabelPageDescription4.Text = Strings.VehiclesAndServicesDescription4;
            LabelPageDescription3.UserInteractionEnabled = true;
            var attributedString = new NSMutableAttributedString(Strings.VehiclesAndServicesDescription3);
            var linkRange = new NSRange(0, Strings.VehiclesAndServicesDescription3.Length);
            var stringAttributes = new UIStringAttributes
            {
                ForegroundColor = UiConstants.BlueTextColor,
                UnderlineStyle = NSUnderlineStyle.Single,
                Font = UIFont.FromName("NJFont-Book", UiConstants.DefaultTextFontSize)
            };
            attributedString.SetAttributes(stringAttributes, linkRange);
            LabelPageDescription3.AttributedText = attributedString;
            var tab = new UITapGestureRecognizer(() =>
            {
                var nsurl = new NSUrl("https://" + Strings.VehiclesAndServicesDescription3);
                UIApplication.SharedApplication.OpenUrl(nsurl);
            });
            LabelPageDescription3.AddGestureRecognizer(tab);

            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;

            await ViewModel.InitViewModel();

            SetGroupedTableViewStyle(TableViewVehicleWithServices);
            SetTableViewStyle(TableViewVehicleWithoutServices);
            BindData();
        }

        private void BindData()
        {
            var vehiclesWithService = ViewModel.LoggedInUser.VehiclesWithServices.ToList();
            TableViewVehicleWithServices.Source = new YourVehiclesWithServicesTableViewSource(vehiclesWithService, v => ViewModel.RemoveVehicle(v.Vrm));

            var vehiclesWithoutService = ViewModel.LoggedInUser.VehiclesWithoutServices.ToList();
            TableViewVehicleWithoutServices.Source = new YourVehiclesWithoutServicesTableViewSource(vehiclesWithoutService);
            SetTextFieldText();
        }

        private void SetTextFieldText()
        {
            if (ViewModel.LoggedInUser.VehiclesWithServices?.Any() == true)
            {
                return;
            }

            var label = new UILabel(new CGRect(0, 0, TableViewVehicleWithServices.Bounds.Size.Width, TableViewVehicleWithServices.Bounds.Size.Height))
            {
                Text = Strings.NoVehicleWithServices,
                TextAlignment = UITextAlignment.Left
            };
            label.SetNormalTextStyle();
            TableViewVehicleWithServices.BackgroundView = label;
        }

        private void SetGroupedTableViewStyle(UITableView tableView)
        {
            tableView.RegisterNibForCellReuse(YourVehicleAndServiceTableViewCellHeader.Nib, YourVehicleAndServiceTableViewCellHeader.Key);
            tableView.RegisterNibForCellReuse(YourVehicleAndServiceTableViewCell.Nib, YourVehicleAndServiceTableViewCell.Key);
            tableView.RowHeight = UITableView.AutomaticDimension;
            tableView.EstimatedRowHeight = 140f;
            tableView.ReloadData();
            tableView.SectionFooterHeight = 1;
            tableView.TableFooterView = new UIView(CGRect.Empty);
            tableView.TableHeaderView = new UIView(CGRect.Empty);
            tableView.ContentInset = new UIEdgeInsets(-36, 0, 0, 0);
            tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }

        private void SetTableViewStyle(UITableView tableView)
        {
            tableView.RegisterNibForCellReuse(VehicleDetailsTableViewCell.Nib, VehicleDetailsTableViewCell.Key);
            tableView.RowHeight = UITableView.AutomaticDimension;
            tableView.EstimatedRowHeight = 140f;
            tableView.ReloadData();
            tableView.TableFooterView = new UIView(CGRect.Empty);
            tableView.TableHeaderView = new UIView(CGRect.Empty);
            tableView.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }
    }
}