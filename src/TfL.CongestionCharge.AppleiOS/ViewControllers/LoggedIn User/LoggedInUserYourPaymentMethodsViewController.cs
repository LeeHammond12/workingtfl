﻿using CoreGraphics;
using Foundation;
using System;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class LoggedInUserYourPaymentMethodsViewController : BaseViewController<LoggedInAccountViewModel>
    {
        public LoggedInUserYourPaymentMethodsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewYourCreditCardsHeight.Constant = TableViewYourCreditCards.ContentSize.Height;
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, ViewContainer.Frame.Height);
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.ManageStoredCreditCards);
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;

            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            SetTableViewStyle();
            BindData();
        }

        private void BindData()
        {
            TableViewYourCreditCards.Source = new YourCardsTableViewSource(() => ViewModel.UserInformation.PaymentCards, RemoveCard);
            SetTextFieldText();
        }

        private async void RemoveCard(PaymentCardViewModel card)
        {
            await ViewModel.DeletePaymentCard(card);
            TableViewYourCreditCards.ReloadData();
            SetTextFieldText();
        }

        private void SetTextFieldText()
        {
            if (ViewModel.UserInformation.PaymentCards.Count == 0)
            {
                LabelPageDescription.Text = Strings.NoPaymentCardsOnAnonAccount;
                TableViewYourCreditCardsTop.Constant = 0;
            }
        }

        private void SetTableViewStyle()
        {
            TableViewYourCreditCards.RegisterNibForCellReuse(YourCardTableViewCell.Nib, YourCardTableViewCell.Key);
            TableViewYourCreditCards.RowHeight = UITableView.AutomaticDimension;
            TableViewYourCreditCards.EstimatedRowHeight = 140f;
            TableViewYourCreditCards.ReloadData();
            TableViewYourCreditCards.TableFooterView = new UIView(CGRect.Empty);
            TableViewYourCreditCards.TableHeaderView = new UIView(CGRect.Empty);
            TableViewYourCreditCards.BackgroundColor = UIColor.White;
            TableViewYourCreditCards.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            TableViewYourCreditCards.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }
    }
}