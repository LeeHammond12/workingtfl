﻿using System;
using CoreGraphics;
using Foundation;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class AutoPayPaymentCardViewController : BaseViewController<AutoPayPaymentCardViewModel>
    {
        public AutoPayPaymentCardViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewHeight.Constant = TableViewCards.ContentSize.Height;
            TableViewCards.LayoutIfNeeded();
            TableViewCards.ReloadData();
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, ViewContainerDescription.Frame.Height);
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetTableViewStyle();
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UIColor.White;

            ViewContainer.Hidden = true;

            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            SetUpNavigationBar(ViewModel.ToolbarText);

            if (ViewModel.ShowDirectDebitScreen)
            {
                LabelPageHeader.Text = Strings.DirectDebitDetails;
                LabelDirectDebitDesc1.Text = Strings.DirectDebitDescription1;

                LabelDirectDebitDesc2.UserInteractionEnabled = true;
                var attributedString = new NSMutableAttributedString(Strings.DirectDebitDescription2);
                var linkRange = new NSRange(Strings.DirectDebitDescription2.Length - Strings.DirectDebitUrl.Length, Strings.DirectDebitUrl.Length);
                var stringAttributes = new UIStringAttributes
                {
                    ForegroundColor = UiConstants.BlueTextColor,
                    UnderlineStyle = NSUnderlineStyle.Single
                };
                attributedString.SetAttributes(stringAttributes, linkRange);
                LabelDirectDebitDesc2.AttributedText = attributedString;
                var tab = new UITapGestureRecognizer(() =>
                {
                    var nsurl = new NSUrl("https://" + Strings.DirectDebitUrl);
                    UIApplication.SharedApplication.OpenUrl(nsurl);
                });
                LabelDirectDebitDesc2.AddGestureRecognizer(tab);

                TableViewCards.Hidden = true;
                ScrollView.Hidden = true;
                ScrollViewHeight.Constant = 0;
                TableViewHeight.Constant = 0;
                ViewContainerDescription.Hidden = true;
                ViewContainer.Hidden = false;
            }
            else
            {
                TableViewCards.Hidden = false;
                ScrollView.Hidden = false;
                ViewContainerDescription.Hidden = false;
                LabelPageHeader.Text = Strings.CardRegistered;
                LabelDirectDebitDesc1.Text = string.Empty;
                LabelDirectDebitDesc2.Text = string.Empty;
                FinishViewDidLoad();
            }
        }

        private void FinishViewDidLoad()
        {
            var source = new AutoCcPayCreditCardTableViewSource(
                ViewModel.PaymentCards,
                c => ViewModel.SetAutoPayPaymentCard(c));

            TableViewCards.Source = source;
            LabelAutoPayCreditCardDescription4.UserInteractionEnabled = true;
            var attributedString = new NSMutableAttributedString(Strings.AutoPayCreditCardDescription4);
            var linkRange = new NSRange(Strings.AutoPayCreditCardDescription4.Length - Strings.AutoPayCreditCardUrl.Length, Strings.AutoPayCreditCardUrl.Length);
            var stringAttributes = new UIStringAttributes
            {
                ForegroundColor = UiConstants.BlueTextColor,
                UnderlineStyle = NSUnderlineStyle.Single
            };
            attributedString.SetAttributes(stringAttributes, linkRange);
            LabelAutoPayCreditCardDescription4.AttributedText = attributedString;
            var tab = new UITapGestureRecognizer(() =>
            {
                var nsurl = new NSUrl("https://" + Strings.AutoPayCreditCardUrl);
                UIApplication.SharedApplication.OpenUrl(nsurl);
            });
            LabelAutoPayCreditCardDescription4.AddGestureRecognizer(tab);
            ViewContainer.Hidden = false;
        }

        private void SetTableViewStyle()
        {
            TableViewCards.RegisterNibForCellReuse(AutoCcPayCreditCardTableViewCell.Nib, AutoCcPayCreditCardTableViewCell.Key);
            TableViewCards.RowHeight = UITableView.AutomaticDimension;
            TableViewCards.EstimatedRowHeight = 140f;
            TableViewCards.ReloadData();
            TableViewCards.TableFooterView = new UIView(CGRect.Empty);
            TableViewCards.TableHeaderView = new UIView(CGRect.Empty);
            TableViewCards.BackgroundColor = UIColor.White;
            TableViewCards.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            TableViewCards.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            SetTextFieldText();
        }

        private void SetTextFieldText()
        {
            if (ViewModel.UserInformation.PaymentCards.Count == 0)
            {
                TableViewCards.BackgroundView = new UILabel(new CGRect(0, 0, TableViewCards.Bounds.Size.Width, TableViewCards.Bounds.Size.Height))
                {
                    Text = "Cards not available",
                    TextAlignment = UITextAlignment.Center,
                    TextColor = UiConstants.PageSubHeaderLabelColor,
                    Font = UiConstants.GetFontFromSize(UiConstants.DefaultTextFontSize)
                };
            }
        }

    }
}