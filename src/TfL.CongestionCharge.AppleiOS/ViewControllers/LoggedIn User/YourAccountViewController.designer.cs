// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("YourAccountViewController")]
    partial class YourAccountViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonArrowNextYourVehicle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonPrivacyPolicy { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonTermAndCondition { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonYourVehicle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAccountHolderName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAccountNo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAccountNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAdd { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAddress { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonArrowNextYourVehicle != null) {
                ButtonArrowNextYourVehicle.Dispose ();
                ButtonArrowNextYourVehicle = null;
            }

            if (ButtonPrivacyPolicy != null) {
                ButtonPrivacyPolicy.Dispose ();
                ButtonPrivacyPolicy = null;
            }

            if (ButtonTermAndCondition != null) {
                ButtonTermAndCondition.Dispose ();
                ButtonTermAndCondition = null;
            }

            if (ButtonYourVehicle != null) {
                ButtonYourVehicle.Dispose ();
                ButtonYourVehicle = null;
            }

            if (LabelAccountHolderName != null) {
                LabelAccountHolderName.Dispose ();
                LabelAccountHolderName = null;
            }

            if (LabelAccountNo != null) {
                LabelAccountNo.Dispose ();
                LabelAccountNo = null;
            }

            if (LabelAccountNumber != null) {
                LabelAccountNumber.Dispose ();
                LabelAccountNumber = null;
            }

            if (LabelAdd != null) {
                LabelAdd.Dispose ();
                LabelAdd = null;
            }

            if (LabelAddress != null) {
                LabelAddress.Dispose ();
                LabelAddress = null;
            }

            if (LabelName != null) {
                LabelName.Dispose ();
                LabelName = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}