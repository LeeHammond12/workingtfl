// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("LoggedInUserYourPaymentMethodsViewController")]
    partial class LoggedInUserYourPaymentMethodsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelPageDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableViewYourCreditCards { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewYourCreditCardsHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewYourCreditCardsTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (LabelPageDescription != null) {
                LabelPageDescription.Dispose ();
                LabelPageDescription = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (TableViewYourCreditCards != null) {
                TableViewYourCreditCards.Dispose ();
                TableViewYourCreditCards = null;
            }

            if (TableViewYourCreditCardsHeight != null) {
                TableViewYourCreditCardsHeight.Dispose ();
                TableViewYourCreditCardsHeight = null;
            }

            if (TableViewYourCreditCardsTop != null) {
                TableViewYourCreditCardsTop.Dispose ();
                TableViewYourCreditCardsTop = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}