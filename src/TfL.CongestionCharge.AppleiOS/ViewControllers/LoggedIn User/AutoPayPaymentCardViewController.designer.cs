// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("AutoPayPaymentCardViewController")]
    partial class AutoPayPaymentCardViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAutoPayCreditCardDescription1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAutoPayCreditCardDescription2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAutoPayCreditCardDescription3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAutoPayCreditCardDescription4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelDirectDebitDesc1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelDirectDebitDesc2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ScrollViewHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableViewCards { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainerDescription { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (LabelAutoPayCreditCardDescription1 != null) {
                LabelAutoPayCreditCardDescription1.Dispose ();
                LabelAutoPayCreditCardDescription1 = null;
            }

            if (LabelAutoPayCreditCardDescription2 != null) {
                LabelAutoPayCreditCardDescription2.Dispose ();
                LabelAutoPayCreditCardDescription2 = null;
            }

            if (LabelAutoPayCreditCardDescription3 != null) {
                LabelAutoPayCreditCardDescription3.Dispose ();
                LabelAutoPayCreditCardDescription3 = null;
            }

            if (LabelAutoPayCreditCardDescription4 != null) {
                LabelAutoPayCreditCardDescription4.Dispose ();
                LabelAutoPayCreditCardDescription4 = null;
            }

            if (LabelDirectDebitDesc1 != null) {
                LabelDirectDebitDesc1.Dispose ();
                LabelDirectDebitDesc1 = null;
            }

            if (LabelDirectDebitDesc2 != null) {
                LabelDirectDebitDesc2.Dispose ();
                LabelDirectDebitDesc2 = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (ScrollViewHeight != null) {
                ScrollViewHeight.Dispose ();
                ScrollViewHeight = null;
            }

            if (TableViewCards != null) {
                TableViewCards.Dispose ();
                TableViewCards = null;
            }

            if (TableViewHeight != null) {
                TableViewHeight.Dispose ();
                TableViewHeight = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }

            if (ViewContainerDescription != null) {
                ViewContainerDescription.Dispose ();
                ViewContainerDescription = null;
            }
        }
    }
}