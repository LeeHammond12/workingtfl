﻿using System;
using System.Linq;
using CoreGraphics;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class AutoPayVehiclesViewController : BaseViewController<AutoPayVehiclesViewModel>
    {
        public AutoPayVehiclesViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewActiveVehiclesHeight.Constant = 300;
            TableViewActiveVehicles.ReloadData();
            TableViewActiveVehicles.LayoutIfNeeded();

            TableViewInactiveVehiclesHeight.Constant = 300;
            TableViewInactiveVehicles.ReloadData();
            TableViewInactiveVehicles.LayoutIfNeeded();
            if (ViewModel.ActiveVehicles?.Any() == true)
            {
                TableViewActiveVehiclesHeight.Constant = TableViewActiveVehicles.ContentSize.Height;
            }
            else
            {
                TableViewActiveVehiclesHeight.Constant = 0;
            }

            if (ViewModel.InactiveVehicles?.Any() == true)
            {
                TableViewInactiveVehiclesHeight.Constant = TableViewInactiveVehicles.ContentSize.Height;
            }
            else
            {
                TableViewInactiveVehiclesHeight.Constant = 0;
            }

            ScrollView.LayoutIfNeeded();
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, ViewContainer.Frame.Height);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            FinishViewDidLoad();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.ManageCCAutoPay);

            SetTableViewStyle(TableViewActiveVehicles);
            SetTableViewStyle(TableViewInactiveVehicles);
        }

        private async void FinishViewDidLoad()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            TableViewActiveVehicles.TableHeaderView =
                TableViewHeaderView("active", Strings.VehiclesActiveOnAutoPay);

            TableViewInactiveVehicles.TableHeaderView =
                TableViewHeaderView("inactive", Strings.VehiclesInactiveOnAutoPay);

            InitLists();

            ButtonAddNewVehicle.SetCommand(Events.TouchUpInside, ViewModel.NavigateToAddAnotherVehicleCommand);
        }

        private async void RemoveVehicle(VehicleViewModel vehicleViewModel)
        {
            await ViewModel.RemoveVehicleFromAutoPay(vehicleViewModel);
            InitLists();
        }

        private async void AddVehicle(VehicleViewModel vehicleViewModel)
        {
            await ViewModel.AddVehicleToAutoPay(vehicleViewModel);
            InitLists();
        }

        private void InitLists()
        {
            TableViewActiveVehicles.Source = new AutoCcPayVehiclesTableViewSource(ViewModel.ActiveVehicles, RemoveVehicle, "remove");
            TableViewInactiveVehicles.Source = new AutoCcPayVehiclesTableViewSource(ViewModel.InactiveVehicles, AddVehicle, "add");
        }

        private void SetTableViewStyle(UITableView tableView)
        {
            tableView.RegisterNibForCellReuse(AutoCcPayVehicleTableViewCell.Nib, AutoCcPayVehicleTableViewCell.Key);
            tableView.RowHeight = UITableView.AutomaticDimension;
            tableView.EstimatedRowHeight = 140f;
            tableView.ReloadData();
            tableView.TableFooterView = new UIView(CGRect.Empty);
            tableView.TableHeaderView = new UIView(CGRect.Empty);
            tableView.BackgroundColor = UIColor.White;
            tableView.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }

        private UIView TableViewHeaderView(string iconName, string title)
        {
            var headerView = new UIView(new CGRect(0, 0, TableViewInactiveVehicles.Frame.Width, 46))
            {
                BackgroundColor = UiConstants.PageHeaderLabelColor
            };
            var icon = new UIImageView(new CGRect(8, 8, 30, 30))
            {
                Image = UIImage.FromBundle(iconName)
            };

            var labelTitle = new UILabel(new CGRect(42, 12, headerView.Frame.Width - 60, 20));
            labelTitle.SetBoldWhiteTextStyle();
            labelTitle.Text = title;

            headerView.AddSubviews(icon, labelTitle);
            return headerView;
        }
    }
}