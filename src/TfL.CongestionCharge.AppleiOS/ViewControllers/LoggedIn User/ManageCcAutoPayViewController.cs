﻿using System;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Services;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class ManageCcAutoPayViewController : BaseViewController<ManageCcAutoPayViewModel>
    {
        public ManageCcAutoPayViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.ManageCCAutoPay);
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ViewContainer.BackgroundColor = UiConstants.ScreenBackgroundColor;

            LabelStatus.SetNormalBlueTextStyle(true);
            ButtonAutoPayCreditCard.SetGrayButtonStyle(UiConstants.BlueTextColor, true);
            ButtonAutoVehicles.SetGrayButtonStyle(UiConstants.BlueTextColor, true);
            ViewContainerAutoPayDisabled.Hidden = true;
            ViewContainerAutoPayEnabled.Hidden = true;
            FinishViewDidLoad();
        }

        private async void FinishViewDidLoad()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            var user = ViewModel.LoggedInUser;

            if (user.HasAutoPay)
            {
                ViewContainerStatus.SetStatusCardViewStyleGreen();
                ViewContainerAutoPayDisabled.Hidden = true;
                ViewContainerAutoPayEnabled.Hidden = false;
                EnabledHeight.Active = false;
                DisabledHeight.Constant = 0;

                LabelCurrentStatus.Text = ViewModel.AutoPayStatusDisplayText;

                LabelAmount.Text = user.Balance;
                LabelNextStatementDate.Text = user.AutoPayStatementDate.ToShortDateString();
                if (user.LastAutoPayStatementDate.HasValue)
                {
                    LabelLastStatementDate.Text = user.LastAutoPayStatementDate.Value.ToShortDateString();
                }
                else
                {
                    HideLastStatementUi();
                }

                LabelBalanceDesc1.Text = Strings.AutoPayServiceActiveDescription1;
                LabelBalanceDesc3.Text = string.Empty;
                LabelBalanceDesc3.Hidden = true;
                Description3TopSpaceConstraint.Constant = 0;

                var attributedString = new NSMutableAttributedString(Strings.AutoPayServiceActiveDescription2Part1);
                var attributedStringLast = new NSMutableAttributedString(Strings.AutoPayServiceActiveDescription2Part2);
                var attributedUrl = GetAttributedUrlFromText(Strings.AutoPayServiceActiveUrl);
                attributedString.Append(attributedUrl);
                attributedString.Append(attributedStringLast);
                LabelBalanceDesc2.AttributedText = attributedString;
                MakeLabelClickable(LabelBalanceDesc2, Strings.AutoPayServiceActiveUrl);

                ButtonAutoPayCreditCard.SetCommand(Events.TouchUpInside, ViewModel.NavigateToAutoPayPaymentCardCommand);
                ButtonAutoVehicles.SetCommand(Events.TouchUpInside, ViewModel.NavigateToAutoPayVehiclesCommand);
                ButtonAutoPayCreditCardArrow.SetCommand(Events.TouchUpInside, ViewModel.NavigateToAutoPayPaymentCardCommand);
                ButtonAutoVehiclesArrow.SetCommand(Events.TouchUpInside, ViewModel.NavigateToAutoPayVehiclesCommand);
                ImageViewTicIcon.Image = GetAutoPayStatusImage(ViewModel.AutoPayStatus);

                if (user.AutoPayStatus != AutoPayStatus.Active)
                {
                    ViewContainerStatus.SetStatusCardViewStyleRed();

                    //HideLastStatementUi();

                    //LabelNextStatement.Text = string.Empty;
                    //LabelNextStatement.Hidden = true;
                    //LabelNextStatementDate.Text = string.Empty;
                    //LabelNextStatementDate.Hidden = true;
                    //NextStatementTopSpaceConstraint.Constant = 0;
                    //NextStatementBottomSpaceConstraint.Constant = 0;
                    //StatusViewBottomSpaceConstraint.Constant = 0;
                    //LastStatementTopSpaceConstraint.Constant = 0;
                }
            }
            else
            {
                var attributedString = new NSMutableAttributedString(Strings.AutoCcPayLink);
                var attributedUrl = GetAttributedUrlFromText(Strings.AutoCcPayUrl);
                attributedString.Append(attributedUrl);
                LabelSignIn.AttributedText = attributedString;
                MakeLabelClickable(LabelSignIn, Strings.AutoCcPayUrl);

                ViewContainerAutoPayEnabled.Hidden = true;
                ViewContainerAutoPayDisabled.Hidden = false;
                EnabledHeight.Constant = 0;
                DisabledHeight.Active = false;
            }
        }

        private static void MakeLabelClickable(UIView view, string url)
        {
            view.UserInteractionEnabled = true;
            var tab = new UITapGestureRecognizer(() =>
            {
                var nsurl = new NSUrl("https://" + url);
                UIApplication.SharedApplication.OpenUrl(nsurl);
            });
            view.AddGestureRecognizer(tab);
        }

        private static NSMutableAttributedString GetAttributedUrlFromText(string url)
        {
            var attributedUrl = new NSMutableAttributedString(url);
            var stringAttributes = new UIStringAttributes
            {
                ForegroundColor = UiConstants.BlueTextColor,
                UnderlineStyle = NSUnderlineStyle.Single,
                Font = UIFont.FromName("NJFont-Book", UiConstants.DefaultTextFontSize)
            };
            attributedUrl.SetAttributes(stringAttributes, new NSRange(0, attributedUrl.Length));
            return attributedUrl;
        }

        private void HideLastStatementUi()
        {
            LabelLastStatement.Text = string.Empty;
            LabelLastStatement.Hidden = true;
            LabelLastStatementDate.Text = string.Empty;
            LabelLastStatementDate.Hidden = true;
        }

        private UIImage GetAutoPayStatusImage(AutoPayStatus autoPayStatus)
        {
            switch (autoPayStatus)
            {
                case AutoPayStatus.Active:
                    return UIImage.FromBundle("active");

                case AutoPayStatus.Pending:
                case AutoPayStatus.PendingSuspension:
                    return UIImage.FromBundle("pending");

                case AutoPayStatus.Closed:
                case AutoPayStatus.Suspended:
                    return UIImage.FromBundle("locked");

                default:
                    return UIImage.FromBundle("locked");
            }
        }
    }
}