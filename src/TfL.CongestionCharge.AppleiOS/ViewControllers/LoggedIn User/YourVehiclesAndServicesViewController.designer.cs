// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("YourVehiclesAndServicesViewController")]
    partial class YourVehiclesAndServicesViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelPageDescription1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelPageDescription2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelPageDescription3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelPageDescription4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableViewVehicleWithoutServices { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewVehicleWithoutServicesHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewVehicleWithoutServicesTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableViewVehicleWithServices { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewVehicleWithServicesHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewVehicleWithServicesTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (LabelPageDescription1 != null) {
                LabelPageDescription1.Dispose ();
                LabelPageDescription1 = null;
            }

            if (LabelPageDescription2 != null) {
                LabelPageDescription2.Dispose ();
                LabelPageDescription2 = null;
            }

            if (LabelPageDescription3 != null) {
                LabelPageDescription3.Dispose ();
                LabelPageDescription3 = null;
            }

            if (LabelPageDescription4 != null) {
                LabelPageDescription4.Dispose ();
                LabelPageDescription4 = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (TableViewVehicleWithoutServices != null) {
                TableViewVehicleWithoutServices.Dispose ();
                TableViewVehicleWithoutServices = null;
            }

            if (TableViewVehicleWithoutServicesHeight != null) {
                TableViewVehicleWithoutServicesHeight.Dispose ();
                TableViewVehicleWithoutServicesHeight = null;
            }

            if (TableViewVehicleWithoutServicesTop != null) {
                TableViewVehicleWithoutServicesTop.Dispose ();
                TableViewVehicleWithoutServicesTop = null;
            }

            if (TableViewVehicleWithServices != null) {
                TableViewVehicleWithServices.Dispose ();
                TableViewVehicleWithServices = null;
            }

            if (TableViewVehicleWithServicesHeight != null) {
                TableViewVehicleWithServicesHeight.Dispose ();
                TableViewVehicleWithServicesHeight = null;
            }

            if (TableViewVehicleWithServicesTop != null) {
                TableViewVehicleWithServicesTop.Dispose ();
                TableViewVehicleWithServicesTop = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}