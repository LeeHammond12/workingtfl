// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("ManageCcAutoPayViewController")]
    partial class ManageCcAutoPayViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint AutoPayPaymentCardHeightConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint AutoPayVehiclesBottomSpaceConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint AutoPayVehiclesCardHeightConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint AutoPayVehiclesTopSpaceConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonAutoPayCreditCard { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonAutoPayCreditCardArrow { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonAutoVehicles { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonAutoVehiclesArrow { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint Description3TopSpaceConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint DisabledHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint EnabledHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewTicIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelBalance { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelBalanceDesc1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelBalanceDesc2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelBalanceDesc3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelCurrentStatus { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemColoredSubHeaderLabel LabelLastStatement { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelLastStatementDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ListItemColoredSubHeaderLabel LabelNextStatement { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelNextStatementDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelNoAutoCCpay { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelSignIn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelStatus { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelYourAutoPay { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LastStatementBottomSpaceConstaint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LastStatementTopSpaceConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint NextStatementBottomSpaceConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint NextStatementTopSpaceConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint StatusViewBottomSpaceConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainerAutoPayDisabled { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainerAutoPayEnabled { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainerStatus { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AutoPayPaymentCardHeightConstraint != null) {
                AutoPayPaymentCardHeightConstraint.Dispose ();
                AutoPayPaymentCardHeightConstraint = null;
            }

            if (AutoPayVehiclesBottomSpaceConstraint != null) {
                AutoPayVehiclesBottomSpaceConstraint.Dispose ();
                AutoPayVehiclesBottomSpaceConstraint = null;
            }

            if (AutoPayVehiclesCardHeightConstraint != null) {
                AutoPayVehiclesCardHeightConstraint.Dispose ();
                AutoPayVehiclesCardHeightConstraint = null;
            }

            if (AutoPayVehiclesTopSpaceConstraint != null) {
                AutoPayVehiclesTopSpaceConstraint.Dispose ();
                AutoPayVehiclesTopSpaceConstraint = null;
            }

            if (ButtonAutoPayCreditCard != null) {
                ButtonAutoPayCreditCard.Dispose ();
                ButtonAutoPayCreditCard = null;
            }

            if (ButtonAutoPayCreditCardArrow != null) {
                ButtonAutoPayCreditCardArrow.Dispose ();
                ButtonAutoPayCreditCardArrow = null;
            }

            if (ButtonAutoVehicles != null) {
                ButtonAutoVehicles.Dispose ();
                ButtonAutoVehicles = null;
            }

            if (ButtonAutoVehiclesArrow != null) {
                ButtonAutoVehiclesArrow.Dispose ();
                ButtonAutoVehiclesArrow = null;
            }

            if (Description3TopSpaceConstraint != null) {
                Description3TopSpaceConstraint.Dispose ();
                Description3TopSpaceConstraint = null;
            }

            if (DisabledHeight != null) {
                DisabledHeight.Dispose ();
                DisabledHeight = null;
            }

            if (EnabledHeight != null) {
                EnabledHeight.Dispose ();
                EnabledHeight = null;
            }

            if (ImageViewTicIcon != null) {
                ImageViewTicIcon.Dispose ();
                ImageViewTicIcon = null;
            }

            if (LabelAmount != null) {
                LabelAmount.Dispose ();
                LabelAmount = null;
            }

            if (LabelBalance != null) {
                LabelBalance.Dispose ();
                LabelBalance = null;
            }

            if (LabelBalanceDesc1 != null) {
                LabelBalanceDesc1.Dispose ();
                LabelBalanceDesc1 = null;
            }

            if (LabelBalanceDesc2 != null) {
                LabelBalanceDesc2.Dispose ();
                LabelBalanceDesc2 = null;
            }

            if (LabelBalanceDesc3 != null) {
                LabelBalanceDesc3.Dispose ();
                LabelBalanceDesc3 = null;
            }

            if (LabelCurrentStatus != null) {
                LabelCurrentStatus.Dispose ();
                LabelCurrentStatus = null;
            }

            if (LabelLastStatement != null) {
                LabelLastStatement.Dispose ();
                LabelLastStatement = null;
            }

            if (LabelLastStatementDate != null) {
                LabelLastStatementDate.Dispose ();
                LabelLastStatementDate = null;
            }

            if (LabelNextStatement != null) {
                LabelNextStatement.Dispose ();
                LabelNextStatement = null;
            }

            if (LabelNextStatementDate != null) {
                LabelNextStatementDate.Dispose ();
                LabelNextStatementDate = null;
            }

            if (LabelNoAutoCCpay != null) {
                LabelNoAutoCCpay.Dispose ();
                LabelNoAutoCCpay = null;
            }

            if (LabelSignIn != null) {
                LabelSignIn.Dispose ();
                LabelSignIn = null;
            }

            if (LabelStatus != null) {
                LabelStatus.Dispose ();
                LabelStatus = null;
            }

            if (LabelYourAutoPay != null) {
                LabelYourAutoPay.Dispose ();
                LabelYourAutoPay = null;
            }

            if (LastStatementBottomSpaceConstaint != null) {
                LastStatementBottomSpaceConstaint.Dispose ();
                LastStatementBottomSpaceConstaint = null;
            }

            if (LastStatementTopSpaceConstraint != null) {
                LastStatementTopSpaceConstraint.Dispose ();
                LastStatementTopSpaceConstraint = null;
            }

            if (NextStatementBottomSpaceConstraint != null) {
                NextStatementBottomSpaceConstraint.Dispose ();
                NextStatementBottomSpaceConstraint = null;
            }

            if (NextStatementTopSpaceConstraint != null) {
                NextStatementTopSpaceConstraint.Dispose ();
                NextStatementTopSpaceConstraint = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (StatusViewBottomSpaceConstraint != null) {
                StatusViewBottomSpaceConstraint.Dispose ();
                StatusViewBottomSpaceConstraint = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }

            if (ViewContainerAutoPayDisabled != null) {
                ViewContainerAutoPayDisabled.Dispose ();
                ViewContainerAutoPayDisabled = null;
            }

            if (ViewContainerAutoPayEnabled != null) {
                ViewContainerAutoPayEnabled.Dispose ();
                ViewContainerAutoPayEnabled = null;
            }

            if (ViewContainerStatus != null) {
                ViewContainerStatus.Dispose ();
                ViewContainerStatus = null;
            }
        }
    }
}