﻿using System;
using CoreGraphics;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class YourAccountViewController : BaseViewController<YourAccountViewModel>
    {
        public YourAccountViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, ViewContainer.Frame.Height);
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();

            SetButtonStyle();

            SetUpNavigationBar(Strings.YourAccount);
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;
            PopulateData();
            SetBindings();
        }

        private void SetButtonStyle()
        {
            ButtonYourVehicle.SetGrayButtonStyle(UiConstants.BlueTextColor, true);
        }

        private void SetBindings()
        {
            ButtonPrivacyPolicy.SetCommand(Events.TouchUpInside, ViewModel.NavigateToPrivacyCommand);
            ButtonTermAndCondition.SetCommand(Events.TouchUpInside, ViewModel.NavigateToTcCommand);
            ButtonYourVehicle.SetCommand(Events.TouchUpInside, ViewModel.NavigateToVehiclesAndServicesCommand);
            ButtonArrowNextYourVehicle.SetCommand(Events.TouchUpInside, ViewModel.NavigateToVehiclesAndServicesCommand);
        }

        private void PopulateData()
        {
            var user = ViewModel.LoggedInUser;

            LabelAccountHolderName.Text = user.Name;
            LabelAccountNo.Text = user.AccountNumber;
            LabelAdd.Text = user.Address;
        }
    }
}