// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("SelectPaymentCardViewController")]
    partial class SelectPaymentCardViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonAddAnotherCard { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners Container { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableViewCards { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewCardsHeight { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonAddAnotherCard != null) {
                ButtonAddAnotherCard.Dispose ();
                ButtonAddAnotherCard = null;
            }

            if (Container != null) {
                Container.Dispose ();
                Container = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (TableViewCards != null) {
                TableViewCards.Dispose ();
                TableViewCards = null;
            }

            if (TableViewCardsHeight != null) {
                TableViewCardsHeight.Dispose ();
                TableViewCardsHeight = null;
            }
        }
    }
}