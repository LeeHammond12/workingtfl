﻿using GalaSoft.MvvmLight.Helpers;
using System;
using CoreGraphics;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayPcn;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class PcnLookupViewController : BaseViewController<PcnLookupViewModel>
    {
        public PcnLookupViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, ViewContainer.Frame.Height);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(ViewModel.PageTitle, () => ViewModel.OnBackNavigationRequested());
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            SetTextFieldStyle();
            FinishViewDidLoad();
            new KeyboardHelper(View, IsViewLoaded, InterfaceOrientation);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            ViewModel.Reset();
        }

        private void SetTextFieldStyle()
        {
            TextFieldPcnNumber.SetTextStartLeftPaddign();
            TextFieldVehicleRegMark.SetTextStartLeftPaddign();
        }

        private async void FinishViewDidLoad()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            TextFieldBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.ButtonText, BindingMode.TwoWay)
                .WhenSourceChanges(() =>
                {
                    ButtonEnterAmount.SetTitle(ViewModel.ButtonText, UIControlState.Normal);
                }));

            ButtonEnterAmount.SetCommand(Events.TouchUpInside, ViewModel.LookupPcnOrGoToPaymentCommand);

            Bindings.Add(this.SetBinding(() => ViewModel.CanProceedToPayment, BindingMode.TwoWay)
                .WhenSourceChanges(() => HandleAmountYouOweVisibility()));

            Bindings.Add(this.SetBinding(() => ViewModel.CanLookupPcnOrGoToPayment, BindingMode.TwoWay)
                .WhenSourceChanges(() => ChangeButtonState()));

            HandleAmountYouOweVisibility();
        }

        private void ChangeButtonState()
        {
            ButtonEnterAmount.Enabled = ViewModel.CanLookupPcnOrGoToPayment;
            ButtonEnterAmount.Alpha = ViewModel.CanLookupPcnOrGoToPayment ? 1.0f : 0.5f;
        }

        private void HandleAmountYouOweVisibility()
        {
            var amountHidden = !ViewModel.CanProceedToPayment;
            ViewContainerAmount.Hidden = amountHidden;
            LabelAmountTop.Constant = amountHidden ? 0 : 12;
            LabelAmountBottom.Constant = amountHidden ? 0 : 12;
        }

        private void TextFieldBindings()
        {
            TextFieldPcnNumber.EditingChanged += (sender, args) =>
                ViewModel.PcnNumber = TextFieldPcnNumber.Text;

            Bindings.Add(this.SetBinding(() => ViewModel.PcnNumber, BindingMode.OneWay)
                .WhenSourceChanges(() => TextFieldPcnNumber.Text = ViewModel.PcnNumber));

            Bindings.Add(this.SetBinding(() => ViewModel.AmountYouOweString, BindingMode.OneWay)
                .WhenSourceChanges(() => LabelAmount.Text = ViewModel.AmountYouOweString));

            TextFieldVehicleRegMark.EditingChanged += (sender, args) =>
                ViewModel.VrmNumber = TextFieldVehicleRegMark.Text;

            Bindings.Add(this.SetBinding(() => ViewModel.VrmNumber, BindingMode.OneWay)
               .WhenSourceChanges(() => TextFieldVehicleRegMark.Text = ViewModel.VrmNumber));
        }
    }
}