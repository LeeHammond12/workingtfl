﻿using System;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class ConfirmVrmViewController : BaseViewController<ConfirmVrmViewModel>
    {
        public ConfirmVrmViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            LabelVrm.Text = Strings.VehicleRegistrationMark;
            LabelVrm.SetLightTextStyle();
            LabelCarMake.Text = Strings.CarMake;
            LabelCarMake.SetLightTextStyle();
            LabelCarmakeDetails.SetNormalTextStyle();
            LabelVrmDetails.SetNormalTextStyle();
            SepratorLine.BackgroundColor = UIColor.Gray;
            SwitchAndLAbelFirstContainer.LabelConfirmationDescription.Text = Strings.ConfirmThisVrmIsCorrect;
            SwitchAndLAbelSecondContainer.LabelConfirmationDescription.Text = Strings.SaveVehicleDetails;
            SwitchAndLAbelFirstContainer.LabelConfirmationDescription.Font = UiConstants.GetFontFromSize(UiConstants.SmallTextFontSize, true);
            SetUpNavigationBar(ViewModel.PageTitle);

            if (ViewModel.HasFlowState)
            {
                PerformDataBind();
            }
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            ViewModel?.GoBackIfNoFlowState();
        }

        private async void PerformDataBind()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            SwitchAndLAbelSecondContainer.Hidden = !ViewModel.ShowSaveCheckBox;
            SwitchAndLAbelSecondContainerHeight.Constant = ViewModel.ShowSaveCheckBox ? 42 : 0;
            SwitchAndLAbelFirstContainer.ConfirmSwitch.ValueChanged += (sender, args) =>
            {
                ViewModel.ConfirmVrmIsCorrect = SwitchAndLAbelFirstContainer.ConfirmSwitch.On;
            };
            SwitchAndLAbelSecondContainer.ConfirmSwitch.ValueChanged += (sender, args) =>
            {
                ViewModel.SaveVehicle = SwitchAndLAbelSecondContainer.ConfirmSwitch.On;
            };

            ButtonChooseYourDate.SetTitle(ViewModel.NextButtonText, UIControlState.Normal);
            ButtonChooseYourDate.SetCommand(Events.TouchUpInside, ViewModel.NextCommand);
            LabelVrmDetails.Text = ViewModel.Vehicle.Vrm;
            LabelCarmakeDetails.Text = ViewModel.VehicleDetails;
        }
    }
}