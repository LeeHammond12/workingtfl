// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("LookupVrmViewController")]
    partial class LookupVrmViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonNext { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel lblPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomSwitchAndLabelControl SwitchAndLabelFirstContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomSwitchAndLabelControl TempView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomTextField TextFieldVehicleRegistrationMark { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonNext != null) {
                ButtonNext.Dispose ();
                ButtonNext = null;
            }

            if (lblPageHeader != null) {
                lblPageHeader.Dispose ();
                lblPageHeader = null;
            }

            if (SwitchAndLabelFirstContainer != null) {
                SwitchAndLabelFirstContainer.Dispose ();
                SwitchAndLabelFirstContainer = null;
            }

            if (TempView != null) {
                TempView.Dispose ();
                TempView = null;
            }

            if (TextFieldVehicleRegistrationMark != null) {
                TextFieldVehicleRegistrationMark.Dispose ();
                TextFieldVehicleRegistrationMark = null;
            }
        }
    }
}