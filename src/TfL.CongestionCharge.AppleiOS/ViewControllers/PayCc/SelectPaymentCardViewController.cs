﻿using CoreGraphics;
using GalaSoft.MvvmLight.Helpers;
using System;
using System.Linq;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Payments;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Helpers;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class SelectPaymentCardViewController : BaseViewController<SelectPaymentCardViewModel>
    {
        private SavedCardPaymentPopup _cardPaymentPopup;

        public SelectPaymentCardViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewCardsHeight.Constant = 300;
            TableViewCards.ReloadData();
            TableViewCards.LayoutIfNeeded();
            TableViewCardsHeight.Constant = TableViewCards.ContentSize.Height;
            ScrollView.LayoutIfNeeded();
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, Container.Frame.Height);
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;
            LoadingTitle = Strings.PleaseWaitForPaymentTitle;
            SetUpNavigationBar(ViewModel.PageTitle);
            SetTableViewStyle();
            ButtonAddAnotherCard.SetCommand(Events.TouchUpInside, ViewModel.ChooseAnotherCardCommand);
            PopulateStoredCards();
        }

        private void PopulateStoredCards()
        {
            if (!ViewModel.UserPaymentCards.Any())
            {
                return;
            }

            TableViewCards.Source = new StoredCardsTableViewSource(() => ViewModel.UserPaymentCards, SelectPaymentCard);
        }

        private void SetTableViewStyle()
        {
            TableViewCards.RegisterNibForCellReuse(StoredCardTableViewCell.Nib, StoredCardTableViewCell.Key);
            TableViewCards.RowHeight = UITableView.AutomaticDimension;
            TableViewCards.BackgroundColor = UiConstants.ScreenBackgroundColor;
            TableViewCards.EstimatedRowHeight = 140f;
            TableViewCards.ReloadData();
            TableViewCards.TableFooterView = new UIView(CGRect.Empty);
            TableViewCards.TableHeaderView = new UIView(CGRect.Empty);
            TableViewCards.BackgroundColor = UIColor.White;
            TableViewCards.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            TableViewCards.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }

        private void SelectPaymentCard(PaymentCardViewModel card = null)
        {
            var bounds = View.Bounds;
            _cardPaymentPopup = new SavedCardPaymentPopup(bounds, Hide, card, ViewModel);
            View.Add(_cardPaymentPopup);
        }

        private void Hide()
        {
            _cardPaymentPopup.Hide();
        }
    }
}