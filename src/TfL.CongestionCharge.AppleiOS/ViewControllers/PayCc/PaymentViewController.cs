﻿using CoreGraphics;
using GalaSoft.MvvmLight.Helpers;
using System;
using Tfl.CongestionCharge.Core.Helper;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Payments;
using TfL.CongestionCharge.AppleiOS.Helpers;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class PaymentViewController : BaseViewController<PaymentViewModel>
    {
        private CvvHintPopupView _loadPop;

        public PaymentViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            UpdateScrollViewContentSize();
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            ImageViewCardTypeIcon.Image = null;
            SetStyle();
            LoadingTitle = Strings.PleaseWaitForPaymentTitle;
            SetUpNavigationBar(ViewModel.PageTitle);
            HideShowMobileEntry(true);
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            PerformDataBind();
            new KeyboardHelper(View, IsViewLoaded, InterfaceOrientation);
        }

        private void SetStyle()
        {
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ViewContainer.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;

            SwitchSaveCardDetails.Hidden = !ViewModel.CanSaveDetails;
            SwitchSaveCardDetailsHeight.Constant = ViewModel.CanSaveDetails ? 33 : 0;
            SwitchSaveCardDetailsBottom.Constant = ViewModel.CanSaveDetails ? 9 : 0;

            SwitchSendReceipt.Hidden = ViewModel.IsPcn;

            SwitchSaveCardDetails.LabelConfirmationDescription.Text = Strings.SaveCardDetails;
            SwitchSendReceipt.LabelConfirmationDescription.Text = Strings.SendMyReceiptViaSms;
            TextFieldSecurityCode.SetWhiteTextFieldStyle("CVV");
            TextFieldExpiryDate.SetWhiteTextFieldStyle("MM/YY");
            TextFieldCardNumber.SetWhiteTextFieldStyle("Card number");
            TextFieldCardHolder.SetWhiteTextFieldStyle("Name on card");
            TextFieldMobileNumber.SetTextFieldStyle();

            LabelCardNumber.SetBoldWhiteTextStyle(11);
            LabelExpiryDate.SetBoldWhiteTextStyle(11);
            LabelCardHolder.SetBoldWhiteTextStyle(11);
            LabelSecurityCode.SetBoldWhiteTextStyle(11);

            CardDetailsViewContainer.BackgroundColor = UiConstants.LightBlueColor;

            TextFieldSecurityCode.SetRightIcon("help_white", BtnCvvHint_TouchUpInside);
        }

        private void BtnCvvHint_TouchUpInside()
        {
            var bounds = View.Bounds;
            _loadPop = new CvvHintPopupView(bounds, Hide);
            View.Add(_loadPop);
        }

        private void Hide()
        {
            _loadPop.Hide();
        }

        private void UpdateScrollViewContentSize(int mobileNumberFieldHeight = 0)
        {
            ScrollView.ContentSize = new CGSize(ScrollView.Frame.Width, ViewContainer.Frame.Height + mobileNumberFieldHeight);
        }

        private void PerformDataBind()
        {
            TextFieldBindings();
            SwitchSaveCardDetails.ConfirmSwitch.ValueChanged += (sender, args) =>
            {
                ViewModel.SaveDetails = SwitchSaveCardDetails.ConfirmSwitch.On;
            };

            ReceiptsViewContainer.Hidden = ViewModel.IsPcn;

            SwitchSendReceipt.ConfirmSwitch.ValueChanged += (sender, args) =>
            {
                ViewModel.IsSmsTextVisible = SwitchSendReceipt.ConfirmSwitch.On;
                var isHidden = !(ViewModel.IsSmsTextVisible && !ViewModel.IsPcn);
                HideShowMobileEntry(isHidden);
                UpdateScrollViewContentSize(isHidden ? -56 : 56);
            };

            ButtonMakePayment.SetCommand(Events.TouchUpInside, ViewModel.MakePaymentCommand);
            ButtonMakePayment.SetTitle(ViewModel.PaymentButtonText, UIControlState.Normal);
        }

        private void TextFieldBindings()
        {
            TextFieldMobileNumber.EditingChanged += (sender, args) =>
            {
                ViewModel.SmsNumber = TextFieldMobileNumber.Text;
            };

            TextFieldCardHolder.EditingChanged += (sender, args) =>
            {
                ViewModel.NameOnCard = TextFieldCardHolder.Text;
            };

            TextFieldCardNumber.EditingChanged += (sender, args) =>
            {
                var isAmex = ViewModel.Issuer == CreditCardType.Amex.ToString();
                CreditCardFormatter.FormatToCreditCardNumber(isAmex, TextFieldCardNumber, TextFieldCardNumber.Text, TextFieldCardNumber.SelectedTextRange);
                ViewModel.CardNumber = TextFieldCardNumber.Text;
                AnimateImage();
            };

            TextFieldExpiryDate.EditingChanged += (sender, args) =>
            {
                CreditCardFormatter.FormatCreditCardDate(TextFieldExpiryDate);
                ViewModel.ExpiryDate = TextFieldExpiryDate.Text;
            };

            TextFieldSecurityCode.EditingChanged += (sender, args) =>
            {
                ViewModel.Cvv2 = TextFieldSecurityCode.Text;
            };

            TextFieldSecurityCode.ShouldChangeCharacters += (field, range, replacementString) =>
            {
                var length = field.Text.Length - range.Length + replacementString.Length;
                ViewModel.Cvv2 = TextFieldSecurityCode.Text;
                return length <= 3;
            };
        }

        private void HideShowMobileEntry(bool isHidden)
        {
            TextFieldMobileNumber.Hidden = isHidden;
            TextFieldMobileNumberHeight.Constant = TextFieldMobileNumber.Hidden ? 0 : 44;
            TextFieldMobileNumberBottomSpace.Constant = TextFieldMobileNumber.Hidden ? 0 : 12;
        }

        private void AnimateImage()
        {
            var image = PaymentCardTypeImageUtils.GetImageFromCardName(ViewModel.Issuer);
            UIView.Transition(ImageViewCardTypeIcon, 1.0,
                UIViewAnimationOptions.TransitionCrossDissolve,
                () => { ImageViewCardTypeIcon.Image = image; },
                null);
        }
    }
}