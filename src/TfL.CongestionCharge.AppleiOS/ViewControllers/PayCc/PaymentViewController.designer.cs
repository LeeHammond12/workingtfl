// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("PaymentViewController")]
    partial class PaymentViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonMakePayment { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView CardDetailsViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewCardTypeIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewChipIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCardHolder { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCardNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelExpiryDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelPageDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelReceipts { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelSecurityCode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ReceiptsViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ReceiptsViewContainerTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomSwitchAndLabelControl SwitchSaveCardDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint SwitchSaveCardDetailsBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint SwitchSaveCardDetailsHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomSwitchAndLabelControl SwitchSendReceipt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFieldCardHolder { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFieldCardNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFieldExpiryDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomTextField TextFieldMobileNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TextFieldMobileNumberBottomSpace { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TextFieldMobileNumberHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFieldSecurityCode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonMakePayment != null) {
                ButtonMakePayment.Dispose ();
                ButtonMakePayment = null;
            }

            if (CardDetailsViewContainer != null) {
                CardDetailsViewContainer.Dispose ();
                CardDetailsViewContainer = null;
            }

            if (ImageViewCardTypeIcon != null) {
                ImageViewCardTypeIcon.Dispose ();
                ImageViewCardTypeIcon = null;
            }

            if (ImageViewChipIcon != null) {
                ImageViewChipIcon.Dispose ();
                ImageViewChipIcon = null;
            }

            if (LabelCardHolder != null) {
                LabelCardHolder.Dispose ();
                LabelCardHolder = null;
            }

            if (LabelCardNumber != null) {
                LabelCardNumber.Dispose ();
                LabelCardNumber = null;
            }

            if (LabelExpiryDate != null) {
                LabelExpiryDate.Dispose ();
                LabelExpiryDate = null;
            }

            if (LabelPageDescription != null) {
                LabelPageDescription.Dispose ();
                LabelPageDescription = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (LabelReceipts != null) {
                LabelReceipts.Dispose ();
                LabelReceipts = null;
            }

            if (LabelSecurityCode != null) {
                LabelSecurityCode.Dispose ();
                LabelSecurityCode = null;
            }

            if (ReceiptsViewContainer != null) {
                ReceiptsViewContainer.Dispose ();
                ReceiptsViewContainer = null;
            }

            if (ReceiptsViewContainerTop != null) {
                ReceiptsViewContainerTop.Dispose ();
                ReceiptsViewContainerTop = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (SwitchSaveCardDetails != null) {
                SwitchSaveCardDetails.Dispose ();
                SwitchSaveCardDetails = null;
            }

            if (SwitchSaveCardDetailsBottom != null) {
                SwitchSaveCardDetailsBottom.Dispose ();
                SwitchSaveCardDetailsBottom = null;
            }

            if (SwitchSaveCardDetailsHeight != null) {
                SwitchSaveCardDetailsHeight.Dispose ();
                SwitchSaveCardDetailsHeight = null;
            }

            if (SwitchSendReceipt != null) {
                SwitchSendReceipt.Dispose ();
                SwitchSendReceipt = null;
            }

            if (TextFieldCardHolder != null) {
                TextFieldCardHolder.Dispose ();
                TextFieldCardHolder = null;
            }

            if (TextFieldCardNumber != null) {
                TextFieldCardNumber.Dispose ();
                TextFieldCardNumber = null;
            }

            if (TextFieldExpiryDate != null) {
                TextFieldExpiryDate.Dispose ();
                TextFieldExpiryDate = null;
            }

            if (TextFieldMobileNumber != null) {
                TextFieldMobileNumber.Dispose ();
                TextFieldMobileNumber = null;
            }

            if (TextFieldMobileNumberBottomSpace != null) {
                TextFieldMobileNumberBottomSpace.Dispose ();
                TextFieldMobileNumberBottomSpace = null;
            }

            if (TextFieldMobileNumberHeight != null) {
                TextFieldMobileNumberHeight.Dispose ();
                TextFieldMobileNumberHeight = null;
            }

            if (TextFieldSecurityCode != null) {
                TextFieldSecurityCode.Dispose ();
                TextFieldSecurityCode = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}