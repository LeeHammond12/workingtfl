﻿using GalaSoft.MvvmLight.Helpers;
using System;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class LookupVrmViewController : BaseViewController<LookupVrmViewModel>
    {
        public LookupVrmViewController(IntPtr handle) : base(handle)
        {
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }
            SetUpNavigationBar(ViewModel.PageTitle, ViewModel.GoBack);

            if (ViewModel.HasFlowState)
            {
                PerformDataBind();
            }

            SwitchAndLabelFirstContainer.LabelConfirmationDescription.Text = Strings.IsVehicleRegisteredInUk;
            new KeyboardHelper(View, IsViewLoaded, InterfaceOrientation);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            ViewModel?.GoBackIfNoFlowState();

            if (string.IsNullOrEmpty(ViewModel?.EnteredVrm))
            {
                TextFieldVehicleRegistrationMark.Text = string.Empty;
                SwitchAndLabelFirstContainer.ConfirmSwitch.On = false;
            }
        }

        private void PerformDataBind()
        {
            TextFieldBindings();
            SwitchAndLabelFirstContainer.ConfirmSwitch.ValueChanged += (sender, args) =>
            {
                ViewModel.IsRegisteredInUk = SwitchAndLabelFirstContainer.ConfirmSwitch.On;
            };

            ButtonNext.SetCommand(Events.TouchUpInside, ViewModel.LookupVRMCommand);
            TextFieldVehicleRegistrationMark.ShouldReturn += TextFieldShouldReturn;
        }

        private bool TextFieldShouldReturn(UITextField textfield)
        {
            if (ViewModel.CanLookupVrm)
            {
                ViewModel.LookupVRMCommand.Execute(null);
            }
            TextFieldVehicleRegistrationMark.ResignFirstResponder();
            return false;
        }

        private void TextFieldBindings()
        {
            TextFieldVehicleRegistrationMark.EditingChanged += (sender, args) =>
            {
                ViewModel.EnteredVrm = TextFieldVehicleRegistrationMark.Text;
            };
        }
    }
}