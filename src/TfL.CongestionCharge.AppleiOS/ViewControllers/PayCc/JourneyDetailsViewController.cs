﻿using CoreGraphics;
using GalaSoft.MvvmLight.Helpers;
using System;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class JourneyDetailsViewController : BaseViewController<JourneyDetailsViewModel>
    {
        public JourneyDetailsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewHeight.Constant = TableViewJourneyDetails.ContentSize.Height - 56;
            ViewContainerJourneyDetails.LayoutIfNeeded();
            ScrollView.ContentSize = new CGSize(ScrollView.Frame.Width, ViewContainerJourneyDetails.Frame.Height);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetTableViewStyle();
            View.BackgroundColor = UiConstants.TableViewCellBackground;
            ScrollView.BackgroundColor = UiConstants.TableViewCellBackground;
            ButtonAddAnotherVehicle.SetLeftIconAndTitleInCenterStyle();
            TableViewJourneyDetails.Source = new JourneyDetailsTableViewSource(ViewModel.GroupedJourneys, ViewModel);
            ButtonConfirmAndPay.SetCommand(Events.TouchUpInside, ViewModel.NavigateToPaymentCommand);
            ButtonAddAnotherVehicle.SetCommand(Events.TouchUpInside, ViewModel.AddAnotherVehicleCommand);

            Bindings.Add(this.SetBinding(() => ViewModel.ButtonText)
               .WhenSourceChanges(() => ButtonConfirmAndPay.SetTitle(ViewModel.ButtonText, UIControlState.Normal)));

            SetUpNavigationBar(Strings.PayCongestionCharge, ViewModel.OnGoBack);
        }

        private void SetTableViewStyle()
        {
            TableViewJourneyDetails.RegisterNibForCellReuse(JourneyDetailsTableViewCell.Nib, JourneyDetailsTableViewCell.Key);
            TableViewJourneyDetails.RegisterNibForCellReuse(JourneyDetailsTableViewHeaderCell.Nib, JourneyDetailsTableViewHeaderCell.Key);
            TableViewJourneyDetails.RowHeight = UITableView.AutomaticDimension;
            TableViewJourneyDetails.EstimatedRowHeight = 140f;
            TableViewJourneyDetails.ReloadData();
            TableViewJourneyDetails.SectionFooterHeight = 0;
            TableViewJourneyDetails.TableFooterView = new UIView(CGRect.Empty);
            TableViewJourneyDetails.TableHeaderView = new UIView(CGRect.Empty);
            TableViewJourneyDetails.ContentInset = new UIEdgeInsets(-36, 0, 0, 0);
            TableViewJourneyDetails.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            AutomaticallyAdjustsScrollViewInsets = false;
        }
    }
}