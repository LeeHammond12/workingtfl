// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("ChooseDatesViewController")]
    partial class ChooseDatesViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonChangeStartDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelChangeStartDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelChangeStartDateDesc { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPayFor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelPayForDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableViewChooseDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainerChangeStartDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainerPayFor { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonChangeStartDate != null) {
                ButtonChangeStartDate.Dispose ();
                ButtonChangeStartDate = null;
            }

            if (LabelChangeStartDate != null) {
                LabelChangeStartDate.Dispose ();
                LabelChangeStartDate = null;
            }

            if (LabelChangeStartDateDesc != null) {
                LabelChangeStartDateDesc.Dispose ();
                LabelChangeStartDateDesc = null;
            }

            if (LabelPayFor != null) {
                LabelPayFor.Dispose ();
                LabelPayFor = null;
            }

            if (LabelPayForDescription != null) {
                LabelPayForDescription.Dispose ();
                LabelPayForDescription = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (TableViewChooseDate != null) {
                TableViewChooseDate.Dispose ();
                TableViewChooseDate = null;
            }

            if (TableViewHeight != null) {
                TableViewHeight.Dispose ();
                TableViewHeight = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }

            if (ViewContainerChangeStartDate != null) {
                ViewContainerChangeStartDate.Dispose ();
                ViewContainerChangeStartDate = null;
            }

            if (ViewContainerPayFor != null) {
                ViewContainerPayFor.Dispose ();
                ViewContainerPayFor = null;
            }
        }
    }
}