﻿using System;
using System.Threading.Tasks;
using CoreGraphics;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Helpers;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class ChooseDatesViewController : BaseViewController<ChooseDatesViewModel>
    {
        private CalendarPopupHelper _calendarPopupHelper;

        public ChooseDatesViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewHeight.Constant = 300;
            TableViewChooseDate.ReloadData();
            TableViewChooseDate.LayoutIfNeeded();
            TableViewHeight.Constant = TableViewChooseDate.ContentSize.Height;
            ScrollView.LayoutIfNeeded();
            ScrollView.ContentSize = new CGSize(ScrollView.Frame.Width, ViewContainer.Frame.Height);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            ViewModel?.GoBackIfNoFlowState();
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ViewContainer.BackgroundColor = UiConstants.ScreenBackgroundColor;
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            SetUpNavigationBar(Strings.ChooseDates, ViewModel.GoBack);

            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            LabelPayForDescription.Text = Strings.PayForDescription;
            LabelChangeStartDate.Text = Strings.ChangeStartDate;
            LabelChangeStartDateDesc.Text = Strings.ChangeStartDateDescription;
            _calendarPopupHelper = new CalendarPopupHelper();
            _calendarPopupHelper?.InitPopupCalendar(OkButtonClick);

            SetTableViewStyle(TableViewChooseDate);
            PerformDataBind();
            BindData();
        }

        private async Task<bool> OkButtonClick()
        {
            if (_calendarPopupHelper.CustomCalendar.IsDateChanged)
            {
                ViewModel.StartDate = _calendarPopupHelper.CustomCalendar.SelectedDate;

                await ViewModel.PopulateCharges(true);

                if (ViewModel.Daily?.Date != null)
                {
                    ButtonChangeStartDate.SetTitle(ViewModel.Daily.Date.Value.ToHumanReadableFormat() + " - " + Strings.ChangeDate, UIControlState.Normal);
                }
            }

            return true;
        }

        private void PerformDataBind()
        {
            ButtonChangeStartDate.TouchUpInside += ButtonChangeStartDate_TouchUpInside;
        }

        private void ButtonChangeStartDate_TouchUpInside(object sender, EventArgs e)
        {
            _calendarPopupHelper.ShowCalendarPopup();
        }

        private void BindData()
        {
            TableViewChooseDate.RegisterNibForCellReuse(ChooseDateTableViewCell.Nib, ChooseDateTableViewCell.Key);
            var tableSource = new PayCcChooseDatesTableViewSource(ViewModel);
            TableViewChooseDate.Source = tableSource;
        }

        private void SetTableViewStyle(UITableView tableView)
        {
            tableView.ReloadData();
            tableView.TableFooterView = new UIView(CGRect.Empty);
            tableView.TableHeaderView = new UIView(CGRect.Empty);
            tableView.BackgroundColor = UIColor.White;
            tableView.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }
    }
}