// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("ConfirmVrmViewController")]
    partial class ConfirmVrmViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonChooseYourDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCarMake { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCarmakeDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelVrm { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelVrmDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel lblPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider SepratorLine { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomSwitchAndLabelControl SwitchAndLAbelFirstContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomSwitchAndLabelControl SwitchAndLAbelSecondContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint SwitchAndLAbelSecondContainerHeight { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonChooseYourDate != null) {
                ButtonChooseYourDate.Dispose ();
                ButtonChooseYourDate = null;
            }

            if (LabelCarMake != null) {
                LabelCarMake.Dispose ();
                LabelCarMake = null;
            }

            if (LabelCarmakeDetails != null) {
                LabelCarmakeDetails.Dispose ();
                LabelCarmakeDetails = null;
            }

            if (LabelVrm != null) {
                LabelVrm.Dispose ();
                LabelVrm = null;
            }

            if (LabelVrmDetails != null) {
                LabelVrmDetails.Dispose ();
                LabelVrmDetails = null;
            }

            if (lblPageHeader != null) {
                lblPageHeader.Dispose ();
                lblPageHeader = null;
            }

            if (SepratorLine != null) {
                SepratorLine.Dispose ();
                SepratorLine = null;
            }

            if (SwitchAndLAbelFirstContainer != null) {
                SwitchAndLAbelFirstContainer.Dispose ();
                SwitchAndLAbelFirstContainer = null;
            }

            if (SwitchAndLAbelSecondContainer != null) {
                SwitchAndLAbelSecondContainer.Dispose ();
                SwitchAndLAbelSecondContainer = null;
            }

            if (SwitchAndLAbelSecondContainerHeight != null) {
                SwitchAndLAbelSecondContainerHeight.Dispose ();
                SwitchAndLAbelSecondContainerHeight = null;
            }
        }
    }
}