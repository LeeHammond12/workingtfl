// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("JourneyDetailsViewController")]
    partial class JourneyDetailsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonAddAnotherVehicle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonConfirmAndPay { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelPageDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint TableViewHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableViewJourneyDetails { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainerJourneyDetails { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonAddAnotherVehicle != null) {
                ButtonAddAnotherVehicle.Dispose ();
                ButtonAddAnotherVehicle = null;
            }

            if (ButtonConfirmAndPay != null) {
                ButtonConfirmAndPay.Dispose ();
                ButtonConfirmAndPay = null;
            }

            if (LabelPageDescription != null) {
                LabelPageDescription.Dispose ();
                LabelPageDescription = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (TableViewHeight != null) {
                TableViewHeight.Dispose ();
                TableViewHeight = null;
            }

            if (TableViewJourneyDetails != null) {
                TableViewJourneyDetails.Dispose ();
                TableViewJourneyDetails = null;
            }

            if (ViewContainerJourneyDetails != null) {
                ViewContainerJourneyDetails.Dispose ();
                ViewContainerJourneyDetails = null;
            }
        }
    }
}