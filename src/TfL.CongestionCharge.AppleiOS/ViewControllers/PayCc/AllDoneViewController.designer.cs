// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("AllDoneViewController")]
    partial class AllDoneViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonHome { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonSaveReceipt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonSaveReceiptHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonSaveReceiptTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider Divider1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider Divider2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImageViewDoneIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelAllDone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAmountPaid { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAmountPaidValue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelPaymentDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelPaymentDateValue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelPaymentSuccess { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelPaymentType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelPaymentTypeValue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelPcnNo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelPcnNoValue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider LabelPcnNumberDividier { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelPcnNumberDividierHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelReceiptNo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider LabelReceiptNoDivider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelReceiptNoDividerHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelReceiptNoValue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView LabelType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelYourReceipt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelYourReceiptValue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainerConfirmation { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainerReceipt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewPcnNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ViewPcnNumberBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ViewPcnNumberTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewReceiptNo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ViewReceiptNoBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ViewReceiptNoTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewYourReceipt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ViewYourReceiptTop { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonHome != null) {
                ButtonHome.Dispose ();
                ButtonHome = null;
            }

            if (ButtonSaveReceipt != null) {
                ButtonSaveReceipt.Dispose ();
                ButtonSaveReceipt = null;
            }

            if (ButtonSaveReceiptHeight != null) {
                ButtonSaveReceiptHeight.Dispose ();
                ButtonSaveReceiptHeight = null;
            }

            if (ButtonSaveReceiptTop != null) {
                ButtonSaveReceiptTop.Dispose ();
                ButtonSaveReceiptTop = null;
            }

            if (Divider1 != null) {
                Divider1.Dispose ();
                Divider1 = null;
            }

            if (Divider2 != null) {
                Divider2.Dispose ();
                Divider2 = null;
            }

            if (ImageViewDoneIcon != null) {
                ImageViewDoneIcon.Dispose ();
                ImageViewDoneIcon = null;
            }

            if (LabelAllDone != null) {
                LabelAllDone.Dispose ();
                LabelAllDone = null;
            }

            if (LabelAmountPaid != null) {
                LabelAmountPaid.Dispose ();
                LabelAmountPaid = null;
            }

            if (LabelAmountPaidValue != null) {
                LabelAmountPaidValue.Dispose ();
                LabelAmountPaidValue = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (LabelPaymentDate != null) {
                LabelPaymentDate.Dispose ();
                LabelPaymentDate = null;
            }

            if (LabelPaymentDateValue != null) {
                LabelPaymentDateValue.Dispose ();
                LabelPaymentDateValue = null;
            }

            if (LabelPaymentSuccess != null) {
                LabelPaymentSuccess.Dispose ();
                LabelPaymentSuccess = null;
            }

            if (LabelPaymentType != null) {
                LabelPaymentType.Dispose ();
                LabelPaymentType = null;
            }

            if (LabelPaymentTypeValue != null) {
                LabelPaymentTypeValue.Dispose ();
                LabelPaymentTypeValue = null;
            }

            if (LabelPcnNo != null) {
                LabelPcnNo.Dispose ();
                LabelPcnNo = null;
            }

            if (LabelPcnNoValue != null) {
                LabelPcnNoValue.Dispose ();
                LabelPcnNoValue = null;
            }

            if (LabelPcnNumberDividier != null) {
                LabelPcnNumberDividier.Dispose ();
                LabelPcnNumberDividier = null;
            }

            if (LabelPcnNumberDividierHeight != null) {
                LabelPcnNumberDividierHeight.Dispose ();
                LabelPcnNumberDividierHeight = null;
            }

            if (LabelReceiptNo != null) {
                LabelReceiptNo.Dispose ();
                LabelReceiptNo = null;
            }

            if (LabelReceiptNoDivider != null) {
                LabelReceiptNoDivider.Dispose ();
                LabelReceiptNoDivider = null;
            }

            if (LabelReceiptNoDividerHeight != null) {
                LabelReceiptNoDividerHeight.Dispose ();
                LabelReceiptNoDividerHeight = null;
            }

            if (LabelReceiptNoValue != null) {
                LabelReceiptNoValue.Dispose ();
                LabelReceiptNoValue = null;
            }

            if (LabelType != null) {
                LabelType.Dispose ();
                LabelType = null;
            }

            if (LabelYourReceipt != null) {
                LabelYourReceipt.Dispose ();
                LabelYourReceipt = null;
            }

            if (LabelYourReceiptValue != null) {
                LabelYourReceiptValue.Dispose ();
                LabelYourReceiptValue = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }

            if (ViewContainerConfirmation != null) {
                ViewContainerConfirmation.Dispose ();
                ViewContainerConfirmation = null;
            }

            if (ViewContainerReceipt != null) {
                ViewContainerReceipt.Dispose ();
                ViewContainerReceipt = null;
            }

            if (ViewPcnNumber != null) {
                ViewPcnNumber.Dispose ();
                ViewPcnNumber = null;
            }

            if (ViewPcnNumberBottom != null) {
                ViewPcnNumberBottom.Dispose ();
                ViewPcnNumberBottom = null;
            }

            if (ViewPcnNumberTop != null) {
                ViewPcnNumberTop.Dispose ();
                ViewPcnNumberTop = null;
            }

            if (ViewPrice != null) {
                ViewPrice.Dispose ();
                ViewPrice = null;
            }

            if (ViewReceiptNo != null) {
                ViewReceiptNo.Dispose ();
                ViewReceiptNo = null;
            }

            if (ViewReceiptNoBottom != null) {
                ViewReceiptNoBottom.Dispose ();
                ViewReceiptNoBottom = null;
            }

            if (ViewReceiptNoTop != null) {
                ViewReceiptNoTop.Dispose ();
                ViewReceiptNoTop = null;
            }

            if (ViewYourReceipt != null) {
                ViewYourReceipt.Dispose ();
                ViewYourReceipt = null;
            }

            if (ViewYourReceiptTop != null) {
                ViewYourReceiptTop.Dispose ();
                ViewYourReceiptTop = null;
            }
        }
    }
}