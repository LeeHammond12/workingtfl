﻿using System;
using CoreGraphics;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Receipts;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class ViewReceiptViewController : BaseViewController<ViewReceiptsViewModel>
    {
        public ViewReceiptViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewReceipts.ReloadData();
            TableViewReceipts.LayoutIfNeeded();
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.Receipts);
            SetTableViewStyle(TableViewReceipts);
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            ShowReceipts();
        }

        private void ShowReceipts()
        {
            var vmReceipts = ViewModel.Receipts;

            TableViewReceipts.Source = new ReceiptTableSource(vmReceipts);
        }

        private void SetTableViewStyle(UITableView tableView)
        {
            tableView.RowHeight = UITableView.AutomaticDimension;
            tableView.EstimatedRowHeight = 140f;
            tableView.ReloadData();
            tableView.TableFooterView = new UIView(CGRect.Empty);
            tableView.TableHeaderView = new UIView(CGRect.Empty);
            tableView.BackgroundColor = UiConstants.TableViewCellBackground;
            tableView.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }
    }
}