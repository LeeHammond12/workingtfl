﻿using Foundation;
using System;
using CoreGraphics;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.Main;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class AllDoneViewController : BaseViewController<AllDoneViewModel>
    {
        public AllDoneViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            ScrollView.ContentSize = new CGSize(ScrollView.Frame.Width, ViewContainer.Frame.Height);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(ViewModel.PageTitle, null, true);
            View.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ViewContainer.BackgroundColor = UiConstants.ScreenBackgroundColor;
            ScrollView.BackgroundColor = UiConstants.ScreenBackgroundColor;

            if (ViewModel.Payment == null)
            {
                NavigationService.NavigateHomeAndClearBackStack();
                return;
            }

            SetLabelStyle();
            BindData();
            HidePcnData();
        }

        private void BindData()
        {
            LabelPaymentType.Text = Strings.TypeColon;
            LabelPaymentTypeValue.Text = ViewModel.ChargeType;

            LabelAmountPaid.Text = Strings.AmountPaid;
            LabelAmountPaidValue.Text = ViewModel.Price.ToCurrencyString();

            LabelPaymentDate.Text = Strings.PaymentDate;
            LabelPaymentDateValue.Text = ViewModel.PaymentDate;

            LabelYourReceipt.Text = Strings.YourReceipt;
            LabelYourReceiptValue.Text = ViewModel.Receipt;

            ButtonSaveReceipt.SetCommand(Events.TouchUpInside, ViewModel.SaveReceiptAndReturnHome);
            ButtonHome.SetCommand(Events.TouchUpInside, ViewModel.ReturnHome);

            ButtonSaveReceipt.Hidden = !ViewModel.IsSaveReceiptButtonVisible;
            ButtonSaveReceiptTop.Constant = ViewModel.IsSaveReceiptButtonVisible ? 16 : 0;
            ButtonSaveReceiptHeight.Constant = ViewModel.IsSaveReceiptButtonVisible ? 44 : 0;
        }

        private void HidePcnData()
        {
            var isPcn = ViewModel.IsPcn;
            ViewPcnNumber.Hidden = !isPcn;
            LabelPcnNoValue.Text = !isPcn ? string.Empty : ViewModel.PcnNumber;
            LabelPcnNo.Text = !isPcn ? string.Empty : Strings.PcnNoColon;
            ViewReceiptNo.Hidden = !isPcn;
            LabelReceiptNoValue.Text = !isPcn ? string.Empty : ViewModel.Receipt;
            LabelReceiptNo.Text = !isPcn ? string.Empty : Strings.ReceiptNoColon;
            LabelPcnNumberDividier.Hidden = !isPcn;
            ViewPcnNumberBottom.Constant = isPcn ? 8 : 0;
            ViewReceiptNoBottom.Constant = isPcn ? 8 : 0;
            ViewReceiptNoTop.Constant = isPcn ? 8 : 0;
            ViewPcnNumberTop.Constant = isPcn ? 8 : 0;
            LabelPcnNumberDividierHeight.Constant = isPcn ? 1 : 0;

            LabelReceiptNoDivider.Hidden = !isPcn;
            LabelReceiptNoDividerHeight.Constant = isPcn ? 1 : 0;
        }

        private void SetLabelStyle()
        {
            Divider1.BackgroundColor = UiConstants.DividerColor;
            Divider2.BackgroundColor = UiConstants.DividerColor;
            LabelReceiptNoDivider.BackgroundColor = UiConstants.DividerColor;
            LabelPcnNumberDividier.BackgroundColor = UiConstants.DividerColor;
            ButtonSaveReceipt.SetGrayButtonStyle(UiConstants.BlueTextColor);
            ButtonSaveReceipt.SetLeftIconAndTitleInCenterStyle();

            LabelYourReceipt.SetNormalBlueTextStyle();
            LabelPaymentType.SetLightTextStyle();
            LabelPcnNo.SetLightTextStyle();
            LabelReceiptNo.SetLightTextStyle();
            LabelAmountPaid.SetLightTextStyle();
            LabelPaymentDate.SetLightTextStyle();

            LabelYourReceiptValue.SetNormalTextBoldStyle();
            LabelPaymentTypeValue.SetNormalTextStyle();
            LabelPcnNoValue.SetNormalTextStyle();
            LabelReceiptNoValue.SetNormalTextStyle();
            LabelAmountPaidValue.SetNormalTextStyle();
            LabelPaymentDateValue.SetNormalTextStyle();

        }
    }
}