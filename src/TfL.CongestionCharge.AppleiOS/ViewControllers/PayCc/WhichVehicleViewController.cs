﻿using CoreGraphics;
using GalaSoft.MvvmLight.Helpers;
using System;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class WhichVehicleViewController : BaseViewController<WhichVehicleViewModel>
    {
        public WhichVehicleViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewHeightConstant.Constant = TableViewSavedVehicles.ContentSize.Height;
            ScrollView.ContentSize = new CGSize(ScrollView.Frame.Width, ViewContainer.Frame.Height + 32);
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }
            SetUpNavigationBar(Strings.PayCongestionCharge, ViewModel.GoBack);

            SetTableViewStyle(TableViewSavedVehicles);
            PerformDataBind();
            BindData();
        }

        private void PerformDataBind()
        {
            ButtonChooseAnotherVehicle.SetCommand(Events.TouchUpInside, ViewModel.ChooseAnotherVehicleCommand);
        }

        private void BindData()
        {
            TableViewSavedVehicles.RegisterNibForCellReuse(VehicleDetailsTableViewCell.Nib, VehicleDetailsTableViewCell.Key);
            var tableSource = new WhichVehicleTableViewSource(ViewModel.SavedVrms, (v) => ViewModel.SetSelectedVrm(v));
            TableViewSavedVehicles.Source = tableSource;
        }

        private void SetTableViewStyle(UITableView tableView)
        {
            tableView.RowHeight = UITableView.AutomaticDimension;
            tableView.EstimatedRowHeight = 140f;
            tableView.ReloadData();
            tableView.TableFooterView = new UIView(CGRect.Empty);
            tableView.TableHeaderView = new UIView(CGRect.Empty);
            tableView.BackgroundColor = UIColor.White;
            tableView.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }
    }
}