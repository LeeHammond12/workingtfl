// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("ViewReceiptViewController")]
    partial class ViewReceiptViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableViewReceipts { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (TableViewReceipts != null) {
                TableViewReceipts.Dispose ();
                TableViewReceipts = null;
            }
        }
    }
}