// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("PcnLookupViewController")]
    partial class PcnLookupViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonEnterAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelAmountBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint LabelAmountTop { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelDueAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomTextField TextFieldPcnNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomTextField TextFieldVehicleRegMark { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainerAmount { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonEnterAmount != null) {
                ButtonEnterAmount.Dispose ();
                ButtonEnterAmount = null;
            }

            if (LabelAmount != null) {
                LabelAmount.Dispose ();
                LabelAmount = null;
            }

            if (LabelAmountBottom != null) {
                LabelAmountBottom.Dispose ();
                LabelAmountBottom = null;
            }

            if (LabelAmountTop != null) {
                LabelAmountTop.Dispose ();
                LabelAmountTop = null;
            }

            if (LabelDueAmount != null) {
                LabelDueAmount.Dispose ();
                LabelDueAmount = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (TextFieldPcnNumber != null) {
                TextFieldPcnNumber.Dispose ();
                TextFieldPcnNumber = null;
            }

            if (TextFieldVehicleRegMark != null) {
                TextFieldVehicleRegMark.Dispose ();
                TextFieldVehicleRegMark = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }

            if (ViewContainerAmount != null) {
                ViewContainerAmount.Dispose ();
                ViewContainerAmount = null;
            }
        }
    }
}