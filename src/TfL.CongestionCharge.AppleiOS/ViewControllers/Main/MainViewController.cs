﻿using System;
using CoreGraphics;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.Main;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class MainViewController : BaseViewController<MainViewModel>
    {
        public MainViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            ScrollView.ContentSize = new CGSize(ScrollView.Frame.Width, ViewContainer.Frame.Height);
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewContainer.BackgroundColor = UIColor.White;
            View.BackgroundColor = UIColor.White;
            await ViewModel.InitViewModel();
            SetNavigationStyle();
            NavigationController.SetNavigationBarHidden(false, true);
            if (!AppEnvironment.Current.AppState.UserHasAcceptedTermsAndConditions)
            {
                NavigateToTermsAndCondition();
            }
            SetButtonStyle();
            ButtonReceipts.Enabled = false;
            SetBindingAsync();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            AppDelegate.Current.AppBecomeActiveAgain += AppBecomeActiveAgain;
            if (AppDelegate.Current.AppComesFromBackgroud)
            {
                AppBecomeActiveAgain(null, null);
            }

            SetReceiptButtonUi();
            UserLoggedIn();
            NavigationController.SetNavigationBarHidden(false, true);
        }

        public override void ViewWillDisappear(bool animated)
        {
            AppDelegate.Current.AppBecomeActiveAgain -= AppBecomeActiveAgain;
            base.ViewWillDisappear(animated);
        }

        private void NavigateToTermsAndCondition()
        {
            var storyBoard = UIStoryboard.FromName("Main", null);
            var controller = storyBoard.InstantiateViewController("TermAndConditionsViewController");
            NavigationController.PushViewController(controller, false);
        }

        private void SetBindingAsync()
        {
            ButtonPayCC.SetCommand(Events.TouchUpInside, ViewModel.NavigateToPayCcCommand);
            ButtonPCN.SetCommand(Events.TouchUpInside, ViewModel.NavigateToPCNLookupCommand);
            ButtonCheckPostcode.SetCommand(Events.TouchUpInside, ViewModel.NavigateToPostCodeLookupCommand);
            ButtonReceipts.SetCommand(Events.TouchUpInside, ViewModel.NavigateToReceiptsCommand);
            ButtonYourAccount.SetCommand(Events.TouchUpInside, ViewModel.NavigateToYourAccountCommand);
            ButtonManageYourAutoPay.SetCommand(Events.TouchUpInside, ViewModel.NavigateToAutoPayCommand);
            ButtonSingInSignOut.TouchUpInside += (s, e) =>
            {
                ViewModel.LoginOrLogoutCommand.Execute(null);
                if (ButtonSingInSignOut.Title(UIControlState.Normal).Equals(Strings.SignOut))
                {
                    UserLoggedIn();
                    SetReceiptButtonUi();
                }
            };

            ButtonManageStoredInfo.SetCommand(Events.TouchUpInside, ViewModel.NavigateToManageStoredInformationCommand);

            Bindings.Add(this.SetBinding(() => ViewModel.ReceiptsAvailable)
                .WhenSourceChanges(() => SetReceiptButtonUi()));
        }

        private void UserLoggedIn()
        {
            if (ViewModel.IsLoggedIn)
            {
                ButtonManageYourAutoPay.Hidden = false;
                ButtonManageYourAutoPayHeight.Constant = 60;
                ButtonManageYourAutoPayBottom.Constant = 8;

                ButtonYourAccount.Hidden = false;
                ButtonYourAccountHeight.Constant = 60;
                ButtonYourAccountBottom.Constant = 8;

                ButtonManageStoredInfo.Hidden = true;
                ButtonManageStoredInfoHeight.Constant = 0;
                ButtonManageStoredInfoBottom.Constant = 0;
                ButtonSingInSignOut.SetTitle(Strings.SignOut, UIControlState.Normal);
            }
            else
            {
                ButtonManageYourAutoPay.Hidden = true;
                ButtonManageYourAutoPayHeight.Constant = 0;
                ButtonManageYourAutoPayBottom.Constant = 0;

                ButtonYourAccount.Hidden = true;
                ButtonYourAccountHeight.Constant = 0;
                ButtonYourAccountBottom.Constant = 0;

                ButtonManageStoredInfo.Hidden = false;
                ButtonManageStoredInfoHeight.Constant = 60;
                ButtonManageStoredInfoBottom.Constant = 8;
                ButtonSingInSignOut.SetTitle(Strings.SignIn, UIControlState.Normal);
            }
        }

        private void AppBecomeActiveAgain(object sender, EventArgs eventArgs)
        {
            AppDelegate.Current.AppComesFromBackgroud = false;
        }

        private void SetReceiptButtonUi()
        {
            if (!ViewModel.IsLoggedIn)
            {
                ButtonReceipts.Hidden = false;
                ButtonReceiptsHeight.Constant = 60;
                ButtonReceiptsBottom.Constant = 8;
                var isReceiptAvailable = ViewModel.ReceiptsAvailable;
                ButtonReceipts.Enabled = isReceiptAvailable;
                ButtonReceipts.SetTitleColor(isReceiptAvailable ? UIColor.Black : UIColor.Gray, isReceiptAvailable ? UIControlState.Normal : UIControlState.Disabled);
            }
            else
            {
                ButtonReceipts.Hidden = true;
                ButtonReceiptsHeight.Constant = 0;
                ButtonReceiptsBottom.Constant = 0;
            }
        }

        private void SetNavigationStyle()
        {
            NavigationController.NavigationBar.BarStyle = UIBarStyle.Black;
            NavigationController.NavigationBar.TintColor = UIColor.White;
            NavigationController.NavigationBar.BarTintColor = UIColor.Black;
            NavigationController.NavigationBar.BackgroundColor = UIColor.Black;
            NavigationItem.TitleView = GetNavigationHeaderView(Strings.HomeScreenHeading);

            if (!AppEnvironment.Current.AppState.UserHasAcceptedTermsAndConditions)
            {
                NavigateToTermsAndCondition();
            }
        }

        private void SetButtonStyle()
        {
            ButtonPayCC.SetGrayButtonStyle(UiConstants.GrayButtonTextColor, true);
            ButtonPCN.SetGrayButtonStyle(UiConstants.GrayButtonTextColor, true);
            ButtonCheckPostcode.SetGrayButtonStyle(UiConstants.GrayButtonTextColor, true);
            ButtonReceipts.SetGrayButtonStyle(UiConstants.GrayButtonTextColor, true);
            ButtonYourAccount.SetGrayButtonStyle(UiConstants.GrayButtonTextColor, true);
            ButtonManageYourAutoPay.SetGrayButtonStyle(UiConstants.GrayButtonTextColor, true);
            ButtonManageStoredInfo.SetGrayButtonStyle(UiConstants.GrayButtonTextColor, true);
        }
    }
}