﻿using GalaSoft.MvvmLight.Helpers;
using System;
using CoreGraphics;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class PostCodeCheckViewController : BaseViewController<PostCodeCheckerViewModel>
    {
        public PostCodeCheckViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, ViewContainer.Frame.Height);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.CheckAPostCode);
            SetStyle();
            TextFieldBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.CanEnterBuildingName, BindingMode.TwoWay)
                .WhenSourceChanges(HandleEnabled));

            Bindings.Add(this.SetBinding(() => ViewModel.CanEnterBuildingNumber, BindingMode.TwoWay)
                .WhenSourceChanges(HandleEnabled));
            ButtonCheck.SetCommand(Events.TouchUpInside, ViewModel.LookupPostCodeCommand);
            new KeyboardHelper(View, IsViewLoaded, InterfaceOrientation);
        }

        private void HandleEnabled()
        {
            TextFieldBuildingName.Enabled = ViewModel.CanEnterBuildingName;
            TextFieldBuildingNumber.Enabled = ViewModel.CanEnterBuildingNumber;
        }

        private void SetStyle()
        {
            TextFieldBuildingNumber.SetTextStartLeftPaddign();
            TextFieldBuildingName.SetTextStartLeftPaddign();
            TextFieldPostcode.SetTextStartLeftPaddign();
            LeftDivider.BackgroundColor = UIColor.DarkGray;
            RightDivider.BackgroundColor = UIColor.DarkGray;
        }

        private void TextFieldBindings()
        {
            TextFieldPostcode.EditingChanged += (sender, args) =>
            {
                ViewModel.PostCode = TextFieldPostcode.Text;
            };

            TextFieldBuildingNumber.EditingChanged += (sender, args) =>
            {
                ViewModel.BuildingNumber = TextFieldBuildingNumber.Text;
            };

            TextFieldBuildingName.EditingChanged += (sender, args) =>
            {
                ViewModel.BuildingName = TextFieldBuildingName.Text;
            };
        }
    }
}