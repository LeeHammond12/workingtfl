// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("LoginViewController")]
    partial class LoginViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonSignIn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelLondonAccount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelNoAccountLink { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomTextField TextFieldCustomerId { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomTextField TextFieldPassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonSignIn != null) {
                ButtonSignIn.Dispose ();
                ButtonSignIn = null;
            }

            if (LabelLondonAccount != null) {
                LabelLondonAccount.Dispose ();
                LabelLondonAccount = null;
            }

            if (LabelNoAccountLink != null) {
                LabelNoAccountLink.Dispose ();
                LabelNoAccountLink = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (TextFieldCustomerId != null) {
                TextFieldCustomerId.Dispose ();
                TextFieldCustomerId = null;
            }

            if (TextFieldPassword != null) {
                TextFieldPassword.Dispose ();
                TextFieldPassword = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}