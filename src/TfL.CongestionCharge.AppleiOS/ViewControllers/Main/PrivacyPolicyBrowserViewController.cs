﻿using GalaSoft.MvvmLight.Helpers;
using System;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.ViewControllers;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class PrivacyPolicyBrowserViewController : BaseWebBrowserViewController<PrivacyPolicyBrowswerViewModel>
    {
        public PrivacyPolicyBrowserViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ShowPrivacyDetailsInWebView(ViewModel.NavigationUri, WebViewPrivacyPolicy);
            ButtonDone.SetCommand(Events.TouchUpInside, ViewModel.GoBackCommand);
        }
    }
}