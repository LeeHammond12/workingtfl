// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("TermAndConditionsViewController")]
    partial class TermAndConditionsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonAccepctTermAndConditions { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonGoToWeb { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CustomerResponsibilityDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CustomerResponsibilityTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageDescriptionLabel LabelTermAndCondition { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonAccepctTermAndConditions != null) {
                ButtonAccepctTermAndConditions.Dispose ();
                ButtonAccepctTermAndConditions = null;
            }

            if (ButtonGoToWeb != null) {
                ButtonGoToWeb.Dispose ();
                ButtonGoToWeb = null;
            }

            if (CustomerResponsibilityDescription != null) {
                CustomerResponsibilityDescription.Dispose ();
                CustomerResponsibilityDescription = null;
            }

            if (CustomerResponsibilityTitle != null) {
                CustomerResponsibilityTitle.Dispose ();
                CustomerResponsibilityTitle = null;
            }

            if (LabelTermAndCondition != null) {
                LabelTermAndCondition.Dispose ();
                LabelTermAndCondition = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}