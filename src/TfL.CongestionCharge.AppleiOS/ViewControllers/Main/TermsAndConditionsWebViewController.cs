﻿using GalaSoft.MvvmLight.Helpers;
using System;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.ViewControllers;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class TermsAndConditionsWebViewController : BaseWebBrowserViewController<TermsAndConditionsViewModel>
    {
        public TermsAndConditionsWebViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ShowPrivacyDetailsInWebView(ViewModel.NavigationUri, WebViewTermAndConditions);
            ButtonDone.SetCommand(Events.TouchUpInside, ViewModel.GoBackCommand);
        }
    }
}