﻿using System;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.Main;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.UIUtils;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class LoginViewController : BaseViewController<LoginViewModel>
    {
        public LoginViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.LoginToYourAccount, ViewModel.OnGoBack);
            SetStyle();
            PerformBindings();
            new KeyboardHelper(View, IsViewLoaded, InterfaceOrientation);
        }

        private void SetStyle()
        {
            TextFieldPassword.SetRightIcon("help", ShowPasswordHint);
            TextFieldCustomerId.SetRightIcon("help", ShowCustomerIdHint);
            LabelLondonAccount.Text = Strings.NoLondonAccountText;
            LabelNoAccountLink.UserInteractionEnabled = true;
            var attributedString = new NSMutableAttributedString(Strings.NoLondonUrl);
            var linkRange = new NSRange(0, Strings.NoLondonUrl.Length);
            var stringAttributes = new UIStringAttributes
            {
                ForegroundColor = UiConstants.BlueTextColor,
                UnderlineStyle = NSUnderlineStyle.Single
            };
            attributedString.SetAttributes(stringAttributes, linkRange);
            LabelNoAccountLink.AttributedText = attributedString;
            var tab = new UITapGestureRecognizer(() =>
            {
                var nsurl = new NSUrl("https://" + Strings.NoLondonUrl);
                UIApplication.SharedApplication.OpenUrl(nsurl);
            });
            LabelNoAccountLink.AddGestureRecognizer(tab);
        }

        private void PerformBindings()
        {
            TextFieldBindings();
            ButtonSignIn.SetCommand(Events.TouchUpInside, ViewModel.LoginCommand);
        }

        private void ShowCustomerIdHint()
        {
            AppEnvironment.Current.DialogService.ShowMessage(Strings.CustomerIdHelpText, Strings.Information);
        }

        private void ShowPasswordHint()
        {
            AppEnvironment.Current.DialogService.ShowMessage(Strings.PasswordHelpText, Strings.Information);
        }

        private void TextFieldBindings()
        {
            TextFieldCustomerId.EditingChanged += (sender, args) =>
            {
                ViewModel.AccountNumber = TextFieldCustomerId.Text;
            };

            TextFieldPassword.EditingChanged += (sender, args) =>
            {
                ViewModel.Password = TextFieldPassword.Text;
            };

            TextFieldCustomerId.Text = ViewModel.AccountNumber;
            TextFieldPassword.Text = ViewModel.Password;
        }
    }
}