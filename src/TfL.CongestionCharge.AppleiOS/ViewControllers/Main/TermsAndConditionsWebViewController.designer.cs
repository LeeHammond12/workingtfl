// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("TermsAndConditionsWebViewController")]
    partial class TermsAndConditionsWebViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonDone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIWebView WebViewTermAndConditions { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonDone != null) {
                ButtonDone.Dispose ();
                ButtonDone = null;
            }

            if (WebViewTermAndConditions != null) {
                WebViewTermAndConditions.Dispose ();
                WebViewTermAndConditions = null;
            }
        }
    }
}