﻿using GalaSoft.MvvmLight.Helpers;
using System;
using Foundation;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class TermAndConditionsViewController : BaseViewController<TermsAndConditionsViewModel>
    {
        public TermAndConditionsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ButtonGoToWeb.SetGrayButtonStyle(UiConstants.BlueTextColor);
            SetUpNavigationBar(Strings.TermsAndConditionTitle, null, !ViewModel.UserHasAcceptedTermsAndConditions);
            ButtonAccepctTermAndConditions.SetCommand(Events.TouchUpInside, ViewModel.AcceptCommand);
            ButtonGoToWeb.SetCommand(Events.TouchUpInside, ViewModel.NavigateToPrivacyCommand);
            ButtonAccepctTermAndConditions.SetTitle(ViewModel.UserHasAcceptedTermsAndConditions
                ? Strings.Close
                : Strings.AcceptTermsAndCondition, UIControlState.Normal);

            CustomerResponsibilityTitle.Text = Strings.CustomerResposibility;
            CustomerResponsibilityDescription.Text = Strings.CustomerResposibilityDescription;
            CustomerResponsibilityTitle.SetNormalLargeTextBoldStyle();
            CustomerResponsibilityDescription.SetNormalTextStyle();

            LabelTermAndCondition.Text = ViewModel.PrivacyPageContent;
        }
    }
}