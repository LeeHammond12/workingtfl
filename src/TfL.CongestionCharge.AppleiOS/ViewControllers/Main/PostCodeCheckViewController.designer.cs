// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("PostCodeCheckViewController")]
    partial class PostCodeCheckViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonCheck { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelOR { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider LeftDivider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.HorizontalDivider RightDivider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFieldBuildingName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFieldBuildingNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFieldPostcode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonCheck != null) {
                ButtonCheck.Dispose ();
                ButtonCheck = null;
            }

            if (LabelOR != null) {
                LabelOR.Dispose ();
                LabelOR = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (LeftDivider != null) {
                LeftDivider.Dispose ();
                LeftDivider = null;
            }

            if (RightDivider != null) {
                RightDivider.Dispose ();
                RightDivider = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (TextFieldBuildingName != null) {
                TextFieldBuildingName.Dispose ();
                TextFieldBuildingName = null;
            }

            if (TextFieldBuildingNumber != null) {
                TextFieldBuildingNumber.Dispose ();
                TextFieldBuildingNumber = null;
            }

            if (TextFieldPostcode != null) {
                TextFieldPostcode.Dispose ();
                TextFieldPostcode = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}