// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("MainViewController")]
    partial class MainViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonCheckPostcode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonManageStoredInfo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonManageStoredInfoBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonManageStoredInfoHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonManageYourAutoPay { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonManageYourAutoPayBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonManageYourAutoPayHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonPayCC { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonPCN { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonReceipts { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonReceiptsBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonReceiptsHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonSingInSignOut { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonYourAccount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonYourAccountBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint ButtonYourAccountHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonCheckPostcode != null) {
                ButtonCheckPostcode.Dispose ();
                ButtonCheckPostcode = null;
            }

            if (ButtonManageStoredInfo != null) {
                ButtonManageStoredInfo.Dispose ();
                ButtonManageStoredInfo = null;
            }

            if (ButtonManageStoredInfoBottom != null) {
                ButtonManageStoredInfoBottom.Dispose ();
                ButtonManageStoredInfoBottom = null;
            }

            if (ButtonManageStoredInfoHeight != null) {
                ButtonManageStoredInfoHeight.Dispose ();
                ButtonManageStoredInfoHeight = null;
            }

            if (ButtonManageYourAutoPay != null) {
                ButtonManageYourAutoPay.Dispose ();
                ButtonManageYourAutoPay = null;
            }

            if (ButtonManageYourAutoPayBottom != null) {
                ButtonManageYourAutoPayBottom.Dispose ();
                ButtonManageYourAutoPayBottom = null;
            }

            if (ButtonManageYourAutoPayHeight != null) {
                ButtonManageYourAutoPayHeight.Dispose ();
                ButtonManageYourAutoPayHeight = null;
            }

            if (ButtonPayCC != null) {
                ButtonPayCC.Dispose ();
                ButtonPayCC = null;
            }

            if (ButtonPCN != null) {
                ButtonPCN.Dispose ();
                ButtonPCN = null;
            }

            if (ButtonReceipts != null) {
                ButtonReceipts.Dispose ();
                ButtonReceipts = null;
            }

            if (ButtonReceiptsBottom != null) {
                ButtonReceiptsBottom.Dispose ();
                ButtonReceiptsBottom = null;
            }

            if (ButtonReceiptsHeight != null) {
                ButtonReceiptsHeight.Dispose ();
                ButtonReceiptsHeight = null;
            }

            if (ButtonSingInSignOut != null) {
                ButtonSingInSignOut.Dispose ();
                ButtonSingInSignOut = null;
            }

            if (ButtonYourAccount != null) {
                ButtonYourAccount.Dispose ();
                ButtonYourAccount = null;
            }

            if (ButtonYourAccountBottom != null) {
                ButtonYourAccountBottom.Dispose ();
                ButtonYourAccountBottom = null;
            }

            if (ButtonYourAccountHeight != null) {
                ButtonYourAccountHeight.Dispose ();
                ButtonYourAccountHeight = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}