﻿using CoreGraphics;
using Foundation;
using System;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.AnonymousAccount;
using TfL.CongestionCharge.AppleiOS.Cells;
using TfL.CongestionCharge.AppleiOS.Tables;
using TfL.CongestionCharge.AppleiOS.ViewControllers;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class AnonymousUserYourVehiclesViewController : BaseViewController<AnonymousAccountViewModel>
    {
        public AnonymousUserYourVehiclesViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            TableViewYourVehicleHeight.Constant = TableViewYourVehicle.ContentSize.Height;
            ViewContainer.LayoutIfNeeded();
            ScrollView.ContentSize = new CGSize(ScrollView.ContentSize.Width, ViewContainer.Frame.Height);
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.ManageStoredVehicles);

            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            SetTableViewStyle();
            BindData();
        }

        private void BindData()
        {
            TableViewYourVehicle.Source = new YourVehiclesTableViewSource(() => ViewModel.UserInformation.Vehicles, RemoveVehicle);
            SetTextFieldText();
        }

        private async void RemoveVehicle(VehicleViewModel v)
        {
            await ViewModel.RemoveVehicle(v);
            TableViewYourVehicle.ReloadData();
            SetTextFieldText();
        }

        private void SetTextFieldText()
        {
            if (ViewModel.UserInformation.Vehicles.Count == 0)
            {
                LabelPageDescription.Text = Strings.NoVehiclesOnAnonAccount;
                TableViewYourVehicleTop.Constant = 0;
            }
        }

        private void SetTableViewStyle()
        {
            TableViewYourVehicle.RegisterNibForCellReuse(YourVehicleTableViewCell.Nib, YourVehicleTableViewCell.Key);
            TableViewYourVehicle.RowHeight = UITableView.AutomaticDimension;
            TableViewYourVehicle.EstimatedRowHeight = 140f;
            TableViewYourVehicle.ReloadData();
            TableViewYourVehicle.TableFooterView = new UIView(CGRect.Empty);
            TableViewYourVehicle.TableHeaderView = new UIView(CGRect.Empty);
            TableViewYourVehicle.BackgroundColor = UIColor.White;
            TableViewYourVehicle.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            TableViewYourVehicle.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }
    }
}