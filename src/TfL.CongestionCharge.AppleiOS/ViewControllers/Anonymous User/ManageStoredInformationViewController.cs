﻿using GalaSoft.MvvmLight.Helpers;
using System;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.AnonymousAccount;
using TfL.CongestionCharge.AppleiOS.Models;
using TfL.CongestionCharge.AppleiOS.Utilities;
using TfL.CongestionCharge.AppleiOS.ViewControllers;

namespace TfL.CongestionCharge.AppleiOS
{
    public partial class ManageStoredInformationViewController : BaseViewController<AnonymousAccountViewModel>
    {
        public ManageStoredInformationViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            SetUpNavigationBar(Strings.YourVehiclesAndCards);
            ButtonYourVehicle.SetGrayButtonStyle(UiConstants.BlueTextColor, true);
            ButtonYourCreditCards.SetGrayButtonStyle(UiConstants.BlueTextColor, true);
            ButtonPrivacyPolicy.SetCommand(Events.TouchUpInside, ViewModel.NavigateToPrivacyCommand);
            ButtonTermAndCondition.SetCommand(Events.TouchUpInside, ViewModel.NavigateToTcCommand);
            ButtonYourVehicle.SetCommand(Events.TouchUpInside, ViewModel.NavigateToVehiclesCommand);
            ButtonYourCreditCards.SetCommand(Events.TouchUpInside, ViewModel.NavigateToYourPaymentMethodsCommand);
        }
    }
}