// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register ("ManageStoredInformationViewController")]
    partial class ManageStoredInformationViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonArrowNextYourCreditCard { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonArrowNextYourVehicle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonPrivacyPolicy { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.CustomButton ButtonTermAndCondition { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonYourCreditCards { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonYourVehicle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.PageHeaderLabel LabelPageHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        TfL.CongestionCharge.AppleiOS.CustomLabel.ViewWithRoundedCorners ViewContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonArrowNextYourCreditCard != null) {
                ButtonArrowNextYourCreditCard.Dispose ();
                ButtonArrowNextYourCreditCard = null;
            }

            if (ButtonArrowNextYourVehicle != null) {
                ButtonArrowNextYourVehicle.Dispose ();
                ButtonArrowNextYourVehicle = null;
            }

            if (ButtonPrivacyPolicy != null) {
                ButtonPrivacyPolicy.Dispose ();
                ButtonPrivacyPolicy = null;
            }

            if (ButtonTermAndCondition != null) {
                ButtonTermAndCondition.Dispose ();
                ButtonTermAndCondition = null;
            }

            if (ButtonYourCreditCards != null) {
                ButtonYourCreditCards.Dispose ();
                ButtonYourCreditCards = null;
            }

            if (ButtonYourVehicle != null) {
                ButtonYourVehicle.Dispose ();
                ButtonYourVehicle = null;
            }

            if (LabelPageHeader != null) {
                LabelPageHeader.Dispose ();
                LabelPageHeader = null;
            }

            if (ViewContainer != null) {
                ViewContainer.Dispose ();
                ViewContainer = null;
            }
        }
    }
}