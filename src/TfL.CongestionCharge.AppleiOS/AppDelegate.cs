﻿using System;
using Foundation;
using Microsoft.Azure.Mobile;
using Microsoft.Azure.Mobile.Analytics;
using Microsoft.Azure.Mobile.Crashes;
using Microsoft.Azure.Mobile.Distribute;
using ObjCRuntime;
using PCLStorage;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Serialization.Implementation;
using Tfl.CongestionCharge.Core.Storage;
using TfL.CongestionCharge.AppleiOS.Environment;
using TfL.CongestionCharge.AppleiOS.KeyProvider;
using TfL.CongestionCharge.AppleiOS.Network;
using UIKit;
using Version.Plugin;

namespace TfL.CongestionCharge.AppleiOS
{
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        public EventHandler AppBecomeActiveAgain;

        private NSObject _notificationHandle;

        public static AppDelegate Current { get; private set; }

        public bool AppComesFromBackgroud { get; set; }

        public UIColor DarkGreyBlue { get; } = UIColor.FromRGB(45, 48, 57);

        public UIColor LightGrey { get; } = UIColor.FromRGB(237, 237, 237);

        public UIColor PrimaryBlue { get; } = UIColor.FromRGB(26, 90, 146);

        public UIColor LightBlue { get; } = UIColor.FromRGB(135, 167, 195);

        public UIColor Turquoise { get; } = UIColor.FromRGB(102, 204, 204);

        public UIColor DarkGrey { get; } = UIColor.FromRGB(238, 238, 238);

        public nfloat ScreenX => UIScreen.MainScreen.Bounds.Width;

        public bool Retina => UIScreen.MainScreen.RespondsToSelector(new Selector("scale"));

        public nfloat ScreenY => UIScreen.MainScreen.Bounds.Height;

        public bool IsIPhone => UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone;

        public override UIWindow Window { get; set; }

        public bool IsConnected { get; set; }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
#if ENABLE_TEST_CLOUD
            Xamarin.Calabash.Start();
#endif

            Distribute.DontCheckForUpdatesInDebug();
            MobileCenter.Start(Sharedkeys.AnalyticsAppKeys.IosApplicationKey, typeof(Analytics), typeof(Crashes), typeof(Distribute));

            Current = this;

            var cryptoKeyStore = new CryptoKeyStore();
            cryptoKeyStore.StoreKey();

            var controller = Window.RootViewController as UINavigationController;
            var nav = new AppNavigationService(controller);

            // todo IOS path
            var encryptedKeyValueStore = new BinarySerializedFileStore(new EncryptedBinarySerializer(cryptoKeyStore), FileSystem.Current.LocalStorage);

            AppEnvironment.Init(
                new VersionImplementation().Version,
                nav,
                new UserDialog(),
                new HttpClientFactory(),
                encryptedKeyValueStore);

            return true;
        }

        public override void OnResignActivation(UIApplication application)
        {
            var blankViewController = new UIViewController { View = { BackgroundColor = UIColor.Black } };
            Window?.RootViewController.PresentViewController(blankViewController, false, null);

            var dateWithTimeIntervalSinceNow = NSDate.FromTimeIntervalSinceNow(0.5);
            NSRunLoop.Current.RunUntil(dateWithTimeIntervalSinceNow);

            application.IgnoreSnapshotOnNextApplicationLaunch();
        }

        public override void DidEnterBackground(UIApplication application)
        {
            AppComesFromBackgroud = false;
            if (_notificationHandle != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_notificationHandle);
                _notificationHandle = null;
            }
        }

        public override void WillEnterForeground(UIApplication application)
        {
            AppComesFromBackgroud = true;
            Current.AppBecomeActiveAgain?.Invoke(null, null);
        }

        public override void OnActivated(UIApplication application)
        {
            Window?.RootViewController.DismissViewController(false, null);
            if (_notificationHandle == null)
            {
                _notificationHandle = NSNotificationCenter.DefaultCenter.AddObserver(
                UIApplication.UserDidTakeScreenshotNotification,
               ScreenShotTaken);
            }
        }

        public override void WillTerminate(UIApplication application)
        {
        }

        private static async void ScreenShotTaken(NSNotification obj)
        {
            var appservice = new UserDialog();
            await appservice.ShowMessage("Screenshot has been taken.", Strings.Information);
        }
    }
}