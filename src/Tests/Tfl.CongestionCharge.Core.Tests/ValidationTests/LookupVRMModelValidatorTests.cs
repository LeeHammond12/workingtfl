﻿using NUnit.Framework;
using Tfl.CongestionCharge.Core.Tests.ViewModelTests;
using Tfl.CongestionCharge.Core.Validators;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.Tests.ValidationTests
{
    [TestFixture]
    public class LookupVRMModelValidatorTests : ViewModelTestFixtureBase
    {
        [Test]
        public void Validation_Fails()
        {
            Environment.FlowState.StartLookupVrmFlow(LookupVrmFlowState.FlowType.CcPayment);

            var model = new LookupVrmViewModel(Environment)
            {
                EnteredVrm = "ATestVrm",
            };

            var validator = new LookupVrmViewModelValidator();
            var result = validator.Validate(model);
            Assert.IsTrue(result.IsValid);

            model.EnteredVrm = "ATest Vrm";
            result = validator.Validate(model);
            Assert.IsTrue(result.IsValid);

            model.EnteredVrm = "ATest!Vrm";
            result = validator.Validate(model);
            Assert.IsFalse(result.IsValid);

            model.EnteredVrm = "$^&^$";
            result = validator.Validate(model);
            Assert.IsFalse(result.IsValid);
        }
    }
}