﻿using NSubstitute;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.Tests.ViewModelTests
{
    public class ViewModelTestFixtureBase
    {
        protected Fixture Fixture { get; set; }

        protected IUserDialog MockUserDialog { get; set; }

        protected IAppNavigationService MockNavigationService { get; set; }

        protected IPersist<AppStateModel> MockAppStatePersist { get; private set; }
        protected IPersist<FlowStatesModel> MockFlowStatePersist { get; private set; }

        protected IAuthService MockAuthService { get; private set; }

        protected AppEnvironment Environment { get; private set; }

        protected ITflApi TflApi => Environment.TflApi;

        [SetUp]
        public void Setup_context()
        {
            Fixture = new Fixture();
            MockUserDialog = Substitute.For<IUserDialog>();
            MockNavigationService = Substitute.For<IAppNavigationService>();
            MockAppStatePersist = Substitute.For<IPersist<AppStateModel>>();
            MockFlowStatePersist = Substitute.For<IPersist<FlowStatesModel>>();
            MockAuthService = Substitute.For<IAuthService>();

            AppEnvironment.UnitTestInit(
                MockNavigationService,
                MockUserDialog,
                MockAppStatePersist,
                MockFlowStatePersist,
                MockAuthService);

            Environment = AppEnvironment.Current;

            Environment.TflApi = Substitute.For<ITflApi>();
        }
    }
}