﻿using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Tests.ViewModelTests
{
    [TestFixture]
    public class PostCodeViewModelTests : ViewModelTestFixtureBase
    {
        private readonly string _validPostCode = "YO24 1AX";

        [Test]
        public void Get_PostCode_Code_Details_Populates_ViewModel_Correctly_For_Outside_ResidentZone_Outside_CcZone()
        {
            var response = new PostcodeDetailDto { InsideCcZone = false, InsideResidentsZone = false };

            TflApi.GetPostcodeDetailByBuildingNumber(_validPostCode, Arg.Any<string>())
                .ReturnsForAnyArgs(Task.FromResult(response));
            var sut = GetDefaultMockViewModel();
            sut.BuildingNumber = "42";
            sut.LookupPostCodeCommand.Execute(null);

            Assert.AreEqual(Strings.AddressOutsideCcZone, sut.InsideCcZoneResult);
            Assert.AreEqual(Strings.AddressOutsideResidentZone, sut.InsideResidentZoneResult);
            Assert.AreEqual(false, sut.IsBusy);
        }

        [Test]
        public void Get_PostCode_Code_Details_Populates_ViewModel_Correctly_For_Inside_ResidentZone_Outside_CcZone()
        {
            var response = new PostcodeDetailDto { InsideCcZone = false, InsideResidentsZone = true };
            TflApi.GetPostcodeDetailByBuildingNumber(_validPostCode, Arg.Any<string>())
                .ReturnsForAnyArgs(Task.FromResult(response));
            var sut = GetDefaultMockViewModel();
            sut.BuildingNumber = "42";
            sut.LookupPostCodeCommand.Execute(null);

            Assert.AreEqual(Strings.AddressOutsideCcZone, sut.InsideCcZoneResult);
            Assert.AreEqual(Strings.AddressInsideResidentZone, sut.InsideResidentZoneResult);
            Assert.AreEqual(false, sut.IsBusy);
        }

        [Test]
        public void Get_PostCode_Code_Details_Populates_ViewModel_Correctly_For_Outside_ResidentZone_Inside_CcZone()
        {
            var response = new PostcodeDetailDto { InsideCcZone = true, InsideResidentsZone = false };
            TflApi.GetPostcodeDetailByBuildingNumber(_validPostCode, Arg.Any<string>())
                .ReturnsForAnyArgs(Task.FromResult(response));
            var sut = GetDefaultMockViewModel();
            sut.BuildingNumber = "42";
            sut.LookupPostCodeCommand.Execute(null);

            Assert.AreEqual(Strings.AddressInsideCcZone, sut.InsideCcZoneResult);
            Assert.AreEqual(Strings.AddressOutsideResidentZone, sut.InsideResidentZoneResult);
            Assert.AreEqual(false, sut.IsBusy);
        }

        [Test]
        public void Get_PostCode_Code_Details_Populates_ViewModel_Correctly_For_Inside_ResidentZone_Inside_CcZone()
        {
            var response = new PostcodeDetailDto { InsideCcZone = true, InsideResidentsZone = true };
            TflApi.GetPostcodeDetailByBuildingNumber(_validPostCode, Arg.Any<string>())
                .ReturnsForAnyArgs(Task.FromResult(response));
            var sut = GetDefaultMockViewModel();
            sut.BuildingNumber = "42";
            sut.LookupPostCodeCommand.Execute(null);

            Assert.AreEqual(Strings.AddressInsideCcZone, sut.InsideCcZoneResult);
            Assert.AreEqual(Strings.AddressInsideResidentZone, sut.InsideResidentZoneResult);
            Assert.AreEqual(false, sut.IsBusy);
        }

        [Test]
        public void Get_PostCode_Code_Details_Calls_Correct_Dialogue_Service_If_Address_Not_Found()
        {
            var sut = new PostCodeCheckerViewModel(Environment)
            {
                PostCode = _validPostCode,
                BuildingNumber = "42" 
            };

            sut.LookupPostCodeCommand.Execute(null);

            MockUserDialog.Received().ShowError(Strings.CouldNotFindAddress);

            Assert.IsTrue(string.IsNullOrEmpty(sut.InsideCcZoneResult));
            Assert.IsTrue(string.IsNullOrEmpty(sut.InsideResidentZoneResult));
        }

        [Test]
        public void LookupPostCodeCommand_Cannot_Execute_Without_PostCode_And_Address_Set()
        {
            var sut = GetDefaultMockViewModel();
            sut.PostCode = string.Empty;

            var canExecute = sut.LookupPostCodeCommand.CanExecute(Arg.Any<object>());
            Assert.IsFalse(canExecute);

            sut.BuildingNumber = Fixture.Create<string>();
            canExecute = sut.LookupPostCodeCommand.CanExecute(Arg.Any<object>());
            Assert.IsFalse(canExecute);

            sut.PostCode = _validPostCode;
            canExecute = sut.LookupPostCodeCommand.CanExecute(Arg.Any<object>());
            Assert.IsTrue(canExecute);
        }

        private PostCodeCheckerViewModel GetDefaultMockViewModel()
        {
            return new PostCodeCheckerViewModel(Environment)
            {
                PostCode = _validPostCode
            };
        }
    }
}