﻿using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.ViewModel.Main;

namespace Tfl.CongestionCharge.Core.Tests.ViewModelTests
{
    public class MainViewModelTests : ViewModelTestFixtureBase
    {
        [Test]
        public async Task Navigate_to_VRM_calls_navigation_service_correctly()
        {
            var sut = new MainViewModel(Environment);
            await sut.InitViewModel();
            sut.NavigateToPayCcCommand.Execute(null);
            MockNavigationService.Received().NavigateTo(AppPage.LookupVrm, Arg.Any<object>());
        }

        [Test]
        public async Task Navigate_to_PCN_calls_navigation_service_correctly()
        {
            var sut = new MainViewModel(Environment);
            await sut.InitViewModel();
            sut.NavigateToPCNLookupCommand.Execute(null);
            MockNavigationService.Received().NavigateTo(AppPage.PcnLookup, Arg.Any<object>());
        }

        [Test]
        public async Task Navigate_to_Postcode_calls_navigation_service_correctly()
        {
            var sut = new MainViewModel(Environment);
            await sut.InitViewModel();
            sut.NavigateToPostCodeLookupCommand.Execute(null);
            MockNavigationService.Received().NavigateTo(AppPage.PostCodeChecker, Arg.Any<object>());
        }
    }
}