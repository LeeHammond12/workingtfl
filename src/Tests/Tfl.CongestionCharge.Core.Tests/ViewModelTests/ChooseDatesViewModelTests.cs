﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Newtonsoft.Json;
using NSubstitute;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.PayCc;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;

namespace Tfl.CongestionCharge.Core.Tests.ViewModelTests
{
    [TestFixture]
    public class ChooseDatesViewModelTests : ViewModelTestFixtureBase
    {
        [Test]
        public async Task Populate_Charges_Populates_Correctly_If_LookupCongestionChargesAsync_Returns_Null()
        {
            var payment = Environment.FlowState.StartCcPaymentFlow();
            payment.Cc.StartNewCcJourney();

            var viewModel = new ChooseDatesViewModel(AppEnvironment.Current);

            await viewModel.PopulateCharges();

            viewModel.Yesterday.Should().BeNull();
            viewModel.Daily.Should().BeNull();
            viewModel.Weekly.Should().BeNull();
            viewModel.Monthly.Should().BeNull();
        }

        [Test]
        public async Task
            Populate_Charges_Populates_Correctly_If_LookupCongestionChargesAsync_Returns_Null_Charge_Lists()
        {
            var response = new CcDetail();
            TflApi.LookupCongestionCharges(Arg.Any<string>(), Arg.Any<string>(), true)
                .Returns(Task.FromResult(response));

            Environment.FlowState.StartCcPaymentFlow();

            var viewModel = new ChooseDatesViewModel(Environment)
            {
                StartDate = DateTime.Today,
            };

            viewModel.Payment.Cc.AddNewCcJourney(new VehicleViewModel(), true);

            await viewModel.PopulateCharges();

            viewModel.Yesterday.Should().BeNull();
            viewModel.Daily.Should().BeNull();
            viewModel.Weekly.Should().BeNull();
            viewModel.Monthly.Should().BeNull();
        }

        [Test]
        public async Task Selecting_charge_populates_journey_correctly()
        {
            var res = @" 
{
  ""AccountNumber"":""2000263119"",
  ""CcChargeList"":{
    ""CcCharge"":[
      {
        ""ChargeStatus"":""Full"",
        ""Daily"":{
          ""Date"":""2017-07-25T12:00:00Z"",
          ""Amount"":11.50,
          ""CcPaid"":false,
          ""LezPaid"":false
        }
      },
      {
        ""ChargeStatus"":""Full"",
        ""PayNextDay"":{
          ""Date"":""2017-07-24T12:00:00Z"",
          ""Amount"":14.00,
          ""CcPaid"":false,
          ""LezPaid"":false
        }
      },
      {
        ""ChargeStatus"":""Full"",
        ""Weekly"":{
          ""StartDate"":""2017-07-25T12:00:00Z"",
          ""EndDate"":""2017-07-31T12:00:00Z"",
          ""Amount"":57.50,
          ""CcPaid"":false
        }
      },
      {
        ""ChargeStatus"":""Full"",
        ""Monthly"":{
          ""StartDate"":""2017-07-25T12:00:00Z"",
          ""EndDate"":""2017-08-21T12:00:00Z"",
          ""Amount"":230.00,
          ""CcPaid"":false
        }
      }
    ]
  },
}";
            var response = JsonConvert.DeserializeObject<CcDetail>(res);
            TflApi.LookupCongestionCharges(Arg.Any<string>(), Arg.Any<string>(), true)
                .Returns(Task.FromResult(response));

            Environment.FlowState.StartCcPaymentFlow();

            var viewModel = new ChooseDatesViewModel(Environment);
            viewModel.Payment.Cc.AddNewCcJourney(new VehicleViewModel(), true);

            await viewModel.PopulateCharges();

            viewModel.Yesterday.Should().NotBeNull();
            viewModel.Yesterday.Date.Should().Be(new DateTime(2017, 7, 24, 12, 0, 0));
            viewModel.Yesterday.ChargePeriod.Should().Be(ChargePeriod.PayNextDay);
            viewModel.Yesterday.CcPaid.Should().BeFalse();
            viewModel.Yesterday.Amount.Should().Be(14m);

            viewModel.Daily.Should().NotBeNull();
            viewModel.Daily.Date.Should().Be(new DateTime(2017, 7, 25, 12, 0, 0));
            viewModel.Daily.ChargePeriod.Should().Be(ChargePeriod.Daily);
            viewModel.Daily.CcPaid.Should().BeFalse();
            viewModel.Daily.Amount.Should().Be(11.5m);

            viewModel.Weekly.Should().NotBeNull();
            viewModel.Weekly.StartDate.Should().Be(new DateTime(2017, 7, 25, 12, 0, 0));
            viewModel.Weekly.EndDate.Should().Be(new DateTime(2017, 7, 31, 12, 0, 0));
            viewModel.Weekly.ChargePeriod.Should().Be(ChargePeriod.Weekly);
            viewModel.Weekly.CcPaid.Should().BeFalse();
            viewModel.Weekly.Amount.Should().Be(57.5m);

            viewModel.Monthly.Should().NotBeNull();
            viewModel.Monthly.StartDate.Should().Be(new DateTime(2017, 7, 25, 12, 0, 0));
            viewModel.Monthly.EndDate.Should().Be(new DateTime(2017, 8, 21, 12, 0, 0));
            viewModel.Monthly.ChargePeriod.Should().Be(ChargePeriod.Monthly);
            viewModel.Monthly.CcPaid.Should().BeFalse();
            viewModel.Monthly.Amount.Should().Be(230m);

            await viewModel.SelectCharge(viewModel.Weekly);

            var journey = viewModel.Journey;

            journey.PeriodType.Should().Be(ChargePeriod.Weekly);
            journey.ConsecutiveDaysString.Should().Be(5 + Strings.ConsecutiveChargingDays);
            journey.ChargeStartDate.Should().Be(new DateTime(2017, 7, 25, 12, 0, 0));
            journey.ChargeEndDate.Should().Be(new DateTime(2017, 7, 31, 12, 0, 0));
            journey.Price.Should().Be(57.5m);

            await viewModel.SelectCharge(viewModel.Monthly);

            journey.PeriodType.Should().Be(ChargePeriod.Monthly);
            journey.ConsecutiveDaysString.Should().Be(20 + Strings.ConsecutiveChargingDays);
            journey.ChargeStartDate.Should().Be(new DateTime(2017, 7, 25, 12, 0, 0));
            journey.ChargeEndDate.Should().Be(new DateTime(2017, 8, 21, 12, 0, 0));
            journey.Price.Should().Be(230m);

            await viewModel.SelectCharge(viewModel.Daily);

            journey.PeriodType.Should().Be(ChargePeriod.Daily);
            journey.ConsecutiveDaysString.Should().Be(string.Empty);
            journey.ChargeStartDate.Should().Be(new DateTime(2017, 7, 25, 12, 0, 0));
            journey.ChargeEndDate.Should().Be(new DateTime(2017, 7, 25, 12, 0, 0));
            journey.Price.Should().Be(11.5m);

            await viewModel.SelectCharge(viewModel.Yesterday);

            journey.PeriodType.Should().Be(ChargePeriod.PayNextDay);
            journey.ConsecutiveDaysString.Should().Be(string.Empty);
            journey.ChargeStartDate.Should().Be(new DateTime(2017, 7, 24, 12, 0, 0));
            journey.ChargeEndDate.Should().Be(new DateTime(2017, 7, 24, 12, 0, 0));
            journey.Price.Should().Be(14m);
        }

        [Test]
        public async Task Selecting_charge_populates_journey_correctly_for_when_CcChargeDays_is_set_to_17()
        {
            var res = @"
{
  ""AccountNumber"":""1001552751"",
  ""CcChargeList"":{
    ""CcCharge"":[
      {
        ""ChargeStatus"":""Discount"",
        ""Daily"":{
          ""Date"":""2017-07-26T12:00:00Z"",
          ""Amount"":1.15,
          ""CcPaid"":false,
          ""LezPaid"":false
        }
      },
      {
        ""ChargeStatus"":""Discount"",
        ""PayNextDay"":{
          ""Date"":""2017-07-25T12:00:00Z"",
          ""Amount"":14.00,
          ""CcPaid"":false,
          ""LezPaid"":false
        }
      },
      {
        ""ChargeStatus"":""Discount"",
        ""Weekly"":{
          ""StartDate"":""2017-07-26T12:00:00Z"",
          ""EndDate"":""2017-08-01T12:00:00Z"",
          ""Amount"":5.75,
          ""CcPaid"":false
        }
      },
      {
        ""ChargeStatus"":""Discount"",
        ""Custom"":{
          ""StartDate"":""2017-07-26T12:00:00Z"",
          ""EndDate"":""2017-08-17T12:00:00Z"",
          ""Amount"":19.55,
          ""CcPaid"":false
        }
      }
    ]
  },
  ""CcChargeDays"":17
}";

            var response = JsonConvert.DeserializeObject<CcDetail>(res);

            response.AccountNumber.Length.Should().BeGreaterThan(0);
            response.CcChargeDays.ShouldBeEquivalentTo(17);

            TflApi.LookupCongestionCharges(Arg.Any<string>(), Arg.Any<string>(), true)
                .Returns(Task.FromResult(response));

            Environment.FlowState.StartCcPaymentFlow();

            var viewModel = new ChooseDatesViewModel(Environment);
            viewModel.Payment.Cc.AddNewCcJourney(new VehicleViewModel(), true);

            await viewModel.PopulateCharges();

            viewModel.Yesterday.Should().NotBeNull();
            viewModel.Yesterday.Date.Should().Be(new DateTime(2017, 7, 25, 12, 0, 0));
            viewModel.Yesterday.ChargePeriod.Should().Be(ChargePeriod.PayNextDay);
            viewModel.Yesterday.CcPaid.Should().BeFalse();
            viewModel.Yesterday.Amount.Should().Be(14m);

            viewModel.Daily.Should().BeNull();

            viewModel.Weekly.Should().NotBeNull();
            viewModel.Weekly.ConsecutiveDaysString.Should().Be(5 + Strings.ConsecutiveChargingDays);
            viewModel.Weekly.StartDate.Should().Be(new DateTime(2017, 7, 26, 12, 0, 0));
            viewModel.Weekly.EndDate.Should().Be(new DateTime(2017, 8, 1, 12, 0, 0));
            viewModel.Weekly.ChargePeriod.Should().Be(ChargePeriod.Weekly);
            viewModel.Weekly.CcPaid.Should().BeFalse();
            viewModel.Weekly.Amount.Should().Be(5.75m);

            viewModel.Monthly.Should().NotBeNull();
            viewModel.Monthly.ConsecutiveDaysString.Should().Be(17 + Strings.ConsecutiveChargingDays);
            viewModel.Monthly.StartDate.Should().Be(new DateTime(2017, 7, 26, 12, 0, 0));
            viewModel.Monthly.EndDate.Should().Be(new DateTime(2017, 8, 17, 12, 0, 0));
            viewModel.Monthly.ChargePeriod.Should().Be(ChargePeriod.Monthly);
            viewModel.Monthly.CcPaid.Should().BeFalse();
            viewModel.Monthly.Amount.Should().Be(19.55m);

            await viewModel.SelectCharge(viewModel.Monthly);

            var journey = viewModel.Journey;

            journey.PeriodType.Should().Be(ChargePeriod.Monthly);
            journey.ConsecutiveDaysString.Should().Be(17 + Strings.ConsecutiveChargingDays);
            journey.ChargeStartDate.Should().Be(new DateTime(2017, 7, 26, 12, 0, 0));
            journey.ChargeEndDate.Should().Be(new DateTime(2017, 8, 17, 12, 0, 0));
            journey.Price.Should().Be(19.55m);
        }

        [Test]
        public async Task Selecting_charge_populates_journey_correctly_for_when_CcChargeDays_is_set_to_2()
        {
            var res = @"
{
  ""AccountNumber"":""1001551857"",
  ""CcChargeList"":{
    ""CcCharge"":[
      {
        ""ChargeStatus"":""Discount"",
        ""Daily"":{
          ""Date"":""2017-08-18T12:00:00Z"",
          ""Amount"":1.15,
          ""CcPaid"":false,
          ""LezPaid"":false
        }
      },
      {
        ""ChargeStatus"":""Discount"",
        ""PayNextDay"":{
          ""Date"":""2017-08-17T12:00:00Z"",
          ""Amount"":14.00,
          ""CcPaid"":false,
          ""LezPaid"":false
        }
      },
      {
        ""ChargeStatus"":""Discount"",
        ""Custom"":{
          ""StartDate"":""2017-08-18T12:00:00Z"",
          ""EndDate"":""2017-08-21T12:00:00Z"",
          ""Amount"":2.30,
          ""CcPaid"":false
        }
      }
    ]
  },
  ""CcChargeDays"":2
}";

            var response = JsonConvert.DeserializeObject<CcDetail>(res);

            response.AccountNumber.Length.Should().BeGreaterThan(0);
            response.CcChargeDays.ShouldBeEquivalentTo(2);

            TflApi.LookupCongestionCharges(Arg.Any<string>(), Arg.Any<string>(), true)
                .Returns(Task.FromResult(response));

            Environment.FlowState.StartCcPaymentFlow();

            var viewModel = new ChooseDatesViewModel(Environment);
            viewModel.Payment.Cc.AddNewCcJourney(new VehicleViewModel(), true);

            await viewModel.PopulateCharges();

            viewModel.Yesterday.Should().NotBeNull();
            viewModel.Yesterday.Date.Should().Be(new DateTime(2017, 8, 17, 12, 0, 0));
            viewModel.Yesterday.ChargePeriod.Should().Be(ChargePeriod.PayNextDay);
            viewModel.Yesterday.CcPaid.Should().BeFalse();
            viewModel.Yesterday.Amount.Should().Be(14m);

            viewModel.Daily.Should().BeNull();

            viewModel.Weekly.Should().NotBeNull();
            viewModel.Weekly.ConsecutiveDaysString.Should().Be(2 + Strings.ConsecutiveChargingDays);
            viewModel.Weekly.StartDate.Should().Be(new DateTime(2017, 8, 18, 12, 0, 0));
            viewModel.Weekly.EndDate.Should().Be(new DateTime(2017, 8, 21, 12, 0, 0));
            viewModel.Weekly.ChargePeriod.Should().Be(ChargePeriod.Weekly);
            viewModel.Weekly.CcPaid.Should().BeFalse();
            viewModel.Weekly.Amount.Should().Be(2.3m);

            viewModel.Monthly.Should().BeNull();

            await viewModel.SelectCharge(viewModel.Weekly);

            var journey = viewModel.Journey;

            journey.PeriodType.Should().Be(ChargePeriod.Weekly);
            journey.ConsecutiveDaysString.Should().Be(2 + Strings.ConsecutiveChargingDays);
            journey.ChargeStartDate.Should().Be(new DateTime(2017, 8, 18, 12, 0, 0));
            journey.ChargeEndDate.Should().Be(new DateTime(2017, 8, 21, 12, 0, 0));
            journey.Price.Should().Be(2.3m);
        }
    }
}
