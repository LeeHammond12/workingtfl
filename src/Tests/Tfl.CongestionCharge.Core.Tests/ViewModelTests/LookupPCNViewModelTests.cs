﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Model.PayPcn;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayPcn;

namespace Tfl.CongestionCharge.Core.Tests.ViewModelTests
{
    [TestFixture]
    public class LookupPCNViewModelTests : ViewModelTestFixtureBase
    {
        private const string ValidPcn = "AT12345678";

        [SetUp]
        public void SetupPcnTests()
        {
            Environment.FlowState.StartPcnPaymentFlow();
        }

        [Test]
        public void Lookup_PCN_Sets_Lookup_Response()
        {
            var sut = GetDefaultLookupPcnViewModel();
            var response = Fixture.Create<PcnDetailDto>();
            sut.PcnNumber = response.PCNNumber;
            sut.Amount = response.AmountDue;
            TflApi.LookupPcn(ValidPcn, Arg.Any<string>()).Returns(Task.FromResult(response));
            sut.LookupPcnOrGoToPaymentCommand.Execute(null);
            Assert.AreEqual(response.AmountDue, sut.Amount);
            Assert.AreEqual(response.PCNNumber, sut.PcnNumber);
            Assert.AreEqual(false, sut.IsBusy);
        }

        [Test]
        public void Lookup_PCN_Can_Execute_Is_Set()
        {
            var sut = GetDefaultLookupPcnViewModel();
            sut.PcnNumber = string.Empty;
            sut.VrmNumber = string.Empty;

            Assert.IsFalse(sut.LookupPcnOrGoToPaymentCommand.CanExecute(null));
            sut.PcnNumber = ValidPcn;
            sut.VrmNumber = Fixture.Create<string>();
            Assert.IsTrue(sut.LookupPcnOrGoToPaymentCommand.CanExecute(null));
            sut.VrmNumber = string.Empty;
            Assert.IsFalse(sut.LookupPcnOrGoToPaymentCommand.CanExecute(null));
        }

        [Test]
        public void Lookup_PCN_Not_Found_Sets_Amount_To_Zero()
        {
            var response = Fixture.Create<PcnDetailDto>();
            response.Errors = new List<ErrorResponseItem> { new ErrorResponseItem() { ErrorCode = "23" } };
            TflApi.LookupPcn(Arg.Any<string>(), Arg.Any<string>())
                .Throws(new ClientApiException(response));

            var sut = GetDefaultLookupPcnViewModel();

            sut.PcnNumber = ValidPcn;
            sut.VrmNumber = "VRM1";

            sut.LookupPcnOrGoToPaymentCommand.Execute(null);
            MockUserDialog.Received().ShowMessage(Arg.Any<string>(), Strings.Information);
            Assert.AreEqual(0, sut.Amount);
        }

        [Test]
        public void Lookup_PCN_Not_Found_Calls_Dialog_Service_Correctly()
        {
            var response = Fixture.Create<PcnDetailDto>();
            response.Errors = new List<ErrorResponseItem> { new ErrorResponseItem() { ErrorCode = "23" } };
            TflApi.LookupPcn(Arg.Any<string>(), Arg.Any<string>())
                .Throws(new ClientApiException(response));

            var sut = GetDefaultLookupPcnViewModel();
            sut.PcnNumber = ValidPcn;
            sut.VrmNumber = "VRM1";

            sut.LookupPcnOrGoToPaymentCommand.Execute(null);

            MockUserDialog.Received().ShowMessage(Strings.CouldntFindPCN, Strings.Information);
        }

        [Test]
        public void Lookup_PCN_Exempt_Calls_Dialog_Service_Correctly()
        {
            var sut = GetDefaultLookupPcnViewModel();
            var response = Fixture.Create<PcnDetailDto>();
            response.CanBePaid = false;
            sut.VrmNumber = Fixture.Create<string>();
            sut.PcnNumber = ValidPcn;

            TflApi.LookupPcn(Arg.Any<string>(), Arg.Any<string>()).Returns(Task.FromResult(response));
            sut.LookupPcnOrGoToPaymentCommand.Execute(null);
            MockUserDialog.Received().ShowOkMessageWithLink(Strings.YouCannotPayThisPcn, Strings.Warning);
        }

        [Test]
        public void Lookup_PCN_Not_Exempt_Sets_ViewModel_Correctly()
        {
            var sut = GetDefaultLookupPcnViewModel();
            var response = Fixture.Create<PcnDetailDto>();
            response.CanBePaid = true;
            sut.VrmNumber = Fixture.Create<string>();
            sut.PcnNumber = ValidPcn;

            TflApi.LookupPcn(Arg.Any<string>(), Arg.Any<string>()).Returns(Task.FromResult(response));
            sut.LookupPcnOrGoToPaymentCommand.Execute(null);

            Assert.IsTrue(sut.CanProceedToPayment);
            Assert.AreEqual(response.AmountDue, sut.Amount);
        }

        [Test]
        public void Enter_Payment_Command_Calls_Navigation_Service_Correctly()
        {
            var sut = GetDefaultLookupPcnViewModel();
            sut.PcnNumber = Fixture.Create<string>();
            sut.VrmNumber = "VRM1";
            sut.Amount = Fixture.Create<decimal>();
            sut.CanProceedToPayment = true;
            sut.Amount = 10;

            sut.LookupPcnOrGoToPaymentCommand.Execute(null);

            MockNavigationService.Received().NavigateTo(AppPage.Payment, Arg.Any<object>());
        }

        [Test]
        public void NavigateToPaymentCommand_Cannot_Execute_With_No_Amount_Set()
        {
            var sut = GetDefaultLookupPcnViewModel();
            sut.PcnNumber = Fixture.Create<string>();
            sut.Amount = 0;

            sut.LookupPcnOrGoToPaymentCommand.Execute(null);
            MockNavigationService.DidNotReceive().NavigateTo(AppPage.Payment, Arg.Any<object>());
        }

        private PcnLookupViewModel GetDefaultLookupPcnViewModel()
        {
            return new PcnLookupViewModel(Environment);
        }
    }
}