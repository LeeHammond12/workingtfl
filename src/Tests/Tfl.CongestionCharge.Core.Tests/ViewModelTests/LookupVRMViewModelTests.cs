﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.Tests.ViewModelTests
{
    [TestFixture]
    public class LookupVRMViewModelTests : ViewModelTestFixtureBase
    {
        private const string TestVRM = "AVRM";

        [Test]
        public void Lookup_VRM_Sets_Lookup_Response()
        {
            var sut = GetDefaultLookupVrmViewModel();
            var response = Fixture.Create<LookupVrmDto>();
            response.VRM = "ATestVRM";
            sut.State.Vehicle.Vrm = response.VRM;
            sut.EnteredVrm = response.VRM;
            TflApi.LookupVrm(Arg.Any<string>(), Arg.Any<bool>()).Returns(Task.FromResult(response));
            sut.LookupVRMCommand.Execute(null);
            Assert.AreEqual(response.Make, sut.Vehicle.Make);
            Assert.AreEqual(response.Model, sut.Vehicle.Model);
            Assert.AreEqual(false, sut.IsBusy);
        }

        [Test]
        public void Lookup_VRM_Can_Execute_Is_Set()
        {
            var sut = GetDefaultLookupVrmViewModel();
            Assert.IsFalse(sut.LookupVRMCommand.CanExecute(null));

            sut.State.Vehicle.Vrm = TestVRM;
            sut.EnteredVrm = TestVRM;
            Assert.IsTrue(sut.LookupVRMCommand.CanExecute(null));

            sut.State.Vehicle.Vrm = string.Empty;
            sut.EnteredVrm = string.Empty;
            Assert.IsFalse(sut.LookupVRMCommand.CanExecute(null));
        }

        [Test]
        public void Lookup_VRM_Not_Found_Sets_Vehicle_To_Empty()
        {
            var response = Fixture.Create<LookupVrmDto>();
            response.Errors = new List<ErrorResponseItem> { new ErrorResponseItem() { ErrorCode = "0133" } };
            TflApi.LookupVrm(Arg.Any<string>(), Arg.Any<bool>())
                .Throws(new ClientApiException(response));
            var sut = GetDefaultLookupVrmViewModel();

            sut.State.Vehicle.Vrm = TestVRM;
            sut.EnteredVrm = TestVRM;

            sut.LookupVRMCommand.Execute(null);
            MockUserDialog.Received().ShowMessage(Arg.Any<string>(), Arg.Any<string>());
            Assert.IsTrue(string.IsNullOrEmpty(sut.Vehicle?.Make));
            Assert.IsTrue(string.IsNullOrEmpty(sut.Vehicle?.Model));
        }

        [Test]
        public void Lookup_VRM_Not_Found_Calls_Dialog_Service_Correctly()
        {
            var response = Fixture.Create<LookupVrmDto>();
            response.Errors = new List<ErrorResponseItem> { new ErrorResponseItem() { ErrorCode = "0133" } };
            TflApi.LookupVrm(Arg.Any<string>(), Arg.Any<bool>())
                .Throws(new ClientApiException(response));
            var sut = GetDefaultLookupVrmViewModel();

            sut.State.Vehicle.Vrm = TestVRM;
            sut.EnteredVrm = TestVRM;

            sut.LookupVRMCommand.Execute(null);
            MockUserDialog.Received().ShowMessage(Strings.CouldntFindVehicle, Strings.Warning);
        }

        [Test]
        public void Lookup_VRM_Exempt_Calls_Dialog_Service_Correctly()
        {
            var sut = GetDefaultLookupVrmViewModel();
            var response = Fixture.Create<LookupVrmDto>();
            response.IsCcChargeable = false;
            sut.State.Vehicle.Vrm = response.VRM;
            sut.EnteredVrm = TestVRM;
            TflApi.LookupVrm(Arg.Any<string>(), Arg.Any<bool>()).Returns(Task.FromResult(response));
            sut.LookupVRMCommand.Execute(null);
            MockUserDialog.Received().ShowWarning(Strings.VehicleExemptMessage);
        }

        private LookupVrmViewModel GetDefaultLookupVrmViewModel()
        {
            Environment.FlowState.StartLookupVrmFlow(LookupVrmFlowState.FlowType.CcPayment);
            return new LookupVrmViewModel(Environment)
            {
                EnteredVrm = string.Empty,
            };
        }
    }
}