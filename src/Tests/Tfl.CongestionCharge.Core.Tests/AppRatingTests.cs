﻿using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel.Main;
using Version.Plugin.Abstractions;

namespace Tfl.CongestionCharge.Core.Tests.AppRatingTests
{
    public class AppRatingTests
    {
        private Fixture Fixture { get; set; }
        private IUserDialog MockUserDialog { get; set; }
        private IAppNavigationService MockNavigationService { get; set; }
        private IPersist<AppStateModel> MockAppStateRepo { get; set; }
        private IPersist<FlowStatesModel> MockFlowStatesRepo { get; set; }
        private IVersion MockVersion { get; set; }

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture();
            MockUserDialog = Substitute.For<IUserDialog>();
            MockAppStateRepo = Substitute.For<IPersist<AppStateModel>>();
            MockFlowStatesRepo = Substitute.For<IPersist<FlowStatesModel>>();
            MockVersion = Substitute.For<IVersion>();
            MockNavigationService = Substitute.For<IAppNavigationService>();
        }

        [Test]
        public async void DoesNotCallShowMessageOnFirstCall()
        {
            MockAppStateRepo.IsStored().ReturnsForAnyArgs(false);

            var appState = new AppState(MockAppStateRepo, MockVersion.Version);
            var sut = new AppRatingChecker(appState, MockUserDialog, MockNavigationService);

            await sut.CheckToShowReviewPrompt();

            await MockUserDialog.DidNotReceive().Show2ButtonMessage(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>());

            appState.StartupCounterForThisVersion.ShouldBeEquivalentTo(1);

            MockAppStateRepo.Received().Backup(Arg.Is<AppStateModel>(x => x.StartupCounterForThisVersion == 1));
        }

        [Test]
        public async void AsksForConfirmationOnThirdCall()
        {
            MockAppStateRepo.IsStored().ReturnsForAnyArgs(true);
            MockAppStateRepo.Restore().ReturnsForAnyArgs(
                new AppStateModel
                {
                    StartupCounterForThisVersion = 2,
                    CurrentVersion = string.Empty
                });

            MockUserDialog.Show2ButtonMessage(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
              .Returns(Task.FromResult(false));

            var appState = new AppState(MockAppStateRepo, MockVersion.Version);
            var sut = new AppRatingChecker(appState, MockUserDialog, MockNavigationService);

            await sut.CheckToShowReviewPrompt();

            appState.StartupCounterForThisVersion.Should().Be(3);
            appState.AskedToRateCounter.Should().Be(1);

            MockAppStateRepo.Received().Backup(Arg.Is<AppStateModel>(x => x.StartupCounterForThisVersion == 3 && x.AskedToRateCounter == 1));
        }

        [Test]
        public async void AsksUserToRateOnThirdCall()
        {
            MockAppStateRepo.IsStored().ReturnsForAnyArgs(true);
            MockAppStateRepo.Restore().ReturnsForAnyArgs(
                new AppStateModel
                {
                    StartupCounterForThisVersion = 2,
                    CurrentVersion = string.Empty
                });

            MockUserDialog.Show2ButtonMessage(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
              .Returns(Task.FromResult(true));

            var appState = new AppState(MockAppStateRepo, MockVersion.Version);
            var sut = new AppRatingChecker(appState, MockUserDialog, MockNavigationService);

            await sut.CheckToShowReviewPrompt();

            MockAppStateRepo.Received().Backup(Arg.Is<AppStateModel>(x => x.StartupCounterForThisVersion == 3 && x.AskedToRateCounter == 1 && x.UserHasRatedApp));
        }

        [Test]
        public async void AsksUserToRateOnTenthCall()
        {
            MockAppStateRepo.IsStored().ReturnsForAnyArgs(true);
            MockAppStateRepo.Restore().ReturnsForAnyArgs(new AppStateModel
            {
                StartupCounterForThisVersion = 12,
                CurrentVersion = string.Empty,
                AskedToRateCounter = 2,
            });

            MockUserDialog.Show2ButtonMessage(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
              .Returns(Task.FromResult(true));

            var appState = new AppState(MockAppStateRepo, MockVersion.Version);
            var sut = new AppRatingChecker(appState, MockUserDialog, MockNavigationService);

            await sut.CheckToShowReviewPrompt();

            MockAppStateRepo.Received().Backup(Arg.Is<AppStateModel>(x => x.StartupCounterForThisVersion == 13 && x.AskedToRateCounter == 3 && x.UserHasRatedApp));
        }

        [Test]
        public async void DoesntAskOnThe20ThCall()
        {
            MockAppStateRepo.IsStored().ReturnsForAnyArgs(true);
            MockAppStateRepo.Restore().ReturnsForAnyArgs(new AppStateModel
            {
                StartupCounterForThisVersion = 20,
                CurrentVersion = string.Empty,
                AskedToRateCounter = 2,
            });

            MockUserDialog.Show2ButtonMessage(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
              .Returns(Task.FromResult(true));

            var appState = new AppState(MockAppStateRepo, MockVersion.Version);
            var sut = new AppRatingChecker(appState, MockUserDialog, MockNavigationService);

            await sut.CheckToShowReviewPrompt();

            MockAppStateRepo.Received().Backup(
                Arg.Is<AppStateModel>(
                    x => x.StartupCounterForThisVersion == 21 &&
                    x.AskedToRateCounter == 2 &&
                    !x.UserHasRatedApp &&
                    x.CurrentVersion == string.Empty));
        }

        [Test]
        public async void ResetRatingIfAppVersionChanges()
        {
            MockAppStateRepo.IsStored().ReturnsForAnyArgs(true);
            MockAppStateRepo.Restore().ReturnsForAnyArgs(new AppStateModel
            {
                StartupCounterForThisVersion = 24,
                CurrentVersion = "oldVersion",
                AskedToRateCounter = 2,
                UserHasRatedApp = true
            });

            MockUserDialog.Show2ButtonMessage(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
              .Returns(Task.FromResult(true));

            var appState = new AppState(MockAppStateRepo, MockVersion.Version);
            var sut = new AppRatingChecker(appState, MockUserDialog, MockNavigationService);

            await sut.CheckToShowReviewPrompt();

            MockAppStateRepo.Received().Backup(Arg.Is<AppStateModel>(x => x.StartupCounterForThisVersion == 1 &&
                x.AskedToRateCounter == 0 &&
                !x.UserHasRatedApp &&
                x.CurrentVersion == string.Empty));
        }
    }
}