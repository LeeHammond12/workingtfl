﻿using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.Tests.DataTests
{
    using System;
    using System.Linq;
    using Core.Model;
    using Core.Model.Payments;
    using Helper;
    using NSubstitute;
    using NUnit.Framework;
    using PCLCrypto;
    using ViewModel.PayCc;

    [TestFixture]
    [Category("Integration")]
    public class SqLiteRepositoryTests
    {
        private readonly string _testDatabase = ":memory:";

        private ISqLiteConnectionFactory _factory;
        private ICryptoKeyStore _cryptoKeyStore;
        private SQLiteConnection _connection;

        [Test]
        public void TestSavingApplicationData()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            sqliteRepo.SaveModelData(new AppStateModel() { CurrentVersion = "1111", Id = 1 });

            var cards = sqliteRepo.GetData<AppStateModel>();
            Assert.AreEqual(1, cards.Id);
        }

        [Test]
        public void TestSavingStoredCard()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            sqliteRepo.SaveListData(new PaymentCardViewModel() { Last4Digits = "1111" });
            var count = sqliteRepo.GetList<PaymentCardViewModel>().Count(sc => sc.Last4Digits == "1111");
            Assert.AreEqual(1, count);
        }

        [Test]
        public void TestSavingReceipt()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            sqliteRepo.SaveListData(new StoredReceipt() { Days = 1, Price = 10m, StartDate = new DateTime(2010, 01, 01), Vrm = "AAA111" });
            var count = sqliteRepo.GetList<StoredReceipt>().Count(sc => sc.Vrm == "AAA111" && sc.Days == 1);
            Assert.AreEqual(1, count);
        }

        [Test]
        public void TestSavedVrms()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            sqliteRepo.SaveListData(new VehicleViewModel { Vrm = "A", Model = "B" });
            var count = sqliteRepo.GetList<VehicleViewModel>().Count(sc => sc.Vrm == "A" && sc.Model == "B");
            Assert.AreEqual(1, count);
        }

        [Test]
        public void TestRetrieveApplicationData()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            sqliteRepo.SaveModelData(new AppStateModel() { CurrentVersion = "1111", Id = 1 });

            var cards = sqliteRepo.GetData<AppStateModel>();
            Assert.AreEqual(1, cards.Id);
            Assert.AreEqual("1111", cards.CurrentVersion);
        }

        [Test]
        public void TestARetrievingStoredCards()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            sqliteRepo.SaveListData(new PaymentCardViewModel() { Last4Digits = "1111" });
            sqliteRepo.SaveListData(new PaymentCardViewModel() { Last4Digits = "2222" });

            var cards = sqliteRepo.GetList<PaymentCardViewModel>();
            Assert.AreEqual(2, cards.Count());
            Assert.AreEqual("1111", cards.First().Last4Digits);
            Assert.AreEqual("2222", cards.Skip(1).First().Last4Digits);
        }

        [Test]
        public void TestARetrievingReceipts()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            sqliteRepo.SaveListData(new StoredReceipt() { Days = 1, Price = 10m, StartDate = new DateTime(2010, 01, 01), Vrm = "AAA111" });
            sqliteRepo.SaveListData(new StoredReceipt() { Days = 2, Price = 20m, StartDate = new DateTime(2018, 01, 01), Vrm = "AAA112" });

            var receipts = sqliteRepo.GetList<StoredReceipt>();
            Assert.AreEqual(2, receipts.Count());
            var first = receipts.First();
            Assert.AreEqual(1, first.Days);
            Assert.AreEqual(10m, first.Price);
            Assert.AreEqual(new DateTime(2010, 01, 01), first.StartDate);
            Assert.AreEqual("AAA111", first.Vrm);
            Assert.AreEqual("AAA112", receipts.Skip(1).First().Vrm);
        }

        [Test]
        public void TestARetrievingSavedVrms()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            sqliteRepo.SaveListData(new VehicleViewModel { Vrm = "A", Model = "B" });
            sqliteRepo.SaveListData(new VehicleViewModel { Vrm = "C", Model = "D" });

            var receipts = sqliteRepo.GetList<VehicleViewModel>();
            Assert.AreEqual(2, receipts.Count());
            var first = receipts.First();
            Assert.AreEqual("A", first.Vrm);
            Assert.AreEqual("C", receipts.Skip(1).First().Vrm);
        }

        [Test]
        public void TestDeletingApplicationData()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            var appdataToDelete = new AppStateModel() { CurrentVersion = "1111", Id = 1 };
            sqliteRepo.SaveModelData(appdataToDelete);

            sqliteRepo.Delete(appdataToDelete);
            var data = sqliteRepo.GetData<AppStateModel>();
            Assert.AreEqual(0, data.Id);
            Assert.AreEqual(null, data.CurrentVersion);
        }

        [Test]
        public void TestDeletingStoredCards()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);
            var cardToDelete = new PaymentCardViewModel() { Last4Digits = "1111" };

            sqliteRepo.SaveListData(cardToDelete);
            sqliteRepo.SaveListData(new PaymentCardViewModel() { Last4Digits = "2222" });

            sqliteRepo.Delete(cardToDelete);
            var cards = sqliteRepo.GetList<PaymentCardViewModel>();
            Assert.AreEqual(1, cards.Count());
            Assert.AreEqual("2222", cards.First().Last4Digits);
        }

        [Test]
        public void TestDeletingReceipts()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);

            var receiptToDelete = new StoredReceipt() { Days = 1, Price = 10m, StartDate = new DateTime(2010, 01, 01), Vrm = "AAA111" };
            sqliteRepo.SaveListData(receiptToDelete);
            sqliteRepo.SaveListData(new StoredReceipt() { Days = 2, Price = 20m, StartDate = new DateTime(2018, 01, 01), Vrm = "AAA112" });

            sqliteRepo.Delete(receiptToDelete);
            var receipts = sqliteRepo.GetList<StoredReceipt>();
            Assert.AreEqual(1, receipts.Count());
            Assert.AreEqual(2, receipts.First().Days);
            Assert.AreEqual(20m, receipts.First().Price);
            Assert.AreEqual("AAA112", receipts.First().Vrm);
        }

        [Test]
        public void TestDeletingSavedVrms()
        {
            var sqliteRepo = new SqLiteRepository(_factory, _cryptoKeyStore);

            var vrmToDelete = new VehicleViewModel { Vrm = "A", Model = "B" };
            sqliteRepo.SaveListData(vrmToDelete);
            sqliteRepo.SaveListData(new VehicleViewModel { Vrm = "C", Model = "D" });

            sqliteRepo.Delete(vrmToDelete);
            var vrms = sqliteRepo.GetList<VehicleViewModel>();
            Assert.AreEqual(1, vrms.Count());
            Assert.AreEqual("C", vrms.First().Vrm);
            Assert.AreEqual("D", vrms.First().Model);
        }

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            _factory = Substitute.For<ISqLiteConnectionFactory>();
            _connection = new SQLiteConnection(_testDatabase);
            _cryptoKeyStore = new FakeCryptoKeyStore();
            _factory.GetConnection().Returns(_connection);
        }

        [SetUp]
        public void SetUp()
        {
            _connection.BeginTransaction();
        }

        [TearDown]
        public void TearDown()
        {
            _connection.Rollback();
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            _connection.Close();
        }
    }
}
