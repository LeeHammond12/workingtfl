﻿using Tfl.CongestionCharge.Core.PCLCrypto;

namespace Tfl.CongestionCharge.Core.Tests.DataTests
{
    public class FakeCryptoKeyStore : ICryptoKeyStore
    {
        public void StoreKey()
        {
            return;
        }

        public byte[] GetKey()
        {
            return CryptoUtilities.GetAes256KeyMaterial();
        }
    }
}
