﻿using System;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Model;

namespace Tfl.CongestionCharge.Core.Tests.Model
{
    [TestFixture]
    public class QuotedDecimalFormatConverterTests
    {
        private readonly QuotedDecimalFormatConverter _converter = new QuotedDecimalFormatConverter();

        [Test]
        public void CanConvertDecimals()
        {
            Assert.AreEqual(true, _converter.CanConvert(typeof(decimal)));
        }

        [Test]
        public void CanReadReturnsTrue()
        {
            Assert.AreEqual(false, _converter.CanRead);
        }

        [Test]
        public void ReadJsonThrows()
        {
            Assert.Throws<NotImplementedException>(() => _converter.ReadJson(null, null, null, null));
        }
    }
}
