﻿using Newtonsoft.Json;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Model;

namespace Tfl.CongestionCharge.Core.Tests.Model
{
    [TestFixture]
    public class PostcodeTests : SerializationTestBase
    {
        [Test]
        public void PostcodeDeserialization()
        {
            string text = @"{
""InsideCcZone"": ""true"",
""InsideResidentsZone"": ""false""
}";

            // Some clean up from what was in the spec.
            text = Clean(text);
            var result = JsonConvert.DeserializeObject<PostcodeDetailDto>(text);

            Assert.AreEqual(true, result.InsideCcZone);
            Assert.AreEqual(false, result.InsideResidentsZone);
        }

        [Test]
        public void PostcodeDeserializationWithTrue()
        {
            string text = @"{
""InsideCcZone"": ""true"",
""InsideResidentsZone"": ""true""
}";

            // Some clean up from what was in the spec.
            text = Clean(text);
            var result = JsonConvert.DeserializeObject<PostcodeDetailDto>(text);

            Assert.AreEqual(true, result.InsideCcZone);
            Assert.AreEqual(true, result.InsideResidentsZone);
        }
    }
}
