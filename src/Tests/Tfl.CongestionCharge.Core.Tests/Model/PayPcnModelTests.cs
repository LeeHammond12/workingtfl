﻿using System;
using Newtonsoft.Json;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model.PayPcn;

namespace Tfl.CongestionCharge.Core.Tests.Model
{
    [TestFixture]
    public class PayPcnModelTests : SerializationTestBase
    {
        [Test]
        public void PcnDetailDeserializationTest()
        {
            var text = @"{
                ""PcnNumber"": ""AA12345678"",
                ""PCNStatus"": ""ISSUED"",
                ""AmountDue"": ""20.50"",
                ""CanBePaid"": ""true"",
                ""CanNotBePaidReason"":""Invalid""
                }";
            var result = JsonConvert.DeserializeObject<PcnDetailDto>(text);
            Assert.AreEqual("AA12345678", result.PCNNumber);
            Assert.AreEqual("ISSUED", result.PCNStatus);
            Assert.AreEqual(20.5m, result.AmountDue);
            Assert.AreEqual(true, result.CanBePaid);
            Assert.AreEqual("Invalid", result.CanNotBePaidReason);
        }

        [Test]
        public void PcnPaymentSerializationTest()
        {
            var text = @"{
""PaymentAmount"": 500.00,
""PaymentDateTime"":""2015-05-05T09:00:00"",
""PaymentTransactionRef"": ""545""
}";
            PcnPaymentDto payment = new PcnPaymentDto
            {
                PaymentAmount = 500.00m,
                PaymentDateTime = new DateTime(2015, 05, 05, 9, 0, 0).ToSitDateFormat(),
                PaymentTransactionRef = "545"
            };

            text = Clean(text);

            var result = JsonConvert.SerializeObject(payment);
            Assert.AreEqual(text, result);
        }
    }
}