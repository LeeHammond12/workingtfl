﻿using System;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Api.Axis;
using Tfl.CongestionCharge.Core.Extensions;

namespace Tfl.CongestionCharge.Core.Tests.Model
{
    [TestFixture]
    public class GetInnerMostExceptionOfTypeTests : SerializationTestBase
    {
        private readonly Exception _sampleException =
            new PaymentApiException(
                new PaymentApiException(
                    new ClientApiException(
                        new ClientApiException(
                            new NoNetworkConnectionException()
                            {
                                Source = "4"
                            })
                        {
                            Source = "3"
                        })) // Source is taken from wrapped client exception
                {
                    Source = "1"
                })
            {
                Source = "0"
            };

        [Test]
        public void TestExceptionAtBegginning()
        {
            var ex = _sampleException.GetInnerMostExceptionOfType<PaymentApiException>();
            Assert.AreEqual("1", ex.Source);
        }

        [Test]
        public void TestExceptionInMiddle()
        {
            var ex = _sampleException.GetInnerMostExceptionOfType<ClientApiException>();
            Assert.AreEqual("3", ex.Source);
        }

        [Test]
        public void TestExceptionAtEnd()
        {
            var ex = _sampleException.GetInnerMostExceptionOfType<NoNetworkConnectionException>();
            Assert.AreEqual("4", ex?.Source);
        }

        [Test]
        public void TestExceptionNotPresent()
        {
            var ex = _sampleException.GetInnerMostExceptionOfType<IndexOutOfRangeException>();
            Assert.IsNull(ex);
        }
    }
}