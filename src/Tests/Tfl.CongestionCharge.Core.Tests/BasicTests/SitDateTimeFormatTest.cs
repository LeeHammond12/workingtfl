﻿using System;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Extensions;

namespace Tfl.CongestionCharge.Core.Tests.Model
{
    [TestFixture]
    public class SitDateTimeFormatTest
    {
        [Test]
        public void TestExceptionAtBegginning()
        {
            var dt = new DateTime(1980, 2, 05, 13, 37, 59);

            var sitString = dt.ToSitDateFormat();

            Assert.AreEqual(sitString, "1980-02-05T13:37:59");
        }
    }
}