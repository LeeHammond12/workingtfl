﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using Refit;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.PayCc;

namespace Tfl.CongestionCharge.Core.Tests.ApiTests
{
    [TestFixture]
    public class HttpClientApiTests
    {
       [Test]
        [ExpectedException(typeof(ClientApiException))]
        public async Task GetRequestAsync_throws_exception_on_failure_status_code()
        {
            var responseMessage = new HttpResponseMessage { StatusCode = HttpStatusCode.ServiceUnavailable };
            var client = GetHttpClientWithResponse(responseMessage);

            var ex = new TflApiExceptionDecorator(new BypassDecorator());
            var sut = new TflApiResponseHandler(ex, RestService.For<ITflApi>(client));

            await sut.LookupVrm("VRM1", false);
        }

        [Test]
        [ExpectedException(typeof(ClientApiException))]
        public async Task PostRequestAsync_throws_exception_on_failure_status_code()
        {
            var responseMessage = new HttpResponseMessage { StatusCode = HttpStatusCode.ServiceUnavailable };
            var client = GetHttpClientWithResponse(responseMessage);

            var ex = new TflApiExceptionDecorator(new BypassDecorator());
            var sut = new TflApiResponseHandler(ex, RestService.For<ITflApi>(client));

            await sut.PayCc(new CcPayment());
        }

        [Test]
        public async Task GetRequestAsync_does_not_throw_exception_on_special_status_codes()
        {
            foreach (var statusCode in new[] { HttpStatusCode.InternalServerError, HttpStatusCode.BadRequest, })
            {
                var response = new LookupVrmDto() { VRM = "VRM1" };
                var responseMessage = new HttpResponseMessage { StatusCode = statusCode, Content = new FakeHttpContent(JsonConvert.SerializeObject(response)) };
                var client = GetHttpClientWithResponse(responseMessage);

                var ex = new TflApiExceptionDecorator(new BypassDecorator());
                var sut = new TflApiResponseHandler(ex, RestService.For<ITflApi>(client));

                var vrm = await sut.LookupVrm("VRM1", false);
                vrm.VRM.Should().BeEquivalentTo(response.VRM);
            }
        }

        [Test]
        public async Task PostRequestAsync_does_not_throw_exception_on_special_status_codes()
        {
            foreach (var statusCode in new[] { HttpStatusCode.InternalServerError, HttpStatusCode.BadRequest, })
            {
                var response = new CcPaymentReceipt() { ReceiptId = "1234" };
                var responseMessage = new HttpResponseMessage { StatusCode = statusCode, Content = new FakeHttpContent(JsonConvert.SerializeObject(response)) };
                var client = GetHttpClientWithResponse(responseMessage);

                var ex = new TflApiExceptionDecorator(new BypassDecorator());
                var sut = new TflApiResponseHandler(ex, RestService.For<ITflApi>(client));

                var vrm = await sut.PayCc(new CcPayment());
                vrm.ReceiptId.Should().BeEquivalentTo(response.ReceiptId);
            }
        }

        [Test]
        [Ignore("this test takes a long time because of the retry mechanism")]
        public async Task GetRequestAsync_is_retried_on_error()
        {
            var messageHandler = new FakeHttpMessageHandler(new Exception("Uh oh"));
            var client = new HttpClient(messageHandler) { BaseAddress = new Uri("https://www.thumbmunkeys.com") };

            var ex = new TflApiExceptionDecorator(new BypassDecorator());
            var retry = new RetryDecorator(ex);
            var sut = new TflApiResponseHandler(retry, RestService.For<ITflApi>(client));

            try
            {
                await sut.LookupVrm("VRM1", false);
            }
            catch (Exception)
            {
            }
            messageHandler.CallCount.Should().Be(5);
        }

        [Test]
        [Ignore("this test takes a long time because of the retry mechanism")]
        public async Task PostRequestAsync_is_retried_on_error()
        {
            var messageHandler = new FakeHttpMessageHandler(new Exception("Uh oh"));
            var client = new HttpClient(messageHandler) { BaseAddress = new Uri("https://www.thumbmunkeys.com") };

            var ex = new TflApiExceptionDecorator(new BypassDecorator());
            var retry = new RetryDecorator(ex);
            var sut = new TflApiResponseHandler(retry, RestService.For<ITflApi>(client));

            try
            {
                await sut.PayCc(new CcPayment());
            }
            catch (Exception)
            {
            }
            messageHandler.CallCount.Should().Be(5);
        }

        private static HttpClient GetHttpClientWithResponse(HttpResponseMessage responseMessage)
        {
            var messageHandler = new FakeHttpMessageHandler(responseMessage);
            var client = new HttpClient(messageHandler)
            {
                BaseAddress = new Uri("https://www.thumbmunkeys.com")
            };
            return client;
        }
    }
}