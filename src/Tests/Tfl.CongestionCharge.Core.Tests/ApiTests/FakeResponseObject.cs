﻿using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Tests.ApiTests
{
    public class FakeResponseObject : ClientApiResponseDtoBase
    {
        public string StringProperty { get; set; }

        public byte ByteProperty { get; set; }

        public sbyte SByteProperty { get; set; }

        public int IntProperty { get; set; }

        public uint UIntProperty { get; set; }

        public short ShortProperty { get; set; }

        public ushort UShortProperty { get; set; }

        public double DoubleProperty { get; set; }

        public float FloatProperty { get; set; }

        public long LongProperty { get; set; }

        public bool BoolProperty { get; set; }

        public char CharProperty { get; set; }

        public decimal DecimalProperty { get; set; }
    }
}