﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Tfl.CongestionCharge.Core.Tests.ApiTests
{
    public class FakeHttpMessageHandler : HttpMessageHandler
    {
        private readonly Exception _ex;
        private readonly HttpResponseMessage _response;

        public int CallCount = 0;

        public FakeHttpMessageHandler(HttpResponseMessage response)
        {
            this._response = response;
        }

        public FakeHttpMessageHandler(Exception ex)
        {
            _ex = ex;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            CallCount++;

            if (_ex != null)
                throw _ex;

            var responseTask = new TaskCompletionSource<HttpResponseMessage>();
            responseTask.SetResult(_response);

            return responseTask.Task;
        }
    }
}