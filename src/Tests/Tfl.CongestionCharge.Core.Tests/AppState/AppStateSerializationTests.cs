﻿using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.Tests.Model
{
    [TestFixture]
    public class AppStateSerializationTests
    {
        [Test]
        public void Test_private_auth_state_and_get_only_lists_serialize_correctly()
        {
            var state = new AppStateModel { Auth = { AccessToken = "A" } };
            state.Vehicles.Add(new StoredVehicle()
            {
                Color = "red",
                Make = "nunno",
                Model = "xx",
                Vrm = "123"
            });

            state.PaymentCards.Add(new StoredCard()
            {
                Last4Digits = "1234",
            });

            var restored = JsonConvert.DeserializeObject<AppStateModel>(JsonConvert.SerializeObject(state));

            restored.ShouldBeEquivalentTo(state);
        }
    }
}
