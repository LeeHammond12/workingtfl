﻿using System;
using Tfl.CongestionCharge.Core.Resources;
using TfL.CongestionCharge.Droid;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace TfL.Android.UITest.Helpers
{
    public class TestMethods
    {
        public static void SaveReceipt(IApp app, string result)
        {
            app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            app.WaitForElementWithTimeout(nameof(Resource.Id.txtYourReceipt));
            app.WaitForElementWithTimeout(Strings.Home);
            app.Screenshot(result);
            app.Tap(nameof(Resource.Id.btnSaveReceipt), Strings.SaveReceipt);
        }

        public static void EnterVrmAndTapNext(IApp app, string vrm, bool expectWarning)
        {
            var button = app.WaitForAnyElement(nameof(Resource.Id.acceptButton), nameof(Resource.Id.buttonPayCC));

            if (button == nameof(Resource.Id.acceptButton))
            {
                HomeScreenDisplay(app);
                app.Tap(nameof(Resource.Id.buttonPayCC));
            }
            else if (button == nameof(Resource.Id.buttonPayCC))
            {
                app.Tap(nameof(Resource.Id.buttonPayCC));
                app.Tap(nameof(Resource.Id.btnAddNewVehicle));
            }
            else
            {
                throw new Exception("Not Found");
            }

            app.WaitForElementWithTimeout(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);

            app.EnterText(nameof(Resource.Id.editTextVRM), vrm);

            app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);

            app.DismissKeyboard();
            app.Screenshot("Enter Vehicle Details");

            app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);

            if (expectWarning)
            {
                app.Screenshot("Warning");
                app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);
            }

            if (!expectWarning && app.ElementExists(nameof(Resource.Id.btnOk)))
            {
                throw new Exception("'Vehicle not found' warning shown, even though it shouldn't");
            }
        }

        public static void HomeScreenDisplay(IApp app)
        {
            app.WaitForElement(nameof(Resource.Id.acceptButton), Strings.AcceptTermsAndCondition);
            app.Screenshot(Strings.TermsAndConditionTitle);

            app.Tap(nameof(Resource.Id.acceptButton), Strings.AcceptTermsAndCondition);

            app.WaitForElementWithText(nameof(Resource.Id.btnCancel), Strings.No);

            app.Screenshot(Strings.LoginToYourAccount);

            app.Tap(nameof(Resource.Id.btnCancel), Strings.No);
            app.WaitForElement(nameof(Resource.Id.buttonSignIn));
            app.Screenshot(Strings.ApplicationName);
        }

        public static void ConfirmVrmAndSaveTapChooseDate(IApp app)
        {
            app.WaitForElement(Strings.ChooseDates);

            app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            app.Tap(nameof(Resource.Id.chkSaveVehicle), Strings.SaveVehicleDetails);

            app.Screenshot("Vehicle details");

            app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            app.WaitForElement(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);
            app.Screenshot("Choose your dates (pay For?)");
        }

        public static void ConfirmVrmAndTapChooseDate(IApp app)
        {
            app.WaitForElement(Strings.ChooseDates);
            app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            app.Screenshot("Vehicle details");

            app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            app.WaitForElement(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);
            app.Screenshot("Choose your dates (pay For?)");
        }

        public static void SelectDailyCongestionCharge(IApp app)
        {
            app.Tap(nameof(Resource.Id.txtDayDescription), Strings.DailyCongestionCharge);
            app.WaitForElement(nameof(Resource.Id.txtJourneyDetails), "Journey Details");
            app.Tap(nameof(Resource.Id.btnConfirmDetailsAndPay));
            app.WaitForElement(nameof(Resource.Id.txtPCCTitle), "Payment details");
        }

        public static void PcnDetailsScreenDisplay(IApp app)
        {
            var button = app.WaitForAnyElement(nameof(Resource.Id.acceptButton), nameof(Resource.Id.buttonPayCC));

            if (button == nameof(Resource.Id.acceptButton))
            {
                HomeScreenDisplay(app);
                app.Tap(nameof(Resource.Id.buttonPayPCN));
            }
            else if (button == nameof(Resource.Id.buttonPayPCN))
            {
                app.Tap(nameof(Resource.Id.buttonPayPCN));
            }
            else
            {
                throw new Exception("Not Found");
            }

            app.WaitForElement(nameof(Resource.Id.txtPCNTitle), Strings.PcnDetails);
            app.Screenshot("Pay a Penalty Charge Notice(PCN)");
        }

        public static void PcnEnterDetailsAndContinue(IApp app, string pcnNumber, string vrm)
        {
            app.EnterText(nameof(Resource.Id.pcnNumberEditText), pcnNumber);
            app.EnterText(nameof(Resource.Id.vrmEditText), vrm);
            app.DismissKeyboard();
            app.Screenshot(Strings.PcnDetails);
            app.Tap(nameof(Resource.Id.btnControl), Strings.Continue);
        }

        public static void PcnPayemtDetailsScreenDisplay(IApp app)
        {
            app.Tap(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            app.WaitForElement(nameof(Resource.Id.txtPCCTitle), Strings.PaymentDetails);
            app.Screenshot(Strings.PaymentDetails);
        }

        public static void EnterCardDetails(IApp app, string cardNumber, string expiryDate, string cardHolderName, string cvv)
        {
            app.EnterText(nameof(Resource.Id.editCardNumber), cardNumber);
            app.EnterText(nameof(Resource.Id.editCardExpire), expiryDate);
            app.DismissKeyboard();
            app.EnterText(nameof(Resource.Id.editName), cardHolderName);
            app.EnterText(nameof(Resource.Id.editCardCVV2), cvv);
            app.DismissKeyboard();
            app.Screenshot("Card Details");
        }

        public static void SelectDatesAndConfirm(IApp app, string dates)
        {
            app.Tap(nameof(Resource.Id.txtDayDescription), dates);
            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Journey Details");

            app.Tap(nameof(Resource.Id.btnConfirmDetailsAndPay));
            app.WaitForElement(nameof(Resource.Id.txtPCCTitle), Strings.PaymentDetails);
            app.Screenshot("Payment details");
        }

        public static void ManageStoredInformationScreenDisplay(IApp app)
        {
            HomeScreenDisplay(app);
            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot(Strings.YourVehiclesAndCards);
        }

        public static void EnterVrmAndSaveVehicle(IApp app, string vrm, bool expectWarning)
        {
            app.WaitForElementWithTimeout(Strings.PayCongestionCharge);
            app.ScrollDownTo(nameof(Resource.Id.btnAddNewVehicle), null, 0, 0.67, 500, true, new TimeSpan(0, 0, 0, 90, 0));
            app.Tap(nameof(Resource.Id.btnAddNewVehicle));
            app.WaitForElementWithText(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            app.EnterText(nameof(Resource.Id.editTextVRM), vrm);
            app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            app.DismissKeyboard();
            app.Screenshot("Enter Vehicle Details");

            app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);

            if (expectWarning)
            {
                app.Screenshot("Warning");
                app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);
            }

            if (!expectWarning && app.ElementExists(nameof(Resource.Id.btnOk)))
            {
                throw new Exception("Vehicle not found warning shown, even though it shouldn't");
            }

            app.WaitForElement(Strings.ChooseDates);
            app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            app.Tap(nameof(Resource.Id.chkSaveVehicle), Strings.SaveVehicleDetails);
            app.Screenshot("Vehicle details");

            app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            app.WaitForElementWithTimeout(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);

            app.TapNavigateBackWithTimeout();
            app.TapNavigateBackWithTimeout();
            app.TapNavigateBackWithTimeout();

            app.Screenshot("Saved Vehicles");
        }
    }
}
