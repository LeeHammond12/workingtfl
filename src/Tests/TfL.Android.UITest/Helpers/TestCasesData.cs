﻿namespace TfL.Android.UITest.Helpers
{
    public class TestCasesData
    {
        public static readonly string VRMIAZ4501 = "IAZ4501";
        public static readonly string VRMIAZ4516 = "IAZ4516";
        public static readonly string VRMIAZ3004 = "IAZ3004";
        public static readonly string VRM191VRB1 = "191VRB1";
        public static readonly string VRMIAZ4546 = "IAZ4546";
        public static readonly string VRMIAZ4562 = "IAZ4562";
        public static readonly string VRMIAZ4594 = "IAZ4594";
        public static readonly string VRMIAZ4603 = "IAZ4603";
        public static readonly string VRMIAZ47 = "IAZ47";
        public static readonly string VRMIAZ4708 = "IAZ4708";
        public static readonly string VRMIAZ4717 = "IAZ4717";
        public static readonly string VRMIAZ4727 = "IAZ4727";
        public static readonly string VRMIAZ474 = "IAZ474";
        public static readonly string VRMIAZ4747 = "IAZ4747";
        public static readonly string VRMIAZ4761 = "IAZ4761";
        public static readonly string VRMIAZ4767 = "IAZ4767";
        public static readonly string VRMIAZ4775 = "IAZ4775";
        public static readonly string VRMIAZ4776 = "IAZ4776";
        public static readonly string VRMIAZ4777 = "IAZ4777";
        public static readonly string VRMIAZ4951 = "IAZ4951";
        public static readonly string VRMIAZ_4501 = "IAZ  4501";
        public static readonly string VRMIAZ___4501 = "IAZ @£4501";
        public static readonly string VRMIAZ454 = "IAZ454";
        public static readonly string VRMIAZ4525 = "IAZ4525";
        public static readonly string VRMIAZ4233 = "IAZ4233";
        public static readonly string VRMIAZ4786 = "IAZ4786";
        public static readonly string VRMIAZ4791 = "IAZ4791";
        public static readonly string VRMIAZ4957 = "IAZ4957";

        public static readonly string VRMET65LBA = "ET65LBA";
        public static readonly string VRMMC01CCC = "MC01CCC";
        public static readonly string VRMMC52XXX = "MC52XXX";
        public static readonly string VRMFV02VGG = "FV02VGG";
        public static readonly string VRMRM021GMN = "RM021GMN";
        public static readonly string VRMRM021TTN = "RM021TTN";
        public static readonly string VRMDS13EZK = "DS13EZK";

        public static readonly string PCNGT628863HH = "GT628863HH";
        public static readonly string PCNTF93025009 = "TF93025009";
        public static readonly string PCNLZ25361494 = "LZ25361494";
        public static readonly string PCNGF65943088 = "GF65943088";
        public static readonly string PCNGT62886359 = "GT62886359";
        public static readonly string PCNTF93025086 = "TF93025086";
        
        public static readonly string PCN19NF = "N19NF";
        public static readonly string PCW1U6TY = "W1U6TY";
        public static readonly string PCSW1E5ND = "SW1E5ND";
        public static readonly string PCHA28LF = "HA28LF";
        public static readonly string PCHA28LFF = "HA28LFF";
        public static readonly string PCSE116DQ = "SE116DQ";

        public static readonly string BN1 = "1";
        public static readonly string BN101 = "101";
        public static readonly string BN124 = "124";
        public static readonly string BNCOALPORTHOUSE = "COALPORT HOUSE";

        public static readonly string CN4543059790016721 = "4543059790016721";
        public static readonly string CN4715059999000437 = "4715059999000437";
        public static readonly string CN4844215500115643 = "4844215500115643";
        public static readonly string CN5454609899026213 = "5454609899026213";
        public static readonly string CN5569500000002312 = "5569500000002312";
        public static readonly string CN5573489900000028 = "5573489900000028";
        public static readonly string CN5641820000000005 = "5641820000000005";
        public static readonly string CN4462030000000000 = "4462030000000000";
        public static readonly string CN3569990010067584 = "3569990010067584";
        
        public static readonly string CE1217 = "1217";
        public static readonly string CE1218 = "1218";

        public static readonly string CUANONYMOUS = "Anonymous";
        
        public static readonly string CVV123 = "123";
        public static readonly string CVV124 = "124";
        public static readonly string CVV147 = "147";
        public static readonly string CVV231 = "231";
        public static readonly string CVV358 = "358";
        public static readonly string CVV555 = "555";
        public static readonly string CVV587 = "587";
        public static readonly string CVV654 = "654";
        public static readonly string CVV751 = "751";
        public static readonly string CVV852 = "852";
        public static readonly string CVV963 = "963";
        
        public static readonly string MOB74438612323 = "74438612323";
        public static readonly string MOB0744386123 = "0744386123";
        public static readonly string MOB07443861232 = "07443861232";
    }
}
