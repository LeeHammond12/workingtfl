﻿using System;
using System.IO;
using System.Reflection;
using Xamarin.UITest;

namespace TfL.Android.UITest.Helpers
{
    public class AppInitializer
    {
        public static IApp StartApp()
        {
#if DEBUG
            throw new Exception("UI Tests must run in Release mode");
#endif
            string dir = new FileInfo(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath).Directory.Parent.Parent.FullName;           
            //string pathtoapk = Path.Combine(dir, "C:\\Users\\P10377632\\Documents\\TFL\\src\\TfL.CongestionCharge.Droid\\bin\\Release\\TfL.CongestionCharge.Droid\\bin\\Release\\tflonline.CongestionCharge.Droid-Signed.apk");
            //string pathtoapk = Path.Combine(dir, "TfL.CongestionCharge.Droid\\bin\\Release\\tfl.CongestionCharge.Droid-Signed.apk");
            var pathtoapk =
                "C:\\Users\\P10377632\\Documents\\TFL\\src\\TfL.CongestionCharge.Droid\\bin\\Release-Prod\\tflonline.CongestionCharge.Droid-Signed.apk";
            return ConfigureApp.Android.EnableLocalScreenshots().ApkFile(pathtoapk).StartApp();
        }
    }
}
