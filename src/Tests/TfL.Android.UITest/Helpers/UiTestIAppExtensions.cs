﻿using System;
using System.Linq;
using Tfl.CongestionCharge.Core.Extensions;
using TfL.CongestionCharge.Droid;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TfL.UITest.Helpers
{
    public static class UiTestIAppExtensions
    {
        public static void WaitForDialogWithMessage(this IApp app, string expected)
        {
            var messageElement = app.WaitForElement(c => c.Marked(nameof(Resource.Id.dialogMessage)));

            var message = messageElement.First().Text;

            var strippedMessage = StripMessage(message);
            var strippedExpected = StripMessage(expected);

            if (strippedMessage.CalcLevenshteinDistance(strippedExpected) > 5)
            {
                throw new Exception($"Expected message '{strippedExpected}' but got '{strippedMessage}'");
            }
        }

        public static void WaitForElementWithText(this IApp app, string elementName, string elementText)
        {
            app.WaitForElement(c => c.Marked(elementName).Text(elementText));
        }

        public static string WaitForAnyElement(this IApp app, params string[] values)
        {
            app.WaitFor(() => values.FirstOrDefault(v => app.Query(v).FirstOrDefault() != null) != null, "", TimeSpan.FromSeconds(5));
            return values.FirstOrDefault(v => app.Query(v).FirstOrDefault() != null);
        }

        public static void Tap(this IApp app, string elementName, string elementText)
        {
            app.Tap(c => c.Marked(elementName).Text(elementText));
        }

        public static void WaitForElementWithTimeout(this IApp app, Func<AppQuery, AppQuery> query)
        {
            app.WaitForElement(query, "Wait for element timed out", TimeSpan.FromSeconds(90));
        }

        public static void WaitForElementWithTimeout(this IApp app, string elementName)
        {
            app.WaitForElement(c => c.Marked(elementName), $"Wait for element {elementName} timed out", TimeSpan.FromSeconds(90));
        }

        public static void WaitForElementWithTimeout(this IApp app, string elementName, string elementText)
        {
            app.WaitForElement(c => c.Marked(elementName).Text(elementText), "Wait for element timed out", TimeSpan.FromSeconds(90));
        }

        public static bool ElementExists(this IApp app, string element)
        {
            return app.Query(c => c.Id(element)).FirstOrDefault() != null;
        }

        public static void TapNavigateBackWithTimeout(this IApp app)
        {
            app.WaitForElementWithTimeout("Navigate up");
            app.Tap("Navigate up");
        }

        public static void TapNavigateBack(this IApp app)
        {
            app.WaitForElement("Navigate up");
            app.Tap("Navigate up");
        }

        private static string StripMessage(string message)
        {
            message = message.StripHtml();
            message = message.Replace("\r", "");
            message = message.Replace("\n", "");

            return message;
        }
    }
}
