﻿using System;
using Xamarin.UITest;

namespace TfL.Android.UITest.Tests
{
    public static class DatePickerExtensions
    {
        private static readonly TimeSpan DefaultTimeout = TimeSpan.FromSeconds(30);
        private const string AndroidDatePickerClass = "datePicker";
        private const string AndroidDatePickerUpdateDateMethod = "updateDate";

        public static void UpdateDatePicker(this IApp App, DateTime dateTime, string pickerClass = AndroidDatePickerClass, TimeSpan? timeout = null)
        {
                App.WaitForElement(c => c.Class(AndroidDatePickerClass), timeout: DefaultTimeout);
                App.Query(x => x.Class(pickerClass).Invoke(AndroidDatePickerUpdateDateMethod, dateTime.Year, dateTime.Month - 1, dateTime.Day));
        }
    }
}
