﻿using NUnit.Framework;
using System;
using System.Diagnostics;
using Tfl.CongestionCharge.Core.Resources;
using TfL.Android.UITest.Helpers;
using TfL.CongestionCharge.Droid;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace TfL.Android.UITest.Tests
{
    [TestFixture]
    [System.Runtime.InteropServices.Guid("C0186BA7-7195-48DE-A604-70278E3B47BA")]
    public class PayCcTests
    {
        private IApp _app;

        [SetUp]
        public void Setup()
        {
            _app = AppInitializer.StartApp();
        }


        [Test]
        public void TestAcceptOrPayCcButtonAvailable()
        {
            var button = _app.WaitForAnyElement(nameof(Resource.Id.acceptButton), nameof(Resource.Id.buttonPayCC));

            if (button == nameof(Resource.Id.acceptButton))
            {
                // terms and conditions
            }
            else if (button == nameof(Resource.Id.buttonPayCC))
            {
                // main screen
            }
            else
            {
                throw new Exception("Not Found");
            }
        }

        public void OpenChangeStartDateCalendar()
        {
            _app.ScrollDown();
            _app.ScrollDown();
            _app.Tap(nameof(Resource.Id.btnChangeStartDate));
            _app.WaitForElement("datePicker");
            _app.Screenshot("Date Picker");
        }

        public void AddAnotherCard(string cardNumber, string expiryDate, string cardHolder, string cvv)
        {
            _app.Tap(nameof(Resource.Id.selectAnotherCardButton), Strings.AddAnotherCard);
            _app.WaitForElementWithText(nameof(Resource.Id.txtPCCTitle), Strings.PaymentDetails);
            _app.Screenshot("Add another Card");

            _app.EnterText(nameof(Resource.Id.editCardNumber), cardNumber);
            _app.EnterText(nameof(Resource.Id.editCardExpire), expiryDate);
            _app.DismissKeyboard();
            _app.EnterText(nameof(Resource.Id.editName), cardHolder);
            _app.EnterText(nameof(Resource.Id.editCardCVV2), cvv);
            _app.DismissKeyboard();
        }

        public void SavePaymentCardAndDisplay(string vrm, string cardNumber, string expiryDate, string cardHolder, string cvv)
        {
            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElementWithText(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), vrm);
            _app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            _app.Tap(nameof(Resource.Id.txtDayDescription), Strings.DailyCongestionCharge);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitle), Strings.PayCongestionCharge);
            _app.Screenshot("Journey Details");

            _app.Tap(nameof(Resource.Id.btnConfirmDetailsAndPay));
            _app.Tap(nameof(Resource.Id.selectAnotherCardButton), Strings.AddAnotherCard);
            _app.WaitForElementWithText(nameof(Resource.Id.txtPCCTitle), Strings.PaymentDetails);
            _app.Screenshot("Add another Card");

            _app.EnterText(nameof(Resource.Id.editCardNumber), cardNumber);
            _app.EnterText(nameof(Resource.Id.editCardExpire), expiryDate);
            _app.DismissKeyboard();
            _app.EnterText(nameof(Resource.Id.editName), cardHolder);
            _app.EnterText(nameof(Resource.Id.editCardCVV2), cvv);
            _app.DismissKeyboard();
            _app.Tap(Strings.SaveCardDetails);
            _app.ScrollDownTo(nameof(Resource.Id.btnConfirmAndPay));
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.Confirmation);
            _app.ScrollDownTo(Strings.Home);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.ManageStoredCreditCards);
            _app.Screenshot("Stored Cards");

            _app.Back();

            _app.Back();
        }

        [Test]
        public void ValidationFor100PercentDiscountedVRM()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRM191VRB1, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            _app.Screenshot("100 Percent Discounted VRM");
        }

        [Test]
        public void PayUpto65ChargingDaysInadvance()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            OpenChangeStartDateCalendar();

            var nonChargingdate = DateTime.Now.AddDays(66);
            _app.UpdateDatePicker(nonChargingdate);

            _app.Tap("button1");
            _app.WaitForElement(Strings.CcCannotPayMoreThan65DaysInFuture);
            _app.Screenshot(" Change Date 65 In advance message");
        }

        [Test]
        public void ChangeDateCancelButtonClosesCalendarView()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);

            OpenChangeStartDateCalendar();

            _app.Tap("button2", Strings.Cancel);
            _app.WaitForElement(nameof(Resource.Id.txtChangeStartDate));
            _app.Screenshot("Cancel button");
        }

        [Test]
        public void ChangeStartDateButtonScreenDisplay()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            OpenChangeStartDateCalendar();

            _app.Tap("button2", Strings.Cancel);
            _app.WaitForElement(nameof(Resource.Id.txtChangeStartDate));
            _app.Screenshot("Cancel button");

            OpenChangeStartDateCalendar();

            var tomorrow = DateTime.Today.AddDays(1);
            var daysUntilMonday = ((int)DayOfWeek.Monday - (int)tomorrow.DayOfWeek + 7) % 7;
            var nextMonday = tomorrow.AddDays(daysUntilMonday);
            _app.UpdateDatePicker(nextMonday);
            _app.Screenshot("Change Date");

            _app.Tap("button1");
            _app.WaitForElement(nameof(Resource.Id.txtChangeStartDate));
            _app.Screenshot("Change Start Date Button");

        }

        [Test]
        public void MessageValidationForaChargeInThePast()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            OpenChangeStartDateCalendar();

            var nonChargingdate = DateTime.Now.AddDays(-2);
            _app.UpdateDatePicker(nonChargingdate);
            _app.Screenshot("Past Date");

            _app.Tap("button1");
            _app.WaitForElement(Strings.CcCannotPayForAChargeInThePast);
            _app.Screenshot("Change Start Date In The Past");
        }

        [Test]
        public void MessageValidationForAlreadyPaidCharge()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            _app.Tap(nameof(Resource.Id.txtDayDescription), Strings.DailyCongestionCharge);
            _app.WaitForElement(Strings.CcChargeHasAlreadyBeenPaidForDatesSelected);
            _app.Screenshot("Already paid charge");
        }

        [Test]
        public void MessageValidationForAlreadyPaidChargeForSingleDateWithInASelectedDateRange()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            _app.Tap(nameof(Resource.Id.txtDayDescription), Strings.WeeklyCongestionCharge);
            _app.WaitForElement(Strings.CcChargeHasAlreadyBeenPaidForDatesSelected);
            _app.Screenshot("Result : " + Strings.CcChargeHasAlreadyBeenPaidForDatesSelected);
        }

        [Test]
        public void MessageValidationForNonChargingDate()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            OpenChangeStartDateCalendar();

            var tomorrow = DateTime.Today.AddDays(1);
            var daysUntilSunday = ((int)DayOfWeek.Sunday - (int)tomorrow.DayOfWeek + 7) % 7;
            var nextSunday = tomorrow.AddDays(daysUntilSunday);

            _app.UpdateDatePicker(nextSunday);
            _app.Screenshot("Non Charging Date");

            _app.Tap("button1");
            _app.WaitForElement(Strings.CcNonChargingDate);
            _app.Screenshot("Change Start Date To non-charging");
        }

        [Test]
        public void MessageValidationForChargeInPlace()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            OpenChangeStartDateCalendar();

            var tomorrow = DateTime.Today.AddDays(1);
            _app.UpdateDatePicker(tomorrow);

            _app.Tap("button1");
            _app.WaitForElement(Strings.WeeklyCongestionCharge);
            _app.Tap(nameof(Resource.Id.txtDayDescription), Strings.WeeklyCongestionCharge);
            _app.WaitForElement(Strings.CcChargeHasAlreadyBeenPaidForDatesSelected);
            _app.Screenshot("Result : " + Strings.CcChargeHasAlreadyBeenPaidForDatesSelected);
        }

        [Test]
        public void PayForScreenDisplayAfterChangeStartDate()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            OpenChangeStartDateCalendar();

            var tomorrow = DateTime.Today.AddDays(1);
            var daysUntilMonday = ((int)DayOfWeek.Monday - (int)tomorrow.DayOfWeek + 7) % 7;
            var nextMonday = tomorrow.AddDays(daysUntilMonday);

            _app.UpdateDatePicker(nextMonday);
            _app.Screenshot("Valid Date");

            _app.Tap("button1");
            _app.WaitForElement(nameof(Resource.Id.txtChangeStartDate));
            _app.Screenshot("Pay For ?");

            TestMethods.SelectDatesAndConfirm(_app, Strings.MonthlyCongestionCharge);
        }

        [Test]
        public void ChooseYourDatesButtonFunctionality()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }

        [Test]
        public void ChooseYourDatesbuttonActivation()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);

            _app.WaitForElement(nameof(Resource.Id.txtVehicleDetailsColor));
            _app.Screenshot("Vehicle details");

            _app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            _app.WaitForElement(Strings.PromptUserToConfirmVrmIsCorrect);
            _app.Screenshot("Information");

            _app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);
            _app.WaitForElement(nameof(Resource.Id.txtVehicleDetailsColor));

            _app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            _app.Screenshot("Vehicle details");
            _app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);
            _app.Screenshot("Pay For");
        }

        [Test]
        public void PayForScreenDisplayAndFunctionality()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);

            _app.WaitForElement(Strings.ChooseDates);
            _app.WaitForElement(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);
            _app.WaitForElement(nameof(Resource.Id.txtPayFor));

            OpenChangeStartDateCalendar();
            _app.Tap("button1");
            _app.WaitForElement(nameof(Resource.Id.txtChangeStartDate));

            _app.ScrollUp();
            _app.WaitForElement(Strings.PreviousChargingDay);
            _app.WaitForElement(Strings.DailyCongestionCharge);
            _app.WaitForElement(Strings.WeeklyCongestionCharge);
            _app.WaitForElement(Strings.MonthlyCongestionCharge);
            _app.Screenshot("Pay For?");

            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void ConfirmDetailsAndPayButtonFunctionality()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.MonthlyCongestionCharge);
        }

        [Test]
        public void CongestionChargePaymentConfirmationScreenCheck()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            _app.Screenshot("100 Percent Discounted VRM");
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.AllDone);
            _app.WaitForElementWithTimeout(Strings.ThanksPaymentCompletedSuccessfully);
            _app.WaitForElementWithTimeout(Strings.SaveReceipt);
            _app.WaitForElementWithTimeout(Strings.Home);
            _app.Screenshot("Payment Confirmation!");

            _app.Tap(Strings.SaveReceipt);
            _app.WaitForElement(Strings.ViewReceipts);
            _app.Tap(Strings.ViewReceipts);
            _app.WaitForElement(Strings.ViewReceipts);
            _app.Screenshot("Saved Receipts");
        }

        [Test]
        public void ValidationFor90PercentDiscountedVRM()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ3004, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            _app.Screenshot("90 Percent Discounted VRM");
        }

        [Test]
        public void MessageValidationForExemptVrm()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElement(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), TestCasesData.VRMIAZ4233);
            _app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);
            _app.WaitForElement(Strings.VehicleExemptMessage);
            _app.Screenshot("Warning");
        }

        [Test]
        public void JourneyDetailPageScreenDisplay()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4516, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            _app.Tap(nameof(Resource.Id.txtDayDescription), Strings.DailyCongestionCharge);
            _app.WaitForElement(Strings.PayCongestionCharge);
            _app.WaitForElement(nameof(Resource.Id.txtJourneyDetails));
            _app.WaitForElement(Strings.JourneyDatesNoColon);
            _app.WaitForElement(Strings.Amount);
            _app.WaitForElement(nameof(Resource.Id.btnConfirmDetailsAndPay));
            _app.Screenshot("Journey Details");
        }

        [Test]
        public void JourneyDetailScreenBackButtonNavigateToPayForScreen()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4516, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.MonthlyCongestionCharge);

            _app.TapNavigateBack();
            _app.TapNavigateBack();
            _app.WaitForElement(Strings.DailyCongestionCharge);
            _app.Screenshot("Result : Choose your dates via screen back button");
        }

        [Test]
        public void JourneyDetailDeviceBackButtonNavigateToPayForScreen()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4516, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.MonthlyCongestionCharge);

            _app.Back();
            _app.Back();
            _app.WaitForElement(Strings.DailyCongestionCharge);
            _app.Screenshot("Result : Choose your dates via device back button");
        }

        [Test]
        public void JourneyDetailSingleDateWithInASelectedDateRange()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4516, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);

            _app.Tap(nameof(Resource.Id.txtDayDescription), Strings.WeeklyCongestionCharge);
            _app.WaitForElement(Strings.PayCongestionCharge);
            _app.Screenshot("Journey Details");

            _app.Tap(nameof(Resource.Id.btnConfirmDetailsAndPay));
            _app.WaitForElementWithText(nameof(Resource.Id.txtPCCTitle), Strings.PaymentDetails);
            _app.Screenshot("Payment details");

            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElement(Strings.Home);

            _app.Tap(Strings.Home);
            _app.WaitForElement(Strings.PayCongestionCharge);

            _app.Tap(Strings.PayCongestionCharge);
            _app.WaitForElementWithTimeout(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), TestCasesData.VRMIAZ4516);

            _app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");
            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);


            _app.ScrollDownTo(Strings.ChangeStartDate);
            _app.Tap(Strings.ChangeStartDate);

            OpenChangeStartDateCalendar();

            var nextWeek = DateTime.Today.AddDays(7);
            var days = ((int)DayOfWeek.Monday - (int)nextWeek.DayOfWeek + 7) % 7;
            var nextWeekDays = nextWeek.AddDays(days);

            _app.UpdateDatePicker(nextWeekDays);
            _app.Screenshot("Change Date");
            _app.Tap("button1");
            _app.Tap(Strings.WeeklyCongestionCharge);
            _app.WaitForElement(nameof(Resource.Id.btnConfirmDetailsAndPay));
            _app.Screenshot("Result : Journey Detail Single Date With In A Selected Date Range");
        }

        [Test]
        public void MessageValidationForNoPaymentConfirmation()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4516, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1217, TestCasesData.CUANONYMOUS, TestCasesData.CVV231);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElement(Strings.CcPaymentFailedMessage);
            _app.Screenshot("Result : Payment failure message from no payment confirmation");
        }

        [Test]
        public void MessageValidationForNoTransactionIdReceived()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4516, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4844215500115643, TestCasesData.CE1217, TestCasesData.CUANONYMOUS, TestCasesData.CVV123);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElement(Strings.CcPaymentFailedMessage);
            _app.Screenshot("Result : Payment failure message from no payment cofirmation");
        }

        [Test]
        public void AmountVerificationForNoneDiscountedVRM()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4525, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void ValidationForPaymentConfirmationScreen()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4525, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.WaitForElement(Strings.AllDone);
            _app.WaitForElement(Strings.ThanksPaymentCompletedSuccessfully);
            _app.WaitForElement(Strings.YourReceipt);
            _app.WaitForElement(Strings.TypeColon);
            _app.WaitForElement(Strings.AmountPaid);
            _app.WaitForElement(Strings.PaymentDate);
            _app.WaitForElement(Strings.SaveReceipt);
            _app.WaitForElement(Strings.Home);
            _app.Screenshot("Result : Payment confirmation screen");
        }

        [Test]
        public void PaymentConfirmationForaDay()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4525, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithText(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.Screenshot("Result : Confirmation for a day");
        }

        [Test]
        public void PaymentConfirmationForaMonth()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4525, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.MonthlyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithText(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.Screenshot("Result  : Confirmation for a month");
        }

        [Test]
        public void PaymentConfirmationForaWeek()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4525, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.WeeklyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithText(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.Screenshot("Result : Confirmation for a week");
        }

        [Test]
        public void PaymentConfirmationForPreviousChargingDay()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ454, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.PreviousChargingDay);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithText(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.Screenshot("Result : Confirmation for previous charging day");
        }

        [Test]
        public void VerificationOfReceiptButtonOnNotSavingReceipt()
        {
            ValidationForPaymentConfirmationScreen();
            _app.Tap(nameof(Resource.Id.btnHome), Strings.Home);
            _app.Screenshot("Result : No receipts button shown");
        }

        [Test]
        public void VerificationOfReceiptButtonOnSavingReceipt()
        {
            CongestionChargePaymentConfirmationScreenCheck();
        }

        [Test]
        public void EnsureMaximum15VrmCanBeSaved()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4546, false);
            _app.WaitForElement(Strings.ChooseDates);
            _app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            _app.Tap(nameof(Resource.Id.chkSaveVehicle), Strings.SaveVehicleDetails);
            _app.Screenshot("Vehicle details");

            _app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            _app.WaitForElementWithTimeout(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);

            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBackWithTimeout();

            _app.WaitForElement(nameof(Resource.Id.buttonPayCC));
            _app.Tap(nameof(Resource.Id.buttonPayCC));

            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4562, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4594, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4603, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ47, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4708, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4717, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4727, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ474, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4747, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4761, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4767, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4775, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4776, false);
            _app.ScrollUpTo(Strings.ChooseAVehicle, null, 0, 0.67, 500, true, new TimeSpan(0, 0, 0, 90, 0));
            _app.Screenshot("Result : Saved Vehicles 1");
            _app.ScrollDown();
            _app.Screenshot("Result : Saved Vehicles 2");
            _app.ScrollDown();
            _app.Screenshot("Result : Saved Vehicles 3");
            _app.ScrollDown();
            _app.Screenshot("Result : Saved Vehicles 4");
            _app.ScrollDownTo(nameof(Resource.Id.btnAddNewVehicle), null, 0, 0.67, 500, true, new TimeSpan(0, 0, 0, 90, 0));
            _app.Screenshot("Result : Saved Vehicles 5");

        }

        [Test]
        public void EnsureSaveCardDetailsCheckBoxIsAvailable()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.PreviousChargingDay);

            _app.DismissKeyboard();
            _app.WaitForElement(Strings.SaveCardDetails);
            _app.Screenshot("Result : Save card details checkbox");

        }

        [Test]
        public void MessageValidationForIncorrectCvvNumber()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV555);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElement(nameof(Resource.Id.dialogMessage));
            _app.Screenshot("Result : Payment error");
        }

        [Test]
        public void CheckDisplayAndFunctionalityOfSaveReceiptButton()
        {
            CongestionChargePaymentConfirmationScreenCheck();
        }

        [Test]
        public void SaveCardDetailsCheckBoxFunctionality()
        {
            EnsureCardDetailsAreTokenized();
            _app.Back();
            _app.Back();

            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4546, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4562, TestCasesData.CN4715059999000437, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV124);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4594, TestCasesData.CN4844215500115643, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4603, TestCasesData.CN5454609899026213, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV358);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4603, TestCasesData.CN5569500000002312, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV147);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4708, TestCasesData.CN5573489900000028, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV852);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4717, TestCasesData.CN5641820000000005, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV963);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4727, TestCasesData.CN4462030000000000, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ474, TestCasesData.CN3569990010067584, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV751);
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4747, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4761, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4767, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4775, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4776, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4777, "", "1218", "Anonymous", "");
            // Need card details
        }

        [Test]
        public void EnsureSaveVehicleCheckBoxDisplayAndFunctionality()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4546, false);
            _app.WaitForElement(Strings.ChooseDates);
            _app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            _app.Tap(nameof(Resource.Id.chkSaveVehicle), Strings.SaveVehicleDetails);
            _app.Screenshot("Vehicle details");

            _app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            _app.WaitForElementWithTimeout(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);

            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBackWithTimeout();

            _app.WaitForElement(nameof(Resource.Id.buttonPayCC));
            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElementWithText(nameof(Resource.Id.txtWhichVehicle), Strings.ChooseAVehicle);
            _app.Screenshot("Result : Saved Vehicle");
        }

        [Test]
        public void EnsureDisplayOfSaveVehicleSectionOnAtleastOneSavedVrm()
        {
            EnsureSaveVehicleCheckBoxDisplayAndFunctionality();

            _app.Tap(nameof(Resource.Id.btnAddNewVehicle));
            _app.WaitForElementWithText(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), TestCasesData.VRMIAZ4951);
            _app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);

            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBackWithTimeout();

            _app.Screenshot("Result : Display of saved vehicles");

        }

        [Test]
        public void SaveReceiptButtonAvailabilityAndFunctionality()
        {
            CongestionChargePaymentConfirmationScreenCheck();
        }

        [Test]
        public void SavePaymentCardDisplayAndFunctionality()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            _app.Tap(Strings.SaveCardDetails);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.Confirmation);
            _app.Tap(Strings.Home);

            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElementWithText(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), TestCasesData.VRMIAZ4501);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            _app.Tap(nameof(Resource.Id.txtDayDescription), Strings.DailyCongestionCharge);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitle), Strings.PayCongestionCharge);
            _app.Screenshot("Journey Details");

            _app.Tap(nameof(Resource.Id.btnConfirmDetailsAndPay));
            _app.WaitForElement(Strings.SelectYourStoredCard);
            _app.Screenshot("Stored Cards");
            _app.Tap(nameof(Resource.Id.cardEndingInText));
            _app.WaitForElement(Strings.SendMyReceiptViaSms);
            _app.Screenshot("NO CVV");

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElement(Strings.Cvv2Length);
            _app.Screenshot("Warning");

            _app.Tap(Strings.Ok);
            _app.EnterText(nameof(Resource.Id.editCardCVV2), TestCasesData.CVV587);
            _app.DismissKeyboard();
            _app.Screenshot("Enter CVV");

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElement(Strings.Confirmation);
            _app.Screenshot("Confirmation");
        }

        [Test]
        public void SaveMyReceiptViaSmsCheckboxDisplayAndFunctionality()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            _app.WaitForElement(Strings.SendMyReceiptViaSms);
            _app.Tap(Strings.SendMyReceiptViaSms);
            _app.WaitForElement(Strings.PleaseEnterYourSmsNumber);
            _app.Screenshot("Enter Your SMS Number");

            _app.EnterText(nameof(Resource.Id.editNumber), TestCasesData.MOB07443861232);
            _app.DismissKeyboard();
            _app.ScrollUpTo(nameof(Resource.Id.btnConfirmAndPay));
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithTimeout(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.Screenshot("Confirmation");
        }

        [Test]
        public void MessageValidationForInvalidNumberLessThan11Chars()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            _app.WaitForElement(Strings.SendMyReceiptViaSms);
            _app.Tap(Strings.SendMyReceiptViaSms);
            _app.WaitForElement(Strings.PleaseEnterYourSmsNumber);
            _app.Screenshot("Enter Your SMS Number");

            _app.EnterText(nameof(Resource.Id.editNumber), TestCasesData.MOB0744386123);
            _app.DismissKeyboard();
            _app.ScrollUpTo(nameof(Resource.Id.btnConfirmAndPay));
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElement(Strings.SmsNumberLength);
            _app.Screenshot("Result : Mobile number validation");
        }

        [Test]
        public void MessageValidationForInvalidNumberDoesNotStartWithZero()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            _app.WaitForElement(Strings.SendMyReceiptViaSms);
            _app.Tap(Strings.SendMyReceiptViaSms);
            _app.WaitForElement(Strings.PleaseEnterYourSmsNumber);
            _app.Screenshot("Enter Your SMS Number");

            _app.EnterText(nameof(Resource.Id.editNumber), TestCasesData.MOB74438612323);
            _app.DismissKeyboard();
            _app.ScrollUpTo(nameof(Resource.Id.btnConfirmAndPay));
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElement(Strings.SmsNumberStartsWith);
            _app.Screenshot("Result : Mobile number validation");
        }

        [Test]
        public void EnsureCardDetailsAreTokenized()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(Strings.SaveCardDetails);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.Confirmation);
            _app.Tap(Strings.Home);

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Screenshot(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Screenshot("Result : Stored cards");
        }

        [Test]
        public void ValidationForAlphanumericCharacterOfvehicleInput()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ_4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);

            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ___4501, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }

        [Test]
        public void EnsureVehicleValidationViaTheNextButton()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }

        [Test]
        public void VehicleValidationViaTheNextButtonForIncorrectVrm()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }

        [Test]
        public void YesterdayButtonAndMessageValidationForNonChargingDate()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            OpenChangeStartDateCalendar();

            var tomorrow = DateTime.Today.AddDays(1);
            var daysUntilSunday = ((int)DayOfWeek.Sunday - (int)tomorrow.DayOfWeek + 7) % 7;
            var nextSunday = tomorrow.AddDays(daysUntilSunday);

            _app.UpdateDatePicker(nextSunday);
            _app.Screenshot("Non Charging Date");

            _app.Tap("button1");
            _app.WaitForElement(Strings.CcNonChargingDate);
            _app.Screenshot("Change Start Date To non-charging");

            _app.Tap(Strings.Ok);
            OpenChangeStartDateCalendar();

            var tomorrowTuesday = DateTime.Today.AddDays(1);
            var daysUntilMonday = ((int)DayOfWeek.Monday - (int)tomorrowTuesday.DayOfWeek + 7) % 7;
            var nextMonday = tomorrow.AddDays(daysUntilMonday);

            _app.UpdateDatePicker(nextMonday);
            _app.Screenshot("Date");

            _app.Tap("button1");
            _app.ScrollUp();
            _app.WaitForNoElement(Strings.PreviousChargingDay);
            _app.Screenshot("No Yesterday");

        }

        [Test]
        public void VerifyVehicleDetailPageDisplay()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951, false);

            _app.WaitForElement(Strings.PayCongestionCharge);
            _app.WaitForElement(Strings.VehicleRegistrationMark1);
            _app.WaitForElement(Strings.CarMake);
            _app.WaitForElement(Strings.ConfirmThisVrmIsCorrect);
            _app.WaitForElement(Strings.SaveVehicleDetails);
            _app.WaitForElement(Strings.ChooseDates);
            _app.Screenshot("Result : Vehicle details");
        }

        [Test]
        public void VerifyWhichVehicleScreenDisplay()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElement(Strings.PayCongestionCharge);
            _app.WaitForElement(Strings.ChooseAVehicle);
            _app.WaitForElement(Strings.IsVehicleRegisteredInUk);
            _app.WaitForElement(Strings.Next);
            _app.Screenshot("Result : " + Strings.ChooseAVehicle);
        }
    }
}
