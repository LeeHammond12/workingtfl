﻿using NUnit.Framework;
using TfL.Android.UITest.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using TfL.CongestionCharge.Droid;
using Xamarin.UITest;
using TfL.UITest.Helpers;

namespace TfL.Android.UITest.Tests
{
    [TestFixture]
    public class Receipts
    {
        private IApp _app;

        [SetUp]
        public void Setup()
        {
            _app = AppInitializer.StartApp();
        }

        [Test]
        public void NoLimitOnNumberOfReceiptsStored()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ5051", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ5051 PCC Receipt Details");

            TestMethods.EnterVrmAndTapNext(_app, "IAZ505", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ505 PCC Receipt Details");

            TestMethods.EnterVrmAndTapNext(_app, "IAZ5030", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ5030 PCC Receipt Details");

            TestMethods.EnterVrmAndTapNext(_app, "IAZ5010", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ5010 PCC Receipt Details");

            _app.Tap(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.Receipts);
            _app.Screenshot("No limit on number of receipts stored. UP");
            _app.ScrollDown();
            _app.Screenshot("No limit on number of receipts stored. MIDDLE");
            _app.ScrollDown();
            _app.ScrollDown();
            _app.Screenshot("No limit on number of receipts stored. DOWN");
        }

        [Test]
        public void PccReceiptDetails()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ5005", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ5005 PCC Receipt Details");

            _app.Tap(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.WaitForElement(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.Receipts);
            _app.WaitForElement(Strings.TypeColon);
            _app.WaitForElement(Strings.VrmColon);
            _app.WaitForElement(Strings.ReceiptNoColon);
            _app.WaitForElement(Strings.AmountPaid);
            _app.WaitForElement(Strings.PaymentDate);
            _app.WaitForElement(Strings.ChargeType);
            _app.WaitForElement(Strings.JourneyDates);
            _app.Screenshot("Result - Saved PCC Receipt");
        }

        [Test]
        public void PCNReceiptDetails()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, "GT62886315", "IAZ5005");
            TestMethods.PcnPayemtDetailsScreenDisplay(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - PCN Receipt Details");

            _app.Tap(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.WaitForElement(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.Receipts);
            _app.WaitForElement(Strings.TypeColon);
            _app.WaitForElement(Strings.VrmColon);
            _app.WaitForElement(Strings.ReceiptNoColon);
            _app.WaitForElement(Strings.AmountPaid);
            _app.WaitForElement(Strings.PaymentDate);
            _app.WaitForElement(Strings.ReceiptPcnNumber);
            _app.Screenshot("Result - Saved PCN Receipt");
        }

        [Test]
        public void ReceiptNotShown()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ5000", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithTimeout(nameof(Resource.Id.txtYourReceipt), Strings.YourReceipt);
            _app.Screenshot("Result - PCN Receipt Details");

            _app.Tap(nameof(Resource.Id.btnHome), Strings.Home);
            _app.WaitForElementWithText(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.Screenshot("Result - VRM2, Receipt Not Shown");
        }

        [Test]
        public void ReceiptScrollBar()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ500", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ500 PCC Receipt Details");

            TestMethods.EnterVrmAndTapNext(_app, "IAZ50", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ50 PCC Receipt Details");

            TestMethods.EnterVrmAndTapNext(_app, "IAZ5", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ5 PCC Receipt Details");

            TestMethods.EnterVrmAndTapNext(_app, "IAZ4999", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ4999 PCC Receipt Details");

            TestMethods.EnterVrmAndTapNext(_app, "IAZ4988", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - IAZ4988 PCC Receipt Details");

            _app.Tap(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.Receipts);
            _app.Screenshot("More Receipts - 1");

            _app.ScrollDown();
            _app.Screenshot("More Receipts - 2");

            _app.ScrollDown();
            _app.Screenshot("More Receipts - 3");

            _app.ScrollDown();
            _app.Screenshot("More Receipts - 4");

            _app.ScrollDown();
            _app.Screenshot("More Receipts - 5");
        }

        [Test]
        public void SavePccReceipt()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ5001", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - PCC Receipt Details");

            _app.WaitForElementWithTimeout(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.Tap(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.Receipts);
            _app.Screenshot("Result - PCC Receipt Saved At Top Of The Page");
        }

        [Test]
        public void SavePCNReceipt()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, "GT62886315", "IAZ5005");
            TestMethods.PcnPayemtDetailsScreenDisplay(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            TestMethods.SaveReceipt(_app, "Result - PCN Receipt Details");

            _app.WaitForElementWithTimeout(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.Tap(nameof(Resource.Id.txtViewReceipts), Strings.Receipts);
            _app.WaitForElementWithTimeout(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.Receipts);
            _app.Screenshot("Result - PCN Receipt Saved At Top Of The Page");
        }

        [Test]
        public void SmsReceipt()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ5004", false);
            TestMethods.ConfirmVrmAndSaveTapChooseDate(_app);
            TestMethods.SelectDailyCongestionCharge(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.ScrollDown();
            _app.Tap(nameof(Resource.Id.chkReceiptViaSMS), Strings.SendMyReceiptViaSms);
            _app.EnterText(nameof(Resource.Id.editNumber), TestCasesData.MOB07443861232);
            _app.DismissKeyboard();
            _app.Screenshot("Receipt Send Via SMS");

            TestMethods.SaveReceipt(_app, "Result - Save PCC Receipt Details");
        }
    }
}

