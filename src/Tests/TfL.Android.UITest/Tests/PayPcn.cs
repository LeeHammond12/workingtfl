﻿using NUnit.Framework;
using Xamarin.UITest;
using Tfl.CongestionCharge.Core.Resources;
using TfL.Android.UITest.Helpers;
using TfL.CongestionCharge.Droid;
using TfL.UITest.Helpers;

namespace TfL.Android.UITest.Tests
{
    [TestFixture]
    public class PayPcn
    {
        private IApp _app;

        [SetUp]
        public void Setup()
        {
            _app = AppInitializer.StartApp();
        }

        [Test]
        public void MessageValidationForCancelledPcn()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNTF93025009, TestCasesData.VRMET65LBA);

            _app.WaitForElement(Strings.YouCannotPayThisPcn);
            _app.Screenshot("Result : " + Strings.Warning + " " + Strings.YouCannotPayThisPcn);
        }

        [Test]
        public void ChangePcnNumberViaBackButton()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNLZ25361494, TestCasesData.VRMMC01CCC);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");

            _app.Back();
            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.Continue);

            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNLZ25361494, TestCasesData.VRMMC01CCC);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");
        }

        [Test]
        public void VerifyContinueButtonChangesToEnterPaymentDetails()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNLZ25361494, TestCasesData.VRMMC01CCC);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");
        }

        [Test]
        public void MessageValidationForNoConfirmationofPaymentReceived()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNLZ25361494, TestCasesData.VRMMC01CCC);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");

            TestMethods.PcnPayemtDetailsScreenDisplay(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1217, TestCasesData.CUANONYMOUS, TestCasesData.CVV231);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithTimeout(Strings.PcnPaymentFailedMessage);
            _app.Screenshot("Error");
        }

        [Test]
        public void ValidateSaveSmsReceiptoptionNotAvailable()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNLZ25361494, TestCasesData.VRMMC01CCC);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");

            TestMethods.PcnPayemtDetailsScreenDisplay(_app);

            _app.WaitForNoElement(Strings.SendMyReceiptViaSms);
            _app.Screenshot("No SMS Option Available");
        }

        [Test]
        public void PaymentConfirmationScreen()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNLZ25361494, TestCasesData.VRMMC01CCC);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);

            _app.Screenshot("The Amount Due is:");
            TestMethods.PcnPayemtDetailsScreenDisplay(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(nameof(Resource.Id.ToolbarTitle), Strings.PayPenaltyChargeNotice);
            _app.WaitForElementWithTimeout(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.WaitForElementWithTimeout(Strings.AllDone);
            _app.WaitForElementWithTimeout(Strings.ThanksPaymentCompletedSuccessfully);
            _app.WaitForElementWithTimeout(Strings.YourReceipt);
            _app.WaitForElementWithTimeout(Strings.AmountPaid);
            _app.WaitForElementWithTimeout(Strings.PaymentDate);
            _app.WaitForElementWithTimeout(Strings.SaveReceipt);

            _app.ScrollDown();
            _app.WaitForElement(Strings.Home);
            _app.Screenshot("Confirmation Screen");

            _app.Tap(Strings.SaveReceipt);
            _app.WaitForElement(Strings.Receipts);
            _app.Screenshot("Home Page");

            _app.Tap(Strings.Receipts);
            _app.WaitForElement(Strings.Receipts);
            _app.Screenshot("Saved Receipt");
        }


        [Test]
        public void PaymentDetailsScreenDisplayCheck()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNLZ25361494, TestCasesData.VRMMC01CCC);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");

            _app.Tap(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitle), Strings.PayPenaltyChargeNotice);
            _app.WaitForElement(Strings.PaymentDetails);
            _app.WaitForElement(Strings.CardNumber);
            _app.WaitForElement(Strings.SaveCardDetails);
            _app.Screenshot("Payment Detail");
            // few things are not mentioned in strings.cs file
        }

        [Test]
        public void MessageValidationForAlreadyPaidPcn()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNLZ25361494, TestCasesData.VRMMC01CCC);

            _app.WaitForElement(Strings.YouCannotPayThisPcn);
            _app.Screenshot("Warning");
        }

        [Test]
        public void MessageValidationForIncorrectPcnCharacter()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNGT628863HH, TestCasesData.VRMMC52XXX);

            _app.WaitForElement(Strings.PcnHasWrongFormat);
            _app.Screenshot("Error");
        }

        [Test]
        public void PCNDetailsScreenDisplayCheck()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPayPCN));
            _app.WaitForElement(Strings.PayPenaltyChargeNotice);
            _app.WaitForElement(Strings.PcnDetails);
            _app.WaitForElement(Strings.PcnNumber);
            _app.WaitForElement(nameof(Resource.Id.vrmEditText));
            _app.WaitForElement(Strings.Continue);
            _app.DismissKeyboard();
            _app.Screenshot(Strings.PayAPenaltyChargeNotice);
        }

        [Test]
        public void MessageValidationForOnHoldPcn()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, "GT62886315", TestCasesData.VRMMC52XXX);

            _app.WaitForElement(Strings.EnterPaymentDetails);
            _app.Screenshot("Result : The Amount Due is:");
        }

        [Test]
        public void PcnUnderRepresenationWhichPlacedOnHold()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, "GT62886315", TestCasesData.VRMMC52XXX);

            _app.WaitForElement(Strings.EnterPaymentDetails);
            _app.Screenshot("Result : Pcn Under Represenation");
        }

        [Test]
        public void MessageValidationForPCNPaymentFailure()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, "GT62886315", TestCasesData.VRMMC52XXX);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");

            TestMethods.PcnPayemtDetailsScreenDisplay(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1217, TestCasesData.CUANONYMOUS, TestCasesData.CVV231);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithTimeout(Strings.PcnPaymentFailedMessage);
            _app.Screenshot("Error");
        }

        [Test]
        public void MessageValidationForPcnNoLongerAllowPayment()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNTF93025009, TestCasesData.VRMET65LBA);

            _app.WaitForElement(Strings.YouCannotPayThisPcn);
            _app.Screenshot("Warning");

            _app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);
            _app.WaitForElement(nameof(Resource.Id.buttonPayPCN));
            _app.Screenshot("Result : Home Screen");
        }

        [Test]
        public void EnsureSaveCardDetailsCheckboxDisplayAndFunctionality()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNGT62886359, TestCasesData.VRMRM021GMN);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");

            TestMethods.PcnPayemtDetailsScreenDisplay(_app);

            _app.EnterText(nameof(Resource.Id.editCardNumber), TestCasesData.CN4543059790016721);
            _app.EnterText(nameof(Resource.Id.editCardExpire), TestCasesData.CE1218);
            _app.DismissKeyboard();

            _app.EnterText(nameof(Resource.Id.editName), TestCasesData.CUANONYMOUS);
            _app.EnterText(nameof(Resource.Id.editCardCVV2), TestCasesData.CVV587);
            _app.DismissKeyboard();

            _app.WaitForElement(Strings.SaveCardDetails);
            _app.Tap(Strings.SaveCardDetails);
            _app.Screenshot("Save Card Details");

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithTimeout(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.ScrollDownTo(Strings.Home);

            _app.Tap(Strings.Home);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot("Home Screen");

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Screenshot("Manage Store Information");

            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.ManageStoredCreditCards);
            _app.Screenshot("Stored Card");
        }

        [Test]
        public void EnsureASaveReceiptButtonDisplayAndFunctionality()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNGT62886359, TestCasesData.VRMRM021GMN);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");

            TestMethods.PcnPayemtDetailsScreenDisplay(_app);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithTimeout(Strings.SaveReceipt);
            _app.Screenshot("Confirmation");

            _app.Tap(Strings.SaveReceipt);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot("Main Screen");

            _app.Tap(Strings.Receipts);
            _app.WaitForElement(Strings.Receipts);
            _app.Screenshot("Saved Receipt");
        }

        [Test]
        public void SavedCardPaymentFunctionality()
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, "GT62886315", TestCasesData.VRMMC52XXX);

            _app.WaitForElementWithText(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.Screenshot("The Amount Due is:");

            TestMethods.PcnPayemtDetailsScreenDisplay(_app);

            _app.EnterText(nameof(Resource.Id.editCardNumber), TestCasesData.CN4543059790016721);
            _app.EnterText(nameof(Resource.Id.editCardExpire), TestCasesData.CE1218);
            _app.DismissKeyboard();

            _app.EnterText(nameof(Resource.Id.editName), TestCasesData.CUANONYMOUS);
            _app.EnterText(nameof(Resource.Id.editCardCVV2), TestCasesData.CVV587);
            _app.DismissKeyboard();

            _app.WaitForElement(Strings.SaveCardDetails);
            _app.Tap(Strings.SaveCardDetails);
            _app.Screenshot("Save Card Details");

            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithTimeout(nameof(Resource.Id.txtTitle), Strings.Confirmation);
            _app.ScrollDownTo(Strings.Home);

            _app.Tap(Strings.Home);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot("Home Screen");

            _app.Tap(nameof(Resource.Id.buttonPayPCN));
            _app.WaitForElementWithText(nameof(Resource.Id.txtPCNTitle), Strings.PcnDetails);
            _app.Screenshot("Pay a Penalty Charge Notice(PCN)");

            TestMethods.PcnEnterDetailsAndContinue(_app, "GT62886315", TestCasesData.VRMMC52XXX);

            _app.Tap(nameof(Resource.Id.btnControl), Strings.EnterPaymentDetails);
            _app.WaitForElement(Strings.SelectYourStoredCard);
            _app.Screenshot("Stored card");

            _app.Tap(nameof(Resource.Id.cardEndingInText));
            _app.WaitForElement(nameof(Resource.Id.enterSecurityText));
            _app.Screenshot("Enter CVV");

            _app.EnterText(nameof(Resource.Id.editCardCVV2), TestCasesData.CVV587);
            _app.Screenshot("Result : Saved Card Payment Functionality");
        }

        [Test]
        public void UserSelectOkOnWarningMessageAndIsTakenToTheHomePage()
        {
            MessageValidationForPcnNoLongerAllowPayment();
        }

        [Test]
        public void MessageValidationForCorrectPcnAndVrmButWrongCombination()
        {
            TestMethods.HomeScreenDisplay(_app);
            _app.Tap(nameof(Resource.Id.buttonPayPCN));
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNGT62886359, TestCasesData.VRMMC52XXX);

            _app.WaitForElement(Strings.CouldntFindPCN);
            _app.Screenshot("Information");
        }

        [Test]
        public void MessageValidationForInvalidPcnNumbers()
        {
            TestMethods.HomeScreenDisplay(_app);
            _app.Tap(nameof(Resource.Id.buttonPayPCN));
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNGT628863HH, TestCasesData.VRMMC52XXX);

            _app.WaitForElement(Strings.PcnNotValid);
            _app.Screenshot("Result : " + Strings.PcnNotValid);
        }

        [Test]
        public void MessageValidationForinvalidVrmNumbers()
        {
            TestMethods.HomeScreenDisplay(_app);
            _app.Tap(nameof(Resource.Id.buttonPayPCN));
            TestMethods.PcnEnterDetailsAndContinue(_app, TestCasesData.PCNGT62886359, TestCasesData.VRMRM021TTN);

            _app.WaitForElement(Strings.PcnNotValid);
            _app.Screenshot("Result : " + Strings.PcnNotValid);
        }
    }
}


