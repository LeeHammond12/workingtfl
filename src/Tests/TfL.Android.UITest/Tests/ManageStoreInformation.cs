﻿using NUnit.Framework;
using Tfl.CongestionCharge.Core.Resources;
using TfL.Android.UITest.Helpers;
using TfL.CongestionCharge.Droid;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace TfL.Android.UITest.Tests
{
    [TestFixture]
    public class ManageStoreInformation
    {
        IApp _app;

        [SetUp]
        public void Setup()
        {
            _app = AppInitializer.StartApp();
        }

        private void SaveCardAndCheckInStoredCardList()
        {
            _app.Tap(Strings.SaveCardDetails);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.Confirmation);
            _app.Tap(Strings.Home);

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Screenshot(Strings.YourCreditCards);

            _app.Tap(nameof(Resource.Id.actionButton));
            _app.WaitForElement(Strings.Confirm);
            _app.WaitForElement(Strings.Yes);
            _app.WaitForElement(Strings.No);
            _app.Screenshot("Warning");
        }

        public void EnterPcnAndPaymentCardcredentials(string pcn, string vrm, string cardNumber, string expiryDate,
            string cardHolderName, string cvv, bool action)
        {
            TestMethods.PcnDetailsScreenDisplay(_app);
            TestMethods.PcnEnterDetailsAndContinue(_app, pcn, vrm);

            _app.WaitForElementWithTimeout(Strings.EnterPaymentDetails);
            _app.Tap(Strings.EnterPaymentDetails);

            if (action)
            {
                _app.Tap(nameof(Resource.Id.selectAnotherCardButton), Strings.AddAnotherCard);
            }

            TestMethods.EnterCardDetails(_app, cardNumber, expiryDate, cardHolderName, cvv);

            _app.Tap(nameof(Resource.Id.chkSaveDetails), Strings.SaveCardDetails);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));
            _app.WaitForElementWithTimeout(nameof(Resource.Id.btnHome));
            _app.Tap(nameof(Resource.Id.btnHome), Strings.Home);
        }

        [Test]
        public void ManagedStoredInformationScreenDisplayCheck()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), (Strings.YourVehiclesAndCards));
            _app.WaitForElement(Strings.ManageStoredInformationDescription);
            _app.Screenshot(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourVehicles);
            _app.WaitForElement(Strings.YourVehicles);
            _app.Screenshot(Strings.YourVehicles);

            _app.Back();
            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Screenshot(Strings.YourCreditCards);

            _app.Back();
            _app.Tap(nameof(Resource.Id.buttonPrivacy), Strings.Privacy);
            _app.WaitForElementWithTimeout(nameof(Resource.Id.btnDone), Strings.Done);
            _app.Screenshot(Strings.Privacy);

            _app.Tap(nameof(Resource.Id.btnDone), Strings.Done);
            _app.WaitForElementWithText(nameof(Resource.Id.buttonTC), Strings.TermsAndConditionTitle);
            _app.Tap(nameof(Resource.Id.buttonTC), Strings.TermsAndConditionTitle);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitle), Strings.TermsAndConditionTitle);
            _app.Screenshot(Strings.TermsAndConditionTitle);
        }

        [Test]
        public void ManagedStoredInformationBackButtonNavigateToMainScreen()
        {
            TestMethods.ManageStoredInformationScreenDisplay(_app);

            _app.TapNavigateBack();
            _app.WaitForElement(nameof(Resource.Id.buttonSignIn));
            _app.Screenshot(Strings.ApplicationName);
        }

        [Test]
        public void ViewPrivacyPolicy()
        {
            TestMethods.ManageStoredInformationScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPrivacy), Strings.Privacy);
            _app.WaitForElementWithTimeout(nameof(Resource.Id.btnDone), Strings.Done);
            _app.Screenshot(Strings.Privacy);
        }

        [Test]
        public void ViewTermsAndConditions()
        {
            TestMethods.ManageStoredInformationScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonTC), Strings.TermsAndConditionTitle);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitle), Strings.TermsAndConditionTitle);
            _app.Screenshot(Strings.TermsAndConditionTitle);

            _app.Tap(nameof(Resource.Id.goToWebButton));
            _app.WaitForElementWithTimeout(nameof(Resource.Id.btnDone), Strings.Done);
            _app.Screenshot("Read More On the Web");
        }

        [Test]
        public void YourCreditCardScreenDisplayCheck()
        {
            TestMethods.ManageStoredInformationScreenDisplay(_app);
            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.ManageStoredCreditCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.WaitForElement(Strings.NoPaymentCardsOnAnonAccount);
            _app.Screenshot("No Credit Cards");
        }

        [Test]
        public void EnsureStoredCardDetailsDisplay()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ52", false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            SaveCardAndCheckInStoredCardList();
        }

        [Test]
        public void YourCreditCardListBackButtonNavigateToManagedStoreInformation()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ5179", false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(Strings.SaveCardDetails);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.Confirmation);
            _app.Tap(Strings.Home);

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Screenshot(Strings.YourCreditCards);

            _app.TapNavigateBack();
            _app.WaitForElement(Strings.YourVehiclesAndCards);

            _app.Screenshot(Strings.YourVehiclesAndCards);
        }

        [Test]
        public void CheckCreditCrdListDisplayWithSavedCards()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ515", false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(Strings.SaveCardDetails);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.Confirmation);
            _app.Tap(Strings.Home);

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.WaitForElement(Strings.ManageStoredCardsDescription);
            _app.Screenshot(Strings.YourCreditCards);
        }

        [Test]
        public void EnsurePaymentCardIsNotDeletedOnSelectingNo()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ511", false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            SaveCardAndCheckInStoredCardList();
            _app.Tap(Strings.No);
            _app.WaitForElement(Strings.ManageStoredCardsDescription);
            _app.Screenshot("Result : Payment Card");
        }

        [Test]
        public void EnsurePaymentCardIsDeletedOnSelectingYes()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ5107", false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            SaveCardAndCheckInStoredCardList();
            _app.Tap(Strings.Yes);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Screenshot("Result : No Payment Card");
        }

        [Test]
        public void CheckYourVehicleListScreenDisplayWithSavedVrm()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4501, false);

            _app.WaitForElement(Strings.ChooseDates);
            _app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            _app.Tap(nameof(Resource.Id.chkSaveVehicle), Strings.SaveVehicleDetails);
            _app.Screenshot("Vehicle details");

            _app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);
            _app.Screenshot("Choose your dates (pay For?)");

            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBack();
            _app.TapNavigateBack();
            _app.WaitForElement(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourVehicles);
            _app.Screenshot(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourVehicles);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.ManageStoredVehicles);
            _app.WaitForElement(Strings.YourVehicles);
            _app.WaitForElement(Strings.RemoveVehicleDescription);
            _app.WaitForElement(nameof(Resource.Id.actionButton));
            _app.Screenshot(Strings.YourVehicles);
        }

        [Test]
        public void VehicleListBackButtonNavigateToManageStoredInformation()
        {
            TestMethods.ManageStoredInformationScreenDisplay(_app);
            _app.Tap(Strings.YourVehicles);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.ManageStoredVehicles);
            _app.Screenshot(Strings.ManageStoredVehicles);

            _app.TapNavigateBack();

            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot(Strings.YourVehiclesAndCards);
        }

        [Test]
        public void CheckYourVehicleListScreenDisplayWithoutAnySavedVrm()
        {
            TestMethods.ManageStoredInformationScreenDisplay(_app);
            _app.Tap(Strings.YourVehicles);
            _app.WaitForElement(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.ManageStoredVehicles);
            _app.WaitForElement(Strings.NoVehiclesOnAnonAccount);
            _app.Screenshot(Strings.YourVehicles);
        }

        [Test]
        public void EnsureVrmIsNotDeletedOnSelectingNo()
        {
            CheckYourVehicleListScreenDisplayWithSavedVrm();

            _app.Tap(nameof(Resource.Id.actionButton));
            _app.WaitForElement(Strings.Confirm);
            _app.Screenshot("Warning");

            _app.Tap(Strings.No);
            _app.WaitForElement(Strings.RemoveVehicleDescription);
            _app.Screenshot("Vrm on selecting No");
        }

        [Test]
        public void EnsureVrmIsDeletedOnSelectingYes()
        {
            CheckYourVehicleListScreenDisplayWithSavedVrm();

            _app.Tap(nameof(Resource.Id.actionButton));
            _app.WaitForElement(Strings.Confirm);
            _app.Screenshot("Warning");

            _app.Tap(Strings.Yes);
            _app.WaitForElement(Strings.NoVehiclesOnAnonAccount);
            _app.Screenshot("Vrm on selecting No");
        }

        [Test]
        public void EnsureConfirmationPopupDisplayOnDeletingVrm()
        {
            CheckYourVehicleListScreenDisplayWithSavedVrm();

            _app.Tap(nameof(Resource.Id.actionButton));
            _app.WaitForElement(Strings.Confirm);
            _app.WaitForElement(Strings.Yes);
            _app.WaitForElement(Strings.No);
            _app.Screenshot("Warning");
        }

        [Test]
        public void EnsureUnableToAddNewCardFromCardList()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4516, false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4462030000000000, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654);
            _app.Tap(Strings.SaveCardDetails);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.Confirmation);
            _app.Tap(Strings.Home);

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.WaitForElement(Strings.ManageStoredCardsDescription);
            _app.Screenshot(Strings.YourCreditCards);

            _app.WaitForNoElement(Strings.AddAnotherCard);
        }

        [Test]
        public void EnsureUnableToAddNewVrmFromVehicleList()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4957, false);

            _app.WaitForElement(Strings.ChooseDates);
            _app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            _app.Tap(nameof(Resource.Id.chkSaveVehicle), Strings.SaveVehicleDetails);
            _app.Screenshot("Vehicle details");

            _app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);
            _app.Screenshot("Choose your dates (pay For?)");

            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBack();

            _app.TapNavigateBack();
            _app.WaitForElement(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourVehicles);
            _app.Screenshot(Strings.YourVehiclesAndCards);

            _app.Tap(Strings.YourVehicles);
            _app.WaitForElement(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.ManageStoredVehicles);
            _app.WaitForElement(Strings.YourVehicles);
            _app.WaitForElement(Strings.RemoveVehicleDescription);
            _app.WaitForElement(nameof(Resource.Id.actionButton));
            _app.Screenshot(Strings.YourVehicles);
            _app.WaitForNoElement(nameof(Resource.Id.btnAddNewVehicle));
        }

        [Test]
        public void DisplayOf15SavedVrmInVehicleList()
        {
            TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4546, false);

            _app.WaitForElement(Strings.ChooseDates);
            _app.Tap(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect), Strings.ConfirmThisVrmIsCorrect);
            _app.Tap(nameof(Resource.Id.chkSaveVehicle), Strings.SaveVehicleDetails);
            _app.Screenshot("Vehicle details");

            _app.Tap(nameof(Resource.Id.nextButton), Strings.ChooseDates);
            _app.WaitForElementWithTimeout(nameof(Resource.Id.ToolbarTitle), Strings.ChooseDates);

            _app.TapNavigateBackWithTimeout();
            _app.TapNavigateBack();
            _app.TapNavigateBack();
            _app.WaitForElement(nameof(Resource.Id.buttonPayCC));

            _app.Tap(nameof(Resource.Id.buttonPayCC));

            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4562, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4501, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4594, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4603, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4708, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4717, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4727, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ474, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4747, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4761, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4767, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4775, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4776, false);
            TestMethods.EnterVrmAndSaveVehicle(_app, TestCasesData.VRMIAZ4957, false);


            _app.TapNavigateBackWithTimeout();
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Tap(Strings.YourVehiclesAndCards);
            _app.Tap(Strings.YourVehicles);

            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.ManageStoredVehicles);
            _app.WaitForElement(Strings.YourVehicles);
            _app.WaitForElementWithText(nameof(Resource.Id.yourVehiclesText), Strings.RemoveVehicleDescription);
            _app.WaitForElement(nameof(Resource.Id.vehicleImage));
            _app.WaitForElement(nameof(Resource.Id.vrmText));
            _app.WaitForElement(nameof(Resource.Id.vehicledetailsText));
            _app.WaitForElement(nameof(Resource.Id.actionButton));

            _app.ScrollUpTo(Strings.YourVehicles);

            _app.Screenshot("Result : Saved Vehicles 1");
            _app.ScrollDown();
            _app.Screenshot("Result : Saved Vehicles 2");
            _app.ScrollDown();
            _app.Screenshot("Result : Saved Vehicles 3");
        }

        [Test]
        public void EnsurePaymentCardIsDisplayedWithMaxOf15SavedCards()
        {
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587, true);
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN4715059999000437, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV124, false);
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN4844215500115643, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654, false);
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN5454609899026213, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV358, false);
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN5569500000002312, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV147, false);
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN5573489900000028, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV852, false);
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN5641820000000005, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV963, false);
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN4462030000000000, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654, false);
            EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, TestCasesData.CN3569990010067584, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV751, false);
            //EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", TestCasesData.VRMMC52XXX, "", "1218", "Anonymous", "");
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Tap(Strings.YourVehiclesAndCards);
            _app.Tap(Strings.YourCreditCards);
            _app.Screenshot("Payment Card Lists");

            _app.TapNavigateBack();
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Screenshot("Your Vehicles and Cards Screen");
        }

        [Test]
        public void EnsureYourPaymentCardsListIsBlank()
        {
            TestMethods.EnterVrmAndTapNext(_app, "IAZ5196", false);
            TestMethods.ConfirmVrmAndTapChooseDate(_app);
            TestMethods.SelectDatesAndConfirm(_app, Strings.DailyCongestionCharge);
            TestMethods.EnterCardDetails(_app, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);

            _app.Tap(Strings.SaveCardDetails);
            _app.Tap(nameof(Resource.Id.btnConfirmAndPay));

            _app.WaitForElementWithTimeout(Strings.Confirmation);
            _app.Tap(Strings.Home);
            _app.Tap(Strings.YourVehiclesAndCards);
            _app.WaitForElement(Strings.YourVehiclesAndCards);
            _app.Tap(Strings.YourCreditCards);
            _app.WaitForElement(Strings.YourCreditCards);
            _app.Tap(nameof(Resource.Id.actionButton));
            _app.WaitForElement(Strings.Confirm);
            _app.Tap(Strings.Yes);
            _app.Screenshot("Result : No Stored Cards Listed");
        }
    }
}
