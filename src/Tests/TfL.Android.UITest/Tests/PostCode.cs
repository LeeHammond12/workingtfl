﻿using NUnit.Framework;
using Tfl.CongestionCharge.Core.Resources;
using TfL.Android.UITest.Helpers;
using TfL.CongestionCharge.Droid;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace TfL.Android.UITest.Tests
{
    [TestFixture]
    class PostCode
    {
        IApp _app;

        [SetUp]
        public void Setup()
        {
            _app = AppInitializer.StartApp();
        }

        public void PostCodeScreenDisplay()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(Strings.CheckAPostCode);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.CheckAPostCode);
            _app.Screenshot(Strings.CheckAPostCode);
        }

        public void CheckPostcodeViaBuildingNumber(string postcode, string buildingNumber)
        {
            _app.EnterText(nameof(Resource.Id.editTextPostCode), postcode);
            _app.DismissKeyboard();
            _app.EnterText(nameof(Resource.Id.txtBuildingNumber), buildingNumber);
            _app.DismissKeyboard();
            _app.Screenshot("Credentials");

            _app.Tap(nameof(Resource.Id.buttonLookupPostCode));
        }

        public void CheckPostCodeViaBuildingName(string postcode, string buildingName)
        {
            _app.EnterText(nameof(Resource.Id.editTextPostCode), postcode);
            _app.DismissKeyboard();
            _app.EnterText(nameof(Resource.Id.txtBuildingName), buildingName);
            _app.DismissKeyboard();
            _app.Screenshot("Credentials");

            _app.Tap(nameof(Resource.Id.buttonLookupPostCode));
        }

        [Test]
        public void MessageValidationForAddressNotFound()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber(TestCasesData.PCN19NF, TestCasesData.BN1);
            _app.WaitForElementWithTimeout(Strings.PostCodeNotFound);
            _app.Screenshot("Result : " + Strings.PostCodeNotFound);
        }

        [Test]
        public void MessageValidationForAddressInsideTheCongestionChargingZoneAndInsideTheResidentZone()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber(TestCasesData.PCW1U6TY, TestCasesData.BN124);

            _app.WaitForElementWithTimeout(Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
            _app.Screenshot("Result : " + Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
        }

        [Test]
        public void MessageValidationForAddressInsideTheCongestionChargingZoneAndOutsideTheResidentZone()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("YO266DL", "10");

            _app.WaitForElementWithTimeout(Strings.AddressInsideCcZone + Strings.AddressOutsideResidentZone);
            _app.Screenshot("Result : " + Strings.AddressInsideCcZone + Strings.AddressOutsideResidentZone);
        }

        [Test]
        public void MessageValidationForAddressOutsideTheCongestionChargingZoneAndInsideTheResidentZone()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber(TestCasesData.PCSW1E5ND, TestCasesData.BN1);

            _app.WaitForElementWithTimeout(Strings.AddressOutsideCcZone + Strings.AddressInsideResidentZone);
            _app.Screenshot("Result : " + Strings.AddressOutsideCcZone + Strings.AddressInsideResidentZone);
        }

        [Test]
        public void MessageValidationForAddressOutsideTheCongestionChargingZoneAndOutsideTheResidentZone()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber(TestCasesData.PCHA28LF, TestCasesData.BN101);

            _app.WaitForElementWithTimeout(Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
            _app.Screenshot("Result : " + Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
        }

        [Test]
        public void CheckPostcodeHomePageScreenDisplay()
        {
            TestMethods.HomeScreenDisplay(_app);
            _app.Tap(Strings.CheckAPostCode);
            _app.WaitForElementWithText(nameof(Resource.Id.ToolbarTitleWithoutCancel), Strings.CheckAPostCode);
            _app.WaitForElement(Strings.FindOutAPostCodeIsInCcZone);
            _app.WaitForElement(Strings.EnterYourPostCode);
            _app.DismissKeyboard();
            _app.WaitForElement(Strings.EnterBuildingNumber);
            _app.WaitForElement(Strings.EnterBuildingName);

            CheckPostcodeViaBuildingNumber(TestCasesData.PCHA28LF, TestCasesData.BN101);

            _app.WaitForElementWithTimeout(Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
            _app.Screenshot("Result : Check Post Code Via Building number");

            _app.TapNavigateBack();
            _app.ClearText(nameof(Resource.Id.editTextPostCode));
            _app.ClearText(nameof(Resource.Id.txtBuildingNumber));

            CheckPostCodeViaBuildingName(TestCasesData.PCSE116DQ, TestCasesData.BNCOALPORTHOUSE);
            _app.WaitForElementWithTimeout(Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
            _app.Screenshot("Result : Check Post Code Via Building number");
        }

        [Test]
        public void PostcodeAndHouseNameValidation()
        {
            PostCodeScreenDisplay();
            CheckPostCodeViaBuildingName(TestCasesData.PCSE116DQ, TestCasesData.BNCOALPORTHOUSE);

            _app.WaitForElementWithTimeout(Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
            _app.Screenshot("Result : " + Strings.Information);
        }

        [Test]
        public void PostcodeAndHouseNumberValidation()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber(TestCasesData.PCHA28LF, TestCasesData.BN101);
            _app.WaitForElementWithTimeout(Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
            _app.Screenshot("Result : " + Strings.Information);
        }

        [Test]
        public void PostcodeCheckButton()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber(TestCasesData.PCHA28LF, TestCasesData.BN101);
            _app.WaitForElementWithTimeout(Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
            _app.Screenshot("Result : Check Post Code Via Building number");

            _app.Back();
            _app.ClearText(nameof(Resource.Id.editTextPostCode));
            _app.ClearText(nameof(Resource.Id.txtBuildingNumber));

            CheckPostCodeViaBuildingName(TestCasesData.PCSE116DQ, TestCasesData.BNCOALPORTHOUSE);
            _app.WaitForElementWithTimeout(Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
            _app.Screenshot("Result : Check Post Code Via Building name");
        }

        [Test]
        public void MessageValidationForIncorrectPostcode()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber(TestCasesData.PCHA28LFF, TestCasesData.BN1);

            _app.WaitForElementWithTimeout(Strings.PleaseEnterAValidPostCode);
            _app.Screenshot("Result : " + Strings.PleaseEnterAValidPostCode);
        }
    }
}
