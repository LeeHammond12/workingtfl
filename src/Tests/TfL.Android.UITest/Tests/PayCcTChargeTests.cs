﻿using NUnit.Framework;
using Tfl.CongestionCharge.Core.Resources;
using TfL.Android.UITest.Helpers;
using TfL.CongestionCharge.Droid;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace TfL.Android.UITest.Tests
{
    [TestFixture]
    public class PayCcTChargeTests
    {
        IApp _app;

        [SetUp]
        public void Setup()
        {
            _app = AppInitializer.StartApp();
        }

        [Test]
        public void MessageValidationForKnownVrmSubjectToTChargeVrm()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElement(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), "J100JOG");
            _app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);
            _app.WaitForDialogWithMessage(Strings.YourVehicleIsSubjectToAnEmissionSurcharge);
            _app.Screenshot("Warning");

            _app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }

        [Test]
        public void MessageValidationForUnknownVrmSubjectToTChargeVrm()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElement(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), "");//Need data for test case
            _app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);

            _app.WaitForDialogWithMessage(Strings.CouldntFindVehicleDetailsForEsVehicle);
            _app.Screenshot("Warning");

            _app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }

        [Test]
        public void MessageValidationForCcExemptAndTChargeVrm()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElement(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), "J111MOV");
            _app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);
            _app.WaitForDialogWithMessage(Strings.EsVehicleIsCcExempt);
            _app.Screenshot("Warning");

            _app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }

        [Test]
        public void MessageValidationForUnknownVrmRegisteredinUk()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElement(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), "UNKNOWN");
            _app.Tap(nameof(Resource.Id.chkUKRegistered), Strings.IsVehicleRegisteredInUk);
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);
            _app.WaitForDialogWithMessage("Couldn't find UK registration"); //(Strings.CouldntFindUkRegisteredVehicle);
            _app.Screenshot("Warning");

            _app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }

        [Test]
        public void MessageValidationForUnknownVrm()
        {
            TestMethods.HomeScreenDisplay(_app);

            _app.Tap(nameof(Resource.Id.buttonPayCC));
            _app.WaitForElement(nameof(Resource.Id.textViewTitle), Strings.ChooseAVehicle);
            _app.EnterText(nameof(Resource.Id.editTextVRM), "UNKNOWN");
            _app.DismissKeyboard();
            _app.Screenshot("Enter Vehicle Details");

            _app.Tap(nameof(Resource.Id.btnCheckVRM), Strings.Next);
            _app.WaitForDialogWithMessage(Strings.CouldntFindVehicle);
            _app.Screenshot("Warning");

            _app.Tap(nameof(Resource.Id.btnOk), Strings.Ok);

            TestMethods.ConfirmVrmAndTapChooseDate(_app);
        }
    }
}
