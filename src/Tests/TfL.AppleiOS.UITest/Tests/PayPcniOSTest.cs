﻿﻿using System;
using NUnit.Framework;
using Tfl.AppleiOS.UITest.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace Tfl.AppleiOS.UITest.Tests
{
    [TestFixture]
    public class PayPcniOSTest
    {
        IApp app;

        [SetUp]
        public void Setup()
        {
            app = ConfigureApp.iOS.EnableLocalScreenshots().StartApp();
        }

        [Test]
        public void MessageValidationForCancelledPcn()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "PC00000009", "ET65LBA");

            app.WaitForElement(Strings.YouCannotPayThisPcn);
            app.Screenshot("Result : " + Strings.Warning + " " + Strings.YouCannotPayThisPcn);
        }

        [Test]
        public void ChangePcnNumberViaBackButton()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "LZ25361494", "MC01CCC");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");

            app.TapNavigationButton();
            app.WaitForElement(Strings.Continue);

            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "LZ25361494", "MC01CCC");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("Result : Change Pcn Number");
        }

        [Test]
        public void VerifyContinueButtonChangesToEnterPaymentDetails()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "LZ25361494", "MC01CCC");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("Result : Continue Button Changes To Enter Payment Detail");
        }

        [Test]
        public void MessageValidationForNoConfirmationofPaymentReceived()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "LZ25361494", "MC01CCC");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");

            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1217", "ANONYMOUS", "231");
            app.TapClass("UIButtonLabel", 0);

            app.WaitForElementWithTextTimeout(Strings.PcnPaymentFailedMessage);
            app.Screenshot("Result : Error");
        }

        [Test]
        public void ValidateSaveSmsReceiptOptionNotAvailable()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "LZ25361494", "MC01CCC");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");
            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);

            app.WaitForNoElement(Strings.SendMyReceiptViaSms);
            app.Screenshot("Result : No SMS Option Available");
        }

        [Test]
        public void PaymentConfirmationScreen()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "LZ25361494", "MC01CCC");


            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");
            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.TapClass("UIButtonLabel", 0);

            app.WaitForElementWithTextTimeout(Strings.PayPenaltyChargeNotice);
            app.WaitForElementWithTextTimeout(Strings.Confirmation);
            app.WaitForElement(Strings.AllDone);
            app.WaitForElement(Strings.ThanksPaymentCompletedSuccessfully);
            app.WaitForElement(Strings.YourReceipt);
            app.WaitForElement(Strings.TypeColon);
            app.WaitForElement(Strings.Pcn);
            app.WaitForElement(Strings.PcnNoColon);
            app.WaitForElement(Strings.ReceiptNo);
            app.WaitForElement(Strings.AmountPaid);
            app.WaitForElement(Strings.PaymentDate);
            app.WaitForElement(Strings.SaveReceipt);

            app.ScrollDown();
            app.WaitForElement(Strings.Home);
            app.Screenshot("Confirmation Screen");

            app.Tap(Strings.SaveReceipt);
            app.WaitForElement(Strings.Receipts);
            app.Screenshot("Result : Home Page");
        }


        [Test]
        public void PaymentDetailsScreenDisplayCheck()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "LZ25361494", "MC01CCC");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");

            app.Tap(Strings.EnterPaymentDetails);

            app.WaitForElementWithTextTimeout(Strings.PayPenaltyChargeNotice);
            app.WaitForElementWithTimeout("PageHeaderLabel", "Payment Details");
            app.WaitForElementWithText("PageDescriptionLabel", "Please enter your Credit Card details.");
            app.WaitForElementWithText("UILabel", "CARD NUMBER");
            app.WaitForElementWithText("UILabel", "EXPIRY DATE");
            app.WaitForElementWithText("UILabel", "CARD HOLDER");
            app.WaitForElementWithText("UILabel", "SECURITY CODE");
            app.WaitForElementWithText("UILabel", "Save card details");
            app.WaitForElement(c => c.Class("UIButtonLabel"));

            app.Screenshot("Result : Payment Details Screen");
        }

        [Test]
        public void MessageValidationForAlreadyPaidPcn()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "LZ25361494", "MC01CCC");

            app.WaitForElement(Strings.YouCannotPayThisPcn);
            app.Screenshot("Result : " + Strings.YouCannotPayThisPcn);
        }

        [Test]
        public void MessageValidationForIncorrectPcnCharacter()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT628863HH", "MC52XXX");

            app.WaitForElement(Strings.PcnHasWrongFormat);
            app.Screenshot("Result : " + Strings.PcnHasWrongFormat);
        }

        [Test]
        public void PCNDetailsScreenDisplayCheck()
        {
            TestMethodsiOS.HomeScreenDisplay(app);

            app.Tap(Strings.PayAPenaltyChargeNotice);
            app.WaitForElement(Strings.PayPenaltyChargeNotice);
            app.WaitForElement(Strings.PcnDetails);
            app.WaitForElement(x => x.Class("UITextField"));
            app.WaitForElement(x => x.Class("UITextField").Index(1));
            app.WaitForElement(Strings.Continue);
            app.DismissKeyboard();
            app.Screenshot("Result : " + Strings.PayAPenaltyChargeNotice);
        }

        [Test]
        public void MessageValidationForOnHoldPcn()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886315", "MC52XXX");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("Result : The Amount Due is:");
        }

        [Test]
        public void PcnUnderRepresenationWhichPlacedOnHold()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886315", "MC52XXX");


            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("Result : Pcn Under Represenation");
        }

        [Test]
        public void MessageValidationForPCNPaymentFailure()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886315", "MC52XXX");


            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");
            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1217", "ANONYMOUS", "231");
            app.TapClass("UIButtonLabel", 0);

            app.WaitForElement(Strings.PcnPaymentFailedMessage, "", new TimeSpan(0, 0, 0, 90, 0));
            app.Screenshot("Result : " + Strings.PcnPaymentFailedMessage);
        }

        [Test]
        public void MessageValidationForPcnNoLongerAllowPayment()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "TF93025009", "ET65LBA");


            app.WaitForElement(Strings.YouCannotPayThisPcn);
            app.Screenshot("Warning");

            app.Tap(Strings.Ok.ToUpper());
            app.Tap(Strings.PayAPenaltyChargeNotice);
            app.Screenshot("Result : Home Screen");
        }

        [Test]
        public void EnsureSaveCardDetailsCheckboxDisplayAndFunctionality()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886359", "RM021GMN");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");
            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);

            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.WaitForElement(c => c.Class("UISwitchModernVisualElement"));
            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Result : Save Card Details");

            app.TapClass("UIButtonLabel", 0);

            app.WaitForElement(Strings.Confirmation, "", new TimeSpan(0, 0, 0, 90, 0));
            app.ScrollDownTo(Strings.Home);
            app.Tap(Strings.Home);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot("Home Screen");

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.Screenshot("Manage Store Information");

            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.ManageStoredCreditCards);
            app.Screenshot("Result : Stored Card");
        }

        [Test]
        public void EnsureASaveReceiptButtonDisplayAndFunctionality()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886359", "RM021GMN");


            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");

            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.TapClass("UIButtonLabel", 0);

            app.WaitForElement(Strings.SaveReceipt, "", new TimeSpan(0, 0, 0, 90, 0));
            app.Screenshot("Result : " + Strings.SaveReceipt + " Button");

            app.Tap(Strings.SaveReceipt);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot("Main Screen");

            app.Tap(Strings.Receipts);
            app.WaitForElement(Strings.Receipts);
            app.Screenshot("Result : Saved Receipt");
        }

        [Test]
        public void SavedCardPaymentFunctionality()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886315", "MC52XXX");

            app.WaitForElement(Strings.EnterPaymentDetails);
            app.Screenshot("The Amount Due is:");
            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);

            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.WaitForElement(c => c.Class("UISwitchModernVisualElement"));
            app.TapClass("UISwitchModernVisualElement", 0);

            app.Screenshot("Save Card Details");

            app.TapClass("UIButtonLabel", 0);

            app.WaitForElement(Strings.Confirmation, "", new TimeSpan(0, 0, 0, 90, 0));
            app.ScrollDownTo(Strings.Home);
            app.Tap(Strings.Home);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot("Home Screen");

            app.Tap(Strings.PayAPenaltyChargeNotice);
            app.WaitForElement(Strings.PcnDetails);
            app.Screenshot("Pay a Penalty Charge Notice(PCN)");

            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886315", "MC52XXX");
            app.Tap(Strings.EnterPaymentDetails);
            app.WaitForElement(Strings.SelectYourStoredCard);
            app.Screenshot("Stored card");

            app.TapClass("ListItemHeaderLabel", 0);
            app.WaitForElementWithText("UILabel", "Enter security code");
            app.WaitForElementWithText("UILabel", "NUMBER:");
            app.WaitForElementWithText("UILabel", "EXPIRY:");
            app.Screenshot("Enter CVV");

            app.EditTextField(0, "587");
            app.Screenshot("Result : Saved Card Payment Functionality");

        }

        [Test]
        public void UserSelectOkOnWarningMessageAndIsTakenToTheHomePage()
        {
            MessageValidationForPcnNoLongerAllowPayment();
        }

        [Test]
        public void MessageValidationForCorrectPcnAndVrmButWrongCombination()
        {
            TestMethodsiOS.HomeScreenDisplay(app);
            app.Tap(Strings.PayAPenaltyChargeNotice);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886359", "MC52XXX");

            app.WaitForElement(Strings.PcnNotValid);
            app.Screenshot("Result : " + Strings.PcnNotValid);
        }

        [Test]
        public void MessageValidationForInvalidPcnNumbers()
        {
            TestMethodsiOS.HomeScreenDisplay(app);
            app.Tap(Strings.PayAPenaltyChargeNotice);

            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT628863HH", "MC52XXX");

            app.WaitForElement(Strings.PcnNotValid);
            app.Screenshot("Result : " + Strings.PcnNotValid);
        }

        [Test]
        public void MessageValidationForinvalidVrmNumbers()
        {
            TestMethodsiOS.HomeScreenDisplay(app);

            app.Tap(Strings.PayAPenaltyChargeNotice);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886359", "RM021TTN");

            app.WaitForElement(Strings.PcnNotValid);
            app.Screenshot("Result : " + Strings.PcnNotValid);
        }
    }
}
