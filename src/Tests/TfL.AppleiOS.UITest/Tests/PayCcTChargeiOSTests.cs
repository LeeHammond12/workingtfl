﻿using System;
using NUnit.Framework;
using Tfl.AppleiOS.UITest.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using TfL.UITest.Helpers;
using Xamarin.UITest;
using Xamarin.UITest.iOS;

namespace Tfl.AppleiOS.UITest.Tests
{
    [TestFixture]
    public class PayCcTChargeiOSTest
    {
        iOSApp app;

        [SetUp]
        public void Setup()
        {
            app = ConfigureApp.iOS.EnableLocalScreenshots().StartApp();
        }

        [Test]
        public void MessageValidationForKnownVrmSubjectToTChargeVrm()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, "J100JOG");

            app.TapClass("UISwitchModernVisualElement", 0);
            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);
            app.WaitForDialogWithMessage(Strings.YourVehicleIsSubjectToAnEmissionSurcharge);
            app.Screenshot("Warning");

            app.Tap(Strings.Ok);

            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void MessageValidationForUnknownVrmSubjectToTChargeVrm()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, "");//Need Data

            app.TapClass("UISwitchModernVisualElement", 0);
            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);
            app.WaitForDialogWithMessage(Strings.CouldntFindVehicleDetailsForEsVehicle);
            app.Screenshot("Warning");

            app.Tap(Strings.Ok);

            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void MessageValidationForCcExemptAndTChargeVrm()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, "J111MOV");

            app.TapClass("UISwitchModernVisualElement", 0);

            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);
            app.WaitForDialogWithMessage(Strings.EsVehicleIsCcExempt);
            app.Screenshot("Warning");

            app.Tap(Strings.Ok);

            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void MessageValidationForUnknownVrmRegisteredinUk()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, "UNKNOWN");
            app.TapClass("UISwitchModernVisualElement", 0);

            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);
            app.WaitForDialogWithMessage("Couldn't find UK registration");//Strings.CouldntFindUkRegisteredVehicle);
            app.Screenshot("Warning");

            app.Tap(Strings.Ok);

            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void MessageValidationForUnknownVrm()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, "UNKNOWN");
            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);

            app.WaitForDialogWithMessage(Strings.CouldntFindVehicle);
            app.Screenshot("Warning");

            app.Tap(Strings.Ok);

            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
        }
    }
}
