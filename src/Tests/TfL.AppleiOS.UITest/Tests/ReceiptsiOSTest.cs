﻿﻿using NUnit.Framework;
using Tfl.AppleiOS.UITest.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace Tfl.AppleiOS.UITest.Tests
{
    [TestFixture]
    public class ReceiptsiOSTest
    {
        IApp app;

        [SetUp]
        public void Setup()
        {
            app = ConfigureApp.iOS.StartApp();
        }

        [Test]
        public void NoLimitOnNumberOfReceiptsStored()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5051", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ5051 PCC Receipt Details");

            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ505", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ505 PCC Receipt Details");

            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5030", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ5030 PCC Receipt Details");

            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5010", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ5010 PCC Receipt Details");

            app.Tap(Strings.Receipts);
            app.WaitForElement(Strings.Receipts);
            app.Screenshot("No limit on number of receipts stored. UP");
            app.ScrollDown();
            app.Screenshot("No limit on number of receipts stored. MIDDLE");
            app.ScrollDown();
            app.ScrollDown();
            app.Screenshot("No limit on number of receipts stored. DOWN");
        }

        [Test]
        public void PccReceiptDetails()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5005", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ5005 PCC Receipt Details");

            app.Tap(Strings.Receipts);
            app.WaitForElement(Strings.Receipts);

            app.WaitForElementWithText("UILabel", Strings.TypeColon);
            app.WaitForElementWithText("UILabel", Strings.VrmColon);
            app.WaitForElementWithText("UILabel", Strings.ReceiptNoColon);
            app.WaitForElementWithText("UILabel", Strings.AmountPaid);
            app.WaitForElementWithText("UILabel", Strings.PaymentDate);
            app.WaitForElementWithText("UILabel", Strings.ChargeType);
            app.WaitForElementWithText("UILabel", Strings.JourneyDates);

            app.Screenshot("Result : Saved PCC Receipt Details");
        }

        [Test]
        public void PCNReceiptDetails()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886315", "IAZ5005");
            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : PCN Receipt Details");

            app.Tap(Strings.Receipts);
            app.WaitForElement(Strings.Receipts);

            app.WaitForElementWithText("UILabel", Strings.TypeColon);
            app.WaitForElementWithText("UILabel", Strings.VrmColon);
            app.WaitForElementWithText("UILabel", Strings.ReceiptNoColon);
            app.WaitForElementWithText("UILabel", Strings.AmountPaid);
            app.WaitForElementWithText("UILabel", Strings.PaymentDate);
            app.WaitForElementWithText("UILabel", Strings.ReceiptPcnNumber);

            app.Screenshot("Result : Saved PCN Receipt Details");
        }

        [Test]
        public void ReceiptNotShown()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5000", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.SaveReceipt);

            app.Screenshot("Result : PCN Receipt Details");
            app.Tap(Strings.Home);

            app.WaitForElement(Strings.Receipts);
            app.Screenshot("Result : Receipt Button Not Shown");
        }

        [Test]
        public void ReceiptScrollBar()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ500", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ500 PCC Receipt Details");

            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ50", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ50 PCC Receipt Details");

            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ5 PCC Receipt Details");

            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ4999", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ4999 PCC Receipt Details");

            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ4988", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : IAZ4988 PCC Receipt Details");

            app.Tap(Strings.Receipts);
            app.WaitForElement(Strings.Receipts);
            app.Screenshot("Result : Receipts - 1");
            app.ScrollDown();
            app.Screenshot("Result : Receipts - 2");
            app.ScrollDown();
            app.Screenshot("Result : Receipts - 3");
            app.ScrollDown();
            app.Screenshot("Result : Receipts - 4");
            app.ScrollDown();
            app.Screenshot("Result : Receipts - 5");
        }

        [Test]
        public void SavePccReceipt()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5001", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result : PCC Receipt Details");

            app.WaitForElement(Strings.Receipts);
            app.Tap(Strings.Receipts);
            app.WaitForElement(Strings.Receipts);
            app.Screenshot("Result : PCC Receipt Saved At Top Of The Page");
        }

        [Test]
        public void SavePCNReceipt()
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, "GT62886315", "IAZ5005");
            TestMethodsiOS.PcnPayemtDetailsScreenDisplay(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            TestMethodsiOS.SaveReceipt(app, "Result - PCN Receipt Details");

            app.WaitForElement(Strings.Receipts);
            app.Tap(Strings.Receipts);
            app.WaitForElement(Strings.Receipts);
            app.Screenshot("Result : PCN Receipt Saved At Top Of The Page");
        }

        [Test]
        public void SmsReceipt()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5004", false);
            TestMethodsiOS.ConfirmVrmAndSaveTapChooseDate(app);
            TestMethodsiOS.SelectDailyCongestionCharge(app);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.ScrollDown();
            app.TapClass("UISwitchModernVisualElement", 1);
            app.EditTextField(4, "07443861232");
            app.DismissKeyboard();
            app.Screenshot("Receipt Send Via SMS");

            TestMethodsiOS.SaveReceipt(app, "Result : Save PCC Receipt Details");
        }
    }
}
