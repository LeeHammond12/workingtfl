﻿using NUnit.Framework;
using Tfl.AppleiOS.UITest.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace Tfl.AppleiOS.UITest.Tests
{
    [TestFixture]
    public class PostCodeiOSTest
    {
        IApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            app = ConfigureApp.iOS.EnableLocalScreenshots().StartApp();
        }
        public void PostCodeScreenDisplay()
        {
            TestMethodsiOS.HomeScreenDisplay(app);
            app.Tap(Strings.CheckAPostCode);
            app.WaitForElement(Strings.CheckAPostCode);
            app.Screenshot(Strings.CheckAPostCode);
        }

        public void CheckPostcodeViaBuildingNumber(string postcode, string buildingNumber)
        {
            app.Tap(Strings.EnterYourPostCode);
            app.EnterText(Strings.EnterYourPostCode, postcode);
            app.DismissKeyboard();
            app.Tap(Strings.EnterBuildingNumber);
            app.EnterText(Strings.EnterBuildingNumber, buildingNumber);
            app.DismissKeyboard();
            app.Screenshot("Post Code Details");

            app.Tap(Strings.CheckAPostCode);
        }

        public void CheckPostCodeViaBuildingName(string postcode, string buildingName)
        {
            app.Tap(Strings.EnterYourPostCode);
            app.EnterText(Strings.EnterYourPostCode, postcode);
            app.DismissKeyboard();
            app.Tap(Strings.EnterBuildingName);
            app.EnterText(Strings.EnterBuildingName, buildingName);
            app.DismissKeyboard();
            app.Screenshot("Post Code Details");

            app.Tap(Strings.CheckAPostCode);
        }

        [Test]
        public void MessageValidationForAddressNotFound()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("N19NF", "1");
            app.WaitForElementWithTimeout("UILabel", Strings.PostCodeNotFound);
            app.Screenshot("Result : " + Strings.PostCodeNotFound);
        }

        [Test]
        public void MessageValidationForAddressInsideTheCongestionChargingZoneAndInsideTheResidentZone()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("W1U6TY", "124");

            app.WaitForElementWithTimeout("UILabel", Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
            app.Screenshot("Result : " + Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
        }

        [Test]
        public void MessageValidationForAddressInsideTheCongestionChargingZoneAndOutsideTheResidentZone()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("YO266DL", "10");

            app.WaitForElementWithTimeout("UILabel", Strings.AddressInsideCcZone + Strings.AddressOutsideResidentZone);
            app.Screenshot("Result : " + Strings.AddressInsideCcZone + Strings.AddressOutsideResidentZone);
        }

        [Test]
        public void MessageValidationForAddressOutsideTheCongestionChargingZoneAndInsideTheResidentZone()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("SW1E5ND", "1");

            app.WaitForElementWithTimeout("UILabel", Strings.AddressOutsideCcZone + Strings.AddressInsideResidentZone);
            app.Screenshot("Result : " + Strings.AddressOutsideCcZone + Strings.AddressInsideResidentZone);
        }

        [Test]
        public void MessageValidationForAddressOutsideTheCongestionChargingZoneAndOutsideTheResidentZone()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("HA28LF", "101");

            app.WaitForElementWithTimeout("UILabel", Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
            app.Screenshot("Result : " + Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
        }

        [Test]
        public void CheckPostcodeHomePageScreenDisplay()
        {
            TestMethodsiOS.HomeScreenDisplay(app);
            app.Tap(Strings.CheckAPostCode);
            app.WaitForElement(Strings.CheckAPostCode);
            app.WaitForElement(Strings.FindOutAPostCodeIsInCcZone);
            app.WaitForElement(Strings.EnterYourPostCode);
            app.DismissKeyboard();
            app.WaitForElement(Strings.EnterBuildingNumber);
            app.WaitForElement(Strings.EnterBuildingName);

            CheckPostcodeViaBuildingNumber("HA28LF", "101");
            app.WaitForElementWithTimeout("UILabel", Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
            app.Screenshot("Result : Check Post Code Via Building number");

            app.Tap(Strings.Ok.ToUpper());

            app.TapNavigationButton();
            app.Tap(Strings.CheckAPostCode);

            CheckPostCodeViaBuildingName("SE116DQ", "COALPORT HOUSE");
            app.WaitForElementWithTimeout("UILabel", Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
            app.Screenshot("Result : Check Post Code Via Building name");
        }

        [Test]
        public void PostcodeAndHouseNameValidation()
        {
            PostCodeScreenDisplay();
            CheckPostCodeViaBuildingName("SE116DQ", "COALPORT HOUSE");

            app.WaitForElementWithTimeout("UILabel", Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
            app.Screenshot("Result : " + Strings.Information);
        }

        [Test]
        public void PostcodeAndHouseNumberValidation()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("HA28LF", "101");
            app.WaitForElementWithTimeout("UILabel", Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
            app.Screenshot("Result : " + Strings.Information);
        }

        [Test]
        public void PostcodeCheckButton()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("HA28LF", "101");


            app.Screenshot("Result : Check Post Code Via Building number");
            app.WaitForElementWithTimeout("UILabel", Strings.AddressOutsideCcZone + Strings.AddressOutsideResidentZone);
            app.Tap(Strings.Ok.ToUpper());

            app.TapNavigationButton();
            app.Tap(Strings.CheckAPostCode);

            CheckPostCodeViaBuildingName("SE116DQ", "COALPORT HOUSE");

            app.WaitForElementWithTimeout("UILabel", Strings.AddressInsideCcZone + Strings.AddressInsideResidentZone);
            app.Screenshot("Result : Check Post Code Via Building name");
        }

        [Test]
        public void MessageValidationForIncorrectPostcode()
        {
            PostCodeScreenDisplay();
            CheckPostcodeViaBuildingNumber("HA28LFF", "1");

            app.WaitForElementWithTimeout("UILabel", Strings.PleaseEnterAValidPostCode);
            app.Screenshot("Result : " + Strings.PleaseEnterAValidPostCode);
        }
    }
}
