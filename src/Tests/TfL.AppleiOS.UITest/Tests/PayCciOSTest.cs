﻿﻿using System;
using NUnit.Framework;
using Tfl.AppleiOS.UITest.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using TfL.UITest.Helpers;
using Xamarin.UITest;
using Xamarin.UITest.iOS;

namespace Tfl.AppleiOS.UITest.Tests
{
    [TestFixture]
    public class PayCciOSTest
    {
        iOSApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            app = ConfigureApp
                .iOS
                .EnableLocalScreenshots()
                .StartApp();
        }

        public void OpenChangeStartDateCalendar()
        {
            app.TapClass("UISwitchModernVisualElement",0);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);

            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");

            app.ScrollDown();
            app.ScrollDown();

            app.Tap(b => b.Button(Strings.ChangeStartDate));
            app.WaitForElement(Strings.Done);
            app.Screenshot("Date Picker");
        }


        [Test]
        public void Validate100PercentDiscountedVrm()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "191VRB1", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");
            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");
        }

        [Test]
        public void PayUpto65ChargingDaysInadvance()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);

            OpenChangeStartDateCalendar();

            app.TapClass("UIButton", 1);
            app.TapClass("UIButton", 1);
            app.TapClass("UIButton", 1);
            app.Tap(c => c.Text("10"));
            app.Tap(Strings.Done);
            app.WaitForElementWithText("UILabel", Strings.CcCannotPayMoreThan65DaysInFuture);
            app.Screenshot("Change Date 65 In advance message");
        }

        [Test]
        public void ChangeStartDateButtonScreenDisplay()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);

            OpenChangeStartDateCalendar();

            var nonChargingdate = DateTime.Now.AddDays(1);
            if (nonChargingdate.DayOfWeek == DayOfWeek.Saturday)
            {
                nonChargingdate = DateTime.Now.AddDays(3);
            }

            if (nonChargingdate.DayOfWeek == DayOfWeek.Sunday)
            {
                nonChargingdate = DateTime.Now.AddDays(2);
            }

            app.Tap(nonChargingdate.Day.ToString());
            app.Screenshot("Change Date");

            app.Tap(Strings.Done);
            app.WaitForElement(Strings.ChangeStartDate);
            app.Screenshot("Change Start Date");
        }

        [Test]
        public void MessageValidationForaChargeInThePast()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);

            OpenChangeStartDateCalendar();

            var nonChargingdate = DateTime.Now.AddDays(-3);
            app.Tap(nonChargingdate.Day.ToString());
            app.Screenshot("Past Date");

            app.Tap(Strings.Done);
            app.WaitForElementWithText("UILabel", Strings.CcCannotPayForAChargeInThePast);
            app.Screenshot("Change Start Date In The Past");
        }

        [Test]
        public void MessageValidationForNonChargingDate()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);

            OpenChangeStartDateCalendar();

            var tomorrow = DateTime.Today.AddDays(1);
            var daysUntilSaturday = ((int)DayOfWeek.Saturday - (int)tomorrow.DayOfWeek + 7) % 7;
            var nextSaturday = tomorrow.AddDays(daysUntilSaturday);

            app.Tap(nextSaturday.Day.ToString());
            app.Screenshot("Non Charging Date");

            app.Tap(Strings.Done);
            app.WaitForElementWithText("UILabel", Strings.CcNonChargingDate);
            app.Screenshot("Change Start Date To non-charging");
        }

        [Test]
        public void PayForScreenDisplayAfterChangeStartDate()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);

            OpenChangeStartDateCalendar();

            var tomorrow = DateTime.Today.AddDays(1);
            var daysUntilMonday = ((int)DayOfWeek.Monday - (int)tomorrow.DayOfWeek + 7) % 7;
            var nextMonday = tomorrow.AddDays(daysUntilMonday);

            app.Tap(nextMonday.Day.ToString());
            app.Screenshot("Valid Date");

            app.Tap(Strings.Done);

            app.Tap(Strings.DailyCongestionCharge);

            app.WaitForPageHeaderLabel();

            app.Screenshot("Journey Details");

            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");
            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");
        }

        [Test]
        public void ChooseYourDatesButtonFunctionality()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);

            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);

            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");
        }

        [Test]
        public void ChooseYourDatesbuttonActivation()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);

            app.WaitForElement(Strings.ChooseDates);
            app.Screenshot("Vehicle details");

            app.Tap(Strings.ChooseDates);
            app.WaitForElement(Strings.PromptUserToConfirmVrmIsCorrect);
            app.Screenshot("Information");

            app.Tap(Strings.Ok.ToUpper());
            app.WaitForElement(Strings.ChooseDates);

            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Vehicle details");

            app.Tap(Strings.ChooseDates);
            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");
        }

        [Test]
        public void PayForScreenDisplayAndFunctionality()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);

            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);
            app.WaitForPageHeaderLabel();
            app.WaitForElementWithText("UILabel", Strings.ChooseDates);
            app.WaitForElementWithText("PageHeaderLabel", Strings.ChangeStartDate);

            app.ScrollDown();
            app.ScrollDown();
            app.Tap(b => b.Button(Strings.ChangeStartDate));
            app.WaitForElement(Strings.Done);
            app.Screenshot("Date Picker");

            app.Tap(Strings.Done);
            app.WaitForElement(Strings.ChangeStartDate);

            app.ScrollUp();
            app.WaitForElement(Strings.DailyCongestionCharge);
            app.WaitForElement(Strings.WeeklyCongestionCharge);
            app.WaitForElement(Strings.MonthlyCongestionCharge);
            app.Screenshot("Pay For?");

            app.Tap(Strings.DailyCongestionCharge);
            app.WaitForPageHeaderLabel();
            app.Screenshot("Journey Details");
        }

        [Test]
        public void ConfirmDetailsAndPayButtonFunctionality()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void CongestionChargePaymentConfirmationScreenCheck()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

            app.WaitForElement(Strings.Confirmation);
            app.WaitForElement(Strings.AllDone);
            app.WaitForElement(Strings.ThanksPaymentCompletedSuccessfully);
            app.WaitForElementWithText("UIButtonLabel", Strings.SaveReceipt);
            app.WaitForElement(Strings.Home);
            app.Screenshot("Confirmation");

            app.Tap(Strings.SaveReceipt);
            app.WaitForElement(Strings.Receipts);
            app.Screenshot("Home");

            app.Tap(Strings.Receipts);
            app.WaitForElement(Strings.Receipts);
            app.Screenshot("Saved Receipts");
        }

        [Test]
        public void ValidationFor90PercentDiscountedVRM()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ3004", true);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void MessageValidationForExemptVrm()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);

            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Main Screen");

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, "IAZ4233");
            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);
            app.WaitForElementWithText("UILabel", Strings.VehicleExemptMessage);
            app.Screenshot("Message For Exempt Vrm");
        }

        [Test]
        public void JourneyDetailPageScreenDisplay()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4516", false);
            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);

            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");

            app.Tap(Strings.DailyCongestionCharge);
            app.WaitForPageHeaderLabel();
            app.WaitForElementWithText("UILabel", Strings.PayCongestionCharge);
            app.WaitForElement(c => c.Class("PageDescriptionLabel"));
            app.WaitForElementWithText("UILabel", Strings.JourneyDatesNoColon);
            app.WaitForElementWithText("UILabel", Strings.Amount);
            app.WaitForElement(c => c.Class("UIButtonLabel").Index(0));
            app.WaitForElement(c => c.Class("UIButtonLabel").Index(1));
            app.Screenshot("Journey Details");
        }

        [Test]
        public void JourneyDetailScreenBackButtonNavigateToPayForScreen()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4516", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);

            app.TapNavigationButton();
            app.WaitForElement(Strings.DailyCongestionCharge);
            app.Screenshot("Choose your dates");
        }

        [Test]
        public void MessageValidationForNoPaymentConfirmation()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4516", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1217", "231");

            app.WaitForElement(t => t.Text(Strings.CcPaymentFailedMessage));
            app.Screenshot("Payment Failure Message");
        }

        [Test]
        public void MessageValidationForNoTransactionIdReceived()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4516", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4844215500115643", "Anonymous", "1217", "123");

            app.WaitForElement(t => t.Text(Strings.CcPaymentFailedMessage));
            app.Screenshot("Payment Failure Message");
        }

        [Test]
        public void AmountVerificationForNoneDiscountedVRM()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4525", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
        }

        [Test]
        public void ValidationForPaymentConfirmationScreen()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4525", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

            app.WaitForElement(Strings.Confirmation);
            app.WaitForElement(Strings.AllDone);
            app.WaitForElement(Strings.ThanksPaymentCompletedSuccessfully);
            app.WaitForElementWithText("UIButtonLabel", Strings.SaveReceipt);
            app.WaitForElement(Strings.Home);
            app.WaitForElement(Strings.YourReceipt);
            app.WaitForElement(Strings.TypeColon);
            app.WaitForElement(Strings.AmountPaid);
            app.WaitForElement(Strings.PaymentDate);
            app.Screenshot("Payment Confirmation Screen");
        }

        [Test]
        public void PaymentConfirmationForaDay()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4525", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");
        }

        [Test]
        public void PaymentConfirmationForaMonth()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4525", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.MonthlyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");
        }

        [Test]
        public void PaymentConfirmationForaWeek()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4525", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.WeeklyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");
        }

        [Test]
        public void PaymentConfirmationForPreviousChargingDay()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4525", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.PreviousChargingDay);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");
        }

        [Test]
        public void VerificationOfReceiptButtonOnNotSavingReceipt()
        {
            ValidationForPaymentConfirmationScreen();

            app.Tap(Strings.Home);
            app.Screenshot("No Receipts");
        }

        [Test]
        public void VerificationOfReceiptButtonOnSavingReceipt()
        {
            CongestionChargePaymentConfirmationScreenCheck();
        }

        [Test]
        public void EnsureMaximum15VrmCanBeSaved()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4546", false);

            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);

            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");

            app.TapNavigationButton();
            app.TapNavigationButton();
            app.TapNavigationButton();

            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4562", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4501", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4594", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4603", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4708", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4717", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4727", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ474", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4747", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4761", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4767", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4775", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4776", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4957", false);

        }

        [Test]
        public void EnsureSaveCardDetailsCheckBoxIsAvailable()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.PreviousChargingDay);

            app.TapClass("UIButtonLabel", 1);
            app.DismissKeyboard();
            app.WaitForElement(Strings.SaveCardDetails);
            app.Screenshot("Save Card Details Checkbox");

        }

        [Test]
        public void MessageValidationForIncorrectCvvNumber()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "555");

            app.WaitForElement(t => t.Text(Strings.PaymentFailedTitle));
            app.Screenshot("Payment Error");
        }

        [Test]
        public void CheckDisplayAndFunctionalityOfSaveReceiptButton()
        {
            CongestionChargePaymentConfirmationScreenCheck();
        }

        /*[Test]
        public void SaveCardDetailsCheckBoxFunctionality()
        {
            EnsureCardDetailsAreTokenized();
            _app.Back();
            _app.Back();

            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4546, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4562, TestCasesData.CN4715059999000437, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV124);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4594, TestCasesData.CN4844215500115643, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4603, TestCasesData.CN5454609899026213, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV358);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4603, TestCasesData.CN5569500000002312, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV147);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4708, TestCasesData.CN5573489900000028, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV852);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4717, TestCasesData.CN5641820000000005, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV963);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4727, TestCasesData.CN4462030000000000, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654);
            SavePaymentCardAndDisplay(TestCasesData.VRMIAZ474, TestCasesData.CN3569990010067584, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV751);
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4747, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4761, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4767, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4775, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4776, "", "1218", "Anonymous", "");
            //SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4777, "", "1218", "Anonymous", "");
            // Need card details
        }*/

        [Test]
        public void EnsureSaveVehicleCheckBoxDisplayAndFunctionality()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);

            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);
            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");

            app.TapNavigationButton();
            app.TapNavigationButton();
            app.TapNavigationButton();
            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Main Screen");

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);
            app.Screenshot("Saved Vehicle");
        }

        [Test]
        public void EnsureDisplayOfSaveVehicleSectionOnAtleastOneSavedVrm()
        {
            EnsureSaveVehicleCheckBoxDisplayAndFunctionality();

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, "IAZ4747");
            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);

            if (app.IsElementAvailable(Strings.Warning))
            {
                app.Screenshot("Warning");
                app.Tap(Strings.Ok.ToUpper());
            }

            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);

            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");

            app.TapNavigationButton();
            app.TapNavigationButton();
            app.TapNavigationButton();
            app.Screenshot("Main Screen");
        }

        [Test]
        public void SaveReceiptButtonAvailabilityAndFunctionality()
        {
            CongestionChargePaymentConfirmationScreenCheck();
        }


        [Test]
        public void SavePaymentCardDisplayAndFunctionality()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);

            app.TapClass("UIButtonLabel", 1);
            app.WaitForPageHeaderLabel();
            app.EditTextField(0, "4543059790016721");
            app.EditTextField(1, "Anonymous");
            app.EditTextField(2, "1218");
            app.EditTextField(3, "587");
            app.DismissKeyboard();

            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Click Save Card CheckBox");

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");

            app.Tap(Strings.Home);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4747", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);

            app.TapClass("UIButtonLabel", 1);
            app.WaitForElementWithText("PageHeaderLabel", Strings.SelectYourStoredCard);
            app.Screenshot("Saved Cards");

            app.TapClass("ListItemHeaderLabel", 0);
            app.WaitForElement(Strings.SendMyReceiptViaSms);
            app.Screenshot("Enter CVV");

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.Cvv2Length);
            app.Screenshot("Error");

            app.Tap(Strings.Ok.ToUpper());
            app.EditTextField(0, "587");

            app.DismissKeyboard();
            app.Screenshot("Enter CVV");

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");
        }


        [Test]
        public void SaveMyReceiptViaSmsCheckboxDisplayAndFunctionality()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);

            app.TapClass("UIButtonLabel", 1);
            app.WaitForPageHeaderLabel();
            app.EditTextField(0, "4543059790016721");
            app.EditTextField(1, "Anonymous");
            app.EditTextField(2, "1218");
            app.EditTextField(3, "587");
            app.DismissKeyboard();

            app.WaitForElement(Strings.SendMyReceiptViaSms);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Click Save Card CheckBox");
            app.Tap(Strings.SendMyReceiptViaSms);
            app.WaitForElement(Strings.PleaseEnterYourSmsNumber);
            app.Screenshot("Enter Your SMS Number");

            app.EditTextField(4, "07443861232");
            app.DismissKeyboard();

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");
        }

        [Test]
        public void MessageValidationForInvalidNumberLessThan11Chars()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            app.TapClass("UIButtonLabel", 1);
            app.WaitForPageHeaderLabel();
            app.EditTextField(0, "4543059790016721");
            app.EditTextField(1, "Anonymous");
            app.EditTextField(2, "1218");
            app.EditTextField(3, "587");
            app.DismissKeyboard();

            app.WaitForElement(Strings.SendMyReceiptViaSms);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Click Save Card CheckBox");
            app.Tap(Strings.SendMyReceiptViaSms);
            app.WaitForElement(Strings.PleaseEnterYourSmsNumber);
            app.Screenshot("Enter Your SMS Number");

            app.EditTextField(4, "0744386123");
            app.DismissKeyboard();

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.SmsNumberLength);
            app.Screenshot("Result : Mobile number validation");
        }

        [Test]
        public void MessageValidationForInvalidNumberDoesNotStartWithZero()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            app.TapClass("UIButtonLabel", 1);
            app.WaitForPageHeaderLabel();
            app.EditTextField(0, "4543059790016721");
            app.EditTextField(1, "Anonymous");
            app.EditTextField(2, "1218");
            app.EditTextField(3, "587");
            app.DismissKeyboard();
            app.Repl();
            app.WaitForElement(Strings.SendMyReceiptViaSms);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Click Save Card CheckBox");
            app.Tap(Strings.SendMyReceiptViaSms);
            app.WaitForElement(Strings.PleaseEnterYourSmsNumber);
            app.Screenshot("Enter Your SMS Number");

            app.EditTextField(4, "74438612323");
            app.DismissKeyboard();

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.SmsNumberStartsWith);
            app.Screenshot("Result : Mobile number validation");
        }

        [Test]
        public void EnsureCardDetailsAreTokenized()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);
            TestMethodsiOS.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);

            app.TapClass("UIButtonLabel", 1);

            app.WaitForPageHeaderLabel();

            app.EditTextField(0, "4543059790016721");
            app.EditTextField(1, "Anonymous");
            app.EditTextField(2, "1218");
            app.EditTextField(3, "587");
            app.DismissKeyboard();

            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.Confirmation);
            app.Screenshot("Confirmation");

            app.Tap(Strings.Home);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot(Strings.Home);

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.Screenshot("Result : Stored cards");
        }

        [Test]
        public void ValidationForAlphanumericCharacterOfvehicleInput()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4501", false);
            app.WaitForElement(c => c.Text("IAZ4501"));
            app.Screenshot("Validation For Alphanumeric Character");

            app.TapNavigationButton();
            app.ClearText(c => c.Class("UITextField"));
            app.EditTextField(0, "IAZ @£4501");

            app.Tap(Strings.Next);
            app.WaitForElement(Strings.VrmCannotContainSpecialCharacters);
            app.Screenshot("Validation For Special Character");
        }

        [Test]
        public void EnsureVehicleValidationViaTheNextButton()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);
            app.WaitForElement(c => c.Text("IAZ4951"));
            app.Screenshot("Validation For Alphanumeric Character");
        }

        [Test]
        public void VehicleValidationViaTheNextButtonForIncorrectVrm()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Main Screen");

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, "IAZ5555");
            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);
            app.WaitForElementWithText("UILabel", Strings.CouldntFindVehicle);
            app.Screenshot("Warning");

            app.Tap(Strings.Ok.ToUpper());
            app.Screenshot("Choose Your Dates");
        }

        [Test]
        public void YesterdayButtonAndMessageValidationForNonChargingDate()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);

            OpenChangeStartDateCalendar();

            var tomorrow = DateTime.Today.AddDays(1);
            var daysUntilSunday = ((int)DayOfWeek.Sunday - (int)tomorrow.DayOfWeek + 7) % 7;
            var nextSunday = tomorrow.AddDays(daysUntilSunday);

            app.Tap(nextSunday.Day.ToString());
            app.Screenshot("Non Charging Date");

            app.Tap(Strings.Done);
            app.WaitForElementWithText("UILabel", Strings.CcNonChargingDate);
            app.Screenshot("Change Start Date To non-charging");

            app.Tap(Strings.Ok.ToUpper());
            app.WaitForNoElement(t => t.Text(Strings.PreviousChargingDay));
            app.Screenshot("No Previous Charging Day");

            app.ScrollDown();
            app.ScrollDown();
            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.Done);
            app.Screenshot("Date Picker");

            var tomorrowDay = DateTime.Today.AddDays(1);
            var daysUntilSaturday = ((int)DayOfWeek.Saturday - (int)tomorrowDay.DayOfWeek + 7) % 7;
            var nextSaturday = tomorrowDay.AddDays(daysUntilSaturday);

            app.Tap((nextSaturday.Day.ToString()));
            app.Screenshot("Non Charging Date");

            app.Tap(Strings.Done);
            app.WaitForElement(Strings.DailyCongestionCharge);
            app.Screenshot("Change Start Date To non-charging");
        }

        [Test]
        public void VerifyVehicleDetailPageDisplay()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);
            TestMethodsiOS.ChooseVehicleAndTapNext(app, "IAZ4951", false);

            app.WaitForElement(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.VehicleRegistrationMark1);
            app.WaitForElement(Strings.CarMake);
            app.WaitForElement(Strings.ConfirmThisVrmIsCorrect);
            app.WaitForElement(Strings.SaveVehicleDetails);
            app.WaitForElement(Strings.ChooseDates);
            app.Screenshot("Result : Vehicle details");
        }

        [Test]
        public void VerifyWhichVehicleScreenDisplay()
        {
            TestMethodsiOS.AcceptTermsAndCondition(app);
            TestMethodsiOS.NoLoginToApp(app);


            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Main Screen");

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);
            app.WaitForElement(Strings.IsVehicleRegisteredInUk);
            app.WaitForElement(Strings.Next);
            app.Screenshot("Result : " + Strings.ChooseAVehicle);
        }
    }
}
