﻿﻿using NUnit.Framework;
using Tfl.AppleiOS.UITest.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using TfL.UITest.Helpers;
using Xamarin.UITest;

namespace Tfl.AppleiOS.UITest.Tests
{
    [TestFixture]
    public class ManageStoreInformationiOSTest
    {
        IApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            app = ConfigureApp.iOS.EnableLocalScreenshots().StartApp();
        }

        private void SaveCardAndCheckInStoredCardList()
        {
            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UIButtonLabel", 0);
            app.WaitForElementWithTextTimeout(Strings.Confirmation);
            app.Tap(Strings.Home);

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.Screenshot(Strings.YourCreditCards);
            app.TapClass("UIButton", 0);
            app.WaitForElement(Strings.Confirm);
            app.WaitForElement(Strings.Yes);
            app.WaitForElement(Strings.No);
            app.Screenshot("Confirmation");
        }

        private void EnterPcnAndPaymentCardcredentials(string pcn, string vrm, string cardNumber, string expiryDate, string cardHolderName, string cvv)
        {
            TestMethodsiOS.PcnDetailsScreenDisplay(app);
            TestMethodsiOS.PcnEnterDetailsAndContinue(app, pcn, vrm);

            app.WaitForElementWithTextTimeout(Strings.EnterPaymentDetails);
            app.Tap(Strings.EnterPaymentDetails);


            if (app.IsElementAvailable("Add another card"))
            {
                app.Tap(c => c.Class("UIButtonLabel").Text("Add another card"));
            }

            TestMethodsiOS.EnterCardDetails(app, cardNumber, expiryDate, cardHolderName, cvv);

            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UIButtonLabel", 0);
            app.WaitForElementWithTextTimeout(Strings.Home);
            app.Tap(Strings.Home);
        }

        [Test]
        public void ManagedStoredInformationScreenDisplayCheck()
        {
            TestMethodsiOS.HomeScreenDisplay(app);

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.ManageStoredInformationDescription);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourVehicles);
            app.WaitForElement(Strings.YourVehicles);
            app.Screenshot(Strings.YourVehicles);

            app.TapNavigationButton();
            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.Screenshot(Strings.YourCreditCards);

            app.TapNavigationButton();
            app.Tap(Strings.Privacy);

            app.Screenshot(Strings.Privacy);

            app.Tap(Strings.Done);
            app.WaitForElement(Strings.TermsAndConditionTitle);
            app.Tap(Strings.TermsAndConditionTitle);
            app.WaitForElement(Strings.TermsAndConditionTitle);
            app.Screenshot("Result : " + Strings.TermsAndConditionTitle);
        }

        [Test]
        public void ManagedStoredInformationBackButtonNavigateToMainScreen()
        {
            TestMethodsiOS.ManageStoredInformationScreenDisplay(app);
            app.TapNavigationButton();
            app.WaitForElementWithTextTimeout(Strings.SignIn);
            app.Screenshot("Result : " + Strings.ApplicationName);
        }

        [Test]
        public void ViewPrivacyPolicy()
        {
            TestMethodsiOS.ManageStoredInformationScreenDisplay(app);
            app.Tap(Strings.Privacy);
            app.WaitForElementWithTextTimeout(Strings.Done);
            app.Screenshot("Result : " + Strings.Privacy);
        }

        [Test]
        public void ViewTermsAndConditions()
        {
            TestMethodsiOS.ManageStoredInformationScreenDisplay(app);
            app.Tap(Strings.TermsAndConditionTitle);
            app.WaitForElement(Strings.TermsAndConditionTitle);
            app.Screenshot(Strings.TermsAndConditionTitle);
            app.Tap(c => c.Class("UIButtonLabel").Text("READ MORE ON THE WEB"));
            app.WaitForElementWithTextTimeout(Strings.Done);
            app.Screenshot("Result : Read More On the Web");
        }

        [Test]
        public void YourCreditCardScreenDisplayCheck()
        {
            TestMethodsiOS.ManageStoredInformationScreenDisplay(app);
            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.ManageStoredCreditCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.WaitForElement(Strings.NoPaymentCardsOnAnonAccount);
            app.Screenshot("Result : No Credit Cards");
        }

        [Test]
        public void EnsureStoredCardDetailsDisplay()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ52", false);
            TestMethodsiOS.ConfirmVrmAndTapChooseDate(app);
            TestMethodsiOS.SelectDatesAndConfirm(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UIButtonLabel", 0);
            app.WaitForElementWithTextTimeout(Strings.Confirmation);
            app.Tap(Strings.Home);

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.YourCreditCards);

            app.Screenshot("Result : Payment Cards Details");
        }

        [Test]
        public void YourCreditCardListBackButtonNavigateToManagedStoreInformation()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5179", false);
            TestMethodsiOS.ConfirmVrmAndTapChooseDate(app);
            TestMethodsiOS.SelectDatesAndConfirm(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.TapClass("UISwitchModernVisualElement", 0);

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElementWithTextTimeout(Strings.Confirmation);
            app.Tap(Strings.Home);

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.Screenshot(Strings.YourCreditCards);

            app.TapNavigationButton();
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot("Result : " + Strings.YourVehiclesAndCards);
        }

        [Test]
        public void CheckCreditCrdListDisplayWithSavedCards()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ515", false);
            TestMethodsiOS.ConfirmVrmAndTapChooseDate(app);
            TestMethodsiOS.SelectDatesAndConfirm(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.TapClass("UISwitchModernVisualElement", 0);

            app.TapClass("UIButtonLabel", 0);
            app.WaitForElementWithTextTimeout(Strings.Confirmation);
            app.Tap(Strings.Home);

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.WaitForElement(Strings.ManageStoredCardsDescription);
            app.Screenshot("Result : " + Strings.YourCreditCards);
        }

        [Test]
        public void EnsurePaymentCardIsNotDeletedOnSelectingNo()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ511", false);
            TestMethodsiOS.ConfirmVrmAndTapChooseDate(app);
            TestMethodsiOS.SelectDatesAndConfirm(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            SaveCardAndCheckInStoredCardList();
            app.Tap(Strings.No);
            app.WaitForElementWithTextTimeout(Strings.ManageStoredCardsDescription);
            app.Screenshot("Result : Payment Card");
        }

        [Test]
        public void EnsurePaymentCardIsDeletedOnSelectingYes()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5107", false);
            TestMethodsiOS.ConfirmVrmAndTapChooseDate(app);
            TestMethodsiOS.SelectDatesAndConfirm(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");
            SaveCardAndCheckInStoredCardList();
            app.Tap(Strings.Yes);
            app.WaitForElementWithTextTimeout(Strings.YourCreditCards);
            app.Screenshot("Result : No Payment Card");
        }

        [Test]
        public void CheckYourVehicleListScreenDisplayWithSavedVrm()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ4501", false);

            app.WaitForElement(Strings.ChooseDates);
            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Vehicle details");

            app.Tap(Strings.ChooseDates);
            app.WaitForElementWithTextTimeout(Strings.ChooseDates);
            app.Screenshot("Choose your dates (pay For?)");

            app.TapNavigationButton();
            app.TapNavigationButton();
            app.TapNavigationButton();

            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehicles);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourVehicles);
            app.WaitForElement(Strings.ManageStoredVehicles);
            app.WaitForElement(Strings.YourVehicles);
            app.WaitForElement(Strings.RemoveVehicleDescription);
            app.WaitForElement(c => c.Class("UIButton"));
            app.Screenshot("Result : " + Strings.YourVehicles);
        }

        [Test]
        public void VehicleListBackButtonNavigateToManageStoredInformation()
        {
            TestMethodsiOS.ManageStoredInformationScreenDisplay(app);
            app.Tap(Strings.YourVehicles);
            app.WaitForElement(Strings.ManageStoredVehicles);
            app.Screenshot(Strings.ManageStoredVehicles);

            app.TapNavigationButton();
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot("Result : " + Strings.YourVehiclesAndCards);
        }

        [Test]
        public void CheckYourVehicleListScreenDisplayWithoutAnySavedVrm()
        {
            TestMethodsiOS.ManageStoredInformationScreenDisplay(app);
            app.Tap(Strings.YourVehicles);
            app.WaitForElement(Strings.ManageStoredVehicles);
            app.WaitForElement(Strings.NoVehiclesOnAnonAccount);
            app.Screenshot("Result : " + Strings.YourVehicles);
        }

        [Test]
        public void EnsureVrmIsNotDeletedOnSelectingNo()
        {
            CheckYourVehicleListScreenDisplayWithSavedVrm();

            app.TapClass("UIButton", 0);
            app.WaitForElement(Strings.Confirm);
            app.Screenshot("Confirmation");

            app.Tap(Strings.No);
            app.WaitForElement(Strings.RemoveVehicleDescription);
            app.Screenshot("Result : Vrm on selecting No");
        }

        [Test]
        public void EnsureVrmIsDeletedOnSelectingYes()
        {
            CheckYourVehicleListScreenDisplayWithSavedVrm();

            app.TapClass("UIButton", 0);
            app.WaitForElement(Strings.Confirm);
            app.Screenshot("Confirmation");

            app.Tap(Strings.Yes);
            app.WaitForElement(Strings.NoVehiclesOnAnonAccount);
            app.Screenshot("Result : Vrm on selecting No");
        }

        [Test]
        public void EnsureConfirmationPopupDisplayOnDeletingVrm()
        {
            CheckYourVehicleListScreenDisplayWithSavedVrm();

            app.TapClass("UIButton", 0);
            app.WaitForElement(Strings.Confirm);
            app.WaitForElement(Strings.Yes);
            app.WaitForElement(Strings.No);
            app.Screenshot("Result : Confirmation");
        }

        [Test]
        public void EnsureUnableToAddNewCardFromCardList()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ4516", false);
            TestMethodsiOS.ConfirmVrmAndTapChooseDate(app);
            TestMethodsiOS.SelectDatesAndConfirm(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.EnterCardDetails(app, "4462030000000000", "1218", "ANONYMOUS", "654");

            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UIButtonLabel", 0);

            app.WaitForElementWithTextTimeout(Strings.Confirmation);

            app.Tap(Strings.Home);

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.WaitForElement(Strings.ManageStoredCardsDescription);
            app.Screenshot("Result : " + Strings.YourCreditCards);

            app.WaitForNoElement(Strings.AddAnotherCard);
        }

        [Test]
        public void EnsureUnableToAddNewVrmFromVehicleList()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ4957", false);

            app.WaitForElement(Strings.ChooseDates);
            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Vehicle details");

            app.Tap(Strings.ChooseDates);
            app.WaitForElement(Strings.ChooseDates);
            app.Screenshot("Choose your dates (pay For?)");

            app.TapNavigationButton();
            app.TapNavigationButton();
            app.TapNavigationButton();

            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehicles);
            app.Screenshot(Strings.YourVehiclesAndCards);

            app.Tap(Strings.YourVehicles);
            app.WaitForElement(Strings.ManageStoredVehicles);
            app.WaitForElement(Strings.YourVehicles);
            app.WaitForElement(Strings.RemoveVehicleDescription);

            app.WaitForElement(c => c.Class("UIButton"));
            app.Screenshot("Result : " + Strings.YourVehicles);
            app.WaitForNoElement(c => c.Class("UIButtonLabel").Text("Choose another vehicle"));
        }

        [Test]
        public void DisplayOf15SavedVrmInVehicleList()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ4546", false);

            app.WaitForElement(Strings.ChooseDates);
            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Vehicle details");

            app.Tap(Strings.ChooseDates);
            app.WaitForElement(Strings.ChooseDates);
            app.Screenshot("Choose your dates (pay For?)");

            app.TapNavigationButton();
            app.TapNavigationButton();
            app.TapNavigationButton();

            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4562", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4501", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4594", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4603", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4708", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4717", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4727", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ474", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4747", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4761", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4767", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4775", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4776", false);
            TestMethodsiOS.ChooseAndSaveVehicle(app, "IAZ4957", false);

            app.Tap(Strings.YourVehiclesAndCards);
            app.Tap(Strings.YourVehicles);

            app.WaitForElement(Strings.ManageStoredVehicles);
            app.WaitForElement(Strings.YourVehicles);
            app.WaitForElement(Strings.RemoveVehicleDescription);
            app.WaitForElement("car.png");
            app.WaitForElement(c => c.Class("ListItemHeaderLabel"));
            app.WaitForElement(c => c.Class("UILabel").Index(1));
            app.WaitForElement("remove");

            app.ScrollUpTo("Your Vehicle");
            app.Screenshot("Saved Vehicles 1");
            app.ScrollDown();
            app.Screenshot("Saved Vehicles 2");
            app.ScrollDown();
            app.Screenshot("Saved Vehicles 3");

        }

        [Test]
        public void EnsurePaymentCardIsDisplayedWithMaxOf15SavedCards()
        {
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "4543059790016721", "1218", "ANONYMOUS", "587");
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "4715059999000437", "1218", "ANONYMOUS", "124");
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "4844215500115643", "1218", "ANONYMOUS", "654");
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "5454609899026213", "1218", "ANONYMOUS", "358");
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "5569500000002312", "1218", "ANONYMOUS", "147");
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "5573489900000028", "1218", "ANONYMOUS", "852");
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "5641820000000005", "1218", "ANONYMOUS", "963");
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "4462030000000000", "1218", "ANONYMOUS", "654");
            EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "3569990010067584", "1218", "ANONYMOUS", "751");
            //EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "", "1218", "Anonymous", "");
            //EnterPcnAndPaymentCardcredentials("GT62886315", "MC52XXX", "", "1218", "Anonymous", "");
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Tap(Strings.YourVehiclesAndCards);
            app.Tap(Strings.YourCreditCards);
            app.Screenshot("Payment Card Lists");

            app.TapNavigationButton();
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot("Result : Your Vehicles and Cards Screen");
        }

        [Test]
        public void EnsureYourPaymentCardsListIsBlank()
        {
            TestMethodsiOS.EnterVrmAndTapNext(app, "IAZ5196", false);
            TestMethodsiOS.ConfirmVrmAndTapChooseDate(app);
            TestMethodsiOS.SelectDatesAndConfirm(app, Strings.DailyCongestionCharge);
            TestMethodsiOS.EnterCardDetails(app, "4543059790016721", "1218", "ANONYMOUS", "587");

            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UIButtonLabel", 0);

            app.WaitForElementWithTextTimeout(Strings.Confirmation);
            app.Tap(Strings.Home);
            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Tap(Strings.YourCreditCards);
            app.WaitForElement(Strings.YourCreditCards);
            app.TapClass("UIButton", 0);

            app.WaitForElement(Strings.Confirm);
            app.Tap(Strings.Yes);
            app.Screenshot("Result : No Stored Cards Listed");
        }
    }
}
