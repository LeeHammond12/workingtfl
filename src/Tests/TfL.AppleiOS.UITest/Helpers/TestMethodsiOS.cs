﻿using System;
using Tfl.CongestionCharge.Core.Resources;
using TfL.UITest.Helpers;
using Xamarin.UITest;
using Xamarin.UITest.iOS;

namespace Tfl.AppleiOS.UITest.Helpers
{
    public class TestMethodsiOS
    {
        public static void HomeScreenDisplay(IApp app)
        {
            var button = app.WaitForAnyElement(Strings.Ok.ToUpper(), Strings.AcceptTermsAndCondition);

            if (button == Strings.Ok.ToUpper())
            {
                app.Tap(Strings.Ok.ToUpper());
                app.WaitForElement(Strings.AcceptTermsAndCondition);
                app.Screenshot("Home Screen");
            }
            else if (button == Strings.AcceptTermsAndCondition)
            {
                app.WaitForElement(Strings.AcceptTermsAndCondition);
                app.Screenshot("Home Screen");
            }
            else
            {
                throw new Exception("Not Found");
            }

            app.Tap((Strings.AcceptTermsAndCondition));
            app.WaitForElementWithText("UILabel", Strings.Information);
            app.Screenshot("Information");

            app.Tap(Strings.No);
            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Main Screen");

        }

        public static void AcceptTermsAndCondition(iOSApp app)
        {
            var button = app.WaitForAnyElement(Strings.Ok.ToUpper(), Strings.AcceptTermsAndCondition);

            if (button == Strings.Ok.ToUpper())
            {
                app.Tap(Strings.Ok.ToUpper());
                app.WaitForElement(Strings.AcceptTermsAndCondition);
                app.Screenshot("Home Screen");
            }
            else if (button == Strings.AcceptTermsAndCondition)
            {
                app.WaitForElement(Strings.AcceptTermsAndCondition);
                app.Screenshot("Home Screen");
            }
            else
            {
                throw new Exception("Not Found");
            }

            app.Tap(Strings.AcceptTermsAndCondition);
        }

        public static void NoLoginToApp(iOSApp app)
        {
            app.WaitForElement(Strings.Information);
            app.Screenshot("Information");

            app.Tap(Strings.No);
        }

        public static void ChooseVehicleAndTapNext(iOSApp app, string vrm, bool expectWarning)
        {
            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Main Screen");

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);

            app.EditTextField(0, vrm);
            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);

            if (expectWarning)
            {
                app.Screenshot("Warning");
                app.Tap(Strings.Ok.ToUpper());
            }

            if (!expectWarning && app.ElementExists(Strings.Warning))
            {
                throw new Exception("'Vehicle not found' warning shown, even though it shouldn't");
            }
        }

        public static void ConfirmVrmAndTapSelectDates(iOSApp app, string payFor)
        {
            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);

            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");
            app.Tap(t => t.Text(payFor));
            app.WaitForPageHeaderLabel();
            app.Screenshot("Journey Details");
        }

        public static void ConfirmDetailsAndMakePayment(iOSApp app, string cardNumber, string cardHolderName, string cardExpiryDate, string cardCvv)
        {
            app.TapClass("UIButtonLabel", 1);
            app.WaitForPageHeaderLabel();
            app.EditTextField(0, cardNumber);
            app.EditTextField(1, cardHolderName);
            app.EditTextField(2, cardExpiryDate);
            app.EditTextField(3, cardCvv);

            app.DismissKeyboard();
            app.Screenshot("payment Details");

            app.TapClass("UIButtonLabel", 0);
        }

        public static void EnterVrmAndTapNext(IApp app, string vrm, bool expectWarning)
        {
            var button = app.WaitForAnyElement(Strings.AcceptTermsAndCondition, Strings.PayCongestionCharge);

            if (button == Strings.AcceptTermsAndCondition)
            {
                HomeScreenDisplay(app);
                app.Tap(Strings.PayCongestionCharge);
            }
            else if (button == Strings.PayCongestionCharge)
            {
                app.Tap(Strings.PayCongestionCharge);
                app.Tap(Strings.ChooseAVehicle);
            }
            else
            {
                throw new Exception("Not Found");
            }

            app.WaitForElement(Strings.ChooseAVehicle);

            if (app.ElementExists("Choose another vehicle"))
            {
                app.ScrollDownTo("Choose another vehicle", null, 0, 0.67, 500, true, new TimeSpan(0, 0, 0, 30, 0));
                app.WaitForElement("Choose another vehicle");
                app.Tap("Choose another vehicle");
            }

            app.EditTextField(0, vrm);
            app.DismissKeyboard();
            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Enter Vehicle Details");
            app.Tap(Strings.Next);

            if (expectWarning)
            {
                app.Screenshot("Warning");
                app.Tap(Strings.Ok.ToUpper());
            }

            if (!expectWarning && app.ElementExists(Strings.Warning))
            {
                throw new Exception("'Vehicle not found' warning shown, even though it shouldn't");
            }
        }

        public static void PcnDetailsScreenDisplay(IApp app)
        {
            var button = app.WaitForAnyElement(Strings.AcceptTermsAndCondition, Strings.PayAPenaltyChargeNotice);

            if (button == Strings.AcceptTermsAndCondition)
            {
                HomeScreenDisplay(app);
                app.Tap(Strings.PayAPenaltyChargeNotice);
            }
            else if (button == Strings.PayAPenaltyChargeNotice)
            {
                app.Tap(Strings.PayAPenaltyChargeNotice);
            }
            else
            {
                throw new Exception("Not Found");
            }

            app.WaitForElement(Strings.PcnDetails);
            app.Screenshot("Pay a Penalty Charge Notice(PCN)");
        }

        public static void ManageStoredInformationScreenDisplay(IApp app)
        {
            HomeScreenDisplay(app);
            app.Tap(Strings.YourVehiclesAndCards);
            app.WaitForElement(Strings.YourVehiclesAndCards);
            app.Screenshot(Strings.YourVehiclesAndCards);
        }

        public static void ConfirmVrmAndTapChooseDate(IApp app)
        {
            app.WaitForElement(Strings.ChooseDates);
            app.TapClass("UISwitchModernVisualElement", 0);
            app.Screenshot("Vehicle details");

            app.Tap(Strings.ChooseDates);
            app.WaitForElement(Strings.ChooseDates);
            app.Screenshot("Choose your dates (pay For?)");
        }

        public static void SelectDatesAndConfirm(IApp app, string dates)
        {
            app.Tap(c => c.Class("UILabel").Text(dates));
            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Journey Details");

            app.TapClass("UIButtonLabel", 1);
            app.WaitForPageHeaderLabel();
            app.Screenshot("Payment details");
        }

        public static void EnterCardDetails(IApp app, string cardNumber, string cardExpiryDate, string cardHolderName, string cvv)
        {
            app.EditTextField(0, cardNumber);
            app.EditTextField(1, cardHolderName);
            app.DismissKeyboard();

            app.EditTextField(2, cardExpiryDate);
            app.EditTextField(3, cvv);

            app.DismissKeyboard();
            app.Screenshot("Card Details");
        }

        public static void PcnEnterDetailsAndContinue(IApp app, string pcnNumber, string vrm)
        {
            app.ClearText(c => c.Class("UITextField"));
            app.ClearText(c => c.Class("UITextField").Index(1));
            app.EditTextField(0, pcnNumber);
            app.EditTextField(1, vrm);
            app.DismissKeyboard();
            app.Screenshot(Strings.PcnDetails);
            app.Tap(Strings.Continue);
        }

        public static void ConfirmVrmAndSaveTapChooseDate(IApp app)
        {
            app.WaitForElement(Strings.ChooseDates);
            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Vehicle details");

            app.Tap(Strings.ChooseDates);
            app.WaitForElement(Strings.ChooseDates);
            app.Screenshot("Choose your dates (pay For?)");
        }

        public static void SelectDailyCongestionCharge(IApp app)
        {
            app.Tap(Strings.DailyCongestionCharge);
            app.WaitForElement("Journey Details");

            var isConfirmDetailsAndPayAvailable = app.Query(e => e.Class("UIButtonLabel").Index(1));

            if (isConfirmDetailsAndPayAvailable.Length == 1)
            {
                app.Screenshot("Result");
                app.TapClass("UIButtonLabel", 1);
            }

            app.WaitForPageHeaderLabel();
        }

        public static void SaveReceipt(IApp app, string result)
        {
            app.TapClass("UIButtonLabel", 0);
            app.WaitForElement(Strings.Confirmation);
            app.WaitForElement(Strings.SaveReceipt);
            app.Screenshot(result);
            app.Tap(Strings.SaveReceipt);
        }

        public static void PcnPayemtDetailsScreenDisplay(IApp app)
        {
            app.Tap(Strings.EnterPaymentDetails);
            app.WaitForPageHeaderLabel();
            app.Screenshot(Strings.PaymentDetails);
        }

        public static void ChooseAndSaveVehicle(IApp app, string Vrm, bool expectWarning)
        {
            app.WaitForElement(Strings.PayCongestionCharge);
            app.Screenshot("Main Screen");

            app.Tap(Strings.PayCongestionCharge);
            app.WaitForElement(Strings.ChooseAVehicle);
            app.ScrollDownTo("Choose another vehicle");
            app.Screenshot("Saved Vehicles");

            app.Tap("Choose another vehicle");
            app.WaitForElement(Strings.ChooseAVehicle);
            app.EditTextField(0, Vrm);
            app.DismissKeyboard();
            app.Screenshot("Choose Vehicle");

            app.Tap(Strings.Next);

            if (expectWarning)
            {
                app.Screenshot("Warning");
                app.Tap(Strings.Ok.ToUpper());
            }

            if (!expectWarning && app.ElementExists(Strings.Warning))
            {
                throw new Exception("'Vehicle not found' warning shown, even though it shouldn't");
            }


            app.TapClass("UISwitchModernVisualElement", 0);
            app.TapClass("UISwitchModernVisualElement", 1);
            app.Screenshot("Vehicle Details");

            app.Tap(Strings.ChooseDates);

            app.WaitForPageHeaderLabel();
            app.Screenshot("Pay For?");

            app.TapClass("UINavigationButton", 0);
            app.TapClass("UINavigationButton", 0);
            app.TapClass("UINavigationButton", 0);
            app.TapClass("UINavigationButton", 0);
        }

    }
}
