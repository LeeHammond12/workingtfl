﻿using System;
using System.Threading;
using Xamarin.UITest;
using System.Linq;
using Tfl.CongestionCharge.Core.Extensions;
using Xamarin.UITest.Queries;

namespace TfL.UITest.Helpers
{
    public static class UiTestIAppExtensions
    {
		public static void WaitForDialogWithMessage(this IApp app, string expected)
        {
            var messageElement = app.WaitForElement(c => c.Class("UITextView"));

            var message = messageElement.First().Text;    

			var strippedMessage = StripMessage(message);
			var strippedExpected = StripMessage(expected);

			if (strippedMessage.CalcLevenshteinDistance(strippedExpected) > 5)
			{
				throw new Exception($"Expected message '{strippedExpected}' but got '{strippedMessage}'");
			}
		}

		public static void EditTextField(this IApp app, int index, string text)
        {
            app.EnterText(c => c.Class("UITextField").Index(index), text);
        }

        public static string WaitForAnyElement(this IApp app, params string[] values)
        {
            app.WaitFor(() => values.FirstOrDefault(v => app.Query(v).FirstOrDefault() != null) != null, "", TimeSpan.FromSeconds(5));
            return values.FirstOrDefault(v => app.Query(v).FirstOrDefault() != null);
        }

        public static void WaitForPageHeaderLabel(this IApp app)
        {
            app.WaitForElement(c => c.Class("PageHeaderLabel"));
        }

        public static void WaitForElementWithTimeout(this IApp app, string elementName)
        {
            app.WaitForElement(c => c.Class(elementName), "Wait for element timed out", TimeSpan.FromSeconds(90));
        }

        public static void WaitForElementWithTimeout(this IApp app, string elementName, string elementText)
        {
            app.WaitForElement(c => c.Class(elementName).Text(elementText), "Wait for element timed out", TimeSpan.FromSeconds(90));
        }

        public static void WaitForElementWithTextTimeout(this IApp app, string elementText)
        {
            app.WaitForElement(elementText, "Wait for element timed out", TimeSpan.FromSeconds(90));
        }

        public static void WaitForElementWithText(this IApp app, string elementName, string elementText)
        {
            app.WaitForElement(c => c.Class(elementName).Text(elementText));
        }

        public static void TapClass(this IApp app, string elementName, int index)
        {
            app.Tap(c => c.Class(elementName).Index(index));
        }

        public static void TapNavigationButton(this IApp app)
        {
            app.Tap(c => c.Class("UINavigationButton"));
        }

        public static bool ElementExists(this IApp app, string element)
        {
            return app.Query(c => c.Text(element)).FirstOrDefault() != null;
        }

        public static bool IsElementAvailable(this IApp app, string element)
        {
            const int maxSleepTime = 1000;

            int sleepTime = 0;

            do
            {
                var result = app.Query(c => c.Id(element));
                if (result.Length > 0)
                {
                    return true;
                }

                const int sleepDuration = 200;
                Thread.Sleep(sleepDuration);
                sleepTime += sleepDuration;

            } while (sleepTime < maxSleepTime);

            return false;
        }

		private static string StripMessage(string message)
		{
			message = message.StripHtml();
			message = message.Replace("\r", "");
			message = message.Replace("\n", "");

			return message;
		}
    }
}
