﻿using System;
using NUnit.Framework;
using Tfl.CongestionCharge.Core.Resources;
using Xamarin.UITest.iOS;
using Xamarin.UITest;


namespace Tfl.AppleiOS.UITest
{
	[TestFixture]
	public class Tests
	{
		iOSApp app;

		[SetUp]
		public void BeforeEachTest()
		{
			app = ConfigureApp
				.iOS
				.EnableLocalScreenshots()
				.StartApp();
		}

		public void OpenChangeStartDateCalendar()
		{
			app.Tap(c => c.Class("UISwitchModernVisualElement"));
			app.Screenshot("Vehicle Details");

			app.Tap(c => c.Button(Strings.ChooseDates));

			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.Screenshot("Pay For?");

			app.ScrollDown();
			app.ScrollDown();
			app.Tap(c => c.Button(Strings.ChangeStartDate));
			app.WaitForElement(c => c.Button(Strings.Done));
			app.Screenshot("Date Picker");
		}

		[Test]
		public void Validate100PercentDiscountedVrm()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "191VRB1");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");
			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");
		}

		[Test]
		public void PayUpto65ChargingDaysInadvance()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");

			OpenChangeStartDateCalendar();

			app.Tap(c => c.Class("UIButton").Index(1));
			app.Tap(c => c.Class("UIButton").Index(1));
			app.Tap(c => c.Class("UIButton").Index(1));
			app.Tap(c => c.Text("10"));
			app.Tap(c => c.Button(Strings.Done));
			app.WaitForElement(c => c.Text(Strings.CcCannotPayMoreThan65DaysInFuture));
			app.Screenshot("Change Date 65 In advance message");
		}

		[Test]
		public void ChangeStartDateButtonScreenDisplay()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");

			OpenChangeStartDateCalendar();

			var nonChargingdate = DateTime.Now.AddDays(1);
			if (nonChargingdate.DayOfWeek == DayOfWeek.Saturday)
			{
				nonChargingdate = DateTime.Now.AddDays(3);
			}

			if (nonChargingdate.DayOfWeek == DayOfWeek.Sunday)
			{
				nonChargingdate = DateTime.Now.AddDays(2);
			}

			app.Tap(c => c.Text(nonChargingdate.Day.ToString()));
			app.Screenshot("Change Date");

			app.Tap(c => c.Button(Strings.Done));
			app.WaitForElement(c => c.Text(Strings.ChangeStartDate));
			app.Screenshot("Change Start Date");
		}

		[Test]
		public void MessageValidationForaChargeInThePast()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");

			OpenChangeStartDateCalendar();

			var nonChargingdate = DateTime.Now.AddDays(-3);
			app.Tap(c => c.Text(nonChargingdate.Day.ToString()));
			app.Screenshot("Past Date");

			app.Tap(c => c.Button(Strings.Done));
			app.WaitForElement(c => c.Text(Strings.CcCannotPayForAChargeInThePast));
			app.Screenshot("Change Start Date In The Past");
		}

		[Test]
		public void MessageValidationForNonChargingDate()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");

			OpenChangeStartDateCalendar();

			var tomorrow = DateTime.Today.AddDays(1);
			var daysUntilSaturday = ((int)DayOfWeek.Saturday - (int)tomorrow.DayOfWeek + 7) % 7;
			var nextSaturday = tomorrow.AddDays(daysUntilSaturday);

			app.Tap(c => c.Text(nextSaturday.Day.ToString()));
			app.Screenshot("Non Charging Date");

			app.Tap(c => c.Button(Strings.Done));
			app.WaitForElement(c => c.Text(Strings.CcNonChargingDate));
			app.Screenshot("Change Start Date To non-charging");
		}

		[Test]
		public void PayForScreenDisplayAfterChangeStartDate()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");

			OpenChangeStartDateCalendar();

			var tomorrow = DateTime.Today.AddDays(1);
			var daysUntilMonday = ((int)DayOfWeek.Monday - (int)tomorrow.DayOfWeek + 7) % 7;
			var nextMonday = tomorrow.AddDays(daysUntilMonday);

			app.Tap(c => c.Text(nextMonday.Day.ToString()));
			app.Screenshot("Valid Date");

			app.Tap(c => c.Button(Strings.Done));

			app.Tap(c => c.Text(Strings.DailyCongestionCharge));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.Screenshot("Journey Details");

			TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");
			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");
		}

		[Test]
		public void ChooseYourDatesButtonFunctionality()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");

			app.Tap(c => c.Class("UISwitchModernVisualElement"));
			app.Screenshot("Vehicle Details");

			app.Tap(c => c.Button(Strings.ChooseDates));

			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.Screenshot("Pay For?");
		}

		[Test]
		public void ChooseYourDatesbuttonActivation()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");

			app.WaitForElement(c => c.Button(Strings.ChooseDates));
			app.Screenshot("Vehicle details");

			app.Tap(c => c.Button(Strings.ChooseDates));
			app.WaitForElement(c => c.Text(Strings.PromptUserToConfirmVrmIsCorrect));
			app.Screenshot("Information");

			app.Tap(c => c.Marked(Strings.Ok.ToUpper()));
			app.WaitForElement(c => c.Button(Strings.ChooseDates));

			app.Tap(c => c.Class("UISwitchModernVisualElement"));
			app.Screenshot("Vehicle details");

			app.Tap(c => c.Button(Strings.ChooseDates));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.Screenshot("Pay For?");
		}

		[Test]
		public void PayForScreenDisplayAndFunctionality()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");

			app.Tap(c => c.Class("UISwitchModernVisualElement"));
			app.Screenshot("Vehicle Details");

			app.Tap(c => c.Button(Strings.ChooseDates));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.WaitForElement(c => c.Class("UILabel").Text(Strings.ChooseDates));
			app.WaitForElement(c => c.Class("PageHeaderLabel").Text(Strings.ChangeStartDate));

			app.ScrollDown();
			app.ScrollDown();
			app.Tap(c => c.Button(Strings.ChangeStartDate));
			app.WaitForElement(c => c.Button(Strings.Done));
			app.Screenshot("Date Picker");

			app.Tap(c => c.Button(Strings.Done));
			app.WaitForElement(c => c.Text(Strings.ChangeStartDate));

			app.ScrollUp();
			app.WaitForElement(c => c.Text(Strings.DailyCongestionCharge));
			app.WaitForElement(c => c.Text(Strings.WeeklyCongestionCharge));
			app.WaitForElement(c => c.Text(Strings.MonthlyCongestionCharge));
			app.Screenshot("Pay For?");

			app.Tap(c => c.Text(Strings.DailyCongestionCharge));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.Screenshot("Journey Details");
		}

		[Test]
		public void ConfirmDetailsAndPayButtonFunctionality()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4501");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
		}

		[Test]
		public void CongestionChargePaymentConfirmationScreenCheck()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "VRM4");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");
			
            app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.WaitForElement(c => c.Text(Strings.AllDone));
			app.WaitForElement(c => c.Text(Strings.ThanksPaymentCompletedSuccessfully));
			app.WaitForElement(c => c.Class("UIButtonLabel").Text(Strings.SaveReceipt));
			app.WaitForElement(c => c.Button(Strings.Home));
            app.Screenshot("Confirmation");

			app.Tap(c => c.Class("UIButtonLabel").Text(Strings.SaveReceipt));
			app.WaitForElement(c => c.Class("UIButtonLabel").Text(Strings.Receipts));
			app.Screenshot("Home");

			app.Tap(c => c.Class("UIButtonLabel").Text(Strings.Receipts));
			app.WaitForElement(c => c.Class("UILabel").Text(Strings.Receipts));
			app.Screenshot("Saved Receipts");
		}

		[Test]
		public void ValidationFor90PercentDiscountedVRM()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
            TestMethods.ChooseVehicleAndTapNext(app, "IAZ3004");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
		}

		[Test]
		public void MessageValidationForExemptVrm()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);

			app.WaitForElement(c => c.Marked(Strings.PayCongestionCharge));
			app.Screenshot("Main Screen");

			app.Tap(c => c.Button(Strings.PayCongestionCharge));
			app.WaitForElement(c => c.Marked(Strings.ChooseAVehicle));

			app.EnterText(c => c.Class("UITextField"), "IAZ4233");
			app.DismissKeyboard();
			app.Screenshot("Choose Vehicle");

			app.Tap(c => c.Button(Strings.Next));
			app.WaitForElement(c => c.Text(Strings.VehicleExemptMessage));
            app.Screenshot("Message For Exempt Vrm");
		}

		[Test]
		public void JourneyDetailPageScreenDisplay()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4516");
			app.Tap(c => c.Class("UISwitchModernVisualElement"));
			app.Screenshot("Vehicle Details");

			app.Tap(c => c.Button(Strings.ChooseDates));

			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.Screenshot("Pay For?");

			app.Tap(c => c.Text(Strings.DailyCongestionCharge));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.WaitForElement(c => c.Class("UILabel").Text(Strings.PayCongestionCharge));
			app.WaitForElement(c => c.Class("PageDescriptionLabel"));
			app.WaitForElement(c => c.Class("UILabel").Text(Strings.JourneyDatesNoColon));
			app.WaitForElement(c => c.Class("UILabel").Text(Strings.Amount));
			app.WaitForElement(c => c.Class("UIButtonLabel").Index(0));
			app.WaitForElement(c => c.Class("UIButtonLabel").Index(1));
			app.Screenshot("Journey Details");
		}

		[Test]
		public void JourneyDetailScreenBackButtonNavigateToPayForScreen()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4516");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);

			app.Tap(c => c.Class("UINavigationButton").Index(0));
			app.WaitForElement(c => c.Marked(Strings.DailyCongestionCharge));
			app.Screenshot("Choose your dates");
		}

		[Test]
		public void MessageValidationForNoPaymentConfirmation()
		{
			TestMethods.AcceptTermsAndCondition(app);
            TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4516");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
            TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1217", "231");

			app.WaitForElement(c => c.Text(Strings.CcPaymentFailedMessage));
			app.Screenshot("Payment Failure Message");
		}

		[Test]
		public void MessageValidationForNoTransactionIdReceived()
		{
            TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4516");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			TestMethods.ConfirmDetailsAndMakePayment(app, "4844215500115643", "Anonymous", "1217", "123");

			app.WaitForElement(c => c.Text(Strings.CcPaymentFailedMessage));
			app.Screenshot("Payment Failure Message");
		}

		[Test]
		public void AmountVerificationForNoneDiscountedVRM()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4525");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
		}

        [Test]
		public void ValidationForPaymentConfirmationScreen()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4525");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.WaitForElement(c => c.Text(Strings.AllDone));
			app.WaitForElement(c => c.Text(Strings.ThanksPaymentCompletedSuccessfully));
			app.WaitForElement(c => c.Class("UIButtonLabel").Text(Strings.SaveReceipt));
			app.WaitForElement(c => c.Button(Strings.Home));
			app.WaitForElement(c => c.Text(Strings.YourReceipt));
			app.WaitForElement(c => c.Text(Strings.TypeColon));
			app.WaitForElement(c => c.Text(Strings.AmountPaid));
			app.WaitForElement(c => c.Text(Strings.PaymentDate));
			app.Screenshot("Payment Confirmation Screen");
		}

		[Test]
		public void PaymentConfirmationForaDay()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4525");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");
		}

        [Test]
        public void PaymentConfirmationForaMonth()
        {
            TestMethods.AcceptTermsAndCondition(app);
            TestMethods.NoLoginToApp(app);
            TestMethods.ChooseVehicleAndTapNext(app, "IAZ4525");
            TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.MonthlyCongestionCharge);
            TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

            app.WaitForElement(c => c.Text(Strings.Confirmation));
            app.Screenshot("Confirmation");
        }

		[Test]
		public void PaymentConfirmationForaWeek()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4525");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.WeeklyCongestionCharge);
			TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");
		}

		[Test]
		public void PaymentConfirmationForPreviousChargingDay()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4525");
            TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.PreviousChargingDay);
			TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "587");

			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");
		}

		[Test]
		public void VerificationOfReceiptButtonOnNotSavingReceipt()
		{
			ValidationForPaymentConfirmationScreen();

			app.Tap(c => c.Button(Strings.Home));
			app.Screenshot("No Receipts");
		}

		[Test]
		public void VerificationOfReceiptButtonOnSavingReceipt()
		{
			CongestionChargePaymentConfirmationScreenCheck();
		}

		/*	[Test]
			public void EnsureMaximum15VrmCanBeSaved()
			{
				TestMethods.AcceptTermsAndCondition(app);
				TestMethods.NoLoginToApp(app);
				TestMethods.ChooseVehicleAndTapNext(app, "IAZ4546");
				_app.WaitForElement(c => c.Marked(Strings.ChooseDates));
				_app.Tap(c => c.Marked(nameof(Resource.Id.checkBoxConfirmVrmIsCorrect)).Text(Strings.ConfirmThisVrmIsCorrect));
				_app.Tap(c => c.Marked(nameof(Resource.Id.chkSaveVehicle)).Text(Strings.SaveVehicleDetails));
				_app.Screenshot("Vehicle details");

				_app.Tap(c => c.Marked(nameof(Resource.Id.nextButton)).Text(Strings.ChooseDates));
				_app.WaitForElement(c => c.Marked(nameof(Resource.Id.ToolbarTitle)).Text(Strings.ChooseDates), "", TimeSpan.FromSeconds(15));

				_app.WaitForElement(c => c.Marked("Navigate up"), "", TimeSpan.FromSeconds(15));
				_app.Tap(c => c.Marked("Navigate up"));
				_app.WaitForElement(c => c.Marked("Navigate up"), "", TimeSpan.FromSeconds(15));
				_app.Tap(c => c.Marked("Navigate up"));
				_app.WaitForElement(c => c.Marked("Navigate up"), "", TimeSpan.FromSeconds(15));
				_app.Tap(c => c.Marked("Navigate up"));

				_app.WaitForElement(c => c.Marked(nameof(Resource.Id.buttonPayCC)), "", TimeSpan.FromSeconds(15));
				_app.Tap(c => c.Marked(nameof(Resource.Id.buttonPayCC)));
				Thread.Sleep(15000);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4562);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4594);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4594);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4603);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ47);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4708);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4717);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4727);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ474);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4747);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4761);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4767);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4775);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4776);
				EnterVrmAndSaveVehicle(TestCasesData.VRMIAZ4777);
				_app.ScrollUpTo(Strings.ChooseAVehicle, null, 0, 0.67, 500, true, new TimeSpan(0, 0, 15000));
				_app.Screenshot("Saved Vehicles 1");
				_app.ScrollDown();
				_app.Screenshot("Saved Vehicles 2");
				_app.ScrollDown();
				_app.Screenshot("Saved Vehicles 3");
				_app.ScrollDown();
				_app.Screenshot("Saved Vehicles 4");
				_app.ScrollDownTo(nameof(Resource.Id.btnAddNewVehicle), null, 0, 0.67, 500, true, new TimeSpan(0, 0, 15000));
				_app.Screenshot("Saved Vehicles 5");

			}*/

		[Test]
		public void EnsureSaveCardDetailsCheckBoxIsAvailable()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4951");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.PreviousChargingDay);

            app.Tap(c => c.Class("UIButtonLabel").Index(1));
			app.DismissKeyboard();
			app.WaitForElement(c => c.Text(Strings.SaveCardDetails));
			app.Screenshot("Save Card Details Checkbox");

		}

		[Test]
		public void MessageValidationForIncorrectCvvNumber()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4951");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			TestMethods.ConfirmDetailsAndMakePayment(app, "4543059790016721", "Anonymous", "1218", "555");

            app.WaitForElement(c => c.Text(Strings.PaymentFailedTitle));
			app.Screenshot("Payment Error");
		}

        [Test]
        public void CheckDisplayAndFunctionalityOfSaveReceiptButton()
        {
            CongestionChargePaymentConfirmationScreenCheck();
        }

		/*[Test]
		public void SaveCardDetailsCheckBoxFunctionality()
		{
			EnsureCardDetailsAreTokenized();
			_app.Back();
			_app.Back();

			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4546, TestCasesData.CN4543059790016721, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV587);
			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4562, TestCasesData.CN4715059999000437, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV124);
			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4594, TestCasesData.CN4844215500115643, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654);
			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4603, TestCasesData.CN5454609899026213, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV358);
			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4603, TestCasesData.CN5569500000002312, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV147);
			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4708, TestCasesData.CN5573489900000028, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV852);
			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4717, TestCasesData.CN5641820000000005, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV963);
			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4727, TestCasesData.CN4462030000000000, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV654);
			SavePaymentCardAndDisplay(TestCasesData.VRMIAZ474, TestCasesData.CN3569990010067584, TestCasesData.CE1218, TestCasesData.CUANONYMOUS, TestCasesData.CVV751);
			//SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4747, "", "1218", "Anonymous", "");
			//SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4761, "", "1218", "Anonymous", "");
			//SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4767, "", "1218", "Anonymous", "");
			//SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4775, "", "1218", "Anonymous", "");
			//SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4776, "", "1218", "Anonymous", "");
			//SavePaymentCardAndDisplay(TestCasesData.VRMIAZ4777, "", "1218", "Anonymous", "");
			// Need card details
		}*/

		[Test]
		public void EnsureSaveVehicleCheckBoxDisplayAndFunctionality()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4951");

            app.Tap(c => c.Class("UISwitchModernVisualElement").Index(0));
            app.Tap(c => c.Class("UISwitchModernVisualElement").Index(1));
			app.Screenshot("Vehicle Details");

			app.Tap(c => c.Button(Strings.ChooseDates));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.Screenshot("Pay For?");

			app.Tap(c => c.Class("UINavigationButton").Index(0));
			app.Tap(c => c.Class("UINavigationButton").Index(0));
            app.Tap(c => c.Class("UINavigationButton").Index(0));
            app.WaitForElement(c => c.Marked(Strings.PayCongestionCharge));
			app.Screenshot("Main Screen");

			app.Tap(c => c.Button(Strings.PayCongestionCharge));
			app.WaitForElement(c => c.Marked(Strings.ChooseAVehicle));
            app.Screenshot("Saved Vehicle");
		}

		[Test]
		public void EnsureDisplayOfSaveVehicleSectionOnAtleastOneSavedVrm()
		{
			EnsureSaveVehicleCheckBoxDisplayAndFunctionality();

            app.Tap(c => c.Class("UIButtonLabel"));
			app.WaitForElement(c => c.Marked(Strings.ChooseAVehicle));

			app.EnterText(c => c.Class("UITextField"), "IAZ4747");
			app.DismissKeyboard();
			app.Screenshot("Choose Vehicle");

			app.Tap(c => c.Button(Strings.Next));

			if (IosUITestHelper.IsElementAvailable(app, Strings.Warning))
			{
				app.Screenshot("Warning");
				app.Tap(c => c.Marked(Strings.Ok.ToUpper()));
			}

			app.Tap(c => c.Class("UISwitchModernVisualElement"));
			app.Screenshot("Vehicle Details");

			app.Tap(c => c.Button(Strings.ChooseDates));

			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.Screenshot("Pay For?");

			app.Tap(c => c.Class("UINavigationButton").Index(0));
			app.Tap(c => c.Class("UINavigationButton").Index(0));
			app.Tap(c => c.Class("UINavigationButton").Index(0));
            app.Screenshot("Main Screen");
		}

		[Test]
		public void SaveReceiptButtonAvailabilityAndFunctionality()
		{
			CongestionChargePaymentConfirmationScreenCheck();
		}

		[Test]
		public void SavePaymentCardDisplayAndFunctionality()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4951");
            TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);

			app.Tap(c => c.Class("UIButtonLabel").Index(1));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.EnterText(c => c.Class("UITextField").Index(0), "4543059790016721");
			app.EnterText(c => c.Class("UITextField").Index(1), "Anonymous");
			app.EnterText(c => c.Class("UITextField").Index(2), "1218");
			app.EnterText(c => c.Class("UITextField").Index(3), "587");
			app.DismissKeyboard();

            app.Tap(c => c.Class("UISwitchModernVisualElement").Index(0));
            app.Screenshot("Click Save Card CheckBox");

			app.Tap(c => c.Class("UIButtonLabel").Index(0));
			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");

            app.Tap(c => c.Button(Strings.Home));
            TestMethods.ChooseVehicleAndTapNext(app, "IAZ4747");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);

			app.Tap(c => c.Class("UIButtonLabel").Index(1));
            app.WaitForElement(c => c.Class("PageHeaderLabel").Text(Strings.SelectYourStoredCard));
            app.Screenshot("Saved Cards");

            app.Tap(c => c.Class("ListItemHeaderLabel"));
            app.WaitForElement(c => c.Text(Strings.SendMyReceiptViaSms));
            app.Screenshot("Enter CVV");

            app.Tap(c => c.Class("UIButtonLabel").Index(0));
            app.WaitForElement(c => c.Text(Strings.Cvv2Length));
            app.Screenshot("Error");

            app.Tap(c => c.Text(Strings.Ok.ToUpper()));
            app.EnterText(c => c.Class("UITextField"), "587");
            app.DismissKeyboard();
            app.Screenshot("Enter CVV");

            app.Tap(c => c.Class("UIButtonLabel").Index(0));
			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");
		}

		[Test]
		public void SaveMyReceiptViaSmsCheckboxDisplayAndFunctionality()
		{
            TestMethods.AcceptTermsAndCondition(app);
            TestMethods.NoLoginToApp(app);
            TestMethods.ChooseVehicleAndTapNext(app, "IAZ4951");
            TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			
            app.Tap(c => c.Class("UIButtonLabel").Index(1));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.EnterText(c => c.Class("UITextField").Index(0), "");
			app.EnterText(c => c.Class("UITextField").Index(1), "Anonymous");
			app.EnterText(c => c.Class("UITextField").Index(2), "1218");
			app.EnterText(c => c.Class("UITextField").Index(3), "587");
			app.DismissKeyboard();

            app.WaitForElement(c => c.Text(Strings.SendMyReceiptViaSms));
			app.Tap(c => c.Class("UISwitchModernVisualElement").Index(1));
			app.Screenshot("Click Save Card CheckBox");
			app.Tap(c => c.Text(Strings.SendMyReceiptViaSms));
		    app.WaitForElement(c => c.Text(Strings.PleaseEnterYourSmsNumber));
            app.Screenshot("Enter Your SMS Number");

			app.EnterText(c => c.Class("UITextField").Index(4), "07443861232");
			app.DismissKeyboard();

            app.Tap(c => c.Class("UIButtonLabel").Index(0));
			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");
		}

		Test]
        public void MessageValidationForInvalidNumberLessThan11Chars()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4951");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			app.Tap(c => c.Class("UIButtonLabel").Index(1));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.EnterText(c => c.Class("UITextField").Index(0), "");
			app.EnterText(c => c.Class("UITextField").Index(1), "Anonymous");
			app.EnterText(c => c.Class("UITextField").Index(2), "1218");
			app.EnterText(c => c.Class("UITextField").Index(3), "587");
			app.DismissKeyboard();

			app.WaitForElement(c => c.Text(Strings.SendMyReceiptViaSms));
			app.Tap(c => c.Class("UISwitchModernVisualElement").Index(1));
			app.Screenshot("Click Save Card CheckBox");
			app.Tap(c => c.Text(Strings.SendMyReceiptViaSms));
			app.WaitForElement(c => c.Text(Strings.PleaseEnterYourSmsNumber));
			app.Screenshot("Enter Your SMS Number");

			app.EnterText(c => c.Class("UITextField").Index(4), "0744386123");
			app.DismissKeyboard();
			
			app.Tap(c => c.Class("UIButtonLabel").Index(0));
			app.WaitForElement(c => c.Text(Strings.SmsNumberLength));
			app.Screenshot("Result : Mobile number validation");
		}

		[Test]
		public void MessageValidationForInvalidNumberDoesNotStartWithZero()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4951");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			app.Tap(c => c.Class("UIButtonLabel").Index(1));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.EnterText(c => c.Class("UITextField").Index(0), "");
			app.EnterText(c => c.Class("UITextField").Index(1), "Anonymous");
			app.EnterText(c => c.Class("UITextField").Index(2), "1218");
			app.EnterText(c => c.Class("UITextField").Index(3), "587");
			app.DismissKeyboard();

			app.WaitForElement(c => c.Text(Strings.SendMyReceiptViaSms));
			app.Tap(c => c.Class("UISwitchModernVisualElement").Index(1));
			app.Screenshot("Click Save Card CheckBox");
			app.Tap(c => c.Text(Strings.SendMyReceiptViaSms));
			app.WaitForElement(c => c.Text(Strings.PleaseEnterYourSmsNumber));
			app.Screenshot("Enter Your SMS Number");

			app.EnterText(c => c.Class("UITextField").Index(4), "7443861232");
			app.DismissKeyboard();
			
			app.Tap(c => c.Class("UIButtonLabel").Index(0));
			app.WaitForElement(c => c.Text(Strings.SmsNumberStartsWith));
			app.Screenshot("Result : Mobile number validation");
		}

		[Test]
		public void EnsureCardDetailsAreTokenized()
		{
			TestMethods.AcceptTermsAndCondition(app);
			TestMethods.NoLoginToApp(app);
			TestMethods.ChooseVehicleAndTapNext(app, "IAZ4951");
			TestMethods.ConfirmVrmAndTapSelectDates(app, Strings.DailyCongestionCharge);
			app.Tap(c => c.Class("UIButtonLabel").Index(1));
			app.WaitForElement(c => c.Class("PageHeaderLabel"));
			app.EnterText(c => c.Class("UITextField").Index(0), "");
			app.EnterText(c => c.Class("UITextField").Index(1), "Anonymous");
			app.EnterText(c => c.Class("UITextField").Index(2), "1218");
			app.EnterText(c => c.Class("UITextField").Index(3), "587");
			app.DismissKeyboard();

			app.Tap(c => c.Class("UISwitchModernVisualElement").Index(0));
			app.Tap(c => c.Class("UIButtonLabel").Index(0));

			app.WaitForElement(c => c.Text(Strings.Confirmation));
			app.Screenshot("Confirmation");

            app.Repl();
			app.Tap(c => c.Marked(Strings.YourVehiclesAndCards));
			app.WaitForElement(c => c.Marked(Strings.YourCreditCards));
			app.Screenshot(Strings.YourVehiclesAndCards);

			app.Tap(c => c.Marked(Strings.YourCreditCards));
			app.WaitForElement(c => c.Marked(Strings.YourCreditCards));
			app.Screenshot("Result : Stored cards");
		}

		/*[Test]
		public void ValidationForAlphanumericCharacterOfvehicleInput()
		{
			TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ_4501);
			TestMethods.ConfirmVrmAndTapChooseDate(_app);

			TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ___4501);
			TestMethods.ConfirmVrmAndTapChooseDate(_app);
		}

		[Test]
		public void EnsureVehicleValidationViaTheNextButton()
		{
			TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951);
			TestMethods.ConfirmVrmAndTapChooseDate(_app);
		}

		[Test]
		public void VehicleValidationViaTheNextButtonForIncorrectVrm()
		{
			TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951);
			TestMethods.ConfirmVrmAndTapChooseDate(_app);
		}

		[Test]
		public void YesterdayButtonAndMessageValidationForNonChargingDate()
		{
			TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951);
			TestMethods.ConfirmVrmAndTapChooseDate(_app);
			OpenChangeStartDateCalendar();

			var tomorrow = DateTime.Today.AddDays(1);
			var daysUntilSunday = ((int)DayOfWeek.Sunday - (int)tomorrow.DayOfWeek + 7) % 7;
			var nextSunday = tomorrow.AddDays(daysUntilSunday);

			_app.UpdateDatePicker(nextSunday);
			_app.Screenshot("Non Charging Date");

			_app.Tap(c => c.Marked("button1"));
			_app.WaitForElement(c => c.Marked(Strings.CcNonChargingDate));
			_app.Screenshot("Change Start Date To non-charging");

			_app.Tap(c => c.Marked(Strings.Ok));
			OpenChangeStartDateCalendar();

			var tomorrowTuesday = DateTime.Today.AddDays(1);
			var daysUntilMonday = ((int)DayOfWeek.Monday - (int)tomorrowTuesday.DayOfWeek + 7) % 7;
			var nextMonday = tomorrow.AddDays(daysUntilMonday);

			_app.UpdateDatePicker(nextMonday);
			_app.Screenshot("Date");

			_app.Tap(c => c.Marked("button1"));
			_app.ScrollUp();
			_app.WaitForNoElement(c => c.Marked(Strings.PreviousChargingDay));
			_app.Screenshot("No Yesterday");

		}

		[Test]
		public void VerifyVehicleDetailPageDisplay()
		{
			TestMethods.EnterVrmAndTapNext(_app, TestCasesData.VRMIAZ4951);

			_app.WaitForElement(c => c.Marked(Strings.PayCongestionCharge));
			_app.WaitForElement(c => c.Marked(Strings.VehicleRegistrationMark1));
			_app.WaitForElement(c => c.Marked(Strings.CarMake));
			_app.WaitForElement(c => c.Marked(Strings.ConfirmThisVrmIsCorrect));
			_app.WaitForElement(c => c.Marked(Strings.SaveVehicleDetails));
			_app.WaitForElement(c => c.Marked(Strings.ChooseDates));
			_app.Screenshot("Result : Vehicle details");
		}

		[Test]
		public void VerifyWhichVehicleScreenDisplay()
		{
			TestMethods.HomeScreenDisplay(_app);

			_app.Tap(c => c.Marked(nameof(Resource.Id.buttonPayCC)));
			_app.WaitForElement(c => c.Marked(Strings.PayCongestionCharge));
			_app.WaitForElement(c => c.Marked(Strings.ChooseAVehicle));
			_app.WaitForElement(c => c.Marked(Strings.IsVehicleRegisteredInUk));
			_app.WaitForElement(c => c.Marked(Strings.Next));
			_app.Screenshot("Result : " + Strings.ChooseAVehicle);
		}*/

	} 
}
