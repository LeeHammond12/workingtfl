﻿using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests
{
    [TestFixture]
    public class SerializerTest
    {
        [Test]
        public async void TestSerialization()
        {
            var testObject = new TestToSerialize() { MemberA = "Hello, World!", MemberB = 42 };

            var serializer = new Serializer();
            var result = await serializer.Serialize(testObject);
            var text = "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<TestToSerialize xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <MemberA>Hello, World!</MemberA>\r\n  <MemberB>42</MemberB>\r\n</TestToSerialize>";

            var expectedDeserialized = await serializer.DeSerialize<TestToSerialize>(text);
            var actualDeserialized = await serializer.DeSerialize<TestToSerialize>(result);

            Assert.AreEqual(expectedDeserialized.MemberA, actualDeserialized.MemberA);
            Assert.AreEqual(testObject.MemberA, actualDeserialized.MemberA);

            Assert.AreEqual(expectedDeserialized.MemberB, actualDeserialized.MemberB);
            Assert.AreEqual(testObject.MemberB, actualDeserialized.MemberB);
        }

        [Test]
        public async void TestDeserialization()
        {
            var serializer = new Serializer();
            var text = "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<TestToSerialize xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <MemberA>Hello, World!</MemberA>\r\n  <MemberB>42</MemberB>\r\n</TestToSerialize>";
            var result = await serializer.DeSerialize<TestToSerialize>(text);
            Assert.AreEqual("Hello, World!", result.MemberA);
            Assert.AreEqual(42, result.MemberB);
        }

        public class TestToSerialize
        {
            public string MemberA { get; set; }

            public int MemberB { get; set; }
        }
    }
}