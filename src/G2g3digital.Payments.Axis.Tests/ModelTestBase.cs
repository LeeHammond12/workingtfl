﻿using G2g3digital.Payments.Axis.Model;
using NSubstitute;

namespace G2g3digital.Payments.Axis.Tests
{
    public class ModelTestBase
    {
        protected IAxisConfiguration axisConfiguration;

        public virtual void SetUp()
        {
            this.axisConfiguration = Substitute.For<IAxisConfiguration>();
            this.axisConfiguration.HmacKey.Returns(
                "F8CIhOmVywNlWCNNK/XhBu2/iP+1N69MSb327+blbeX9BVMmdLFdw0qDVgpQ+54RM9c5/haUPyF+MsOSa/KN8Q==");
            this.axisConfiguration.HmacKeyId.Returns(1);
            this.axisConfiguration.Identifier.Returns(997);
            this.axisConfiguration.SubjectType.Returns(subjectType.CapitaSite);
        }
    }
}