﻿using System;
using G2g3digital.Payments.Axis.Model;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Model
{
    [TestFixture]
    public class CredentialsTests : ModelTestBase
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
        }

        [Test]
        public void PlainText_Matches()
        {
            credentials creds = new credentials(this.axisConfiguration, "121212121999", new DateTime(2011, 06, 02, 15, 20, 57, DateTimeKind.Utc));
            Assert.AreEqual("CapitaSite!997!121212121999!20110602152057!Original!1", creds.PlainText);
        }

        [Test]
        public void Digest_Calculation()
        {
            credentials creds = new credentials(this.axisConfiguration, "121212121999", new DateTime(2011, 06, 02, 15, 20, 57, DateTimeKind.Utc));
            Assert.AreEqual("Onpc2GlUiOXzG12ZB6uNygMl647pg2zl7qohoJr6OYg=", creds.signature.digest);
        }
    }
}