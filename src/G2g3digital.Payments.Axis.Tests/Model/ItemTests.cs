﻿using G2g3digital.Payments.Axis.Model;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Model
{
    [TestFixture]
    public class Item
    {
        [Test]
        public void Item_Default()
        {
            var payment = new item();
            Assert.AreEqual(0, payment.amountInMinorUnits);
        }

        [Test]
        public void Item_RoundNumber()
        {
            var payment = new item(string.Empty, string.Empty, 10.00m);
            Assert.AreEqual(1000, payment.amountInMinorUnits);
        }

        [Test]
        public void Item_Fractional()
        {
            var payment = new item(string.Empty, string.Empty, 12.34m);
            Assert.AreEqual(1234, payment.amountInMinorUnits);
        }

        [Test]
        public void Item_Penny_Fractions_Down()
        {
            var payment = new item(string.Empty, string.Empty, 12.343m);
            Assert.AreEqual(1234, payment.amountInMinorUnits);
        }

        [Test]
        public void Item_Penny_Fractions_Up()
        {
            // Should just truncate
            var payment = new item(string.Empty, string.Empty, 12.349m);
            Assert.AreEqual(1234, payment.amountInMinorUnits);
        }
    }
}