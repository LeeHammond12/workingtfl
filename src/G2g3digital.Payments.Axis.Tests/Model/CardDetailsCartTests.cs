﻿using G2g3digital.Payments.Axis.Model;
using NSubstitute;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Model
{
    [TestFixture]
    public class CardDetailsCartTests
    {
        private IAxisConfiguration config;

        [SetUp]
        public void SetUp()
        {
            this.config = Substitute.For<IAxisConfiguration>();
        }

        [Test]
        public void CardDetailsCart_Default()
        {
            // for coverage
            var payment = new cardDetailsCart();
            Assert.AreEqual(0, payment.amountInMinorUnits);
        }

        [Test]
        public void CardDetailsCart_RoundNumber()
        {
            var payment = new cardDetailsCart(10.00m, cardTranType.B2, new storedCardDetailsExisting("1111", "AAA"));
            Assert.AreEqual(1000, payment.amountInMinorUnits);
        }

        [Test]
        public void CardDetailsCart()
        {
            var payment = new cardDetailsCart(12.34m, cardTranType.B2, new storedCardDetailsExisting("1111", "AAA"));
            Assert.AreEqual(1234, payment.amountInMinorUnits);
        }

        [Test]
        public void CardDetailsCart_Penny_Fractions_Down()
        {
            var payment = new cardDetailsCart(12.343m, cardTranType.B2, new storedCardDetailsExisting("1111", "AAA"));
            Assert.AreEqual(1234, payment.amountInMinorUnits);
        }

        [Test]
        public void CardDetailsCart_Penny_Fractions_Up()
        {
            // Should just truncate
            var payment = new cardDetailsCart(12.349m, cardTranType.B2, new storedCardDetailsExisting("1111", "AAA"));
            Assert.AreEqual(1234, payment.amountInMinorUnits);
        }
    }
}