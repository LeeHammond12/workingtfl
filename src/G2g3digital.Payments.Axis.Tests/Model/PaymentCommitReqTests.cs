﻿using System;
using G2g3digital.Payments.Axis.Model;
using NSubstitute;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Model
{
    [TestFixture]
    public class PaymentCommitReqTests
    {
        private IAxisConfiguration config;

        private UniqueIdGenerator uniqueIdGenerator;

        [SetUp]
        public void SetUp()
        {
            this.config = Substitute.For<IAxisConfiguration>();

            this.uniqueIdGenerator = Substitute.For<UniqueIdGenerator>();
        }

        [Test]
        public void PaymentCommitReq_Total_One_Item()
        {
            var item = new item(string.Empty, string.Empty, 12.34m);
            var req = new paymentCommitReq(
                this.config,
                this.uniqueIdGenerator,
                DateTime.Now,
                string.Empty,
                DateTime.Now,
                new item[] { item });

            Assert.AreEqual(1234, req.payments.payment.paymentGeneral.amountInMinorUnits);
        }

        [Test]
        public void PaymentCommitReq_Total_Two_Item()
        {
            var item1 = new item(string.Empty, string.Empty, 12.34m);
            var item2 = new item(string.Empty, string.Empty, 56.78m);
            var req = new paymentCommitReq(
                this.config,
                this.uniqueIdGenerator,
                DateTime.Now,
                string.Empty,
                DateTime.Now,
                new item[] { item1, item2 });

            Assert.AreEqual(6912, req.payments.payment.paymentGeneral.amountInMinorUnits);
        }
    }
}