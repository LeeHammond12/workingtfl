﻿using System;
using System.Diagnostics;
using G2g3digital.Payments.Axis.Model;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Model
{
    [TestFixture]
    public class SourceDetailsTransactionTests : ModelTestBase
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
        }

        [Test]
        public void Check_transaction_date()
        {
            var date = new DateTime(2010, 01, 02, 03, 04, 05, DateTimeKind.Utc);
            date = date.AddMilliseconds(234);
            sourceDetailsTransaction sdt = new sourceDetailsTransaction(this.axisConfiguration, "AAA", date);
            Debug.WriteLine(sdt.transactionDate.ToString("o"));
            Assert.AreEqual(2010, sdt.transactionDate.Year);
            Assert.AreEqual(1, sdt.transactionDate.Month);
            Assert.AreEqual(2, sdt.transactionDate.Day);
            Assert.AreEqual(3, sdt.transactionDate.Hour);
            Assert.AreEqual(4, sdt.transactionDate.Minute);
            Assert.AreEqual(5, sdt.transactionDate.Second);
            Assert.AreEqual(0, sdt.transactionDate.Millisecond);
        }
    }
}