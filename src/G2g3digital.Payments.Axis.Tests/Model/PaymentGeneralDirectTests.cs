﻿using G2g3digital.Payments.Axis.Model;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Model
{
    [TestFixture]
    public class PaymentGeneralDirectTests
    {
        [Test]
        public void PaymentGeneralDirect_Default()
        {
            var payment = new paymentGeneralDirect();
            Assert.AreEqual(null, payment.amountInMinorUnits);
        }

        [Test]
        public void PaymentGeneralDirect_RoundNumber()
        {
            var payment = new paymentGeneralDirect(10.00m, cardTranType.B2);
            Assert.AreEqual("1000", payment.amountInMinorUnits);
        }

        [Test]
        public void PaymentGeneralDirect()
        {
            var payment = new paymentGeneralDirect(12.34m, cardTranType.B2);
            Assert.AreEqual("1234", payment.amountInMinorUnits);
        }

        [Test]
        public void PaymentGeneralDirect_Penny_Fractions_Down()
        {
            var payment = new paymentGeneralDirect(12.343m, cardTranType.B2);
            Assert.AreEqual("1234", payment.amountInMinorUnits);
        }

        [Test]
        public void PaymentGeneralDirect_Penny_Fractions_Up()
        {
            // Should just truncate
            var payment = new paymentGeneralDirect(12.349m, cardTranType.B2);
            Assert.AreEqual("1234", payment.amountInMinorUnits);
        }
    }
}