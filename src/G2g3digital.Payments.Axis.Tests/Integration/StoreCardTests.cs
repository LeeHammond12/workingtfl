﻿using System;
using System.Diagnostics;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Integration
{
    [Ignore("Used as a harness for dev - This test should work but ignored as is an integration test that sends a network message")]
    [TestFixture]
    [Category("Integration")]
    public class StoreCardTests
    {
        [Test]
        public async void Test_store_card_returns_success_code()
        {
            IAxisPaymentService service = new AxisPaymentService(new AxisConfiguration());

            var storeResponse = await service.StoreCardAsync(
                DateTime.Now,
                "4444333322221111",
                DateTime.Now.AddYears(1).ToString("MMyy"),
                "Joe Bloggs");

            if (storeResponse.header.statusCode != 0)
            {
                Debug.WriteLine("Response:");
                Debug.WriteLine(storeResponse.header.statusCodeDescription);
            }

            Assert.AreEqual(0, storeResponse.header.statusCode);

            var deleteResponse = await service.StoreCardDeleteAsync(
                DateTime.Now,
                storeResponse.LastFourDigits,
                storeResponse.TemplateNo);

            Assert.AreEqual(0, deleteResponse.header.statusCode);
        }
    }
}