﻿using G2g3digital.Payments.Axis.Model;

namespace G2g3digital.Payments.Axis.Tests.Integration
{
    public class AxisConfiguration : IAxisConfiguration
    {
        public subjectType SubjectType => subjectType.CapitaSite;

        public int Identifier => 453;

        public int HmacKeyId => 1;

        public string HmacKey => "rncZpPVoECyw3ZVBKC9CORalmb6XjE9EEbQjTH+3TZKryiFM7kAfEs0SUJly7SCuGQn2G3pNHWkpAh5GWLffoA==";

        public ushort SiteId => 453;

        public systemCode SystemCode => systemCode.API;

        public string PaymentUri => "https://sbsctest.e-paycapita.com/CommonPayments/CommonPayments.asmx";
    }
}