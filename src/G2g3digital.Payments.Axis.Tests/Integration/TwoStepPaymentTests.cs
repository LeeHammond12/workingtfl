﻿using System;
using System.Diagnostics;
using G2g3digital.Payments.Axis.Model;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Integration
{
    [Ignore("Used as a harness for dev - This test should work but ignored as is an integration test that sends a network message")]
    [TestFixture]
    public class TwoStepPaymentTests
    {
        [Test]
        public async void PaymentAuthorizeReq_StoredCard_Authorize()
        {
            IAxisPaymentService service = new AxisPaymentService(new AxisConfiguration());

            var storedCardResponse =
                await
                service.StoreCardAsync(DateTime.Now, "4444333322221111", DateTime.Now.AddYears(1).ToString("MMyy"), "Joe Bloggs");

            Assert.AreEqual(0, storedCardResponse.header.statusCode);

            var authTransactionDate = DateTime.Now;
            var payAuthResponse =
                await
                service.PaymentAuthorizeAsync(
                    authTransactionDate,
                    10.00m,
                    cardTranType.B2,
                    storedCardResponse.LastFourDigits,
                    "123",
                    storedCardResponse.TemplateNo,
                    "MOB1");

            if (payAuthResponse.header.statusCode != 0)
            {
                Debug.WriteLine("Response:");
                Debug.WriteLine(payAuthResponse.header.statusCodeDescription);
            }

            Assert.AreEqual(0, payAuthResponse.header.statusCode);
        }

        [Test]
        public async void PaymentAuthorizeReq_StoredCard_Authorize_And_Commit()
        {
            IAxisPaymentService service = new AxisPaymentService(new AxisConfiguration());

            var storedCardResponse = await service.StoreCardAsync(
                DateTime.Now,
                "4444333322221111",
                DateTime.Now.AddYears(1).ToString("MMyy"),
                "Joe Bloggs");

            Assert.AreEqual(0, storedCardResponse.header.statusCode);

            var authTransactionDate = DateTime.Now;
            var payAuthResponse = await service.PaymentAuthorizeAsync(
                authTransactionDate,
                10.00m,
                cardTranType.B2,
                storedCardResponse.LastFourDigits,
                "123",
                storedCardResponse.TemplateNo,
                "MOB1");

            if (payAuthResponse.header.statusCode != 0)
            {
                Debug.WriteLine("Response:");
                Debug.WriteLine(payAuthResponse.header.statusCodeDescription);
            }

            Assert.AreEqual(0, payAuthResponse.header.statusCode);

            var payCommitResponse =
                await
                service.PaymentCommitAsync(
                    DateTime.Now,
                    payAuthResponse.header.uniqueTranID,
                    authTransactionDate,
                    "MOB01",
                    "12345678",
                    10.00m);

            if (payCommitResponse.header.statusCode != 0)
            {
                Debug.WriteLine("Response:");
                Debug.WriteLine(payCommitResponse.header.statusCodeDescription);
            }

            Assert.AreEqual(0, payCommitResponse.header.statusCode);
        }
    }
}