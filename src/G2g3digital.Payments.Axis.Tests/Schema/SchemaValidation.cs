﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using G2g3digital.Payments.Axis.Model;
using NSubstitute;
using NUnit.Framework;

namespace G2g3digital.Payments.Axis.Tests.Schema
{
    [TestFixture]
    public class SchemaValidation : ModelTestBase
    {
        private Serializer serializer;

        private UniqueIdGenerator uniqueIdGenerator;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            this.serializer = new Serializer();
            this.uniqueIdGenerator = Substitute.For<UniqueIdGenerator>();
            this.uniqueIdGenerator.GenerateUniqueId().Returns("123456789012");
        }

        [Test]
        public async void StoreCardReqTest()
        {
            var expiry = new DateTime(2011, 01, 01).ToString("MMyy");
            var request = new storeCardReq(
                this.axisConfiguration,
                this.uniqueIdGenerator,
                new DateTime(2010, 01, 01),
                "1111222233334444",
                expiry,
                "Joe Bloggs");
            var xml = await this.serializer.Serialize(request);
            this.Validate(xml);
        }

        [Test]
        public async void DirectAuthStoredCardReqTest()
        {
            var request = new directAuthorizeStoredCardReq(
                this.axisConfiguration,
                this.uniqueIdGenerator,
                new DateTime(2010, 01, 01),
                10.00m,
                cardTranType.B2,
                "1111",
                "+eGIwxJkCk6gtLHjmDxQ5w==");
            var xml = await this.serializer.Serialize(request);
            this.Validate(xml);
        }

        [Test]
        public async void PaymentAuthorizeReqTest()
        {
            var request = new paymentAuthorizeReq(
                this.axisConfiguration,
                this.uniqueIdGenerator,
                new DateTime(2010, 01, 01),
                10.00m,
                cardTranType.B2,
                "1111",
                "123",
                "+eGIwxJkCk6gtLHjmDxQ5w==",
                "1234");
            var xml = await this.serializer.Serialize(request);
            this.Validate(xml);
        }

        [Test]
        public async void PaymentCommitReqTest()
        {
            var request = new paymentCommitReq(
                this.axisConfiguration,
                this.uniqueIdGenerator,
                new DateTime(2010, 01, 01),
                "AAAA11112222",
                new DateTime(2010, 01, 01),
                new item[] { new item("fund", "fakeReference", 10.00m) });
            var xml = await this.serializer.Serialize(request);
            this.Validate(xml);
        }

        private XmlSchema LoadSchema(string schemaResourceName)
        {
            var assembly = Assembly.GetAssembly(typeof(Serializer));
            using (
                var stream = assembly.GetManifestResourceStream(schemaResourceName))
            {
                Assert.IsNotNull(stream);
                return XmlSchema.Read(stream, null);
            }
        }

        private void Validate(string xml)
        {
            using (var reader = new StringReader(xml))
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(reader);
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.paymentCommitReq.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.paymentAuthorizeReq.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.directAuthorizeStoredCardReq.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.storeCardReq.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.CommonFoundationTypes.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.commonPaymentsCartTypes.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.commonPaymentsGeneralTypes.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.commonPaymentsHeaderTypes.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.commonPaymentsPaymentTypes.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.commonPaymentsSimpleTypes.xsd"));
                doc.Schemas.Add(this.LoadSchema("G2g3digital.Payments.Axis.Model.Xsd.commonPaymentsStoredCardTypes.xsd"));
                bool success = true;
                doc.Validate((sender, args) =>
                {
                    Debug.WriteLine(args.Message);
                    success = false;
                });
                Assert.IsTrue(success);
            }
        }
    }
}