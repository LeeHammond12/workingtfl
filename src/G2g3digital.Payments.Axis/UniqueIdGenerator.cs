﻿using System;

namespace G2g3digital.Payments.Axis
{
    public class UniqueIdGenerator
    {
        private const string ValidChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private const int Length = 12;

        private static readonly Random Random = new Random();

        /// <summary>
        /// Generate a 12 character unique ID.
        /// </summary>
        /// <returns>A unique Id.</returns>
        public virtual string GenerateUniqueId()
        {
            lock (Random)
            {
                char[] chars = new char[Length];
                for (int i = 0; i < Length; i++)
                {
                    chars[i] = ValidChars[Random.Next(ValidChars.Length)];
                }
                return new string(chars);
            }
        }
    }
}