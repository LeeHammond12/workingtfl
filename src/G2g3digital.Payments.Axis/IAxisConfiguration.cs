﻿using G2g3digital.Payments.Axis.Model;

namespace G2g3digital.Payments.Axis
{
    public interface IAxisConfiguration
    {
        subjectType SubjectType { get; }

        int Identifier { get; }

        int HmacKeyId { get; }

        string HmacKey { get; }

        ushort SiteId { get; }

        systemCode SystemCode { get; }

        string PaymentUri { get; }
    }
}