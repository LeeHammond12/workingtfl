﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using G2g3digital.Payments.Axis.Model;
using G2g3digital.Payments.Axis.Service;

namespace G2g3digital.Payments.Axis
{
    /// <summary>
    /// Deals with the wrapping and unwrapping of the XML body of the SOAP requests.
    /// </summary>
    public class AxisCommonPaymentsWrapper
    {
        private readonly Serializer serializer;
        private readonly AXISCommonPaymentsSoap soap;

        public AxisCommonPaymentsWrapper(string paymentUri) :
            this(new EndpointAddress(paymentUri))
        {
        }

        public AxisCommonPaymentsWrapper(EndpointAddress endpointAddress)
        {
            this.serializer = new Serializer();

            BasicHttpBinding binding = new BasicHttpBinding
            {
                MaxBufferSize = int.MaxValue,
                MaxReceivedMessageSize = int.MaxValue
            };
            binding.Security.Mode = BasicHttpSecurityMode.Transport;

            this.soap = new AXISCommonPaymentsSoapClient(binding, endpointAddress);
        }

        public async Task<storeCardRes> StoreCardAsync(storeCardReq request)
        {
            var requestBody = await this.serializer.Serialize(request);
            var storeCardRequest = new StoreCardRequest { Body = new StoreCardRequestBody(requestBody) };
            var retval = await this.FromAsync(this.soap.BeginStoreCard, this.soap.EndStoreCard, storeCardRequest);
            var entity = await this.serializer.DeSerialize<storeCardRes>(retval.Body.StoreCardResult);
            return entity;
        }

        public async Task<storeCardRes> StoreCardDeleteAsync(storedCardDeleteReq request)
        {
            var requestBody = await this.serializer.Serialize(request);
            var storeCardRequest = new StoredCardDeleteRequest() { Body = new StoredCardDeleteRequestBody(requestBody) };
            var retval = await this.FromAsync(this.soap.BeginStoredCardDelete, this.soap.EndStoredCardDelete, storeCardRequest);
            var entity = await this.serializer.DeSerialize<storeCardRes>(retval.Body.StoredCardDeleteResult);
            return entity;
        }

        public async Task<paymentRes> PaymentAuthorizeAsync(paymentAuthorizeReq request)
        {
            var requestBody = await this.serializer.Serialize(request);
            var paymentAuthorizeRequest = new PaymentAuthorizeRequest() { Body = new PaymentAuthorizeRequestBody(requestBody) };
            var retval = await this.FromAsync(this.soap.BeginPaymentAuthorize, this.soap.EndPaymentAuthorize, paymentAuthorizeRequest);
            var entity = await this.serializer.DeSerialize<paymentRes>(retval.Body.PaymentAuthorizeResult);
            return entity;
        }

        public async Task<paymentRes> PaymentCommitAsync(paymentCommitReq request)
        {
            var requestBody = await this.serializer.Serialize(request);
            var paymentCommitRequest = new PaymentCommitRequest() { Body = new PaymentCommitRequestBody(requestBody) };
            var retval = await this.FromAsync(this.soap.BeginPaymentCommit, this.soap.EndPaymentCommit, paymentCommitRequest);
            var entity = await this.serializer.DeSerialize<paymentRes>(retval.Body.PaymentCommitResult);
            return entity;
        }

        private Task<Out> FromAsync<In, Out>(
            Func<In, AsyncCallback, object, IAsyncResult> beginMethod,
            Func<IAsyncResult, Out> endMethod,
            In request)
        {
            return Task.Factory.FromAsync<In, Out>(
                        beginMethod,
                        endMethod,
                        request,
                        new object(),
                        TaskCreationOptions.None);
        }
    }
}