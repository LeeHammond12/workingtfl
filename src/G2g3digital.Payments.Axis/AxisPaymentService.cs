﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using G2g3digital.Payments.Axis.Model;

namespace G2g3digital.Payments.Axis
{
    public class AxisPaymentService : IAxisPaymentService
    {
        public const string SchemaVersion = "All";

        private readonly UniqueIdGenerator uniqueIdGenerator;

        private readonly IAxisConfiguration config;

        private readonly AxisCommonPaymentsWrapper wrapper;

        public AxisPaymentService(IAxisConfiguration config)
        {
            this.uniqueIdGenerator = new UniqueIdGenerator();
            this.config = config;
            this.wrapper = new AxisCommonPaymentsWrapper(config.PaymentUri);
        }

        public async Task<storeCardRes> StoreCardAsync(DateTime transactionDate, string cardNumber, string expiryDate, string cardHolderName)
        {
            var request = new storeCardReq(this.config, this.uniqueIdGenerator, transactionDate, cardNumber, expiryDate, cardHolderName);
            return await this.wrapper.StoreCardAsync(request).ConfigureAwait(false);
        }

        public async Task<storeCardRes> StoreCardDeleteAsync(DateTime transactionDate, string lastFourDigits, string templateNo)
        {
            var request = new storedCardDeleteReq(this.config, this.uniqueIdGenerator, transactionDate, lastFourDigits, templateNo);
            return await this.wrapper.StoreCardDeleteAsync(request).ConfigureAwait(false);
        }

        public async Task<paymentRes> PaymentAuthorizeAsync(DateTime transactionDate, decimal amount, cardTranType cardTranType, string lastFourDigits, string cardSecurityCode, string templateNo, string merchantCode)
        {
            var payRequest = new paymentAuthorizeReq(this.config, this.uniqueIdGenerator, transactionDate, amount, cardTranType, lastFourDigits, cardSecurityCode, templateNo, merchantCode);
            return await this.wrapper.PaymentAuthorizeAsync(payRequest).ConfigureAwait(false);
        }

        public async Task<paymentRes> PaymentAuthorizeAsync(DateTime transactionDate, decimal amount, cardTranType cardTranType, string cardHolderName, string cardNumber, string cardSecurityCode, string expiryDate, string merchantCode)
        {
            var payRequest = new paymentAuthorizeReq(this.config, this.uniqueIdGenerator, transactionDate, amount, cardTranType, cardHolderName, cardNumber, cardSecurityCode, expiryDate, merchantCode);
            return await this.wrapper.PaymentAuthorizeAsync(payRequest).ConfigureAwait(false);
        }

        public async Task<paymentRes> PaymentCommitAsync(DateTime transactionDate, string authTranId, DateTime authTransactionDate, ICollection<item> items)
        {
            var payRequest = new paymentCommitReq(this.config, this.uniqueIdGenerator, transactionDate, authTranId, authTransactionDate, items);
            return await this.wrapper.PaymentCommitAsync(payRequest).ConfigureAwait(false);
        }

        public async Task<paymentRes> PaymentCommitAsync(DateTime transactionDate, string authTranId, DateTime authTransactionDate, string fundCode, string reference, decimal amount)
        {
            var item = new item(fundCode, reference, amount);
            return await this.PaymentCommitAsync(transactionDate, authTranId, authTransactionDate, new item[] { item }).ConfigureAwait(false);
        }
    }
}