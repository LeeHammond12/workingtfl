﻿using System;

namespace G2g3digital.Payments.Axis
{
    internal static class Extensions
    {
        /// <remarks>
        /// CPS does not like the XML default date time serialization;
        /// must remove not only milliseconds but also microseconds which
        /// allows this to be serialized correctly without modifying the
        /// code generated for the WCF proxy.
        /// </remarks>
        public static DateTime ToCpsDateTime(this DateTime dateTime)
        {
            // CPS does not like the XML default date time serialization;
            // must remove not only milliseconds but also microseconds which
            // allows this to be serialized correctly without modifying the
            // code generated for the WCF proxy.
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                dateTime.Hour,
                dateTime.Minute,
                dateTime.Second);
        }
    }
}