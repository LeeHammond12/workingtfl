﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace G2g3digital.Payments.Axis
{
    public class Serializer
    {
        private TaskFactory taskFactory = new TaskFactory();

        /// <summary>
        /// Serialize an instance of T into a string.
        /// </summary>
        /// <typeparam name="T">The type to serialize.</typeparam>
        /// <param name="instance">The instance of type T to serialize.</param>
        /// <returns>Awaitable for a string.</returns>
        public Task<string> Serialize<T>(T instance)
        {
            return this.taskFactory.StartNew(
                () =>
                {
                    var serializer = new XmlSerializer(typeof(T));
                    try
                    {
                        using (var writer = new StringWriter())
                        {
                            serializer.Serialize(writer, instance);
                            return writer.ToString();
                        }
                    }
                    catch (Exception)
                    {
                        return string.Empty;
                    }
                });
        }

        /// <summary>
        /// Serialize a string to a class of type T.
        /// </summary>
        /// <typeparam name="T">Type of class to serialize to.</typeparam>
        /// <param name="xml">XML to deserialize.</param>
        /// <returns>Awaitable for type T</returns>
        public Task<T> DeSerialize<T>(string xml)
        {
            return this.taskFactory.StartNew(
                () =>
                {
                    try
                    {
                        var serializer = new XmlSerializer(typeof(T));
                        using (var reader = new StringReader(xml))
                        {
                            return (T)serializer.Deserialize(reader);
                        }
                    }
                    catch (Exception)
                    {
                        return default(T);
                    }
                });
        }
    }
}