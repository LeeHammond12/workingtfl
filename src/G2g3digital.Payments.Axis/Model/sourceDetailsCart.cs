﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class sourceDetailsCart
    {
        public sourceDetailsCart()
        {
        }

        public sourceDetailsCart(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate, string merchantCode)
            : base(configuration, uniqueTranId, transactionDate)
        {
            this.systemCode = systemCode1.EXT;
            this.merchantCode = merchantCode;
        }

        public sourceDetailsCart(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate)
        : base(configuration, uniqueTranId, transactionDate)
        {
            this.systemCode = systemCode1.EXT;
        }
    }
}