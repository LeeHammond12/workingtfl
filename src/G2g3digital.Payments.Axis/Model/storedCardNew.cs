﻿namespace G2g3digital.Payments.Axis.Model
{
    public partial class storedCardNew
    {
        public storedCardNew()
        {
        }

        public storedCardNew(string cardNumber, string expiryDate, string cardHolderName)
        {
            this.storedCardDetails = new storedCardDetailsNew(cardNumber, expiryDate, cardHolderName);
        }
    }
}