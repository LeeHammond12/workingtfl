﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class directAuthorizeStoredCardReq
    {
        public directAuthorizeStoredCardReq()
        {
        }

        public directAuthorizeStoredCardReq(IAxisConfiguration configuration, UniqueIdGenerator uniqueIdGenerator, DateTime transactionDate, decimal amount, cardTranType cardTranType, string lastFourDigits, string templateNo)
        {
            var uniqueId = uniqueIdGenerator.GenerateUniqueId();
            this.header = new headerDirect(configuration, uniqueId, transactionDate);
            this.credentials = new credentials(configuration, uniqueId, transactionDate);
            this.schemaVersion = AxisPaymentService.SchemaVersion;

            this.payments = new paymentsDirectAuthorizeStoredCard();
            this.payments.payment = new paymentsDirectAuthorizeStoredCardPayment();

            this.payments.payment.paymentGeneral = new paymentGeneralDirect(amount, cardTranType);

            this.payments.payment.storedCardDetails =
                new storedCardDetailsExisting(
                    lastFourDigits,
                    templateNo);
        }
    }
}