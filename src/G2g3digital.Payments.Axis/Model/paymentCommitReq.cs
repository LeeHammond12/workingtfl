﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class paymentCommitReq
    {
        public paymentCommitReq()
        {
        }

        public paymentCommitReq(IAxisConfiguration configuration, UniqueIdGenerator uniqueIdGenerator, DateTime transactionDate, string authTranId, DateTime authTransactionDate, ICollection<item> items)
            : this()
        {
            var uniqueId = uniqueIdGenerator.GenerateUniqueId();
            this.header = new headerCart(configuration, authTranId, transactionDate);
            this.credentials = new credentials(configuration, uniqueId, transactionDate);
            this.schemaVersion = AxisPaymentService.SchemaVersion;

            this.invoice = new invoice { items = items.ToArray() };

            this.payments = new paymentsCartCommit
            {
                payment = new paymentsCartCommitPayment
                {
                    paymentGeneral = new paymentGeneralCart
                    {
                        amountInMinorUnits = items.Sum(i => i.amountInMinorUnits)
                    }
                }
            };

            this.payments.payment.originalAuthorization = new generalKey
            {
                machineCode = this.header.sourceDetails.machineCode,
                ItemElementName = ItemChoiceType1.uniqueTranID,
                Item = authTranId,
                transactionDate = authTransactionDate.ToCpsDateTime()
            };
        }
    }
}