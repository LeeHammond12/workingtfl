﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class storeCardReq
    {
        public storeCardReq()
        {
        }

        public storeCardReq(IAxisConfiguration configuration, UniqueIdGenerator uniqueIdGenerator, DateTime transactionDate, string cardNumber, string expiryDate, string cardHolderName)
        {
            var uniqueId = uniqueIdGenerator.GenerateUniqueId();
            this.header = new headerStoredCard(configuration, uniqueId, transactionDate);
            this.credentials = new credentials(configuration, uniqueId, transactionDate);
            this.schemaVersion = AxisPaymentService.SchemaVersion;
            this.storedCard = new storedCardNew(cardNumber, expiryDate, cardHolderName);
        }
    }
}