﻿namespace G2g3digital.Payments.Axis.Model
{
    public partial class item
    {
        public item()
        {
        }

        public item(string fundCode, string reference, decimal amount)
        {
            this.fundCode = fundCode;
            this.reference = reference;
            this.amountInMinorUnits = (int)(amount * 100);
        }
    }
}