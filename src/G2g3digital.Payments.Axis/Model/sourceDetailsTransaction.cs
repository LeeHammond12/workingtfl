﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class sourceDetailsTransaction
    {
        public sourceDetailsTransaction()
        {
        }

        public sourceDetailsTransaction(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate) : this()
        {
            this.siteID = configuration.SiteId;
            this.siteIDSpecified = true;

            // CPS recommend this is padded to five characters.
            this.machineCode = configuration.SiteId.ToString().PadLeft(5, '0');
            this.uniqueTranID = uniqueTranId;

            this.transactionDate = transactionDate.ToCpsDateTime();
            this.transactionDateSpecified = true;
        }
    }
}