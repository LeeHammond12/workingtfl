﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class credentials
    {
        public credentials()
        {
        }

        public credentials(IAxisConfiguration configuration, string uniqueReference, DateTime timestamp) : this()
        {
            this.subject = new subject();
            this.subject.subjectType = configuration.SubjectType;
            this.subject.identifier = configuration.Identifier;
            this.subject.systemCode = configuration.SystemCode;
            this.subject.systemCodeSpecified = true;

            this.requestIdentification = new requestIdentification();
            this.requestIdentification.uniqueReference = uniqueReference;
            this.requestIdentification.timeStamp = this.FormatTimestamp(timestamp);

            this.signature = new signature();
            this.signature.algorithm = algorithm.Original;
            this.signature.hmacKeyID = configuration.HmacKeyId;
            this.signature.digest = this.GenerateDigest(configuration);
        }

        [XmlIgnore]
        public string PlainText
        {
            get
            {
                return this.subject.subjectType
                            + "!" + this.subject.identifier
                            + "!" + this.requestIdentification.uniqueReference
                            + "!" + this.requestIdentification.timeStamp
                            + "!" + this.signature.algorithm
                            + "!" + this.signature.hmacKeyID;
            }
        }

        public bool ShouldSerializedevice()
        {
            return this.device != null;
        }

        /// <summary>
        /// Calculate string timestamp according to Axis docs.
        /// </summary>
        /// <param name="timestamp">Datetime in utc time.</param>
        /// <returns>String representation of datetime.</returns>
        /// <remarks>This function taken from the spec doc page 39.</remarks>
        private string FormatTimestamp(DateTime timestamp)
        {
            var utcCurrentTime = timestamp.ToUniversalTime();
            string result = utcCurrentTime.Year
                + utcCurrentTime.Month.ToString("00")
                + utcCurrentTime.Day.ToString("00")
                + utcCurrentTime.Hour.ToString("00")
                + utcCurrentTime.Minute.ToString("00")
                + utcCurrentTime.Second.ToString("00");
            return result;
        }

        private string GenerateDigest(IAxisConfiguration configuration)
        {
            var key = Convert.FromBase64String(configuration.HmacKey);
            var plaintextBytes = Encoding.UTF8.GetBytes(this.PlainText);
            var hmacSha256 = new HMACSHA256(key);
            var hash = hmacSha256.ComputeHash(plaintextBytes);
            return Convert.ToBase64String(hash);
        }
    }
}