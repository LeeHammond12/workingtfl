﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class sourceDetailsDirect
    {
        public sourceDetailsDirect()
        {
        }

        public sourceDetailsDirect(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate)
            : base(configuration, uniqueTranId, transactionDate)
        {
            this.systemCode = systemCode1.DIR;
        }
    }
}