﻿namespace G2g3digital.Payments.Axis.Model
{
    public partial class storeCardRes
    {
        public string LastFourDigits
        {
            get
            {
                return this.storedCard.storedCardDetails.cardLastFourDigits;
            }
        }

        public string TemplateNo
        {
            get
            {
                return this.storedCard.storedCardDetails.storedCardTemplateNo;
            }
        }
    }
}