﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class headerStoredCard
    {
        public headerStoredCard()
        {
        }

        public headerStoredCard(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate) : this()
        {
            this.sourceDetails = new sourceDetailsStoredCard(configuration, uniqueTranId, transactionDate);
        }
    }
}