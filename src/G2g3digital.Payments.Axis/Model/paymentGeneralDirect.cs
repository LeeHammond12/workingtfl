﻿namespace G2g3digital.Payments.Axis.Model
{
    public partial class paymentGeneralDirect
    {
        public paymentGeneralDirect()
        {
        }

        public paymentGeneralDirect(decimal amount, cardTranType cardTranType)
        {
            this.amountInMinorUnits = ((int)(amount * 100)).ToString();
            this.cardTranType = cardTranType;
        }
    }
}