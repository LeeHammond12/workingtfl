﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class storedCardDeleteReq
    {
        public storedCardDeleteReq()
        {
        }

        public storedCardDeleteReq(IAxisConfiguration configuration, UniqueIdGenerator uniqueIdGenerator, DateTime transactionDate, string lastFourDigits, string templateNo)
        {
            var uniqueId = uniqueIdGenerator.GenerateUniqueId();
            this.header = new headerStoredCard(configuration, uniqueId, transactionDate);
            this.credentials = new credentials(configuration, uniqueId, transactionDate);
            this.schemaVersion = AxisPaymentService.SchemaVersion;
            this.storedCard = new storedCardExisting();
            this.storedCard.storedCardDetails = new storedCardDetailsExisting(lastFourDigits, templateNo);
        }
    }
}