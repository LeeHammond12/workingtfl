﻿namespace G2g3digital.Payments.Axis.Model
{
    public partial class cardDetailsCart
    {
        public cardDetailsCart()
        {
        }

        public cardDetailsCart(decimal amount, cardTranType cardTranType, storedCardDetailsExisting storedCard)
            : this(amount, cardTranType)
        {
            this.ItemElementName = ItemChoiceType.storedCardDetails;
            this.Item = storedCard;
        }

        public cardDetailsCart(decimal amount, cardTranType cardTranType, string cardHolderName, string cardNumber, string expiryDate)
            : this(amount, cardTranType)
        {
            this.ItemElementName = ItemChoiceType.cardNumber;
            this.Item = cardNumber;
            this.cardHolderName = cardHolderName;
            this.expiryDate = expiryDate;
        }

        public cardDetailsCart(decimal amount, cardTranType cardTranType, string cardHolderName, string cardNumber, string expiryDate, string issueNumber)
            : this(amount, cardTranType, cardHolderName, cardNumber, expiryDate)
        {
            this.issueNumber = issueNumber;
        }

        private cardDetailsCart(decimal amount, cardTranType cardTranType) : this()
        {
            this.Amount = amount;
            this.cardTranType = cardTranType;
        }

        private decimal Amount
        {
            get
            {
                return ((decimal)this.amountInMinorUnits) / 100;
            }

            set
            {
                this.amountInMinorUnits = (int)(value * 100);
            }
        }
    }
}