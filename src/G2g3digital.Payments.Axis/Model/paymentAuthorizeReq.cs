﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class paymentAuthorizeReq
    {
        public paymentAuthorizeReq()
        {
        }

        public paymentAuthorizeReq(
            IAxisConfiguration configuration,
            UniqueIdGenerator uniqueIdGenerator,
            DateTime transactionDate,
            decimal amount,
            cardTranType cardTranType,
            string lastFourDigits,
            string cardSecurityCode,
            string templateNo,
            string merchantCode)
            : this(configuration, uniqueIdGenerator, transactionDate, merchantCode)
        {
            var storedCard = new storedCardDetailsExisting(lastFourDigits, templateNo);
            this.payments.payment.cardDetails = new cardDetailsCart(amount, cardTranType, storedCard);
            this.payments.payment.securityAVSCSC = new securityAVSCSC
            {
                cardSecurityCode = cardSecurityCode
            };
        }

        public paymentAuthorizeReq(
            IAxisConfiguration configuration,
            UniqueIdGenerator uniqueIdGenerator,
            DateTime transactionDate,
            decimal amount,
            cardTranType cardTranType,
            string cardHolderName,
            string cardNumber,
            string cardSecurityCode,
            string expiryDate,
            string merchantCode)
            : this(configuration, uniqueIdGenerator, transactionDate, merchantCode)
        {
            this.payments.payment.cardDetails = new cardDetailsCart(amount, cardTranType, cardHolderName, cardNumber, expiryDate);
            this.payments.payment.securityAVSCSC = new securityAVSCSC
            {
                cardSecurityCode = cardSecurityCode
            };
        }

        private paymentAuthorizeReq(
            IAxisConfiguration configuration,
            UniqueIdGenerator uniqueIdGenerator,
            DateTime transactionDate,
            string merchantCode)
            : this()
        {
            var uniqueId = uniqueIdGenerator.GenerateUniqueId();
            this.header = new headerCart(configuration, uniqueId, transactionDate, merchantCode);
            this.credentials = new credentials(configuration, uniqueId, transactionDate);
            this.schemaVersion = AxisPaymentService.SchemaVersion;

            this.payments = new paymentsCartAuthorize { payment = new paymentsCartAuthorizePayment() };
        }
    }
}