﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class headerCart
    {
        public headerCart()
        {
        }

        public headerCart(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate) : this()
        {
            this.sourceDetails = new sourceDetailsCart(configuration, uniqueTranId, transactionDate);
        }

        public headerCart(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate, string merchantCode) : this()
        {
            this.sourceDetails = new sourceDetailsCart(configuration, uniqueTranId, transactionDate, merchantCode);
        }
    }
}