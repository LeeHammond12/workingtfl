﻿namespace G2g3digital.Payments.Axis.Model
{
    public partial class storedCardDetailsNew
    {
        public storedCardDetailsNew()
        {
        }

        public storedCardDetailsNew(string cardNumber, string expiryDate, string cardHolderName)
        {
            this.cardNumber = cardNumber;
            this.expiryDate = expiryDate;
            this.cardHolderName = cardHolderName;
        }
    }
}