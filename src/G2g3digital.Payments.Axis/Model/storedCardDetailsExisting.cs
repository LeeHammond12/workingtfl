﻿namespace G2g3digital.Payments.Axis.Model
{
    public partial class storedCardDetailsExisting
    {
        public storedCardDetailsExisting()
        {
        }

        public storedCardDetailsExisting(string lastFourDigits, string templateNo)
        {
            this.cardLastFourDigits = lastFourDigits;
            this.storedCardTemplateNo = templateNo;
        }
    }
}