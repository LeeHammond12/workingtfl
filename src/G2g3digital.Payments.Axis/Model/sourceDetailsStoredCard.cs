﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class sourceDetailsStoredCard
    {
        public sourceDetailsStoredCard()
        {
        }

        public sourceDetailsStoredCard(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate)
            : base(configuration, uniqueTranId, transactionDate)
        {
            this.systemCode = systemCode1.EXT;
            //this.cardholderID = "1";
        }
    }
}