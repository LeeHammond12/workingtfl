﻿using System;

namespace G2g3digital.Payments.Axis.Model
{
    public partial class headerDirect
    {
        public headerDirect()
        {
        }

        public headerDirect(IAxisConfiguration configuration, string uniqueTranId, DateTime transactionDate)
        {
            this.sourceDetails = new sourceDetailsDirect(configuration, uniqueTranId, transactionDate);
        }
    }
}