﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using G2g3digital.Payments.Axis.Model;

namespace G2g3digital.Payments.Axis
{
    public interface IAxisPaymentService
    {
        Task<storeCardRes> StoreCardAsync(DateTime transactionDate, string cardNumber, string expiryDate, string cardHolderName);

        Task<storeCardRes> StoreCardDeleteAsync(DateTime transactionDate, string lastFourDigits, string templateNo);

        Task<paymentRes> PaymentAuthorizeAsync(
            DateTime transactionDate,
            decimal amount,
            cardTranType cardTranType,
            string lastFourDigits,
            string cardSecurityCode,
            string templateNo,
            string merchantCode);

        Task<paymentRes> PaymentAuthorizeAsync(
            DateTime transactionDate,
            decimal amount,
            cardTranType cardTranType,
            string cardHolderName,
            string cardNumber,
            string cardSecurityCode,
            string expiryDate,
            string merchantCode);

        Task<paymentRes> PaymentCommitAsync(
            DateTime transactionDate,
            string authTranId,
            DateTime authTransactionDate,
            ICollection<item> items);

        Task<paymentRes> PaymentCommitAsync(
            DateTime transactionDate,
            string authTranId,
            DateTime authTransactionDate,
            string fundCode,
            string reference,
            decimal amount);
    }
}