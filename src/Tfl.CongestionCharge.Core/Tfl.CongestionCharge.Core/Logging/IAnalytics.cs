﻿using System;
using System.Collections.Generic;

namespace Tfl.CongestionCharge.Core.Logging
{
    public interface IAnalytics
    {
        void Close();

        void AddUnhandledExceptionEvent(Exception ex, string message);

        void AddHandledExceptionEvent(Exception ex, string message);

        void AddEvent(string eventName, Dictionary<string, string> attributes = null);

        void AddEvent(string eventName, string key, string value);
    }
}