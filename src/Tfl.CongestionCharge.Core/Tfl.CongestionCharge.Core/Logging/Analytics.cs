﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Helper;

namespace Tfl.CongestionCharge.Core.Logging
{
    public class Analytics
    {
        private static readonly List<IAnalytics> Listeners = new List<IAnalytics>();

        public static void AddHandledExceptionEvent(Exception ex, [CallerMemberName] string callerMemberName = "")
        {
            Listeners.ForEach(l => ExecuteFailSafe(() => l.AddHandledExceptionEvent(ex, callerMemberName)));
        }

        public static void AddUnhandledExceptionEvent(Exception ex, string message)
        {
            Listeners.ForEach(l => ExecuteFailSafe(() => l.AddUnhandledExceptionEvent(ex, message)));
        }

        public static void AddEvent(string eventName, Dictionary<string, string> attributes = null)
        {
            Listeners.ForEach(l => ExecuteFailSafe(() => l.AddEvent(eventName, attributes)));
        }

        public static void AddEvent(string eventName, string key, string value)
        {
            Listeners.ForEach(l => ExecuteFailSafe(() => l.AddEvent(eventName, key, value)));
        }

        public static void AddListener(IAnalytics listener)
        {
            Listeners.Add(listener);
        }

        public static void RemoveListener(IAnalytics listener)
        {
            Listeners.Remove(listener);
        }

        public static void Close()
        {
            Listeners.ForEach(l => ExecuteFailSafe(l.Close));
        }

        private static void ExecuteFailSafe(Action a)
        {
            try
            {
                a();
            }
            catch (Exception)
            {
                Dbg.BreakIfAttached();
            }
        }
    }
}