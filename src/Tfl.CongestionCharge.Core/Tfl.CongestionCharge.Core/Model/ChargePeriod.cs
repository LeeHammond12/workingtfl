﻿namespace Tfl.CongestionCharge.Core.Model
{
    public enum ChargePeriod
    {
        Daily,
        PayNextDay,
        Weekly,
        Monthly,
    }
}
