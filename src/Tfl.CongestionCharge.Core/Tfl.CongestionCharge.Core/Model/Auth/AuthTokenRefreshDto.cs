﻿using Refit;

namespace Tfl.CongestionCharge.Core.Model.Auth
{
    public class AuthTokenRefreshDto
    {
        [AliasAs("grant_type")]
        public string GrantType => "refresh_token";

        [AliasAs("refresh_token")]
        public string RefreshToken { get; set; }
    }
}