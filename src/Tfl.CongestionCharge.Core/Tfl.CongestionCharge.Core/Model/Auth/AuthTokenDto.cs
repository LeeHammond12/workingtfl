﻿using Refit;

namespace Tfl.CongestionCharge.Core.Model.Auth
{
    public class AuthRequestDto
    {
        [AliasAs("grant_type")]
        public string GrantType => "password";

        [AliasAs("username")]
        public string UserName { get; set; }

        [AliasAs("password")]
        public string Password { get; set; }
    }
}