﻿using Newtonsoft.Json;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model.Auth
{
    public class AuthResponseDto : ClientApiResponseDtoBase
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}