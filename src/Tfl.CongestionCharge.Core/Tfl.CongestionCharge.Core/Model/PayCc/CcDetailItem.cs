﻿namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcDetailItem
    {
        public string ChargeStatus { get; set; }

        public CcDetailSingleDay Daily { get; set; }

        public CcDetailSingleDay PayNextDay { get; set; }

        public CcDetailMultipleDay Weekly { get; set; }

        public CcDetailMultipleDay Monthly { get; set; }

        public CcDetailMultipleDay Custom { get; set; }

        public int CcChargeDays { get; set; }
    }
}
