﻿namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcPaymentDetail
    {
        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string PeriodType { get; set; }

        ////[JsonConverter(typeof(QuotedDecimalFormatConverter))]
        public decimal Amount { get; set; }

        public CcVehicle Vehicle { get; set; }
    }
}