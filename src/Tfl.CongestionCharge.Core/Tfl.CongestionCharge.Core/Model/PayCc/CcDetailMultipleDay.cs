﻿using System;

namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcDetailMultipleDay : ICcDetail
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public decimal Amount { get; set; }

        public bool CcPaid { get; set; }
    }
}