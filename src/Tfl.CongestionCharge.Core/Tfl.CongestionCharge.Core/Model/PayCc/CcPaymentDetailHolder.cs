﻿namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcPaymentDetailHolder
    {
        public CcPaymentDetail ChargePayment { get; set; }
    }
}