﻿using System;

namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcDetailSingleDay : ICcDetail
    {
        public DateTime? Date { get; set; }

        public decimal Amount { get; set; }

        public bool CcPaid { get; set; }
    }
}
