﻿using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcPaymentReceipt : ClientApiResponseDtoBase
    {
        public string ReceiptId { get; set; }
    }
}