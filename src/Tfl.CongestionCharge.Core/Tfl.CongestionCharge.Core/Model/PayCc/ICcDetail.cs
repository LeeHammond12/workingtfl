﻿namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public interface ICcDetail
    {
        decimal Amount { get; set; }

        bool CcPaid { get; set; }
    }
}