﻿using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcDetail : ClientApiResponseDtoBase
    {
        public string AccountNumber { get; set; }

        public CcChargeListHolder CcChargeList { get; set; }

        public int CcChargeDays { get; set; }
    }
}