﻿using System.Collections.Generic;

namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcChargeListHolder
    {
        public List<CcDetailItem> CcCharge { get; set; }
    }
}