﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcPayment
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AccountNumber { get; set; }

        public List<CcPaymentDetail> CcPaymentList { get; set; }

        public CcPaymentAuthDetail PaymentAuthDetail { get; set; }

        public string MobileTelNumber { get; set; }

        ////[JsonConverter(typeof(QuotedDecimalFormatConverter))]
        public decimal TotalAmount { get; set; }
    }
}