﻿namespace Tfl.CongestionCharge.Core.Model.PayCc
{
    public class CcPaymentAuthDetail
    {
        public string PaymentTransactionDate { get; set; }

        public string PaymentTransactionId { get; set; }

        //// public string PrimaryCan { get; set; }

        public string MachineCode { get; set; }

        public string Last4Digits { get; set; }
    }
}