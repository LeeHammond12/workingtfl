﻿using Tfl.CongestionCharge.Core.Model.AccountInfo;

namespace Tfl.CongestionCharge.Core.Model
{
    public class ReceiptDto
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public string TransactionId { get; set; }
        public string TotalAmount { get; set; }
        public AccountVehicleDto Vehicle { get; set; }
        public ReceiptAuthDetailDto PaymentAuthDetail { get; set; }
    }
}
