﻿using System;

namespace Tfl.CongestionCharge.Core.Model
{
    public class ReceiptAuthDetailDto
    {
        public string PaymentTransactionId { get; set; }
        public string TransactionCanList { get; set; }
        public DateTime? PaymentTransactionDate { get; set; }
    }
}
