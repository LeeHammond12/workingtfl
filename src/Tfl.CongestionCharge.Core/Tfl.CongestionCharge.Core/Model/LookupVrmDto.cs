﻿using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model
{
    public class LookupVrmDto : ClientApiResponseDtoBase, IVehicleDto
    {
        public string VRM { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string Colour { get; set; }

        public bool IsCcChargeable { get; set; }

        // 0 if not Subject to ES
        // 1 if Subject to ES
        // 2 if Subject to ES but Exempt
        public int IsESChargeable { get; set; }

        /// <summary>
        /// Set to True if the vehicle is linked to a “non-closed”
        /// Autopay service(CC and Fleet)
        /// </summary>
        public bool InAutoPay { get; set; }

        public bool Is100PcDiscounted { get; set; }

        public bool ModelMakeColorEmpty
            => string.IsNullOrEmpty(Model) &&
               string.IsNullOrEmpty(Make) &&
               string.IsNullOrEmpty(Colour);
    }
}