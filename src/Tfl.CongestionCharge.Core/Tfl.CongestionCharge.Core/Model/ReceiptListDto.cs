﻿using System.Collections.Generic;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model
{
    public class ReceiptListDto : ClientApiResponseDtoBase
    {
        public List<ReceiptDto> FinancialTransactionList { get; set; }
    }
}