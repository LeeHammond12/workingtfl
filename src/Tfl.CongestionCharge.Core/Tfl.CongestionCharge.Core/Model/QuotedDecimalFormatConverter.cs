﻿using System;
using Newtonsoft.Json;

namespace Tfl.CongestionCharge.Core.Model
{
    /// <summary>
    /// TFL Api spec specifies quoted decimals in some places e.g. page 49.
    /// </summary>
    public class QuotedDecimalFormatConverter : JsonConverter
    {
        public override bool CanRead => false;

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(decimal);
        }

        public override void WriteJson(
            JsonWriter writer,
            object value,
            JsonSerializer serializer)
        {
            writer.WriteValue("{value:N2}");
        }

        public override object ReadJson(
            JsonReader reader,
            Type objectType,
            object existingValue,
            JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}