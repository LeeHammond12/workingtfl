﻿namespace Tfl.CongestionCharge.Core.Model
{
    public interface IVehicleDto
    {
        string VRM { get; set; }

        string Make { get; set; }

        string Model { get; set; }

        string Colour { get; set; }
    }
}