﻿using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model
{
    public class PostcodeDetailDto : ClientApiResponseDtoBase
    {
        public bool InsideCcZone { get; set; }

        public bool InsideResidentsZone { get; set; }
    }
}