﻿using System.Collections.Generic;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class AccountVehicleWithServicesDto : AccountVehicleDto
    {
        public List<AccountTflServiceDto> ServiceList { get; }

        public AccountVehicleWithServicesDto()
        {
            ServiceList = new List<AccountTflServiceDto>();
        }

        public AccountVehicleWithServicesDto(VehicleViewModel vehicle)
            : base(vehicle)
        {
            ServiceList = new List<AccountTflServiceDto>();
        }
    }
}