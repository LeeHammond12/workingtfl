﻿namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class AccountAddressDto
    {
        public string BuildingNumber { get; set; }
        public string Street { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
    }
}