﻿using Newtonsoft.Json;

namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class AddDeleteOrReplacePaymentCardDto
    {
        public AccountPaymentCardDto PaymentCardDetail { get; set; }

        public bool AutoPay { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ServiceId { get; set; }
    }
}