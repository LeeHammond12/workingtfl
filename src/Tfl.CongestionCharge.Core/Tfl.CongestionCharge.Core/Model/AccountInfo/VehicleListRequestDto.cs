﻿using System.Collections.Generic;

namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class VehicleListRequestDto
    {
        public List<AccountVehicleDto> VehicleList { get; set; }
    }
}
