﻿using System.Collections.Generic;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class VehicleListResponseDto : ClientApiResponseDtoBase
    {
        public List<AccountVehicleWithServicesDto> VehicleList { get; set; }
    }
}
