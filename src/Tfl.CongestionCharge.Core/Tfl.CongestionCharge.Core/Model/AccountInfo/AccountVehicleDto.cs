﻿using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class AccountVehicleDto : IVehicleDto
    {
        public string VRM { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string Colour { get; set; }

        //public string Type { get; set; }
        public AccountVehicleDto()
        {
        }

        public AccountVehicleDto(VehicleViewModel vehicle)
        {
            Colour = vehicle.Color;
            VRM = vehicle.Vrm;
            Make = vehicle.Make;
            Model = vehicle.Model;
        }
    }
}