﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class AccountInformationDto : ClientApiResponseDtoBase
    {
        public string AccountNumber { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string AccountType { get; set; }
        public AccountAddressDto Address { get; set; }
        public string DaytimeTelNumber { get; set; }
        public string EveningTelNumber { get; set; }
        public string MobileTelNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Balance { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<AccountVehicleWithServicesDto> VehicleList { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<AccountPaymentCardDto> PaymentCardList { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<AccountTflServiceDto> ServiceList { get; set; }

        public DateTime AutoPayStatementDate { get; set; }
        public DateTime? LastAutoPayStatementDate { get; set; }

        public AccountInformationDto()
        {
            VehicleList = new List<AccountVehicleWithServicesDto>();
            PaymentCardList = new List<AccountPaymentCardDto>();
            ServiceList = new List<AccountTflServiceDto>();
        }
    }
}
