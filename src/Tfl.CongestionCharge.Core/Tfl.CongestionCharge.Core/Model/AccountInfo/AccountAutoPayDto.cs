﻿namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class AccountAutoPayDto
    {
        public string AutoPayType { get; set; }
        public string AutoPayDescription { get; set; }
        public string AutoPayStatus { get; set; }
        public string AutoPayCurrentBalance { get; set; }
        public string AutoPayStatementBalance { get; set; }
    }
}