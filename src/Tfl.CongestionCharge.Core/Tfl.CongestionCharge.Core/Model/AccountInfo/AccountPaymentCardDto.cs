﻿using Newtonsoft.Json;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class AccountPaymentCardDto
    {
        public string Token { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Last4Digits { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ExpiryDate { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Issuer { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CardType { get; set; }

        public bool IsAutoPayCard => CardType == "A";

        public AccountPaymentCardDto()
        {
        }

        public AccountPaymentCardDto(PaymentCardViewModel card)
        {
            CardType = card.CardType;
            ExpiryDate = card.ExpiryDate;
            Issuer = card.Issuer;
            Last4Digits = card.Last4Digits;
            Token = card.TemplateNumber;
        }
    }
}