﻿using System;

namespace Tfl.CongestionCharge.Core.Model.AccountInfo
{
    public class AccountTflServiceDto
    {
        public string Name { get; set; }
        public string ServiceId { get; set; }
        public string ServiceType { get; set; }
        public AccountAutoPayDto AutoPayType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsRenewable { get; set; }
        public string PaymentMethod { get; set; }
        public AccountPaymentCardDto PaymentCardDetail { get; set; }

        public bool IsCcAutoPay => AutoPayType != null && AutoPayType.AutoPayType == "CC" && AutoPayType.AutoPayStatus == "Active";
    }
}