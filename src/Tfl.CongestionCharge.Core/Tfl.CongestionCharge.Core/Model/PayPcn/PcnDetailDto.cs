﻿using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model.PayPcn
{
    public class PcnDetailDto : ClientApiResponseDtoBase
    {
        public string PCNNumber { get; set; }

        public string PCNStatus { get; set; }

        public decimal AmountDue { get; set; }

        public bool CanBePaid { get; set; }

        public string CanNotBePaidReason { get; set; }
    }
}