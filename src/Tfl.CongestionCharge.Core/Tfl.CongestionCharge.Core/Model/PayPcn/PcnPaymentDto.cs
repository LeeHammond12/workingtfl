﻿namespace Tfl.CongestionCharge.Core.Model.PayPcn
{
    public class PcnPaymentDto
    {
        ////[JsonConverter(typeof(QuotedDecimalFormatConverter))]
        public decimal PaymentAmount { get; set; }

        public string PaymentDateTime { get; set; }

        public string PaymentTransactionRef { get; set; }
    }
}