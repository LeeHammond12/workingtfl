﻿using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Model.PayPcn
{
    public class PcnPaymentReceiptDto : ClientApiResponseDtoBase
    {
        public string ReceiptId { get; set; }
    }
}