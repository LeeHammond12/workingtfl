﻿using System;

namespace Tfl.CongestionCharge.Core.Model.Payments
{
    public class PaymentRequestDto
    {
        public decimal Amount { get; set; }

        public string UniqueTransactionId { get; set; }

        public DateTime TransactionDate { get; set; }

        public string FundCode { get; set; }

        public string ReceiptId { get; set; }

        public string CardNumber { get; set; }

        public string NameOnCard { get; set; }

        public string CardNumberLastFourDigits { get; set; }

        public string CardExpiryDate { get; set; }

        public string TemplateNumber { get; set; }

        public string MobileNumber { get; set; }

        public string AccountNumber { get; set; }

        public string CardSecurityCode { get; set; }

        public string Issuer { get; set; }

        public PaymentRequestDto()
        {
        }
    }
}