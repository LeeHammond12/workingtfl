﻿using System.Collections.Generic;
using System.Linq;

namespace Tfl.CongestionCharge.Core.Model.Base
{
    public abstract class ClientApiResponseDtoBase
    {
        public List<ErrorResponseItem> Errors { get; set; }

        public bool HasErrors => Errors?.Count > 0;

        public bool HasData => Errors?.FirstOrDefault()?.Data?.Count > 0;

        public int? IsEsChargeable => ParseIsEsChargeableFlag();

        private int? ParseIsEsChargeableFlag()
        {
            int? value = null;
            try
            {
                if (HasData)
                {
                    value = int.Parse(Errors.FirstOrDefault().Data
                        .FirstOrDefault(d => d.Fieldname == "IsESChargeable").Value);
                }
            }
            catch (System.Exception)
            {
            }
            return value;
        }
    }
}