﻿namespace Tfl.CongestionCharge.Core.Model.Base
{
    public class ClientApiResponseDataItem
    {
        public string Fieldname { get; set; }
        public string Value { get; set; }
    }
}
