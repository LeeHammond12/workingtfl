﻿namespace Tfl.CongestionCharge.Core.Model.Base
{
    public class ClientApiErrorCodes
    {
        // Generic error messages that can be thrown from any API
        public static string ApiKeyNotPresent = "7000001";

        public static string ApiKeyNotValid = "7000002";

        // public static string JsonAcceptHeaderMissing = 7000003;

        // specific errors for =>  lruc/v1/account/vehicledetail/quickmanaged (POST)
        public static string CustomerCannotRegisterMoreThan10PreferredVehicles = "26500";
        public static string AlreadyHaveAPreferredVehicleWithGivenVrm = "10009";

        // specific errors for =>  lruc/v1/pcn/{PcnNumber}?vrm={VRM} (GET)
        public static string PcnNotFoundOnTarantoSystem = "12";

        public static string PcnReferenceDoesNotExist = "23";
        public static string PcnReferenceNotValid = "24";
        public static string PcnOverpayment = "25";
        public static string PcnAlreadyCancelled = "40";
        public static string PcnNotFound = "51";
        public static string PcnMissingFieldMissingInRequest = "7011004";

        // specific errors for =>  lruc/v1/vrm (GET)
        public static string VrmNoVehicleDetailsFound = "0133";

        public static string VrmOneOrMoreFieldsMissing = "7013004";

        // /lruc/v1/congestioncharge/{VRM}?IsUk={true|false}&StartDate={StartDate}&EndDate={EndDate} (GET)
        public static string ChargeCannotStartFurtherInThePastThanThePreviousChargingDay = "12600";

        public static string ChargeCannotStartMoreThan65DaysInFuture = "12601";
        public static string StartDateIsAfterTheResidentDiscountPeriod = "12607";
        public static string ChargeCannotStartFurtherInThePastThanThePreviousChargingDay1 = "12608";

        // lruc/v1/congestioncharge/ (GET)
        public static string CcOneOrMoreFieldsMissingInBackEndResponse = "7016003";

        public static string CcOneOrMoreFieldsMissingInRequest = "7016004";

        // specific errors for => /lruc/v1/congestioncharge/payment (POST)
        public static string CcPaymentNoVehicleDetailsFound = "11259";

        public static string CcDateIsNotChargeable = "11260";
        public static string CcVrmAlreadyHas100PercentDiscount = "11604";
        public static string CcVrmOnAutoPayServiceCannotBeUsed = "40711";

        // lruc/v1/zonechecker/address
        public static string PcAddressNotFound = "50001";

        public static string PcMissingParameters = "7014004";

        // others
        public static string CannotAddVehicleToAutoPayBecauseItsAlreadyOnAutoPay = "1020110013";
        public static string StatementDateIsInComing4DaysSoYouCanNotChangeToDirectDebit = "4080710753";

    }
}