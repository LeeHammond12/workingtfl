﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tfl.CongestionCharge.Core.Model.Base
{
    public class ErrorResponseItem
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("code")]
        public string ErrorCode { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("data")]
        public List<ClientApiResponseDataItem> Data { get; set; }
    }
}