﻿using FluentValidation;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.Main;

namespace Tfl.CongestionCharge.Core.Validators
{
    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        public LoginViewModelValidator()
        {
            RuleFor(model => model.AccountNumber)
                .Matches(@"^([a-zA-Z0-9]){1,10}$")
                .WithMessage(Strings.ValidCustomerIdRequired);

            RuleFor(model => model.Password)
                .Matches(@"^.{1,40}$")
                .WithMessage(Strings.ValidPasswordRequired);
        }
    }
}
