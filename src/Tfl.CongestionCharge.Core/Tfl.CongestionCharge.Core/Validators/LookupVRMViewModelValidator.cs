﻿using FluentValidation;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;

namespace Tfl.CongestionCharge.Core.Validators
{
    public class LookupVrmViewModelValidator : AbstractValidator<LookupVrmViewModel>
    {
        public LookupVrmViewModelValidator()
        {
            RuleFor(model => model.EnteredVrm)
                .Matches(@"^[\w\s]*$")
                .WithMessage(Strings.VrmCannotContainSpecialCharacters);
        }
    }
}