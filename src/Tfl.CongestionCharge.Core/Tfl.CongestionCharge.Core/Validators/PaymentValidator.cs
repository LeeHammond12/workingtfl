﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using FluentValidation;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.Payments;

namespace Tfl.CongestionCharge.Core.Validators
{
    public class PaymentValidator : AbstractValidator<PaymentViewModelBase>
    {
        public PaymentValidator(bool hasToken)
        {
            var cardNumberLengthList = new List<int> { 13, 15, 16 };

            if (!hasToken)
            {
                RuleFor(model => model.NameOnCard)
                    .NotEmpty();
            }

            RuleFor(model => model.CardNumber)
                .Must(model => cardNumberLengthList
                .Contains(model.Length))
                .WithMessage(Strings.CardNumberNotValid);

            RuleFor(model => model.ExpiryDate)
                .Length(4);

            RuleFor(model => model.Cvv2)
                .Length(3)
                .WithMessage(Strings.Cvv2Length);

            RuleFor(model => model.SmsNumber)
                .Length(11)
                .When(model => model.IsSmsTextVisible)
                .WithMessage(Strings.SmsNumberLength);

            RuleFor(model => model.SmsNumber)
                .Matches(new Regex(@"^\d+$"))
                .When(model => !model.SmsNumber.Equals(string.Empty))
                .WithMessage(Strings.PleaseEnterCorrectSmsNumber);

            RuleFor(model => model.SmsNumber)
                .Must(str => str.StartsWith("07"))
                .When(model => model.IsSmsTextVisible)
                .WithMessage(Strings.SmsNumberStartsWith);
        }
    }
}