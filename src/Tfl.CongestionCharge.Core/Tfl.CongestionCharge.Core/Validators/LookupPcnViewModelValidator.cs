﻿using FluentValidation;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayPcn;

namespace Tfl.CongestionCharge.Core.Validators
{
    public class LookupPcnViewModelValidator : AbstractValidator<PcnLookupViewModel>
    {
        public LookupPcnViewModelValidator()
        {
            RuleFor(model => model.PcnNumber)
                .Matches(@"^\p{L}{2,2}\p{Nd}{7,7}[1234567890A]$")
                .WithMessage(Strings.PcnHasWrongFormat);
        }
    }
}