﻿using FluentValidation;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Validators
{
    public class PostCodeViewModelValidator : AbstractValidator<PostCodeCheckerViewModel>
    {
        public PostCodeViewModelValidator()
        {
            RuleFor(model => model.PostCode)
                .Matches(@"^(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX‌​]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY]))))\s?[0-9][A-Z-[C‌​IKMOV]]{2})$")
                .WithMessage(Strings.PleaseEnterAValidPostCode);

            RuleFor(model => model.BuildingNumber)
                .Length(0, 5)
                .WithMessage(Strings.PleaseEnterAValidBuildingNumber);
        }
    }
}