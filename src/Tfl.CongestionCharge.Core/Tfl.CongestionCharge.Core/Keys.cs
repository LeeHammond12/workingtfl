﻿namespace Tfl.CongestionCharge.Core
{
    internal static class Keys
    {
        internal static readonly string ApiKeyHeader = "apiKey";

        internal class ApiEndpoints
        {
#if USE_SIT_API
            internal static readonly string BaseUri = "https://m.tflsit.co.uk/";
#elif USE_UAT_API
            internal static readonly string BaseUri = "https://2m.tflpp.co.uk/";
#elif USE_PRODUCTION_API
            internal static readonly string BaseUri = "https://m.tflcc.co.uk/";
#else
            // internal static readonly string BaseUri = "http://tflcongestionchargemockapi-v1-3.azurewebsites.net/";
            // internal static readonly string BaseUri = "http://tflcongestionchargemockapi.azurewebsites.net/";
            internal static readonly string BaseUri = "https://tfl.api.thumbmunkeys.com";
#endif
        }
    }
}