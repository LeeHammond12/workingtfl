﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Extensions;

namespace Tfl.CongestionCharge.Core
{
    public static class NetworkUtils
    {
        public static async Task PrintRequest(HttpRequestMessage request, HttpResponseMessage response)
        {
            Debug.WriteLine($"[{DateTime.Now.ToHumanReadableFormat()}] Request: {request.RequestUri} ({request.Method})");

            Debug.WriteLine($"++++++++");

            if (response?.Content != null)
            {
                Debug.WriteLine($"Response: {response.StatusCode}:\r\n {await response.Content.ReadAsStringAsync()}");
            }
            else
            {
                Debug.WriteLine($"Response: {response?.StatusCode}\r\rn Content was NULL");
            }

            Debug.WriteLine($"***********************************************");
        }
    }
}