﻿namespace Tfl.CongestionCharge.Core.PCLCrypto
{
    public interface ICryptoKeyStore
    {
        void StoreKey();

        byte[] GetKey();
    }
}