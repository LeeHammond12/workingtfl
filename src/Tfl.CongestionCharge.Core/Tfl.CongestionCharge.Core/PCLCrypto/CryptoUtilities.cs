﻿using System;
using System.Text;
using PCLCrypto;

namespace Tfl.CongestionCharge.Core.PCLCrypto
{
    public static class CryptoUtilities
    {
        public const int IvSize = 16;

        public static byte[] GetAes256KeyMaterial()
        {
            return WinRTCrypto.CryptographicBuffer.GenerateRandom(32);
        }

        public static byte[] Get256BitSalt()
        {
            return WinRTCrypto.CryptographicBuffer.GenerateRandom(32);
        }

        public static byte[] Encrypt(string plainText, byte[] keyMaterial)
        {
            var data = StringToByteArray(plainText);
            var provider = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            var symmetricKey = provider.CreateSymmetricKey(keyMaterial);
            var iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(IvSize);
            var cipher = WinRTCrypto.CryptographicEngine.Encrypt(symmetricKey, data, iv);
            var ciphertext = new byte[cipher.Length + iv.Length];
            iv.CopyTo(ciphertext, 0);
            cipher.CopyTo(ciphertext, iv.Length);
            return ciphertext;
        }

        public static string Decrypt(byte[] cipherText, byte[] keyMaterial)
        {
            var provider = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            var symmetricKey = provider.CreateSymmetricKey(keyMaterial);
            var iv = new byte[IvSize];
            Array.Copy(cipherText, iv, iv.Length);

            var cipher = new byte[cipherText.Length - iv.Length];
            Array.Copy(cipherText, iv.Length, cipher, 0, cipher.Length);
            return ByteArrayToString(WinRTCrypto.CryptographicEngine.Decrypt(symmetricKey, cipher, iv));
        }

        public static byte[] GetHash(byte[] data, byte[] salt)
        {
            var salteddata = new byte[salt.Length + data.Length];
            Array.Copy(salt, salteddata, salt.Length);
            Array.Copy(data, 0, salteddata, salt.Length, data.Length);
            var sha = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha256);
            return sha.HashData(salteddata);
        }

        public static string ByteArrayToString(byte[] data)
        {
            return Encoding.Unicode.GetString(data, 0, data.Length);
        }

        public static byte[] StringToByteArray(string text)
        {
            return Encoding.Unicode.GetBytes(text);
        }
    }
}
