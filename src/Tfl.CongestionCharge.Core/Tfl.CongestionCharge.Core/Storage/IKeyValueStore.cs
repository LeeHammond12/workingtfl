﻿namespace Tfl.CongestionCharge.Core.Storage
{
    public interface IKeyValueStore
    {
        /// <summary>
        /// Backup or replace a token.
        /// </summary>
        void Backup(string token, object value);

        /// <summary>
        /// restore value associated with token.
        /// Throws KeyNotFoundException if token not found.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        T Restore<T>(string token);

        T Restore<T>(string token, T defaultValue);

        bool ContainsKey(string token);

        bool RemoveKey(string token);
    }
}