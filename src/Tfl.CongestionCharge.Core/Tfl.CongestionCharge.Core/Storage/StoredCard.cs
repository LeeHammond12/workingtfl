﻿using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Storage
{
    public class StoredCard
    {
        //public string NameOnCard { get; set; }

        public string Last4Digits { get; set; }

        public string Issuer { get; set; }

        public string ExpiryDate { get; set; }

        public string Token { get; set; }

        public StoredCard()
        {
        }

        public StoredCard(PaymentCardViewModel card)
        {
            //NameOnCard = card.NameOnCard;
            Last4Digits = card.Last4Digits;
            ExpiryDate = card.ExpiryDate;
            Token = card.TemplateNumber;
            Issuer = card.Issuer;
        }
    }
}