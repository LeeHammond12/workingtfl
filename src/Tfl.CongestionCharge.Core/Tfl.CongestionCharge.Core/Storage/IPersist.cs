﻿namespace Tfl.CongestionCharge.Core.Storage
{
    public interface IPersist<T>
    {
        void Backup(T value);

        /// <summary>
        /// restore value associated with token.
        /// Throws KeyNotFoundException if token not found.
        /// </summary>
        T Restore();

        bool IsStored();

        bool Remove();
    }
}