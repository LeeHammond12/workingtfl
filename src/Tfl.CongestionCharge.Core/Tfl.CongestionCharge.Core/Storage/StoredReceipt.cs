﻿using System;
using Tfl.CongestionCharge.Core.Model;

namespace Tfl.CongestionCharge.Core.Storage
{
    public class StoredReceipt
    {
        public bool IsPcn => !string.IsNullOrWhiteSpace(PcnNumber);

        public string ReceiptId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime PaymentDate { get; set; }

        //public DateTime ChargingDate { get; set; }

        public string Vrm { get; set; }

        public int Days { get; set; }

        public decimal Price { get; set; }

        public ChargePeriod PeriodType { get; set; }

        public string PcnNumber { get; set; }

        public string DateString { get; set; }
    }
}