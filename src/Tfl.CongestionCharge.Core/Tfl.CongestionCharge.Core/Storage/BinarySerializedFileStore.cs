﻿using System.Collections.Generic;
using Nito.AsyncEx.Synchronous;
using PCLStorage;
using PCLStorage.Exceptions;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Serialization.Interfaces;

namespace Tfl.CongestionCharge.Core.Storage
{
    /// <summary>
    /// can be used in combination with a protoserializer.
    /// Stores each key as a separate file in the provided directory.
    /// Key existance is determined by file existance.
    /// </summary>
    public class BinarySerializedFileStore : IKeyValueStore
    {
        private readonly IBinarySerializer _serializer;

        public IFolder Folder { get; }

        public BinarySerializedFileStore(IBinarySerializer serializer, IFolder folder)
        {
            _serializer = serializer;
            Folder = folder;
        }

        public void Backup(string token, object value)
        {
            var bytes = _serializer.Serialize(value);
            var file = Folder.CreateFileAsync(token, CreationCollisionOption.ReplaceExisting).WaitAndUnwrapException();
            file.WriteAllBytes(bytes).WaitAndUnwrapException();
        }

        public T Restore<T>(string token)
        {
            try
            {
                var file = Folder.GetFileAsync(token).WaitAndUnwrapException();
                var bytes = file.ReadAllBytes().WaitAndUnwrapException();
                return _serializer.Deserialize<T>(bytes);
            }
            catch (FileNotFoundException)
            {
                throw new KeyNotFoundException(token);
            }
        }

        public T Restore<T>(string token, T defaultValue)
        {
            if (!ContainsKey(token))
                return defaultValue;
            return Restore<T>(token);
        }

        public bool ContainsKey(string token)
        {
            return Folder.FileExists(token).WaitAndUnwrapException();
        }

        public bool RemoveKey(string token)
        {
            try
            {
                var file = Folder.GetFileAsync(token).WaitAndUnwrapException();
                file.DeleteAsync().WaitAndUnwrapException();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}