﻿namespace Tfl.CongestionCharge.Core.Storage
{
    public abstract class Persist<T> : IPersist<T>
    {
        protected abstract string Key { get; }

        private readonly IKeyValueStore _store;

        protected Persist(IKeyValueStore store)
        {
            _store = store;
        }

        public void Backup(T value)
        {
            _store.Backup(Key, value);
        }

        public T Restore()
        {
            return _store.Restore<T>(Key);
        }

        public bool IsStored()
        {
            return _store.ContainsKey(Key);
        }

        public bool Remove()
        {
            return _store.RemoveKey(Key);
        }
    }
}
