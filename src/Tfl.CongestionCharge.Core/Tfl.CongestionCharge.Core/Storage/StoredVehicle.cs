﻿using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Storage
{
    public class StoredVehicle
    {
        public string Vrm { get; set; }

        public string Model { get; set; }

        public string Color { get; set; }

        public string Make { get; set; }

        public StoredVehicle()
        {
        }

        public StoredVehicle(VehicleViewModel v)
        {
            Vrm = v.Vrm;
            Model = v.Model;
            Color = v.Color;
            Make = v.Make;
        }
    }
}
