﻿using System.Threading.Tasks;
using PCLStorage;

namespace Tfl.CongestionCharge.Core.Storage
{
    public interface IFolderProvider
    {
        Task<IFolder> GetFolder();
    }
}
