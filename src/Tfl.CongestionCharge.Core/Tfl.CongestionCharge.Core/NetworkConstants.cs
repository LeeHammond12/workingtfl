﻿namespace Tfl.CongestionCharge.Core
{
    public static class NetworkConstants
    {
        public const string AutoPayServiceName = "AutoPay";

        public const string AuthEndpoint = "/auth/oauth/v2/token";

        internal const int AuthRefreshToleranceInSeconds = 30;

#if USE_SIT_API
        internal static readonly string BaseUri = "https://m.tflpp.co.uk";
        public const string AuthHeader = "YmVlMWI5ZmYtMjAyZS00NDA5LThkM2QtNDAyYmU3NThiYjc0OjFiYTg4NmIyLTkzOTYtNDM0NC04ODQyLTFmNGY0YTUyMzhmOQ==";
#elif USE_UAT_API
        internal static readonly string BaseUri = "https://2m.tflpp.co.uk/";
        public const string AuthHeader = "NzU3ZTRmYmYtMDlhZC00MDAwLWFkN2UtZDk3ZWRiMzc4MzQxOjU4OTYzYmVlLWZkNzktNDllMC04ZjJlLWZiMDcwYTA4MjRmZQ==";
#elif USE_PRODUCTION_API
        internal static readonly string BaseUri = "https://m.tflcc.co.uk/";
        public const string AuthHeader = "ZDQ5YzU0MmUtYTg5My00YWM3LWE0NjItNTMwYzE3Mzg5MzkwOjUzYzlmY2Y0LWNlZDctNDdkZC1hOGM4LTlmZGYyZmZkZGNmMA==";
#else
        public const string AuthHeader = "YmVlMWI5ZmYtMjAyZS00NDA5LThkM2QtNDAyYmU3NThiYjc0OjFiYTg4NmIyLTkzOTYtNDM0NC04ODQyLTFmNGY0YTUyMzhmOQ==";
        internal static readonly string BaseUri = "https://tfl.api.thumbmunkeys.com";
#endif
    }
}