﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Services
{
    public interface IUserInformationService
    {
        Task UpdateInformation(bool useCachedValue = false);

        IReadOnlyList<VehicleViewModel> Vehicles { get; }
        IReadOnlyList<VehicleViewModel> VehiclesNotOnCcAutoPay { get; }
        IReadOnlyList<PaymentCardViewModel> PaymentCards { get; }

        Task AddVehicle(VehicleViewModel vehicle);
        Task RemoveVehicle(string vrm);
    }
}