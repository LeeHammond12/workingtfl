﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model.AccountInfo;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Services
{
    public class LoggedInUserInformationService : ILoggedInUserInformationService
    {
        private readonly ITflApi _api;

        private bool _isLoaded;
        private bool _backgroundLoadInProgress;
        private AccountInformationDto _info = new AccountInformationDto();

        private AccountInformationDto Info
        {
            get { return _info; }
            set { _info = Sanitize(value); }
        }

        public string AccountNumber => Info.AccountNumber;
        public string AccountType => Info.AccountType;
        public string Name => $"{Info.Title.WithSpace()}{Info.FirstName} {Info.MiddleName.WithSpace()}{Info.Surname}";
        public string Address => GetAddress();

        public string Balance => "£" + AutoPayService?.AutoPayType?.AutoPayCurrentBalance;
        public DateTime AutoPayStatementDate => Info.AutoPayStatementDate;
        public DateTime? LastAutoPayStatementDate => Info.LastAutoPayStatementDate;

        public AutoPayStatus AutoPayStatus => GetAutoPayStatus();

        public string AutoPayStatusText => AutoPayService?.AutoPayType?.AutoPayStatus ?? string.Empty;

        public IReadOnlyList<AccountTflServiceDto> ServiceList => Info.ServiceList ?? new List<AccountTflServiceDto>();
        public IReadOnlyList<VehicleViewModel> Vehicles => Info.VehicleList?.Select(v => new VehicleViewModel(v))?.ToList() ?? new List<VehicleViewModel>();

        public IReadOnlyList<VehicleViewModel> VehiclesNotOnCcAutoPay => Vehicles.Where(v => !v.IsOnCcAutoPay).ToList();
        public IReadOnlyList<VehicleViewModel> VehiclesActiveOnCcAutoPay => Vehicles.Where(v => v.IsOnCcAutoPay).ToList();
        public IReadOnlyList<VehicleViewModel> VehiclesWithServices => Vehicles.Where(v => v.Services?.Count() > 0).ToList();
        public IReadOnlyList<VehicleViewModel> VehiclesWithoutServices => Vehicles.Except(VehiclesWithServices).ToList();

        public IReadOnlyList<PaymentCardViewModel> PaymentCards => Info?.PaymentCardList.Select(p => new PaymentCardViewModel(p))?.ToList() ?? new List<PaymentCardViewModel>();
        //public IReadOnlyList<ReceiptViewModel> Receipts => _appState.Receipts;

        public PaymentCardViewModel AutoPayPaymentCard => PaymentCards?.FirstOrDefault(p => p.IsAutoPayCard);

        public AccountTflServiceDto AutoPayService => ServiceList?.FirstOrDefault(s => s?.ServiceType != null && s.ServiceType.Equals(NetworkConstants.AutoPayServiceName, StringComparison.OrdinalIgnoreCase));

        public bool HasAutoPay => AutoPayService != null;

        public LoggedInUserInformationService(ITflApi api)
        {
            _api = api;
        }

        public async Task UpdateInformation(bool useCachedValue)
        {
            if (useCachedValue && _isLoaded)
            {
                LoadInBackground();
                return;
            }

            Info = await _api.GetAccountInformation();
            if (Info != null)
            {
                _isLoaded = true;
            }
            else
            {
                throw new ClientApiException(new Exception("No user info returned"));
            }
        }

        private async void LoadInBackground()
        {
            if (_backgroundLoadInProgress)
                return;

            _backgroundLoadInProgress = true;
            try
            {
                Info = await _api.GetAccountInformation();
            }
            catch
            {
            }
            finally
            {
                _backgroundLoadInProgress = false;
            }
        }

        public async Task AddVehicle(VehicleViewModel vehicle)
        {
            var newList = await _api.AddVehicleToQuickList(GetVehicleListDto(vehicle));
            Info.VehicleList = newList.VehicleList;
        }

        public async Task RemoveVehicle(string vrm)
        {
            var newList = await _api.RemoveVehicleFromQuickList(vrm);
            Info.VehicleList = newList.VehicleList;
        }

        //public async Task<List<VehicleViewModel>> GetQmlVehicles()
        //{
        //    return (await _api.GetVehicleQuickList())?.VehicleList?.Select(r => new VehicleViewModel(r))?.ToList() ??
        //            new List<VehicleViewModel>();
        //}

        public async Task<List<VehicleViewModel>> GetAutoPayVehicles()
        {
            return (await _api.GetAutoPayVehicles())?.VehicleList?.Select(r => new VehicleViewModel(r))?.ToList() ??
                    new List<VehicleViewModel>();
        }

        public async Task<List<VehicleViewModel>> AddAutoPayVehicle(VehicleViewModel vehicle)
        {
            var vehicleList = GetVehicleListDto(vehicle);
            var vehicles = await _api.AddVehicleToAutoPay(vehicleList);
            return vehicles?.VehicleList?.Select(r => new VehicleViewModel(r))?.ToList() ??
                    new List<VehicleViewModel>();
        }

        public async Task<List<VehicleViewModel>> RemoveAutoPayVehicle(string vrm)
        {
            return (await _api.RemoveVehicleFromAutoPay(vrm))?.VehicleList?.Select(r => new VehicleViewModel(r))?.ToList() ??
                new List<VehicleViewModel>();
        }

        public async Task<AccountInformationDto> RemovePaymentCard(PaymentCardViewModel card)
        {
            var dto = new AddDeleteOrReplacePaymentCardDto()
            {
                AutoPay = false,
                PaymentCardDetail = new AccountPaymentCardDto(card),
            };

            return await _api.DeleteCreditCard(dto);
        }

        public async Task UpdateAutoPayPaymentCard(PaymentCardViewModel card, bool isAutoPay)
        {
            var dto = new AddDeleteOrReplacePaymentCardDto()
            {
                AutoPay = isAutoPay,
                PaymentCardDetail = new AccountPaymentCardDto(card),
                ServiceId = isAutoPay ? NetworkConstants.AutoPayServiceName : null
            };

            dto.PaymentCardDetail.CardType = "A";

            var info = await _api.AddOrReplaceCreditCardPut(dto.PaymentCardDetail.Token, dto);
            if (info != null)
            {
                Info = info;
            }
        }

        private static VehicleListRequestDto GetVehicleListDto(VehicleViewModel vehicle)
        {
            var dto = new AccountVehicleDto(vehicle);
            var vehicleList = new VehicleListRequestDto()
            {
                VehicleList = new List<AccountVehicleDto>()
                {
                    dto
                }
            };
            return vehicleList;
        }

        //public async Task<IReadOnlyList<ReceiptViewModel>> GetReceipts()
        //{
        //    var loggedInReceipts = (await _api.GetReceipts())?.FinancialTransactionList?.Select(r => new ReceiptViewModel(r))?.ToList() ??
        //            new List<ReceiptViewModel>();

        //    var anonymousPcnReceipts = _appState.Receipts.Where(r => r.IsPcn);

        //    return loggedInReceipts.Concat(anonymousPcnReceipts).OrderByDescending(r => r.Date).ToList();
        //}

        private AutoPayStatus GetAutoPayStatus()
        {
            var status = AutoPayStatusText;
            switch (status?.ToLowerInvariant())
            {
                case "active":
                    return AutoPayStatus.Active;

                case "closed":
                    return AutoPayStatus.Closed;

                case "pending":
                    return AutoPayStatus.Pending;

                case "suspended":
                    return AutoPayStatus.Suspended;

                case "pre-suspended":
                    return AutoPayStatus.PendingSuspension;

                default:
                    return AutoPayStatus.Unknown;
            }
        }

        private string GetAddress()
        {
            var address = Info?.Address;
            if (address == null)
                return string.Empty;

            var streetAndRest = $"{address.Street}\n{address.PostCode} {address.Town}\n{address.County}".TrimStart();

            // building number is included in street
            return !string.IsNullOrEmpty(address.BuildingNumber) && address.Street?.StartsWith(address.BuildingNumber) == true ?
                streetAndRest :
                $"{address.BuildingNumber} {streetAndRest}".TrimStart();
        }

        private AccountInformationDto Sanitize(AccountInformationDto info)
        {
            // remove all cards that appear twice in the list as 
            // preferred and auto pay cards
            if (info?.PaymentCardList != null)
            {
                var grouped = info.PaymentCardList
                                .Where(c => c.Token != null)
                                .GroupBy(c => c.Token).ToList();

                var remove = new List<AccountPaymentCardDto>();

                foreach (var group in grouped.Where(g => g.Any(c => c.IsAutoPayCard)))
                {
                    var autoPayCard = group.First(c => c.IsAutoPayCard);
                    remove.AddRange(group.Except(new[] { autoPayCard }));
                }
                info.PaymentCardList = info.PaymentCardList.Except(remove).ToList();
            }

            return info;
        }
    }
}
