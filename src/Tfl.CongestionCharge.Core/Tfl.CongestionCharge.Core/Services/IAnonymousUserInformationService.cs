﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Receipts;

namespace Tfl.CongestionCharge.Core.Services
{
    public interface IAnonymousUserInformationService : IUserInformationService
    {
        Task<IReadOnlyList<ReceiptViewModel>> GetReceipts();

        Task AddPaymentCard(PaymentCardViewModel card);

        Task RemovePaymentCard(PaymentCardViewModel vehicle);

        Task AddReceipt(StoredReceipt receipt);

        Task AddReceipts(List<StoredReceipt> receipts);
    }
}
