﻿namespace Tfl.CongestionCharge.Core.Services
{
    public enum AutoPayStatus
    {
        Unknown,
        Closed,
        Active,
        Suspended,
        Pending,
        PendingSuspension
    }
}
