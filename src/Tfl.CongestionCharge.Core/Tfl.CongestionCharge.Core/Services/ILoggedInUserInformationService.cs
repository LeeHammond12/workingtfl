﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Model.AccountInfo;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Services
{
    public interface ILoggedInUserInformationService : IUserInformationService
    {
        string AccountNumber { get; }
        string AccountType { get; }
        string Name { get; }
        string Address { get; }
        string Balance { get; }

        PaymentCardViewModel AutoPayPaymentCard { get; }

        IReadOnlyList<AccountTflServiceDto> ServiceList { get; }
        IReadOnlyList<VehicleViewModel> VehiclesActiveOnCcAutoPay { get; }
        IReadOnlyList<VehicleViewModel> VehiclesWithServices { get; }
        IReadOnlyList<VehicleViewModel> VehiclesWithoutServices { get; }

        AccountTflServiceDto AutoPayService { get; }

        bool HasAutoPay { get; }

        DateTime AutoPayStatementDate { get; }
        DateTime? LastAutoPayStatementDate { get; }
        AutoPayStatus AutoPayStatus { get; }
        string AutoPayStatusText { get; }

        Task<List<VehicleViewModel>> GetAutoPayVehicles();
        //Task<List<VehicleViewModel>> GetQmlVehicles();
        Task<List<VehicleViewModel>> AddAutoPayVehicle(VehicleViewModel vehicle);
        Task<List<VehicleViewModel>> RemoveAutoPayVehicle(string vrm);

        Task UpdateAutoPayPaymentCard(PaymentCardViewModel card, bool isAutoPay);

        Task<AccountInformationDto> RemovePaymentCard(PaymentCardViewModel card);
    }
}