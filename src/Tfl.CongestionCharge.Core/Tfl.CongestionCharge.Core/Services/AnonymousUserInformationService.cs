﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Receipts;

namespace Tfl.CongestionCharge.Core.Services
{
    public class AnonymousUserInformationService : IAnonymousUserInformationService
    {
        private readonly AppState _appState;

        public IReadOnlyList<VehicleViewModel> Vehicles => _appState.Vehicles;
        public IReadOnlyList<VehicleViewModel> VehiclesNotOnCcAutoPay => Vehicles;
        public IReadOnlyList<PaymentCardViewModel> PaymentCards => _appState.PaymentCards;
        public IReadOnlyList<ReceiptViewModel> Receipts => _appState.Receipts;

        public AnonymousUserInformationService(AppState appState)
        {
            _appState = appState;
        }
//Suppress warning CS1998: This async method lacks 'await'
#pragma warning disable 1998
        public async Task UpdateInformation(bool useCachedValue = false)
        {
        }

        public async Task AddVehicle(VehicleViewModel vehicle)
        {
            _appState.AddVehicle(vehicle);
        }

        public async Task RemoveVehicle(string vrm)
        {
            _appState.RemoveVehicle(vrm);
        }

        public async Task AddReceipt(StoredReceipt receipt)
        {
            _appState.AddReceipt(receipt);
        }

        public async Task AddReceipts(List<StoredReceipt> receipts)
        {
            _appState.AddReceipts(receipts);
        }

        public async Task<IReadOnlyList<ReceiptViewModel>> GetReceipts()
        {
            return _appState.Receipts;
        }

        public async Task AddPaymentCard(PaymentCardViewModel card)
        {
            _appState.AddPaymentCard(card);
        }

        public async Task RemovePaymentCard(PaymentCardViewModel card)
        {
            _appState.RemovePaymentCard(card);
        }
#pragma warning restore 1998
    }
}
