﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Tfl.CongestionCharge.Core.Logging;

namespace Tfl.CongestionCharge.Core.Helper
{
    public static class Dbg
    {
        [DebuggerStepThrough]
        public static void IsNotNull(object o, [CallerMemberName] string memberName = "")
        {
            if (o == null)
                BreakAndLog("Dbg.IsNotNull()", memberName);
        }

        [DebuggerStepThrough]
        public static void IsNull(object o, [CallerMemberName] string memberName = "")
        {
            if (o != null)
                BreakAndLog("Dbg.IsNull()", memberName);
        }

        [DebuggerStepThrough]
        public static void IsTrue(bool b, [CallerMemberName] string memberName = "")
        {
            if (!b)
                BreakAndLog("Dbg.IsTrue()", memberName);
        }

        [DebuggerStepThrough]
        public static void IsFalse(bool b, [CallerMemberName] string memberName = "")
        {
            if (b)
                BreakAndLog("Dbg.IsFalse()", memberName);
        }

        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public static void BreakIfAttached(string message = null, [CallerMemberName] string memberName = "")
        {
            if (Debugger.IsAttached)
                Debugger.Break();
        }

        [DebuggerStepThrough]
        public static void BreakAndLog(string reason = "", [CallerMemberName] string memberName = "")
        {
            Analytics.AddEvent(reason + " in " + memberName);
            if (Debugger.IsAttached)
                Debugger.Break();
        }

        [DebuggerStepThrough]
        public static void LogUnhandledException(Exception ex, string message = "")
        {
            Analytics.AddUnhandledExceptionEvent(ex, message);
        }

        [DebuggerStepThrough]
        public static void LogHandledException(Exception ex, [CallerMemberName] string memberName = "")
        {
            Analytics.AddHandledExceptionEvent(ex, memberName);
        }

        [DebuggerStepThrough]
        public static void LogEvent(string eventName, string key, string value, [CallerMemberName] string memberName = "")
        {
            Analytics.AddEvent(eventName, key, value);
        }

        [DebuggerStepThrough]
        public static void BreakAndLog(Exception ex, [CallerMemberName] string memberName = "")
        {
            Debug.WriteLine(GetMessage(GetExceptionString(ex, memberName), memberName));
            Analytics.AddHandledExceptionEvent(ex, memberName);

            if (Debugger.IsAttached)
                Debugger.Break();
        }

        [DebuggerStepThrough]
        private static string GetExceptionString(Exception ex, string memberName = null)
        {
            if (ex == null)
                return "Exception was null (shouldn't happen!)  in " + memberName;
            return ex.GetType() + ": " + ex.Message;
        }

        [DebuggerStepThrough]
        private static string GetMessage(string message, string memberName)
        {
            return (message ?? string.Empty) + " in " + memberName;
        }
    }
}