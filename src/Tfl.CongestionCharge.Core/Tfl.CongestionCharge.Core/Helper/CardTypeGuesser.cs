﻿namespace Tfl.CongestionCharge.Core.Helper
{
    public class CardTypeGuesser
    {
        public static string GuessCardType(string cardNumber)
        {
            cardNumber = CardUtils.GetStrippedNumber(cardNumber);

            if (string.IsNullOrWhiteSpace(cardNumber) || cardNumber.Length < 4)
                return string.Empty;

            var innString = cardNumber.Substring(0, 2);

            int inn;

            // http://stackoverflow.com/a/22034170/451540
            if (int.TryParse(innString, out inn))
            {
                if (inn >= 40 && inn <= 49)
                {
                    return CreditCardType.Visa.ToString();
                }
                if (inn >= 51 && inn <= 55)
                {
                    return CreditCardType.MasterCard.ToString();
                }
                if (inn >= 60 && inn <= 65)
                {
                    return CreditCardType.Discover.ToString();
                }
                if (inn >= 34 && inn <= 37)
                {
                    return CreditCardType.Amex.ToString();
                }
            }

            return string.Empty;
        }
    }
}
