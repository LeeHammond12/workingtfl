﻿using System.Text.RegularExpressions;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.Helper
{
    public enum CreditCardType
    {
        Visa,
        MasterCard,
        Discover,
        Amex,
        Switch,
        Solo
    }

    public static class CardUtils
    {
        private const string CardRegex = "^(?:(?<Visa>4[0-9]{12}(?:[0-9]{3})?)|" +
                                         "(?<MasterCard>5[1-5][0-9]{14})|" +
                                         "(?<Discover>6(?:011|5[0-9]{2})[0-9]{12})|" +
                                         "(?<Amex>3[47][0-9]{13})|" +
                                         "(?<DinersClub>3(?:0[0-5]|[68][0-9])?[0-9]{11}))$";

        public static string GetStrippedNumber(string num)
        {
            return num.Replace("-", string.Empty).Replace(" ", string.Empty);
        }

        public static string GetCardTypeFromNumber(string cardNum)
        {
            var cardTest = new Regex(CardRegex);
            var gc = cardTest.Match(cardNum).Groups;

            if (gc[CreditCardType.Amex.ToString()].Success)
            {
                return Strings.Amex;
            }
            if (gc[CreditCardType.MasterCard.ToString()].Success)
            {
                return Strings.MasterCard;
            }
            if (gc[CreditCardType.Visa.ToString()].Success)
            {
                return Strings.Visa;
            }
            if (gc[CreditCardType.Discover.ToString()].Success)
            {
                return Strings.Discover;
            }
            if (gc[CreditCardType.Solo.ToString()].Success)
            {
                return Strings.Solo;
            }
            if (gc[CreditCardType.Switch.ToString()].Success)
            {
                return Strings.Switch;
            }
            return string.Empty;
        }
    }
}
