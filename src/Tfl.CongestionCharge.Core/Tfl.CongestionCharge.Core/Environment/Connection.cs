﻿using Plugin.Connectivity;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class Connection : IConnection
    {
        public bool NetworkConnected()
        {
            return CrossConnectivity.Current.IsConnected;
        }
    }
}