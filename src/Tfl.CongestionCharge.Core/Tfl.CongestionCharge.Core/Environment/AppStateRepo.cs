﻿using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class AppStateRepo : Persist<AppStateModel>
    {
        protected override string Key => "AppStateModel_V1";

        public AppStateRepo(IKeyValueStore store) : base(store)
        {
        }
    }
}
