﻿using System.Threading.Tasks;

namespace Tfl.CongestionCharge.Core.Environment
{
    public interface IUserDialog
    {
        Task ShowError(string message);

        Task ShowUnknownError();

        Task ShowWarning(string message);

        Task ShowMessage(string message, string title);

        Task ShowOkMessageWithLink(string message, string title);

        Task<bool> Show2ButtonMessage(string message, string title, string buttonConfirmText, string buttonCancelText);
    }
}
