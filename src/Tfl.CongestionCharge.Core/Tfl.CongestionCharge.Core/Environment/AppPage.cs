﻿namespace Tfl.CongestionCharge.Core.Environment
{
    public enum AppPage
    {
        TermsAndConditionsBrowser,
        TermsAndConditions,
        LookupVrm,
        ChooseAVehicle,
        PcnLookup,
        PostCodeChecker,
        ChooseDates,
        SelectPaymentCard,
        Payment,
        ViewReceipt,
        AllDone,
        AutoPayCreditCard,
        YourVehiclesAndServices,
        AnonymousUserYourPaymentMethods,
        LoggedInUserYourPaymentMethods,
        JourneyDetails,
        YourAccount,
        ManageCcAutoPay,
        LoginPageKey,
        PayCcSelectVehicle,
        YourVehiclesAndCards,
        YourVehiclesAnonymousUser,
        AutoPayVehicles,
        ConfirmVrm,
        PrivacyPolicyBrowser
    }
}