﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using Tfl.CongestionCharge.Core.Helper;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Receipts;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class AppState : ObservableObject
    {
        private readonly IPersist<AppStateModel> _store;

        private AppStateModel Current { get; }

        public IReadOnlyList<VehicleViewModel> Vehicles => Current.Vehicles.Select(v => new VehicleViewModel(v)).ToList();

        public IReadOnlyList<PaymentCardViewModel> PaymentCards => Current.PaymentCards.Select(v => new PaymentCardViewModel(v)).ToList();

        public IReadOnlyList<ReceiptViewModel> Receipts => Current.Receipts.Select(r => new ReceiptViewModel(r)).ToList();

        public AuthStateModel Auth => Current.Auth;

        public bool UserHasReceipts => Current.Receipts.Count > 0;

        public int AskedToRateCounter
        {
            get { return Current.AskedToRateCounter; }
            set
            {
                Current.AskedToRateCounter = value;
                Save();
            }
        }

        public int StartupCounterForThisVersion
        {
            get { return Current.StartupCounterForThisVersion; }
            set
            {
                Current.StartupCounterForThisVersion = value;
                Save();
            }
        }

        public bool UserHasRatedApp
        {
            get { return Current.UserHasRatedApp; }
            set
            {
                Current.UserHasRatedApp = value;
                Save();
            }
        }

        public bool UserHasAcceptedTermsAndConditions
        {
            get { return Current.UserHasAcceptedTermsAndConditions; }
            set
            {
                Current.UserHasAcceptedTermsAndConditions = value;
                Save();
            }
        }

        public string SmsNumber
        {
            get { return Current.SmsNumber; }
            set
            {
                Current.SmsNumber = value;
                Save();
            }
        }

        public AppState(IPersist<AppStateModel> store, string appVersion)
        {
            _store = store;
            Current = GetCurrent(appVersion);
        }

        public void AddVehicle(VehicleViewModel vehicle)
        {
            var existing = GetStoredVehicle(vehicle.Vrm);
            if (existing == null)
            {
                Current.Vehicles.Add(new StoredVehicle(vehicle));
                Save();
            }
        }

        public void RemoveVehicle(string vrm)
        {
            var vehicle = GetStoredVehicle(vrm);
            if (vehicle != null)
            {
                Current.Vehicles.Remove(vehicle);
                Save();
            }
        }

        public void AddReceipt(StoredReceipt receipt)
        {
            Current.Receipts.Add(receipt);
            Save();
        }

        public void AddReceipts(List<StoredReceipt> receipts)
        {
            Current.Receipts.AddRange(receipts);
            Save();
        }

        public void AddPaymentCard(PaymentCardViewModel card)
        {
            var existing = GetStoredCard(card.Last4Digits, card.ExpiryDate);
            if (existing == null)
            {
                Current.PaymentCards.Add(new StoredCard(card));
                Save();
            }
        }

        public void RemovePaymentCard(PaymentCardViewModel card)
        {
            var storedCard = GetStoredCard(card.Last4Digits, card.ExpiryDate);
            if (storedCard != null)
            {
                Current.PaymentCards.Remove(storedCard);
                Save();
            }
        }

        public StoredCard GetStoredCard(string last4Digits, string expiry)
        {
            return Current.PaymentCards.FirstOrDefault(c => c.Last4Digits == last4Digits && c.ExpiryDate == expiry);
        }

        private StoredVehicle GetStoredVehicle(string vrm)
        {
            return Current.Vehicles.FirstOrDefault(v => v.Vrm == vrm);
        }

        private AppStateModel GetCurrent(string appVersion)
        {
            AppStateModel appStateModel = null;
            try
            {
                if (_store.IsStored())
                {
                    appStateModel = _store.Restore();
                }
            }
            catch (Exception ex)
            {
                Dbg.BreakAndLog(ex);
            }
            finally
            {
                if (appStateModel == null)
                {
                    appStateModel = new AppStateModel
                    {
                        CurrentVersion = appVersion,
                    };
                }
            }

            // new app version, reset the counters
            if (appVersion != appStateModel.CurrentVersion)
            {
                appStateModel.AskedToRateCounter = 0;
                appStateModel.StartupCounterForThisVersion = 0;
                appStateModel.UserHasRatedApp = false;
                appStateModel.CurrentVersion = appVersion;
            }

            return appStateModel;
        }

        public void Save()
        {
            lock (Current)
            {
                _store.Backup(Current);
            }
        }
    }
}