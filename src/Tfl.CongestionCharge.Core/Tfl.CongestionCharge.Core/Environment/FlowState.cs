﻿using System;
using Tfl.CongestionCharge.Core.Helper;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class FlowState
    {
        private readonly IPersist<FlowStatesModel> _store;

        private FlowStatesModel Current { get; }

        public PaymentFlowState PaymentFlowState
        {
            get { return Current.PaymentFlowState; }
            set { Current.PaymentFlowState = value; }
        }

        public LookupVrmFlowState LookupVrmFlowState
        {
            get { return Current.LookupVrmFlowState; }
            set { Current.LookupVrmFlowState = value; }
        }

        public FlowState(IPersist<FlowStatesModel> store)
        {
            _store = store;
            Current = GetCurrent();
        }

        public void StartLookupVrmFlow(LookupVrmFlowState.FlowType flowType)
        {
            LookupVrmFlowState = new LookupVrmFlowState()
            {
                Type = flowType
            };
        }

        public void FinishLookupVrmFlow()
        {
            LookupVrmFlowState = null;
        }

        public PaymentFlowState StartPcnPaymentFlow()
        {
            return PaymentFlowState = new PaymentFlowState()
            {
                Inner = new PcnState()
            };
        }

        public PaymentFlowState StartCcPaymentFlow()
        {
            return PaymentFlowState = new PaymentFlowState()
            {
                Inner = new CcState()
            };
        }

        public void FinishPaymentFlow()
        {
            PaymentFlowState = null;
        }

        private FlowStatesModel GetCurrent()
        {
            FlowStatesModel flowStatesModel = null;

            try
            {
                if (_store.IsStored())
                {
                    flowStatesModel = _store.Restore();
                }
            }
            catch (Exception ex)
            {
                Dbg.BreakAndLog(ex);
            }
            finally
            {
                if (flowStatesModel == null)
                {
                    flowStatesModel = new FlowStatesModel();
                }
            }

            return flowStatesModel;
        }

        public void Save()
        {
            _store.Backup(Current);
        }
    }
}
