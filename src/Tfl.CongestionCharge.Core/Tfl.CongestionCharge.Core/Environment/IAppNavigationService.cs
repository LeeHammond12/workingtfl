﻿namespace Tfl.CongestionCharge.Core.Environment
{
    public interface IAppNavigationService
    {
        void NavigateTo(AppPage page, object parameter);

        void NavigateHomeAndClearBackStack();

        void NavigateToLoginAndClearBackStack();

        void GoBackOnePage();

        void NavigateToAppDownloadPage();

        void NavigateToAppStoreReview();
    }
}
