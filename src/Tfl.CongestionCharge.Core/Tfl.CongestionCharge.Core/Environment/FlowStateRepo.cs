﻿using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class FlowStateRepo : Persist<FlowStatesModel>
    {
        protected override string Key => "FlowsStateModel_V1";

        public FlowStateRepo(IKeyValueStore store) : base(store)
        {
        }
    }
}
