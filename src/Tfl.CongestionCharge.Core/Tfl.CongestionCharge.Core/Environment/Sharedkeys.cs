﻿namespace Tfl.CongestionCharge.Core.Environment
{
    public static class Sharedkeys
    {
        public static readonly string PclCryptoKey = "PCLCryptoKey";

        public static readonly string LaunchKey = "TflLaunchKey";

        public class AnalyticsAppKeys
        {
#if USE_SIT_API
            public static string IosApplicationKey = "e3e8ad59-9f12-422a-b14c-4fc8ec1d2623";
            public static string AndroidApplicationKey = "f57caa93-98f2-4dba-8525-fe7f698b992b";
#elif USE_UAT_API
            public static string IosApplicationKey = "73346335-25bc-40e6-9179-2f08be1b4dbc";
            public static string AndroidApplicationKey = "bd6b1067-4c92-40e5-a353-220209ddb3b6";
#elif USE_PRODUCTION_API
            public static string IosApplicationKey = "07b99885-0427-46f5-8901-46ad9ec29e26";
            public static string AndroidApplicationKey = "8e8a14de-8be7-4348-af65-3ce22f9c15a6";
#else
            public static string IosApplicationKey = "529da8a9-6d83-463a-a888-9a5ba2567741";
            public static string AndroidApplicationKey = "23c72161-3fd1-483a-ab56-c683e8ce3740";
#endif
        }

        public class CpsFundCodes
        {
            public static string BusinessOperations = "MOB2";

            public static string EnforcementOperations = "MOB1";
        }
    }
}