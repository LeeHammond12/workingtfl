﻿using G2g3digital.Payments.Axis;
using G2g3digital.Payments.Axis.Model;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class AxisConfiguration : IAxisConfiguration
    {
        public subjectType SubjectType => subjectType.CapitaSite;

        public int Identifier => 453;

        public int HmacKeyId => 1;

        public ushort SiteId => 453;

        public systemCode SystemCode => systemCode.API;

#if USE_PRODUCTION_API
        public string PaymentUri => "https://sbs.e-paycapita.com/CommonPayments/CommonPayments.asmx";
        public string HmacKey => "8E2NzgdhAZUO8DnDg2nJx41/1252umjwkPGm72YrFhQQv0JhCt9fnP2gLFRjGjDgqKzfEBmRjb5OCUMoXOt7ug==";
#else
        public string PaymentUri => "https://sbsctest.e-paycapita.com/CommonPayments/CommonPayments.asmx";
        public string HmacKey => "rncZpPVoECyw3ZVBKC9CORalmb6XjE9EEbQjTH+3TZKryiFM7kAfEs0SUJly7SCuGQn2G3pNHWkpAh5GWLffoA==";
#endif
    }
}