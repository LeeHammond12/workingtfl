﻿using System;
using Tfl.CongestionCharge.Core.Model.Auth;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class AuthStateModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public bool RefreshingToken { get; set; }

        public DateTime ExpiresAt { get; set; }
        public bool HasAccessTokenExpired => IsLoggedIn && DateTime.Now > ExpiresAt;

        public bool IsLoggedIn => !string.IsNullOrWhiteSpace(AccessToken);

        public AuthStateModel()
        {
        }

        public AuthStateModel(AuthResponseDto dto)
        {
            AccessToken = dto.AccessToken;
            RefreshToken = dto.RefreshToken;
            ExpiresAt = DateTime.Now.AddSeconds(dto.ExpiresIn - NetworkConstants.AuthRefreshToleranceInSeconds);
            RefreshingToken = false;
        }

        public void Update(AuthStateModel m)
        {
            AccessToken = m.AccessToken;
            RefreshToken = m.RefreshToken;
            ExpiresAt = m.ExpiresAt;
            RefreshingToken = m.RefreshingToken;
        }

        public void Logout()
        {
            AccessToken = null;
            ExpiresAt = DateTime.MinValue;
            RefreshingToken = false;
        }
    }
}