﻿using System.Net.Http;

namespace Tfl.CongestionCharge.Core.Environment
{
    public interface IHttpClientFactory
    {
        HttpClient GetClient(IAuthService authService);
    }
}