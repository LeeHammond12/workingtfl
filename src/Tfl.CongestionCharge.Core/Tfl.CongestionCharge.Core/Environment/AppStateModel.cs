﻿using System.Collections.Generic;
using GalaSoft.MvvmLight;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class AppStateModel : ObservableObject
    {
        public AuthStateModel Auth { get; set; }

        public int AskedToRateCounter { get; set; }

        public int StartupCounterForThisVersion { get; set; }

        public string CurrentVersion { get; set; }

        public bool UserHasRatedApp { get; set; }

        public bool UserHasAcceptedTermsAndConditions { get; set; }

        public List<StoredVehicle> Vehicles { get; } = new List<StoredVehicle>();

        public List<StoredCard> PaymentCards { get; } = new List<StoredCard>();

        public List<StoredReceipt> Receipts { get; } = new List<StoredReceipt>();

        public string SmsNumber { get; set; }

        public AppStateModel()
        {
            Auth = new AuthStateModel();
        }
    }
}