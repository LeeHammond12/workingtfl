﻿using Ninject.Modules;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class NinjectRuntimeModule : NinjectModule
    {
        public override void Load()
        {
        }
    }
}