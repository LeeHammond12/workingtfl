﻿using System;
using G2g3digital.Payments.Axis;
using Ninject;
using Refit;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Api.Axis;
using Tfl.CongestionCharge.Core.Helper;
using Tfl.CongestionCharge.Core.Services;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class AppEnvironment
    {
        public static AppEnvironment Current { get; private set; }

        private IKernel Kernel { get; }

        public AppState AppState { get; }

        public FlowState FlowState { get; }

        public IAuthService AuthService { get; }

        public IAppNavigationService NavigationService { get; }

        public IUserInformationService UserInformation { get; private set; }

        public IUserDialog DialogService { get; }

        public IClientApiExceptionHandler ClientApiExceptionHandler { get; }

        public ITflApi TflApi { get; set; }

        public PaymentService PaymentService { get; }

        private AppEnvironment(
            IAppNavigationService navigationService,
            IUserDialog dialogService,
            AppState appState,
            FlowState flowState,
            IHttpClientFactory clientFactory)
        {
            Kernel = new StandardKernel(new NinjectRuntimeModule());
            Kernel.Bind<AppEnvironment>().ToConstant(this);

            AppState = appState;
            FlowState = flowState;

            AuthService = new AuthService(this);

            NavigationService = navigationService;
            DialogService = dialogService;
            ClientApiExceptionHandler = new ClientApiExceptionHandler(DialogService, NavigationService);

            var tflApi = GetTflApiService(clientFactory, AuthService, new Connection());

            var axisApi = GetAxisApiService(new Connection());
            PaymentService = new PaymentService(axisApi, tflApi, AppState);

            TflApi = tflApi;

            UpdateUserLoginState();
        }

        private AppEnvironment(
            IAppNavigationService navigationService,
            IUserDialog dialogService,
            AppState appState,
            FlowState flowState,
            IAuthService authService)
        {
            AppState = appState;
            FlowState = flowState;
            NavigationService = navigationService;
            DialogService = dialogService;
            AuthService = authService;

            ClientApiExceptionHandler = new ClientApiExceptionHandler(dialogService, navigationService);

            UpdateUserLoginState();
        }

        public void UpdateUserLoginState()
        {
            if (AuthService.IsLoggedIn)
            {
                UserInformation = new LoggedInUserInformationService(TflApi);
            }
            else
            {
                UserInformation = new AnonymousUserInformationService(AppState);
            }
        }

        public static void Init(
            string appVersion,
            IAppNavigationService navigationService,
            IUserDialog dialogService,
            IHttpClientFactory clientFactory,
            IKeyValueStore keyValueStore)
        {
            if (Current != null)
            {
                Dbg.BreakAndLog();
                return;
            }

            var appStateStore = new AppStateRepo(keyValueStore);
            var flowStateStore = new FlowStateRepo(keyValueStore);

            var appState = new AppState(appStateStore, appVersion);
            var flowState = new FlowState(flowStateStore);

            Current = new AppEnvironment(navigationService, dialogService, appState, flowState, clientFactory);
        }

        public static void UnitTestInit(
            IAppNavigationService navigationService,
            IUserDialog dialogService,
            IPersist<AppStateModel> appStateStore,
            IPersist<FlowStatesModel> flowStateStore,
            IAuthService authService)
        {
            var appState = new AppState(appStateStore, "1");
            var flowState = new FlowState(flowStateStore);
            Current = new AppEnvironment(navigationService, dialogService, appState, flowState, authService);
        }

        private static IAxisPaymentService GetAxisApiService(IConnection connection)
        {
            return new AxisPaymentServiceRetryPolicyDecorator(
                new AxisPaymentServiceNetworkAvailabilityDecorator(
                    new AxisPaymentService(new AxisConfiguration()), connection));
        }

        private static TflApiResponseHandler GetTflApiService(
            IHttpClientFactory clientFactory,
            IAuthService authService,
            IConnection connection)
        {
            var client = clientFactory.GetClient(authService);

            client.BaseAddress = new Uri(NetworkConstants.BaseUri);

            var retry = new RetryDecorator(
                new TflApiExceptionDecorator(
                    new NetworkCheckerDecorator(connection)));

            var tflApi = new TflApiResponseHandler(retry, RestService.For<ITflApi>(client));
            return tflApi;
        }

        public T GetViewModel<T>() where T : TflViewModelBase
        {
            return Kernel.Get<T>();
        }
    }
}