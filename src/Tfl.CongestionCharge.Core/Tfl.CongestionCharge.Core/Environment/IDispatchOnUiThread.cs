﻿using System;
using System.Threading.Tasks;

namespace Tfl.CongestionCharge.Core.Environment
{
    public interface IDispatchOnUiThread
    {
        void Invoke(Action action);

        void Invoke(Task<Action> action);
    }
}