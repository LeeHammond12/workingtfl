﻿namespace Tfl.CongestionCharge.Core.Environment
{
    public interface IConnection
    {
        bool NetworkConnected();
    }
}
