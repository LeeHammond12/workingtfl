﻿using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Model.Auth;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class AuthService : IAuthService
    {
        private readonly AppEnvironment _appEnvironment;
        private AppState AppState => _appEnvironment.AppState;
        private AuthStateModel Auth => AppState.Auth;

        public bool IsLoggedIn => Auth?.IsLoggedIn == true;

        public AuthService(AppEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
        }

        public void Logout()
        {
            Auth?.Logout();

            //AppState.UserHasReceipts = false;
            AppState.Save();

            _appEnvironment.UpdateUserLoginState();
        }

        public async Task Login(string accountNumber, string password)
        {
            var auth = await _appEnvironment.TflApi.GetAuthToken(
                new AuthRequestDto
                {
                    UserName = accountNumber,
                    Password = password
                });

            if (auth != null)
            {
                Auth.Update(new AuthStateModel(auth));
                AppState.Save();

                //AppState.UserHasReceipts = false;
                _appEnvironment.UpdateUserLoginState();
            }
        }

        public async Task RefreshToken()
        {
            var response = await _appEnvironment.TflApi.GetRefreshToken(
                new AuthTokenRefreshDto
                {
                    RefreshToken = Auth.RefreshToken
                });

            Auth.Update(new AuthStateModel(response));
            AppState.Save();
        }
    }
}
