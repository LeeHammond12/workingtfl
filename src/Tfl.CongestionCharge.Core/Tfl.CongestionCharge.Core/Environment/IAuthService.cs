﻿using System.Threading.Tasks;

namespace Tfl.CongestionCharge.Core.Environment
{
    public interface IAuthService
    {
        bool IsLoggedIn { get; }
        void Logout();
        Task Login(string accountNumber, string password);
        Task RefreshToken();
    }
}