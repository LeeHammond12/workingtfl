﻿using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.Environment
{
    public class FlowStatesModel
    {
        public PaymentFlowState PaymentFlowState { get; set; }

        public LookupVrmFlowState LookupVrmFlowState { get; set; }
    }
}
