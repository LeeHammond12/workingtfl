﻿using System.Threading.Tasks;

namespace Tfl.CongestionCharge.Core.Api
{
    public interface IClientApiExceptionHandler
    {
        bool ContainsErrorCode(ClientApiException exception, string errorCode);

        Task HandleErrors(ClientApiException exception);

        /// <summary>
        /// checks if the exception contains any application
        /// level error codes (see ApiErrorCodeLookup.cs) that should be handled.
        /// </summary>
        /// <param name="exception">the exception</param>
        /// <returns>true if an error message was shown</returns>
        Task<bool> HandleApplicationErrors(ClientApiException exception);

        /// <summary>
        /// checks if the exception contains api key was invalid or missing.
        /// If so, it asks the user to navigate to the app store.
        /// </summary>
        /// <param name="exception">the exception</param>
        /// <returns>true if the api key was invalid</returns>
        Task<bool> HandleInvalidApiKey(ClientApiException exception);
    }
}