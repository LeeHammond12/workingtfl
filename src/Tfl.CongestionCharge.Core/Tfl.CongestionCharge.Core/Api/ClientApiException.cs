﻿using System;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Api
{
    public class ClientApiException : Exception
    {
        public ClientApiResponseDtoBase Response { get; }

        public bool IsAuthenticationException => this.GetInnerMostExceptionOfType<AuthenticationException>() != null;
        public bool IsNoNetworkException => this.GetInnerMostExceptionOfType<NoNetworkConnectionException>() != null;

        public bool ShouldRetry => Response == null && !IsAuthenticationException;

        public ClientApiException(ClientApiResponseDtoBase response)
        {
            Response = response;
        }

        public ClientApiException(ClientApiException exception)
            : base(exception?.Message, exception?.InnerException)
        {
            Response = exception?.Response;
            Source = exception?.Source;
        }

        public ClientApiException(Exception innerException)
            : base(string.Empty, innerException)
        {
        }
    }
}