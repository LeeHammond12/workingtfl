﻿using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Refit;
using Tfl.CongestionCharge.Core.Model.Auth;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Api
{
    /// <summary>
    /// makes sure all exceptions thrown by api calls are of type ClientApiException.
    /// </summary>
    public class TflApiExceptionDecorator : IRequestExecuter
    {
        private readonly IRequestExecuter _decroatee;

        public TflApiExceptionDecorator(IRequestExecuter decroatee)
        {
            _decroatee = decroatee;
        }

        public async Task<T> ExecuteRequest<T>(Func<Task<T>> api) where T : ClientApiResponseDtoBase
        {
            try
            {
                var response = await _decroatee.ExecuteRequest(api);
                ThrowOnError(response);

                return response;
            }
            // this is thrown on a bad status code from refits perspective
            catch (ApiException ex1)
            {
                // these are actually good status codes from tfl servers perspective,
                // they are used to signal application errors 
                if (ex1.StatusCode == HttpStatusCode.InternalServerError ||
                    ex1.StatusCode == HttpStatusCode.BadRequest)
                {
                    // try to decode the message
                    T response;
                    try
                    {
                        response = JsonConvert.DeserializeObject<T>(ex1.Content);
                    }
                    catch (Exception deserializeException)
                    {
                        throw new ClientApiException(deserializeException);
                    }

                    ThrowOnError(response);

                    // 400 is returned on a login failure.
                    // We're here when there was a bad status code, but
                    // the server response message actually did not contain any errors
                    if (ex1.StatusCode == HttpStatusCode.BadRequest && 
                        typeof(T) == typeof(AuthResponseDto))
                    {
                        throw new ClientApiException(new AuthenticationException());
                    }

                    return response;
                }

                if (ex1.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new ClientApiException(new AuthenticationException());
                }

                throw new ClientApiException(ex1);
            }
            // need to rethrow here because the response.HasErrors check might throw
            catch (ClientApiException)
            {
                throw;
            }
            // thrown when an exception in AuthenticatedHttpClientHandler is thrown
            catch (Exception ex2)
            {
                throw new ClientApiException(ex2);
            }
        }

        private static void ThrowOnError<T>(T response) where T : ClientApiResponseDtoBase
        {
            if (response?.HasErrors == true)
            {
                throw new ClientApiException(response);
            }
        }
    }
}