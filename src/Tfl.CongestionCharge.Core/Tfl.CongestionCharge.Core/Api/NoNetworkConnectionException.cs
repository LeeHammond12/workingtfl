﻿using System;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.Api
{
    public class NoNetworkConnectionException : Exception
    {
        public NoNetworkConnectionException()
            : base(Strings.DeviceHasNoNetworkConnection)
        {
        }
    }
}