﻿using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.Api
{
    public class ApplicationMessage
    {
        public string Title { get; } = Strings.ErrorTitle;

        public string Message { get; }

        public ApplicationMessage(string message)
        {
            Message = message;
        }

        public ApplicationMessage(string message, string title)
            : this(message)
        {
            Title = title;
        }
    }
}