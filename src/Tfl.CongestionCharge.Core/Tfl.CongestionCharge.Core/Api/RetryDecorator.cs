﻿using System;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Api
{
    public class RetryDecorator : IRequestExecuter
    {
        private const int RetryCount = 5;
        private const int BackOffPower = 2;

        private readonly IRequestExecuter _requestExecuter;

        public RetryDecorator(IRequestExecuter requestExecuter)
        {
            _requestExecuter = requestExecuter;
        }

        public async Task<T> ExecuteRequest<T>(Func<Task<T>> api) where T : ClientApiResponseDtoBase
        {
            int attempt = 1;
            do
            {
                try
                {
                    return await _requestExecuter.ExecuteRequest(api);
                }
                catch (ClientApiException ex)
                {
                    if (attempt >= RetryCount || !ex.ShouldRetry)
                    {
                        throw;
                    }

                    attempt++;

                    await Task.Delay(TimeSpan.FromSeconds(Math.Pow(BackOffPower, attempt)));
                }
            }
            while (true);
        }
    }
}