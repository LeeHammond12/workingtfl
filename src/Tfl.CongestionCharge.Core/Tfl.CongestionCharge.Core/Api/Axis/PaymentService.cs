﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using G2g3digital.Payments.Axis;
using Tfl.CongestionCharge.Core.Api.Axis.CPS;
using Tfl.CongestionCharge.Core.Api.Axis.TfL;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model.Payments;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.Api.Axis
{
    public class PaymentService
    {
        private readonly StoreCardRequestHandler _storeCard;
        private readonly AuthoriseTransactionRequestHandler _authorize;
        private readonly NotifyTfLRequestHandler _notifyTfl;
        private readonly CommitTransactionRequestHandler _commit;

        public PaymentService(IAxisPaymentService paymentService, ITflApi tflApi, AppState appState)
        {
            _storeCard = new StoreCardRequestHandler(paymentService, appState);
            _authorize = new AuthoriseTransactionRequestHandler(paymentService);
            _notifyTfl = new NotifyTfLRequestHandler(tflApi);
            _commit = new CommitTransactionRequestHandler(paymentService);
        }

        public async Task MakePayment(PaymentRequestDto payment, PaymentFlowState pcnOrCcPayment, bool storeCard)
        {
            try
            {
                if (storeCard)
                {
                    await _storeCard.SaveCardDetails(payment);
                }

                await _authorize.AuthorizeTransaction(payment, pcnOrCcPayment.ChargeType);

                await _notifyTfl.NotifyTfl(payment, pcnOrCcPayment);

                await _commit.CommitTransaction(payment);
            }
            catch (PaymentApiException ex1)
            {
                Debug.WriteLine(ex1);
                throw;
            }
            catch (Exception ex)
            {
                throw new PaymentApiException(ex);
            }
        }
    }
}
