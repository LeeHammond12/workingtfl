﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using G2g3digital.Payments.Axis;
using G2g3digital.Payments.Axis.Model;
using Tfl.CongestionCharge.Core.Environment;

namespace Tfl.CongestionCharge.Core.Api.Axis
{
    /// <summary>
    /// checks the network before every api call. Throws exception if no network.
    /// </summary>
    public class AxisPaymentServiceNetworkAvailabilityDecorator : IAxisPaymentService
    {
        private readonly IAxisPaymentService _decoratee;
        private readonly IConnection _connection;

        public AxisPaymentServiceNetworkAvailabilityDecorator(IAxisPaymentService decoratee, IConnection connection)
        {
            _decoratee = decoratee;
            _connection = connection;
        }

        public async Task<storeCardRes> StoreCardAsync(DateTime transactionDate, string cardNumber, string expiryDate, string cardHolderName)
        {
            return await ExecuteWithErrorHandling(
                async () => await _decoratee.StoreCardAsync(transactionDate, cardNumber, expiryDate, cardHolderName).ConfigureAwait(false));
        }

        public async Task<storeCardRes> StoreCardDeleteAsync(DateTime transactionDate, string lastFourDigits, string templateNo)
        {
            return await ExecuteWithErrorHandling(
                async () => await _decoratee.StoreCardDeleteAsync(transactionDate, lastFourDigits, templateNo).ConfigureAwait(false));
        }

        public async Task<paymentRes> PaymentAuthorizeAsync(DateTime transactionDate, decimal amount, cardTranType cardTranType, string lastFourDigits, string cardSecurityCode, string templateNo, string merchantCode)
        {
            return await ExecuteWithErrorHandling(
                async () => await _decoratee.PaymentAuthorizeAsync(transactionDate, amount, cardTranType, lastFourDigits, cardSecurityCode, templateNo, merchantCode).ConfigureAwait(false));
        }

        public async Task<paymentRes> PaymentAuthorizeAsync(
            DateTime transactionDate,
            decimal amount,
            cardTranType cardTranType,
            string cardHolderName,
            string cardNumber,
            string cardSecurityCode,
            string expiryDate,
            string merchantCode)
        {
            return await ExecuteWithErrorHandling(
                async () => await _decoratee.PaymentAuthorizeAsync(transactionDate, amount, cardTranType, cardHolderName, cardNumber, cardSecurityCode, expiryDate, merchantCode));
        }

        public async Task<paymentRes> PaymentCommitAsync(DateTime transactionDate, string authTranId, DateTime authTransactionDate, ICollection<item> items)
        {
            return await ExecuteWithErrorHandling(
                async () => await _decoratee.PaymentCommitAsync(transactionDate, authTranId, authTransactionDate, items).ConfigureAwait(false));
        }

        public async Task<paymentRes> PaymentCommitAsync(DateTime transactionDate, string authTranId, DateTime authTransactionDate, string fundCode,
            string reference, decimal amount)
        {
            return await ExecuteWithErrorHandling(
                async () => await _decoratee.PaymentCommitAsync(transactionDate, authTranId, authTransactionDate, fundCode, reference, amount).ConfigureAwait(false));
        }

        private async Task<T> ExecuteWithErrorHandling<T>(Func<Task<T>> action)
        {
            if (!_connection.NetworkConnected())
            {
                throw new NoNetworkConnectionException();
            }

            var result = await action.Invoke().ConfigureAwait(false);
            return result;
        }
    }
}