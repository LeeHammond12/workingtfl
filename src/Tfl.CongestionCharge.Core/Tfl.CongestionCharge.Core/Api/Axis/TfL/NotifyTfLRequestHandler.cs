﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.PayCc;
using Tfl.CongestionCharge.Core.Model.Payments;
using Tfl.CongestionCharge.Core.Model.PayPcn;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.Api.Axis.TfL
{
    public class NotifyTfLRequestHandler
    {
        private readonly ITflApi _tflApi;

        public NotifyTfLRequestHandler(ITflApi tflApi)
        {
            _tflApi = tflApi;
        }

        public async Task NotifyTfl(PaymentRequestDto command, PaymentFlowState pcnOrCcPayment)
        {
            if (pcnOrCcPayment.ChargeType == ChargeType.CongestionCharge)
            {
                command.FundCode = Sharedkeys.CpsFundCodes.BusinessOperations;
                await RecordCcTransaction(command, pcnOrCcPayment);
            }
            else
            {
                command.FundCode = Sharedkeys.CpsFundCodes.EnforcementOperations;
                await RecordPcnTransaction(command, pcnOrCcPayment);
            }
        }

        private async Task RecordCcTransaction(PaymentRequestDto command, PaymentFlowState ccPayment)
        {
            var ccPaymentList = new List<CcPaymentDetail>();
            foreach (var journey in ccPayment.Cc.Journeys)
            {
                var detail = new CcPaymentDetail
                {
                    Amount = journey.Price,
                    StartDate = journey.ChargeStartDate.ToSitDateFormat(),
                    EndDate = journey.ChargeEndDate.ToSitDateFormat(),
                    PeriodType = journey.PeriodType.ToString(),
                    Vehicle = new CcVehicle
                    {
                        VRM = journey.Vehicle.Vrm
                    }
                };
                ccPaymentList.Add(detail);
            }

            var payment = new CcPayment
            {
                AccountNumber = command.AccountNumber,
                TotalAmount = ccPayment.TotalPrice,
                PaymentAuthDetail = new CcPaymentAuthDetail
                {
                    PaymentTransactionDate = command.TransactionDate.ToSitDateFormat(),
                    Last4Digits = command.CardNumberLastFourDigits,
                    MachineCode = "00453",
                    PaymentTransactionId = command.UniqueTransactionId,
                },
                CcPaymentList = ccPaymentList,
                MobileTelNumber = command.MobileNumber
            };

            var receipt = await _tflApi.PayCc(payment);
            if (receipt.HasErrors)
            {
                throw new PaymentApiException(new ClientApiException(receipt));
            }

            command.ReceiptId = receipt.ReceiptId;
        }

        private async Task RecordPcnTransaction(PaymentRequestDto command, PaymentFlowState pcnPayment)
        {
            var payment = new PcnPaymentDto
            {
                PaymentAmount = pcnPayment.TotalPrice,
                PaymentDateTime = command.TransactionDate.ToSitDateFormat(),
                PaymentTransactionRef = command.UniqueTransactionId
            };
            var receipt = await _tflApi.PayPcn(pcnPayment.Pcn.PcnNumber, payment);
            // cancel the payment process if there were any errors
            if (receipt.HasErrors)
            {
                throw new PaymentApiException(new ClientApiException(receipt));
            }

            command.ReceiptId = receipt.ReceiptId;
        }
    }
}