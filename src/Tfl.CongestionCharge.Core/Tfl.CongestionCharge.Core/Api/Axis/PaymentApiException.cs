﻿using System;
using Tfl.CongestionCharge.Core.Extensions;

namespace Tfl.CongestionCharge.Core.Api.Axis
{
    public class PaymentApiException : Exception
    {
        public PaymentApiException(Exception innerException)
            : base(string.Empty, innerException)
        {
        }

        public bool IsNoNetworkConnectedException => this.GetInnerMostExceptionOfType<NoNetworkConnectionException>() != null;

        public ClientApiException InnerClientApiException => this.GetInnerMostExceptionOfType<ClientApiException>();

        public AxisPaymentApiException InnerAxisPaymentApiException => this.GetInnerMostExceptionOfType<AxisPaymentApiException>();
    }
}