﻿namespace Tfl.CongestionCharge.Core.Api.Axis
{
    public enum AxisPaymentExceptionType
    {
        Declined,
        InvalidMerchant,
        CallAuthCenter,
        RequestInvalid,
        DeferralX,
        ReferralB,
        CscFail,
        OtherError,
        StoreCardFailed,
        CallFailedWithNonZeroErrorCode,
        ReferralX
    }
}