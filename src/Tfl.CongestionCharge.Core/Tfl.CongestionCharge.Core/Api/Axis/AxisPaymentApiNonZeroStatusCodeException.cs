﻿using G2g3digital.Payments.Axis.Model;

namespace Tfl.CongestionCharge.Core.Api.Axis
{
    public class AxisPaymentApiNonZeroStatusCodeException : AxisPaymentApiException
    {
        public AxisPaymentApiNonZeroStatusCodeException(headerResponse header)
            : base(AxisPaymentExceptionType.CallFailedWithNonZeroErrorCode)
        {
            Header = header;
        }

        public headerResponse Header { get; set; }
    }
}