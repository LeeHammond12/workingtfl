﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using G2g3digital.Payments.Axis;
using G2g3digital.Payments.Axis.Model;

namespace Tfl.CongestionCharge.Core.Api.Axis
{
    /// <summary>
    /// RetryPolicyHttpClientDecorator will try connecting to our service five times, with an exponential wait of 2, 4, 8, 16, and 32 seconds between tries.
    /// This should give the device a chance to reestablish its network connection and continue the request to the api.
    /// The retry _only_ happens on a NoNetworkConnectionException.
    /// </summary>
    public class AxisPaymentServiceRetryPolicyDecorator : IAxisPaymentService
    {
        private const int RetryCount = 5;
        private const int BackOffPower = 2;

        private readonly IAxisPaymentService _decoratee;

        public AxisPaymentServiceRetryPolicyDecorator(IAxisPaymentService decoratee)
        {
            _decoratee = decoratee;
        }

        public async Task<storeCardRes> StoreCardAsync(DateTime transactionDate, string cardNumber, string expiryDate, string cardHolderName)
        {
            return await ExecuteWithRetry(
                async () => await _decoratee.StoreCardAsync(transactionDate, cardNumber, expiryDate, cardHolderName));
        }

        public async Task<storeCardRes> StoreCardDeleteAsync(DateTime transactionDate, string lastFourDigits, string templateNo)
        {
            return await ExecuteWithRetry(
                async () => await _decoratee.StoreCardDeleteAsync(transactionDate, lastFourDigits, templateNo));
        }

        public async Task<paymentRes> PaymentAuthorizeAsync(DateTime transactionDate, decimal amount, cardTranType cardTranType, string lastFourDigits, string cardSecurityCode, string templateNo, string merchantCode)
        {
            return await ExecuteWithRetry(
                async () => await _decoratee.PaymentAuthorizeAsync(transactionDate, amount, cardTranType, lastFourDigits, cardSecurityCode, templateNo, merchantCode));
        }

        public async Task<paymentRes> PaymentAuthorizeAsync(DateTime transactionDate, decimal amount, cardTranType cardTranType, string cardHolderName,
            string cardNumber, string cardSecurityCode, string expiryDate, string merchantCode)
        {
            return await ExecuteWithRetry(
                async () => await _decoratee.PaymentAuthorizeAsync(transactionDate, amount, cardTranType, cardHolderName, cardNumber, cardSecurityCode, expiryDate, merchantCode));
        }

        public async Task<paymentRes> PaymentCommitAsync(DateTime transactionDate, string authTranId, DateTime authTransactionDate, ICollection<item> items)
        {
            return await ExecuteWithRetry(
                async () => await _decoratee.PaymentCommitAsync(transactionDate, authTranId, authTransactionDate, items));
        }

        public async Task<paymentRes> PaymentCommitAsync(DateTime transactionDate, string authTranId, DateTime authTransactionDate, string fundCode,
            string reference, decimal amount)
        {
            return await ExecuteWithRetry(
                async () => await _decoratee.PaymentCommitAsync(transactionDate, authTranId, authTransactionDate, fundCode, reference, amount));
        }

        private async Task<T> ExecuteWithRetry<T>(Func<Task<T>> action)
        {
            int attempt = 1;
            do
            {
                try
                {
                    return await action.Invoke();
                }
                catch (Exception ex)
                {
                    attempt++;
                    if (attempt >= RetryCount)
                    {
                        throw new PaymentApiException(ex);
                    }
                    await Task.Delay(TimeSpan.FromSeconds(Math.Pow(BackOffPower, attempt)));
                }
            }
            while (true);
        }
    }
}