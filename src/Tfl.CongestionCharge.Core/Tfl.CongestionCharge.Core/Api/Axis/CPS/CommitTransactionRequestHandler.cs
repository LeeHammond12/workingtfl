﻿using System.Threading.Tasks;
using G2g3digital.Payments.Axis;
using G2g3digital.Payments.Axis.Model;
using Tfl.CongestionCharge.Core.Model.Payments;

namespace Tfl.CongestionCharge.Core.Api.Axis.CPS
{
    public class CommitTransactionRequestHandler
    {
        private readonly IAxisPaymentService _paymentService;

        public CommitTransactionRequestHandler(IAxisPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task CommitTransaction(PaymentRequestDto requestDto)
        {
            var commitResponse = await _paymentService.PaymentCommitAsync(
                              requestDto.TransactionDate,
                              requestDto.UniqueTransactionId,
                              requestDto.TransactionDate,
                              requestDto.FundCode,
                              requestDto.ReceiptId,
                              requestDto.Amount);

            ValidateResponse(commitResponse);
        }

        private void ValidateResponse(paymentRes response)
        {
            if (response == null)
            {
                throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.OtherError));
            }

            if (response.header?.statusCode != 0)
            {
                throw new PaymentApiException(new AxisPaymentApiNonZeroStatusCodeException(response.header));
            }

            var payment = response.payments?.payment;
            if (payment == null)
            {
                throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.OtherError));
            }
        }
    }
}