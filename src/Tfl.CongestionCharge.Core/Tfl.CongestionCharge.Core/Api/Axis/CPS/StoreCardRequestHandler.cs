﻿using System;
using System.Threading.Tasks;
using G2g3digital.Payments.Axis;
using G2g3digital.Payments.Axis.Model;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Helper;
using Tfl.CongestionCharge.Core.Model.Payments;
using Tfl.CongestionCharge.Core.ViewModel;

namespace Tfl.CongestionCharge.Core.Api.Axis.CPS
{
    public class StoreCardRequestHandler
    {
        private readonly IAxisPaymentService _paymentService;
        private readonly AppState _appState;

        public StoreCardRequestHandler(IAxisPaymentService paymentService, AppState appState)
        {
            _paymentService = paymentService;
            _appState = appState;
        }

        public async Task SaveCardDetails(PaymentRequestDto command)
        {
            // Have we already saved this card?
            var storedCard = _appState.GetStoredCard(command.CardNumberLastFourDigits, command.CardExpiryDate);
            if (storedCard == null)
            {
                await StoreCardOnCps(command);
                StoredCardOnDevice(command);
            }
        }

        private async Task StoreCardOnCps(PaymentRequestDto command)
        {
            var storeCardResponse = await _paymentService.StoreCardAsync(
                DateTime.Now,
                command.CardNumber,
                command.CardExpiryDate,
                command.NameOnCard);

            ValidateResponse(storeCardResponse);

            command.TemplateNumber = storeCardResponse.TemplateNo;
        }

        private void ValidateResponse(storeCardRes response)
        {
            if (response == null)
            {
                throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.StoreCardFailed));
            }

            if (response.header?.statusCode != 0)
            {
                throw new PaymentApiException(new AxisPaymentApiNonZeroStatusCodeException(response.header));
            }
        }

        private void StoredCardOnDevice(PaymentRequestDto command)
        {
            var storedCard = new PaymentCardViewModel
            {
                Last4Digits = command.CardNumberLastFourDigits,
                CardType = CardUtils.GetCardTypeFromNumber(command.CardNumber),
                ExpiryDate = command.CardExpiryDate,
                NameOnCard = command.NameOnCard,
                TemplateNumber = command.TemplateNumber,
                Issuer = command.Issuer
            };
            _appState.AddPaymentCard(storedCard);
        }
    }
}