﻿using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using G2g3digital.Payments.Axis;
using G2g3digital.Payments.Axis.Model;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.Payments;

namespace Tfl.CongestionCharge.Core.Api.Axis.CPS
{
    public class AuthoriseTransactionRequestHandler
    {
        private readonly IAxisPaymentService _paymentService;

        public AuthoriseTransactionRequestHandler(IAxisPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task AuthorizeTransaction(PaymentRequestDto dto, ChargeType chargeType)
        {
            paymentRes authorisationResponse;

            var merchantCode = chargeType == ChargeType.CongestionCharge ?
                Sharedkeys.CpsFundCodes.BusinessOperations :
                Sharedkeys.CpsFundCodes.EnforcementOperations;

            if (!string.IsNullOrEmpty(dto.TemplateNumber))
            {
                authorisationResponse = await AuthoriseStoredCardPayment(dto, merchantCode);
            }
            else
            {
                authorisationResponse = await AuthoriseCardPayment(dto, merchantCode);
            }

            ValidateResponse(authorisationResponse);

            dto.UniqueTransactionId = authorisationResponse.header.uniqueTranID;

            Debug.WriteLine("Authorised the payment.  TransactionID = {0}", dto.UniqueTransactionId);
        }

        private void ValidateResponse(paymentRes response)
        {
            if (response == null)
            {
                throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.OtherError));
            }

            if (response.header?.statusCode != 0)
            {
                throw new PaymentApiException(new AxisPaymentApiNonZeroStatusCodeException(response.header));
            }

            var message = response.payments?.payment?.authDetail.message;

            // returned on a successful payment
            const string authCode = @"^\""AUTH CODE:\d+\""$";
            var authCodeRegex = new Regex(authCode);

            if (message == null || !authCodeRegex.IsMatch(message))
            {
                if (message == @"""DECLINED""")
                {
                    throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.Declined));
                }

                if (message == @"""INVALID MERCHANT""")
                {
                    throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.InvalidMerchant));
                }

                if (message == @"""CALL AUTH CENTRE""")
                {
                    throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.CallAuthCenter));
                }

                if (message == @"""REQUEST INVALID""")
                {
                    throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.RequestInvalid));
                }

                if (message == @"""REFERRAL X""")
                {
                    throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.ReferralX));
                }

                if (message == @"""REFERRAL B""")
                {
                    throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.ReferralB));
                }

                throw new PaymentApiException(new AxisPaymentApiException(AxisPaymentExceptionType.OtherError));
            }
        }

        private async Task<paymentRes> AuthoriseStoredCardPayment(PaymentRequestDto requestDto, string merchantCode)
        {
            var authorisationResponse = await _paymentService.PaymentAuthorizeAsync(
                requestDto.TransactionDate,
                requestDto.Amount,
                cardTranType.B2,
                requestDto.CardNumberLastFourDigits,
                requestDto.CardSecurityCode,
                requestDto.TemplateNumber,
                merchantCode);

            return authorisationResponse;
        }

        private async Task<paymentRes> AuthoriseCardPayment(PaymentRequestDto requestDto, string merchantCode)
        {
            var authorisationResponse = await _paymentService.PaymentAuthorizeAsync(
                requestDto.TransactionDate,
                requestDto.Amount,
                cardTranType.B2,
                requestDto.NameOnCard,
                requestDto.CardNumber,
                requestDto.CardSecurityCode,
                requestDto.CardExpiryDate,
                merchantCode);

            return authorisationResponse;
        }
    }
}