﻿using System;

namespace Tfl.CongestionCharge.Core.Api.Axis
{
    public class AxisPaymentApiException : Exception
    {
        public AxisPaymentApiException(AxisPaymentExceptionType errorType)
        {
            ErrorType = errorType;
        }

        public AxisPaymentApiException(Exception ex)
            : base(string.Empty, ex)
        {
        }

        public AxisPaymentExceptionType ErrorType { get; protected set; }
    }
}