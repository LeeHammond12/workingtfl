﻿using System;
using System.Threading.Tasks;
using Refit;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.AccountInfo;
using Tfl.CongestionCharge.Core.Model.Auth;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Model.PayCc;
using Tfl.CongestionCharge.Core.Model.PayPcn;

namespace Tfl.CongestionCharge.Core.Api
{
    public class TflApiResponseHandler : ITflApi
    {
        private readonly IRequestExecuter _executer;
        private readonly ITflApi _tflApi;

        public TflApiResponseHandler(IRequestExecuter executer, ITflApi tflApi)
        {
            _executer = executer;
            _tflApi = tflApi;
        }

        public async Task<LookupVrmDto> LookupVrm(string vrm, bool isUk)
            => await ExecuteRequest(() => _tflApi.LookupVrm(vrm, isUk));

        public async Task<PcnDetailDto> LookupPcn(string pcnNumber, string vrm)
            => await ExecuteRequest(() => _tflApi.LookupPcn(pcnNumber, vrm));

        public async Task<PcnPaymentReceiptDto> PayPcn(string pcnNumber, PcnPaymentDto pcnPayment)
            => await ExecuteRequest(() => _tflApi.PayPcn(pcnNumber, pcnPayment));

        public async Task<PostcodeDetailDto> GetPostcodeDetailByBuildingNumber(string postcode, string buildingNumber)
            => await ExecuteRequest(() => _tflApi.GetPostcodeDetailByBuildingNumber(postcode, buildingNumber));

        public async Task<PostcodeDetailDto> GetPostcodeDetailByBuildingName(string postcode, string buildingName)
            => await ExecuteRequest(() => _tflApi.GetPostcodeDetailByBuildingName(postcode, buildingName));

        public async Task<CcDetail> LookupCongestionCharges(string vrm, string startDate, bool isRegisteredInUk)
            => await ExecuteRequest(() => _tflApi.LookupCongestionCharges(vrm, startDate, isRegisteredInUk));

        public async Task<CcDetail> ValidateCongestionCharges(string vrm, string startDate, string endDate, bool isRegisteredInUk)
            => await ExecuteRequest(() => _tflApi.ValidateCongestionCharges(vrm, startDate, endDate, isRegisteredInUk));

        public async Task<CcPaymentReceipt> PayCc(CcPayment payment)
            => await ExecuteRequest(() => _tflApi.PayCc(payment));

        public async Task<AuthResponseDto> GetAuthToken(AuthRequestDto authRequest)
            => await ExecuteRequest(() => _tflApi.GetAuthToken(authRequest));

        public async Task<AuthResponseDto> GetRefreshToken(AuthTokenRefreshDto authRequest)
            => await ExecuteRequest(() => _tflApi.GetRefreshToken(authRequest));

        public async Task<AccountInformationDto> GetAccountInformation()
            => await ExecuteRequest(() => _tflApi.GetAccountInformation());

        //public async Task<VehicleListResponseDto> GetVehicleQuickList()
        //    => await ExecuteRequest(() => _tflApi.GetVehicleQuickList());

        public async Task<VehicleListResponseDto> AddVehicleToQuickList(VehicleListRequestDto vehicleList)
            => await ExecuteRequest(() => _tflApi.AddVehicleToQuickList(vehicleList));

        public async Task<VehicleListResponseDto> RemoveVehicleFromQuickList(string vrm)
            => await ExecuteRequest(() => _tflApi.RemoveVehicleFromQuickList(vrm));

        public async Task<VehicleListResponseDto> GetAutoPayVehicles()
            => await ExecuteRequest(() => _tflApi.GetAutoPayVehicles());

        public async Task<VehicleListResponseDto> AddVehicleToAutoPay(VehicleListRequestDto vehicleList)
            => await ExecuteRequest(() => _tflApi.AddVehicleToAutoPay(vehicleList));

        public async Task<VehicleListResponseDto> RemoveVehicleFromAutoPay(string vrm)
            => await ExecuteRequest(() => _tflApi.RemoveVehicleFromAutoPay(vrm));

        public async Task<AccountInformationDto> AddOrReplaceCreditCard(AddDeleteOrReplacePaymentCardDto dto)
            => await ExecuteRequest(() => _tflApi.AddOrReplaceCreditCard(dto));

        public async Task<AccountInformationDto> AddOrReplaceCreditCardPut(string token, AddDeleteOrReplacePaymentCardDto dto)
            => await ExecuteRequest(() => _tflApi.AddOrReplaceCreditCardPut(token, dto));

        public async Task<AccountInformationDto> DeleteCreditCard([Body(BodySerializationMethod.Json)] AddDeleteOrReplacePaymentCardDto dto)
            => await ExecuteRequest(() => _tflApi.DeleteCreditCard(dto));

        public async Task<ReceiptListDto> GetReceipts()
            => await ExecuteRequest(() => _tflApi.GetReceipts());

        private async Task<T> ExecuteRequest<T>(Func<Task<T>> api) where T : ClientApiResponseDtoBase
            => await _executer.ExecuteRequest(api);
    }
}
