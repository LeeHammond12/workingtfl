﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.Api
{
    public class ClientApiExceptionHandler : IClientApiExceptionHandler
    {
        private readonly IUserDialog _dialogService;
        private readonly IAppNavigationService _navigationService;

        private ClientApiException _exception;

        public ClientApiExceptionHandler(IUserDialog dialogService, IAppNavigationService navigationService)
        {
            _dialogService = dialogService;
            _navigationService = navigationService;
        }

        private List<ErrorResponseItem> Errors => _exception?.Response?.Errors ?? new List<ErrorResponseItem>();

        public bool ContainsErrorCode(ClientApiException exception, string errorCode)
        {
            _exception = exception;
            return Errors.Any(error => error.ErrorCode == errorCode);
        }

        public async Task HandleErrors(ClientApiException exception)
        {
            _exception = exception;

            if (exception.IsAuthenticationException)
            {
                _navigationService.NavigateToLoginAndClearBackStack();
                return;
            }

            await HandleApplicationErrors(exception);

            // includes invalid Api key check
            await HandleRemainingErrors();
        }

        public async Task<bool> HandleInvalidApiKey(ClientApiException exception)
        {
            _exception = exception;

            var apiKeyNotPresentError = GetAndPurgeError(ClientApiErrorCodes.ApiKeyNotPresent);
            var apiKeyNotValidError = GetAndPurgeError(ClientApiErrorCodes.ApiKeyNotValid);

            if (apiKeyNotPresentError != null || apiKeyNotValidError != null)
            {
                var res = await _dialogService.Show2ButtonMessage(Strings.NewSoftwareIsNowAvailable, Strings.Information, Strings.Ok, Strings.Cancel);

                if (res)
                {
                    _navigationService.NavigateToAppDownloadPage();
                }

                return true;
            }

            return false;
        }

        public async Task<bool> HandleApplicationErrors(ClientApiException exception)
        {
            if (exception.IsAuthenticationException)
            {
                _navigationService.NavigateToLoginAndClearBackStack();
                return true;
            }

            _exception = exception;

            foreach (var error in Errors)
            {
                ApplicationMessage message;
                if (ApiErrorCodeLookup.ErrorCodes.TryGetValue(error.ErrorCode, out message))
                {
                    var applicationError = GetAndPurgeError(error.ErrorCode);
                    if (applicationError != null)
                    {
                        await _dialogService.ShowMessage(message.Message, message.Title);
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// this will handle all exceptions and the "api key wrong" error.
        /// this method needs to be called _after_ any app specific error codes
        /// are called, because it will show an error if there are any unhandled
        /// error codes remaining.
        /// </summary>
        /// <returns>task</returns>
        private async Task HandleRemainingErrors()
        {
            var inner = _exception?.InnerException;
            if (inner != null)
            {
                if (inner is NoNetworkConnectionException)
                {
                    await _dialogService.ShowMessage(Strings.NoNetworkMessage, Strings.NoNetworkTitle);
                }
                else
                {
                    await ShowGeneralErrorMessage();
                }
            }

            await HandleInvalidApiKey(_exception);

            if (_exception?.Response?.HasErrors == true)
            {
                await ShowGeneralErrorMessage();
            }
        }

        private async Task ShowGeneralErrorMessage()
        {
            await _dialogService.ShowOkMessageWithLink(Strings.GeneralApiErrorMessage, Strings.ErrorTitle);
        }

        private ErrorResponseItem GetAndPurgeError(string errorCode)
        {
            var error = Errors.FirstOrDefault(e => e.ErrorCode == errorCode);
            Errors.Remove(error);
            return error;
        }
    }
}