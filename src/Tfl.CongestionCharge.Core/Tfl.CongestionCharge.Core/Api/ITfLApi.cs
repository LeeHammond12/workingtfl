﻿using System.Threading.Tasks;
using Refit;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.AccountInfo;
using Tfl.CongestionCharge.Core.Model.Auth;
using Tfl.CongestionCharge.Core.Model.PayCc;
using Tfl.CongestionCharge.Core.Model.PayPcn;

namespace Tfl.CongestionCharge.Core.Api
{
#if USE_SIT_API || USE_UAT_API
    [Headers("Accept: application/json", "apiKey: 71b1c019-ee38-415a-a383-25fe01f127e6")]
#elif USE_PRODUCTION_API
    [Headers("Accept: application/json", "apiKey: e7e39b16-93dd-47d1-ad21-23839617f3d1")]
#else
    [Headers("Accept: application/json", "apiKey: a51155b5-d154-442c-90ec-cc0438260bb6")]
#endif
    public interface ITflApi
    {
        [Get("/lruc/v1/vrm/{vrm}?isUk={isUk}")]
        Task<LookupVrmDto> LookupVrm(string vrm, bool isUk);

        [Get("/lruc/v1/pcn/{pcnNumber}?vrm={vrm}")]
        Task<PcnDetailDto> LookupPcn(string pcnNumber, string vrm);

        [Post("/lruc/v1/pcn/{pcnNumber}/payment")]
        [Headers("Content-Type: application/json")]
        Task<PcnPaymentReceiptDto> PayPcn(string pcnNumber, [Body]PcnPaymentDto pcnPayment);

        [Get("/lruc/v1/zonechecker/address/{postcode}?BuildingNumber={buildingNumber}")]
        Task<PostcodeDetailDto> GetPostcodeDetailByBuildingNumber(string postcode, string buildingNumber);

        [Get("/lruc/v1/zonechecker/address/{postcode}?BuildingName={buildingName}")]
        Task<PostcodeDetailDto> GetPostcodeDetailByBuildingName(string postcode, string buildingName);

        [Get("/lruc/v1/congestioncharge/{vrm}?IsUk={isRegisteredInUk}&StartDate={startDate}")]
        Task<CcDetail> LookupCongestionCharges(string vrm, string startDate, bool isRegisteredInUk);

        [Get("/lruc/v1/congestioncharge/{vrm}/validate?IsUk={isRegisteredInUk}&StartDate={startDate}&EndDate={endDate}")]
        Task<CcDetail> ValidateCongestionCharges(string vrm, string startDate, string endDate, bool isRegisteredInUk);

        [Post("/lruc/v1/congestioncharge/payment")]
        [Headers("Content-Type: application/json")]
        Task<CcPaymentReceipt> PayCc([Body]CcPayment payment);

        [Post("/auth/oauth/v2/token")]
        Task<AuthResponseDto> GetAuthToken([Body(BodySerializationMethod.UrlEncoded)] AuthRequestDto authRequest);

        [Post("/auth/oauth/v2/token")]
        Task<AuthResponseDto> GetRefreshToken([Body(BodySerializationMethod.UrlEncoded)] AuthTokenRefreshDto authRequest);

        [Get("/lruc/v1/account")]
        Task<AccountInformationDto> GetAccountInformation();

        //[Get("/lruc/v1/account/vehicledetail/quickmanaged")]
        //Task<VehicleListResponseDto> GetVehicleQuickList();

        [Post("/lruc/v1/account/vehicledetail/quickmanaged")]
        Task<VehicleListResponseDto> AddVehicleToQuickList([Body]VehicleListRequestDto vehicleList);

        [Delete("/lruc/v1/account/vehicledetail/quickmanaged/{vrm}")]
        Task<VehicleListResponseDto> RemoveVehicleFromQuickList(string vrm);

        [Get("/lruc/v1/account/vehicledetail/ccautopay")]
        Task<VehicleListResponseDto> GetAutoPayVehicles();

        [Post("/lruc/v1/account/vehicledetail/ccautopay")]
        Task<VehicleListResponseDto> AddVehicleToAutoPay([Body]VehicleListRequestDto vehicleList);

        [Delete("/lruc/v1/account/vehicledetail/ccautopay/{vrm}")]
        Task<VehicleListResponseDto> RemoveVehicleFromAutoPay(string vrm);

        [Post("/lruc/v1/account/paymentdetail")]
        Task<AccountInformationDto> AddOrReplaceCreditCard([Body] AddDeleteOrReplacePaymentCardDto dto);

        [Put("/lruc/v1/account/paymentdetail/{token}")]
        [Headers("Activity: MaintainAccountPaymentDetail")]
        Task<AccountInformationDto> AddOrReplaceCreditCardPut(string token, [Body] AddDeleteOrReplacePaymentCardDto dto);

        [Post("/lruc/v1/account/paymentdetail/removecard")]
        Task<AccountInformationDto> DeleteCreditCard([Body] AddDeleteOrReplacePaymentCardDto dto);

        [Get("/lruc/v1/account/financialtransactions")]
        Task<ReceiptListDto> GetReceipts();
    }
}
