﻿using System;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Api
{
    public class BypassDecorator : IRequestExecuter
    {
        public Task<T> ExecuteRequest<T>(Func<Task<T>> api) where T : ClientApiResponseDtoBase
        {
            return api.Invoke();
        }
    }
}
