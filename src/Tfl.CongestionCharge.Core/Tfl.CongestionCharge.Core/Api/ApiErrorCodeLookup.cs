﻿using System.Collections.Generic;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.Api
{
    public static class ApiErrorCodeLookup
    {
        public static readonly Dictionary<string, ApplicationMessage> ErrorCodes = new Dictionary<string, ApplicationMessage>()
        {
            { ClientApiErrorCodes.StatementDateIsInComing4DaysSoYouCanNotChangeToDirectDebit, new ApplicationMessage(Strings.StatementDateIsInComing4DaysSoYouCanNotChangeToDirectDebit) },
            { ClientApiErrorCodes.CannotAddVehicleToAutoPayBecauseItsAlreadyOnAutoPay, new ApplicationMessage(Strings.CannotAddVehicleToAutoPayBecauseItsAlreadyOnAutoPayErrorMessage) },
            { ClientApiErrorCodes.CustomerCannotRegisterMoreThan10PreferredVehicles, new ApplicationMessage(Strings.CustomerCannotRegisterMoreThan10PreferredVehicles, Strings.Information) },
            { ClientApiErrorCodes.AlreadyHaveAPreferredVehicleWithGivenVrm, new ApplicationMessage(Strings.AlreadyHaveAPreferredVehicleWithGivenVrm, Strings.Information) },
            { ClientApiErrorCodes.PcnNotFoundOnTarantoSystem, new ApplicationMessage(Strings.CouldntFindPCN, Strings.Information) },
            { ClientApiErrorCodes.PcnOverpayment, new ApplicationMessage(Strings.PCNOverpayment) },
            { ClientApiErrorCodes.PcnReferenceDoesNotExist, new ApplicationMessage(Strings.CouldntFindPCN, Strings.Information) },
            { ClientApiErrorCodes.PcnReferenceNotValid, new ApplicationMessage(Strings.CouldntFindPCN, Strings.Information) },
            { ClientApiErrorCodes.PcnAlreadyCancelled, new ApplicationMessage(Strings.PcnAlreadyCancelled, Strings.Information) },
            { ClientApiErrorCodes.PcnNotFound, new ApplicationMessage(Strings.CouldntFindPCN, Strings.Information) },
            { ClientApiErrorCodes.VrmNoVehicleDetailsFound, new ApplicationMessage(Strings.CouldntFindVehicle, Strings.Warning) },
            { ClientApiErrorCodes.CcPaymentNoVehicleDetailsFound, new ApplicationMessage(Strings.CcPaymentNoVehicleDetailsFound) },
            { ClientApiErrorCodes.CcDateIsNotChargeable, new ApplicationMessage(Strings.CcNonChargingDate, Strings.Warning) },
            { ClientApiErrorCodes.ChargeCannotStartMoreThan65DaysInFuture, new ApplicationMessage(Strings.CcCannotPayMoreThan65DaysInFuture, Strings.Information) },
            { ClientApiErrorCodes.CcVrmAlreadyHas100PercentDiscount, new ApplicationMessage(Strings.CcVrmAlreadyHas100PercentDiscount, Strings.Warning) },
            { ClientApiErrorCodes.CcVrmOnAutoPayServiceCannotBeUsed, new ApplicationMessage(Strings.CcVrmOnAutoPayServiceCannotBeUsed) },
            { ClientApiErrorCodes.PcAddressNotFound, new ApplicationMessage(Strings.PostCodeNotFound) },
            { ClientApiErrorCodes.ChargeCannotStartFurtherInThePastThanThePreviousChargingDay, new ApplicationMessage(Strings.CcCannotPayForAChargeInThePast, Strings.Information) },
            { ClientApiErrorCodes.ChargeCannotStartFurtherInThePastThanThePreviousChargingDay1, new ApplicationMessage(Strings.CcCannotPayForAChargeInThePast, Strings.Information) },
            { ClientApiErrorCodes.StartDateIsAfterTheResidentDiscountPeriod, new ApplicationMessage(Strings.CcStartDateIsOutsideDiscountPeriod, Strings.Information) },
        };
    }
}