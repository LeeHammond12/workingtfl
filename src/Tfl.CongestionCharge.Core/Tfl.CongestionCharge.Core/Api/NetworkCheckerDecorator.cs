﻿using System;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model.Base;

namespace Tfl.CongestionCharge.Core.Api
{
    public class NetworkCheckerDecorator : IRequestExecuter
    {
        private readonly IConnection _connection;

        public NetworkCheckerDecorator(IConnection connection)
        {
            _connection = connection;
        }

        public async Task<T> ExecuteRequest<T>(Func<Task<T>> api) where T : ClientApiResponseDtoBase
        {
            if (!_connection.NetworkConnected())
                throw new NoNetworkConnectionException();

            return await api.Invoke();
        }
    }
}