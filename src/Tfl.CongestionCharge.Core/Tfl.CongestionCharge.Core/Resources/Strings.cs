﻿namespace Tfl.CongestionCharge.Core.Resources
{
    //// a class to hold all our strings which are accessed via code
    //// Can't seem to get resource files working correctly for all platforms
    //// this is kind of a half way house
    public class Strings
    {
        public static string AutoPayVehiclesDescription1 = "Here you can see which vehicles are associated to your Auto Pay. Add additional vehicles by selecting available vehicles or by adding a new vehicle.";

        public static string VehiclesAndServicesDescription1 = "Here you can view active vehicles and services on your account .";

        public static string VehiclesAndServicesDescription2 = "To add additional vehicles, sign in to your London Road User Charging Account at";

        public static string VehiclesAndServicesDescription3 = "tfl.gov.uk/congestioncharge";

        public static string VehiclesAndServicesDescription4 = "Vehicles active on a Discount or Auto Pay";

        public static string DirectDebitDescription1 => "Your monthly payments are being taken by Direct Debit.";

        public static string DirectDebitDescription2 => "To change your payment method or view your direct debit details sign in to your online account at <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a>";

        public static string ReviewInformationAndCharges = "Review information & charges";

        public static string ReviewDatesAndCharges = "Review dates & charges";

        public static string CardRegistered => "Card Registered for Auto Pay";

        public static string YourAccount => "Your Account";

        public static string YourVehicles => "Your vehicles";

        public static string YourVehiclesAndServices => "Your Vehicles & Services";

        public static string AutoCcPayUrl => "tfl.gov.uk/congestioncharge";

        public static string DirectDebitUrl => "tfl.gov.uk/congestioncharge";

        public static string NoLondonUrl => "tfl.gov.uk/congestioncharge";

        public static string AutoCcPayLink => "To set up, sign in to your London Road User Charging account online at ";

        public static string YourAutoPay => "Your Auto Pay";

        public static string YourServices => "Your Services";

        public static string YourPaymentMethods => "Your Payment Methods";

        public static string ApplicationName => "Transport for London";

        public static string Name => "Name";

        public static string PleaseWaitTitle => "Please wait...";

        public static string PleaseWaitForPaymentTitle => "Processing payment.";

        public static string ThisMayTakeUpToAMinute => string.Empty;

        public static string PleaseWaitMessage => string.Empty;

        public static string PleaseWaitForPaymentMessage => string.Empty;

        public static string PayChargeAnywayMessage => "Pay charge anyway";

        public static string Exit => "Exit";

        public static string CouldntFindVehicle => "We don’t have details for this vehicle.\r\n\r\n<b>UK registered:</b> If your vehicle's make and model have not been returned, make sure you entered the right Vehicle Registration Mark before continuing.\r\n\r\n<b>Registered outside the United Kingdom:</b> We can’t display the make, model and colour information.\r\n\r\nConfirm this is the correct vehicle to continue.";

        public static string VehicleExemptMessage => "This vehicle is exempt from the Congestion Charge.\r\n\r\nThis is based on the vehicle registration details you entered, and the information held today by the DVLA or DVANI and TfL.\r\n\r\nIf you think this information isn’t right, and you still want to pay the Congestion Charge, then you can continue.\r\n\r\nThis information is correct today, but may change without notice.\r\n\r\nAlways check to see if payment is required.";
        public static string AutoPayVehicleExemptMessage => "This vehicle is exempt from the Congestion Charge.\r\n\r\nThis is based on the vehicle registration details you entered, and the information held today by the DVLA or DVANI and TfL.\r\n\r\nIf you think this information isn’t right and you still want to add this vehicle to your Auto Pay service, then you can continue.\r\n\r\nThis information is correct  today, but may change without notice.\r\n\r\nAlways check to see if payment is required.";

        public static string EsVehicleIsCcExempt => "Your vehicle does not meet the emissions standard.\r\n\r\nThis means that although it is exempt from the Congestion Charge based on the information held today by the DVLA or DVANI and tfl you still need to pay the T-Charge value to travel in the zone.\r\n\r\n<a href=\"https://tfl.gov.uk/T-Charge\"> Find out more about the T-Charge</a> ";

        public static string EsAutoPayVehicleExemptMessage => "Your vehicle does not meet the emissions standard.\r\n\r\nThis means that although it is exempt from the Congestion Charge based on the information held today by the DVLA or DVANI and tfl you still need to pay the T-Charge amount to travel in the zone.\r\n\r\nYou can pay the T-Charge amount by adding this vehicle to your Auto Pay service.\r\n\r\n<a href=\"https://tfl.gov.uk/T-Charge\">Find out more about the T-Charge</a> ";

        public static string VehicleExemptTitle => "Warning";

        public static string IsVehicleRegisteredInUk => "Is this vehicle registered in the UK?";

        public static string SaveVehicleDetails => "Save vehicle details";

        public static string SelectYourSavedVehicles => "Select your saved vehicles";

        public static string CarMake => "Vehicle make";

        public static string Ok => "Ok";

        public static string PayCongestionCharge => "Pay Congestion Charge (CC)";

        public static string PromptUserToEnterCvv => "Please enter your 3 digit CVV number and select the 'Confirm details and pay' button";

        public static string ChooseAVehicle => "Choose a vehicle";

        public static string AreYouCcZoneResident => "Are you a CC zone resident?";

        public static string PromptUserToConfirmVrmIsCorrect => "Please select the 'I confirm this VRM is correct' checkbox to continue";

        public static string ConfirmThisVrmIsCorrect => "I confirm this VRM is correct";

        public static string PromptToSearchForVrm => "Please select 'Search' button so we can check that your VRM is correct.";

        public static string PaymentsWillNotBeRecordedInLruAccount => "Payments made in the app will not be recorded in your account.\r\n\r\nPlease enter your account number and select the 'I confirm this VRM is correct' checkbox to continue";

        public static string Warning => "Warning";

        public static string Yes => "Yes";

        public static string No => "No";

        public static string ChooseDates => "Choose your dates";

        public static string ChangeStartDate => "Change Start Date";

        public static string ChangeStartDateDescription => "Please select a different date if needed. Please note that the Congestion Charge can only be paid up to 65 charging days in advance.";

        public static string ResidentAccountNumber => "Resident Account Number";

        public static string AccountNumber => "Account Number";

        public static string Loading => "Loading";

        public static string PreviousChargingDay => "Previous Charging Day";

        public static string DailyCongestionCharge => "Daily Charge";

        public static string Today => "Today";

        public static string OneDay => "1 Day";

        public static string PayFor => "Pay for?";

        public static string EnterPaymentDetails => "Enter payment details";

        public static string NameAsItAppearsOnCars => "Name as it appears on the card";

        public static string CardNumber => "Card number";

        public static string ExpiryMmyy => "Expiry MM/YY";

        public static string EnterCcv => "Enter CVV";

        public static string SaveCardDetails => "Save card details";

        public static string SelectYourStoredCard => "Choose card";

        public static string SendMyReceiptViaSms => "Send receipt by text to your mobile";

        public static string Receipt => "Receipt:";

        public static string Receipts => "Receipts";

        public static string ReceiptNoColon => "Receipt No:";

        public static string PleaseEnterYourSmsNumber => "Please enter your SMS number";

        public static string PleaseEnterCorrectSmsNumber => "Mobile number can only contain digits";

        public static string ConfirmDetailsAndPay => "Confirm details and pay";

        public static string Placeholder => " ";

        public static string AddAnotherCard => "Add another Card";

        public static string PaymentDetails => "Payment details";

        public static string VRMRequired => "Vehicle registration mark required";

        public static string ResidentNumberRequired => "Resident account number required";

        public static string ErrorTitle => "Error";

        public static string GeneralApiErrorMessage => "Sorry - we were unable to complete this action.\r\n\r\nPlease try again or visit <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a>";

        public static string UnknownErrorMessage => "Sorry - we were unable to complete this action.\r\n\r\nPlease try again or visit <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a>";

        public static string TheFollowingErrorsOccurred => "The following errors occured:";

        public static string YouCannotPayThisPcn => "You cannot pay this PCN.\r\n\r\nFor more information visit\r\n<a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a>";

        public static string CouldntFindPCN => "Sorry. We don't recognise that PCN and VRM combination.\r\n\r\nPlease check and try again";

        public static string CustomerCannotRegisterMoreThan10PreferredVehicles => "You cannot save more than 10 vehicles onto your account.\r\n\r\n This vehicle has not been saved";

        public static string AlreadyHaveAPreferredVehicleWithGivenVrm => "There already exists a preferred vehicle with that VRM.";

        public static string PCNOverpayment => "Overpayment. The amount of payment has resulted in the case been overpaid";

        public static string AddressInsideCcZone => "This address is INSIDE the congestion charging zone and";

        public static string AddressOutsideCcZone => "This address is OUTSIDE the congestion charging zone and";

        public static string AddressInsideResidentZone => " INSIDE the resident zone";

        public static string AddressOutsideResidentZone => " OUTSIDE the resident zone";

        public static string CouldNotFindAddress => "No address details have been found.\r\n\r\nPlease check and try again";

        public static string Yesterday => "Yesterday";

        public static string Address => "Address";

        public static string PaymentFailedTitle => "Payment Failed";

        public static string NoNetworkTitle => "No Network Connection";

        public static string NoNetworkMessage => "You must be connected to the internet to use this app.";

        public static string NumberInvalidTitle => "SMS Number Error";

        public static string NumberInvalidMessage => "The number you have entered is invalid";

        public static string CardNumberNotValid => "'Card Number' is not a valid credit card number";

        public static string Cvv2Length => "'CVV' is not a valid CVV number";

        public static string SmsNumberStartsWith => "Please enter a valid mobile number starting with '07'.";

        public static string SmsNumberLength => "Mobile number must be 11 characters in length with no spaces.";

        public static string VrmCannotContainSpecialCharacters => "'VRM' cannot contain special characters and has to be at most 10 characters long.";

        public static string ResidentAccountNumberCannotContainSpecialCharacters => "'Resident Account Number' should only contain numbers";

        public static string PcnHasWrongFormat => "'PCN' is not valid, it should be 2 letters followed by 8 digits.";

        public static string Vrm => "VRM";

        public static string PayPenaltyChargeNotice => "Pay Penalty Charge Notice (PCN)";

        public static string PayAPenaltyChargeNotice => "Pay a Penalty Charge Notice (PCN)";

        public static string CheckAPostCode => "Check a postcode";

        public static string PaymentReceipts => "Payment Receipts";

        public static string ViewReceipts => "Receipts";

        public static string Congestion => "Congestion";

        public static string Penalty => "Penalty";

        public static string Confirmation => "Confirmation";

        public static string AllDone => "All done!";

        public static string YourReceipt => "Your receipt:";

        public static string VrmColon => "VRM:";

        public static string Type => "Type";

        public static string TypeColon => "Type:";

        public static string PcnNoColon => "PCN No:";

        public static string Vehicle => "Vehicle";

        public static string Price => "Price";

        public static string AmountPaid => "Amount Paid:";

        public static string ChargeType => "Charge Type:";

        public static string JourneyDates => "Journey Dates:";

        public static string JourneyDatesNoColon => "Journey Dates";

        public static string PaymentDate => "Payment Date:";

        public static string Amount => "Amount";

        public static string SaveReceipt => "Save receipt";

        public static string Home => "Home";

        public static string Next => "Next";

        public static string ThanksPaymentCompletedSuccessfully => "Thanks, your payment has been completed successfully";

        public static string PcnNumber => "PCN Number";

        public static string ReceiptPcnNumber => "PCN No:";

        public static string VehicleRegistrationMark => "Vehicle registration mark";

        public static string VehicleRegistrationMark1 => "Vehicle registration mark";

        public static string TheAmountYouOweIs => "The amount due is: £";

        public static string PcnDetails => "PCN details";

        public static string Continue => "Continue";

        public static string PayForWhat => "Pay for?";

        public static string ConsecutiveChargingDays => " consecutive charging days";

        public static string ChangeDate => "Change Date";

        public static string NotAvailable => "n/a";

        public static string FindOutAPostCodeIsInCcZone => "Check if a postcode is in the Congestion Charge zone";

        public static string EnterYourPostCode => "Enter your postcode";

        public static string EnterBuildingNumber => "Enter building number";

        public static string EnterBuildingName => "Enter building name";

        public static string Check => "Check a postcode";

        public static string Or => "OR";

        public static string Done => "Done";

        public static string Search => "Search";

        public static string YourSavedReceipts => "Your saved receipts";

        public static string LookupPostCode => "Lookup postcode";

        public static string WeeklyCongestionCharge => "One Week";

        public static string MonthlyCongestionCharge => "One Month";

        public static string ManageCCAutoPay => "Manage CC Auto Pay";

        public static string AddVehicleToAutoPay => "Add Vehicle to Auto Pay";

        public static string AutoPayCreditCard => "Auto Pay payment card";

        public static string DirectDebitDetails => "Direct Debit details";

        public static string AutoPayVehicles => "Auto Pay Vehicles";

        // credit card names
        public static string Amex => "Amex";

        public static string MasterCard => "Mastercard";

        public static string Visa => "Visa";

        public static string Discover => "Discover";

        public static string Solo => "Solo";

        public static string Switch => "Switch";

        public static string DeviceHasNoNetworkConnection => "Device has no network connection";

        public static string UpdateAvailableText => "There is an update for this app available, do you want to download it?";

        public static string UpdateAvailableTitle => "Update available";

        public static string NewSoftwareIsNowAvailable => "New software is now available.\r\n\r\nPlease download the latest version from the app store";

        public static string PcnNotValid => "Invalid PCN Number, or PCN and VRM combination does not exist.\r\n\r\nPlease try again.";

        public static string PcnAlreadyCancelled => "This PCN is already cancelled, no further action is necessary.";

        public static string CcPaymentNoVehicleDetailsFound => "No vehicle details were found for this payment.";

        public static string CcNonChargingDate => "The date you have selected is a non-charging date.\r\n\r\nThe next charging day has been provided for you.\r\n\r\nPlease check the date and select the 'Continue' button to pay.";

        public static string PromptUserToSelectContinueAfterChoosingDate => "Please choose a date option and then select the 'Continue' button to proceed";

        public static string CcCannotPayMoreThan65DaysInFuture => "Sorry. You cannot pay for a date more than 65 charging days in the future.\r\n\r\nTo continue please select another date.";

        public static string CcVrmAlreadyHas100PercentDiscount => "Your vehicle is 100% discounted and no charges are applicable at this point\r\n\r\n Please click 'OK' to return to the home screen.";

        public static string CcCannotPayForAChargeInThePast => "Sorry. You cannot pay for a charge in the past.\r\n\r\nPlease select another date.";

        public static string CcStartDateIsOutsideDiscountPeriod => "The date you have selected is outside of your discount period.\r\n\r\nPlease select another date";

        public static string CcChargeHasAlreadyBeenPaidForDatesSelected => "A charge has already been paid for one or more of the dates of travel selected.\r\n\r\nPlease choose an alternate date.";

        public static string YouAlreadySelectedAChargeForThisVehicle => "Some of your already selected charges overlap with this charge.\r\n\r\nPlease review your current charges for any overlaps to avoid unnecessary payments.";

        public static string SomeChargesAreOverlapping => "One or more of your charges overlap with other charges you selected.\r\n\r\nPlease review your current charges for any overlaps to avoid unnecessary payments.";

        public static string CcNoChargesAvailableForThisDate => "No charges available for this date.";

        public static string CcVrmOnAutoPayServiceCannotBeUsed => "This VRM exists on Auto Pay service and can not be charged.";

        public static string PostCodeNotFound => "No address details have been found.\r\n\r\nPlease check and try again.";

        public static string PleaseEnterAValidPostCode => "The postcode is not in the correct format.\r\n\r\nPlease check and try again.";

        public static string PleaseEnterAValidBuildingNumber => "The building number is not in the correct format.\r\n\r\nPlease check and try again.";

        public static string PleaseEnterAValidBuildingName => "The building name is not in the correct format.\r\n\r\nPlease check and try again.";

        public static string StartDate => "Start date";

        public static string ReceiptNo => "Receipt No";

        public static string Pcn => "PCN";

        public static string CongestionChargeCc => "Congestion Charge (CC)";

        public static string PayNextDay => "Pay next day";

        public static string Information => "Information";

        public static string Cancel => "Cancel";

        public static string Rate => "Rate";

        public static string PleaseRateThisAppTitle => "Rate?";

        public static string PleaseRateThisApp => "Please rate this app";

        public static string TransactionFailedTitle => "Transaction failed";

        public static string TransactionFailedMessage => "Please try again or visit <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a>";

        public static string PcnPaymentFailedMessage => "Sorry, we have been unable to complete this transaction.\r\nPlease try again or visit <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a>";

        public static string CcPaymentFailedTitle => "Payment Failed";

        public static string CcPaymentFailedMessage => "To pay visit <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a>\r\n\r\nFailure to pay may result in the issue of a Penalty Charge Notice.";

        public static string TermsAndConditionsTitle => "Terms and Conditions for using this app";

        public static string TermsAndConditionTitle => "Terms and Conditions";

        public static string TermsAndConditions => "The Terms and Conditions for using this app are...";

        public static string Decline => "Decline";

        public static string Accept => "Accept";

        public static string AcceptTermsAndCondition = "Accept Terms and Conditions";

        public static string Close = "Close";

        public static string ApplyForAutoPayOnTheWeb = "Apply for Auto Pay on the TfL website";

        public static string PrivacyNoticeLink => "https://tfl.gov.uk/corporate/privacy-and-cookies/road-user-charging#on-this-page-9";

        public static string PrivacyPageTitle => "Privacy & data protection policy";

        public static string Privacy => "Privacy Policy";

        public static string PrivacyPageContent => "Please click on the link below to view the London Road User Charging Privacy Policy.\r\n\r\nThe webpage page explains how Transport " +
                "for London (TfL) uses the personal information collected for the operation of our London Road User Charging schemes, which include the " +
                "Congestion Charge and the Low Emission Zone. It also describes the circumstances in which we might disclose it to a third party.";

        public static string Sorry => "Sorry";

        public static string RemoveVehicleDescription => "Here you can view and remove vehicles from your device:";

        public static string ValidCustomerIdRequired => "Please enter a valid CustomerId";
        public static string WrongCustomerIdOrPassword => "The customer ID and password combination is incorrect.\r\n\r\nPlease try again.";

        public static string NoLondonAccount =>
            "Don't have a London Road User Charging account or have forgotten your details? Go to <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a>";

        public static string NoLondonAccountText =>
            "Don't have a London Road User Charging account or have forgotten your details? Go to";

        public static string ValidPasswordRequired => "Please enter a valid password";

        public static string LoginToYourAccount => "Login to your London\r\nRoad User Charging account";
        public static string YourVehiclesAndCards => "Your vehicles and cards";
        public static string ManageStoredInformationDescription => "View and delete information stored on your device:";
        public static string ManageStoredCreditCards => "Manage Stored Credit Cards";
        public static string ManageStoredVehicles => "Manage Stored Vehicles";

        public static string HomeScreenHeading => "TfL Congestion Charge";

        public static string ManageStoredCardsDescription => "Here you can view and remove payment cards from your device:";
        public static string YourCreditCards => "Your Payment Cards";

        public static string ManageYourCreditCards => "Manage your Credit Cards";
        public static string ThereIsAnAdministrationChargeForEachVehicle => "There is a £10 annual registration charge per vehicle. This will be added to your next monthly statement.\r\n\r\nContinue to add vehicle?";

        public static string MakePaymentOf => "Make payment of ";
        public static string ReallyRemoveThisJourney => "Are you sure you want to remove this Journey?";
        public static string DoYouWantToLogin => "Do you want to login into your TFL account?";
        public static string NoDetailsFound => "No details found";
        public static string CustomerIdHelpText => "Your Customer ID is unique to you.\r\n\r\nIf you are the account holder this number is the same as your London Road User Charging account number.\r\n\r\nIf you have been given access to someone else's account, you will have an individual Customer ID for you.\r\n\r\nIf you do not know this ID, the account holder will be able to tell you.\r\n\r\nIf you used to add a '0' before your account number, please now remove this.";
        public static string PasswordHelpText => "If you signed up for a London Road User Charging account online, or you have previously signed in to your online account, please enter your password.\r\n\r\nIf you signed up offline, or were added as an account user, and have not yet signed in online you should enter your six digit PIN number in the box below.\r\n\r\nOnce you sign in online with your PIN for the first time you will be asked to create a password. Once a password has been set your PIN will no longer be valid for sign in and you should enter your password.";
        public static string FailedToSetAutoPayPaymentCard => "Sorry, we have been unable to complete this transaction.\r\n\r\nPlease check and try again.";

        public static string DoYouReallyWantToRemoveXFromAutoPay => "If you remove this vehicle from your Auto Pay service you will need to pay the £10 registration charge if you decide to re add it.\r\n\r\nDo you still want to remove it?";
        public static string ReallyRemoveThisVehicle => "Really remove this vehicle?";
        public static string Confirm => "Confirm";
        public static string DoYouReallyWantToRemoveVehicleX => "Do you really want to remove {0}?";
        public static string DoYouReallyWantToRemoveCardX => "Do you really want to remove the card ending with {0}?";
        public static string SignOut => "Sign Out";
        public static string SignIn => "Sign In";

        public static string VehiclesActiveOnAutoPay = "Vehicles active on CC Auto Pay";
        public static string VehiclesInactiveOnAutoPay = "Vehicles inactive on CC Auto Pay";
        public static string CvvHintText => "The security code is the three digits on the back of your card, next to the signature panel";

        public static string CVV => "CVV";

        public static string NoVehiclesOnAnonAccount => "You currently have no vehicles stored.";

        public static string NoPaymentCardsOnAnonAccount => "You currently have no cards stored.";
        public static string Unknown => "Unknown";

        public static string BalanceDescriptionText2 => "If the status of your service is 'Pending suspension' or 'Suspended' you need to visit <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a> to reactivate it.";

        //public static string BalanceDescriptionTextWhenNotActive => "";

        public static string SuspendedOrClosedAccountWarning => "Your Auto Pay service is currently in a suspended or closed status.\r\n\r\nIn order to amend any details on your service you will need to sign into your account via <a href=\"https://tfl.gov.uk/congestioncharge\">tfl.gov.uk/congestioncharge</a> and follow the instructions to reactivate it or reapply.\r\n\r\nIn the meantime you will need to pay the Congestion Charge via another channel, failure to do so may result in Penalty Charge Notices being issued.";

        public static string NoVehicleWithServices => "You have no vehicles active on a Discount or on Auto Pay";

        public static string BalanceDescriptionText1 =>
            "This is your current balance. It includes Congestion Charge zone payments, vehicle registration charges and your last statement balance, minus any payments received since your last statement was issued.";

        public static string TheAppCannotBeUsedWithABusinessAccount => "Sorry, the app does not support business accounts.";

        public static string ChargingDate => "Charging Date";
        public static string AutoPayServiceSuspendedDescription1 => "Your Auto Pay service is currently in a suspended or closed status.";
        public static string AutoPayServiceSuspendedDescription2Part1 => "In order to amend any details on your service you will need to sign into your account via ";
        public static string AutoPayServiceSuspendedDescription2Part2 => " and follow the instructions to reactivate it or reapply.";
        public static string AutoPayServiceSuspendedDescription3 => "In the meantime you will need to pay the Congestion Charge via another channel,  failure to do so may result in Penalty Charge Notices being issued.";
        public static string AutoPayServiceSuspendedUrl => "tfl.gov.uk/congestioncharge";

        public static string AutoPayServiceActiveDescription1 => "This is your current balance. It includes Congestion Charge zone payments, vehicle registration charges and your last statement balance, minus any payments received since your last statement was issued.";
        public static string AutoPayServiceActiveDescription2Part1 => "If the status of your service is 'Pending suspension' or 'Suspended' you need to visit ";
        public static string AutoPayServiceActiveDescription2Part2 => " to reactivate it.";
        public static string AutoPayServiceActiveUrl => "tfl.gov.uk/congestioncharge";

        public static string AutoPayCreditCardDescription1 = "Your monthly payments are being taken from the payment card shown above.";

        public static string AutoPayCreditCardDescription2 = "Please check the expiry date, if your card expires payments will fail, and you may be issued with Penalty Charge Notices.";

        public static string AutoPayCreditCardDescription3 = "You can change your payment card at any time, all future monthly payments will be taken from your new card once switched.";

        public static string AutoPayCreditCardUrl => "tfl.gov.uk/congestioncharge";

        public static string ErrorSavingVehicleDoYouWantToContinue => "Sorry, there was an error saving the vehicle to your account.\r\n\r\nDo you want to continue with the CC payment?";

        public static string CouldNotSaveVehicleToQuickList => "Sorry, there was a problem saving the vehicle to your account.";

        public static string AutoPayCreditCardDescription4 => "To add a new payment card or view Direct Debit details sign in to your online account at " +
                                                             AutoPayCreditCardUrl;

        public static string PayForDescription => "Please select which date(s) you would like to pay for";

        public static string CannotAddMoreThan5VehiclesToAutoPay => "You can’t add more than five vehicles to your CC Auto Pay service.\r\n\r\nIf you want to add another one you will need to remove one first.";

        public static string CannotAddVehicleToAutoPayBecauseItsAlreadyOnAutoPayErrorMessage => "We are unable to add this vehicle to your autopay service.\r\n\r\nIt may already be on another autopay service.\r\n\r\nPlease contact the call centre on 0343 222 2222 for further information.";

        public static string YouHaveNoVehiclesActive => "You have no vehicles active on a Discount or on Auto Pay";

        public static string StatementDateIsInComing4DaysSoYouCanNotChangeToDirectDebit => "The next statement date is in coming 4 days so you can not change the payment method to Direct Debit.";

        public static string DoYouReallyWantToGoToTheHomeScreen => "Do you really want to go back to the home screen?";

        public static string DoYouReallyWantToGoBackAndLoseAllYourSelectedCharges => "Do you really want to go back and lose all your selected charges?";

        public static string YourVehicleIsSubjectToAnEmissionSurcharge => "Your vehicle doesn’t meet the emissions standard.\r\n\r\nThis means the total you pay for the Congestion Charge will also include an amount for the T-Charge.\r\n\r\n<a href=\"https://tfl.gov.uk/T-Charge\">Find out more about the T-Charge</a> ";
        public static string YourAutoPayVehicleIsSubjectToAnEmissionSurcharge => "Your vehicle doesn’t meet the emissions standard.\r\n\r\nThis means that  your daily Auto Pay charge will also include an amount for the T-Charge.\r\n\r\n<a href=\"https://tfl.gov.uk/T-Charge\">Find out more about the T-Charge</a> ";

        public static string CustomerResposibilityDescription => "You must not install or use the app on a jail - broken or rooted device. These have had their security features changed in order to work, which makes them less secure, and means that fraudsters could access your phone or tablet.";

        public static string CustomerResposibility => "Responsibilities of customer";

        public static string CouldntFindVehicleDetailsForEsAutoPayVehicle => "We don’t have the make, model or colour details for this vehicle.\r\n\r\nMake sure you entered the right vehicle registration mark before continuing.\r\n\r\n Your vehicle does not meet the emissions standard.\r\n\r\nThis means that your daily  Auto Pay charge  will also include an amount for the T-Charge.\r\n\r\n<a href=\"https://tfl.gov.uk/T-Charge\">Find out more about the T-Charge</a> ";

        public static string CouldntFindVehicleDetailsForEsVehicle => "We don’t have the make, model or colour details for this vehicle.\r\n\r\nMake sure you entered the right vehicle registration mark before continuing.\r\n\r\nYour vehicle does not meet the emissions standard, this means the total you pay for the Congestion Charge will also include an amount for the T-Charge.\r\n\r\n<a href=\"https://tfl.gov.uk/T-Charge\">Find out more about the T-Charge</a> ";
    }
}