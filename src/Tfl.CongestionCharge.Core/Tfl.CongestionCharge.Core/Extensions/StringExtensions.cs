﻿using System;
using System.Text.RegularExpressions;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class StringExtensions
    {
        public static string WithSpace(this string s)
        {
            if (string.IsNullOrWhiteSpace(s))
                return string.Empty;
            return s + " ";
        }

        // works only for well formed html
        public static string StripHtml(this string input)
        {
            return Regex.Replace(input, "<.*?>", string.Empty);
        }

        public static int CalcLevenshteinDistance(this string a, string b)
        {
            if (string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b))
                return 0;

            int lengthA = a.Length;
            int lengthB = b.Length;
            var distances = new int[lengthA + 1, lengthB + 1];

            for (int i = 0; i <= lengthA; distances[i, 0] = i++)
            {
            }
            for (int j = 0; j <= lengthB; distances[0, j] = j++)
            {
            }

            for (int i = 1; i <= lengthA; i++)
            {
                for (int j = 1; j <= lengthB; j++)
                {
                    int cost = b[j - 1] == a[i - 1] ? 0 : 1;

                    distances[i, j] = Math.Min(
                        Math.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1),
                        distances[i - 1, j - 1] + cost);
                }
            }
            return distances[lengthA, lengthB];
        }
    }
}
