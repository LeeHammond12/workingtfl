﻿using System;
using System.IO;
using System.Threading.Tasks;
using PCLStorage;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class IFileSystemExtensions
    {
        public static async Task<Stream> OpenFileStream(this IFileSystem fileSystem, string filePath)
        {
            return
                await
                    (await FileSystem.Current.GetFileFromPathAsync(filePath).NoSyncBack()).OpenAsync(FileAccess.Read)
                        .NoSyncBack();
        }

        public static async Task<bool> FileExists(this IFileSystem fileSystem, string fileName)
        {
            try
            {
                var file = await fileSystem.GetFileFromPathAsync(fileName);
                return file != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static async Task<bool> FolderExists(this IFileSystem fileSystem, string folderName)
        {
            try
            {
                var file = await fileSystem.GetFolderFromPathAsync(folderName);
                return file != null;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}