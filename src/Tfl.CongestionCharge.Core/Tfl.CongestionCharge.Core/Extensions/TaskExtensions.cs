﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class TaskExtensions
    {
        public static ConfiguredTaskAwaitable NoSyncBack(this Task task)
        {
            return task.ConfigureAwait(false);
        }

        public static ConfiguredTaskAwaitable<T> NoSyncBack<T>(this Task<T> task)
        {
            return task.ConfigureAwait(false);
        }
    }
}
