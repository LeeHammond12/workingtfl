﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using PCLStorage;
using PCLStorage.Exceptions;
using FileNotFoundException = PCLStorage.Exceptions.FileNotFoundException;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class IFolderExtensions
    {
        public static async Task<bool> FolderExists(this IFolder baseFolder, string folderName)
        {
            try
            {
                return await baseFolder.GetFolderAsync(folderName).NoSyncBack() != null;
            }
            catch (DirectoryNotFoundException)
            {
                return false;
            }
        }

        public static async Task<bool> FileExists(this IFolder baseFolder, string fileName)
        {
            try
            {
                return await baseFolder.GetFileAsync(fileName).NoSyncBack() != null;
            }
            catch (FileNotFoundException)
            {
                return false;
            }
        }

        public static async Task<Stream> OpenStream(this IFolder baseFolder, string fileName, FileAccess access)
        {
            return await (await baseFolder.GetFileAsync(fileName).NoSyncBack()).OpenAsync(FileAccess.ReadAndWrite).NoSyncBack();
        }

        public static async Task DeleteSubFolders(this IFolder baseFolder)
        {
            var subFolders = await baseFolder.GetFoldersAsync().NoSyncBack();
            foreach (var subFolder in subFolders)
            {
                await subFolder.DeleteAsync().NoSyncBack();
            }
        }

        /// <summary>
        /// Recursive copy of files and folders from source to destination.
        /// </summary>
        public static async Task CopyContentsRecursive(this IFolder source, IFolder dest)
        {
            await CopyContentsShallow(source, dest).NoSyncBack();

            var subfolders = await source.GetFoldersAsync().NoSyncBack();
            foreach (var storageFolder in subfolders)
            {
                await storageFolder.CopyContentsRecursive(await dest.GetFolderAsync(storageFolder.Name).NoSyncBack()).NoSyncBack();
            }
        }

        /// <summary>
        /// Shallow copy of files and folders from source to destination.
        /// </summary>
        public static async Task CopyContentsShallow(this IFolder source, IFolder destination)
        {
            await source.CopyFiles(destination).NoSyncBack();

            var items = await source.GetFoldersAsync().NoSyncBack();

            foreach (var storageFolder in items)
            {
                await destination.CreateFolderAsync(storageFolder.Name, CreationCollisionOption.ReplaceExisting).NoSyncBack();
            }
        }

        /// <summary>
        /// Copy files from source into destination folder.
        /// </summary>
        private static async Task CopyFiles(this IFolder source, IFolder destination)
        {
            var items = await source.GetFilesAsync().NoSyncBack();

            foreach (var storageFile in items)
            {
                var destFile = await destination.CreateFileAsync(storageFile.Name, CreationCollisionOption.ReplaceExisting).NoSyncBack();

                using (var destStream = await destFile.OpenAsync(FileAccess.ReadAndWrite).NoSyncBack())
                using (var srcStream = await storageFile.OpenAsync(FileAccess.Read).NoSyncBack())
                {
                    srcStream.CopyTo(destStream);
                }
            }
        }

        ///  <summary>
        ///
        ///  </summary>
        ///  <param name="baseFolder"></param>
        ///  <param name="folderAction">may be null</param>
        ///  <param name="fileAction">may be null</param>
        /// <param name="recursive"></param>
        /// <param name="token"></param>
        ///  <returns></returns>
        public static async Task<List<Exception>> PerformActionOnFiles(
                                            this IFolder baseFolder,
                                            Func<IFile, Task> fileAction,
                                            bool recursive,
                                            CancellationToken token = default(CancellationToken))
        {
            var exceptions = new List<Exception>();
            if (fileAction != null)
            {
                var files = await baseFolder.GetFilesAsync(token).NoSyncBack();
                foreach (var file in files)
                {
                    Debug.WriteLine(file.Path);
                    try
                    {
                        await fileAction(file).NoSyncBack();
                    }
                    catch (Exception ex)
                    {
                        exceptions.Add(ex);
                    }
                }
            }

            if (recursive)
            {
                var subFolders = await baseFolder.GetFoldersAsync(token).NoSyncBack();
                foreach (var subFolder in subFolders)
                {
                    Debug.WriteLine(subFolder.Path);
                    try
                    {
                        exceptions.AddRange(await subFolder.PerformActionOnFiles(fileAction, true, token).NoSyncBack());
                    }
                    catch (Exception ex)
                    {
                        exceptions.Add(ex);
                    }
                }
            }
            return exceptions;
        }
    }
}