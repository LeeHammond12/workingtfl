﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class ListExtensions
    {
        private static readonly Random Random = new Random(DateTime.Now.Millisecond);

        public static void DisposeAll(this IEnumerable l)
        {
            l.OfType<IDisposable>().ForEach(f => f.Dispose());
        }

        /// <summary>
        /// Adds the specified element at the end of the IEnummerable.
        /// </summary>
        /// <typeparam name="T">The type of elements the IEnumerable contans.</typeparam>
        /// <param name="target">The target.</param>
        /// <param name="item">The item to be concatenated.</param>
        /// <returns>An IEnumerable, enumerating first the items in the existing enumerable</returns>
        public static IEnumerable<T> Concat<T>(this IEnumerable<T> target, T item)
        {
            foreach (T t in target) yield return t;
            yield return item;
        }

        public static List<T> EnsureNElements<T>(this List<T> l, int n, Func<T> createFunc)
        {
            if (l == null)
                l = new List<T>();

            while (l.Count < n)
            {
                l.Add(createFunc());
            }

            while (l.Count > n)
            {
                l.RemoveAt(l.Count - 1);
            }

            return l;
        }

        // get min element
        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector)
        {
            return source.MinBy(selector, Comparer<TKey>.Default);
        }

        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            using (IEnumerator<TSource> sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    throw new InvalidOperationException("Sequence was empty");
                }
                TSource min = sourceIterator.Current;
                TKey minKey = selector(min);
                while (sourceIterator.MoveNext())
                {
                    TSource candidate = sourceIterator.Current;
                    TKey candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, minKey) < 0)
                    {
                        min = candidate;
                        minKey = candidateProjected;
                    }
                }
                return min;
            }
        }

        /// <summary>
        /// Fors the each.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="action">The action.</param>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
            {
                action(item);
            }
        }

        public static void ForEachWithLock<T>(this IEnumerable<T> source, object lockObj, Action<T> action)
        {
            lock (lockObj)
            {
                foreach (var item in source)
                {
                    action(item);
                }
            }
        }

        /// <summary>
        /// Toes the observable collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> source)
        {
            var result = new ObservableCollection<T>();

            foreach (T item in source)
            {
                result.Add(item);
            }

            return result;
        }
    }
}
