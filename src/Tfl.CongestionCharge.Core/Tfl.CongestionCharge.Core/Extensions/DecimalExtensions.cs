﻿using System.Globalization;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class DecimalExtensions
    {
        public static string ToCurrencyString(this decimal source)
        {
            return string.Format(new CultureInfo("en-GB"), "{0:C}", source);
        }
    }
}