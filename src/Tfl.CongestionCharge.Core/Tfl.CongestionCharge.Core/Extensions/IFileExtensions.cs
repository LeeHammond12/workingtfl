﻿using System.Threading.Tasks;
using PCLStorage;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class IFileExtensions
    {
        public static async Task CopyTo(this IFile file, IFile targetFile)
        {
            using (var stream = await file.OpenAsync(FileAccess.Read).NoSyncBack())
            using (var targetStream = await targetFile.OpenAsync(FileAccess.ReadAndWrite).NoSyncBack())
            {
                await stream.CopyToAsync(targetStream);
            }
        }

        public static async Task WriteAllBytes(this IFile file, byte[] bytes)
        {
            using (var stream = await file.OpenAsync(FileAccess.ReadAndWrite).NoSyncBack())
            {
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        public static async Task<byte[]> ReadAllBytes(this IFile file)
        {
            using (var stream = await file.OpenAsync(FileAccess.Read).NoSyncBack())
            {
                return stream.ReadAllBytes();
            }
        }
    }
}