﻿using System;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class ExceptionExtensions
    {
        public static T GetInnerMostExceptionOfType<T>(this Exception current) where T : Exception
        {
            T exception = current as T;

            while (current.InnerException != null)
            {
                current = current.InnerException;

                var tEx = current as T;
                if (tEx != null)
                {
                    exception = tEx;
                }
            }

            return exception;
        }
    }
}