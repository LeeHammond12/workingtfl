﻿using System;
using Humanizer;

namespace Tfl.CongestionCharge.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToSitDateFormat(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        public static string ToSitDateFormat(this DateTime? dt)
        {
            return dt.HasValue ? dt.Value.ToSitDateFormat() : string.Empty;
        }

        public static string ToHumanReadableFormat(this DateTime dt)
        {
            return dt.GetFormattedDate();
        }

        public static string ToShortDateFormat(this DateTime date)
        {
            var year = date.Year.ToString();
            var month = date.Month.ToString();
            if (date.Month < 10)
            {
                month = "0" + month;
            }

            var shortDate = year.Length == 4
                ? $"{month}/{year.Substring(2, 2)}"
                : year;
            return shortDate;
        }

        private static string GetFormattedDate(this DateTime date)
        {
            return $"{date.Day.Ordinalize()} {date.ToString("MMM")} '{date.ToString("yy")}";
        }
    }
}