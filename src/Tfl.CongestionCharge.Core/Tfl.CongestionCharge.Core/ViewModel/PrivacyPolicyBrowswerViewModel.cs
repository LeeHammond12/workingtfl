﻿using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel
{
    public class PrivacyPolicyBrowswerViewModel : TflViewModelBase, IBrowserViewModel
    {
        public string PageTitle => Strings.PrivacyPageTitle;

        public string NavigationUri => Strings.PrivacyNoticeLink;

        public override bool IsGoBackToHomeButtonVisible => false; 

        public PrivacyPolicyBrowswerViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }
    }
}