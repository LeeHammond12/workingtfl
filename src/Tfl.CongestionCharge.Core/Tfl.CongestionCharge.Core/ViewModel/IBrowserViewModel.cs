﻿using GalaSoft.MvvmLight.Command;

namespace Tfl.CongestionCharge.Core.ViewModel
{
    public interface IBrowserViewModel
    {
        string NavigationUri { get; }
        RelayCommand GoBackCommand { get; }
    }
}