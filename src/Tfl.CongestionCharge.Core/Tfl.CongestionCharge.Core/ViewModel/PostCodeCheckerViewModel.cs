﻿using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.Validators;

namespace Tfl.CongestionCharge.Core.ViewModel
{
    public class PostCodeCheckerViewModel : TflViewModelBase
    {
        private const string ViewStateKey = "PostCodeCheckerViewState";

        public class ViewStateModel
        {
            public string PostCode { get; set; }

            public string BuildingNumber { get; set; }

            public string BuildingName { get; set; }
        }

        public override bool IsGoBackToHomeButtonVisible => false; 

        private RelayCommand _lookupPostCodeCommand;

        public string PageTitle => Strings.CheckAPostCode;

        public ViewStateModel ViewState { get; set; }

        public string PostCode
        {
            get { return ViewState.PostCode; }
            set { ViewState.PostCode = value; }
        }

        public string BuildingNumber
        {
            get { return ViewState.BuildingNumber; }
            set { ViewState.BuildingNumber = value; }
        }

        public string BuildingName
        {
            get { return ViewState.BuildingName; }
            set { ViewState.BuildingName = value; }
        }

        public bool CanEnterBuildingNumber => string.IsNullOrWhiteSpace(BuildingName);

        public bool CanEnterBuildingName => string.IsNullOrWhiteSpace(BuildingNumber);

        public string InsideCcZoneResult { get; set; }

        public string InsideResidentZoneResult { get; set; }

        public RelayCommand LookupPostCodeCommand
        {
            get
            {
                return _lookupPostCodeCommand
                    ?? (_lookupPostCodeCommand = new RelayCommand(async () => await LookupPostCode(), () => CanLookupPostcode));
            }
        }

        public bool CanLookupPostcode => (!string.IsNullOrWhiteSpace(BuildingNumber) ||
            !string.IsNullOrWhiteSpace(BuildingName)) &&
            !string.IsNullOrWhiteSpace(PostCode);

        public PostCodeCheckerViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
            ViewState = new ViewStateModel();
        }

        public override void SaveUiState(IKeyValueStore store)
        {
            base.SaveUiState(store);
            store.Backup(ViewStateKey, ViewState);
        }

        public override void RestoreUiState(IKeyValueStore store)
        {
            base.RestoreUiState(store);
            ViewState = store.Restore<ViewStateModel>(ViewStateKey, new ViewStateModel());
            RaisePropertyChanged();
        }

        private void OnBuildingNameChanged()
        {
            RaisePropertyChanged(() => CanEnterBuildingNumber);
        }

        private void OnBuildingNumberChanged()
        {
            RaisePropertyChanged(() => CanEnterBuildingName);
        }

        private async Task LookupPostCode()
        {
            try
            {
                PostCode = PostCode.ToUpper().Trim();

                var validator = new PostCodeViewModelValidator();
                ValidationResult = validator.Validate(this);
                if (!ValidationResult.IsValid)
                {
                    await DialogService.ShowError(string.Join("\n\n", ValidationResult.Errors));
                    return;
                }

                PostcodeDetailDto response = null;

                IsBusy = true;

                if (!string.IsNullOrWhiteSpace(BuildingNumber))
                {
                    response = await AppEnvironment.TflApi.GetPostcodeDetailByBuildingNumber(PostCode, BuildingNumber);
                }
                else if (!string.IsNullOrWhiteSpace(BuildingName))
                {
                    response = await AppEnvironment.TflApi.GetPostcodeDetailByBuildingName(PostCode, BuildingName);
                }

                IsBusy = false;

                if (response != null)
                {
                    InsideCcZoneResult = response.InsideCcZone
                        ? Strings.AddressInsideCcZone
                        : Strings.AddressOutsideCcZone;

                    InsideResidentZoneResult = response.InsideResidentsZone
                        ? Strings.AddressInsideResidentZone
                        : Strings.AddressOutsideResidentZone;

                    await DialogService.ShowMessage(InsideCcZoneResult + InsideResidentZoneResult, string.Empty);
                }
                else
                {
                    InsideCcZoneResult = string.Empty;
                    InsideResidentZoneResult = string.Empty;

                    await DialogService.ShowError(Strings.CouldNotFindAddress);
                }
            }
            catch (ClientApiException ex)
            {
                await ClientApiExceptionHandler.HandleErrors(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}