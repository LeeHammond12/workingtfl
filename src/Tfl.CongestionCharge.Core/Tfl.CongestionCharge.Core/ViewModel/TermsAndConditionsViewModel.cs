﻿using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel
{
    public class TermsAndConditionsViewModel : TflViewModelBase, IBrowserViewModel
    {
        private RelayCommand _continueCommand;
        private RelayCommand _acceptCommand;

        public override bool IsGoBackToHomeButtonVisible => UserHasAcceptedTermsAndConditions;

        public bool UserHasAcceptedTermsAndConditions
        {
            get { return AppState.UserHasAcceptedTermsAndConditions; }
            set { AppState.UserHasAcceptedTermsAndConditions = value; }
        }

        public string PageTitle => Strings.TermsAndConditionsTitle;

        public string NavigationUri => Strings.PrivacyNoticeLink;

        public string PrivacyPageTitle => Strings.PrivacyPageTitle;

        public string PrivacyPageContent => Strings.PrivacyPageContent;

        public RelayCommand AcceptCommand
            => _acceptCommand ?? (_acceptCommand = new RelayCommand(Accept));

        public RelayCommand Continue
            => _continueCommand ?? (_continueCommand = new RelayCommand(ContinueFromPrivacyPage));

        public TermsAndConditionsViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        private void ContinueFromPrivacyPage()
        {
            UserHasAcceptedTermsAndConditions = true;
            NavigationService.NavigateHomeAndClearBackStack();
        }

        public async void Accept()
        {
            if (UserHasAcceptedTermsAndConditions)
            {
                NavigationService.GoBackOnePage();
            }
            else
            {
                var goToLogin = await AppEnvironment.Current.DialogService.Show2ButtonMessage(
                     Strings.DoYouWantToLogin,
                     Strings.Information,
                     Strings.Yes, Strings.No);

                GoToLoginPageOrAnonymousHome(goToLogin);
            }
        }

        private void GoToLoginPageOrAnonymousHome(bool goToLogin)
        {
            UserHasAcceptedTermsAndConditions = true;
            if (goToLogin)
            {
                NavigationService.NavigateToLoginAndClearBackStack();
            }
            else
            {
                NavigationService.NavigateHomeAndClearBackStack();
            }
        }
    }
}