﻿using System;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Api.Axis;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Helper;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.Payments;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Validators;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.Payments
{
    public abstract class PaymentViewModelBase : TflViewModelBase
    {
        private string _smsNumber = string.Empty;
        private string _cardNumber = string.Empty;
        private string _expiryDate = string.Empty;

        private PaymentService PaymentService => AppEnvironment.PaymentService;

        protected PaymentFlowState Payment => FlowState.PaymentFlowState;

        public string Cvv2 { get; set; } = string.Empty;

        public bool CanSaveDetails => !AppState.Auth.IsLoggedIn;

        public bool SaveDetails { get; set; }

        public string PaymentButtonText => Strings.MakePaymentOf + Payment.TotalPrice.ToCurrencyString();

        public bool IsPcn => Payment.ChargeType == ChargeType.PenaltyChargeNotice;

        public bool IsSmsTextVisible { get; set; }

        public bool SendReceiptViaSmsVisible => !IsPcn;

        protected string Last4Digits { get; set; } = string.Empty;

        public string CardNumber
        {
            get { return _cardNumber; }
            set
            {
                _cardNumber = value.Replace(" ", string.Empty).Replace("-", string.Empty);

                var type = string.Empty;

                if (_cardNumber.Length == 13 || _cardNumber.Length == 15 || _cardNumber.Length == 16)
                {
                    Last4Digits = _cardNumber.Substring(_cardNumber.Length - 4, 4);
                    type = CardUtils.GetCardTypeFromNumber(_cardNumber);
                }

                if (string.IsNullOrEmpty(type))
                {
                    type = CardTypeGuesser.GuessCardType(_cardNumber);
                }

                Issuer = type;
            }
        }

        public string Issuer { get; private set; }

        public string NameOnCard { get; set; }

        public string ExpiryDate
        {
            get
            {
                return _expiryDate.Length <= 1 ? string.Empty : _expiryDate;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var date = value.Replace(" ", string.Empty).Replace("/", string.Empty);
                    _expiryDate = date;
                }
            }
        }

        public string SmsNumber
        {
            get { return _smsNumber; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var strip = value.Replace(" ", string.Empty).Replace("-", string.Empty);
                    _smsNumber = strip;
                }
            }
        }

        protected PaymentViewModelBase(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
            _smsNumber = AppState.SmsNumber ?? string.Empty;
        }

        // for stored cards we don't need a number, just a token
        protected async Task<bool> PayWithCard(string templateNumber)
        {
            Validate(templateNumber);

            if (!ValidationResult.IsValid)
            {
                await DialogService.ShowError(string.Join("\n\n", ValidationResult.Errors));
                return false;
            }

            bool success = false;

            var command = new PaymentRequestDto
            {
                //AccountNumber = AppEnvironment.AuthService.AccountNumber,
                TransactionDate = DateTime.Now,
                CardNumber = CardNumber,
                NameOnCard = NameOnCard,
                CardNumberLastFourDigits = Last4Digits,
                CardExpiryDate = ExpiryDate,
                TemplateNumber = templateNumber,
                MobileNumber = SmsNumber,
                CardSecurityCode = Cvv2,
                Amount = Payment.TotalPrice,
                Issuer = Issuer
            };

            try
            {
                IsBusy = true;

                await PaymentService.MakePayment(command, Payment, SaveDetails);

                AppState.SmsNumber = _smsNumber;

                IsBusy = false;

                Payment.ReceiptNumber = command.ReceiptId;
                Payment.PaymentDate = command.TransactionDate;

                success = true;
            }
            catch (PaymentApiException ex)
            {
                IsBusy = false;

                await ShowErrorMessage(ex);
            }

            return success;
        }

        protected void Validate(string templateNumber)
        {
            var validator = new PaymentValidator(!string.IsNullOrEmpty(templateNumber));
            ValidationResult = validator.Validate(this);
        }

        private async Task ShowErrorMessage(PaymentApiException ex)
        {
            // handle invalid api key and application error messages from client api
            if (ex.InnerClientApiException != null)
            {
                if (ex.InnerClientApiException.IsAuthenticationException)
                {
                    NavigationService.NavigateToLoginAndClearBackStack();
                    return;
                }

                if (await ClientApiExceptionHandler.HandleInvalidApiKey(ex.InnerClientApiException))
                {
                    return;
                }

                if (await ClientApiExceptionHandler.HandleApplicationErrors(ex.InnerClientApiException))
                {
                    return;
                }
            }

            if (ex.IsNoNetworkConnectedException)
            {
                await ShowTransactionFailedMessage();
                return;
            }

            await ShowPaymentFailedMessage();
        }

        private async Task ShowPaymentFailedMessage()
        {
            var message = IsPcn ? Strings.PcnPaymentFailedMessage : Strings.CcPaymentFailedMessage;
            var title = IsPcn ? Strings.ErrorTitle : Strings.CcPaymentFailedTitle;
            await DialogService.ShowOkMessageWithLink(message, title);
        }

        private async Task ShowTransactionFailedMessage()
        {
            await DialogService.ShowOkMessageWithLink(Strings.TransactionFailedMessage, Strings.TransactionFailedTitle);
        }

        private void OnPaymentChanged()
        {
            RaisePropertyChanged(() => IsPcn);
            RaisePropertyChanged(() => IsSmsTextVisible);
        }

        protected void DEBUG_SetPaymentFlowStateIfNotSet()
        {
            if (Payment == null)
            {
                FlowState.StartPcnPaymentFlow();
                Payment.Inner = new PcnState
                {
                    PcnNumber = "123456",
                    Price = 12,
                    Vrm = "VRM1",
                };
                Payment.ReceiptNumber = "MOCK 123";
            }
        }
    }
}