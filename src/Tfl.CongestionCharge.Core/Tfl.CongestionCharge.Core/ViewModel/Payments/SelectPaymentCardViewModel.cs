﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel.Payments
{
    public class SelectPaymentCardViewModel : PaymentViewModelBase
    {
        private RelayCommand _chooseAnotherCardCommand;

        public string PageTitle => IsPcn ? Strings.PayPenaltyChargeNotice : Strings.PayCongestionCharge;

        public IReadOnlyList<PaymentCardViewModel> UserPaymentCards => UserInformation.PaymentCards;

        public RelayCommand ChooseAnotherCardCommand => _chooseAnotherCardCommand ?? (_chooseAnotherCardCommand =
                    new RelayCommand(() => NavigationService.NavigateTo(AppPage.Payment, this)));

        public SelectPaymentCardViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            if (await LoadUserInformationWithRetry() != ViewModelInitResult.Success)
                return ViewModelInitResult.Abort;
#if DEBUG
            DEBUG_SetPaymentFlowStateIfNotSet();
#endif
            if (Payment == null)
            {
                NavigationService.NavigateHomeAndClearBackStack();
                return ViewModelInitResult.Abort;
            }

            return ViewModelInitResult.Success;
        }

        public async Task<bool> PayWithCard(PaymentCardViewModel card)
        {
            if (card == null)
                return false;

            Last4Digits = card.Last4Digits;
            CardNumber = "**** **** **** " + Last4Digits;
            NameOnCard = card.NameOnCard;
            ExpiryDate = card.ExpiryDate;

            var success = await PayWithCard(card.TemplateNumber);
            if (success)
            {
                NavigationService.NavigateTo(AppPage.AllDone, this);
            }
            return success;
        }
    }
}