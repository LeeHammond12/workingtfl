﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel.Payments
{
    public class PaymentViewModel : PaymentViewModelBase
    {
        private RelayCommand _makePaymentCommand;

        public string PageTitle => IsPcn ? Strings.PayPenaltyChargeNotice : Strings.PayCongestionCharge;

        public bool CanProceedWithPayment => !string.IsNullOrEmpty(Cvv2) &&
                                             !string.IsNullOrEmpty(CardNumber) &&
                                             !string.IsNullOrEmpty(ExpiryDate) &&
                                             !string.IsNullOrEmpty(NameOnCard) &&
                                             !IsBusy;

        public IReadOnlyList<PaymentCardViewModel> UserPaymentCards => UserInformation.PaymentCards;

        public RelayCommand MakePaymentCommand => _makePaymentCommand ?? (_makePaymentCommand =
                    new RelayCommand(async () => await PayWithCard(), () => CanProceedWithPayment));

        public PaymentViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

//Suppress warning CS1998: This async method lacks 'await'
#pragma warning disable 1998
        public override async Task<ViewModelInitResult> InitViewModel()
        {
#if DEBUG
            DEBUG_FillDummyCardDetails();
            DEBUG_SetPaymentFlowStateIfNotSet();
#endif
            if (Payment == null)
            {
                NavigationService.NavigateHomeAndClearBackStack();
                return ViewModelInitResult.Abort;
            }

            return ViewModelInitResult.Success;
        }
#pragma warning restore 1998

        public async Task<bool> PayWithCard()
        {
            var success = await PayWithCard(null);
            if (success)
            {
                NavigationService.NavigateTo(AppPage.AllDone, this);
            }
            return success;
        }

        private void DEBUG_FillDummyCardDetails()
        {
            // *****************
            NameOnCard = "Joe Bloggs";
            CardNumber = "4543059790016721";
            ExpiryDate = "1218";
            Cvv2 = "587";
        }
    }
}