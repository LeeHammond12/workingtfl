﻿using System;
using System.Collections.Generic;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.ViewModel.Receipts
{
    public class ReceiptViewModel
    {
        public bool IsPcn { get; }
        public DateTime Date { get; }

        public List<KeyValuePair<string, string>> Fields { get; }

        public ReceiptViewModel(StoredReceipt receipt)
        {
            IsPcn = receipt.IsPcn;
            Date = receipt.PaymentDate;

            Fields = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(Strings.TypeColon, receipt.IsPcn ? Strings.Pcn : Strings.CongestionChargeCc),
                new KeyValuePair<string, string>(Strings.VrmColon, receipt.Vrm),
                new KeyValuePair<string, string>(Strings.ReceiptNoColon, receipt.ReceiptId),
                new KeyValuePair<string, string>(Strings.AmountPaid, receipt.Price.ToCurrencyString()),
                new KeyValuePair<string, string>(Strings.PaymentDate, receipt.PaymentDate.ToHumanReadableFormat()),
            };

            if (receipt.IsPcn)
            {
                Fields.Add(new KeyValuePair<string, string>(Strings.ReceiptPcnNumber, receipt.PcnNumber));
            }
            else
            {
                Fields.Add(new KeyValuePair<string, string>(Strings.ChargeType, receipt.PeriodType.ToString()));
                Fields.Add(new KeyValuePair<string, string>(Strings.JourneyDates, receipt.DateString));
            }
            //else
            //{
            //    Fields.Insert(2, new KeyValuePair<string, string>(Strings.ChargingDate, receipt.ChargingDate.ToHumanReadableFormat()));
            //}
        }

        //public ReceiptViewModel(ReceiptDto receipt)
        //{
        //    var dateString = receipt.Date;

        //    try
        //    {
        //        Date = DateTime.Parse(dateString);
        //        dateString = Date.ToHumanReadableFormat();
        //    }
        //    catch (Exception ex)
        //    {
        //        Dbg.BreakAndLog(ex);
        //    }

        //    Fields = new List<KeyValuePair<string, string>>
        //    {
        //        new KeyValuePair<string, string>(Strings.VrmColon, receipt.Vehicle.VRM),
        //        //new KeyValuePair<string, string>(Strings.ChargingDate, receipt.ChargingDate.ToHumanReadableFormat()),
        //        new KeyValuePair<string, string>(Strings.ReceiptNoColon, receipt.Id),
        //        new KeyValuePair<string, string>(Strings.AmountPaid, "£" + receipt.TotalAmount),
        //        new KeyValuePair<string, string>(Strings.PaymentDate, dateString),
        //    };
        //}
    }
}