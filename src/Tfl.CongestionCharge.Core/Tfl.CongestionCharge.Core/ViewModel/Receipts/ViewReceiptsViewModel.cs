﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel.Receipts
{
    public class ViewReceiptsViewModel : TflViewModelBase
    {
        public string PageTitle => Strings.ViewReceipts;

        public override bool IsGoBackToHomeButtonVisible => false;

        public IReadOnlyList<ReceiptViewModel> Receipts { get; private set; } = new List<ReceiptViewModel>();

        public ViewReceiptsViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            if (IsLoggedIn)
            {
                NavigationService.NavigateHomeAndClearBackStack();
                return ViewModelInitResult.Abort;
            }

            Receipts = await ExecuteUntilResult(async () => await AnonymousUser.GetReceipts());
            return Receipts == null ?
                ViewModelInitResult.Abort :
                ViewModelInitResult.Success;
        }
    }
}