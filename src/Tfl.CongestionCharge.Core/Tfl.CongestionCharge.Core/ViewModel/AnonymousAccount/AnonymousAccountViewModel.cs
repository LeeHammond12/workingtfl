﻿using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel.AnonymousAccount
{
    public class AnonymousAccountViewModel : TflViewModelBase
    {
        private RelayCommand _navigateToVehiclesCommand;
        private RelayCommand _navigateToPaymentMethodsCommand;

        public RelayCommand NavigateToVehiclesCommand
                => _navigateToVehiclesCommand
                       ?? (_navigateToVehiclesCommand = new RelayCommand(
                           () => NavigationService.NavigateTo(AppPage.YourVehiclesAnonymousUser, this)));

        public RelayCommand NavigateToYourPaymentMethodsCommand
                => _navigateToPaymentMethodsCommand
                       ?? (_navigateToPaymentMethodsCommand = new RelayCommand(
                           () => NavigationService.NavigateTo(AppPage.AnonymousUserYourPaymentMethods, this)));

        public override bool IsGoBackToHomeButtonVisible => false; 

        public AnonymousAccountViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            await LoadUserInformationWithRetry();
            return ViewModelInitResult.Success;
        }

        public async Task RemoveVehicle(VehicleViewModel vehicle)
        {
            var text = string.Format(Strings.DoYouReallyWantToRemoveVehicleX, vehicle.Vrm);
            if (await DialogService.Show2ButtonMessage(text, Strings.Confirm, Strings.Yes, Strings.No))
            {
                await UserInformation.RemoveVehicle(vehicle?.Vrm);
            }
        }

        public async Task RemovePaymentCard(PaymentCardViewModel card)
        {
            var text = string.Format(Strings.DoYouReallyWantToRemoveCardX, card.Last4Digits);
            if (await DialogService.Show2ButtonMessage(text, Strings.Confirm, Strings.Yes, Strings.No))
            {
                await AnonymousUser.RemovePaymentCard(card);
            }
        }
    }
}
