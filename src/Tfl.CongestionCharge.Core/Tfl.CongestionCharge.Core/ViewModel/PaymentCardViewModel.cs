﻿using Tfl.CongestionCharge.Core.Model.AccountInfo;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.ViewModel
{
    public class PaymentCardViewModel
    {
        public string Last4Digits { get; set; }

        public string CardType { get; set; }

        public string IssuerWithCardPostfix => Issuer?.Replace("card", string.Empty) + " Card";

        public string NameOnCard { get; set; }

        public string ExpiryDate { get; set; }

        public string ExpiryDateWithSeparator => ExpiryDate?.Length == 4
            ? $"{ExpiryDate.Substring(0, 2)}/{ExpiryDate.Substring(2, 2)}"
            : ExpiryDate;

        public string TemplateNumber { get; set; }

        public string CardNumber => "**** **** **** " + Last4Digits;

        public string Issuer { get; set; }

        public bool IsAutoPayCard { get; set; }

        public PaymentCardViewModel()
        {
        }

        public PaymentCardViewModel(StoredCard p)
        {
            Last4Digits = p.Last4Digits;
            ExpiryDate = p.ExpiryDate;
            TemplateNumber = p.Token;

            Issuer = !string.IsNullOrEmpty(p.Issuer) ? p.Issuer : "Unknown";
        }

        public PaymentCardViewModel(AccountPaymentCardDto p)
        {
            TemplateNumber = p.Token;
            Last4Digits = p.Last4Digits;
            ExpiryDate = p.ExpiryDate;

            Issuer = !string.IsNullOrEmpty(p.Issuer) ? p.Issuer : "Unknown";
            CardType = p.CardType;
            IsAutoPayCard = p.IsAutoPayCard;
        }
    }
}