﻿using GalaSoft.MvvmLight;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.PayPcn
{
    public class ConfirmPCNViewModel : ViewModelBase
    {
        public JourneyDetails Vrm { get; set; }

        public StoredReceipt Receipt { get; set; }
    }
}