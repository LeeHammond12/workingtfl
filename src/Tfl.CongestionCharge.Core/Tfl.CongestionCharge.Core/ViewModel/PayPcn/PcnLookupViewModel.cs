﻿using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Validators;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.PayPcn
{
    public class PcnLookupViewModel : TflViewModelBase
    {
        private RelayCommand _lookupPCNCommand;

        public string PageTitle => Strings.PayPenaltyChargeNotice;

        public string ButtonText => CanProceedToPayment ? Strings.EnterPaymentDetails : Strings.Continue;

        public override bool IsGoBackToHomeButtonVisible => false;

        public string PcnNumber
        {
            get { return Pcn.PcnNumber; }
            set
            {
                Pcn.PcnNumber = value;
                Reset();
            }
        }

        public string VrmNumber
        {
            get { return Pcn.Vrm; }
            set
            {
                Pcn.Vrm = value;
                Reset();
            }
        }

        public PaymentFlowState Payment => FlowState.PaymentFlowState;

        public PcnState Pcn => Payment?.Pcn;

        public decimal Amount
        {
            get { return Pcn.Price; }
            set
            {
                Pcn.Price = value;

                RaisePropertyChanged(() => AmountYouOweString);
                RaisePropertyChanged(() => ButtonText);
            }
        }

        public string AmountYouOweString => "£" + Pcn.Price.ToString("F");

        public RelayCommand LookupPcnOrGoToPaymentCommand =>
            _lookupPCNCommand ?? (_lookupPCNCommand = new RelayCommand(async () => await LookupPcnOrGoToPayment(), () => CanLookupPcnOrGoToPayment));

        public bool CanLookupPcnOrGoToPayment =>
            !string.IsNullOrEmpty(PcnNumber) &&
            !string.IsNullOrEmpty(VrmNumber) &&
            // button should be greyed out when PCN was looked up and Amount == 0
            (!CanProceedToPayment || (CanProceedToPayment && Amount > 0)) &&
            !IsBusy;

        public bool CanProceedToPayment { get; set; }

        public PcnLookupViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        // the view has 2 steps, goes back one step or to previous page
        public void OnBackNavigationRequested()
        {
            if (CanProceedToPayment)
            {
                PcnNumber = string.Empty;
                VrmNumber = string.Empty;
                Reset();
            }
            else
            {
                NavigationService.NavigateHomeAndClearBackStack();
            }
        }

        public void Reset()
        {
            Amount = 0;
            CanProceedToPayment = false;

            RaisePropertyChanged(() => CanLookupPcnOrGoToPayment);
            RaisePropertyChanged(() => AmountYouOweString);
            RaisePropertyChanged(() => ButtonText);
        }

        private async Task LookupPcnOrGoToPayment()
        {
            // needed because the same command is reused for lookup and proceed to payment
            if (CanProceedToPayment)
            {
                NavigateToPaymentPage();
                return;
            }

            var validator = new LookupPcnViewModelValidator();
            ValidationResult = validator.Validate(this);

            if (ValidationResult.IsValid)
            {
                try
                {
                    IsBusy = true;

                    var pcn = StripSpaceAndToUpper(PcnNumber);
                    if (pcn.StartsWith("L"))
                    {
                        await DialogService.ShowOkMessageWithLink(Strings.YouCannotPayThisPcn, Strings.Warning);
                        NavigationService.NavigateHomeAndClearBackStack();
                        return;
                    }

                    var response = await AppEnvironment.TflApi.LookupPcn(pcn, StripSpaceAndToUpper(Pcn.Vrm));

                    IsBusy = false;

                    CanProceedToPayment = response.CanBePaid;

                    if (!CanProceedToPayment)
                    {
                        await DialogService.ShowOkMessageWithLink(Strings.YouCannotPayThisPcn, Strings.Warning);
                        NavigationService.NavigateHomeAndClearBackStack();
                        return;
                    }

                    Amount = response.AmountDue;
                }
                catch (ClientApiException ex)
                {
                    IsBusy = false;

                    Amount = 0;

                    await ClientApiExceptionHandler.HandleErrors(ex);
                }
            }
            else
            {
                await DialogService.ShowError(string.Join("\n\n", ValidationResult.Errors));
            }
        }

        private void NavigateToPaymentPage()
        {
            Pcn.PcnNumber = StripSpaceAndToUpper(PcnNumber);
            NavigateToPaymentPage(ChargeType.PenaltyChargeNotice);
        }

        private string StripSpaceAndToUpper(string s)
        {
            return s.ToUpper().Replace(" ", string.Empty);
        }
    }
}