﻿using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Validators;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public class LookupVrmViewModel : LookupOrConfirmVrmViewModelBase
    {
        private RelayCommand _lookupVRMCommand;

        public bool VrmIsValidated { get; set; }

        public bool ConfirmVrmIsCorrect { get; set; }

        public bool SaveVehicle { get; set; }

        private bool IsAutoPayFlow => State.Type == LookupVrmFlowState.FlowType.AutoPay;

        public string EnteredVrm
        {
            get { return State.Vehicle.Vrm; }
            set
            {
                State.Vehicle.Vrm = value;
                ClearVrmFlags();
            }
        }

        public string SanitizedVrm => EnteredVrm.ToUpper().Replace(" ", string.Empty);

        public RelayCommand LookupVRMCommand
            => _lookupVRMCommand ?? (_lookupVRMCommand = new RelayCommand(async () => await LookupVrm(), () => CanLookupVrm));

        public bool CanLookupVrm => !string.IsNullOrEmpty(EnteredVrm) && !IsBusy;

        public string PageTitle => State?.Type == LookupVrmFlowState.FlowType.CcPayment
            ? Strings.PayCongestionCharge
            : Strings.AddVehicleToAutoPay;

        public override bool IsGoBackToHomeButtonVisible => true; // FlowState.PaymentFlowState?.Cc?.GoBackToJourneyDetailsScreen == true;

        public LookupVrmViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public void GoBack()
        {
            if (FlowState.PaymentFlowState?.Cc?.GoBackToJourneyDetailsScreen == true)
            {
                NavigationService.NavigateTo(AppPage.JourneyDetails, this);
            }
            else
            {
                NavigationService.GoBackOnePage();
            }
        }

        private void ClearVrmFlags()
        {
            VrmIsValidated = false;
            ConfirmVrmIsCorrect = false;
        }

        private async Task LookupVrm()
        {
            var validator = new LookupVrmViewModelValidator();
            ValidationResult = validator.Validate(this);

            if (ValidationResult.IsValid)
            {
                IsBusy = true;

                try
                {
                    State.Vehicle = new VehicleViewModel()
                    {
                        Vrm = SanitizedVrm
                    };
                    RaisePropertyChanged(() => EnteredVrm);

                    ClearVrmFlags();

                    var lookupVrmDto = await TflApi.LookupVrm(SanitizedVrm, IsRegisteredInUk);

                    State.Vehicle = new VehicleViewModel(lookupVrmDto);

                    IsBusy = false;

                    await ShowMessages(lookupVrmDto);

                    VrmIsValidated = true;
                }
                catch (ClientApiException ex)
                {
                    IsBusy = false;

                    VrmIsValidated = ClientApiExceptionHandler.ContainsErrorCode(ex, ClientApiErrorCodes.VrmNoVehicleDetailsFound);

                    if (VrmIsValidated)
                    {
                        var isEsSubject = ex.Response?.IsEsChargeable == (int)EsChargeFlag.Subject;
                        await ShowUnknownVrmMessages(isEsSubject);
                    }
                    else
                    {
                        await ClientApiExceptionHandler.HandleErrors(ex);
                    }
                }
            }
            else
            {
                await DialogService.ShowError(string.Join("\n\n", ValidationResult.Errors));
            }

            if (VrmIsValidated)
            {
                NavigationService.NavigateTo(AppPage.ConfirmVrm, this);
            }

            RaiseAllCanExecuteChanged();
        }

        private async Task ShowMessages(LookupVrmDto lookupVrmDto)
        {
            var knownVrm = !lookupVrmDto.ModelMakeColorEmpty;
            var isEsSubject = lookupVrmDto.IsESChargeable == (int)EsChargeFlag.Subject;

            if (lookupVrmDto.IsCcChargeable)
            {
                if (knownVrm && isEsSubject)
                {
                    var text = IsAutoPayFlow
                        ? Strings.YourAutoPayVehicleIsSubjectToAnEmissionSurcharge
                        : Strings.YourVehicleIsSubjectToAnEmissionSurcharge;

                    await DialogService.ShowOkMessageWithLink(text, Strings.Warning);
                }
                else if (!knownVrm)
                {
                    await ShowUnknownVrmMessages(isEsSubject);
                }
            }
            else
            {
                var text = IsAutoPayFlow
                    ? Strings.AutoPayVehicleExemptMessage
                    : Strings.VehicleExemptMessage;

                if (isEsSubject)
                {
                    text = IsAutoPayFlow
                        ? Strings.EsAutoPayVehicleExemptMessage
                        : Strings.EsVehicleIsCcExempt;
                }

                await DialogService.ShowOkMessageWithLink(text, Strings.Warning);
            }
        }

        private async Task ShowUnknownVrmMessages(bool isEsSubject)
        {
            var text = Strings.CouldntFindVehicle;

            if (isEsSubject)
            {
                text = IsAutoPayFlow
                    ? Strings.CouldntFindVehicleDetailsForEsAutoPayVehicle
                    : Strings.CouldntFindVehicleDetailsForEsVehicle;
            }

            await DialogService.ShowOkMessageWithLink(text, Strings.Warning);
        }
    }
}