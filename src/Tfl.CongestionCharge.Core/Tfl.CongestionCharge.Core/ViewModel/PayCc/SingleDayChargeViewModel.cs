﻿using System;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.PayCc;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public class SingleDayChargeViewModel : ChargeViewModelBase
    {
        public DateTime? Date { get; set; }

        public override string DateString { get; }

        public override ChargePeriod ChargePeriod { get; }

        public SingleDayChargeViewModel(CcDetailSingleDay detail, ChargePeriod period)
            : base(detail)
        {
            Date = detail?.Date;
            DateString = Date != null ? Date.Value.ToHumanReadableFormat() : string.Empty;
            ChargePeriod = period;
        }
    }
}