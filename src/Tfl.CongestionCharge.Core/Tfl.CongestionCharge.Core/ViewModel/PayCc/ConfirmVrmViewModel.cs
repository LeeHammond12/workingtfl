﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public class ConfirmVrmViewModel : LookupOrConfirmVrmViewModelBase
    {
        private bool _showSaveCheckBox = false;

        private RelayCommand _nextCommand;

        public bool ConfirmVrmIsCorrect { get; set; }

        public bool SaveVehicle { get; set; }

        public bool IsCcPayment => State.Type == LookupVrmFlowState.FlowType.CcPayment;

        public bool ShowSaveCheckBox => _showSaveCheckBox;

        public bool CanSaveAndProceed =>
            !string.IsNullOrEmpty(Vehicle.Vrm) &&
            !IsBusy;

        public RelayCommand NextCommand
            => _nextCommand ?? (_nextCommand = new RelayCommand(async () => await Next(), () => CanSaveAndProceed));

        public string NextButtonText => IsCcPayment ? Strings.ChooseDates : Strings.Continue;

        public string PageTitle => State?.Type == LookupVrmFlowState.FlowType.CcPayment
            ? Strings.PayCongestionCharge
            : Strings.AddVehicleToAutoPay;

        public ConfirmVrmViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            if (!IsCcPayment)
            {
                return ViewModelInitResult.Success;
            }

            if (IsLoggedIn)
            {
                try
                {
                    if (!IsLoggedIn)
                    {
                        _showSaveCheckBox = true;
                    }
                    else
                    {
                        await LoggedInUser.UpdateInformation(true);
                        _showSaveCheckBox = LoggedInUser.VehiclesNotOnCcAutoPay.Count() < 10;
                    }
                }
                catch (Exception)
                {
                    _showSaveCheckBox = false;
                }
            }
            else
            {
                _showSaveCheckBox = true;
            }

            return ViewModelInitResult.Success;
        }

        private async Task Next()
        {
            try
            {
                if (!ConfirmVrmIsCorrect)
                {
                    await DialogService.ShowMessage(Strings.PromptUserToConfirmVrmIsCorrect, Strings.Information);
                    return;
                }

                if (IsCcPayment)
                {
                    await SaveAndChooseDates();
                }
                else
                {
                    await AddVehicleToAutoPayAndNavigate();
                }
            }
            catch (Exception)
            {
                await DialogService.ShowUnknownError();
            }
        }

        private async Task SaveAndChooseDates()
        {
            if (SaveVehicle)
            {
                IsBusy = true;

                try
                {
                    await UserInformation.AddVehicle(State.Vehicle);
                }
                catch (ClientApiException ex)
                {
                    IsBusy = false;

                    // ignore already existing vrm
                    if (!ClientApiExceptionHandler.ContainsErrorCode(ex, ClientApiErrorCodes.AlreadyHaveAPreferredVehicleWithGivenVrm))
                    {
                        if (ex.IsAuthenticationException)
                        {
                            NavigationService.NavigateToLoginAndClearBackStack();
                            return;
                        }
                        else if (ex.IsNoNetworkException)
                        {
                            await DialogService.ShowMessage(Strings.NoNetworkMessage, Strings.NoNetworkTitle);
                        }
                        else
                        {
                            await DialogService.ShowMessage(Strings.CouldNotSaveVehicleToQuickList, Strings.Warning);
                        }
                    }
                }
                finally
                {
                    IsBusy = false;
                }
            }

            FlowState.PaymentFlowState.Cc.AddNewCcJourney(State.Vehicle, State.IsVehicleRegisteredInUk);

            NavigationService.NavigateTo(AppPage.ChooseDates, State);
        }

        private async Task AddVehicleToAutoPayAndNavigate()
        {
            var result = await DialogService.Show2ButtonMessage(
                    Strings.ThereIsAnAdministrationChargeForEachVehicle,
                    Strings.Confirmation, Strings.Yes, Strings.No);

            if (result)
            {
                IsBusy = true;
                try
                {
                    await LoggedInUser.AddAutoPayVehicle(State.Vehicle);

                    FlowState.StartLookupVrmFlow(LookupVrmFlowState.FlowType.AutoPay);

                    NavigationService.NavigateTo(AppPage.AutoPayVehicles, State);
                }
                catch (ClientApiException ex)
                {
                    IsBusy = false;
                    await ClientApiExceptionHandler.HandleErrors(ex);
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}