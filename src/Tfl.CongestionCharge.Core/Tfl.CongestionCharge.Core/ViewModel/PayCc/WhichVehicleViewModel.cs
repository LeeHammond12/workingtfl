﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public class WhichVehicleViewModel : TflViewModelBase
    {
        private RelayCommand _chooseAnotherVehicleCommand;

        public IReadOnlyList<VehicleViewModel> SavedVrms => UserInformation.VehiclesNotOnCcAutoPay;

        public RelayCommand ChooseAnotherVehicleCommand
            => _chooseAnotherVehicleCommand ?? (_chooseAnotherVehicleCommand = new RelayCommand(AddAnotherVehicle));

        public override bool IsGoBackToHomeButtonVisible => FlowState.PaymentFlowState?.Cc?.GoBackToJourneyDetailsScreen == true;

        public WhichVehicleViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            return await LoadUserInformationWithRetry(true) == ViewModelInitResult.Success
                ? ViewModelInitResult.Success
                : ViewModelInitResult.Abort;
        }

        public void GoBack()
        {
            if (FlowState.PaymentFlowState?.Cc?.GoBackToJourneyDetailsScreen == true)
            {
                NavigationService.NavigateTo(AppPage.JourneyDetails, this);
            }
            else
            {
                NavigationService.GoBackOnePage();
            }
        }

        public async void SetSelectedVrm(VehicleViewModel vehicle)
        {
            if (IsBusy)
            {
                return;
            }

            var vrmIsValidated = await CheckQmlVehicleForTCharge(vehicle);
            if (vrmIsValidated)
            {
                FlowState.PaymentFlowState.Cc.AddNewCcJourney(vehicle, true);
                NavigationService.NavigateTo(AppPage.ChooseDates, this);
            }
        }

        private void AddAnotherVehicle()
        {
            FlowState.StartLookupVrmFlow(LookupVrmFlowState.FlowType.CcPayment);

            NavigationService.NavigateTo(AppPage.LookupVrm, this);
        }
    }
}