﻿namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public enum EsChargeFlag
    {
        NotSubject = 0,
        Subject = 1,
        SubjectButExempt = 2
    }
}
