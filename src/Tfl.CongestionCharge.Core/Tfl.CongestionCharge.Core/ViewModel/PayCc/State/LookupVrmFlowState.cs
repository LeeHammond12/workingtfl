﻿namespace Tfl.CongestionCharge.Core.ViewModel.PayCc.State
{
    public class LookupVrmFlowState
    {
        public enum FlowType
        {
            CcPayment,
            AutoPay
        }

        public VehicleViewModel Vehicle { get; set; }

        public bool IsVehicleRegisteredInUk { get; set; }

        public FlowType Type { get; set; }

        public LookupVrmFlowState()
        {
            Vehicle = new VehicleViewModel();
        }
    }
}
