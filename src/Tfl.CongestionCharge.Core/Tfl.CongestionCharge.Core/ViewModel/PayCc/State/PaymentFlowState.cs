﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc.State
{
    public class PaymentFlowState
    {
        [JsonIgnore]
        public IPcnOrCcState Inner
        {
            get { return (IPcnOrCcState)Pcn ?? Cc; }
            set
            {
                if (value is PcnState)
                {
                    Pcn = (PcnState)value;
                    return;
                }
                if (value is CcState)
                {
                    Cc = (CcState)value;
                    return;
                }
                throw new InvalidOperationException();
            }
        }

        public PcnState Pcn { get; set; }
        public CcState Cc { get; set; }

        public DateTime PaymentDate { get; set; }

        public string ReceiptNumber { get; set; }

        public ChargeType ChargeType => Inner.ChargeType;

        public decimal TotalPrice => Inner.TotalPrice;

        public List<StoredReceipt> GetReceipts() => Inner.GetReceiptsForJourneys(PaymentDate, ReceiptNumber);

        public PaymentFlowState()
        {
        }
    }
}
