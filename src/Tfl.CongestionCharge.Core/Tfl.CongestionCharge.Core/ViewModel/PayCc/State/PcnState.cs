﻿using System;
using System.Collections.Generic;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc.State
{
    public class PcnState : IPcnOrCcState
    {
        public string PcnNumber { get; set; }

        public string Vrm { get; set; }

        public ChargeType ChargeType => ChargeType.PenaltyChargeNotice;

        public decimal TotalPrice => Price;

        public decimal Price { get; set; }

        public PcnState()
        {
        }

        public List<StoredReceipt> GetReceiptsForJourneys(DateTime paymentDate, string receiptId)
        {
            return new List<StoredReceipt>()
            {
                new StoredReceipt()
                {
                    Vrm = Vrm,
                    Price = Price,
                    PaymentDate = paymentDate,
                    ReceiptId = receiptId,
                    PcnNumber = PcnNumber,
                }
            };
        }
    }
}