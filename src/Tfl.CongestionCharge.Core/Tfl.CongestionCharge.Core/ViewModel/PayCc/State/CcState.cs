﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc.State
{
    public class CcState : IPcnOrCcState
    {
        public List<JourneyDetails> Journeys { get; }

        public JourneyDetails CurrentJourney { get; set; }

        public ChargeType ChargeType => ChargeType.CongestionCharge;

        public decimal TotalPrice => Journeys.Sum(j => j.Price);

        public bool GoBackToJourneyDetailsScreen { get; set; }

        public bool DoAnyJourneysOverlap => DoAnyOverlap();
        public bool DoesCurrentJourneyOverlap => DoOverlap(CurrentJourney);

        public CcState()
        {
            Journeys = new List<JourneyDetails>();
        }

        public void StartNewCcJourney()
        {
            CurrentJourney = new JourneyDetails();
        }

        public void AddNewCcJourney(VehicleViewModel vehicle, bool isRegisteredInUk)
        {
            CurrentJourney = new JourneyDetails()
            {
                Vehicle = vehicle,
                IsVehicleRegisteredInUk = isRegisteredInUk
            };
        }

        public void AddCurrentJourney()
        {
            if (!Journeys.Contains(CurrentJourney))
                Journeys.Add(CurrentJourney);
        }

        public List<StoredReceipt> GetReceiptsForJourneys(DateTime paymentDate, string receiptId)
        {
            var receipts = new List<StoredReceipt>();

            for (var index = 0; index < Journeys.Count; index++)
            {
                var journey = Journeys[index];
                receipts.Add(new StoredReceipt()
                {
                    Vrm = journey.Vehicle.Vrm,
                    Price = journey.Price,
                    PeriodType = journey.PeriodType,
                    DateString = journey.TimePeriod,
                    StartDate = journey.ChargeStartDate,
                    PaymentDate = paymentDate,
                    ReceiptId = receiptId + (Journeys.Count > 1 ? " - " + (index + 1) : string.Empty),
                });
            }
            return receipts;
        }

        private bool DoAnyOverlap()
        {
            if (CurrentJourney == null)
                return false;

            foreach (var journey in Journeys)
            {
                if (DoOverlap(journey))
                    return true;
            }
            return false;
        }

        private bool DoOverlap(JourneyDetails journey)
        {
            if (journey == null)
                return false;

            var startDate = journey.ChargeStartDate;
            var endDate = journey.ChargeEndDate;

            var otherJourneys = Journeys.Where(j => j != journey && j.Vehicle.Equals(journey.Vehicle));

            // https://stackoverflow.com/a/325964/451540
            return otherJourneys.Any(other => other.ChargeStartDate <= endDate && other.ChargeEndDate >= startDate);
        }
    }
}