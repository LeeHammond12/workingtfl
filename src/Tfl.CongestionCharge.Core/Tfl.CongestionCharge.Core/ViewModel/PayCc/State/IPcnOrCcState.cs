﻿using System;
using System.Collections.Generic;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc.State
{
    public interface IPcnOrCcState
    {
        ChargeType ChargeType { get; }

        decimal TotalPrice { get; }

        List<StoredReceipt> GetReceiptsForJourneys(DateTime paymentDate, string receiptId);
    }
}