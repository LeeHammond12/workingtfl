﻿using System;
using Tfl.CongestionCharge.Core.Model;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc.State
{
    public class JourneyDetails
    {
        public ChargePeriod PeriodType { get; set; }

        public VehicleViewModel Vehicle { get; set; }

        public DateTime ChargeStartDate { get; set; }

        public DateTime ChargeEndDate { get; set; }

        public string TimePeriod { get; set; }

        public decimal Price { get; set; }

        public string ConsecutiveDaysString { get; set; }

        public bool IsVehicleRegisteredInUk { get; set; }

        public JourneyDetails()
        {
            Vehicle = new VehicleViewModel();
        }
    }
}