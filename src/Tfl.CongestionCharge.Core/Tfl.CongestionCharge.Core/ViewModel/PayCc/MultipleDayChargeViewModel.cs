﻿using System;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.PayCc;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public class MultipleDayChargeViewModel : ChargeViewModelBase
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public override string DateString { get; }

        public override ChargePeriod ChargePeriod { get; }

        public string ConsecutiveDaysString { get; }

        public MultipleDayChargeViewModel(CcDetailMultipleDay detail, int days)
            : base(detail, days)
        {
            EndDate = detail.EndDate;
            StartDate = detail.StartDate;

            ConsecutiveDaysString = days + Strings.ConsecutiveChargingDays;
            DateString =
                 (StartDate != null && EndDate != null) ?
                    StartDate.Value.ToHumanReadableFormat() + " - " + EndDate.Value.ToHumanReadableFormat() :
                    string.Empty;

            ChargePeriod = days <= 5 ? ChargePeriod.Weekly : ChargePeriod.Monthly;
        }
    }
}