﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Model.PayCc;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public class ChooseDatesViewModel : TflViewModelBase
    {
        public string PageTitle => Strings.PayCongestionCharge;

        public JourneyDetails Journey => Payment?.Cc?.CurrentJourney;

        public PaymentFlowState Payment => FlowState.PaymentFlowState;

        public DateTime StartDate { private get; set; }

        public List<ChargeViewModelBase> Charges { get; } = new List<ChargeViewModelBase>();

        public SingleDayChargeViewModel Daily => Charges.FirstOrDefault(c => c.ChargePeriod == ChargePeriod.Daily) as SingleDayChargeViewModel;
        public SingleDayChargeViewModel Yesterday => Charges.FirstOrDefault(c => c.ChargePeriod == ChargePeriod.PayNextDay) as SingleDayChargeViewModel;
        public MultipleDayChargeViewModel Weekly => Charges.FirstOrDefault(c => c.ChargePeriod == ChargePeriod.Weekly) as MultipleDayChargeViewModel;
        public MultipleDayChargeViewModel Monthly => Charges.FirstOrDefault(c => c.ChargePeriod == ChargePeriod.Monthly) as MultipleDayChargeViewModel;

        public ChooseDatesViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
            StartDate = DateTime.Today;
        }

        public void GoBack()
        {
            if (FlowState.PaymentFlowState?.Cc?.GoBackToJourneyDetailsScreen == true)
            {
                NavigationService.NavigateTo(AppPage.JourneyDetails, this);
            }
            else
            {
                NavigationService.GoBackOnePage();
            }
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            if (Payment != null)
            {
                await PopulateCharges();
            }
            else
            {
                NavigationService.NavigateHomeAndClearBackStack();
                return ViewModelInitResult.Abort;
            }
            return ViewModelInitResult.Success;
        }

        public void GoBackIfNoFlowState()
        {
            if (Payment?.Cc?.CurrentJourney == null)
            {
                NavigationService.GoBackOnePage();
            }
        }

        public async Task PopulateCharges(bool userSelectedDate = false)
        {
            try
            {
                if (Journey == null)
                {
                    return;
                }

                IsBusy = true;

                var ccDetail = await AppEnvironment.TflApi.LookupCongestionCharges(Journey.Vehicle.Vrm, StartDate.ToSitDateFormat(), Journey.IsVehicleRegisteredInUk);

                IsBusy = false;

                await UpdateCharges(userSelectedDate, ccDetail);
            }
            catch (ClientApiException ex)
            {
                IsBusy = false;

                await ShowErrorMessages(ex);
            }
        }

        private async Task UpdateCharges(bool userSelectedDate, CcDetail ccDetail)
        {
            if (ccDetail?.CcChargeList?.CcCharge != null)
            {
                Update(ccDetail);

                var daily = ccDetail?.CcChargeList?.CcCharge.FirstOrDefault()?.Daily;
                if (daily?.Amount > 0 && daily?.Date?.Date != StartDate.Date &&
                    userSelectedDate)
                {
                    await DialogService.ShowWarning(Strings.CcNonChargingDate);
                }
            }
            else
            {
                Charges.Clear();

                await DialogService.ShowError(Strings.CcNoChargesAvailableForThisDate);
            }
        }

        private void Update(CcDetail ccDetail)
        {
            Charges.Clear();

            var charges = ccDetail?.CcChargeList?.CcCharge;
            if (charges == null)
            {
                return;
            }

            var showYesterday = StartDate.Date == DateTime.Now.Date;
            var payNextDay = charges.FirstOrDefault(c => c.PayNextDay != null)?.PayNextDay;
            if (showYesterday && payNextDay != null)
            {
                Charges.Add(new SingleDayChargeViewModel(payNextDay, ChargePeriod.PayNextDay));
            }

            var chargeDays = ccDetail.CcChargeDays;
            var dailyList = charges.FirstOrDefault(c => c.Daily != null);
            if (chargeDays == 0 || dailyList.ChargeStatus == "Discount9Plus")
            {
                var daily = dailyList?.Daily;
                if (daily != null)
                {
                    Charges.Add(new SingleDayChargeViewModel(daily, ChargePeriod.Daily));
                }
            }

            bool applyCustomWeeklyCharge = chargeDays > 0 && chargeDays < 5;
            bool applyCustomMonthlyCharge = !applyCustomWeeklyCharge && chargeDays > 0 && chargeDays < 20;

            var weekly = charges.FirstOrDefault(c => c.Weekly != null)?.Weekly;
            if (weekly != null && !applyCustomWeeklyCharge)
            {
                Charges.Add(new MultipleDayChargeViewModel(weekly, 5));
            }

            var monthly = charges.FirstOrDefault(c => c.Monthly != null)?.Monthly;
            if (monthly != null && !applyCustomMonthlyCharge)
            {
                Charges.Add(new MultipleDayChargeViewModel(monthly, 20));
            }

            var custom = charges.FirstOrDefault(c => c.Custom != null)?.Custom;
            if (custom != null && chargeDays > 0 && chargeDays < 20)
            {
                var customViewModel = new MultipleDayChargeViewModel(custom, chargeDays);
                Charges.Add(customViewModel);
            }
        }

        public async Task SelectCharge(ChargeViewModelBase charge)
        {
            Journey.Price = charge?.Amount ?? 0;

            if (charge == null)
            {
                return;
            }

            PopulateJourney(charge);

            try
            {
                IsBusy = true;

                var ccDetail = await AppEnvironment.TflApi.ValidateCongestionCharges(
                    Journey.Vehicle.Vrm,
                    Journey.ChargeStartDate.ToSitDateFormat(),
                    Journey.ChargeEndDate.ToSitDateFormat(),
                    Journey.IsVehicleRegisteredInUk);

                IsBusy = false;

                var chargeValidationResult = ccDetail?.CcChargeList?.CcCharge?.FirstOrDefault();
                if (chargeValidationResult != null)
                {
                    await DialogService.ShowWarning(Strings.CcChargeHasAlreadyBeenPaidForDatesSelected);
                }
                else
                {
                    if (Payment.Cc.DoesCurrentJourneyOverlap)
                    {
                        await DialogService.ShowWarning(Strings.YouAlreadySelectedAChargeForThisVehicle);
                    }

                    Payment.Cc.AddCurrentJourney();

                    NavigationService.NavigateTo(AppPage.JourneyDetails, Journey);
                }
            }
            catch (ClientApiException ex)
            {
                IsBusy = false;

                await ShowErrorMessages(ex);
            }
        }

        private void PopulateJourney(ChargeViewModelBase charge)
        {
            Journey.PeriodType = charge.ChargePeriod;

            var single = charge as SingleDayChargeViewModel;
            if (single?.Date != null)
            {
                var date = single.Date.Value;
                Journey.ChargeStartDate = date;
                Journey.ChargeEndDate = date;
                Journey.TimePeriod = single.DateString;
                Journey.ConsecutiveDaysString = string.Empty;
            }

            var multiple = charge as MultipleDayChargeViewModel;
            if (multiple?.StartDate != null && multiple?.EndDate != null)
            {
                Journey.ChargeStartDate = multiple.StartDate.Value;
                Journey.ChargeEndDate = multiple.EndDate.Value;
                Journey.TimePeriod = multiple.DateString;
                Journey.ConsecutiveDaysString = multiple.ConsecutiveDaysString;
            }
        }

        private async Task ShowErrorMessages(ClientApiException ex)
        {
            var shouldgoHome = ClientApiExceptionHandler.ContainsErrorCode(ex, ClientApiErrorCodes.CcVrmAlreadyHas100PercentDiscount);

            await ClientApiExceptionHandler.HandleErrors(ex);

            if (shouldgoHome)
            {
                NavigationService.NavigateHomeAndClearBackStack();
            }
        }
    }
}