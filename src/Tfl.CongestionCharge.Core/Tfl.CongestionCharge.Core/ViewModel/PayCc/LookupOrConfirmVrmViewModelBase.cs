﻿using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public class LookupOrConfirmVrmViewModelBase : TflViewModelBase
    {
        protected bool IsForCcCharge => State.Type == LookupVrmFlowState.FlowType.CcPayment;

        public VehicleViewModel Vehicle => State.Vehicle;

        public string VehicleDetails => Vehicle?.ColorMakeModel;

        public LookupVrmFlowState State => FlowState.LookupVrmFlowState;

        public bool HasFlowState => State != null;

        public bool IsRegisteredInUk
        {
            get { return State.IsVehicleRegisteredInUk; }
            set { State.IsVehicleRegisteredInUk = value; }
        }

        public LookupOrConfirmVrmViewModelBase(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public void GoBackIfNoFlowState()
        {
            if (!HasFlowState)
            {
                NavigationService.GoBackOnePage();
            }
        }
    }
}