﻿using System;
using GalaSoft.MvvmLight;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.PayCc;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public abstract class ChargeViewModelBase : ViewModelBase
    {
        private int _days;

        public decimal Amount { get; set; }

        public bool CcPaid { get; set; }

        public abstract string DateString { get; }

        public abstract ChargePeriod ChargePeriod { get; }

        public string ChargePeriodString
        {
            get
            {
                switch (ChargePeriod)
                {
                    case ChargePeriod.Daily: return Strings.DailyCongestionCharge;
                    case ChargePeriod.PayNextDay: return Strings.PreviousChargingDay;
                    case ChargePeriod.Weekly: return Strings.WeeklyCongestionCharge;
                    case ChargePeriod.Monthly: return Strings.MonthlyCongestionCharge;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        protected ChargeViewModelBase(ICcDetail detail, int days = 1)
        {
            Amount = detail.Amount;
            CcPaid = detail.CcPaid;
            _days = days;
        }
    }
}