﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.PayCc
{
    public class JourneyDetailsViewModel : TflViewModelBase
    {
        public class GroupedJourneyViewModel
        {
            public VehicleViewModel Vehicle { get; set; }
            public bool IsRegisteredInUk => Journeys.First().IsVehicleRegisteredInUk;
            public List<JourneyDetails> Journeys { get; set; }
        }

        private RelayCommand _chooseAnotherVehicleCommand;
        private RelayCommand _navigateToPaymentCommand;

        private PaymentFlowState Payment => FlowState.PaymentFlowState;
        private CcState CcJourney => Payment.Cc;

        public RelayCommand AddAnotherVehicleCommand
            => _chooseAnotherVehicleCommand ?? (_chooseAnotherVehicleCommand = new RelayCommand(AddAnotherVehicle));

        public RelayCommand NavigateToPaymentCommand
            => _navigateToPaymentCommand ?? (_navigateToPaymentCommand = new RelayCommand(
                    () => CheckOverlapsAndNavigateToPayment(),
                    () => Payment.TotalPrice > 0));

        public List<GroupedJourneyViewModel> GroupedJourneys { get; set; }

        public string ButtonText => Strings.ConfirmDetailsAndPay + " " + CcJourney.TotalPrice.ToCurrencyString();

        public JourneyDetailsViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
            var groups = CcJourney.Journeys.GroupBy(v => v.Vehicle.Vrm);
            GroupedJourneys = new List<GroupedJourneyViewModel>();
            foreach (var @group in groups)
            {
                var groupedJourney = new GroupedJourneyViewModel()
                {
                    Vehicle = @group.First().Vehicle,
                    Journeys = @group.ToList()
                };
                GroupedJourneys.Add(groupedJourney);
            }
        }

        public async void OnGoBack()
        {
            CcJourney.GoBackToJourneyDetailsScreen = false;
            CcJourney.Journeys.Remove(CcJourney.CurrentJourney);

            if (CcJourney?.CurrentJourney == null)
            {
                var text = (CcJourney.Journeys?.Count > 0)
                    ? Strings.DoYouReallyWantToGoBackAndLoseAllYourSelectedCharges
                    : Strings.DoYouReallyWantToGoToTheHomeScreen;

                if (await DialogService.Show2ButtonMessage(text, Strings.Confirm, Strings.Yes, Strings.No))
                {
                    NavigationService.NavigateHomeAndClearBackStack();
                }
            }
            else
            {
                NavigationService.GoBackOnePage();
            }
        }

        public void AddDate(VehicleViewModel vehicle, bool isRegisteredInUk)
        {
            CcJourney.StartNewCcJourney();

            CcJourney.GoBackToJourneyDetailsScreen = true;
            CcJourney.CurrentJourney.Vehicle = vehicle;
            CcJourney.CurrentJourney.IsVehicleRegisteredInUk = isRegisteredInUk;

            NavigationService.NavigateTo(AppPage.ChooseDates, this);
        }

        public async Task<bool> DeleteJourney(JourneyDetails journey)
        {
            if (!await DialogService.Show2ButtonMessage(Strings.ReallyRemoveThisJourney, Strings.Confirmation, Strings.Yes, Strings.No))
            {
                return false;
            }

            CcJourney.Journeys.Remove(journey);
            var group = GroupedJourneys.First(j => j.Vehicle.Equals(journey.Vehicle));
            group.Journeys.Remove(journey);

            if (group.Journeys.Count == 0)
            {
                GroupedJourneys.Remove(group);
            }

            if (CcJourney.CurrentJourney == journey)
            {
                CcJourney.CurrentJourney = null;
                FlowState.FinishLookupVrmFlow();
            }

            RaisePropertyChanged(() => ButtonText);

            return true;
        }

        private async void AddAnotherVehicle()
        {
            await LoadUserInformationWithRetry(true);

            FlowState.FinishLookupVrmFlow();

            CcJourney.GoBackToJourneyDetailsScreen = true;

            if (UserInformation.VehiclesNotOnCcAutoPay.Count > 0)
            {
                NavigationService.NavigateTo(AppPage.ChooseAVehicle, this);
            }
            else
            {
                FlowState.StartLookupVrmFlow(LookupVrmFlowState.FlowType.CcPayment);
                NavigationService.NavigateTo(AppPage.LookupVrm, this);
            }
        }

        private async void CheckOverlapsAndNavigateToPayment()
        {
            if (CcJourney.DoAnyJourneysOverlap)
            {
                await DialogService.ShowMessage(Strings.SomeChargesAreOverlapping, Strings.Warning);
                return;
            }

            NavigateToPaymentPage(ChargeType.CongestionCharge);
        }
    }
}