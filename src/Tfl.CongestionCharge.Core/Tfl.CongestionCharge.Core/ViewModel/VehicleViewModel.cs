﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.AccountInfo;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Storage;

namespace Tfl.CongestionCharge.Core.ViewModel
{
    public class VehicleViewModel
    {
        public string Vrm { get; set; }

        public string Model { get; set; }

        public string Color { get; set; }

        public string Make { get; set; }

        public bool IsOnCcAutoPay => Services.Any(s => s?.IsCcAutoPay == true);

        public List<AccountTflServiceDto> Services { get; set; } = new List<AccountTflServiceDto>();

        public string ColorMakeModel
            => string.IsNullOrEmpty(Model) && string.IsNullOrEmpty(Make) ?
                Strings.NoDetailsFound :
                $"{Color} {Make} {Model}";

        public VehicleViewModel(IVehicleDto vehicle)
        {
            Vrm = vehicle?.VRM ?? string.Empty;
            Model = vehicle?.Model ?? string.Empty;
            Color = vehicle?.Colour ?? string.Empty;
            Make = vehicle?.Make ?? string.Empty;

            var services = (vehicle as AccountVehicleWithServicesDto)?.ServiceList;
            if (services != null)
            {
                Services.AddRange(services);
            }
        }

        public VehicleViewModel(StoredVehicle vehicle)
        {
            Vrm = vehicle?.Vrm ?? string.Empty;
            Model = vehicle?.Model ?? string.Empty;
            Color = vehicle?.Color ?? string.Empty;
            Make = vehicle?.Make ?? string.Empty;
        }

        public VehicleViewModel()
        {
            Vrm = string.Empty;
            Model = string.Empty;
            Color = string.Empty;
            Make = string.Empty;
        }

        public override bool Equals(object obj)
        {
            var other = obj as VehicleViewModel;
            if (other == null)
                return false;

            return other.Vrm.Equals(Vrm, StringComparison.OrdinalIgnoreCase);
        }

        public override int GetHashCode()
        {
            return Vrm?.GetHashCode() ?? 0;
        }
    }
}