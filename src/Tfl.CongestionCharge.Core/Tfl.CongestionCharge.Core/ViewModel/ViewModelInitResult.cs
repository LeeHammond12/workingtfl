namespace Tfl.CongestionCharge.Core.ViewModel
{
    public enum ViewModelInitResult
    {
        Success,
        Abort
    }
}