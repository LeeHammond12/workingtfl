﻿using System.Diagnostics;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Services;

namespace Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount
{
    public class LoggedInAccountViewModel : TflViewModelBase
    {
        private RelayCommand _navigateToVehiclesAndServicesCommand;
        private RelayCommand _navigateToAutoPayPaymentCardCommand;
        private RelayCommand _navigateToAutoPayVehiclesCommand;

        public bool IsAutoPayEnabled => LoggedInUser.HasAutoPay;
        public AutoPayStatus AutoPayStatus => LoggedInUser.AutoPayStatus;
        public string AutoPayStatusDisplayText => GetHumanReadableAutoPayStatus();

        public RelayCommand NavigateToVehiclesAndServicesCommand
            => _navigateToVehiclesAndServicesCommand
               ?? (_navigateToVehiclesAndServicesCommand = new RelayCommand(
                   () => NavigationService.NavigateTo(AppPage.YourVehiclesAndServices, this)));

        public RelayCommand NavigateToAutoPayPaymentCardCommand
                => _navigateToAutoPayPaymentCardCommand
                       ?? (_navigateToAutoPayPaymentCardCommand = new RelayCommand(
                           () => NavigateToAutoPayPaymentCards()));

        public RelayCommand NavigateToAutoPayVehiclesCommand
                => _navigateToAutoPayVehiclesCommand
                       ?? (_navigateToAutoPayVehiclesCommand = new RelayCommand(
                           () => NavigateToAutoPayVehicles()));

        public LoggedInAccountViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public async Task RemoveVehicle(string vrm)
        {
            IsBusy = true;
            try
            {
                var text = string.Format(Strings.DoYouReallyWantToRemoveVehicleX, vrm);
                if (await DialogService.Show2ButtonMessage(text, Strings.Confirm, Strings.Yes, Strings.No))
                {
                    await LoggedInUser.RemoveVehicle(vrm);
                }
            }
            catch (ClientApiException ex)
            {
                IsBusy = false;
                await ClientApiExceptionHandler.HandleErrors(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task DeletePaymentCard(PaymentCardViewModel card)
        {
            IsBusy = true;
            try
            {
                var text = string.Format(Strings.DoYouReallyWantToRemoveCardX, card.Last4Digits);
                if (await DialogService.Show2ButtonMessage(text, Strings.Confirm, Strings.Yes, Strings.No))
                {
                    var info = await LoggedInUser.RemovePaymentCard(card);
                    Debug.WriteLine("INFO " + JsonConvert.SerializeObject(info));
                }
            }
            catch (ClientApiException ex)
            {
                IsBusy = false;
                await ClientApiExceptionHandler.HandleErrors(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            return await LoadUserInformationWithRetry(true);
        }

        private void NavigateToAutoPayVehicles()
        {
            if (!CheckAccountStatus())
                return;

            NavigationService.NavigateTo(AppPage.AutoPayVehicles, this);
        }

        private void NavigateToAutoPayPaymentCards()
        {
            if (!CheckAccountStatus())
                return;

            NavigationService.NavigateTo(AppPage.AutoPayCreditCard, this);
        }

        private bool CheckAccountStatus()
        {
            if (AutoPayStatus == AutoPayStatus.Closed || AutoPayStatus == AutoPayStatus.Suspended)
            {
                DialogService.ShowOkMessageWithLink(Strings.SuspendedOrClosedAccountWarning, Strings.Warning);
                return false;
            }
            return true;
        }

        private string GetHumanReadableAutoPayStatus()
        {
            switch (AutoPayStatus)
            {
                case AutoPayStatus.PendingSuspension:
                    return "Pending Suspension";

                default:
                    return LoggedInUser.AutoPayStatusText;
            }
        }
    }
}
