﻿using Tfl.CongestionCharge.Core.Environment;

namespace Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount
{
    public class YourAccountViewModel : LoggedInAccountViewModel 
    {
        public override bool IsGoBackToHomeButtonVisible => false;

        public YourAccountViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }
    }
}
