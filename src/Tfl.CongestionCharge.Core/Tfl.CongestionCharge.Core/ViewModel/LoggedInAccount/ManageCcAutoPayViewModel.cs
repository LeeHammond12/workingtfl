﻿using Tfl.CongestionCharge.Core.Environment;

namespace Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount
{
    public class ManageCcAutoPayViewModel : LoggedInAccountViewModel
    {
        public override bool IsGoBackToHomeButtonVisible => false;

        public ManageCcAutoPayViewModel(AppEnvironment appEnvironment) : base(appEnvironment)
        {
        }
    }
}
