﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount
{
    public class AutoPayPaymentCardViewModel : TflViewModelBase
    {
        public PaymentCardViewModel ActivePaymentCard => LoggedInUser.AutoPayPaymentCard;
        public IReadOnlyList<PaymentCardViewModel> PaymentCards => LoggedInUser.PaymentCards;

        public bool ShowDirectDebitScreen => PaymentCards?.Any() == false;

        public string ToolbarText { get; set; }

        public AutoPayPaymentCardViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            var result = await LoadUserInformationWithRetry() == ViewModelInitResult.Success ?
                ViewModelInitResult.Success :
                ViewModelInitResult.Abort;

            if (result == ViewModelInitResult.Success)
            {
                ToolbarText = PaymentCards?.Any() == true ?
                    Strings.AutoPayCreditCard :
                    Strings.DirectDebitDetails;
            }

            return result;
        }

        public async Task<bool> SetAutoPayPaymentCard(PaymentCardViewModel paymentCard)
        {
            IsBusy = true;

            bool success = false;
            try
            {
                await LoggedInUser.UpdateAutoPayPaymentCard(paymentCard, true);
                await LoggedInUser.UpdateInformation();

                success = LoggedInUser.AutoPayPaymentCard?.TemplateNumber == paymentCard.TemplateNumber;
                if (!success)
                {
                    IsBusy = false;
                    await DialogService.ShowWarning(Strings.FailedToSetAutoPayPaymentCard);
                }
                return success;
            }
            catch (ClientApiException ex)
            {
                IsBusy = false;
                await ClientApiExceptionHandler.HandleErrors(ex);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                IsBusy = false;
                await DialogService.ShowUnknownError();
            }
            finally
            {
                IsBusy = false;
            }
            return success;
        }
    }
}
