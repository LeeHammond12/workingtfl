﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount
{
    public class AutoPayVehiclesViewModel : TflViewModelBase
    {
        public const int MaxNumVehiclesOnAutoPay = 5;

        private RelayCommand _navigateToAddAnotherVehicleCommand;

        public RelayCommand NavigateToAddAnotherVehicleCommand
                => _navigateToAddAnotherVehicleCommand
                       ?? (_navigateToAddAnotherVehicleCommand = new RelayCommand(AddAnotherVehicle));

        public List<VehicleViewModel> ActiveVehicles { get; set; } = new List<VehicleViewModel>();
        public List<VehicleViewModel> InactiveVehicles { get; set; } = new List<VehicleViewModel>();

        public bool CanAddVehicleToAutoPay => ActiveVehicles?.Count < MaxNumVehiclesOnAutoPay;

        public AutoPayVehiclesViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public override async Task<ViewModelInitResult> InitViewModel()
        {
            if (await LoadUserInformationWithRetry() == ViewModelInitResult.Success)
            {
                ActiveVehicles = await ExecuteUntilResult(() => LoggedInUser.GetAutoPayVehicles());
                if (ActiveVehicles == null)
                {
                    return ViewModelInitResult.Abort;
                }

                RefreshLists(ActiveVehicles);
            }
            return ViewModelInitResult.Success;
        }

        public async Task AddVehicleToAutoPay(VehicleViewModel vehicleViewModel)
        {
            if (!CanAddVehicleToAutoPay)
            {
                await DialogService.ShowMessage(
                       Strings.CannotAddMoreThan5VehiclesToAutoPay,
                       Strings.Information);
                return;
            }

            var validated = await CheckQmlVehicleForTCharge(vehicleViewModel);
            if (!validated)
            {
                return;
            }

            var result = await DialogService.Show2ButtonMessage(
                    Strings.ThereIsAnAdministrationChargeForEachVehicle,
                    Strings.Confirmation, Strings.Yes, Strings.No);

            if (result)
            {
                IsBusy = true;

                try
                {
                    var newVehicles = await LoggedInUser.AddAutoPayVehicle(vehicleViewModel);
                    RefreshLists(newVehicles);
                }
                catch (ClientApiException ex)
                {
                    IsBusy = false;
                    await ClientApiExceptionHandler.HandleErrors(ex);
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        public async Task RemoveVehicleFromAutoPay(VehicleViewModel vehicleViewModel)
        {
            var result = await DialogService.Show2ButtonMessage(
                Strings.DoYouReallyWantToRemoveXFromAutoPay,
                Strings.Warning,
                Strings.Yes, Strings.No);

            if (result)
            {
                IsBusy = true;
                try
                {
                    var newVehicles = await LoggedInUser.RemoveAutoPayVehicle(vehicleViewModel.Vrm);
                    RefreshLists(newVehicles);
                }
                catch (ClientApiException ex)
                {
                    IsBusy = false;
                    await ClientApiExceptionHandler.HandleErrors(ex);
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        private void RefreshLists(List<VehicleViewModel> activeVehicles)
        {
            ActiveVehicles = activeVehicles;

            var all = UserInformation.Vehicles;
            InactiveVehicles = all.Where(v => ActiveVehicles.All(a => !a.Vrm.Equals(v.Vrm, StringComparison.OrdinalIgnoreCase)))
                .ToList();
        }

        private async void AddAnotherVehicle()
        {
            if (!CanAddVehicleToAutoPay)
            {
                await DialogService.ShowMessage(
                       Strings.CannotAddMoreThan5VehiclesToAutoPay,
                       Strings.Information);
                return;
            }

            FlowState.StartLookupVrmFlow(LookupVrmFlowState.FlowType.AutoPay);

            NavigationService.NavigateTo(AppPage.LookupVrm, this);
        }
    }
}
