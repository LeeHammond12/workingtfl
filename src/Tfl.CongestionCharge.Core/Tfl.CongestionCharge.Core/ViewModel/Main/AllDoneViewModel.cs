﻿using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.Main
{
    public class AllDoneViewModel : TflViewModelBase
    {
        private RelayCommand _returnHomeCommand;
        private RelayCommand _saveReceiptAndReturnHomeCommand;

        public AllDoneViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        private PcnState Pcn => Payment.Pcn;

        public string PageTitle => IsPcn ? Strings.PayPenaltyChargeNotice : Strings.PayCongestionCharge;

        public bool IsPcn => Payment.ChargeType == Model.ChargeType.PenaltyChargeNotice;

        public PaymentFlowState Payment => FlowState.PaymentFlowState;

        public string ChargeType => IsPcn ? Strings.Pcn : Strings.CongestionChargeCc;

        public string PcnNumber => Pcn?.PcnNumber;

        public decimal Price => Payment.TotalPrice;

        public string PaymentDate => Payment.PaymentDate.ToHumanReadableFormat();

        public string Receipt => !string.IsNullOrEmpty(Payment.ReceiptNumber) ? Payment.ReceiptNumber : Strings.NotAvailable;

        public bool IsSaveReceiptButtonVisible => !IsLoggedIn;

        public RelayCommand ReturnHome => _returnHomeCommand ?? (_returnHomeCommand = new RelayCommand(NavigateHome));

        public RelayCommand SaveReceiptAndReturnHome => _saveReceiptAndReturnHomeCommand ?? (_saveReceiptAndReturnHomeCommand = new RelayCommand(SaveReceiptAndNavigateHome));

        private void SaveReceiptAndNavigateHome()
        {
            StoreReceiptOnDevice();
            NavigationService.NavigateHomeAndClearBackStack();
        }

        private void NavigateHome()
        {
            NavigationService.NavigateHomeAndClearBackStack();
        }

        private void StoreReceiptOnDevice()
        {
            AnonymousUser?.AddReceipts(Payment.GetReceipts());
        }
    }
}