﻿using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;

namespace Tfl.CongestionCharge.Core.ViewModel.Main
{
    public class MainViewModel : TflViewModelBase
    {
        private RelayCommand _navigateToPayCcCommand;
        private RelayCommand _navigateToPayPcnCommand;
        private RelayCommand _navigateToPostCodeLookupCommand;
        private RelayCommand _navigateToReceiptsCommand;
        private RelayCommand _navigateToYourAccountCommand;
        private RelayCommand _navigateToAutoPayCommand;
        private RelayCommand _navigateToLogInCommand;
        private RelayCommand _navigateToManageStoredInformationCommand;

        public MainViewModel(
            AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
        }

        public bool ReceiptsAvailable => AppState.UserHasReceipts;

        public RelayCommand NavigateToPayCcCommand
                => _navigateToPayCcCommand ?? (_navigateToPayCcCommand = new RelayCommand(NavigateToPayCc));

        public RelayCommand NavigateToPCNLookupCommand
                => _navigateToPayPcnCommand
                       ?? (_navigateToPayPcnCommand = new RelayCommand(NavigateToPayPcn));

        public RelayCommand NavigateToPostCodeLookupCommand
                => _navigateToPostCodeLookupCommand
                       ?? (_navigateToPostCodeLookupCommand = new RelayCommand(
                           () => NavigationService.NavigateTo(AppPage.PostCodeChecker, this)));

        public RelayCommand NavigateToReceiptsCommand
                => _navigateToReceiptsCommand
                       ?? (_navigateToReceiptsCommand = new RelayCommand(
                           () => NavigationService.NavigateTo(AppPage.ViewReceipt, this), () => ReceiptsAvailable));

        public RelayCommand NavigateToYourAccountCommand
                => _navigateToYourAccountCommand ?? (_navigateToYourAccountCommand = new RelayCommand(
                    () => NavigationService.NavigateTo(AppPage.YourAccount, this)));

        public RelayCommand NavigateToManageStoredInformationCommand
                => _navigateToManageStoredInformationCommand ?? (_navigateToManageStoredInformationCommand = new RelayCommand(
                    () => NavigationService.NavigateTo(AppPage.YourVehiclesAndCards, this)));

        public RelayCommand NavigateToAutoPayCommand
                => _navigateToAutoPayCommand ?? (_navigateToAutoPayCommand = new RelayCommand(
                    () => NavigationService.NavigateTo(AppPage.ManageCcAutoPay, this)));

        public RelayCommand LoginOrLogoutCommand
            => _navigateToLogInCommand ?? (_navigateToLogInCommand = new RelayCommand(LogInOrLogout));

        //Suppress warning CS1998: This async method lacks 'await'
#pragma warning disable 1998
        public override async Task<ViewModelInitResult> InitViewModel()
        {
            return ViewModelInitResult.Success;
        }
#pragma warning restore 1998

        private async void NavigateToPayCc()
        {
            FlowState.StartCcPaymentFlow();

            if (await LoadUserInformation(true) == ViewModelInitResult.Success)
            {
                if (UserInformation.VehiclesNotOnCcAutoPay.Count > 0)
                {
                    NavigationService.NavigateTo(AppPage.ChooseAVehicle, this);
                }
                else
                {
                    FlowState.StartLookupVrmFlow(LookupVrmFlowState.FlowType.CcPayment);
                    NavigationService.NavigateTo(AppPage.LookupVrm, this);
                }
            }
        }

        private void NavigateToPayPcn()
        {
            FlowState.StartPcnPaymentFlow();
            NavigationService.NavigateTo(AppPage.PcnLookup, this);
        }

        private void LogInOrLogout()
        {
            if (AppEnvironment.AuthService.IsLoggedIn)
            {
                AppEnvironment.AuthService.Logout();
                NavigationService.NavigateHomeAndClearBackStack();
            }
            else
            {
                NavigationService.NavigateTo(AppPage.LoginPageKey, this);
            }
        }
    }
}