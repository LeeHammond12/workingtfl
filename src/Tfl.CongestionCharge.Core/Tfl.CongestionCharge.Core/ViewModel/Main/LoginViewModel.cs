﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Helper;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Validators;

namespace Tfl.CongestionCharge.Core.ViewModel.Main
{
    public class LoginViewModel : TflViewModelBase
    {
        private RelayCommand _loginCommand;

        public string AccountNumber { get; set; } = string.Empty;

        public string Password { get; set; } = string.Empty;

        public override bool IsGoBackToHomeButtonVisible => false;

        public RelayCommand LoginCommand => _loginCommand ?? (_loginCommand = new RelayCommand(async
                           () => await TryLogin()));

        public LoginViewModel(AppEnvironment appEnvironment)
            : base(appEnvironment)
        {
            AppEnvironment.AuthService.Logout();
#if DEBUG
            DEBUG_SetDummyData();
#endif
        }

        public void OnGoBack()
        {
            NavigationService.NavigateHomeAndClearBackStack();
        }

        private async Task TryLogin()
        {
            try
            {
                var validator = new LoginViewModelValidator();
                if (!validator.Validate(this).IsValid)
                {
                    await DialogService.ShowError(validator.Validate(this).Errors[0].ToString());
                    return;
                }

                IsBusy = true;

                await AppEnvironment.AuthService.Login(AccountNumber, Password);

                if (IsLoggedIn)
                {
                    await UpdateUserInformation(true);

                    if (LoggedInUser.AccountType?.ToLowerInvariant() == "b")
                    {
                        AppEnvironment.AuthService.Logout();
                        await DialogService.ShowError(Strings.TheAppCannotBeUsedWithABusinessAccount);
                    }

                    NavigationService.NavigateHomeAndClearBackStack();
                }
                else
                {
                    await DialogService.ShowUnknownError();
                    //await DialogService.ShowWarning(Strings.WrongCustomerIdOrPassword);
                }
            }
            catch (ClientApiException ex)
            {
                IsBusy = false;

                // this is necessary in case the UpdateUserInformation() call fails 
                AppEnvironment.AuthService.Logout();

                if (ex.IsNoNetworkException)
                {
                    await DialogService.ShowMessage(Strings.NoNetworkMessage, Strings.NoNetworkTitle);
                }
                else if (ex.IsAuthenticationException)
                {
                    await DialogService.ShowWarning(Strings.WrongCustomerIdOrPassword);
                }
                else
                {
                    await ClientApiExceptionHandler.HandleErrors(ex);
                }
            }
            catch (Exception ex1)
            {
                Dbg.BreakAndLog(ex1);

                IsBusy = false;

                AppEnvironment.AuthService.Logout();

                await DialogService.ShowUnknownError();
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void DEBUG_SetDummyData()
        {
            AccountNumber = "2000000591";
            Password = "Password01";
        }
    }
}
