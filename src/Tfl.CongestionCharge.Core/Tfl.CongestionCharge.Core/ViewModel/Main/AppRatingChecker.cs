﻿using System.Threading.Tasks;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace Tfl.CongestionCharge.Core.ViewModel.Main
{
    public class AppRatingChecker : IAppRatingChecker
    {
        private const int FirstTimeAsk = 3;
        private const int SecondTimeAsk = 10;

        private readonly AppState _appState;
        private readonly IUserDialog _dialogService;
        private readonly IAppNavigationService _navigationService;

        public AppRatingChecker(
            AppState appState,
            IUserDialog dialogService,
            IAppNavigationService navigationService)
        {
            _appState = appState;
            _dialogService = dialogService;
            _navigationService = navigationService;
        }

        public async Task CheckToShowReviewPrompt()
        {
            _appState.StartupCounterForThisVersion++;

            if (_appState.AskedToRateCounter >= 3 || _appState.UserHasRatedApp)
            {
                return;
            }

            if (_appState.StartupCounterForThisVersion == FirstTimeAsk || (_appState.StartupCounterForThisVersion - FirstTimeAsk) % SecondTimeAsk == 0)
            {
                _appState.AskedToRateCounter++;

                var result = await _dialogService.Show2ButtonMessage(Strings.PleaseRateThisApp, Strings.PleaseRateThisAppTitle, Strings.Rate, Strings.Cancel);

                if (result)
                {
                    _appState.UserHasRatedApp = true;

                    _navigationService.NavigateToAppStoreReview();
                }
            }
        }
    }
}