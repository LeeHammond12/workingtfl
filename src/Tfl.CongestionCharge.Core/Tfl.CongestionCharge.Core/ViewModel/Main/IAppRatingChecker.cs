﻿using System.Threading.Tasks;

namespace Tfl.CongestionCharge.Core.ViewModel.Main
{
    public interface IAppRatingChecker
    {
        Task CheckToShowReviewPrompt();
    }
}