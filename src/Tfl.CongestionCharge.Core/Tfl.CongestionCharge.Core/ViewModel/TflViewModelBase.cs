﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FluentValidation.Results;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Model;
using Tfl.CongestionCharge.Core.Model.Base;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Services;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;

namespace Tfl.CongestionCharge.Core.ViewModel
{
    public enum ViewModelInitResult
    {
        Success,
        Abort
    }

    public abstract class TflViewModelBase : ViewModelBase
    {
        private List<RelayCommand> _commands;

        private RelayCommand _navigateToPrivacyCommand;
        private RelayCommand _navigateToTcCommand;
        private RelayCommand _goBackCommand;
        private RelayCommand _goBackToHomeCommand;

        public bool IsBusy { get; set; }

        public bool IsLoggedIn => AppEnvironment.AuthService.IsLoggedIn;

        public virtual bool IsGoBackToHomeButtonVisible => true;

        // these commands are used in a few places, that's why they're here
        public RelayCommand GoBackToHomeCommand
            => _goBackToHomeCommand ?? (_goBackToHomeCommand = new RelayCommand(
                           () => GoBackToHome(),
                           () => !IsBusy && IsGoBackToHomeButtonVisible));

        public RelayCommand NavigateToPrivacyCommand
            => _navigateToPrivacyCommand ?? (_navigateToPrivacyCommand = new RelayCommand(
                           () => NavigationService.NavigateTo(AppPage.PrivacyPolicyBrowser, this)));

        public RelayCommand NavigateToTcCommand => _navigateToTcCommand
                       ?? (_navigateToTcCommand = new RelayCommand(
                           () => NavigationService.NavigateTo(AppPage.TermsAndConditions, this)));

        public RelayCommand GoBackCommand
            => _goBackCommand ?? (_goBackCommand = new RelayCommand(() => NavigationService.GoBackOnePage()));

        protected IAppNavigationService NavigationService => AppEnvironment.NavigationService;

        protected IUserDialog DialogService => AppEnvironment.DialogService;

        protected IClientApiExceptionHandler ClientApiExceptionHandler => AppEnvironment.ClientApiExceptionHandler;

        public IUserInformationService UserInformation => AppEnvironment.UserInformation;
        public ILoggedInUserInformationService LoggedInUser => UserInformation as ILoggedInUserInformationService;
        public IAnonymousUserInformationService AnonymousUser => UserInformation as IAnonymousUserInformationService;

        protected ITflApi TflApi => AppEnvironment.TflApi;

        protected AppEnvironment AppEnvironment { get; }

        protected FlowState FlowState => AppEnvironment.FlowState;

        protected AppState AppState => AppEnvironment.AppState;

        protected ValidationResult ValidationResult { get; set; }

        protected TflViewModelBase(AppEnvironment appEnvironment)
        {
            AppEnvironment = appEnvironment;
            PropertyChanged += BaseViewModel_PropertyChanged;

        }

        //Suppress warning CS1998: This async method lacks 'await'
#pragma warning disable 1998
        public virtual async Task<ViewModelInitResult> InitViewModel()
        {
            return ViewModelInitResult.Success;
        }
#pragma warning restore 1998

        public virtual void SaveUiState(IKeyValueStore store)
        {
        }

        public virtual void RestoreUiState(IKeyValueStore store)
        {
        }

        /// <summary>
        /// loads the user information from the device or the cloud.
        /// If the loading fails, a dialog may be shown to retry loading.
        /// </summary>
        /// <returns>true if loading succeeded</returns>
        protected async Task<ViewModelInitResult> LoadUserInformationWithRetry(bool useCachedValue = false)
        {
            return await ExecuteUntilResult(async () => await UpdateUserInformation(useCachedValue)) != null ?
                ViewModelInitResult.Success :
                ViewModelInitResult.Abort;
        }

        protected async Task<ViewModelInitResult> LoadUserInformation(bool useCachedValue = false)
        {
            return (await Execute(async () => await UpdateUserInformation(useCachedValue)))?.Item2 == true ?
                ViewModelInitResult.Success :
                ViewModelInitResult.Abort;
        }

        protected async Task<T> ExecuteUntilResult<T>(Func<Task<T>> api) where T : class
        {
            do
            {
                var result = await Execute(api);
                if (result.Item2)
                {
                    return result.Item1;
                }
            }
            while (true);
        }

        private async Task<Tuple<T, bool>> Execute<T>(Func<Task<T>> api) where T : class
        {
            IsBusy = true;
            try
            {
                var result = await api.Invoke();
                return new Tuple<T, bool>(result, true);
            }
            catch (ClientApiException ex)
            {
                IsBusy = false;

                await ClientApiExceptionHandler.HandleErrors(ex);

                // Handle errors will navigate to login when this error occurs,
                // therefore the loop needs to be aborted
                if (ex.IsAuthenticationException)
                {
                    return new Tuple<T, bool>(null, true);
                }
            }
            finally
            {
                IsBusy = false;
            }
            return new Tuple<T, bool>(null, false);
        }

        protected void RaiseAllCanExecuteChanged()
        {
            if (_commands == null)
            {
                _commands = new List<RelayCommand>();
                var properties = GetType().GetRuntimeProperties();
                foreach (var property in properties.Where(p => p.PropertyType == typeof(RelayCommand)))
                {
                    var val = (RelayCommand)property.GetValue(this);
                    _commands.Add(val);
                }
            }

            foreach (var relayCommand in _commands)
            {
                relayCommand.RaiseCanExecuteChanged();
            }
        }

        protected void NavigateToPaymentPage(ChargeType chargeType)
        {
            var canSelectCards = UserInformation.PaymentCards.Any() &&
                                                    // for logged in users no stored cards are available when paying PCN
                                                    !(chargeType == ChargeType.PenaltyChargeNotice && AppEnvironment.AuthService.IsLoggedIn);
            NavigationService.NavigateTo(canSelectCards ? AppPage.SelectPaymentCard : AppPage.Payment, this);
        }

        protected virtual void GoBackToHome()
        {
            NavigationService.NavigateHomeAndClearBackStack();
        }

        protected async Task<object> UpdateUserInformation(bool useCachedValue)
        {
            await UserInformation.UpdateInformation(useCachedValue);
            return new object();
        }

        public async Task<bool> CheckQmlVehicleForTCharge(VehicleViewModel vehicle)
        {
            if (!IsLoggedIn)
                return true;

            bool vrmIsValidated;

            try
            {
                IsBusy = true;

                var lookupVrmDto = await TflApi.LookupVrm(vehicle.Vrm, false);

                IsBusy = false;

                if (!lookupVrmDto.IsCcChargeable)
                {
                    if (lookupVrmDto.IsESChargeable == (int)EsChargeFlag.NotSubject ||
                        lookupVrmDto.IsESChargeable == (int)EsChargeFlag.SubjectButExempt)
                    {
                        await DialogService.ShowOkMessageWithLink(Strings.VehicleExemptMessage, Strings.Warning);
                    }
                    else if (lookupVrmDto.IsESChargeable == (int)EsChargeFlag.Subject)
                    {
                        await DialogService.ShowOkMessageWithLink(Strings.EsVehicleIsCcExempt, Strings.Warning);
                    }
                }

                vrmIsValidated = true;
            }
            catch (ClientApiException ex)
            {
                IsBusy = false;

                vrmIsValidated = ClientApiExceptionHandler.ContainsErrorCode(ex, ClientApiErrorCodes.VrmNoVehicleDetailsFound);

                await ClientApiExceptionHandler.HandleErrors(ex);
            }
            return vrmIsValidated;
        }

        private void BaseViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RaiseAllCanExecuteChanged();
        }
    }
}