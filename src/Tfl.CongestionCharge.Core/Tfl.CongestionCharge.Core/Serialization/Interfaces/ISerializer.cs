﻿namespace Tfl.CongestionCharge.Core.Serialization.Interfaces
{
    public interface IDeserializer<T>
    {
        Q Deserialize<Q>(T qToDeserialize);
    }

    public interface ISerializer<T>
    {
        T Serialize(object objectToSerialize);
    }
}
