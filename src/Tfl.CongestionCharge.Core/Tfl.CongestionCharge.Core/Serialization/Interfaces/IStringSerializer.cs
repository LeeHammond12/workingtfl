﻿namespace Tfl.CongestionCharge.Core.Serialization.Interfaces
{
    public interface IFromStringDeserializer : IDeserializer<string>
    { }

    public interface IToStringSerializer : ISerializer<string>
    { }

    public interface IStringSerializer : IFromStringDeserializer, IToStringSerializer
    { }
}
