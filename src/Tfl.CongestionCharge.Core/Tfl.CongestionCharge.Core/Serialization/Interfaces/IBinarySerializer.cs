﻿namespace Tfl.CongestionCharge.Core.Serialization.Interfaces
{
    public interface IFromBinaryDeserializer : IDeserializer<byte[]>
    { }

    public interface IToBinarySerializer : ISerializer<byte[]>
    { }

    public interface IBinarySerializer : IFromBinaryDeserializer, IToBinarySerializer
    { }
}
