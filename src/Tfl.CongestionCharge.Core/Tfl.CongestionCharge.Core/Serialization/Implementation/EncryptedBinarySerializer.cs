﻿using Newtonsoft.Json;
using Tfl.CongestionCharge.Core.PCLCrypto;
using Tfl.CongestionCharge.Core.Serialization.Interfaces;

namespace Tfl.CongestionCharge.Core.Serialization.Implementation
{
    public class EncryptedBinarySerializer : IBinarySerializer
    {
        private readonly byte[] _key;

        public EncryptedBinarySerializer(ICryptoKeyStore cryptoKeyStore)
        {
            _key = cryptoKeyStore.GetKey();
        }

        public T Deserialize<T>(byte[] qToDeserialize)
        {
            var json = CryptoUtilities.Decrypt(qToDeserialize, _key);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public byte[] Serialize(object objectToSerialize)
        {
            var data = JsonConvert.SerializeObject(objectToSerialize);
            return CryptoUtilities.Encrypt(data, _key);
        }
    }
}
