﻿using Newtonsoft.Json.Linq;
using Tfl.CongestionCharge.Core.Serialization.Interfaces;

namespace Tfl.CongestionCharge.Core.Serialization.Implementation
{
    public class StringToJsonDeserializer : IFromStringDeserializer
    {
        public Q Deserialize<Q>(string qToDeserialize)
        {
            if (qToDeserialize.StartsWith("["))
            {
                var arr = JArray.Parse(qToDeserialize);
                return arr.ToObject<Q>();
            }

            var jobject = JObject.Parse(qToDeserialize);
            return jobject.ToObject<Q>();
        }
    }
}
