﻿using Newtonsoft.Json;
using Tfl.CongestionCharge.Core.Serialization.Interfaces;

namespace Tfl.CongestionCharge.Core.Serialization.Implementation
{
    public class JsonToStringSerializer : IToStringSerializer
    {
        public string Serialize(object objectToSerialize)
        {
            return JsonConvert.SerializeObject(objectToSerialize);
        }
    }
}
