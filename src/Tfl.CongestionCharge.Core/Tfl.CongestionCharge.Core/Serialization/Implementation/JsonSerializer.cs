﻿using Tfl.CongestionCharge.Core.Serialization.Interfaces;

namespace Tfl.CongestionCharge.Core.Serialization.Implementation
{
    public class JsonSerializer : IToStringSerializer, IFromStringDeserializer
    {
        private static readonly StringToJsonDeserializer StringToJsonDeserializer = new StringToJsonDeserializer();
        private static readonly JsonToStringSerializer JsonToStringSerializer = new JsonToStringSerializer();

        public string Serialize(object objectToSerialize)
        {
            return JsonToStringSerializer.Serialize(objectToSerialize);
        }

        public Q Deserialize<Q>(string qToDeserialize)
        {
            return StringToJsonDeserializer.Deserialize<Q>(qToDeserialize);
        }
    }
}
