﻿using Android.Text;
using Android.Widget;
using Java.Lang;
using Java.Text;
using Java.Util;
using Object = Java.Lang.Object;

namespace TfL.CongestionCharge.Droid.Utils
{
    public class CreditCardDateFormatter : Object, ITextWatcher
    {
        private EditText _mExpiryDate;
        private string _mLastInput;

        public CreditCardDateFormatter(EditText mExpiryDate)
        {
            _mExpiryDate = mExpiryDate;
        }

        public void AfterTextChanged(IEditable s)
        {
            var input = s.ToString();
            var formatter = new SimpleDateFormat("MM/yy", Locale.English);
            var expiryDateDate = Calendar.Instance;
            try
            {
                expiryDateDate.Time = formatter.Parse(input);
            }
            catch (ParseException)
            {
                if (s.Length() == 2 && !_mLastInput.EndsWith("/"))
                {
                    var month = Integer.ParseInt(input);
                    if (month <= 12)
                    {
                        _mExpiryDate.Text = _mExpiryDate.Text + "/";
                        _mExpiryDate.SetSelection(_mExpiryDate.Text.Length);
                    }
                }
                else if (s.Length() == 2 && _mLastInput.EndsWith("/"))
                {
                    var month = Integer.ParseInt(input);
                    if (month <= 12)
                    {
                        _mExpiryDate.Text = _mExpiryDate.Text.Substring(0, 1);
                        _mExpiryDate.SetSelection(_mExpiryDate.Text.Length);
                    }
                    else
                    {
                        _mExpiryDate.Text = string.Empty;
                        _mExpiryDate.SetSelection(_mExpiryDate.Text.Length);
                    }
                }
                else if (s.Length() == 1)
                {
                    var month = Integer.ParseInt(input);
                    if (month > 1)
                    {
                        _mExpiryDate.Text = "0" + _mExpiryDate.Text + "/";
                        _mExpiryDate.SetSelection(_mExpiryDate.Text.Length);
                    }
                }
                _mLastInput = _mExpiryDate.Text;
            }
        }

        public void BeforeTextChanged(ICharSequence s, int start, int count, int after)
        {
        }

        public void OnTextChanged(ICharSequence s, int start, int before, int count)
        {
        }
    }
}