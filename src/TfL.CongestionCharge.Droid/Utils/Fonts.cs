﻿using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Util;
using Android.Widget;

namespace TfL.CongestionCharge.Droid.Utils
{
    public static class Fonts
    {
        private static readonly ConcurrentDictionary<string, Typeface> FontCache = new ConcurrentDictionary<string, Typeface>();

        public static void SetFont(TextView textview, Context context, IAttributeSet attrs)
        {
            TypedArray a = Application.Context.ObtainStyledAttributes(attrs, Resource.Styleable.TflTextView);
            string fontName = a.GetString(Resource.Styleable.TflTextView_fontName);

            // there must be some sort of object pool for this type
            a.Recycle();

            Typeface tf = FontCache.GetOrAdd(fontName, LoadFont);
            textview.SetTypeface(tf, TypefaceStyle.Normal);
        }

        public static bool Precache(string fontName)
        {
            return FontCache.TryAdd(fontName, LoadFont(fontName));
        }

        public static void SetBook(this EditText editView)
        {
            var tf = Typeface.CreateFromAsset(Application.Context.Assets, "fonts/njfont_book.ttf");
            editView.SetTypeface(tf, TypefaceStyle.Normal);
        }

        public static void SetBookLight(this EditText editView)
        {
            var tf = Typeface.CreateFromAsset(Application.Context.Assets, "fonts/njfont_light.ttf");
            editView.SetTypeface(tf, TypefaceStyle.Normal);
        }

        public static void SetBook(this CheckBox chkBox, bool bold = false)
        {
            var tf = Typeface.CreateFromAsset(Application.Context.Assets, "fonts/njfont_book.ttf");
            chkBox.SetTypeface(tf, bold ? TypefaceStyle.Bold : TypefaceStyle.Normal);
        }

        public static void SetBook(this RadioButton rdoButton)
        {
            var tf = Typeface.CreateFromAsset(Application.Context.Assets, "fonts/njfont_book.ttf");
            rdoButton.SetTypeface(tf, TypefaceStyle.Normal);
        }

        private static Typeface LoadFont(string fontName)
        {
            var filename = $"{fontName}.ttf";

            // If the typeface doesn't exist log it and return the default typeface
            var assets = Application.Context.Resources.Assets.List("fonts");
            if (!assets.Contains(filename))
            {
                Debug.WriteLine($"Could not load typeface {filename}");
                return Typeface.Default;
            }

            return Typeface.CreateFromAsset(Application.Context.Assets, $"fonts/{filename}");
        }
    }
}