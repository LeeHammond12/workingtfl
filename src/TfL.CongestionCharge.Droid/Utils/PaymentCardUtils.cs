﻿using System.Linq;
using TfL.CongestionCharge.Droid.Activities;

namespace TfL.CongestionCharge.Droid.Utils
{
    internal class PaymentCardUtils
    {
        public static int GetImageResourceFromCardName(string cardName)
        {
            try
            {
                var current = ForegroundActivity.Current;
                var name = cardName.Split('.');
                if (name.Length == 0 || current == null)
                {
                    return -1;
                }

                var card = name.FirstOrDefault()?.ToLower();
                if (card == null)
                {
                    return -1;
                }

                card = card.Replace("debit", string.Empty);
                return current.Resources.GetIdentifier(card, "drawable", current.PackageName);
            }
            catch (System.Exception)
            {
                return -1;
            }
        }
    }
}