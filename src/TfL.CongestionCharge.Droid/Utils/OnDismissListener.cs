﻿using System;
using Android.Content;
using Object = Java.Lang.Object;

namespace TfL.CongestionCharge.Droid
{
    public class OnDismissListener : Object, IDialogInterfaceOnDismissListener
    {
        private readonly Action _action;

        public OnDismissListener(Action action)
        {
            _action = action;
        }

        public void OnDismiss(IDialogInterface dialog)
        {
            _action();
        }
    }
}