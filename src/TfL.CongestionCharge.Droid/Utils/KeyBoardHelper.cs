﻿using System;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;

namespace TfL.CongestionCharge.Droid.Utils
{
    public class KeyBoardHelper
    {
        public static void HideKeyboard(Context ctx, Window window)
        {
            try
            {
                var inputManager = (InputMethodManager)ctx.GetSystemService(Context.InputMethodService);
                inputManager?.HideSoftInputFromWindow(window.DecorView.WindowToken, HideSoftInputFlags.NotAlways);
            }
            catch (Exception)
            {
                //ignored -- Emulator Issue when SoftKeyboard is Disabled
            }
        }
    }
}