﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace TfL.CongestionCharge.Droid.Utils
{
    public class AndroidUtil
    {
        public static ViewStates ToVisibility(bool visible)
        {
            return visible ? ViewStates.Visible : ViewStates.Gone;
        }

        public static ViewStates ToInvertedVisibility(bool visible)
        {
            return ToVisibility(!visible);
        }

        public static float ConvertDpToPixel(float dp, Context context)
        {
            var resources = context.Resources;
            var metrics = resources.DisplayMetrics;
            float px = dp * ((float)metrics.DensityDpi / (float)DisplayMetrics.DensityDefault);
            return px;
        }

        public static float ConvertPixelsToDp(float px, Context context)
        {
            var resources = context.Resources;
            var metrics = resources.DisplayMetrics;
            float dp = px / ((float)metrics.DensityDpi / (float)DisplayMetrics.DensityDefault);
            return dp;
        }
    }
}