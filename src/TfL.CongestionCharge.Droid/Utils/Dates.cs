﻿using System.Collections.Generic;

namespace TfL.CongestionCharge.Droid.Utils
{
    public static class Dates
    {
        public static int MonthValFromMonth(string month)
        {
            var mo = new List<string>()
            {
                "january",
                "february",
                "march",
                "april",
                "may",
                "june",
                "july",
                "august",
                "september",
                "october",
                "november",
                "december"
            };
            month = month.ToLower();
            var retVal = mo.IndexOf(month) + 1;
            return retVal;
        }

        public static bool ValidateMonthName(string month)
        {
            var mo = new List<string>()
            {
                "january",
                "february",
                "march",
                "april",
                "may",
                "june",
                "july",
                "august",
                "september",
                "october",
                "november",
                "december"
            };
            month = month.ToLower();
            return mo.Contains(month) ? true : false;
        }
    }
}