﻿using Android.Graphics;
using Android.Text.Style;
using Java.Lang;

namespace TfL.CongestionCharge.Droid.Utils
{
    public class PaddingRightSpan : ReplacementSpan
    {
        private int mPadding;

        public PaddingRightSpan(int padding)
        {
            mPadding = padding;
        }

        public override void Draw(Canvas canvas, ICharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint)
        {
            canvas.DrawText(text, start, end, x, y, paint);
        }

        public override int GetSize(Paint paint, ICharSequence text, int start, int end, Paint.FontMetricsInt fm)
        {
            var widths = new float[end - start];
            paint.GetTextWidths(text, start, end, widths);
            var sum = mPadding;
            for (var i = 0; i < widths.Length; i++)
            {
                sum += (int)widths[i];
            }
            return sum;
        }
    }
}