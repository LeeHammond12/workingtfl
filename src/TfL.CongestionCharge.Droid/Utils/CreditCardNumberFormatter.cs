﻿using Android.Content;
using Android.Text;
using Android.Util;
using Android.Widget;
using Java.Lang;
using Java.Text;
using Java.Util;
using String = System.String;

namespace TfL.CongestionCharge.Droid.Utils
{
    public class CreditCardNumberFormatter : Object, ITextWatcher
    {
        public static int NoMaxLength = -1;
        private int _maxLength = NoMaxLength;
        private int _paddingPx;
        private bool _internalStopFormatFlag;

        public CreditCardNumberFormatter(int paddingPx)
        {
            SetPaddingPx(paddingPx);
        }

        public CreditCardNumberFormatter(TextView textView)
        {
            SetPaddingEm(textView, 1f);
        }

        public CreditCardNumberFormatter(TextView textView, float paddingEm)
        {
            SetPaddingEm(textView, paddingEm);
        }

        public CreditCardNumberFormatter(Context context, float paddingSp)
        {
            SetPaddingSp(context, paddingSp);
        }

        public void SetPaddingEm(TextView textView, float em)
        {
            var emSize = textView.Paint.MeasureText("x");
            SetPaddingPx((int)(em * emSize));
        }

        public void SetPaddingPx(int paddingPx)
        {
            _paddingPx = paddingPx;
        }

        public void SetPaddingSp(Context context, float paddingSp)
        {
            SetPaddingPx((int)TypedValue.ApplyDimension(ComplexUnitType.Sp, paddingSp,
                context.Resources.DisplayMetrics));
        }

        public void SetMaxLength(int maxLength)
        {
            _maxLength = maxLength;
        }

        public void FormatCardNumber(TextView textView)
        {
            AfterTextChanged(textView.EditableText);
        }

        public static void FormatCardNumber(IEditable ccNumber, int paddingPx)
        {
            FormatCardNumber(ccNumber, paddingPx, NoMaxLength);
        }

        public static void FormatCardNumber(IEditable ccNumber, int paddingPx, int maxLength)
        {
            var textLength = ccNumber.Length();
            // first remove any previous span
            var spans = ccNumber.GetSpans(0, ccNumber.Length(), Class.FromType(typeof(PaddingRightSpan)));

            for (var i = 0; i < spans.Length; i++)
            {
                ccNumber.RemoveSpan(spans[i]);
            }

            // then truncate to max length
            if (maxLength > 0 && textLength > maxLength - 1)
            {
                ccNumber.Replace(maxLength, textLength, string.Empty);
            }
            // finally add margin spans
            for (var i = 1; i <= (textLength - 1) / 4; i++)
            {
                var end = i * 4;
                var start = end - 1;
                var marginSPan = new PaddingRightSpan(paddingPx);
                ccNumber.SetSpan(marginSPan, start, end, SpanTypes.ExclusiveExclusive);
            }
        }

        public void AfterTextChanged(IEditable s)
        {
            if (_internalStopFormatFlag)
            {
                return;
            }
            _internalStopFormatFlag = true;
            FormatCardNumber(s, _paddingPx, _maxLength);
            _internalStopFormatFlag = false;
        }

        public void BeforeTextChanged(ICharSequence s, int start, int count, int after)
        {
        }

        public void OnTextChanged(ICharSequence s, int start, int before, int count)
        {
        }
    }
}