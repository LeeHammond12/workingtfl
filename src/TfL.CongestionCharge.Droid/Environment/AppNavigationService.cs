﻿using System;
using Android.App;
using Android.Content;
using JimBobBennett.MvvmLight.AppCompat;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Helper;
using TfL.CongestionCharge.Droid.Activities;
using Uri = Android.Net.Uri;

namespace TfL.CongestionCharge.Droid.Environment
{
    public class AppNavigationService : IAppNavigationService
    {
        private readonly AppCompatNavigationService _navigationService;

        public AppNavigationService()
        {
            _navigationService = new AppCompatNavigationService();

            _navigationService.Configure(AppPage.ChooseAVehicle.ToString(), typeof(WhichVehicleActivity));
            _navigationService.Configure(AppPage.LookupVrm.ToString(), typeof(LookupVrmActivity));
            _navigationService.Configure(AppPage.ConfirmVrm.ToString(), typeof(ConfirmVrmActivity));
            _navigationService.Configure(AppPage.PcnLookup.ToString(), typeof(PcnLookupActivity));
            _navigationService.Configure(AppPage.PostCodeChecker.ToString(), typeof(PostCodeCheckerActivity));
            _navigationService.Configure(AppPage.ChooseDates.ToString(), typeof(ChooseDatesActivity));
            _navigationService.Configure(AppPage.AllDone.ToString(), typeof(AllDoneActivity));
            _navigationService.Configure(AppPage.SelectPaymentCard.ToString(), typeof(SelectPaymentCardActivity));
            _navigationService.Configure(AppPage.Payment.ToString(), typeof(PaymentActivity));
            _navigationService.Configure(AppPage.ViewReceipt.ToString(), typeof(ViewReceiptsActivity));
            _navigationService.Configure(AppPage.YourVehiclesAndServices.ToString(), typeof(YourVehiclesAndServicesActivity));
            _navigationService.Configure(AppPage.YourAccount.ToString(), typeof(YourAccountActivity));
            _navigationService.Configure(AppPage.LoginPageKey.ToString(), typeof(LoginActivity));
            _navigationService.Configure(AppPage.AutoPayCreditCard.ToString(), typeof(AutoPayPaymentCardActivity));
            _navigationService.Configure(AppPage.PayCcSelectVehicle.ToString(), typeof(WhichVehicleActivity));
            _navigationService.Configure(AppPage.JourneyDetails.ToString(), typeof(JourneyDetailsActivity));
            _navigationService.Configure(AppPage.YourVehiclesAndServices.ToString(), typeof(YourVehiclesAndServicesActivity));
            _navigationService.Configure(AppPage.YourVehiclesAnonymousUser.ToString(), typeof(AnonymousUserYourVehiclesActivity));
            _navigationService.Configure(AppPage.AnonymousUserYourPaymentMethods.ToString(), typeof(AnonymousUserYourPaymentMethodsActivity));
            _navigationService.Configure(AppPage.LoggedInUserYourPaymentMethods.ToString(), typeof(LoggedInUserYourPaymentMethodsActivity));
            _navigationService.Configure(AppPage.YourVehiclesAndCards.ToString(), typeof(ManageStoredInformationActivity));
            _navigationService.Configure(AppPage.AutoPayVehicles.ToString(), typeof(AutoPayVehiclesActivity));
            _navigationService.Configure(AppPage.TermsAndConditionsBrowser.ToString(), typeof(TermsAndConditionsBrowserActivity));
            _navigationService.Configure(AppPage.TermsAndConditions.ToString(), typeof(TermsAndConditionsActivity));
            _navigationService.Configure(AppPage.PrivacyPolicyBrowser.ToString(), typeof(PrivacyPolicyBrowserActivity));
            _navigationService.Configure(AppPage.ManageCcAutoPay.ToString(), typeof(ManageCcAutoPayActivity));
        }

        public void NavigateTo(AppPage page, object parameter)
        {
            _navigationService.NavigateTo(page.ToString(), parameter);
            AppCompatActivityBase.CurrentActivity.OverridePendingTransition(Resource.Animation.slide_in_left, Resource.Animation.slide_out_left);
        }

        public void NavigateHomeAndClearBackStack()
        {
            var context = ForegroundActivity.Current;
            Intent intent = new Intent(context, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
            context.StartActivity(intent);
            context.Finish();
        }

        public void NavigateToLoginAndClearBackStack()
        {
            var context = ForegroundActivity.Current;
            Intent intent = new Intent(context, typeof(LoginActivity));
            intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
            context.StartActivity(intent);
            context.Finish();
        }

        public void GoBackOnePage()
        {
            ForegroundActivity.Current.Finish();
        }

        public void NavigateToAppDownloadPage()
        {
            try
            {
                string appPackageName = Application.Context.PackageName;

                try
                {
                    var intent = new Intent(Intent.ActionView, Uri.Parse("market://details?id=" + appPackageName));

                    // we need to add this, because the activity is in a new context.
                    // Otherwise the runtime will block the execution and throw an exception
                    intent.AddFlags(ActivityFlags.NewTask);

                    Application.Context.StartActivity(intent);
                }
                catch (ActivityNotFoundException)
                {
                    var intent = new Intent(Intent.ActionView, Uri.Parse("http://play.google.com/store/apps/details?id=" + appPackageName));

                    // we need to add this, because the activity is in a new context.
                    // Otherwise the runtime will block the execution and throw an exception
                    intent.AddFlags(ActivityFlags.NewTask);

                    Application.Context.StartActivity(intent);
                }
            }
            catch (Exception ex)
            {
                Dbg.BreakAndLog(ex);
            }
        }

        public void NavigateToAppStoreReview()
        {
            NavigateToAppDownloadPage();
        }
    }
}