﻿using System.Threading.Tasks;
using GalaSoft.MvvmLight.Views;
using JimBobBennett.MvvmLight.AppCompat;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;

namespace TfL.CongestionCharge.Droid.Environment
{
    public class UserDialog : IUserDialog
    {
        private readonly IDialogService _dialogService;

        public UserDialog()
        {
            _dialogService = new AppCompatDialogService();
        }

        public async Task ShowError(string message)
        {
            await ShowMessage(message, Strings.ErrorTitle);
        }

        public async Task ShowUnknownError()
        {
            await ShowMessage(Strings.TransactionFailedMessage, Strings.TransactionFailedTitle);
        }

        public async Task ShowWarning(string message)
        {
            await ShowMessage(message, Strings.Warning);
        }

        public async Task ShowMessage(string message, string title)
        {
            await _dialogService.ShowMessage(message, title);
        }

        public async Task<bool> Show2ButtonMessage(string message, string title, string buttonConfirmText, string buttonCancelText)
        {
            return await _dialogService.ShowMessage(message, title, buttonConfirmText, buttonCancelText, null);
        }

        public async Task ShowOkMessageWithLink(string message, string title)
        {
            await _dialogService.ShowMessage(message, title);
        }
    }
}