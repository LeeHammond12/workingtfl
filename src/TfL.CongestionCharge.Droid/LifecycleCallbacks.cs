﻿using Android.App;
using Android.OS;
using Java.Lang;

namespace TfL.CongestionCharge.Droid
{
    public class LifecycleCallbacks : Object, Application.IActivityLifecycleCallbacks
    {
        public static bool WasInBackground;
        public static bool InStoppedState;
        public static bool TrimMemoryHasBeenCalled;

        public static void Reset()
        {
            WasInBackground = false;
            TrimMemoryHasBeenCalled = false;
        }

        public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
        {
            InStoppedState = false;
            WasInBackground = false;
            TrimMemoryHasBeenCalled = false;
        }

        public void OnActivityDestroyed(Activity activity)
        {
            WasInBackground = false;
        }

        public void OnActivityPaused(Activity activity)
        {
            InStoppedState = false;
        }

        public void OnActivityResumed(Activity activity)
        {
            InStoppedState = false;
        }

        public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
        {
            InStoppedState = false;
        }

        public void OnActivityStarted(Activity activity)
        {
            InStoppedState = false;
        }

        public void OnActivityStopped(Activity activity)
        {
            InStoppedState = true;

            if (TrimMemoryHasBeenCalled)
            {
                WasInBackground = true;
            }
        }

        public void TrimMemoryCalled()
        {
            TrimMemoryHasBeenCalled = true;

            if (InStoppedState)
            {
                WasInBackground = true;
            }
        }
    }
}