﻿namespace Thumbmunkeys.Common.Platform.CSharpCompatibility
{
    public class JObjectWrapper : Java.Lang.Object
    {
        public object Object { get; set; }

        public JObjectWrapper()
        {
        }

        public JObjectWrapper(object o)
        {
            Object = o;
        }
    }
}