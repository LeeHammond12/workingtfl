﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Text;
using Android.Text.Method;
using Android.Text.Util;
using Android.Views;
using Android.Widget;
using JimBobBennett.MvvmLight.AppCompat;
using Tfl.CongestionCharge.Core.Helper;
using Tfl.CongestionCharge.Core.Resources;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.AppCompat
{
    public static class DialogHelper
    {
        private static TfLAlertDialog Dialog;
        private static View View;

        public static AlertDialogInfo CreateDialog(string message, string title, string okText = null, string cancelText = null, Action<bool> afterHideCallbackWithResponse = null)
        {
            var tcs = new TaskCompletionSource<bool>();

            View = GetViewWithValues(title, okText, message, cancelText);

            var builder = GetBuilderForView(View);

            var okButton = View.FindButton(Resource.Id.btnOk);
            var cancelButton = View.FindButton(Resource.Id.btnCancel);

            if (string.IsNullOrEmpty(cancelText))
            {
                ShowOnlyOkButton(okButton, cancelButton);
            }
            else
            {
                cancelButton.Click += (s, e) => Dialog?.Dismiss();
            }

            okButton.Click += delegate
            {
                try
                {
                    Dialog?.Hide();
                    Dialog?.Dispose();

                    afterHideCallbackWithResponse?.Invoke(true);
                    tcs.TrySetResult(true);
                }
                catch (ObjectDisposedException)
                {
                    Dialog = null;
                }
            };

            builder.SetOnDismissListener(new OnDismissListener(() =>
            {
                try
                {
                    Dialog?.Dispose();

                    afterHideCallbackWithResponse?.Invoke(false);
                    tcs.TrySetResult(false);
                }
                catch (ObjectDisposedException)
                {
                    Dialog = null;
                }
            }));

            Dialog = builder;

            return new AlertDialogInfo(Dialog, tcs);
        }

        private static TfLAlertDialog GetBuilderForView(View view)
        {
            var builder = new TfLAlertDialog(AppCompatActivityBase.CurrentActivity, Resource.Style.CustomDialogTheme);
            builder.SetView(view);
            builder.SetCancelable(true);
            builder.SetCanceledOnTouchOutside(true);
            return builder;
        }

        private static View GetViewWithValues(string title, string okText, string message, string cancelText)
        {
            var view = AppCompatActivityBase.CurrentActivity.LayoutInflater.Inflate(Resource.Layout.CustomDialogWithTwoButton, null);
            view.FindTextView(Resource.Id.title).Text = !string.IsNullOrEmpty(title) ? title : "Info";

            var okButton = view.FindButton(Resource.Id.btnOk);
            okButton.Text = !string.IsNullOrEmpty(okText) ? okText : Strings.Ok;

            var cancelButton = view.FindButton(Resource.Id.btnCancel);
            cancelButton.Text = !string.IsNullOrEmpty(cancelText) ? cancelText : Strings.Cancel;

            var messageText = view.FindTextView(Resource.Id.dialogMessage);

            if (!string.IsNullOrEmpty(message))
            {
                messageText?.FormatTextViewTextWithHyperLink(message);
            }

            return view;
        }

        private static void ShowOnlyOkButton(Button okButton, Button cancelButton)
        {
            cancelButton.Visibility = ViewStates.Gone;

            var layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent)
            {
                Weight = 1
            };

            okButton.LayoutParameters = layoutParams;
            okButton.Invalidate();
        }

        public static void ShowHintMessage(Activity activity, int layout)
        {
            View = activity.LayoutInflater.Inflate(layout, null);
            var closePopupImage = View.FindViewById<ImageView>(Resource.Id.closePopupImage);

            closePopupImage.Click += (sender, args) =>
            {
                if (Dialog != null)
                {
                    Dialog.Hide();
                    Dialog.Dispose();
                    Dialog = null;
                }
            };

            Dialog = GetBuilderForView(View);
            Dialog.SetView(View);
            Dialog.SetCancelable(true);
            Dialog.Show();
        }

        public struct AlertDialogInfo
        {
            private TfLAlertDialog _dialog;
            private TaskCompletionSource<bool> _tcs;

            public AlertDialogInfo(TfLAlertDialog dialog, TaskCompletionSource<bool> tcs)
            {
                _dialog = dialog;
                _tcs = tcs;
            }

            public Task<bool> Show()
            {
                _dialog.Show();
                return _tcs.Task;
            }
        }

        public class TfLAlertDialog : AlertDialog
        {
            public TfLAlertDialog(Context context, bool cancelable, EventHandler cancelHandler) : base(context, cancelable, cancelHandler)
            {
            }

            public TfLAlertDialog(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
            {
            }

            public TfLAlertDialog(Context context) : base(context)
            {
            }

            public TfLAlertDialog(Context context, bool cancelable, IDialogInterfaceOnCancelListener cancelListener) : base(context, cancelable, cancelListener)
            {
            }

            public TfLAlertDialog(Context context, int themeResId) : base(context, themeResId)
            {
            }
        }

        private sealed class OnDismissListener : Java.Lang.Object, IDialogInterfaceOnDismissListener
        {
            private readonly Action _action;

            public OnDismissListener(Action action)
            {
                _action = action;
            }

            public void OnDismiss(IDialogInterface dialog)
            {
                _action();
            }
        }
    }
}