﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Views;
using Tfl.CongestionCharge.Core.Resources;
using TfL.CongestionCharge.Droid.AppCompat;

namespace JimBobBennett.MvvmLight.AppCompat
{
    public class AppCompatDialogService : IDialogService
    {
        public async Task ShowError(string message, string title, string buttonText, Action afterHideCallback)
        {
            var afterHideCallbackWithResponse = (Action<bool>)(r =>
           {
               if (afterHideCallback == null)
                   return;
               afterHideCallback();
               afterHideCallback = null;
           });

            var dialog = DialogHelper.CreateDialog(message, title, buttonText, null, afterHideCallbackWithResponse);
            await dialog.Show();
        }

        public async Task ShowError(Exception error, string title, string buttonText, Action afterHideCallback)
        {
            var afterHideCallbackWithResponse = (Action<bool>)(r =>
            {
                if (afterHideCallback == null)
                    return;
                afterHideCallback();
                afterHideCallback = null;
            });

            var dialog = DialogHelper.CreateDialog(error.Message, title, buttonText, null, afterHideCallbackWithResponse);
            await dialog.Show();
        }

        public async Task ShowMessage(string message, string title)
        {
            var dialog = DialogHelper.CreateDialog(message, title);
            await dialog.Show();
        }

        public async Task ShowMessage(string message, string title, string buttonText, Action afterHideCallback)
        {
            var afterHideCallbackWithResponse = (Action<bool>)(r =>
            {
                if (afterHideCallback == null)
                    return;
                afterHideCallback();
                afterHideCallback = null;
            });

            var dialog = DialogHelper.CreateDialog(message, title, buttonText, null, afterHideCallbackWithResponse);
            await dialog.Show();
        }

        public Task<bool> ShowMessage(string message, string title, string buttonConfirmText, string buttonCancelText, Action<bool> afterHideCallback)
        {
            var afterHideCallbackWithResponse = (Action<bool>)(r =>
            {
                if (afterHideCallback == null)
                    return;
                afterHideCallback(r);
                afterHideCallback = null;
            });

            var dialog = DialogHelper.CreateDialog(message, title, buttonConfirmText, buttonCancelText ?? Strings.Cancel, afterHideCallbackWithResponse);
            return dialog.Show();
        }

        public Task ShowMessageBox(string message, string title)
        {
            return ShowMessage(message, title);
        }
    }
}