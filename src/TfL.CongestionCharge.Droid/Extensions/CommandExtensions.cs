﻿using Android.Widget;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Environment;
using TfL.CongestionCharge.Droid.Models;

namespace TfL.CongestionCharge.Droid.Extensions
{
    public static class CommandExtensions
    {
        public static void SetClickCommand(this object element, RelayCommand command)
        {
            element.SetCommand(Events.Click, command);
        }

        public static void NavigateToOnClick(this Button element, AppPage page, object parameter)
        {
            element.Click += (s, e) => AppEnvironment.Current.NavigationService.NavigateTo(page, parameter);
        }
    }
}
