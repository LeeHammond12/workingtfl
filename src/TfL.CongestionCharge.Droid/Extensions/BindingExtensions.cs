﻿using System;
using System.Linq.Expressions;
using GalaSoft.MvvmLight.Helpers;

namespace TfL.CongestionCharge.Droid.Extensions
{
    public static class BindingExtensions
    {
        public static Binding<TSource, TTarget> SetTwoWayBinding<TSource, TTarget>(this object source, Expression<Func<TSource>> sourcePropertyExpression, Expression<Func<TTarget>> targetPropertyExpression = null)
        {
            return source.SetBinding<TSource, TTarget>(sourcePropertyExpression, targetPropertyExpression, BindingMode.TwoWay);
        }

        public static Binding<TSource, TTarget> SetOneWayBinding<TSource, TTarget>(this object source, Expression<Func<TSource>> sourcePropertyExpression, Expression<Func<TTarget>> targetPropertyExpression = null)
        {
            return source.SetBinding<TSource, TTarget>(sourcePropertyExpression, targetPropertyExpression, BindingMode.OneWay);
        }

        public static Binding<TSource, TSource> SetTwoWayBinding<TSource>(this object source, Expression<Func<TSource>> sourcePropertyExpression)
        {
            return source.SetBinding(sourcePropertyExpression, BindingMode.TwoWay);
        }

        public static Binding<TSource, TSource> SetOneWayBinding<TSource>(this object source, Expression<Func<TSource>> sourcePropertyExpression)
        {
            return source.SetBinding(sourcePropertyExpression, BindingMode.OneWay);
        }
    }
}
