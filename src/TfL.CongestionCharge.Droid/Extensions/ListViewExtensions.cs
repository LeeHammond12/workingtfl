﻿using Android.Views;
using Android.Widget;

namespace TfL.CongestionCharge.Droid.Extensions
{
    public static class ListViewExtensions
    {
        public static void JustifyListViewHeightBasedOnChildren(this ListView listView)
        {
            var adapter = listView.Adapter;
            if (adapter == null)
            {
                return;
            }

            ViewGroup vg = listView;
            int totalHeight = 0;
            for (int i = 0; i < adapter.Count; i++)
            {
                View listItem = adapter.GetView(i, null, vg);
                listItem.Measure(0, 0);
                totalHeight += listItem.MeasuredHeight;
            }

            ViewGroup.LayoutParams par = listView.LayoutParameters;
            par.Height = totalHeight + (listView.DividerHeight * (adapter.Count - 1));
            listView.LayoutParameters = par;
            listView.RequestLayout();
        }
    }
}