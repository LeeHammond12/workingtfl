﻿using Android.Text;
using Android.Text.Method;
using Android.Text.Util;
using Android.Views;
using Android.Widget;

namespace TfL.CongestionCharge.Droid.Extensions
{
    public static class ViewExtensions
    {
        public static TextView FindTextView(this View view, int id)
        {
            return view.FindViewById<TextView>(id);
        }

        public static Button FindButton(this View view, int id)
        {
            return view.FindViewById<Button>(id);
        }

        public static void FormatTextViewTextWithHyperLink(this TextView textView, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                if (text.Contains("href") || text.Contains("<b>"))
                {
                    text = text.Replace("\r\n", "<br/>");
                    var html = Html.FromHtml(text);
                    textView.MovementMethod = LinkMovementMethod.Instance;
                    textView.TextFormatted = html;
                }
                else
                {
                    textView.MovementMethod = LinkMovementMethod.Instance;
                    textView.AutoLinkMask = MatchOptions.WebUrls;
                    textView.Text = text;
                }
            }
        }
    }
}