﻿using System;
using Android.Views;
using Android.Widget;

namespace TfL.CongestionCharge.Droid.Extensions
{
    public static class EditTextExtensions
    {
        public static void AddSearchKeyHandler(this EditText txt, Action a)
        {
            txt.KeyPress += (s, e) =>
            {
                var handled = false;
                if (e.KeyCode == Keycode.Enter)
                {
                    a?.Invoke();
                    handled = true;
                }

                e.Handled = handled;
            };
        }
    }
}