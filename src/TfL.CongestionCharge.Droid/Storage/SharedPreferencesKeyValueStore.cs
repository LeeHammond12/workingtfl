﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Newtonsoft.Json;
using Tfl.CongestionCharge.Core.Storage;

namespace Thumbmunkeys.Common.Platform.Storage.KeyValueStorage
{
    /// <summary>
    /// 4 k limit
    /// </summary>
    public class SharedPreferencesKeyValueStore : IKeyValueStore
    {
        private ISharedPreferences Store { get; }

        public SharedPreferencesKeyValueStore(string containerName)
        {
            Store = Application.Context.GetSharedPreferences(containerName, FileCreationMode.Private);
        }

        public T Restore<T>(string key)
        {
            if (!ContainsKey(key))
                throw new KeyNotFoundException(key);

            return JsonConvert.DeserializeObject<T>(Store.GetString(key, null));
        }

        public T Restore<T>(string token, T defaultValue)
        {
            return ContainsKey(token) ? Restore<T>(token) : defaultValue;
        }

        public void Backup(string token, object value)
        {
            var prefEditor = Store.Edit();
            prefEditor.PutString(token, JsonConvert.SerializeObject(value));
            prefEditor.Commit();
        }

        public bool RemoveKey(string key)
        {
            if (!ContainsKey(key))
                return false;

            var prefEditor = Store.Edit();
            prefEditor.Remove(key);
            prefEditor.Commit();
            return true;
        }

        public bool ContainsKey(string key)
        {
            return Store.Contains(key);
        }
    }
}
