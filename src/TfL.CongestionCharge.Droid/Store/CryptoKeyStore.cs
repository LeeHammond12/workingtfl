﻿using System;
using Android.Content;
using Android.Preferences;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.PCLCrypto;

namespace TfL.CongestionCharge.Droid.Store
{
    public class CryptoKeyStore : ICryptoKeyStore
    {
        private readonly ISharedPreferences _pref;
        private readonly ISharedPreferencesEditor _editor;
        private string _kv;

        public CryptoKeyStore(Context context)
        {
            _pref = PreferenceManager.GetDefaultSharedPreferences(context);
            _editor = _pref.Edit();
        }

        public void StoreKey()
        {
            _kv = _pref.GetString(Sharedkeys.PclCryptoKey, string.Empty);
            if (string.IsNullOrEmpty(_kv))
            {
                _editor.PutString(Sharedkeys.PclCryptoKey, Convert.ToBase64String(CryptoUtilities.GetAes256KeyMaterial()));
                _editor.Commit();
            }
        }

        public byte[] GetKey()
        {
            _kv = _pref.GetString(Sharedkeys.PclCryptoKey, string.Empty);

            return !string.IsNullOrEmpty(_kv) ? Convert.FromBase64String(_kv) : null;
        }
    }
}