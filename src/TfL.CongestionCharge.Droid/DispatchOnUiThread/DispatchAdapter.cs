﻿using System;
using System.Threading.Tasks;
using Android.App;
using Tfl.CongestionCharge.Core.Environment;

namespace TfL.CongestionCharge.Droid.DispatchOnUiThread
{
    public class DispatchAdapter : IDispatchOnUiThread
    {
        private readonly Activity _owner;

        public DispatchAdapter(Activity owner)
        {
            _owner = owner;
        }

        public void Invoke(Action action)
        {
            _owner.RunOnUiThread(action);
        }

        public void Invoke(Task<Action> action)
        {
            _owner.RunOnUiThread(async () => await action);
        }
    }
}