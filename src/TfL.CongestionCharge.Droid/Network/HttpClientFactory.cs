﻿using System.Net.Http;
using Tfl.CongestionCharge.Core.Environment;

namespace TfL.CongestionCharge.Droid.Network
{
    public class HttpClientFactory : IHttpClientFactory
    {
        public HttpClient GetClient(IAuthService authService)
        {
            return new HttpClient(new AndroidAuthenticatedHttpClientHandler(authService));
        }
    }
}