﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Tfl.CongestionCharge.Core;
using Tfl.CongestionCharge.Core.Api;
using Tfl.CongestionCharge.Core.Environment;
using Xamarin.Android.Net;

namespace TfL.CongestionCharge.Droid.Network
{
    public class AndroidAuthenticatedHttpClientHandler : AndroidClientHandler
    {
        private readonly IAuthService _authService;

        public AndroidAuthenticatedHttpClientHandler(IAuthService authService)
        {
            _authService = authService;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var addAuthHeader = request.RequestUri.AbsoluteUri.Contains(NetworkConstants.AuthEndpoint);

            var authState = AppEnvironment.Current.AppState.Auth;

            //var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{NetworkConstants.ApiClientId}:{NetworkConstants.ApiClientSecret}"));

            if (addAuthHeader)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", NetworkConstants.AuthHeader);
            }

            if (authState.HasAccessTokenExpired && authState.IsLoggedIn && !authState.RefreshingToken)
            {
                authState.RefreshingToken = true;

                try
                {
                    await _authService.RefreshToken().ConfigureAwait(false);
                }
                catch (Exception)
                {
                }

                authState.RefreshingToken = false;

                if (authState.HasAccessTokenExpired)
                {
                    throw new AuthenticationException();
                }
            }

            if (authState.IsLoggedIn && !authState.RefreshingToken)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", authState.AccessToken);
            }

            try
            {
                var response = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
#if DEBUG
                await NetworkUtils.PrintRequest(request, response);
#endif
                return response;
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"EXCEPTION {ex.Message}");
                throw;
            }
        }
    }
}
