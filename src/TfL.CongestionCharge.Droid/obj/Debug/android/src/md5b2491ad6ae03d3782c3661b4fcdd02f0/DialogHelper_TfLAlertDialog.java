package md5b2491ad6ae03d3782c3661b4fcdd02f0;


public class DialogHelper_TfLAlertDialog
	extends android.app.AlertDialog
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("TfL.CongestionCharge.Droid.AppCompat.DialogHelper+TfLAlertDialog, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", DialogHelper_TfLAlertDialog.class, __md_methods);
	}


	public DialogHelper_TfLAlertDialog (android.content.Context p0)
	{
		super (p0);
		if (getClass () == DialogHelper_TfLAlertDialog.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.AppCompat.DialogHelper+TfLAlertDialog, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public DialogHelper_TfLAlertDialog (android.content.Context p0, boolean p1, android.content.DialogInterface.OnCancelListener p2)
	{
		super (p0, p1, p2);
		if (getClass () == DialogHelper_TfLAlertDialog.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.AppCompat.DialogHelper+TfLAlertDialog, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Boolean, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:Android.Content.IDialogInterfaceOnCancelListener, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public DialogHelper_TfLAlertDialog (android.content.Context p0, int p1)
	{
		super (p0, p1);
		if (getClass () == DialogHelper_TfLAlertDialog.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.AppCompat.DialogHelper+TfLAlertDialog, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
