package md5a10ed596b7899a6e8438254712e7a949;


public class WhichVehicleActivity
	extends md5a10ed596b7899a6e8438254712e7a949.BaseActivity_1
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onBackPressed:()V:GetOnBackPressedHandler\n" +
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"n_onResume:()V:GetOnResumeHandler\n" +
			"";
		mono.android.Runtime.register ("TfL.CongestionCharge.Droid.Activities.WhichVehicleActivity, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", WhichVehicleActivity.class, __md_methods);
	}


	public WhichVehicleActivity ()
	{
		super ();
		if (getClass () == WhichVehicleActivity.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.Activities.WhichVehicleActivity, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onBackPressed ()
	{
		n_onBackPressed ();
	}

	private native void n_onBackPressed ();


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);


	public void onResume ()
	{
		n_onResume ();
	}

	private native void n_onResume ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
