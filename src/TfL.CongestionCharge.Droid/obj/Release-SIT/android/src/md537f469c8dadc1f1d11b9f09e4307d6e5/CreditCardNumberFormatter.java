package md537f469c8dadc1f1d11b9f09e4307d6e5;


public class CreditCardNumberFormatter
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.text.TextWatcher,
		android.text.NoCopySpan
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_afterTextChanged:(Landroid/text/Editable;)V:GetAfterTextChanged_Landroid_text_Editable_Handler:Android.Text.ITextWatcherInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"n_beforeTextChanged:(Ljava/lang/CharSequence;III)V:GetBeforeTextChanged_Ljava_lang_CharSequence_IIIHandler:Android.Text.ITextWatcherInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"n_onTextChanged:(Ljava/lang/CharSequence;III)V:GetOnTextChanged_Ljava_lang_CharSequence_IIIHandler:Android.Text.ITextWatcherInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("TfL.CongestionCharge.Droid.Utils.CreditCardNumberFormatter, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CreditCardNumberFormatter.class, __md_methods);
	}


	public CreditCardNumberFormatter ()
	{
		super ();
		if (getClass () == CreditCardNumberFormatter.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.Utils.CreditCardNumberFormatter, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public CreditCardNumberFormatter (int p0)
	{
		super ();
		if (getClass () == CreditCardNumberFormatter.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.Utils.CreditCardNumberFormatter, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0 });
	}

	public CreditCardNumberFormatter (android.widget.TextView p0)
	{
		super ();
		if (getClass () == CreditCardNumberFormatter.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.Utils.CreditCardNumberFormatter, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Widget.TextView, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}

	public CreditCardNumberFormatter (android.widget.TextView p0, float p1)
	{
		super ();
		if (getClass () == CreditCardNumberFormatter.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.Utils.CreditCardNumberFormatter, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Widget.TextView, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Single, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1 });
	}

	public CreditCardNumberFormatter (android.content.Context p0, float p1)
	{
		super ();
		if (getClass () == CreditCardNumberFormatter.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.Utils.CreditCardNumberFormatter, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Single, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1 });
	}


	public void afterTextChanged (android.text.Editable p0)
	{
		n_afterTextChanged (p0);
	}

	private native void n_afterTextChanged (android.text.Editable p0);


	public void beforeTextChanged (java.lang.CharSequence p0, int p1, int p2, int p3)
	{
		n_beforeTextChanged (p0, p1, p2, p3);
	}

	private native void n_beforeTextChanged (java.lang.CharSequence p0, int p1, int p2, int p3);


	public void onTextChanged (java.lang.CharSequence p0, int p1, int p2, int p3)
	{
		n_onTextChanged (p0, p1, p2, p3);
	}

	private native void n_onTextChanged (java.lang.CharSequence p0, int p1, int p2, int p3);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
