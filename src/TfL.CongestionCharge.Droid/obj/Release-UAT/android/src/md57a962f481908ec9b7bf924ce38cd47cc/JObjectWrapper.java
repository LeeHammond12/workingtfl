package md57a962f481908ec9b7bf924ce38cd47cc;


public class JObjectWrapper
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Thumbmunkeys.Common.Platform.CSharpCompatibility.JObjectWrapper, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", JObjectWrapper.class, __md_methods);
	}


	public JObjectWrapper ()
	{
		super ();
		if (getClass () == JObjectWrapper.class)
			mono.android.TypeManager.Activate ("Thumbmunkeys.Common.Platform.CSharpCompatibility.JObjectWrapper, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
