package md5a10ed596b7899a6e8438254712e7a949;


public class PaymentActivity
	extends md5a10ed596b7899a6e8438254712e7a949.BaseActivity_1
	implements
		mono.android.IGCUserPeer,
		android.widget.ViewSwitcher.ViewFactory,
		android.view.View.OnTouchListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"n_makeView:()Landroid/view/View;:GetMakeViewHandler:Android.Widget.ViewSwitcher/IViewFactoryInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"n_onTouch:(Landroid/view/View;Landroid/view/MotionEvent;)Z:GetOnTouch_Landroid_view_View_Landroid_view_MotionEvent_Handler:Android.Views.View/IOnTouchListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("TfL.CongestionCharge.Droid.Activities.PaymentActivity, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", PaymentActivity.class, __md_methods);
	}


	public PaymentActivity ()
	{
		super ();
		if (getClass () == PaymentActivity.class)
			mono.android.TypeManager.Activate ("TfL.CongestionCharge.Droid.Activities.PaymentActivity, TfL.CongestionCharge.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);


	public android.view.View makeView ()
	{
		return n_makeView ();
	}

	private native android.view.View n_makeView ();


	public boolean onTouch (android.view.View p0, android.view.MotionEvent p1)
	{
		return n_onTouch (p0, p1);
	}

	private native boolean n_onTouch (android.view.View p0, android.view.MotionEvent p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
