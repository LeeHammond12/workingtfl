﻿using System;
using System.Diagnostics;
using Android.App;
using Android.Content;
using Android.Runtime;
using Microsoft.Azure.Mobile;
using Microsoft.Azure.Mobile.Analytics;
using Microsoft.Azure.Mobile.Crashes;
using PCLStorage;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Serialization.Implementation;
using Tfl.CongestionCharge.Core.Storage;
using TfL.CongestionCharge.Droid.Environment;
using TfL.CongestionCharge.Droid.Network;
using TfL.CongestionCharge.Droid.Store;
using Version.Plugin;

namespace TfL.CongestionCharge.Droid
{
    [Application(Icon = "@drawable/icon")]
    public class App : Application
    {
        private static LifecycleCallbacks LifecycleCallbacks;

        public App(IntPtr h, JniHandleOwnership jho)
            : base(h, jho)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            MobileCenter.Start(Sharedkeys.AnalyticsAppKeys.AndroidApplicationKey, typeof(Analytics), typeof(Crashes));

            LifecycleCallbacks = new LifecycleCallbacks();
            RegisterActivityLifecycleCallbacks(LifecycleCallbacks);

            var cryptoKeyStore = new CryptoKeyStore(this);
            cryptoKeyStore.StoreKey();

            var nav = new AppNavigationService();

            var encryptedKeyValueStore
                = new BinarySerializedFileStore(
                     new EncryptedBinarySerializer(cryptoKeyStore),
                        FileSystem.Current.LocalStorage);

            AppEnvironment.Init(
                new VersionImplementation().Version,
                nav,
                new UserDialog(),
                new HttpClientFactory(),
                encryptedKeyValueStore);
        }

        public override void OnTrimMemory(TrimMemory level)
        {
            LifecycleCallbacks.TrimMemoryCalled();
            base.OnTrimMemory(level);
        }
    }
}