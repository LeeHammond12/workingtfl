﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.Activities
{
    public abstract class BrowserActivity<T> : BaseActivity<T> where T : TflViewModelBase, IBrowserViewModel
    {
        private WebViewDataLoading _webViewDataLoading;

        private ProgressBar _progressBarForWebView;
        private LinearLayout _privacyLinkLayout;
        private WebView _privacyWebView;
        private Button _doneButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.PrivacyDetailsBrowser);
            SetupToolbar(Strings.Privacy);
            _doneButton = FindButton(Resource.Id.btnDone);
            _progressBarForWebView = FindViewById<ProgressBar>(Resource.Id.progressBarWebViewLoading);
            _privacyLinkLayout = FindViewById<LinearLayout>(Resource.Id.relativeLayoutPrivacyWebView);
            _privacyWebView = FindViewById<WebView>(Resource.Id.webViewForPrivacyDetails);

            ShowDetailsInWebView();

            _doneButton.SetClickCommand(ViewModel.GoBackCommand);
        }

        private void ShowDetailsInWebView()
        {
            _privacyWebView.Settings.JavaScriptEnabled = true;
            if (_webViewDataLoading == null)
            {
                _webViewDataLoading = new WebViewDataLoading();
            }

            _webViewDataLoading.ShowProgressBar += ShowWebViewProgrssBar;

            _webViewDataLoading.HideProgressBar += HideWebViewProgrssBar;

            _privacyWebView.SetWebViewClient(_webViewDataLoading);
            _privacyWebView.LoadUrl(ViewModel.NavigationUri);
        }

        private void ShowWebViewProgrssBar(object sender, EventArgs args)
        {
            _progressBarForWebView.Visibility = ViewStates.Visible;
            _webViewDataLoading.ShowProgressBar -= ShowWebViewProgrssBar;
        }

        private void HideWebViewProgrssBar(object sender, EventArgs args)
        {
            _progressBarForWebView.Visibility = ViewStates.Gone;
            _privacyLinkLayout.Visibility = ViewStates.Visible;
            _webViewDataLoading.HideProgressBar -= HideWebViewProgrssBar;
        }
    }
}