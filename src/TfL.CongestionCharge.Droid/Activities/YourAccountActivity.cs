﻿using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class YourAccountActivity : BaseActivity<YourAccountViewModel>
    {
        private TextView _nameTextView, _accountNumberTextView, _addressTextView;
        private CardView _vehicleCardView;
        private Button _buttonPrivacy, _buttonTc;

        public TextView NameTextView => _nameTextView ?? (_nameTextView = FindTextView(Resource.Id.Name));
        public TextView AccountNumberTextView => _accountNumberTextView ?? (_accountNumberTextView = FindTextView(Resource.Id.AccountNumber));
        public TextView AddressTextView => _addressTextView ?? (_addressTextView = FindTextView(Resource.Id.Address));
        public CardView VehiclesAndServicesButton => _vehicleCardView ?? (_vehicleCardView = FindViewById<CardView>(Resource.Id.vehiclesAndServicesCardView));
        public Button PrivacyButton => _buttonPrivacy ?? (_buttonPrivacy = FindButton(Resource.Id.buttonPrivacy));
        public Button TcButton => _buttonTc ?? (_buttonTc = FindButton(Resource.Id.buttonTC));

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.YourAccount);
            SetupToolbar(Strings.YourAccount);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            PrivacyButton.SetClickCommand(ViewModel.NavigateToPrivacyCommand);
            TcButton.SetClickCommand(ViewModel.NavigateToTcCommand);
            VehiclesAndServicesButton.SetClickCommand(ViewModel.NavigateToVehiclesAndServicesCommand);

            var user = ViewModel.LoggedInUser;

            NameTextView.Text = user.Name;
            AccountNumberTextView.Text = user.AccountNumber;
            AddressTextView.Text = user.Address;
        }
    }
}