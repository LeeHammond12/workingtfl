﻿using System;
using Android.Graphics;
using Android.Webkit;

namespace TfL.CongestionCharge.Droid.Activities
{
    public class WebViewDataLoading : WebViewClient
    {
        public event EventHandler ShowProgressBar;

        public event EventHandler HideProgressBar;

        public override void OnPageStarted(WebView view, string url, Bitmap favicon)
        {
            base.OnPageStarted(view, url, favicon);
            ShowProgressBar?.Invoke(null, null);
        }

        public override void OnPageFinished(WebView view, string url)
        {
            base.OnPageFinished(view, url);
            HideProgressBar?.Invoke(null, null);
        }
    }
}