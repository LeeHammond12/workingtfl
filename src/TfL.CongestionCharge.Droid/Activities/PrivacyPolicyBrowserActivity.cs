﻿using Android.App;
using Android.Content.PM;
using Tfl.CongestionCharge.Core.ViewModel;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class PrivacyPolicyBrowserActivity : BrowserActivity<PrivacyPolicyBrowswerViewModel>
    {
    }
}