﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.ViewModel.Main;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class AllDoneActivity : BaseActivity<AllDoneViewModel>
    {
        private Button _btnSaveReceipt;
        private Button _btnHome;

        public Button BtnSaveReceipt => _btnSaveReceipt ?? (_btnSaveReceipt = FindButton(Resource.Id.btnSaveReceipt));

        public Button BtnHome => _btnHome ?? (_btnHome = FindButton(Resource.Id.btnHome));

        public override void OnBackPressed()
        {
            NavigationService.NavigateHomeAndClearBackStack();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            if (ViewModel.Payment == null)
            {
                NavigationService.NavigateHomeAndClearBackStack();
                return;
            }

            SetContentView(Resource.Layout.AllDone);
            SetupToolbar(ViewModel.PageTitle);
            HideToolbarBackButton();

            SetupView();
        }

        private void SetupView()
        {
            var pcnVisible = ViewModel.IsPcn ? ViewStates.Visible : ViewStates.Gone;

            FindViewById(Resource.Id.pcnNumberTableRow).Visibility = pcnVisible;
            FindTextView(Resource.Id.txtPcnNumber).Text = ViewModel.PcnNumber;

            FindViewById(Resource.Id.receiptNoTableRow).Visibility = pcnVisible;
            FindTextView(Resource.Id.txtChargeType).Text = ViewModel.ChargeType;
            FindTextView(Resource.Id.txtPrice).Text = ViewModel.Price.ToCurrencyString();
            FindTextView(Resource.Id.txtPaymentDate).Text = ViewModel.PaymentDate;
            FindTextView(Resource.Id.txtReceiptNumber).Text = ViewModel.Receipt;
            FindTextView(Resource.Id.txtReceiptNo).Text = ViewModel.Receipt;

            BtnSaveReceipt.Visibility = ViewModel.IsSaveReceiptButtonVisible ? ViewStates.Visible : ViewStates.Gone;

            BtnSaveReceipt.SetClickCommand(ViewModel.SaveReceiptAndReturnHome);
            BtnHome.SetClickCommand(ViewModel.ReturnHome);
        }
    }
}