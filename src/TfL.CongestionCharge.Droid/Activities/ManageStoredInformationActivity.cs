﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.AnonymousAccount;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class ManageStoredInformationActivity : BaseActivity<AnonymousAccountViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.YourVehiclesAndCards);
            SetupToolbar(Strings.YourVehiclesAndCards);

            var vehicleCardView = FindViewById<CardView>(Resource.Id.vehiclesCardView);
            var yourCreditCardsCardView = FindViewById<CardView>(Resource.Id.yourCreditCardsCardView);
            var buttonPrivacy = FindButton(Resource.Id.buttonPrivacy);
            var buttonTc = FindButton(Resource.Id.buttonTC);

            buttonPrivacy.SetClickCommand(ViewModel.NavigateToPrivacyCommand);
            buttonTc.SetClickCommand(ViewModel.NavigateToTcCommand);
            vehicleCardView.SetClickCommand(ViewModel.NavigateToVehiclesCommand);
            yourCreditCardsCardView.SetClickCommand(ViewModel.NavigateToYourPaymentMethodsCommand);
        }
    }
}