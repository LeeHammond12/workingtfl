﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.AnonymousAccount;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.TableHandlers;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class AnonymousUserYourVehiclesActivity : BaseActivity<AnonymousAccountViewModel>
    {
        private ListView _listYourVehicles;
        private VehiclesListAdapter _adapter;

        public ListView YourVehiclesList
            => _listYourVehicles ?? (_listYourVehicles = FindViewById<ListView>(Resource.Id.listYourVehicles));

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AnonymousUserYourVehicles);
            SetupToolbar(Strings.ManageStoredVehicles);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            YourVehiclesList.Adapter = _adapter = new VehiclesListAdapter(LayoutInflater,
                Resource.Layout.vehicle_item_view_with_delete_button, RemoveVehicle, () => ViewModel.UserInformation.Vehicles);
            YourVehiclesList.JustifyListViewHeightBasedOnChildren();

            SetTextFieldText();
        }

        private async void RemoveVehicle(VehicleViewModel v)
        {
            await ViewModel.RemoveVehicle(v);
            _adapter.NotifyDataSetChanged();
            YourVehiclesList.JustifyListViewHeightBasedOnChildren();
            SetTextFieldText();
        }

        private void SetTextFieldText()
        {
            if (ViewModel.UserInformation.Vehicles.Count == 0)
            {
                FindTextView(Resource.Id.yourVehiclesText).Text = Strings.NoVehiclesOnAnonAccount;
            }
        }
    }
}