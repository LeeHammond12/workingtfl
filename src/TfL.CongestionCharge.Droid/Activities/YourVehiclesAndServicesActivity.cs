﻿using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.TableHandlers;
using Tfl.CongestionCharge.Core.ViewModel;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class YourVehiclesAndServicesActivity : BaseActivity<LoggedInAccountViewModel>
    {
        private ExpandableListView _listYourActiveVehiclesAndServices;
        private ListView _listYourInactiveVehiclesAndServices;
        private VehicleAndServicesExpandableListViewAdapter _listActiveVehiclesAdapter;
        private VehiclesListAdapter _listInActiveVehiclesAdapter;

        public ExpandableListView YourActiveVehiclesAndServices => _listYourActiveVehiclesAndServices ?? (_listYourActiveVehiclesAndServices = FindViewById<ExpandableListView>(Resource.Id.listYourActiveVehiclesAndServices));
        public ListView YourInactiveVehiclesAndServices => _listYourInactiveVehiclesAndServices ?? (_listYourInactiveVehiclesAndServices = FindViewById<ListView>(Resource.Id.listYourInactiveVehiclesAndServices));

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.YourVehiclesAndServices);
            SetupToolbar(Strings.YourVehiclesAndServices);
            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            await ViewModel.InitViewModel();
            
            _listInActiveVehiclesAdapter = new VehiclesListAdapter(LayoutInflater, Resource.Layout.vehicle_item_view, null, () => ViewModel.LoggedInUser.VehiclesWithoutServices);
            YourInactiveVehiclesAndServices.Adapter = _listInActiveVehiclesAdapter;
            YourInactiveVehiclesAndServices.JustifyListViewHeightBasedOnChildren();
            
            _listActiveVehiclesAdapter = new VehicleAndServicesExpandableListViewAdapter(this, ViewModel.LoggedInUser, (v) => Delete(v));
            YourActiveVehiclesAndServices.SetAdapter(_listActiveVehiclesAdapter);
            YourActiveVehiclesAndServices.JustifyListViewHeightBasedOnChildren();

            FindTextView(Resource.Id.YouHaveNoVehiclesActiveText).Visibility = ViewModel.LoggedInUser.VehiclesWithServices?.Any() == true ? ViewStates.Gone : ViewStates.Visible;
        }

        private async Task Delete(VehicleViewModel v)
        {
            await ViewModel.RemoveVehicle(v.Vrm);
            _listActiveVehiclesAdapter.NotifyDataSetChanged();
            YourActiveVehiclesAndServices.JustifyListViewHeightBasedOnChildren();
        }
    }
}