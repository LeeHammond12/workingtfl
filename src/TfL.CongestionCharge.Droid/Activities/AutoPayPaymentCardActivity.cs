﻿using System.Collections.Generic;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.Droid.TableHandlers;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class AutoPayPaymentCardActivity : BaseActivity<AutoPayPaymentCardViewModel>
    {
        private ListView _paymentCards;

        public List<PaymentCardViewHolder> SavedCardRadioButtons { get; set; } = new List<PaymentCardViewHolder>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AutoPayCreditCard);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            SetupToolbar(ViewModel.ToolbarText);

            if (ViewModel.ShowDirectDebitScreen)
            {
                FindViewById(Resource.Id.directDebitLayout).Visibility = ViewStates.Visible;
            }
            else
            {
                FindViewById(Resource.Id.hasPaymentCardsLayout).Visibility = ViewStates.Visible;

                _paymentCards = FindViewById<ListView>(Resource.Id.creditCardList);
                _paymentCards.Adapter = new PaymentCardAdapterWithCheckBoxes(
                    LayoutInflater,
                    ViewModel.PaymentCards,
                    (c) => ViewModel.SetAutoPayPaymentCard(c));
            }
        }
    }
}