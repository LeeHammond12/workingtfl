﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Services;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class ManageCcAutoPayActivity : BaseActivity<ManageCcAutoPayViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ManageAutoPay);
            SetupToolbar(Strings.ManageCCAutoPay);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            var user = ViewModel.LoggedInUser;

            if (user.HasAutoPay)
            {
                var enabledLayout = FindViewById<View>(Resource.Id.autoPayEnabledContainer);
                enabledLayout.Visibility = ViewStates.Visible;

                var cardButton = FindButton(Resource.Id.autoPayCreditCardButton);
                cardButton.SetClickCommand(ViewModel.NavigateToAutoPayPaymentCardCommand);

                var vehiclesButton = FindButton(Resource.Id.autoPayVehiclesButton);
                vehiclesButton.SetClickCommand(ViewModel.NavigateToAutoPayVehiclesCommand);

                UpdateStatusTextAndIcon(user, FindViewById(Resource.Id.linearLayoutStatus));

                var balanceText = FindTextView(Resource.Id.balanceText);
                var nextStatementDateText = FindTextView(Resource.Id.nextStatementDateText);
                var lastStatementBalanceText = FindTextView(Resource.Id.lastStatementBalanceText);

                balanceText.Text = user.Balance;
                nextStatementDateText.Text = user.AutoPayStatementDate.ToShortDateString();

                if (user.LastAutoPayStatementDate.HasValue)
                {
                    lastStatementBalanceText.Text = user.LastAutoPayStatementDate.Value.ToShortDateString();
                }
                else
                {
                    FindViewById(Resource.Id.lastStatementBalanceLayout).Visibility = ViewStates.Gone;
                }
            }
            else
            {
                var disabledLayout = FindViewById<View>(Resource.Id.autoPayDisabledContainer);
                disabledLayout.Visibility = ViewStates.Visible;
            }
        }

        private void UpdateStatusTextAndIcon(ILoggedInUserInformationService user, View view)
        {
            var autoPayStatusText = view.FindTextView(Resource.Id.statusText);
            var tickImage = view.FindViewById<ImageView>(Resource.Id.tickImage);

            view.Background = Resources.GetDrawable(ViewModel.AutoPayStatus == AutoPayStatus.Active
                ? Resource.Drawable.oval
                : Resource.Drawable.oval_suspended_or_closed);

            tickImage.SetImageResource(GetAutoPayStatusImage(ViewModel.AutoPayStatus));
            autoPayStatusText.Text = ViewModel.AutoPayStatusDisplayText;
        }

        private int GetAutoPayStatusImage(AutoPayStatus autoPayStatus)
        {
            switch (autoPayStatus)
            {
                case AutoPayStatus.Active:
                    return Resource.Drawable.confirmpayment;

                case AutoPayStatus.Pending:
                    return Resource.Drawable.pending;

                case AutoPayStatus.Closed:
                    return Resource.Drawable.closed;

                case AutoPayStatus.PendingSuspension:
                    return Resource.Drawable.pending;

                case AutoPayStatus.Suspended:
                    return Resource.Drawable.inactive;

                default:
                    return Resource.Drawable.closed;
            }
        }
    }
}