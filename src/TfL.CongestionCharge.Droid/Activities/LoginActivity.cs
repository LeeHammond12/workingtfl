﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.Main;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class LoginActivity : BaseActivity<LoginViewModel>, View.IOnTouchListener
    {
        private EditText _editTextCustomerId, _editTextPassword;
        private Button _buttonLogin;

        public EditText EditTextCustomerId => _editTextCustomerId ??
                                              (_editTextCustomerId =
                                                  FindViewById<EditText>(Resource.Id.editTextCustomerId));

        public EditText EditTextPassword => _editTextPassword ??
                                            (_editTextPassword = FindViewById<EditText>(Resource.Id.editTextPassword));

        public Button ButtonLogin => _buttonLogin ?? (_buttonLogin = FindButton(Resource.Id.buttonLogin));

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Login);

            SetupToolbar(Strings.LoginToYourAccount);
            var noLondonAccountText = FindViewById<TextView>(Resource.Id.NoLondonAccountText);
            noLondonAccountText.FormatTextViewTextWithHyperLink(Strings.NoLondonAccount);
            PerformBindings();
        }

        public override void OnBackPressed()
        {
            ViewModel.OnGoBack();
        }

        private void PerformBindings()
        {
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.AccountNumber, () => EditTextCustomerId.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.Password, () => EditTextPassword.Text));

            ButtonLogin.Click += (sender, args) =>
            {
                KeyBoardHelper.HideKeyboard(this, Window);
                ViewModel.LoginCommand.Execute(null);
            };

            EditTextCustomerId.SetOnTouchListener(this);
            EditTextPassword.SetOnTouchListener(this);
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            var editText = v as EditText;
            if (editText == null)
                return false;

            var drawableRight = 2;
            if (e.Action == MotionEventActions.Up)
            {
                if (e.RawX >= editText.Right - editText.GetCompoundDrawables()[drawableRight].Bounds.Width())
                {
                    if (editText == EditTextCustomerId)
                    {
                        AppEnvironment.Current.DialogService.ShowMessage(Strings.CustomerIdHelpText, Strings.Information);
                    }
                    if (editText == EditTextPassword)
                    {
                        AppEnvironment.Current.DialogService.ShowMessage(Strings.PasswordHelpText, Strings.Information);
                    }
                    return true;
                }
            }
            return false;
        }
    }
}