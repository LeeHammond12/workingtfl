﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayPcn;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class PcnLookupActivity : BaseActivity<PcnLookupViewModel>
    {
        private EditText _pcnNumberEditText;
        private EditText _vrmEditText;
        private Button _btnControl;
        private TextView _amountYouOweTextView;
        private LinearLayout _amountYouOweLabelLayout;

        public EditText PcnNumberEditText => _pcnNumberEditText ?? (_pcnNumberEditText = FindViewById<EditText>(Resource.Id.pcnNumberEditText));
        public EditText VrmEditText => _vrmEditText ?? (_vrmEditText = FindViewById<EditText>(Resource.Id.vrmEditText));
        public Button BtnControl => _btnControl ?? (_btnControl = FindButton(Resource.Id.btnControl));
        public TextView AmountYouOweTextView => _amountYouOweTextView ?? (_amountYouOweTextView = FindTextView(Resource.Id.amountYouOweLabelTextView));
        public LinearLayout AmountYouOweLabelLayout => _amountYouOweLabelLayout ?? (_amountYouOweLabelLayout = FindViewById<LinearLayout>(Resource.Id.amountYouOweLabelLayout));

        public override void OnBackPressed()
        {
            ViewModel.OnBackNavigationRequested();
            SetBackNavigationTransition();
        }

        protected override void OnResume()
        {
            base.OnResume();
            ViewModel.Reset();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.PCNLookup);
            SetupToolbar(ViewModel.PageTitle);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            PcnNumberEditText.SetBook();
            VrmEditText.SetBook();

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.PcnNumber, () => PcnNumberEditText.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.VrmNumber, () => VrmEditText.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.ButtonText, () => BtnControl.Text));

            BtnControl.Click += (sender, args) =>
            {
                KeyBoardHelper.HideKeyboard(this, Window);
                ViewModel.LookupPcnOrGoToPaymentCommand.Execute(null);
            };

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.CanLookupPcnOrGoToPayment)
                .WhenSourceChanges(() =>
                {
                    BtnControl.Enabled = ViewModel.CanLookupPcnOrGoToPayment;
                }));

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.AmountYouOweString, () => AmountYouOweTextView.Text));

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.CanProceedToPayment)
                .WhenSourceChanges(() =>
                {
                    var amountVisibility = !ViewModel.CanProceedToPayment ? ViewStates.Gone : ViewStates.Visible;

                    AmountYouOweLabelLayout.Visibility = amountVisibility;

                    if (amountVisibility == ViewStates.Visible)
                    {
                        DismissKeyBoard();
                    }
                }));
        }
    }
}