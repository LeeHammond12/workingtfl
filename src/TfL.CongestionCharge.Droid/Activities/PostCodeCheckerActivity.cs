﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.Activities
{
    /// <summary>
    /// The post code checker activity.
    /// </summary>
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class PostCodeCheckerActivity : BaseActivity<PostCodeCheckerViewModel>
    {
        private Button _postCodeLookupButton;
        private EditText _editTextPostCode;
        private EditText _editTextBuildingNumber, _editTextBuildingName;

        public Button PostCodeLookupButton => _postCodeLookupButton ?? (_postCodeLookupButton = FindButton(Resource.Id.buttonLookupPostCode));
        public EditText EditTextPostCode => _editTextPostCode ?? (_editTextPostCode = FindViewById<EditText>(Resource.Id.editTextPostCode));
        public EditText EditTextBuildingNumber => _editTextBuildingNumber ?? (_editTextBuildingNumber = FindViewById<EditText>(Resource.Id.txtBuildingNumber));
        public EditText EditTextBuildingName => _editTextBuildingName ?? (_editTextBuildingName = FindViewById<EditText>(Resource.Id.txtBuildingName));

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.PostCodeChecker);
            SetupToolbar(Strings.CheckAPostCode);

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.PostCode, () => EditTextPostCode.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.BuildingNumber, () => EditTextBuildingNumber.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.BuildingName, () => EditTextBuildingName.Text));

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.CanEnterBuildingName)
                            .WhenSourceChanges(HandleEnabled));

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.CanEnterBuildingNumber)
                            .WhenSourceChanges(HandleEnabled));

            PostCodeLookupButton.Click += (sender, args) =>
            {
                KeyBoardHelper.HideKeyboard(this, Window);
                ViewModel.LookupPostCodeCommand.Execute(null);
            };

            EditTextBuildingNumber.SetBook();
            EditTextPostCode.SetBook();
            EditTextBuildingName.SetBook();
        }

        private void HandleEnabled()
        {
            EditTextBuildingName.Enabled = ViewModel.CanEnterBuildingName;
            EditTextBuildingNumber.Enabled = ViewModel.CanEnterBuildingNumber;
        }
    }
}