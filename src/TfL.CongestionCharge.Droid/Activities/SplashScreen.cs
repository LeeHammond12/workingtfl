﻿using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Java.Lang;
using Tfl.CongestionCharge.Core.Environment;
using TfL.CongestionCharge.Droid.Utils;
using Thread = System.Threading.Thread;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = true, Theme = "@style/Theme.Splash", ScreenOrientation = ScreenOrientation.Portrait, NoHistory = true, Icon = "@drawable/Icon")]
    public class SplashScreen : Activity
    {
        private const int SplashDurationMs = 2500;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.SplashScreen);
            ApplicationInit();
        }

        private void ApplicationInit()
        {
            var startInit = JavaSystem.CurrentTimeMillis();

            // Preload fonts
            Fonts.Precache("njfont_book");
            Fonts.Precache("njfont_bookbold");
            Fonts.Precache("njfont_medium");

            ThreadPool.QueueUserWorkItem(o =>
            {
                var initDuration = (int)(JavaSystem.CurrentTimeMillis() - startInit);

                var sleepDuration = Math.Max(SplashDurationMs - initDuration, 0);
                if (sleepDuration > 0)
                {
                    Thread.Sleep(sleepDuration);
                }

                var acceptedTAndCs = AppEnvironment.Current.AppState.UserHasAcceptedTermsAndConditions;
                RunOnUiThread(() => StartActivity(acceptedTAndCs ? typeof(MainActivity) : typeof(TermsAndConditionsActivity)));
            });
        }
    }
}