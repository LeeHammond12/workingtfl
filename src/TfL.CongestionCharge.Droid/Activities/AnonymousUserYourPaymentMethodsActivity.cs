﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.AnonymousAccount;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.TableHandlers;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class AnonymousUserYourPaymentMethodsActivity : BaseActivity<AnonymousAccountViewModel>
    {
        private ListView _listYourPayments;
        private PaymentCardAdapter _adapter;

        public ListView YourPaymentCards => _listYourPayments ?? (_listYourPayments = FindViewById<ListView>(Resource.Id.listYourPayments));

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AnonymousUserYourPaymentMethods);
            SetupToolbar(Strings.ManageStoredCreditCards);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            PopulateCards();

            SetTextFieldText();
        }

        private void PopulateCards()
        {
            YourPaymentCards.Adapter = _adapter = new PaymentCardAdapter(
                LayoutInflater,
                Resource.Layout.paymentCardWithDeleteButton,
                RemoveCard,
                () => ViewModel.UserInformation.PaymentCards);

            YourPaymentCards.JustifyListViewHeightBasedOnChildren();
        }

        private async void RemoveCard(PaymentCardViewModel card)
        {
            await ViewModel.RemovePaymentCard(card);
            _adapter.NotifyDataSetChanged();
            YourPaymentCards.JustifyListViewHeightBasedOnChildren();

            SetTextFieldText();
        }

        private void SetTextFieldText()
        {
            if (ViewModel.UserInformation.PaymentCards.Count == 0)
            {
                FindTextView(Resource.Id.yourPaymentCardsText).Text = Strings.NoPaymentCardsOnAnonAccount;
            }
        }
    }
}