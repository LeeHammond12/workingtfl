﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Payments;
using TfL.CongestionCharge.Droid.AppCompat;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class PaymentActivity : BaseActivity<PaymentViewModel>, ViewSwitcher.IViewFactory, View.IOnTouchListener
    {
        private EditText _editCardNumber;
        private EditText _editCardExpiry;
        private EditText _editCardCvv2;
        private EditText _editCardName;
        private EditText _editNumber;
        private ListView _listView;
        private CheckBox _chkSavePaymentDetails;
        private CheckBox _chkSendReceiptViaSms;
        private Button _btnConfirmAndPay;
        private ImageSwitcher _cardTypeImage;

        public EditText EditCardName => _editCardName ?? (_editCardName = FindViewById<EditText>(Resource.Id.editName));
        public EditText EditNumber => _editNumber ?? (_editNumber = FindViewById<EditText>(Resource.Id.editNumber));
        public EditText EditCardNumber => _editCardNumber ?? (_editCardNumber = FindViewById<EditText>(Resource.Id.editCardNumber));
        public EditText EditCardExpiry => _editCardExpiry ?? (_editCardExpiry = FindViewById<EditText>(Resource.Id.editCardExpire));
        public EditText EditCardCvv2 => _editCardCvv2 ?? (_editCardCvv2 = FindViewById<EditText>(Resource.Id.editCardCVV2));
        public ListView ListView => _listView ?? (_listView = FindViewById<ListView>(Resource.Id.listCards));
        public CheckBox ChkSavePaymentDetails => _chkSavePaymentDetails ?? (_chkSavePaymentDetails = FindViewById<CheckBox>(Resource.Id.chkSaveDetails));
        public CheckBox ChkSendReceiptViaSms => _chkSendReceiptViaSms ?? (_chkSendReceiptViaSms = FindViewById<CheckBox>(Resource.Id.chkReceiptViaSMS));
        public Button BtnConfirmAndPay => _btnConfirmAndPay ?? (_btnConfirmAndPay = FindButton(Resource.Id.btnConfirmAndPay));

        // IViewFactory
        public View MakeView()
        {
            ImageView imageView = new ImageView(this);
            imageView.SetScaleType(ImageView.ScaleType.FitCenter);
            imageView.LayoutParameters =
                new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MatchParent,
                    ViewGroup.LayoutParams.WrapContent);
            return imageView;
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            var editText = v as EditText;
            if (editText == null)
                return false;

            var drawableRight = 2;
            if (e.Action == MotionEventActions.Up)
            {
                if (e.GetX() >= editText.Width - editText.GetCompoundDrawables()[drawableRight].Bounds.Width())
                {
                    if (editText == EditCardCvv2)
                    {
                        KeyBoardHelper.HideKeyboard(this, Window);
                        DialogHelper.ShowHintMessage(this, Resource.Layout.help_popup);
                    }
                    return true;
                }
            }
            return false;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.PaymentDetails);

            ProgressDialog.SetTitle(Strings.PleaseWaitForPaymentTitle);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            var smsVisibility = ViewModel.IsPcn ? ViewStates.Gone : ViewStates.Visible;
            FindViewById(Resource.Id.sendReceiptViaTextLayout).Visibility = smsVisibility;

            ChkSavePaymentDetails.Visibility = ViewModel.CanSaveDetails ? ViewStates.Visible : ViewStates.Gone;

            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            SetupToolbar(ViewModel.PageTitle);
            PerformDataBind();

            EditNumber.SetBookLight();
            ChkSendReceiptViaSms.SetBook();
            ChkSavePaymentDetails.SetBook(true);
            _chkSavePaymentDetails.SetBook();
            _editCardCvv2.SetBookLight();
            EditCardName.SetBookLight();
            EditCardNumber.SetBookLight();
            _editCardExpiry.SetBookLight();
        }

        private void PerformDataBind()
        {
            _cardTypeImage = FindViewById<ImageSwitcher>(Resource.Id.cardTypeImage);
            _cardTypeImage.SetFactory(this);
            _cardTypeImage.SetInAnimation(this, Resource.Animation.abc_fade_in);
            _cardTypeImage.SetOutAnimation(this, Resource.Animation.abc_fade_out);

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.SmsNumber, () => EditNumber.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.NameOnCard, () => EditCardName.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.CardNumber, () => EditCardNumber.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.ExpiryDate, () => EditCardExpiry.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.Cvv2, () => EditCardCvv2.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.SaveDetails, () => ChkSavePaymentDetails.Checked));

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.Issuer)
                .WhenSourceChanges(() =>
                        _cardTypeImage?.SetImageResource(PaymentCardUtils.GetImageResourceFromCardName(ViewModel.Issuer))));

            Bindings.Add(this.SetBinding(() => ChkSendReceiptViaSms.Checked)
               .WhenSourceChanges(() =>
               {
                   ViewModel.IsSmsTextVisible = ChkSendReceiptViaSms.Checked;
                   EditNumber.Visibility = ViewModel.IsSmsTextVisible && !ViewModel.IsPcn ? ViewStates.Visible : ViewStates.Gone;
               }));

            BtnConfirmAndPay.Click += (sender, args) =>
            {
                KeyBoardHelper.HideKeyboard(this, Window);
                ViewModel.MakePaymentCommand.Execute(null);
            };
            BtnConfirmAndPay.Text = ViewModel.PaymentButtonText;

            var numberFormatter = new CreditCardNumberFormatter(EditCardNumber);
            EditCardNumber.AddTextChangedListener(numberFormatter);

            var dateFormatter = new CreditCardDateFormatter(EditCardExpiry);
            EditCardExpiry.AddTextChangedListener(dateFormatter);

            EditCardCvv2.SetOnTouchListener(this);
        }
    }
}