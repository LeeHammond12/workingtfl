﻿using System.Collections.Generic;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class ConfirmVrmActivity : BaseActivity<ConfirmVrmViewModel>
    {
        private CheckBox _chkSaveVehicle, _confirmVrmIsCorrectCheckBox;

        private Button _nextButton;

        public CheckBox SaveVehicleCheckBox => _chkSaveVehicle ?? (_chkSaveVehicle = FindViewById<CheckBox>(Resource.Id.chkSaveVehicle));

        public CheckBox ConfirmVrmIsCorrectCheckBox => _confirmVrmIsCorrectCheckBox ?? (_confirmVrmIsCorrectCheckBox = FindViewById<CheckBox>(Resource.Id.checkBoxConfirmVrmIsCorrect));
        public Button NextButton => _nextButton ?? (_nextButton = FindButton(Resource.Id.nextButton));
        public List<RadioButton> SavedVehicleRadioButtons { get; set; } = new List<RadioButton>();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.ConfirmVrm);

            SetupToolbar(ViewModel.PageTitle);

            if (ViewModel.HasFlowState)
                PerformDataBind();
        }

        protected override void OnResume()
        {
            base.OnResume();
            ViewModel?.GoBackIfNoFlowState();
        }

        private async void PerformDataBind()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
            {
                return;
            }

            SaveVehicleCheckBox.SetBook();
            ConfirmVrmIsCorrectCheckBox.SetBook(true);

            SaveVehicleCheckBox.Visibility = ViewModel.ShowSaveCheckBox ? ViewStates.Visible : ViewStates.Gone;

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.ConfirmVrmIsCorrect, () => ConfirmVrmIsCorrectCheckBox.Checked));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.SaveVehicle, () => SaveVehicleCheckBox.Checked));

            NextButton.Text = ViewModel.NextButtonText;

            NextButton.Click += (sender, args) =>
            {
                KeyBoardHelper.HideKeyboard(this, Window);
                ViewModel.NextCommand.Execute(null);
            };

            FindTextView(Resource.Id.vehicleRegistrationMark).Text = ViewModel.Vehicle.Vrm;
            FindTextView(Resource.Id.carMakeText).Text = ViewModel.VehicleDetails;
        }
    }
}