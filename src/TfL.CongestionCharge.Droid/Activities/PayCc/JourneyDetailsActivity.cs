﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using GalaSoft.MvvmLight.Helpers;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.TableHandlers;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class JourneyDetailsActivity : BaseActivity<JourneyDetailsViewModel>
    {
        private TextView _txtJourneyDetails;
        private TextView _txtJourneyDetailsInfo;
        private ExpandableListView _listJourneyDetails;
        private Button _btnAddAnotherVehicle;
        private Button _btnConfirmDetailsAndPay;
        private ScrollView _scrollView;

        public TextView JourneyDetails => _txtJourneyDetails ?? (_txtJourneyDetails = FindTextView(Resource.Id.txtJourneyDetails));
        public TextView JourneyDetailsInfo => _txtJourneyDetailsInfo ?? (_txtJourneyDetailsInfo = FindTextView(Resource.Id.txtJourneyDetailsInfo));
        public ExpandableListView JourneyDetailsList => _listJourneyDetails ?? (_listJourneyDetails = FindViewById<ExpandableListView>(Resource.Id.listJourneyDetails));
        public Button AddAnotherVehicle => _btnAddAnotherVehicle ?? (_btnAddAnotherVehicle = FindButton(Resource.Id.btnAddAnotherVehicle));
        public Button ConfirmDetailsAndPay => _btnConfirmDetailsAndPay ?? (_btnConfirmDetailsAndPay = FindButton(Resource.Id.btnConfirmDetailsAndPay));
        public ScrollView ScrollView => _scrollView ?? (_scrollView = FindViewById<ScrollView>(Resource.Id.scrollView));

        private JourneyDetailsExpandableListViewAdapter _listAdapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.JourneyDetails);
            SetupToolbar(Strings.PayCongestionCharge);

            _listAdapter = new JourneyDetailsExpandableListViewAdapter(this, ViewModel.GroupedJourneys, JourneyDetailsList);

            JourneyDetailsList.SetAdapter(_listAdapter);
            JourneyDetailsList.JustifyListViewHeightBasedOnChildren();

            ConfirmDetailsAndPay.SetClickCommand(ViewModel.NavigateToPaymentCommand);
            AddAnotherVehicle.SetClickCommand(ViewModel.AddAnotherVehicleCommand);

            Bindings.Add(this.SetBinding(() => ViewModel.ButtonText)
               .WhenSourceChanges(() => ConfirmDetailsAndPay.Text = ViewModel.ButtonText));
        }

        public override void OnBackPressed()
        {
            ViewModel.OnGoBack();
        }
    }
}