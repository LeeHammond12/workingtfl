﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Text;
using Android.Widget;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class LookupVrmActivity : BaseActivity<LookupVrmViewModel>
    {
        private Button _btnCheckVRM;
        private EditText _editTextVRM;

        private CheckBox _chkUkRegistered;

        public Button BtnCheckVRM => _btnCheckVRM ?? (_btnCheckVRM = FindButton(Resource.Id.btnCheckVRM));
        public EditText EditTextVRM => _editTextVRM ?? (_editTextVRM = FindViewById<EditText>(Resource.Id.editTextVRM));

        public CheckBox ChkUkRegistered => _chkUkRegistered ?? (_chkUkRegistered = FindViewById<CheckBox>(Resource.Id.chkUKRegistered));

        public override void OnBackPressed()
        {
            ViewModel.GoBack();
            SetBackNavigationTransition();
        }

        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            SetContentView(Resource.Layout.LookupVrm);
            SetupToolbar(ViewModel.PageTitle);

            if (ViewModel.HasFlowState)
            {
                PerformDataBind();
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            ViewModel?.GoBackIfNoFlowState();
        }

        private void PerformDataBind()
        {
            ChkUkRegistered.SetBook();
            EditTextVRM.SetBook();

            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.EnteredVrm, () => EditTextVRM.Text));
            Bindings.Add(this.SetTwoWayBinding(() => ViewModel.IsRegisteredInUk, () => ChkUkRegistered.Checked));
            EditTextVRM.SetFilters(new IInputFilter[] { new InputFilterAllCaps() });
            EditTextVRM.TextChanged += (sender, args) => { };

            BtnCheckVRM.Click += (sender, args) =>
            {
                KeyBoardHelper.HideKeyboard(this, Window);
                ViewModel.LookupVRMCommand.Execute(null);
            };

            EditTextVRM.AddSearchKeyHandler(() =>
            {
                if (ViewModel.CanLookupVrm)
                {
                    ViewModel.LookupVRMCommand.Execute(null);
                    DismissKeyBoard();
                }
            });
        }
    }
}