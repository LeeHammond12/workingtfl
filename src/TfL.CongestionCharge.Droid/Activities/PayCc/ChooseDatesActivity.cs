﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.Droid.Activities.PayCc;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class ChooseDatesActivity : BaseActivity<ChooseDatesViewModel>
    {
        private const int DialogCalendar = 1;

        private Button _btnChangeDates;
        private ListView _listViewJourneyDates;
        private ChooseDatesListAdapter _adapter;

        private Button ChangeDatesButton => _btnChangeDates ?? (_btnChangeDates = FindViewById<Button>(Resource.Id.btnChangeStartDate));
        public ListView ViewJourneyDatesList => _listViewJourneyDates ?? (_listViewJourneyDates = FindViewById<ListView>(Resource.Id.listViewJourneyDates));

        public override void OnBackPressed()
        {
            ViewModel.GoBack();
            SetBackNavigationTransition();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ChooseDates);
            SetupToolbar(Strings.ChooseDates);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            ViewJourneyDatesList.Adapter = _adapter = new ChooseDatesListAdapter(this);
            ViewJourneyDatesList.JustifyListViewHeightBasedOnChildren();

            ChangeDatesButton.Click += (_, __) => ShowDialog(DialogCalendar);
        }

        protected override void OnResume()
        {
            base.OnResume();
            ViewModel?.GoBackIfNoFlowState();
        }

        protected override Dialog OnCreateDialog(int id)
        {
            if (id == DialogCalendar)
            {
                return new DatePickerDialog(this, HandleDatePicker, DateTime.Today.Year, DateTime.Today.Month - 1, DateTime.Today.Day);
            }
            return base.OnCreateDialog(id);
        }

        private async void HandleDatePicker(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            ViewModel.StartDate = e.Date;

            if (((View)sender).IsShown)
            {
                await ViewModel.PopulateCharges(true);

                if (ViewModel.Daily?.Date != null)
                {
                    ChangeDatesButton.Text = ViewModel.Daily.Date.Value.ToHumanReadableFormat() + " - " + Strings.ChangeDate;
                }

                _adapter.NotifyDataSetChanged();
                ViewJourneyDatesList.JustifyListViewHeightBasedOnChildren();
            }
        }
    }
}