﻿using System;
using System.Collections.Generic;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using Thumbmunkeys.Common.Platform.CSharpCompatibility;

namespace TfL.CongestionCharge.Droid.Activities.PayCc
{
    public class ChooseDatesListAdapter : BaseAdapter<ChargeViewModelBase>
    {
        private readonly List<View> _containerViews = new List<View>();
        private readonly ChooseDatesActivity _actContext;
        private ChooseDatesViewModel ViewModel => _actContext.ViewModel;
        private IReadOnlyList<ChargeViewModelBase> Charges => ViewModel.Charges;

        public ChooseDatesListAdapter(ChooseDatesActivity context) : base()
        {
            _actContext = context;
        }

        public override int Count => Charges.Count;

        public override ChargeViewModelBase this[int position] => Charges[position];

        public override long GetItemId(int position) => position;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var charge = Charges[position];

            var view = _actContext.LayoutInflater.Inflate(Resource.Layout.pay_for_dates_item, null);

            if (!_containerViews.Contains(view))
            {
                _containerViews.Add(view);
            }

            var single = charge as SingleDayChargeViewModel;
            var multiple = charge as MultipleDayChargeViewModel;

            var txtPayableAmount = view.FindViewById<TextView>(Resource.Id.txtPayableAmount);
            txtPayableAmount.Text = charge.Amount.ToCurrencyString();

            var txtDayDescription = view.FindViewById<TextView>(Resource.Id.txtDayDescription);
            txtDayDescription.Text = charge.ChargePeriodString;
            var txtDate = view.FindViewById<TextView>(Resource.Id.txtDate);
            txtDate.Text = charge.DateString;

            var txtConsecutiveDays = view.FindViewById<TextView>(Resource.Id.txtConsecutiveDays);
            txtConsecutiveDays.Visibility = ViewStates.Gone;

            if (multiple != null)
            {
                txtConsecutiveDays.Visibility = ViewStates.Visible;
                txtConsecutiveDays.Text = multiple.ConsecutiveDaysString;
            }

            view.Click -= SelectCharge;
            view.Click += SelectCharge;
            view.Tag = new JObjectWrapper(charge);

            return view;
        }

        private async void SelectCharge(object sender, EventArgs e)
        {
            var charge = (ChargeViewModelBase)((JObjectWrapper)((View)sender).Tag)?.Object;
            await ViewModel.SelectCharge(charge);
        }
    }
}