﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.TableHandlers;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class WhichVehicleActivity : BaseActivity<WhichVehicleViewModel>
    {
        private ListView _listRegisteredVehicle;
        private Button _btnAddNewVehicle;
        private VehiclesListAdapter _adapter;

        public ListView RegisteredVehicleList => _listRegisteredVehicle ?? (_listRegisteredVehicle = FindViewById<ListView>(Resource.Id.listViewRegisteredVehicle));

        public override void OnBackPressed()
        {
            ViewModel.GoBack();
            SetBackNavigationTransition();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.PCC_Select_Vehicle);
            SetupToolbar(Strings.PayCongestionCharge);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            RegisteredVehicleList.Adapter = _adapter = new VehiclesListAdapter(LayoutInflater,
                Resource.Layout.vehicle_details_item, (v) => ViewModel.SetSelectedVrm(v), () => ViewModel.SavedVrms);

            RegisteredVehicleList.JustifyListViewHeightBasedOnChildren();

            _btnAddNewVehicle = FindButton(Resource.Id.btnAddNewVehicle);
            _btnAddNewVehicle.SetClickCommand(ViewModel.ChooseAnotherVehicleCommand);
        }

        protected override void OnResume()
        {
            base.OnResume();

            _adapter?.NotifyDataSetChanged();
            RegisteredVehicleList?.JustifyListViewHeightBasedOnChildren();
        }
    }
}