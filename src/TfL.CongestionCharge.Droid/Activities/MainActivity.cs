﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel.Main;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class MainActivity : BaseActivity<MainViewModel>
    {
        private Button _loginOrLogoutButton;
        private CardView _receiptsCardView;
        private LinearLayout _layoutViewReceipts;
        private TextView _txtViewReceipts;
        private ImageView _imageReceipt;

        private Button LoginOrLogoutButton
           => _loginOrLogoutButton ?? (_loginOrLogoutButton = FindButton(Resource.Id.buttonSignIn));

        protected override void OnStart()
        {
            base.OnStart();

            if (LifecycleCallbacks.WasInBackground)
            {
                LifecycleCallbacks.Reset();
            }
        }

        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // ignore errors on this page
            await ViewModel.InitViewModel();

            SetContentView(Resource.Layout.Main);

            _receiptsCardView = FindViewById<CardView>(Resource.Id.receiptsCardView);
            _receiptsCardView.Enabled = false;

            var ccCardView = FindViewById<CardView>(Resource.Id.ccCardView);
            var postCodeCardView = FindViewById<CardView>(Resource.Id.postCodeCardView);
            var pcnCardView = FindViewById<CardView>(Resource.Id.pcnCardView);
            var autoPayCardView = FindViewById<CardView>(Resource.Id.autoPayCardView);
            var yourAccountCardView = FindViewById<CardView>(Resource.Id.yourAccountCardView);
            _layoutViewReceipts = FindViewById<LinearLayout>(Resource.Id.buttonViewReceipts);
            _txtViewReceipts = FindViewById<TextView>(Resource.Id.txtViewReceipts);
            _imageReceipt = FindViewById<ImageView>(Resource.Id.imageReceipt);

            ccCardView.SetClickCommand(ViewModel.NavigateToPayCcCommand);
            pcnCardView.SetClickCommand(ViewModel.NavigateToPCNLookupCommand);
            postCodeCardView.SetClickCommand(ViewModel.NavigateToPostCodeLookupCommand);
            _receiptsCardView.SetClickCommand(ViewModel.NavigateToReceiptsCommand);
            yourAccountCardView.SetClickCommand(ViewModel.NavigateToYourAccountCommand);
            autoPayCardView.SetClickCommand(ViewModel.NavigateToAutoPayCommand);
            LoginOrLogoutButton.SetClickCommand(ViewModel.LoginOrLogoutCommand);

            var manageStoredInformationButton = FindViewById<CardView>(Resource.Id.manageStoredInformationCardView);
            manageStoredInformationButton.SetClickCommand(ViewModel.NavigateToManageStoredInformationCommand);

            SetReceiptButtonUi();

            if (ViewModel.IsLoggedIn)
            {
                autoPayCardView.Visibility = ViewStates.Visible;
                manageStoredInformationButton.Visibility = ViewStates.Gone;

                LoginOrLogoutButton.SetText(Strings.SignOut, TextView.BufferType.Normal);
            }
            else
            {
                yourAccountCardView.Visibility = ViewStates.Gone;
                autoPayCardView.Visibility = ViewStates.Gone;
            }
        }

        private void SetReceiptButtonUi()
        {
            if (!ViewModel.IsLoggedIn)
            {
                _receiptsCardView.Enabled = ViewModel.ReceiptsAvailable;

                var alpha = ViewModel.ReceiptsAvailable ? 1.0f : 0.5f;
                _layoutViewReceipts.Alpha = alpha;
                _txtViewReceipts.Alpha = alpha;
                _imageReceipt.Alpha = alpha;
            }
            else
            {
                _receiptsCardView.Visibility = ViewStates.Gone;
            }
        }
    }
}