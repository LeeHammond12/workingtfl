﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.LoggedInAccount;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.TableHandlers;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class AutoPayVehiclesActivity : BaseActivity<AutoPayVehiclesViewModel>
    {
        private Button _addAnotherVehicleButton;
        private ListView _listActiveVehicles;
        private ListView _listInactiveVehicles;
        private LinearLayout _layoutInactiveVehicles;
        private LinearLayout _layoutactiveVehicles;

        public Button AddAnotherVehicleButton => _addAnotherVehicleButton ?? (_addAnotherVehicleButton = FindButton(Resource.Id.addAnotherVehicleButton));

        public ListView ActiveVehiclesList => _listActiveVehicles ?? (_listActiveVehicles = FindViewById<ListView>(Resource.Id.listActiveVehicles));
        public ListView InactiveVehiclesList => _listInactiveVehicles ?? (_listInactiveVehicles = FindViewById<ListView>(Resource.Id.listInactiveVehicles));
        public LinearLayout LayoutInactiveVehicles => _layoutInactiveVehicles ?? (_layoutInactiveVehicles = FindViewById<LinearLayout>(Resource.Id.layoutInactiveVehicles));
        public LinearLayout LayoutActiveVehicles => _layoutactiveVehicles ?? (_layoutactiveVehicles = FindViewById<LinearLayout>(Resource.Id.layoutactiveVehicles));

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AutoPayVehicles);
            SetupToolbar(Strings.ManageCCAutoPay);

            FinishOnCreate();
        }

        private async void FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            InitLists();

            AddAnotherVehicleButton.SetClickCommand(ViewModel.NavigateToAddAnotherVehicleCommand);
        }

        private async void AddVehicle(VehicleViewModel vehicleViewModel)
        {
            await ViewModel.AddVehicleToAutoPay(vehicleViewModel);
            InitLists();
        }

        private async void RemoveVehicle(VehicleViewModel vehicleViewModel)
        {
            await ViewModel.RemoveVehicleFromAutoPay(vehicleViewModel);
            InitLists();
        }

        private void InitLists()
        {
            ActiveVehiclesList.Adapter = new VehiclesListAdapter(LayoutInflater,
                Resource.Layout.autopay_remove_vehicle_item_view, RemoveVehicle, () => ViewModel.ActiveVehicles);
            ActiveVehiclesList.JustifyListViewHeightBasedOnChildren();
            LayoutActiveVehicles.Visibility = ActiveVehiclesList.Adapter.Count == 0 ? ViewStates.Gone : ViewStates.Visible;

            InactiveVehiclesList.Adapter = new VehiclesListAdapter(LayoutInflater,
                Resource.Layout.autopay_add_vehicle_item_view, AddVehicle, () => ViewModel.InactiveVehicles);
            InactiveVehiclesList.JustifyListViewHeightBasedOnChildren();
            LayoutInactiveVehicles.Visibility = InactiveVehiclesList.Adapter.Count == 0 ? ViewStates.Gone : ViewStates.Visible;
        }
    }
}