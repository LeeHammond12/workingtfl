﻿using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Helpers;
using JimBobBennett.MvvmLight.AppCompat;
using Ninject.Activation;
using Tfl.CongestionCharge.Core.Environment;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.Storage;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.Droid.Environment;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;
using Thumbmunkeys.Common.Platform.Storage.KeyValueStorage;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ExcludeFromRecents = true, WindowSoftInputMode = SoftInput.StateAlwaysHidden)]
    public class BaseActivity<TViewModel> : AppCompatActivityBase where TViewModel : TflViewModelBase
    {
        private IKeyValueStore _uiStateStore;

        private IKeyValueStore UiStateStore => _uiStateStore ?? (_uiStateStore = new SharedPreferencesKeyValueStore("UiStateStore"));

        // prevents the bindings from being garbage collected
        protected List<Binding> Bindings = new List<Binding>();

        public TViewModel ViewModel { get; }

        protected AppNavigationService NavigationService
            => (AppNavigationService)AppEnvironment.Current.NavigationService;

        protected ProgressDialog ProgressDialog { get; private set; }

        public BaseActivity()
        {
            ForegroundActivity.Current = this;

            ViewModel = AppEnvironment.Current.GetViewModel<TViewModel>();
            ViewModel.RestoreUiState(UiStateStore);
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            ViewModel.SaveUiState(UiStateStore);
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            SetBackNavigationTransition();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId != Android.Resource.Id.Home)
            {
                return base.OnOptionsItemSelected(item);
            }

            OnBackPressed();
            OverridePendingTransition(Resource.Animation.slide_in_right, Resource.Animation.slide_out_right);
            return true;
        }

        protected override void OnPause()
        {
            base.OnPause();
            SaveAppState();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

#if USE_PRODUCTION_API 
            Window.SetFlags(WindowManagerFlags.Secure, WindowManagerFlags.Secure);
#endif

            //// hide the keyboard
            //Window.SetSoftInputMode(SoftInput.StateHidden);
            KeyBoardHelper.HideKeyboard(this, Window);

            CreateProgressDialog();

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(HandleLoadingNotifications));
        }

        protected override void OnResume()
        {
            base.OnResume();
            ForegroundActivity.Current = this;
        }

        protected void DismissKeyBoard()
        {
            var view = CurrentFocus;
            if (view != null)
            {
                var imm = (InputMethodManager)GetSystemService(InputMethodService);
                imm.HideSoftInputFromWindow(view.WindowToken, 0);
            }
        }

        protected void SetupToolbar(string toolbarTitle)
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            var titleTextViewWithoutCancel = FindTextView(Resource.Id.ToolbarTitleWithoutCancel);
            if (ViewModel.IsGoBackToHomeButtonVisible)
            {
                toolbar.FindViewById<LinearLayout>(Resource.Id.mainLinearLayout).Visibility = ViewStates.Visible;
                titleTextViewWithoutCancel.Visibility = ViewStates.Gone;
                FindTextView(Resource.Id.ToolbarTitle).Text = toolbarTitle;
                toolbar.FindTextView(Resource.Id.ToolbarRightButton).Click += (s, e) =>
                {
                    if (ViewModel.GoBackToHomeCommand.CanExecute(null))
                    {
                        ViewModel.GoBackToHomeCommand.Execute(null);
                    }
                };
            }
            else
            {
                titleTextViewWithoutCancel.Text = toolbarTitle;
            }

            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        protected void HideToolbarBackButton()
        {
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetDisplayHomeAsUpEnabled(false);
        }

        protected void SetBackNavigationTransition()
        {
            OverridePendingTransition(Resource.Animation.slide_in_right, Resource.Animation.slide_out_right);
        }

        private static void SaveAppState()
        {
            var flowState = AppEnvironment.Current?.FlowState;
            flowState?.Save();
        }

        private void CreateProgressDialog()
        {
            ProgressDialog = new ProgressDialog(this);
            ProgressDialog.SetCancelable(false);
            ProgressDialog.SetTitle(Strings.PleaseWaitTitle);
            ProgressDialog.SetMessage(Strings.ThisMayTakeUpToAMinute);
        }

        protected TextView FindTextView(int id)
        {
            return FindViewById<TextView>(id);
        }

        protected Button FindButton(int id)
        {
            return FindViewById<Button>(id);
        }

        protected void SetClickCommand(int id, RelayCommand command)
        {
            FindViewById<View>(id).SetClickCommand(command);
        }

        private void HandleLoadingNotifications()
        {
            RunOnUiThread(() =>
            {
                try
                {
                    if (!IsFinishing)
                    {
                        if (ViewModel.IsBusy)
                        {
                            ProgressDialog?.Show();
                        }
                        else
                        {
                            ProgressDialog?.Dismiss();
                        }
                    }
                }
                catch (System.Exception)
                {
                }
            });
        }
    }
}