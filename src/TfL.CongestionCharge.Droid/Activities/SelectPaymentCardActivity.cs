﻿using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using JimBobBennett.MvvmLight.AppCompat;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using Tfl.CongestionCharge.Core.ViewModel.Payments;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.TableHandlers;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class SelectPaymentCardActivity : BaseActivity<SelectPaymentCardViewModel>
    {
        private ListView _listView;
        private Button _selectAnotherCardButton;
        private PaymentCardAdapter _paymentCardListAdapter;

        public ListView ListView => _listView ?? (_listView = FindViewById<ListView>(Resource.Id.listCards));
        public Button SelectAnotherCardButton => _selectAnotherCardButton ?? (_selectAnotherCardButton = FindButton(Resource.Id.selectAnotherCardButton));

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.SelectPaymentCard);
            SetupToolbar(ViewModel.PageTitle);

            ProgressDialog.SetTitle(Strings.PleaseWaitForPaymentTitle);

            await FinishOnCreate();
        }

        private async Task FinishOnCreate()
        {
            if (await ViewModel.InitViewModel() == ViewModelInitResult.Abort)
                return;

            SelectAnotherCardButton.SetClickCommand(ViewModel.ChooseAnotherCardCommand);

            PopulateStoredCards();
        }

        private void PopulateStoredCards()
        {
            if (!ViewModel.UserPaymentCards.Any())
            {
                return;
            }

            ListView.Adapter = _paymentCardListAdapter = new PaymentCardAdapter(
                LayoutInflater,
                Resource.Layout.paymentCardWithArrowButton,
                SelectPaymentCard,
                () => ViewModel.UserPaymentCards);
            ListView.JustifyListViewHeightBasedOnChildren();
        }

        private void SelectPaymentCard(PaymentCardViewModel card)
        {
            var alert = new AlertDialog.Builder(this, Resource.Style.CustomDialogTheme).Create();

            var view = CurrentActivity.LayoutInflater.Inflate(Resource.Layout.CvvInputDialog, null);

            var cardNameText = view.FindViewById<TextView>(Resource.Id.cardNameText);
            cardNameText.Text = card.IssuerWithCardPostfix;

            var cardEndingInText = view.FindViewById<TextView>(Resource.Id.cardEndingInText);
            cardEndingInText.Text = card.CardNumber;

            var cardExpiresInText = view.FindViewById<TextView>(Resource.Id.cardExpiresInText);
            cardExpiresInText.Text = card.ExpiryDateWithSeparator;

            var image = PaymentCardUtils.GetImageResourceFromCardName(card.Issuer);
            if (image > 0)
            {
                var paymentsCardImage = view.FindViewById<ImageView>(Resource.Id.paymentsCardImage);
                paymentsCardImage.SetImageResource(image);
            }
            var smsVisibility = ViewModel.IsPcn ? ViewStates.Gone : ViewStates.Visible;
            view.FindViewById(Resource.Id.sendReceiptViaTextLayout).Visibility = smsVisibility;

            var smsText = view.FindViewById<EditText>(Resource.Id.editNumber);
            smsText.SetBook();
            smsText.Text = ViewModel.SmsNumber;

            var chkSendReceiptViaSms = view.FindViewById<CheckBox>(Resource.Id.chkReceiptViaSMS);

            chkSendReceiptViaSms.CheckedChange += (s, e) =>
            {
                ViewModel.IsSmsTextVisible = chkSendReceiptViaSms.Checked;
                smsText.Visibility = ViewModel.IsSmsTextVisible ? ViewStates.Visible : ViewStates.Gone;
                KeyBoardHelper.HideKeyboard(alert.Context, alert.Window);
            };

            var sendButton = view.FindButton(Resource.Id.btnConfirmAndPay);
            sendButton.Text = ViewModel.PaymentButtonText;

            sendButton.Click += async (s, e) =>
            {
                KeyBoardHelper.HideKeyboard(alert.Context, alert.Window);
                var cvvText = view.FindViewById<EditText>(Resource.Id.editCardCVV2);
                cvvText.SetBookLight();

                ViewModel.Cvv2 = cvvText.Text;

                ViewModel.SmsNumber = smsText.Text;

                await ViewModel.PayWithCard(card);
            };

            var hintImage = view.FindViewById<ImageView>(Resource.Id.hintImage);
            var hintLayout = view.FindViewById(Resource.Id.hintLayout);
            var closePopupImage = view.FindViewById<ImageView>(Resource.Id.closePopupImage);

            closePopupImage.Click += (sender, args) =>
            {
                alert.Hide();
            };

            hintImage.Click += (sender, args) =>
            {
                switch (hintLayout.Visibility)
                {
                    case ViewStates.Gone:
                        hintLayout.Visibility = ViewStates.Visible;
                        hintImage.SetImageResource(Resource.Drawable.remove);
                        break;

                    case ViewStates.Visible:
                        hintLayout.Visibility = ViewStates.Gone;
                        hintImage.SetImageResource(Resource.Drawable.help);
                        break;
                }
            };
            alert.SetView(view);
            alert.Show();
        }
    }
}