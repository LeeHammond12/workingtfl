﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Tfl.CongestionCharge.Core.Resources;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.Activities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/MyTheme")]
    public class TermsAndConditionsActivity : BaseActivity<TermsAndConditionsViewModel>
    {
        private Button _acceptButton, _goToWebButton;
        private TextView _privacyPageContent;

        public Button AcceptButton => _acceptButton ?? (_acceptButton = FindButton(Resource.Id.acceptButton));
        public Button GoToWebButton => _goToWebButton ?? (_goToWebButton = FindButton(Resource.Id.goToWebButton));
        public TextView PrivacyPageContent => _privacyPageContent ?? (_privacyPageContent = FindTextView(Resource.Id.textViewPrivacyContent));

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.TermAndConditions);

            SetupToolbar(Strings.TermsAndConditionTitle);

            if (!ViewModel.UserHasAcceptedTermsAndConditions)
            {
                HideToolbarBackButton();
            }

            AcceptButton.SetClickCommand(ViewModel.AcceptCommand);
            GoToWebButton.SetClickCommand(ViewModel.NavigateToPrivacyCommand);

            AcceptButton.Text = ViewModel.UserHasAcceptedTermsAndConditions
                ? Strings.Close
                : Strings.AcceptTermsAndCondition;

            BindPrivacyPageContent();
        }

        private void BindPrivacyPageContent()
        {
            PrivacyPageContent.SetText(ViewModel.PrivacyPageContent, TextView.BufferType.Normal);
        }
    }
}