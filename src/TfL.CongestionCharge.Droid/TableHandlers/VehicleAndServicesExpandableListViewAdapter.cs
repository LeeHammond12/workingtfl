﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.Model.AccountInfo;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.Droid.Activities;
using TfL.CongestionCharge.Droid.Extensions;
using Tfl.CongestionCharge.Core.Services;
using System.Linq;
using Android.App;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Support.V4.Content;

namespace TfL.CongestionCharge.Droid.TableHandlers
{
    public class VehicleAndServicesExpandableListViewAdapter : BaseExpandableListAdapter
    {
        private readonly YourVehiclesAndServicesActivity _ctx;
        private readonly ILoggedInUserInformationService _userInfo;
        private readonly Func<VehicleViewModel, Task> _deleteAction;

        private IReadOnlyList<VehicleViewModel> VehiclesAndServices => _userInfo.VehiclesWithServices;

        public override int GroupCount => VehiclesAndServices?.Count ?? 0;
        public override bool HasStableIds => false;

        public VehicleAndServicesExpandableListViewAdapter(
            YourVehiclesAndServicesActivity ctx,
            ILoggedInUserInformationService userInfo,
            Func<VehicleViewModel, Task> deleteAction)
        {
            _ctx = ctx;
            _userInfo = userInfo;
            _deleteAction = deleteAction;
        }

        public override Java.Lang.Object GetGroup(int groupPosition)
            => GetVehicle(groupPosition).Vrm;
        public override Java.Lang.Object GetChild(int groupPosition, int childPosition)
            => GetService(groupPosition, childPosition).Name;

        public override long GetChildId(int groupPosition, int childPosition)
            => childPosition;

        public override int GetChildrenCount(int groupPosition)
            => VehiclesAndServices[groupPosition]?.Services?.Count ?? 0;

        public override long GetGroupId(int groupPosition)
            => groupPosition;

        public override View GetChildView(int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent)
        {
            var service = GetService(groupPosition, childPosition);

            convertView = _ctx.LayoutInflater.Inflate(Resource.Layout.VehicleAndServiceListItem, null);
            if (service == null)
                return convertView;

            convertView.FindViewById<View>(Resource.Id.bottomDivider).Visibility = isLastChild ? ViewStates.Gone : ViewStates.Visible;
            var mainLayout = convertView.FindViewById<LinearLayout>(Resource.Id.mainLayoutVehicleServiceListItem);
            if (isLastChild)
            {
                mainLayout.SetBackgroundColor(Color.Transparent);
                mainLayout.Background = ContextCompat.GetDrawable(Application.Context, Resource.Drawable.rounded_bottom_without_border);
            }
            else
            {
                mainLayout.SetBackgroundColor(new Color(ContextCompat.GetColor(Application.Context, Resource.Color.lightgrey)));
            }

            var txtServiceName = convertView.FindTextView(Resource.Id.txtServiceName);
            txtServiceName.Text = service.Name ?? "Unknown";

            var expiresLabel = convertView.FindTextView(Resource.Id.txtExpires);
            var serviceExpireDate = convertView.FindTextView(Resource.Id.serviceExpireDate);

            if (service.EndDate.HasValue)
            {
                serviceExpireDate.Text = service.EndDate.Value.ToHumanReadableFormat();
                expiresLabel.Visibility = serviceExpireDate.Visibility = ViewStates.Visible;
            }
            else
            {
                expiresLabel.Visibility = serviceExpireDate.Visibility = ViewStates.Gone;
            }

            return convertView;
        }

        public override View GetGroupView(int groupPosition, bool isExpanded, View convertView, ViewGroup parent)
        {
            convertView = _ctx.LayoutInflater.Inflate(Resource.Layout.VehicleAndServiceListHeader, null);
            var txtVehicleRegNumber = convertView.FindTextView(Resource.Id.txtVehicleRegNumber);

            var vehicle = GetVehicle(groupPosition);
            if (string.IsNullOrWhiteSpace(vehicle.Vrm))
            {
                convertView.FindViewById(Resource.Id.vehicleLayout).Visibility = ViewStates.Gone;
            }
            else
            {
                convertView.FindViewById(Resource.Id.serviceLayout).Visibility = ViewStates.Gone;

                var vrm = vehicle.Vrm;

                txtVehicleRegNumber.Text = vrm;

                var txtVehicleDetail = convertView.FindTextView(Resource.Id.txtVehicleDetail);
                txtVehicleDetail.Text = vehicle.ColorMakeModel;

                var delete = convertView.FindViewById<ImageView>(Resource.Id.deleteImage);
                //#if !DEBUG
                delete.Visibility = ViewStates.Gone;
                //#endif
                delete.Click += async (s, e) => await _deleteAction?.Invoke(vehicle);
            }

            var expandableListView = (ExpandableListView)parent;
            expandableListView.ExpandGroup(groupPosition);

            return convertView;
        }

        public override bool IsChildSelectable(int groupPosition, int childPosition)
            => true;

        private VehicleViewModel GetVehicle(int position)
            => VehiclesAndServices[position];

        private AccountTflServiceDto GetService(int groupPosition, int childPosition)
            => VehiclesAndServices[groupPosition]?.Services[childPosition];
    }
}