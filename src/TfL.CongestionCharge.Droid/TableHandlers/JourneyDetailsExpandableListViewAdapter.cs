﻿using System.Collections.Generic;
using Android.App;
using Android.Graphics;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Tfl.CongestionCharge.Core.Extensions;
using Tfl.CongestionCharge.Core.ViewModel.PayCc;
using Tfl.CongestionCharge.Core.ViewModel.PayCc.State;
using TfL.CongestionCharge.Droid.Activities;
using TfL.CongestionCharge.Droid.Extensions;

namespace TfL.CongestionCharge.Droid.TableHandlers
{
    public class JourneyDetailsExpandableListViewAdapter : BaseExpandableListAdapter
    {
        private readonly JourneyDetailsActivity _ctx;
        private readonly List<JourneyDetailsViewModel.GroupedJourneyViewModel> _groupedJourneys;
        private readonly ExpandableListView _journeyDetailsList;
        private JourneyDetailsViewModel ViewModel => _ctx.ViewModel;

        public override int GroupCount => _groupedJourneys.Count;

        public override bool HasStableIds => false;

        public JourneyDetailsExpandableListViewAdapter(
            JourneyDetailsActivity context,
            List<JourneyDetailsViewModel.GroupedJourneyViewModel> groupedJourneys,
            ExpandableListView journeyDetailsList)
        {
            _ctx = context;
            _groupedJourneys = groupedJourneys;
            _journeyDetailsList = journeyDetailsList;
        }

        public override Object GetGroup(int groupPosition)
            => GetJourney(groupPosition).Vehicle.Vrm;

        public override Object GetChild(int groupPosition, int childPosition)
            => GetChildJourney(groupPosition, childPosition).GetHashCode().ToString();

        public override long GetChildId(int groupPosition, int childPosition)
            => childPosition;

        public override int GetChildrenCount(int groupPosition)
            => _groupedJourneys[groupPosition].Journeys.Count;

        public override long GetGroupId(int groupPosition)
            => groupPosition;

        public override View GetChildView(int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent)
        {
            var journey = GetChildJourney(groupPosition, childPosition);
            convertView = _ctx.LayoutInflater.Inflate(Resource.Layout.journeyDetailsListItem, null);

            convertView.FindViewById<View>(Resource.Id.bottomDividerJourneyDetailsListItem).Visibility = isLastChild ? ViewStates.Gone : ViewStates.Visible;
            var mainLayout = convertView.FindViewById<LinearLayout>(Resource.Id.mainLayoutJourneyDetailsListItem);
            if (isLastChild)
            {
                mainLayout.SetBackgroundColor(Color.Transparent);
                mainLayout.Background = ContextCompat.GetDrawable(Application.Context, Resource.Drawable.rounded_bottom_without_border);
            }
            else
            {
                mainLayout.SetBackgroundColor(new Color(ContextCompat.GetColor(Application.Context, Resource.Color.lightgrey)));
            }

            var txtJourneyDate = (TextView)convertView.FindViewById(Resource.Id.txtJourneyDate);
            txtJourneyDate.Text = journey.ChargeStartDate.ToHumanReadableFormat();

            var txtConsecutiveDays = convertView.FindTextView(Resource.Id.txtConsecutiveDays);
            txtConsecutiveDays.Text = journey.ConsecutiveDaysString;
            if (string.IsNullOrWhiteSpace(txtConsecutiveDays.Text))
                txtConsecutiveDays.Visibility = ViewStates.Gone;

            var txtAmount = convertView.FindTextView(Resource.Id.txtAmount);
            txtAmount.Text = journey.Price.ToCurrencyString();

            if (childPosition == 0)
            {
                var headingLinearLayout = convertView.FindViewById<RelativeLayout>(Resource.Id.headingLinearLayout);
                headingLinearLayout.Visibility = ViewStates.Visible;
            }

            var deleteView = convertView.FindViewById(Resource.Id.imgDelete);
            deleteView.Click += (s, e) => DeleteJourney(journey);

            return convertView;
        }

        private async void DeleteJourney(JourneyDetails journey)
        {
            if (!await ViewModel.DeleteJourney(journey))
                return;

            NotifyDataSetChanged();

            _ctx.RunOnUiThread(() => _journeyDetailsList.JustifyListViewHeightBasedOnChildren());
        }

        public override View GetGroupView(int groupPosition, bool isExpanded, View convertView, ViewGroup parent)
        {
            convertView = _ctx.LayoutInflater.Inflate(Resource.Layout.journeyDetailsListHeader, null);
            var txtVehicleRegNumber = convertView.FindTextView(Resource.Id.txtVehicleRegNumber);

            var journey = GetJourney(groupPosition);
            var vehicle = journey.Vehicle;

            txtVehicleRegNumber.Text = vehicle.Vrm;

            var txtVehicleDetail = convertView.FindTextView(Resource.Id.txtVehicleDetail);
            txtVehicleDetail.Text = vehicle.ColorMakeModel;

            var container = convertView.FindViewById<View>(Resource.Id.addDateLayout);
            container.Click += (s, e) => ViewModel.AddDate(vehicle, journey.IsRegisteredInUk);

            var expandableListView = (ExpandableListView)parent;
            expandableListView.ExpandGroup(groupPosition);

            return convertView;
        }

        public override bool IsChildSelectable(int groupPosition, int childPosition)
        {
            return false;
        }

        private JourneyDetailsViewModel.GroupedJourneyViewModel GetJourney(int groupPosition)
            => _groupedJourneys[groupPosition];

        private JourneyDetails GetChildJourney(int groupPosition, int childPosition)
            => _groupedJourneys[groupPosition].Journeys[childPosition];
    }
}