﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.ViewModel.Receipts;

namespace TfL.CongestionCharge.Droid.TableHandlers
{
    internal class ReceiptListAdapter : BaseAdapter<ReceiptViewModel>
    {
        private readonly IReadOnlyList<ReceiptViewModel> _listItems;
        private readonly Activity _actContext;

        public ReceiptListAdapter(Activity context, IReadOnlyList<ReceiptViewModel> items)
        {
            _actContext = context;
            _listItems = items;
        }

        public override int Count => _listItems.Count;

        public override ReceiptViewModel this[int position] => _listItems[position];

        public override long GetItemId(int position) => position;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _listItems[position];

            var view = _actContext.LayoutInflater.Inflate(Resource.Layout.listReceipt, null);
            var container = view.FindViewById<LinearLayout>(Resource.Id.receiptsContainer);

            foreach (var keyValuePair in item.Fields)
            {
                var itemView = _actContext.LayoutInflater.Inflate(Resource.Layout.receiptListItem, null);
                var left = itemView.FindViewById<TextView>(Resource.Id.left);
                var right = itemView.FindViewById<TextView>(Resource.Id.right);
                left.Text = keyValuePair.Key;
                right.Text = keyValuePair.Value;

                container.AddView(itemView);

                var line = itemView.FindViewById(Resource.Id.horizontalLine);
                line.Visibility = keyValuePair.Key == item.Fields.LastOrDefault().Key ? ViewStates.Invisible : ViewStates.Visible;
            }

            return view;
        }
    }
}