﻿using System;
using System.Collections.Generic;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;
using Thumbmunkeys.Common.Platform.CSharpCompatibility;

namespace TfL.CongestionCharge.Droid.TableHandlers
{
    public class PaymentCardAdapter : BaseAdapter<PaymentCardViewModel>
    {
        private readonly List<View> _containerViews = new List<View>();
        private readonly LayoutInflater _layoutInflater;
        private readonly int _listItemLayout;
        private readonly Action<PaymentCardViewModel> _clickAction;
        private readonly Func<IReadOnlyList<PaymentCardViewModel>> _cardsListGetter;

        public PaymentCardAdapter(
            LayoutInflater layoutInflater,
            int listItemLayout,
            Action<PaymentCardViewModel> clickAction,
            Func<IReadOnlyList<PaymentCardViewModel>> cardsListGetter)
        {
            _layoutInflater = layoutInflater;
            _listItemLayout = listItemLayout;
            _clickAction = clickAction;
            _cardsListGetter = cardsListGetter;
        }

        public override int Count => _cardsListGetter.Invoke().Count;

        public override PaymentCardViewModel this[int position] => _cardsListGetter.Invoke()[position];

        public override long GetItemId(int position) => position;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var card = _cardsListGetter.Invoke()[position];

            var view = convertView ?? _layoutInflater.Inflate(_listItemLayout, null);

            if (!_containerViews.Contains(view))
            {
                _containerViews.Add(view);
            }

            // view.Tag = card;
            var cardNameText = view.FindTextView(Resource.Id.cardNameText);
            cardNameText.Text = card.IssuerWithCardPostfix;

            var cardEndingInText = view.FindTextView(Resource.Id.cardEndingInText);
            cardEndingInText.Text = card.CardNumber;

            var cardExpiresInText = view.FindTextView(Resource.Id.cardExpiresInText);
            cardExpiresInText.Text = card.ExpiryDateWithSeparator;

            var cardImageId = PaymentCardUtils.GetImageResourceFromCardName(card.Issuer.ToLower());
            if (cardImageId > 0)
            {
                view.FindViewById<ImageView>(Resource.Id.paymentsCardImage).SetImageResource(cardImageId);
            }

            if (_clickAction != null)
            {
                var actionView = view.FindViewById(Resource.Id.actionButton) ?? view;

                actionView.Tag = new JObjectWrapper(card);
                actionView.Click -= ClickAction;
                actionView.Click += ClickAction;
            }

            return view;
        }

        private void ClickAction(object sender, EventArgs e)
        {
            var card = (PaymentCardViewModel)((JObjectWrapper)((View)sender).Tag).Object;
            _clickAction?.Invoke(card);
        }
    }
}