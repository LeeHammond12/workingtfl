﻿using System;
using System.Collections.Generic;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.Droid.Extensions;
using Thumbmunkeys.Common.Platform.CSharpCompatibility;

namespace TfL.CongestionCharge.Droid.TableHandlers
{
    public class VehiclesListAdapter : BaseAdapter<VehicleViewModel>
    {
        private readonly LayoutInflater _layoutInflater;
        private readonly int _layoutResource;
        private readonly Action<VehicleViewModel> _clickAction;
        private readonly Func<IReadOnlyList<VehicleViewModel>> _vehiclesGetter;

        private IReadOnlyList<VehicleViewModel> Vehicles => _vehiclesGetter.Invoke();

        public override int Count => Vehicles.Count;

        public override VehicleViewModel this[int position] => Vehicles[position];

        public override long GetItemId(int position) => position;

        public VehiclesListAdapter(
            LayoutInflater layoutInflater,
            int layoutResource,
            Action<VehicleViewModel> clickAction,
            Func<IReadOnlyList<VehicleViewModel>> vehiclesGetter)
        {
            _layoutInflater = layoutInflater;
            _layoutResource = layoutResource;
            _clickAction = clickAction;
            _vehiclesGetter = vehiclesGetter;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var vehicle = Vehicles[position];

            var view = convertView ?? _layoutInflater.Inflate(_layoutResource, null);

            view.Tag = vehicle.Vrm;
            var vrmText = view.FindTextView(Resource.Id.vrmText);
            vrmText.Text = vehicle.Vrm;

            var vehicledetailsText = view.FindTextView(Resource.Id.vehicledetailsText);
            vehicledetailsText.Text = vehicle.ColorMakeModel;

            if (_clickAction != null)
            {
                var actionView = view.FindViewById<View>(Resource.Id.actionButton) ?? view;
                actionView.Tag = new JObjectWrapper(vehicle);
                actionView.Click -= ClickAction;
                actionView.Click += ClickAction;
            }

            return view;
        }

        private void ClickAction(object sender, EventArgs e)
        {
            var vehicle = (VehicleViewModel)((JObjectWrapper)((View)sender).Tag).Object;
            _clickAction?.Invoke(vehicle);
        }
    }
}