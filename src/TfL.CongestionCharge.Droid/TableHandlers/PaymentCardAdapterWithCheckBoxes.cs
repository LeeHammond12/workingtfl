﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.ViewModel;
using TfL.CongestionCharge.Droid.Extensions;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.TableHandlers
{
    internal class PaymentCardAdapterWithCheckBoxes : BaseAdapter<PaymentCardViewModel>
    {
        private readonly LayoutInflater _layoutInflater;
        private readonly IReadOnlyList<PaymentCardViewModel> _cards;
        private readonly List<PaymentCardViewHolder> _viewHolders = new List<PaymentCardViewHolder>();
        private readonly Func<PaymentCardViewModel, Task<bool>> _cardSelectedAction;

        public PaymentCardAdapterWithCheckBoxes(
            LayoutInflater layoutInflater,
            IReadOnlyList<PaymentCardViewModel> cards,
            Func<PaymentCardViewModel, Task<bool>> cardSelectedAction)
        {
            _layoutInflater = layoutInflater;
            _cards = cards;
            _cardSelectedAction = cardSelectedAction;
        }

        public override int Count => _cards.Count;

        public override PaymentCardViewModel this[int position] => _cards[position];

        public override long GetItemId(int position) => position;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var card = _cards[position];
            return GetView(_layoutInflater, card).View;
        }

        public void DeselectAllExcept(PaymentCardViewModel selected)
        {
            var others = _viewHolders.Where(r => r.ViewModel != selected);
            foreach (var creditCardViewHolder in others)
            {
                creditCardViewHolder.RadioButton.Checked = false;
            }
        }

        private PaymentCardViewHolder GetView(LayoutInflater layoutInflater, PaymentCardViewModel card)
        {
            var view = layoutInflater.Inflate(Resource.Layout.card_with_radio_button, null);

            var image = PaymentCardUtils.GetImageResourceFromCardName(card.Issuer);
            if (image > 0)
            {
                view.FindViewById<ImageView>(Resource.Id.imgCard).SetImageResource(image);
            }

            var cardNameText = view.FindTextView(Resource.Id.cardNameText);
            cardNameText.Text = card.IssuerWithCardPostfix;

            var cardEndingInText = view.FindTextView(Resource.Id.cardEndingInText);
            cardEndingInText.Text = card.CardNumber;

            var cardExpiresInText = view.FindTextView(Resource.Id.cardExpiresInText);
            cardExpiresInText.Text = card.ExpiryDateWithSeparator;

            var radioButton = view.FindViewById<RadioButton>(Resource.Id.rdoSelectCard);
            radioButton.SetBook();

            if (card?.IsAutoPayCard == true)
            {
                radioButton.Checked = true;
            }

            var vh = _viewHolders.FirstOrDefault(v => v.ViewModel == card);
            if (vh == null)
            {
                vh = new PaymentCardViewHolder(card);
                _viewHolders.Add(vh);
            }

            vh.View = view;
            vh.RadioButton = radioButton;

            radioButton.CheckedChange += (s, e) => CheckedChange(vh);

            return vh;
        }

        private async void CheckedChange(PaymentCardViewHolder viewHolder)
        {
            if (!viewHolder.RadioButton.Checked)
                return;

            var selected = viewHolder;

            if (!await _cardSelectedAction.Invoke(selected.ViewModel))
            {
                selected.RadioButton.Checked = false;
            }
            else
            {
                DeselectAllExcept(selected.ViewModel);
            }
        }
    }
}