﻿using Android.Views;
using Android.Widget;
using Tfl.CongestionCharge.Core.ViewModel;

namespace TfL.CongestionCharge.Droid.TableHandlers
{
    public class PaymentCardViewHolder
    {
        public View View { get; set; }
        public RadioButton RadioButton { get; set; }
        public PaymentCardViewModel ViewModel { get; }

        public PaymentCardViewHolder(PaymentCardViewModel vm)
        {
            ViewModel = vm;
        }
    }
}