﻿using Android.Content;
using Android.Util;
using Android.Widget;
using TfL.CongestionCharge.Droid.Utils;

namespace TfL.CongestionCharge.Droid.Views
{
    public class TflButton : Button
    {
        public TflButton(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            Fonts.SetFont(this, context, attrs);
        }

        public TflButton(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Fonts.SetFont(this, context, attrs);
        }

        public TflButton(Context context) : base(context)
        {
        }
    }
}